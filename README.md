<h3>App Installation</h3>

<h4>PHP and extensions</h4>
```bash
sudo apt update && sudo apt install php7.4-fpm php7.4-intl php7.4-zip php7.4-gd php7.4-mysql php7.4-bcmath php7.4-opcache php7.4-soap php7.4-xmlrpc php7.4-tidy php7.4-imap php7.4-pspell php7.4-ssh2 php7.4-redis
```
---
<h4>Mysql</h4>
```bash
sudo apt install mysql-server-8.0

# Start MySQL Server and enable it to start on boot
sudo systemctl start mysql
sudo systemctl enable mysql
```
---
<h4>Nginx</h4>
```bash
sudo apt install nginx

#  Start Nginx Server and enable it to start on boot
sudo systemctl start nginx
sudo systemctl enable nginx
```
---
<h4>Composer</h4>
```bash
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === 'e21205b207c3ff031906575712edab6f13eb0b361f2085f1f1237b7126d785e826a450292b6cfd1d64d92e6563bbde02') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"

sudo mv composer.phar /usr/local/bin/composer
```
---
<h4>Beanstalkd</h4>
```bash
apt-get update \
    && apt-get install -y build-essential curl \
    && curl -sL https://github.com/beanstalkd/beanstalkd/archive/v1.11.tar.gz | tar xvz -C /tmp \
    && cd /tmp/beanstalkd-1.11 \
    && make \
    && cp beanstalkd /usr/bin
```
---
<h4>Redis</h4>
<h5>Pre Requirements</h5>
```bash
sudo apt install lsb-release curl gpg
```
<h5>Main Redis Installation</h5>
```bash
sudo add-apt-repository ppa:redislabs/redis

sudo apt-get update

sudo apt-get install redis
```

<h5>Verify Redis Version</h5>
```bash
redis-server --version
```
---
<h4>Supervisor</h4>
```bash
apt update && apt install -y supervisor && mkdir -p /var/log/supervisor
```
---
<h4>Swoole</h4>
```bash
wget https://github.com/redis/hiredis/archive/v0.14.1.tar.gz && tar zxvf v0.14.1.tar.gz \
    && cd hiredis-0.14.1/ \
    && make -j && make install \
    && ldconfig \
    && cd .. \
    && git clone https://github.com/swoole/swoole-src \
    && cd swoole-src/ \
    && git checkout tags/v4.4.16 \
    && phpize && ./configure \
    && make -j 4 && make install \
    && git clone https://github.com/swoole/ext-async \
    && cd ext-async/ \
    && git checkout tags/v4.4.16 \
    && phpize && ./configure \
    && make -j 4 && make install
```
---
<h4>Mysql Setup</h4>
Create database in mysql; For example "creditoff".
Import database from file dump.sql or run migrations(in next steps).

<h4>Clone project from repository</h4>
~ git should be installed
```bash
git clone git@bitbucket.org:agridasov94/creditoff.git
```
or it can be copied from source in to directory /var/www/creditoff

<h5>Configuration steps:</h5>
- run commands in terminal in project directory
  - ```sudo chmod +x init ``` make file executable
  - ```./init``` after that you should choose dev env
- copy or rename .env.example into .env file; Fill all configuration variables
- make copies of files *.conf in directory common/config/supervisor. You should remove .text from file naming.
- run ```composer install```
- run ```./yii migrate``` this command will create all tables; This command is optional if data was imported in previous step.

<h5>Nginx configuration</h5>
The Nginx configuration example can be found in the nginx.conf file at the project root.

  