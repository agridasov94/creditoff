#!/bin/bash

username="$1"
password="$2"
database="$3"
dumpSqlFile="/dump/dump.sql"

#mysqldump -Q -c -e -v -u "$username" -p"$password" "$database" > "/dump/dump.$(date +%d-%m-%Y_%H-%M-%S).data.sql"
mysql -v -u "$username" -p"$password" "$database" < "$dumpSqlFile" 2> /dev/null