#!/bin/bash

firstArg="$1"

runNginx () {
    docker run -it -p 8451:80 --volume ./frontend/nginx/template:/etc/nginx/templates --volume ./../:/var/www/app --name creditoff-frontend-nginx --network creditoff creditoff-frontend-nginx
}

runPhpFpm() {
    docker run -it --volume /var/www/creditoff:/var/www/app --name creditoff-frontend-php-fpm --network creditoff creditoff-frontend-php-fpm
}

runMysql() {
  docker run -it --volume /var/www/creditoff/docker/common/mysql/data/:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=1111 -e MYSQL_DATABASE=creditoff -e MYSQL_USER=creditoff -e MYSQL_PASSWORD=11111111 --network creditoff -p 3307:3306 --name creditoff-mysql creditoff-mysql
}

runBeanstalkd() {
  docker run -it -p 11301:11300 --network creditoff --name creditoff-beanstalkd creditoff-beanstalkd
}

runSupervisor() {
  docker run -it --volume ./../:/var/www/app --network creditoff --name creditoff-supervisor creditoff-supervisor
}

runSwoole() {
  docker run -it --volume ./../:/var/www/app --network creditoff --name creditoff-swoole creditoff-swoole
}

runRedis() {
  docker run -it -p 6380:6379 --network creditoff --name creditoff-redis creditoff-redis
}

startMysql() {
  docker start -a creditoff-mysql
}

startNginx() {
  docker start -a creditoff-frontend-nginx
}

startPhpFpm() {
  docker start -a creditoff-frontend-php-fpm
}

startBeanstalkd() {
  docker start -a creditoff-beanstalkd
}

startSupervisor() {
  docker start -a creditoff-supervisor
}

startSwoole() {
  docker start -a creditoff-swoole
}

startRedis() {
  docker start -a creditoff-redis
}

if [ "$firstArg" == "run-nginx" ]; then
    runNginx
elif [ "$firstArg" == "run-php-fpm" ]; then
    runPhpFpm
elif [ "$firstArg" == "run-mysql" ]; then
    runMysql
elif [ "$firstArg" == "run-beanstalkd" ]; then
    runBeanstalkd
elif [ "$firstArg" == "start-mysql" ]; then
    startMysql
elif [ "$firstArg" == "start-nginx" ]; then
    startNginx
elif [ "$firstArg" == "start-php-fpm" ]; then
    startPhpFpm
elif [ "$firstArg" == "start-beanstalkd" ]; then
    startBeanstalkd
elif [ "$firstArg" == "run-supervisor" ]; then
    runSupervisor
elif [ "$firstArg" == "start-supervisor" ]; then
    startSupervisor
elif [ "$firstArg" == "run-swoole" ]; then
    runSwoole
elif [ "$firstArg" == "run-redis" ]; then
    runRedis
elif [ "$firstArg" == "start-redis" ]; then
    startRedis
elif [ "$firstArg" == "start-swoole" ]; then
    startSwoole
fi