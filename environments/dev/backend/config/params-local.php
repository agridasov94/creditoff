<?php
return [
    'frontendHost' => $_ENV['FRONTEND_HOST'],
    'domain' => $_ENV['DOMAIN'],
    'websocketServer' => [
        'connectionUrl' => $_ENV['BACKEND_PARAMS_WEBSOCKET_SERVER_CONNECTION_URL']
    ],
    'backendHost' => $_ENV['BACKEND_HOST']
];
