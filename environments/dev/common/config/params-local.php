<?php
return [
    'backWsIdentityCookie' => [
        'name' => '_identity_ws', 'httpOnly' => true, 'domain' => $_ENV['WS_IDENTITY_COOKIE']
    ],
    'crypt' => [
        'method'    => 'aes-256-cbc',
        'password' => $_ENV['COMMON_MAIN_CONFIG_CRYPT_PASSWORD'],
        'iv' => $_ENV['COMMON_MAIN_CONFIG_CRYPT_IV']
    ]
];
