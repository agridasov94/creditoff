<?php

use yii\queue\beanstalk\Queue;

return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=' . $_ENV['MYSQL_HOST'] . ';dbname=' . $_ENV['MYSQL_DATABASE'],
            'username' => $_ENV['MYSQL_USER'],
            'password' => $_ENV['MYSQL_PASSWORD'],
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            'transport' => [
                'class' => Swift_SmtpTransport::class,
                'encryption' => 'tls',
                'host' => $_ENV['MAILER_TRANSPORT_HOST'],
                'port' => $_ENV['MAILER_TRANSPORT_PORT'],
                'username' => $_ENV['MAILER_TRANSPORT_USERNAME'],
                'password' => $_ENV['MAILER_TRANSPORT_PASSWORD']
            ]
        ],
        'queue_loan_application_distribution' => [
            'class' => Queue::class,
            'host' => $_ENV['QUEUE_LOAN_APPLICATION_DISTRIBUTION_HOST'],
            'port' => $_ENV['QUEUE_LOAN_APPLICATION_DISTRIBUTION_PORT'],
            'tube' => 'queue_loan_application_distribution',
        ],
        'queue_sms_job' => [
            'class' => Queue::class,
            'host' => $_ENV['QUEUE_SMS_HOST'],
            'port' => $_ENV['QUEUE_SMS_PORT'],
            'tube' => 'queue_sms_job'
        ]
    ],
];
