<?php

$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '',
        ],
        'reCaptcha3' => [
            'class'      => \kekaadrenalin\recaptcha3\ReCaptcha::class,
            'site_key'   => $_ENV['RE_CAPTCHA_SITE_KEY'],
            'secret_key' => $_ENV['RE_CAPTCHA_SECRET_KEY'],
        ],
    ],
];

if (!YII_ENV_TEST) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['*'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
