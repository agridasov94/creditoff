<?php
return [
    'components' => [
        'redis' => [
            'class' => \yii\redis\Connection::class,
            'hostname' => $_ENV['REDIS_HOST'],
            'unixSocket' => null,
            'port' => $_ENV['REDIS_PORT'],
            'database' => 0,
            'password' => null
        ]
    ]
];
