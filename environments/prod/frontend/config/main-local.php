<?php
return [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '',
        ],
        'reCaptcha3' => [
            'class'      => \kekaadrenalin\recaptcha3\ReCaptcha::class,
            'site_key'   => $_ENV['RE_CAPTCHA_SITE_KEY'],
            'secret_key' => $_ENV['RE_CAPTCHA_SECRET_KEY'],
        ],
    ],
];
