<?php
return [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '',
        ],
        'crypt' => [
            'class' => \backend\components\CryptComponent::class,
            'method' => 'aes-256-cbc',
            'password' => $_ENV['BACKEND_MAIN_CONFIG_CRYPT_PASSWORD'],
            'iv' => $_ENV['BACKEND_MAIN_CONFIG_CRYPT_IV']
        ]
    ],
];
