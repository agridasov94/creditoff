<?php

namespace common\src\entities\language;

use lajax\translatemanager\models\Language;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

class LanguageQuery
{
    public static function getList(int $cache = 3600): array
    {
        $data = Language::find()->where(['status' => Language::STATUS_ACTIVE])->cache($cache)->asArray()->all();
        return ArrayHelper::map($data, 'language_id', static function ($item) {
            return ucfirst($item['language']);
        });
    }

    /**
     * @param array $langIds
     * @return Language[]
     */
    public static function getListExcludingLang(array $langIds): array
    {
        $data = Language::find()->select(['language_id'])->where(['status' => Language::STATUS_ACTIVE])->andWhere(['NOT IN', 'language_id', $langIds])->all();
        return ArrayHelper::map($data, 'language_id', static function ($item) {
            return ucfirst($item['language_id']);
        });
    }

    public static function getListAscii(int $cache = 3600): array
    {
        $data = Language::find()->select(['language_id', 'name_ascii'])->where(['status' => Language::STATUS_ACTIVE])->cache($cache)->asArray()->all();
        return ArrayHelper::map($data, 'language_id', 'name_ascii');
    }
}