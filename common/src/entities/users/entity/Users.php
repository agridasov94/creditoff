<?php

namespace common\src\entities\users\entity;

use backend\src\entities\partnerCompany\amountHistory\entity\PartnerCompanyAmountHistory;
use backend\src\entities\partnerCompany\entity\PartnerCompany;
use backend\src\entities\partnerCompany\position\entity\PartnerCompanyPosition;
use backend\src\entities\usersProductAccess\entity\UsersProductAccess;
use common\src\entities\blog\categories\entity\BlogCategories;
use common\src\entities\blog\posts\entity\BlogPosts;
use common\src\entities\client\email\entity\ClientEmail;
use common\src\entities\client\employmentData\entity\ClientEmploymentData;
use common\src\entities\client\entity\Client;
use common\src\entities\client\passportData\entity\ClientPassportData;
use common\src\entities\client\phone\entity\ClientPhone;
use common\src\entities\product\entity\Product;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $verification_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property string $password write-only password
 * @property string $first_name [varchar(50)]
 * @property string $last_name [varchar(50)]
 * @property string $phone [varchar(50)]
 * @property int $partner_company_id [int]
 * @property int $partner_company_position_id [int]
 * @property string $created_dt [datetime]
 * @property string $updated_dt [datetime]
 * @property string $last_login_dt [datetime]
 * @property int $created_user_id [int]
 * @property int $updated_user_id [int]
 * @property bool $is_online [tinyint(1)]
 *
 * @property BlogCategories[] $blogCategoriesCreatedBy
 * @property BlogCategories[] $blogCategoriesUpdatedBy
 * @property BlogPosts[] $blogPostsCreatedBy
 * @property BlogPosts[] $blogPostsUpdatedBy
 * @property Client[] $clientsCreatedBy
 * @property Client[] $clientsUpdatedBy
 * @property ClientEmail[] $clientEmailsCreatedBy
 * @property ClientEmail[] $clientEmailsUpdatedBy
 * @property ClientEmploymentData[] $clientEmploymentDataCreatedBy
 * @property ClientEmploymentData[] $clientEmploymentDataUpdatedBy
 * @property ClientPassportData[] $clientPassportDataCreatedBy
 * @property ClientPassportData[] $clientPassportDataUpdatedBy
 * @property ClientPhone[] $clientPhonesCreatedBy
 * @property ClientPhone[] $clientPhonesUpdatedBy
 * @property PartnerCompany[] $partnerCompaniesCreatedBy
 * @property PartnerCompany[] $partnerCompaniesUpdatedBy
 * @property PartnerCompanyAmountHistory[] $partnerCompanyAmountHistoriesCreatedBy
 * @property PartnerCompanyPosition[] $partnerCompanyPositionsCreatedBy
 * @property PartnerCompanyPosition[] $partnerCompanyPositionsUpdatedBy
 * @property Product[] $productsCreatedBy
 * @property PartnerCompany $partnerCompany
 * @property PartnerCompanyPosition $partnerCompanyPosition
 * @property UsersProductAccess[] $usersProductAccess
 * @property string $uid [varchar(15)]
 */
class Users extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;

    const STATUS_LABEL_LIST = [
        self::STATUS_DELETED => 'badge badge-danger',
        self::STATUS_ACTIVE => 'badge badge-success',
        self::STATUS_INACTIVE => 'badge badge-warning'
    ];

    const STATUS_IS_ONLINE = [
        0 => 'badge badge-danger',
        1 => 'badge badge-success'
    ];

    public const ROLE_ROOT = 'root';
    public const ROLE_ADMIN = 'admin';

    public $rolesName = null;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%users}}';
    }

    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_dt'],
                ],
                'value' => date('Y-m-d H:i:s') //new Expression('NOW()'),
            ],
            'user' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_user_id',
                'updatedByAttribute' => null,
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'password_hash', 'email', 'phone'], 'required'],
            [['status', 'is_online', 'partner_company_id', 'partner_company_position_id', 'created_user_id', 'updated_user_id'], 'integer'],
            ['status', 'default', 'value' => self::STATUS_INACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE, self::STATUS_DELETED]],
            [['created_dt', 'updated_dt', 'last_login_dt'], 'safe'],
            [['username', 'password_hash', 'password_reset_token', 'email', 'verification_token'], 'string', 'max' => 255],
            [['first_name', 'last_name', 'phone'], 'string', 'max' => 50],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['phone'], 'unique'],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds user by verification email token
     *
     * @param string $token verify email token
     * @return static|null
     */
    public static function findByVerificationToken($token) {
        return static::findOne([
            'verification_token' => $token,
            'status' => self::STATUS_INACTIVE
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Generates new token for email verification
     */
    public function generateEmailVerificationToken()
    {
        $this->verification_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function loginHandler(): void
    {
        $this->is_online = 1;
        $this->last_login_dt = date('Y-m-d H:i:s');
        $this->save();
    }

    public function logoutHandler(): void
    {
        $this->is_online = 0;
        $this->save();
    }

    public function attributeLabels(): array
    {
        return [
            'id' => Yii::t('product', 'ID'),
            'username' => Yii::t('product', 'Username'),
            'first_name' => Yii::t('product', 'First Name'),
            'last_name' => Yii::t('product', 'Last Name'),
            'auth_key' => Yii::t('product', 'Auth Key'),
            'password_hash' => Yii::t('product', 'Password Hash'),
            'password_reset_token' => Yii::t('product', 'Password Reset Token'),
            'email' => Yii::t('product', 'Email'),
            'phone' => Yii::t('product', 'Phone'),
            'status' => Yii::t('product', 'Status'),
            'is_online' => Yii::t('product', 'Is Online'),
            'partner_company_id' => Yii::t('product', 'Partner Company ID'),
            'partner_company_position_id' => Yii::t('product', 'Partner Company Position ID'),
            'created_dt' => Yii::t('product', 'Created Dt'),
            'updated_dt' => Yii::t('product', 'Updated Dt'),
            'verification_token' => Yii::t('product', 'Verification Token'),
            'last_login_dt' => Yii::t('product', 'Last Login Dt'),
            'created_user_id' => Yii::t('product', 'Created User'),
            'updated_user_id' => Yii::t('product', 'Updated User'),
        ];
    }

    /**
     * Gets query for [[BlogCategories]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\blog\categories\entity\Scope
     */
    public function getBlogCategoriesCreatedBy()
    {
        return $this->hasMany(BlogCategories::class, ['bc_created_user_id' => 'id']);
    }

    /**
     * Gets query for [[BlogCategories0]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\blog\categories\entity\Scope
     */
    public function getBlogCategoriesUpdatedBy()
    {
        return $this->hasMany(BlogCategories::class, ['bc_updated_user_id' => 'id']);
    }

    /**
     * Gets query for [[BlogPosts]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\blog\posts\entity\Scope
     */
    public function getBlogPostsAuthorBy()
    {
        return $this->hasMany(BlogPosts::class, ['bp_author_id' => 'id']);
    }

    /**
     * Gets query for [[BlogPosts0]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\blog\posts\entity\Scope
     */
    public function getBlogPostsCreatedBy()
    {
        return $this->hasMany(BlogPosts::class, ['bp_created_user_id' => 'id']);
    }

    /**
     * Gets query for [[BlogPosts1]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\blog\posts\entity\Scope
     */
    public function getBlogPostsUpdatedBy()
    {
        return $this->hasMany(BlogPosts::class, ['bp_updated_user_id' => 'id']);
    }

    /**
     * Gets query for [[Clients]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\client\entity\Scope
     */
    public function getClientsCreatedBy()
    {
        return $this->hasMany(Client::class, ['c_created_user_id' => 'id']);
    }

    /**
     * Gets query for [[Clients0]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\client\entity\Scope
     */
    public function getClientsUpdatedBy()
    {
        return $this->hasMany(Client::class, ['c_updated_user_id' => 'id']);
    }

    /**
     * Gets query for [[ClientEmails]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\client\email\entity\Scope
     */
    public function getClientEmailsCreatedBy()
    {
        return $this->hasMany(ClientEmail::class, ['ce_created_user_id' => 'id']);
    }

    /**
     * Gets query for [[ClientEmails0]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\client\email\entity\Scope
     */
    public function getClientEmailsUpdatedBy()
    {
        return $this->hasMany(ClientEmail::class, ['ce_updated_user_id' => 'id']);
    }

    /**
     * Gets query for [[ClientEmploymentDatas]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\client\employmentData\entity\Scope
     */
    public function getClientEmploymentDataCreatedBy()
    {
        return $this->hasMany(ClientEmploymentData::class, ['ced_created_user_id' => 'id']);
    }

    /**
     * Gets query for [[ClientEmploymentDatas0]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\client\employmentData\entity\Scope
     */
    public function getClientEmploymentDataUpdatedBy()
    {
        return $this->hasMany(ClientEmploymentData::class, ['ced_updated_user_id' => 'id']);
    }

    /**
     * Gets query for [[ClientPassportDatas]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\client\passportData\entity\Scope
     */
    public function getClientPassportDataCreatedBy()
    {
        return $this->hasMany(ClientPassportData::class, ['cpd_created_user_id' => 'id']);
    }

    /**
     * Gets query for [[ClientPassportDatas0]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\client\passportData\entity\Scope
     */
    public function getClientPassportDataUpdatedBy()
    {
        return $this->hasMany(ClientPassportData::class, ['cpd_updated_user_id' => 'id']);
    }

    /**
     * Gets query for [[ClientPhones]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\client\phone\entity\Scope
     */
    public function getClientPhonesCreatedBy()
    {
        return $this->hasMany(ClientPhone::class, ['cp_created_user_id' => 'id']);
    }

    /**
     * Gets query for [[ClientPhones0]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\client\phone\entity\Scope
     */
    public function getClientPhonesUpdatedBy()
    {
        return $this->hasMany(ClientPhone::class, ['cp_updated_user_id' => 'id']);
    }

    /**
     * Gets query for [[PartnerCompanies]].
     *
     * @return \yii\db\ActiveQuery|\backend\src\entities\partnerCompany\entity\Scope
     */
    public function getPartnerCompaniesCreatedBy()
    {
        return $this->hasMany(PartnerCompany::class, ['pc_created_user_id' => 'id']);
    }

    /**
     * Gets query for [[PartnerCompanies0]].
     *
     * @return \yii\db\ActiveQuery|\backend\src\entities\partnerCompany\entity\Scope
     */
    public function getPartnerCompaniesUpdatedBy()
    {
        return $this->hasMany(PartnerCompany::class, ['pc_updated_user_id' => 'id']);
    }

    /**
     * Gets query for [[PartnerCompanyAmountHistories]].
     *
     * @return \yii\db\ActiveQuery|\backend\src\entities\partnerCompany\amountHistory\entity\Scope
     */
    public function getPartnerCompanyAmountHistoriesCreatedBy()
    {
        return $this->hasMany(PartnerCompanyAmountHistory::class, ['pcah_created_user_id' => 'id']);
    }

    /**
     * Gets query for [[PartnerCompanyPositions]].
     *
     * @return \yii\db\ActiveQuery|\backend\src\entities\partnerCompany\position\entity\Scope
     */
    public function getPartnerCompanyPositionsCreatedBy()
    {
        return $this->hasMany(PartnerCompanyPosition::class, ['pcp_created_user_id' => 'id']);
    }

    /**
     * Gets query for [[PartnerCompanyPositions0]].
     *
     * @return \yii\db\ActiveQuery|\backend\src\entities\partnerCompany\position\entity\Scope
     */
    public function getPartnerCompanyPositionsUpdatedBy()
    {
        return $this->hasMany(PartnerCompanyPosition::class, ['pcp_updated_user_id' => 'id']);
    }

    /**
     * Gets query for [[Products]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\product\entity\Scope
     */
    public function getProductsCreatedBy()
    {
        return $this->hasMany(Product::class, ['p_created_user_id' => 'id']);
    }

    /**
     * Gets query for [[Products0]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\product\entity\Scope
     */
    public function getProductsUpdatedBy()
    {
        return $this->hasMany(Product::class, ['p_updated_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery|\backend\src\entities\partnerCompany\entity\Scope
     */
    public function getPartnerCompany()
    {
        return $this->hasOne(PartnerCompany::class, ['pc_id' => 'partner_company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery|\backend\src\entities\partnerCompany\position\entity\Scope
     */
    public function getPartnerCompanyPosition()
    {
        return $this->hasOne(PartnerCompanyPosition::class, ['pcp_id' => 'partner_company_position_id']);
    }

    public static function getStatusNameList(): array
    {
        return [
            self::STATUS_DELETED => Yii::t('users', 'Deleted'),
            self::STATUS_ACTIVE => Yii::t('users', 'Active'),
            self::STATUS_INACTIVE => Yii::t('users', 'Deactivated')
        ];
    }

    public function getStatusName(): ?string
    {
        return self::getStatusNameList()[$this->status] ?? null;
    }

    public function getFullName(): string
    {
        $fullName = trim($this->first_name . ' ' . $this->last_name);
        return $fullName ?: $this->username;
    }

    /**
     * @param bool $onlyNames
     * @return array
     */
    public function getRoles($onlyNames = false): array
    {
        if ($this->rolesName === null) {
            //todo
            $query = (new Query())->select('b.*')
                ->from(['a' => 'auth_assignment', 'b' => 'auth_item'])
                ->where('{{a}}.[[item_name]]={{b}}.[[name]]')
                ->andWhere(['a.user_id' => (string) $this->id])
                ->andWhere(['b.type' => 1]);
            $this->rolesName = ArrayHelper::map($query->all(), 'name', 'name');
//            $this->rolesName = ArrayHelper::map(Yii::$app->authManager->getRolesByUser($this->id), 'name', 'description');
        }
        if ($onlyNames) {
            return array_keys($this->rolesName);
        }
        return $this->rolesName;
    }

    /**
     * @return array
     */
    public static function getAllRoles(): array
    {
        $auth = \Yii::$app->authManager;
        $result = $auth->getRoles();
        return ArrayHelper::map($result, 'name', 'name');
    }

    public static function create(
        string $username,
        string $firstName,
        string $lastName,
        ?string $email,
        string $phone,
        ?int $status,
        ?int $partnerCompanyId,
        ?int $partnerCompanyPositionId
    ):self {
        $self = new self();
        $self->username = $username;
        $self->first_name = $firstName;
        $self->last_name = $lastName;
        $self->email = $email;
        $self->phone = $phone;
        $self->status = $status;
        $self->partner_company_id = $partnerCompanyId;
        $self->partner_company_position_id = $partnerCompanyPositionId;
        $self->generateUid();
        return $self;
    }

    /**
     * @return \yii\db\ActiveQuery|Scope
     */
    public function getCreatedUser()
    {
        return $this->hasOne(self::class, ['id' => 'created_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery|Scope
     */
    public function getUpdatedUser()
    {
        return $this->hasOne(self::class, ['id' => 'updated_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery|\backend\src\entities\usersProductAccess\entity\Scope
     */
    public function getUsersProductAccess()
    {
        return $this->hasMany(UsersProductAccess::class, ['upa_user_id' => 'id']);
    }

    public function setInActiveStatus(): void
    {
        $this->status = self::STATUS_INACTIVE;
    }

    public function getStatusNameLabel(): string
    {
        return Html::tag('span', $this->getStatusName(), ['class' => self::STATUS_LABEL_LIST[$this->status] ?? '']);
    }

    public function isDeletedOrDeactivated(): bool
    {
        return $this->status === self::STATUS_DELETED || $this->status === self::STATUS_INACTIVE;
    }

    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return in_array(self::ROLE_ADMIN, $this->getRoles(true), true);
    }

    /**
     * @return bool
     */
    public function isRoot(): bool
    {
        return in_array(self::ROLE_ROOT, $this->getRoles(true), true);
    }

    public function generateUid(): void
    {
        $this->uid = uniqid();
    }

    public static function getVerificationCodeKey(string $uid): string
    {
        return 'partner_' . $uid . '_code';
    }

    /**
     * {@inheritdoc}
     * @return Scope the active query used by this AR class.
     */
    public static function find()
    {
        return new Scope(get_called_class());
    }
}
