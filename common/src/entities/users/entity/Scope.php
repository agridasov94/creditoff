<?php

namespace common\src\entities\users\entity;

/**
 * This is the ActiveQuery class for [[BlogPosts]].
 *
 * @see BlogPosts
 */
class Scope extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Users[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Users|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function byId(int $id): self
    {
        return $this->andWhere(['p_id' => $id]);
    }

    public function active(): self
    {
        return $this->andWhere(['status' => Users::STATUS_ACTIVE]);
    }
}
