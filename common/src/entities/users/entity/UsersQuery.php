<?php

namespace common\src\entities\users\entity;

use backend\src\entities\partnerCompany\entity\PartnerCompany;
use backend\src\entities\usersProductAccess\entity\UsersProductAccess;
use common\src\entities\loanApplicationAccess\entity\LoanApplicationAccess;
use common\src\entities\loanApplicationAccess\entity\LoanApplicationAccessStatus;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class UsersQuery
{
    public static function findByUsernameAndPartialPhoneNumber(string $username, $partialPhoneNumber): ?Users
    {
        return Users::find()->select(['phone', 'uid'])
            ->where(['username' => $username])
            ->andWhere(['LIKE', 'phone', '%' . $partialPhoneNumber, false])
            ->one();
    }

    public static function findByUid(string $uid): ?Users
    {
        return Users::findOne(['uid' => $uid]);
    }

    public static function getList(): array
    {
        $models = Users::find()->select(['id'])->addSelect(new Expression("concat(first_name, ' ', last_name) as full_name"))->asArray()->all();
        return ArrayHelper::map($models, 'id', 'full_name');
    }

    public static function getListUsername(): array
    {
        $models = Users::find()->select(['id'])->addSelect(new Expression("concat(first_name, ' ', last_name) as full_name"))->asArray()->all();
        return ArrayHelper::map($models, 'full_name', 'full_name');
    }

    /**
     * @param array $roles
     * @return Users[]
     */
    public static function getAllUsersByRoles(array $roles): array
    {
        return Users::find()
            ->innerJoin('auth_assignment', 'auth_assignment.user_id = id')
            ->andWhere(['auth_assignment.item_name' => $roles])
            ->all();
    }

    /**
     * @param int $loanApplicationId
     * @return Users[]
     */
    public static function findAvailableUsersForLoanApplication(int $loanApplicationId, int $productId, int $exceptCompany): array
    {
        return Users::find()
            ->alias('u')
            ->leftJoin(LoanApplicationAccess::tableName(), 'u.id = laa_user_id and laa_la_id = :applicationId and laa_status = :statusId', [
                'applicationId' => $loanApplicationId,
                'statusId' => LoanApplicationAccessStatus::DECLINED
            ])
            ->innerJoin(UsersProductAccess::tableName(), 'upa_user_id = u.id and upa_product_id = :productId', [
                'productId' => $productId
            ])
            ->innerJoin(PartnerCompany::tableName(), 'pc_id = partner_company_id')
            ->andWhere(['laa_id' => null])
            ->andWhere(['<>', 'partner_company_id', $exceptCompany])
            ->andWhere(['>', 'upa_request_price', 0])
            ->andWhere(['IS NOT', 'upa_request_price', null])
            ->andWhere(['or', 'pc_total_amount >= upa_request_price', 'pc_total_bonus_amount >= upa_request_price'])
            ->active()
            ->all();
    }
}