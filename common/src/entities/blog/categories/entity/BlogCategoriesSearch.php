<?php

namespace common\src\entities\blog\categories\entity;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\src\entities\blog\categories\entity\BlogCategories;

/**
 * BlogCategoriesSearch represents the model behind the search form of `common\src\entities\blog\categories\entity\BlogCategories`.
 */
class BlogCategoriesSearch extends BlogCategories
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bc_id', 'bc_created_user_id', 'bc_updated_user_id'], 'integer'],
            [['bc_lang_id', 'bc_name', 'bc_created_dt', 'bc_updated_dt', 'bc_url'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BlogCategories::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'bc_id' => $this->bc_id,
            'date(bc_created_dt)' => $this->bc_created_dt,
            'date(bc_updated_dt)' => $this->bc_updated_dt,
            'bc_created_user_id' => $this->bc_created_user_id,
            'bc_updated_user_id' => $this->bc_updated_user_id,
        ]);

        $query->andFilterWhere(['like', 'bc_lang_id', $this->bc_lang_id])
            ->andFilterWhere(['like', 'bc_url', $this->bc_url])
            ->andFilterWhere(['like', 'bc_name', $this->bc_name]);

        return $dataProvider;
    }
}
