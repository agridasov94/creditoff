<?php

namespace common\src\entities\blog\categories\entity;

use yii\helpers\ArrayHelper;

class BlogCategoriesQuery
{
    public static function getListByLang(?string $lang): array
    {
        $query = BlogCategories::find();
        if ($lang) {
            $query->byLang($lang);
        }
        $data = $query->asArray()->all();
        return ArrayHelper::map($data, 'bc_id', 'bc_name');
    }

    public static function findByLangId(?string $lang): array
    {
        $query = BlogCategories::find();
        if ($lang) {
            $query->byLang($lang);
        }
        return $query->all();
    }

    public static function findByLangAndUrl(string $lang, string $url): ?BlogCategories
    {
        $query = BlogCategories::find();
        $query->byLang($lang);
        $query->byUrl($url);
        return $query->one();
    }
}