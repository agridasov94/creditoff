<?php

namespace common\src\entities\blog\categories\entity;

use common\src\entities\blog\posts\entity\BlogPosts;
use common\src\entities\blog\posts\entity\Scope as PostsScope;
use common\src\entities\users\entity\Scope as UserScope;
use common\src\entities\users\entity\Users;
use lajax\translatemanager\models\Language;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "blog_categories".
 *
 * @property int $bc_id
 * @property string $bc_lang_id
 * @property string|null $bc_name
 * @property string|null $bc_created_dt
 * @property string|null $bc_updated_dt
 * @property int|null $bc_created_user_id
 * @property int|null $bc_updated_user_id
 *
 * @property Users $createdUser
 * @property Users $updatedUser
 * @property BlogPosts[] $blogPosts
 * @property Language $lang
 * @property string $bc_url [varchar(255)]
 */
class BlogCategories extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'blog_categories';
    }

    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['bc_created_dt'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['bc_updated_dt'],
                ],
                'value' => date('Y-m-d H:i:s') //new Expression('NOW()'),
            ],
            'user' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'bc_created_user_id',
                'updatedByAttribute' => 'bc_updated_user_id',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bc_lang_id', 'bc_url'], 'required'],
            [['bc_created_dt', 'bc_updated_dt'], 'safe'],
            [['bc_created_user_id', 'bc_updated_user_id'], 'integer'],
            [['bc_lang_id'], 'string', 'max' => 5],
            [['bc_name'], 'string', 'max' => 50],
            [['bc_url'], 'string', 'max' => 255],
            [['bc_lang_id', 'bc_url'], 'unique', 'targetAttribute' => ['bc_lang_id', 'bc_url']],
            [['bc_created_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['bc_created_user_id' => 'id']],
            [['bc_updated_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['bc_updated_user_id' => 'id']],
            [['bc_created_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['bc_created_user_id' => 'id']],
            [['bc_lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::class, 'targetAttribute' => ['bc_lang_id' => 'language_id']],
            [['bc_updated_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['bc_updated_user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'bc_id' => Yii::t('backend.blog_categories', 'ID'),
            'bc_lang_id' => Yii::t('backend.blog_categories', 'Lang ID'),
            'bc_name' => Yii::t('backend.blog_categories', 'Name'),
            'bc_created_dt' => Yii::t('backend.blog_categories', 'Created Dt'),
            'bc_updated_dt' => Yii::t('backend.blog_categories', 'Updated Dt'),
            'bc_created_user_id' => Yii::t('backend.blog_categories', 'Created User ID'),
            'bc_updated_user_id' => Yii::t('backend.blog_categories', 'Updated User ID'),
            'bc_url' => Yii::t('backend.blog_categories', 'URL Address'),
        ];
    }

    /**
     * Gets query for [[BcCreatedUser]].
     *
     * @return \yii\db\ActiveQuery|UserScope
     */
    public function getCreatedUser()
    {
        return $this->hasOne(Users::class, ['id' => 'bc_created_user_id']);
    }

    /**
     * Gets query for [[BcUpdatedUser]].
     *
     * @return \yii\db\ActiveQuery|UserScope
     */
    public function getUpdatedUser()
    {
        return $this->hasOne(Users::class, ['id' => 'bc_updated_user_id']);
    }

    /**
     * Gets query for [[BlogPosts]].
     *
     * @return \yii\db\ActiveQuery|PostsScope
     */
    public function getBlogPosts()
    {
        return $this->hasMany(BlogPosts::class, ['bp_category_id' => 'bc_id']);
    }

    /**
     * Gets query for [[BcLang]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(Language::class, ['language_id' => 'bc_lang_id']);
    }

    /**
     * {@inheritdoc}
     * @return Scope the active query used by this AR class.
     */
    public static function find()
    {
        return new Scope(get_called_class());
    }

    public function countRelatedPostsByLang(string $langId)
    {
        return $this->hasMany(BlogPosts::class, ['bp_category_id' => 'bc_id'])->where(['bp_lang_id' => $langId])->count();
    }

    public function getUrl(): string
    {
        return '/blog/category/' . $this->bc_url;
    }
}
