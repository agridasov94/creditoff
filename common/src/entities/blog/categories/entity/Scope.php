<?php

namespace common\src\entities\blog\categories\entity;

/**
 * This is the ActiveQuery class for [[BlogCategories]].
 *
 * @see BlogCategories
 */
class Scope extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return BlogCategories[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return BlogCategories|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function byLang(string $lang): self
    {
        return $this->andWhere(['bc_lang_id' => $lang]);
    }

    public function byUrl(string $url): self
    {
        return $this->andWhere(['bc_url' => $url]);
    }
}
