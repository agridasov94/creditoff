<?php

namespace common\src\entities\blog\posts\entity;

/**
 * This is the ActiveQuery class for [[BlogPosts]].
 *
 * @see BlogPosts
 */
class Scope extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return BlogPosts[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return BlogPosts|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function active(): self
    {
        return $this->andWhere(['bp_active' => true]);
    }

    public function byLang(string $lang): self
    {
        return $this->andWhere(['bp_lang_id' => $lang]);
    }

    public function byUrl(string $url): self
    {
        return $this->andWhere(['bp_url' => $url]);
    }
}
