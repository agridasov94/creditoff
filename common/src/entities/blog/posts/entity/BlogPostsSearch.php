<?php

namespace common\src\entities\blog\posts\entity;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\src\entities\blog\posts\entity\BlogPosts;

/**
 * BlogPostsSearch represents the model behind the search form of `common\src\entities\blog\posts\entity\BlogPosts`.
 */
class BlogPostsSearch extends BlogPosts
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bp_id', 'bp_category_id', 'bp_author_id', 'bp_active', 'bp_created_user_id', 'bp_updated_user_id'], 'integer'],
            [['bp_lang_id', 'bp_title', 'bp_short_description', 'bp_content', 'bp_alias', 'bp_created_dt', 'bp_updated_dt', 'bp_preview_image'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BlogPosts::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'bp_id' => $this->bp_id,
            'bp_category_id' => $this->bp_category_id,
            'bp_author_id' => $this->bp_author_id,
            'bp_active' => $this->bp_active,
            'bp_created_user_id' => $this->bp_created_user_id,
            'bp_updated_user_id' => $this->bp_updated_user_id,
            'bp_created_dt' => $this->bp_created_dt,
            'bp_updated_dt' => $this->bp_updated_dt,
        ]);

        $query->andFilterWhere(['like', 'bp_lang_id', $this->bp_lang_id])
            ->andFilterWhere(['like', 'bp_title', $this->bp_title])
            ->andFilterWhere(['like', 'bp_short_description', $this->bp_short_description])
            ->andFilterWhere(['like', 'bp_content', $this->bp_content])
            ->andFilterWhere(['like', 'bp_alias', $this->bp_alias]);

        return $dataProvider;
    }
}
