<?php

namespace common\src\entities\blog\posts\entity;

class BlogPostsQuery
{
    /**
     * @param int|null $excludeBlogId
     * @param string $langId
     * @param int $limit
     * @return BlogPosts[]
     */
    public static function getRandomOtherPostsExcluding(?int $excludeBlogId, string $langId, int $limit = 3): array
    {
        $query = BlogPosts::find()
            ->where(['bp_active' => true]);
        if ($excludeBlogId) {
            $query->andWhere(['<>', 'bp_id', $excludeBlogId]);
        }
        return $query->andWhere(['bp_lang_id' => $langId])
            ->limit($limit)
            ->orderBy('rand()')
            ->all();
    }

    public static function findByUrlAndLang(string $url, string $lang): ?BlogPosts
    {
        return BlogPosts::find()->byLang($lang)->byUrl($url)->active()->one();
    }

    /**
     * @param int $limit
     * @return BlogPosts[]
     */
    public static function findLastPosts(string $lang, int $limit = 2): array
    {
        return BlogPosts::find()->active()->byLang($lang)->limit(2)->orderBy(['bp_id' => SORT_DESC])->all();
    }
}