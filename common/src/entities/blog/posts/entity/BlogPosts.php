<?php

namespace common\src\entities\blog\posts\entity;

use backend\src\useCase\blogCreate\BlogPostCreateForm;
use common\src\entities\blog\categories\entity\BlogCategories;
use common\src\entities\blog\categories\entity\Scope as CategoriesScope;
use common\src\entities\language\LanguageQuery;
use common\src\entities\users\entity\Scope as UserScope;
use common\src\entities\users\entity\Users;
use common\src\helpers\app\AppHelper;
use lajax\translatemanager\models\Language;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * This is the model class for table "blog_posts".
 *
 * @property int $bp_id
 * @property string $bp_lang_id
 * @property int $bp_category_id
 * @property string $bp_title
 * @property string|null $bp_short_description
 * @property string|null $bp_content
 * @property int $bp_author_id
 * @property string|null $bp_alias
 * @property string|null $bp_preview_image
 * @property int|null $bp_active
 * @property int|null $bp_created_user_id
 * @property int|null $bp_updated_user_id
 * @property string|null $bp_created_dt
 * @property string|null $bp_updated_dt
 *
 * @property Users $author
 * @property BlogCategories $category
 * @property Users $createdUser
 * @property Users $updatedUser
 * @property Language $language
 * @property UploadedFile $preview
 * @property string $bp_url [varchar(255)]
 */
class BlogPosts extends \yii\db\ActiveRecord
{
    /**
     * @var UploadedFile
     */
    public $preview;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'blog_posts';
    }

    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['bp_created_dt'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['bp_updated_dt'],
                ],
                'value' => date('Y-m-d H:i:s') //new Expression('NOW()'),
            ],
            'user' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'bp_created_user_id',
                'updatedByAttribute' => 'bp_updated_user_id',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bp_lang_id', 'bp_category_id', 'bp_title', 'bp_author_id', 'bp_url'], 'required'],
            [['bp_category_id', 'bp_author_id', 'bp_active', 'bp_created_user_id', 'bp_updated_user_id'], 'integer'],
            [['bp_content', 'bp_url'], 'string'],
            [['bp_lang_id', 'bp_url'], 'unique', 'targetAttribute' => ['bp_lang_id', 'bp_url']],
            [['bp_created_dt', 'bp_updated_dt'], 'safe'],
            [['bp_lang_id'], 'string', 'max' => 5],
            [['bp_title'], 'string', 'max' => 150],
            [['bp_short_description', 'bp_alias', 'bp_preview_image', 'bp_url'], 'string', 'max' => 255],
            [['bp_author_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['bp_author_id' => 'id']],
            [['bp_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => BlogCategories::class, 'targetAttribute' => ['bp_category_id' => 'bc_id']],
            [['bp_created_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['bp_created_user_id' => 'id']],
            [['bp_updated_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['bp_updated_user_id' => 'id']],
            [['preview'], 'file', 'skipOnEmpty' => true, 'extensions' => ['png', 'jpg', 'jpeg']]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'bp_id' => Yii::t('backend.blog_posts', 'ID'),
            'bp_lang_id' => Yii::t('backend.blog_posts', 'Language'),
            'bp_category_id' => Yii::t('backend.blog_posts', 'Category'),
            'bp_title' => Yii::t('backend.blog_posts', 'Title'),
            'bp_short_description' => Yii::t('backend.blog_posts', 'Short Description'),
            'bp_content' => Yii::t('backend.blog_posts', 'Content'),
            'bp_author_id' => Yii::t('backend.blog_posts', 'Author ID'),
            'bp_alias' => Yii::t('backend.blog_posts', 'Alias'),
            'bp_preview_image' => Yii::t('backend.blog_posts', 'Preview Image'),
            'bp_active' => Yii::t('backend.blog_posts', 'Active'),
            'bp_created_user_id' => Yii::t('backend.blog_posts', 'Created User ID'),
            'bp_updated_user_id' => Yii::t('backend.blog_posts', 'Updated User ID'),
            'bp_created_dt' => Yii::t('backend.blog_posts', 'Created Dt'),
            'bp_updated_dt' => Yii::t('backend.blog_posts', 'Updated Dt'),
            'preview' => \Yii::t('backend.blog_posts', 'Preview'),
            'bp_url' => \Yii::t('backend.blog_posts', 'URL Address'),
        ];
    }

    /**
     * Gets query for [[BpAuthor]].
     *
     * @return \yii\db\ActiveQuery|UserScope
     */
    public function getAuthor()
    {
        return $this->hasOne(Users::class, ['id' => 'bp_author_id']);
    }

    /**
     * Gets query for [[BpCategory]].
     *
     * @return \yii\db\ActiveQuery|CategoriesScope
     */
    public function getCategory()
    {
        return $this->hasOne(BlogCategories::class, ['bc_id' => 'bp_category_id']);
    }

    /**
     * Gets query for [[BpCreatedUser]].
     *
     * @return \yii\db\ActiveQuery|UserScope
     */
    public function getCreatedUser()
    {
        return $this->hasOne(Users::class, ['id' => 'bp_created_user_id']);
    }

    /**
     * Gets query for [[BpCreatedUser]].
     *
     * @return \yii\db\ActiveQuery|LanguageQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Language::class, ['language_id' => 'bp_lang_id']);
    }

    /**
     * Gets query for [[BpUpdatedUser]].
     *
     * @return \yii\db\ActiveQuery|UserScope
     */
    public function getUpdatedUser()
    {
        return $this->hasOne(Users::class, ['id' => 'bp_updated_user_id']);
    }

    /**
     * {@inheritdoc}
     * @return Scope the active query used by this AR class.
     */
    public static function find()
    {
        return new Scope(get_called_class());
    }

    public static function create(
        BlogPostCreateForm $form
    ): self {
        $model = new static();
        $model->bp_lang_id = $form->langId;
        $model->bp_category_id = $form->categoryId;
        $model->bp_title = $form->title;
        $model->bp_short_description = $form->shortDescription;
        $model->bp_content = $form->content;
        $model->bp_author_id = $form->authorId;
        $model->bp_active = $form->active;
        $model->bp_url = AppHelper::stringToUrl($form->url, 255);
        $model->generateAlias();
        return $model;
    }

    public function generateAlias(): void
    {
        $this->bp_alias = md5($this->bp_lang_id . $this->bp_title);
    }

    public function setPreviewImage(string $image): void
    {
        $this->bp_preview_image = $image;
    }

    public function getPreviewFrontendPath(): string
    {
        return \Yii::getAlias('@blogFrontend/' . $this->bp_alias);
    }

    public function getPreviewBackendPath(): string
    {
        return \Yii::getAlias('@blogBackend/' . $this->bp_alias);
    }

    public function getPublicPreviewPath(): string
    {
        return '/images/blog/' . $this->bp_alias . '/' . $this->bp_preview_image;
    }

    public function getPreviewFrontendImagePath(): string
    {
        return $this->getPreviewFrontendPath() . '/' . $this->bp_preview_image;
    }

    public function getPreviewBackendImagePath(): string
    {
        return $this->getPreviewBackendPath() . '/' . $this->bp_preview_image;
    }

    public function getUrl(): string
    {
        return '/blog/post/' . $this->bp_url;
    }
}
