<?php

namespace common\src\entities\settings\entity;

use common\src\entities\users\entity\Scope;
use common\src\entities\users\entity\Users;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "settings".
 *
 * @property int $s_id
 * @property string $s_key
 * @property string|null $s_name
 * @property string|null $s_description
 * @property string $s_type
 * @property string $s_value
 * @property string|null $s_updated_dt
 * @property int|null $s_updated_user_id
 *
 * @property Users $updatedUser
 */
class Settings extends \yii\db\ActiveRecord
{
    public const TYPE_BOOL      = 'bool';
    public const TYPE_INT       = 'int';
    public const TYPE_DOUBLE    = 'double';
    public const TYPE_STRING    = 'string';
    public const TYPE_ARRAY     = 'array';
    public const TYPE_OBJECT    = 'object';
    public const TYPE_NULL      = 'null';

    public const TYPE_LIST      = [
        self::TYPE_BOOL     => 'boolean',
        self::TYPE_INT      => 'integer',
        self::TYPE_DOUBLE   => 'double',
        self::TYPE_STRING   => 'string',
        self::TYPE_ARRAY    => 'array',
        self::TYPE_OBJECT   => 'object',
        self::TYPE_NULL     => 'null',
    ];

    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['s_updated_dt'],
                ],
                'value' => date('Y-m-d H:i:s') //new Expression('NOW()'),
            ],
            'user' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => false,
                'updatedByAttribute' => 's_updated_user_id',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['s_key', 's_type', 's_value'], 'required'],
            [['s_updated_dt'], 'safe'],
            [['s_updated_user_id'], 'integer'],
            [['s_key', 's_name', 's_description', 's_value'], 'string', 'max' => 255],
            [['s_type'], 'string', 'max' => 10],
            [['s_key'], 'unique'],
            [['s_updated_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['s_updated_user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            's_id' => Yii::t('loan_application', 'ID'),
            's_key' => Yii::t('loan_application', 'Key'),
            's_name' => Yii::t('loan_application', 'Name'),
            's_description' => Yii::t('loan_application', 'Description'),
            's_type' => Yii::t('loan_application', 'Type'),
            's_value' => Yii::t('loan_application', 'Value'),
            's_updated_dt' => Yii::t('loan_application', 'Updated Dt'),
            's_updated_user_id' => Yii::t('loan_application', 'Updated User'),
        ];
    }

    /**
     * Gets query for [[SUpdatedUser]].
     *
     * @return \yii\db\ActiveQuery|Scope
     */
    public function getUpdatedUser()
    {
        return $this->hasOne(Users::class, ['id' => 's_updated_user_id']);
    }

    /**
     * {@inheritdoc}
     * @return Scopes the active query used by this AR class.
     */
    public static function find()
    {
        return new Scopes(get_called_class());
    }
}
