<?php

namespace common\src\entities\product\entity;

use backend\src\entities\usersProductAccess\entity\UsersProductAccess;
use backend\src\entities\usersProductAccess\entity\UsersProductAccessQuery;
use common\components\validators\IsArrayValidator;
use common\src\entities\users\entity\Users;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Json;

/**
 * This is the model class for table "product".
 *
 * @property int $p_id
 * @property string|null $p_name
 * @property int|null $p_is_active
 * @property int|null $p_min_credit_amount
 * @property int|null $p_max_credit_amount
 * @property int|null $p_min_credit_term
 * @property int|null $p_max_credit_term
 * @property int|null $p_min_request_price
 * @property int|null $p_created_user_id
 * @property int|null $p_updated_user_id
 * @property string|null $p_created_dt
 * @property string|null $p_updated_dt
 *
 * @property Users $createdUser
 * @property Users $updatedUser
 * @property string $p_step_in_queue [smallint unsigned]
 * @property bool $p_step_in_queue_auto_calculate [tinyint(1)]
 * @property UsersProductAccess[] $usersProductAccess
 * @property int $p_sort_order [smallint]
 * @property string $p_settings [json]
 */
class Product extends \yii\db\ActiveRecord
{
    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['p_created_dt'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['p_updated_dt'],
                ],
                'value' => date('Y-m-d H:i:s') //new Expression('NOW()'),
            ],
            'user' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'p_created_user_id',
                'updatedByAttribute' => 'p_updated_user_id',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['p_name'], 'required'],
            [['p_name'], 'string', 'max' => 100],
            [['p_is_active', 'p_min_credit_amount', 'p_max_credit_amount', 'p_min_credit_term',
                'p_max_credit_term', 'p_min_request_price', 'p_created_user_id', 'p_updated_user_id'], 'integer'],
            [['p_created_dt', 'p_updated_dt'], 'safe'],
            [['p_settings'], 'string'],
            [['p_step_in_queue'], 'integer', 'min' => 0],
            [['p_sort_order'], 'integer', 'min' => 0, 'max' => 32767],
            [['p_step_in_queue_auto_calculate'], 'boolean'],
            [['p_step_in_queue_auto_calculate'], 'default', 'value' => 1],
            [['p_min_request_price'], 'validatePrice'],
            [['p_created_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['p_created_user_id' => 'id']],
            [['p_updated_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['p_updated_user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'p_id' => Yii::t('product', 'ID'),
            'p_name' => Yii::t('product', 'Name'),
            'p_is_active' => Yii::t('product', 'Is Active'),
            'p_min_credit_amount' => Yii::t('product', 'Min Credit Amount'),
            'p_max_credit_amount' => Yii::t('product', 'Max Credit Amount'),
            'p_min_credit_term' => Yii::t('product', 'Min Credit Term'),
            'p_max_credit_term' => Yii::t('product', 'Max Credit Term'),
            'p_min_request_price' => Yii::t('product', 'Min Request Price'),
            'p_created_user_id' => Yii::t('product', 'Created User'),
            'p_updated_user_id' => Yii::t('product', 'Updated User'),
            'p_created_dt' => Yii::t('product', 'Created Dt'),
            'p_updated_dt' => Yii::t('product', 'Updated Dt'),
            'p_step_in_queue' => Yii::t('product', 'Step In Queue'),
            'p_step_in_queue_auto_calculate' => Yii::t('product', 'Step In Queue Auto Calculation'),
            'p_sort_order' => Yii::t('product', 'Sort Order Value'),
            'p_settings' => Yii::t('product', 'Settings')
        ];
    }

    /**
     * Gets query for [[PCreatedUser]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\users\entity\Scope
     */
    public function getCreatedUser()
    {
        return $this->hasOne(Users::class, ['id' => 'p_created_user_id']);
    }

    /**
     * Gets query for [[PUpdatedUser]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\users\entity\Scope
     */
    public function getUpdatedUser()
    {
        return $this->hasOne(Users::class, ['id' => 'p_updated_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery|\backend\src\entities\usersProductAccess\entity\Scope
     */
    public function getUsersProductAccess()
    {
        return $this->hasMany(UsersProductAccess::class, ['upa_product_id' => 'p_id']);
    }

    /**
     * {@inheritdoc}
     * @return Scope the active query used by this AR class.
     */
    public static function find()
    {
        return new Scope(get_called_class());
    }

    public function validatePrice()
    {
        if (!$this->isNewRecord) {
            $minProductPriceByUsers = UsersProductAccessQuery::getAccessPriceLessThen($this->p_id, $this->p_min_request_price);
            if ($minProductPriceByUsers) {
                $this->addError('p_min_request_price', Yii::t('product', '{attribute} cannot be larger then price: {price} of user: {username}', [
                    'attribute' => $this->getAttributeLabel('p_min_request_price'),
                    'price' => $minProductPriceByUsers->upa_request_price,
                    'username' => $minProductPriceByUsers->user->getFullName()
                ]));
            }
        }
    }

    public function getStepInQueueVariables(): array
    {
        return !empty($this->p_settings) ? (Json::decode($this->p_settings)['step_in_queue_variables'] ?? []) : [];
    }
}
