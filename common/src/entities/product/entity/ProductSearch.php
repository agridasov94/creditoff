<?php

namespace common\src\entities\product\entity;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\src\entities\product\entity\Product;

/**
 * ProductSearch represents the model behind the search form of `common\src\entities\product\entity\Product`.
 */
class ProductSearch extends Product
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['p_id', 'p_is_active', 'p_min_credit_amount', 'p_max_credit_amount', 'p_min_credit_term', 'p_max_credit_term',
                'p_min_request_price', 'p_created_user_id', 'p_updated_user_id', 'p_step_in_queue', 'p_step_in_queue_auto_calculate', 'p_sort_order'], 'integer'],
            [['p_name', 'p_created_dt', 'p_updated_dt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Product::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'p_id' => $this->p_id,
            'p_is_active' => $this->p_is_active,
            'p_min_credit_amount' => $this->p_min_credit_amount,
            'p_max_credit_amount' => $this->p_max_credit_amount,
            'p_min_credit_term' => $this->p_min_credit_term,
            'p_max_credit_term' => $this->p_max_credit_term,
            'p_min_request_price' => $this->p_min_request_price,
            'p_step_in_queue' => $this->p_step_in_queue,
            'p_step_in_queue_auto_calculate' => $this->p_step_in_queue_auto_calculate,
            'p_created_user_id' => $this->p_created_user_id,
            'p_updated_user_id' => $this->p_updated_user_id,
            'p_created_dt' => $this->p_created_dt,
            'p_updated_dt' => $this->p_updated_dt,
            'p_sort_order' => $this->p_sort_order,
        ]);

        $query->andFilterWhere(['like', 'p_name', $this->p_name]);

        return $dataProvider;
    }
}
