<?php

namespace common\src\entities\product\entity;

use backend\src\entities\usersProductAccess\entity\UsersProductAccess;

/**
 * This is the ActiveQuery class for [[Product]].
 *
 * @see Product
 */
class Scope extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Product[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Product|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function joinUserProductAccess(int $userId): Scope
    {
        return $this->innerJoin(UsersProductAccess::tableName(), 'upa_product_id = p_id and upa_user_id = :userId', [
            'userId' => $userId
        ]);
    }

    public function byId(int $id): self
    {
        return $this->andWhere(['p_id' => $id]);
    }

    public function active(): self
    {
        return $this->andWhere(['p_is_active' => true]);
    }
}
