<?php

namespace common\src\entities\product\entity;

class ProductRepository
{
    public function save(Product $product): int
    {
        if (!$product->save()) {
            throw new \RuntimeException($product->getErrorSummary(true)[0]);
        }
        return $product->p_id;
    }
}