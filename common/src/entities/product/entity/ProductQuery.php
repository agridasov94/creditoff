<?php

namespace common\src\entities\product\entity;

use backend\src\entities\usersProductAccess\entity\UsersProductAccess;
use common\src\entities\users\entity\Users;
use yii\helpers\ArrayHelper;

class ProductQuery
{
    public static function getList(): array
    {
        $products = Product::find()->select(['p_id', 'p_name'])->orderBy(['p_sort_order' => SORT_DESC])->asArray()->all();
        return ArrayHelper::map($products, 'p_id', 'p_name');
    }

    public static function getListByUserId(int $id): array
    {
        $products = Product::find()->select(['p_id', 'p_name'])->joinUserProductAccess($id)->orderBy(['p_sort_order' => SORT_DESC])->asArray()->all();
        return ArrayHelper::map($products, 'p_id', 'p_name');
    }

    public static function getProductById(int $id): ?Product
    {
        return Product::find()->select(['p_min_request_price'])->byId($id)->one();
    }

    public static function getActiveById(int $id): ?Product
    {
        return Product::find()->byId($id)->active()->one();
    }

    public static function getAllActive(): array
    {
        return Product::find()->active()->orderBy(['p_sort_order' => SORT_DESC])->all();
    }

    public static function getListIdsByUsersAssignedToCompany(int $id): array
    {
        $result = Product::find()->select('p_id')
            ->innerJoin(UsersProductAccess::tableName(), 'upa_product_id = p_id')
            ->innerJoin(Users::tableName(), 'id = upa_user_id')
            ->andWhere(['partner_company_id' => $id])
            ->groupBy(['p_id'])->asArray()->all();
        return ArrayHelper::map($result, 'p_id', 'p_id');
    }
}