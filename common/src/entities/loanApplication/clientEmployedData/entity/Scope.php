<?php

namespace common\src\entities\loanApplication\clientEmployedData\entity;

/**
 * This is the ActiveQuery class for [[LoanApplicationClientEmployedData]].
 *
 * @see LoanApplicationClientEmployedData
 */
class Scope extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return LoanApplicationClientEmployedData[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return LoanApplicationClientEmployedData|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
