<?php

namespace common\src\entities\loanApplication\clientEmployedData\entity;

class LoanApplicationClientEmployedDataRepository
{
    public function save(LoanApplicationClientEmployedData $model): int
    {
        if (!$model->save()) {
            throw new \RuntimeException($model->getErrorSummary(true)[0]);
        }
        return $model->laced_id;
    }
}