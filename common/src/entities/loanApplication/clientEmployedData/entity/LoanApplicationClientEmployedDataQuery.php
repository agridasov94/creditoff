<?php

namespace common\src\entities\loanApplication\clientEmployedData\entity;

class LoanApplicationClientEmployedDataQuery
{
    public static function removeByLoanApplicationId(int $id): void
    {
        LoanApplicationClientEmployedData::deleteAll(['laced_la_id' => $id]);
    }
}