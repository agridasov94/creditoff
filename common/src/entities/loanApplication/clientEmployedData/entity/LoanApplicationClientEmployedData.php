<?php

namespace common\src\entities\loanApplication\clientEmployedData\entity;

use common\src\entities\client\entity\Client;
use common\src\entities\client\entity\ClientQuery;
use common\src\entities\loanApplication\entity\LoanApplication;
use common\src\entities\loanApplication\entity\LoanApplicationQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "loan_application_client_employed_data".
 *
 * @property int $laced_id
 * @property int|null $laced_client_id
 * @property int|null $laced_la_id
 * @property int|null $laced_type
 * @property string|null $laced_work_phone
 * @property string|null $laced_organization_name
 * @property string|null $laced_position
 * @property string|null $laced_seniority
 * @property string|null $laced_official_wages
 * @property string|null $laced_unofficial_wages
 * @property string|null $laced_revenues_documented
 * @property string|null $laced_revenues_non_documented
 * @property string|null $laced_created_dt
 *
 * @property Client $client
 * @property LoanApplication $loanApplication
 */
class LoanApplicationClientEmployedData extends \yii\db\ActiveRecord
{
    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['laced_created_dt'],
                ],
                'value' => date('Y-m-d H:i:s') //new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'loan_application_client_employed_data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['laced_client_id', 'laced_la_id', 'laced_type'], 'integer'],
            [['laced_created_dt'], 'safe'],
            [['laced_work_phone'], 'string', 'max' => 30],
            [['laced_work_phone'], 'match', 'pattern' => '/^(07|06)([0-9]){7}$/', 'message' => \Yii::t('frontend', 'The phone number must be in the format 0XXXXXXXX and start with 07 or 06')],
            [['laced_organization_name', 'laced_position'], 'string', 'max' => 100],
            [['laced_seniority'], 'string', 'max' => 10],
            [['laced_official_wages', 'laced_unofficial_wages'], 'string', 'max' => 50],
            [['laced_revenues_documented', 'laced_revenues_non_documented'], 'string', 'max' => 255],
            [['laced_client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::class, 'targetAttribute' => ['laced_client_id' => 'c_id']],
            [['laced_la_id'], 'exist', 'skipOnError' => true, 'targetClass' => LoanApplication::class, 'targetAttribute' => ['laced_la_id' => 'la_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'laced_id' => Yii::t('loan_application', 'ID'),
            'laced_client_id' => Yii::t('loan_application', 'Client'),
            'laced_la_id' => Yii::t('loan_application', 'Loan Application ID'),
            'laced_type' => Yii::t('loan_application', 'Type'),
            'laced_work_phone' => Yii::t('loan_application', 'Work Phone'),
            'laced_organization_name' => Yii::t('loan_application', 'Organization Name'),
            'laced_position' => Yii::t('loan_application', 'Position'),
            'laced_seniority' => Yii::t('loan_application', 'Seniority'),
            'laced_official_wages' => Yii::t('loan_application', 'Official Wages'),
            'laced_unofficial_wages' => Yii::t('loan_application', 'Unofficial Wages'),
            'laced_revenues_documented' => Yii::t('loan_application', 'Revenues Documented'),
            'laced_revenues_non_documented' => Yii::t('loan_application', 'Revenues Non Documented'),
            'laced_created_dt' => Yii::t('loan_application', 'Created Dt'),
        ];
    }

    /**
     * Gets query for [[LacedClient]].
     *
     * @return \yii\db\ActiveQuery|ClientQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::class, ['c_id' => 'laced_client_id']);
    }

    /**
     * Gets query for [[LacedLa]].
     *
     * @return \yii\db\ActiveQuery|LoanApplicationQuery
     */
    public function getLoanApplication()
    {
        return $this->hasOne(LoanApplication::class, ['la_id' => 'laced_la_id']);
    }

    /**
     * {@inheritdoc}
     * @return Scope the active query used by this AR class.
     */
    public static function find()
    {
        return new Scope(get_called_class());
    }
}
