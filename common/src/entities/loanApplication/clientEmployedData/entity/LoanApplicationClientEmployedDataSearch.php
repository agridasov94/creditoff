<?php

namespace common\src\entities\loanApplication\clientEmployedData\entity;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\src\entities\loanApplication\clientEmployedData\entity\LoanApplicationClientEmployedData;

/**
 * LoanApplicationClientEmployedDataSearch represents the model behind the search form of `common\src\entities\loanApplication\clientEmployedData\entity\LoanApplicationClientEmployedData`.
 */
class LoanApplicationClientEmployedDataSearch extends LoanApplicationClientEmployedData
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['laced_id', 'laced_client_id', 'laced_la_id', 'laced_type'], 'integer'],
            [['laced_work_phone', 'laced_organization_name', 'laced_position', 'laced_seniority', 'laced_official_wages', 'laced_unofficial_wages', 'laced_revenues_documented', 'laced_revenues_non_documented', 'laced_created_dt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LoanApplicationClientEmployedData::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'laced_id' => $this->laced_id,
            'laced_client_id' => $this->laced_client_id,
            'laced_la_id' => $this->laced_la_id,
            'laced_type' => $this->laced_type,
            'laced_created_dt' => $this->laced_created_dt,
        ]);

        $query->andFilterWhere(['like', 'laced_work_phone', $this->laced_work_phone])
            ->andFilterWhere(['like', 'laced_organization_name', $this->laced_organization_name])
            ->andFilterWhere(['like', 'laced_position', $this->laced_position])
            ->andFilterWhere(['like', 'laced_seniority', $this->laced_seniority])
            ->andFilterWhere(['like', 'laced_official_wages', $this->laced_official_wages])
            ->andFilterWhere(['like', 'laced_unofficial_wages', $this->laced_unofficial_wages])
            ->andFilterWhere(['like', 'laced_revenues_documented', $this->laced_revenues_documented])
            ->andFilterWhere(['like', 'laced_revenues_non_documented', $this->laced_revenues_non_documented]);

        return $dataProvider;
    }
}
