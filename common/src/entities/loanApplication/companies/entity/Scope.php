<?php

namespace common\src\entities\loanApplication\companies\entity;

/**
 * This is the ActiveQuery class for [[LoanApplicationCompanies]].
 *
 * @see LoanApplicationCompanies
 */
class Scope extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return LoanApplicationCompanies[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return LoanApplicationCompanies|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function byApplicationId(int $id): self
    {
        return $this->andWhere(['lac_la_id' => $id]);
    }

    public function byCompanyId(int $id): self
    {
        return $this->andWhere(['lac_partner_company_id' => $id]);
    }
}
