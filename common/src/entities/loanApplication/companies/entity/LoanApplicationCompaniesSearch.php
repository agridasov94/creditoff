<?php

namespace common\src\entities\loanApplication\companies\entity;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\src\entities\loanApplication\companies\entity\LoanApplicationCompanies;

/**
 * LoanApplicationCompaniesSearch represents the model behind the search form of `common\src\entities\loanApplication\companies\entity\LoanApplicationCompanies`.
 */
class LoanApplicationCompaniesSearch extends LoanApplicationCompanies
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lac_la_id', 'lac_partner_company_id'], 'integer'],
            [['lac_created_dt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LoanApplicationCompanies::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'lac_la_id' => $this->lac_la_id,
            'lac_partner_company_id' => $this->lac_partner_company_id,
            'lac_created_dt' => $this->lac_created_dt,
        ]);

        return $dataProvider;
    }
}
