<?php

namespace common\src\entities\loanApplication\companies\entity;

use backend\src\entities\partnerCompany\entity\PartnerCompany;
use backend\src\entities\partnerCompany\entity\PartnerCompanyQuery;
use common\src\entities\loanApplication\entity\LoanApplication;
use common\src\entities\loanApplication\entity\LoanApplicationQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "loan_application_companies".
 *
 * @property int $lac_la_id
 * @property int $lac_partner_company_id
 * @property string|null $lac_created_dt
 *
 * @property LoanApplication $loanApplication
 * @property PartnerCompany $partnerCompany
 */
class LoanApplicationCompanies extends \yii\db\ActiveRecord
{
    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['lac_created_dt'],
                ],
                'value' => date('Y-m-d H:i:s') //new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'loan_application_companies';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lac_la_id', 'lac_partner_company_id'], 'required'],
            [['lac_la_id', 'lac_partner_company_id'], 'integer'],
            [['lac_created_dt'], 'safe'],
            [['lac_la_id', 'lac_partner_company_id'], 'unique', 'targetAttribute' => ['lac_la_id', 'lac_partner_company_id']],
            [['lac_la_id'], 'exist', 'skipOnError' => true, 'targetClass' => LoanApplication::class, 'targetAttribute' => ['lac_la_id' => 'la_id']],
            [['lac_partner_company_id'], 'exist', 'skipOnError' => true, 'targetClass' => PartnerCompany::class, 'targetAttribute' => ['lac_partner_company_id' => 'pc_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'lac_la_id' => Yii::t('loan_application', 'Loan Application ID'),
            'lac_partner_company_id' => Yii::t('loan_application', 'Partner Company ID'),
            'lac_created_dt' => Yii::t('loan_application', 'Created Dt'),
        ];
    }

    /**
     * Gets query for [[LacLa]].
     *
     * @return \yii\db\ActiveQuery|LoanApplicationQuery
     */
    public function getLoanApplication()
    {
        return $this->hasOne(LoanApplication::class, ['la_id' => 'lac_la_id']);
    }

    /**
     * Gets query for [[LacPartnerCompany]].
     *
     * @return \yii\db\ActiveQuery|PartnerCompanyQuery
     */
    public function getPartnerCompany()
    {
        return $this->hasOne(PartnerCompany::class, ['pc_id' => 'lac_partner_company_id']);
    }

    /**
     * {@inheritdoc}
     * @return Scope the active query used by this AR class.
     */
    public static function find()
    {
        return new Scope(get_called_class());
    }
}
