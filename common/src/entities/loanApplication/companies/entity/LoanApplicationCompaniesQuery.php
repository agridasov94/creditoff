<?php

namespace common\src\entities\loanApplication\companies\entity;

class LoanApplicationCompaniesQuery
{
    public static function applicationCanViewedByCompany(int $applicationId, int $companyId): bool
    {
        return LoanApplicationCompanies::find()
            ->byApplicationId($applicationId)
            ->byCompanyId($companyId)
            ->exists();
    }
}