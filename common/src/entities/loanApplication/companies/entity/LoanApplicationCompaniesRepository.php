<?php

namespace common\src\entities\loanApplication\companies\entity;

class LoanApplicationCompaniesRepository
{
    public function save(LoanApplicationCompanies $model): void
    {
        if (!$model->save()) {
            throw new \RuntimeException($model->getErrorSummary(true)[0]);
        }
    }
}