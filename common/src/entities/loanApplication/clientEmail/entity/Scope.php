<?php

namespace common\src\entities\loanApplication\clientEmail\entity;

/**
 * This is the ActiveQuery class for [[LoanApplicationClientEmail]].
 *
 * @see LoanApplicationClientEmail
 */
class Scope extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return LoanApplicationClientEmail[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return LoanApplicationClientEmail|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function byClientId(int $id): self
    {
        return $this->andWhere(['lace_client_id' => $id]);
    }

    public function byLoanApplicationId(int $id): self
    {
        return $this->andWhere(['lace_la_id' => $id]);
    }
}
