<?php

namespace common\src\entities\loanApplication\clientEmail\entity;

class LoanApplicationClientEmailRepository
{
    public function save(LoanApplicationClientEmail $model): int
    {
        if (!$model->save()) {
            throw new \RuntimeException($model->getErrorSummary(true)[0]);
        }
        return $model->lace_id;
    }
}