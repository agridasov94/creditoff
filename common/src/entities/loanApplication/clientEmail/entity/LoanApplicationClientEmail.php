<?php

namespace common\src\entities\loanApplication\clientEmail\entity;

use common\src\entities\client\entity\Client;
use common\src\entities\client\entity\ClientQuery;
use common\src\entities\loanApplication\entity\LoanApplication;
use common\src\entities\loanApplication\entity\LoanApplicationQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "loan_application_client_email".
 *
 * @property int $lace_id
 * @property int|null $lace_client_id
 * @property int $lace_la_id
 * @property string $lace_email
 * @property string|null $lace_created_dt
 *
 * @property Client $client
 * @property LoanApplication $loanApplication
 */
class LoanApplicationClientEmail extends \yii\db\ActiveRecord
{
    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['lace_created_dt'],
                ],
                'value' => date('Y-m-d H:i:s') //new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'loan_application_client_email';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lace_client_id', 'lace_la_id'], 'integer'],
            [['lace_la_id', 'lace_email'], 'required'],
            [['lace_created_dt'], 'safe'],
            [['lace_email'], 'string', 'max' => 100],
            [['lace_client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::class, 'targetAttribute' => ['lace_client_id' => 'c_id']],
            [['lace_la_id'], 'exist', 'skipOnError' => true, 'targetClass' => LoanApplication::class, 'targetAttribute' => ['lace_la_id' => 'la_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'lace_id' => Yii::t('loan_application', 'Id'),
            'lace_client_id' => Yii::t('loan_application', 'Client'),
            'lace_la_id' => Yii::t('loan_application', 'Loan Application Id'),
            'lace_email' => Yii::t('loan_application', 'Email'),
            'lace_created_dt' => Yii::t('loan_application', 'Created'),
        ];
    }

    /**
     * Gets query for [[LaceClient]].
     *
     * @return \yii\db\ActiveQuery|ClientQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::class, ['c_id' => 'lace_client_id']);
    }

    /**
     * Gets query for [[LaceLa]].
     *
     * @return \yii\db\ActiveQuery|LoanApplicationQuery
     */
    public function getLoanApplication()
    {
        return $this->hasOne(LoanApplication::class, ['la_id' => 'lace_la_id']);
    }

    /**
     * {@inheritdoc}
     * @return Scope the active query used by this AR class.
     */
    public static function find()
    {
        return new Scope(get_called_class());
    }
}
