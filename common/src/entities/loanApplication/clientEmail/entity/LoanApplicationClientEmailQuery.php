<?php

namespace common\src\entities\loanApplication\clientEmail\entity;

class LoanApplicationClientEmailQuery
{
    public static function removeByLoanApplicationId(int $id): void
    {
        LoanApplicationClientEmail::deleteAll(['lace_la_id' => $id]);
    }

    /**
     * @param int $clientId
     * @param int $loanApplicationId
     * @return LoanApplicationClientEmail[]
     */
    public static function findAllByClientIdLoanApplicationId(int $clientId, int $loanApplicationId): array
    {
        return LoanApplicationClientEmail::find()
            ->byClientId($clientId)
            ->byLoanApplicationId($loanApplicationId)
            ->all();
    }
}