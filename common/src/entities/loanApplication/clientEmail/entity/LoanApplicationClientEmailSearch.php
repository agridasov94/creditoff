<?php

namespace common\src\entities\loanApplication\clientEmail\entity;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\src\entities\loanApplication\clientEmail\entity\LoanApplicationClientEmail;

/**
 * LoanApplicationClientEmailSearch represents the model behind the search form of `common\src\entities\loanApplication\clientEmail\entity\LoanApplicationClientEmail`.
 */
class LoanApplicationClientEmailSearch extends LoanApplicationClientEmail
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lace_id', 'lace_client_id', 'lace_la_id'], 'integer'],
            [['lace_email', 'lace_created_dt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LoanApplicationClientEmail::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'lace_id' => $this->lace_id,
            'lace_client_id' => $this->lace_client_id,
            'lace_la_id' => $this->lace_la_id,
            'lace_created_dt' => $this->lace_created_dt,
        ]);

        $query->andFilterWhere(['like', 'lace_email', $this->lace_email]);

        return $dataProvider;
    }
}
