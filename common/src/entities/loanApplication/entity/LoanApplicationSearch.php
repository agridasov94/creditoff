<?php

namespace common\src\entities\loanApplication\entity;

use common\src\entities\loanApplication\clientEmail\entity\LoanApplicationClientEmail;
use common\src\entities\loanApplication\clientPhone\entity\LoanApplicationClientPhone;
use common\src\entities\loanApplicationAccess\entity\LoanApplicationAccess;
use common\src\entities\loanApplicationAccess\entity\LoanApplicationAccessStatus;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * LoanApplicationSearch represents the model behind the search form of `common\src\entities\loanApplication\entity\LoanApplication`.
 */
class LoanApplicationSearch extends LoanApplication
{
    public $clientPhone;
    public $clientEmail;
    public $companyId;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['la_id', 'la_product_id', 'la_owner_id', 'la_status', 'la_loan_amount', 'la_loan_term', 'la_client_id',
                'la_updated_user_id', 'la_manual_distribution', 'la_distribution_iteration'], 'integer'],
            [['la_number', 'la_client_name', 'la_client_birth_date', 'la_client_idnp', 'la_updated_status_dt',
                'la_created_dt', 'la_updated_dt', 'clientPhone', 'clientEmail'], 'safe'],
            [['companyId'], 'integer'],
            [['companyId'], 'filter', 'filter' => 'intval'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LoanApplication::find();
        $query->joinWith('owner');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->companyId) {
            $query->andWhere(['partner_company_id' => $this->companyId]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'la_id' => $this->la_id,
            'la_product_id' => $this->la_product_id,
            'la_owner_id' => $this->la_owner_id,
            'la_status' => $this->la_status,
            'la_loan_amount' => $this->la_loan_amount,
            'la_loan_term' => $this->la_loan_term,
            'la_client_id' => $this->la_client_id,
            'date(la_client_birth_date)' => $this->la_client_birth_date,
            'date(la_updated_status_dt)' => $this->la_updated_status_dt,
            'date(la_created_dt)' => $this->la_created_dt,
            'date(la_updated_dt)' => $this->la_updated_dt,
            'la_updated_user_id' => $this->la_updated_user_id,
            'la_manual_distribution' => $this->la_manual_distribution,
            'la_distribution_iteration' => $this->la_distribution_iteration
        ]);

        $query->andFilterWhere(['like', 'la_number', $this->la_number])
            ->andFilterWhere(['like', 'la_client_name', $this->la_client_name])
            ->andFilterWhere(['like', 'la_client_idnp', $this->la_client_idnp]);

        return $dataProvider;
    }

    public function searchByStatus($params, int $status)
    {
        $query = LoanApplication::find();
        $query->andWhere(['la_status' => $status]);
        $query->leftJoin(LoanApplicationClientPhone::tableName(), 'lacp_la_id = la_id');
        $query->leftJoin(LoanApplicationClientEmail::tableName(), 'lace_la_id = la_id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'la_id' => $this->la_id,
            'la_product_id' => $this->la_product_id,
            'la_owner_id' => $this->la_owner_id,
            'la_loan_amount' => $this->la_loan_amount,
            'la_loan_term' => $this->la_loan_term,
            'la_client_id' => $this->la_client_id,
            'date(la_client_birth_date)' => $this->la_client_birth_date,
            'date(la_updated_status_dt)' => $this->la_updated_status_dt,
            'date(la_created_dt)' => $this->la_created_dt,
            'date(la_updated_dt)' => $this->la_updated_dt,
            'la_updated_user_id' => $this->la_updated_user_id,
        ]);

        $query->andFilterWhere(['like', 'la_number', $this->la_number])
            ->andFilterWhere(['like', 'la_client_name', $this->la_client_name])
            ->andFilterWhere(['like', 'la_client_idnp', $this->la_client_idnp])
            ->andFilterWhere(['like', 'lacp_phone', $this->clientPhone])
            ->andFilterWhere(['like', 'lace_email', $this->clientEmail]);

        return $dataProvider;
    }

    public function findSentLoanApplicationForProcessing(int $userId, array $params)
    {
        $query = LoanApplication::find()
            ->hasSentStatus()
            ->innerJoin(LoanApplicationAccess::tableName(), 'laa_la_id = la_id')
            ->andWhere(['laa_status' => LoanApplicationAccessStatus::PENDING])
            ->andWhere(['laa_user_id' => $userId]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['la_created_dt' => SORT_ASC]],
            'pagination' => false
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'la_product_id' => $this->la_product_id,
            'la_loan_amount' => $this->la_loan_amount,
            'la_loan_term' => $this->la_loan_term
        ]);

        $query->andFilterWhere(['like', 'la_number', $this->la_number]);

        return $dataProvider;
    }

    public function searchDeclined($params, int $userId)
    {
        $secondQuery = LoanApplication::find()
            ->leftJoin(LoanApplicationClientPhone::tableName(), 'lacp_la_id = la_id')
            ->leftJoin(LoanApplicationClientEmail::tableName(), 'lace_la_id = la_id')
            ->innerJoin(LoanApplicationAccess::tableName(), 'laa_la_id = la_id and laa_status = :status', [
                'status' => LoanApplicationAccessStatus::DECLINED
            ])
            ->andWhere(['laa_user_id' => $userId]);

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $secondQuery,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $secondQuery->andFilterWhere([
            'la_id' => $this->la_id,
            'la_product_id' => $this->la_product_id,
            'la_loan_amount' => $this->la_loan_amount,
            'la_loan_term' => $this->la_loan_term,
            'la_client_id' => $this->la_client_id,
            'date(la_client_birth_date)' => $this->la_client_birth_date,
            'date(la_updated_status_dt)' => $this->la_updated_status_dt,
            'date(la_created_dt)' => $this->la_created_dt,
            'date(la_updated_dt)' => $this->la_updated_dt,
            'la_updated_user_id' => $this->la_updated_user_id,
        ]);

        $secondQuery->andFilterWhere(['like', 'la_number', $this->la_number])
            ->andFilterWhere(['like', 'la_client_name', $this->la_client_name])
            ->andFilterWhere(['like', 'la_client_idnp', $this->la_client_idnp])
            ->andFilterWhere(['like', 'lacp_phone', $this->clientPhone])
            ->andFilterWhere(['like', 'lace_email', $this->clientEmail]);

        return $dataProvider;
    }
}
