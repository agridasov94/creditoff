<?php

namespace common\src\entities\loanApplication\entity;

/**
 * This is the ActiveQuery class for [[LoanApplication]].
 *
 * @see LoanApplication
 */
class Scope extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return LoanApplication[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return LoanApplication|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function byClientId(int $id): self
    {
        return $this->andWhere(['la_client_id' => $id]);
    }

    public function byProductId(int $id): self
    {
        return $this->andWhere(['la_product_id' => $id]);
    }

    public function hasSentStatus(): self
    {
        return $this->andWhere(['la_status' => LoanApplicationStatus::STATUS_SENT]);
    }

    public function hasSentOrCanceledStatus(): self
    {
        return $this->andWhere(['OR', ['la_status' => LoanApplicationStatus::STATUS_SENT], ['la_status' => LoanApplicationStatus::STATUS_CANCELED]]);
    }

    public function isNotManualDistribution(): self
    {
        return $this->andWhere(['la_manual_distribution' => 0]);
    }

    public function byNumber(string $number): self
    {
        return $this->andWhere(['la_number' => $number]);
    }

    public function byOwner(int $id): self
    {
        return $this->andWhere(['la_owner_id' => $id]);
    }
}
