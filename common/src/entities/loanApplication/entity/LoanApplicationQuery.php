<?php

namespace common\src\entities\loanApplication\entity;

use common\src\entities\loanApplicationAccess\entity\LoanApplicationAccess;
use common\src\entities\loanApplicationAccess\entity\LoanApplicationAccessQuery;
use common\src\entities\loanApplicationAccess\entity\LoanApplicationAccessStatus;
use yii\db\Query;

class LoanApplicationQuery
{
    /**
     * @param int $id
     * @return LoanApplication[]
     */
    public static function findByClientId(int $id): array
    {
        return LoanApplication::find()->byClientId($id)->orderBy(['la_created_dt' => SORT_DESC])->all();
    }

    public static function findByNumber(string $number): ?LoanApplication
    {
        return LoanApplication::find()->byNumber($number)->one();
    }

    public static function findByOwnerAndNumber(string $number, int $ownerId): ?LoanApplication
    {
        return LoanApplication::find()->byNumber($number)->byOwner($ownerId)->one();
    }

    public static function findAllSentByProductIdQuery(int $id): Scope
    {
        return LoanApplication::find()->byProductId($id)->isNotManualDistribution()->hasSentStatus();
    }

    /**
     * @param int $id
     * @return LoanApplication[]
     */
    public static function findAllSentExceptDeclinedByProductId(int $id): array
    {
        $subQuery = LoanApplicationAccessQuery::findDeclinedLoanApplicationsQuery();
        return self::findAllSentByProductIdQuery($id)
            ->leftJoin(['laa' => $subQuery], 'laa.laa_la_id = loan_application.la_id')
            ->andWhere(['laa_la_id' => null])
            ->all();
    }

    public static function countAllSentExceptDeclinedByProductId(int $id): int
    {
        $subQuery = LoanApplicationAccessQuery::findDeclinedLoanApplicationsQuery();
        return self::findAllSentByProductIdQuery($id)
            ->leftJoin(['laa' => $subQuery], 'laa.laa_la_id = loan_application.la_id')
            ->andWhere(['laa_la_id' => null])
            ->count();
    }

    /**
     * @param int $productId
     * @param int $limit
     * @return LoanApplication[]
     */
    public static function findAvailableForUser(int $productId, ?int $limit): array
    {
        return LoanApplication::find()
            ->hasSentStatus()
            ->byProductId($productId)
            ->isNotManualDistribution()
            ->limit($limit)
            ->all();
    }

    /**
     * @param int $productId
     * @param ?int $limit
     * @return int[]
     */
    public static function findAvailableIdsForUser(int $productId, ?int $limit): array
    {
        $subQuery = LoanApplicationAccessQuery::findDeclinedLoanApplicationsQuery();
        return LoanApplication::find()
            ->select(['la_id'])
            ->hasSentOrCanceledStatus()
            ->byProductId($productId)
            ->isNotManualDistribution()
            ->leftJoin(['laa' => $subQuery], 'laa.laa_la_id = loan_application.la_id')
            ->andWhere(['laa_la_id' => null])
            ->limit($limit)
            ->asArray()
            ->indexBy('la_id')
            ->column();
    }

    public static function getLastByClientId(int $id): ?LoanApplication
    {
        return LoanApplication::find()->where(['la_client_id' => $id])->orderBy(['la_id' => SORT_DESC])->one();
    }

    public static function isApplicationWasDeclined(int $id): bool
    {
        return LoanApplicationAccessQuery::findDeclinedLoanApplicationsQuery()
            ->andWhere(['laa_la_id' => $id])->exists();
    }
}