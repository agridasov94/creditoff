<?php

namespace common\src\entities\loanApplication\entity;

use yii\helpers\Html;

class LoanApplicationStatus
{
    const STATUS_SENT = 1;
    const STATUS_IN_WORK = 2;
    const STATUS_APPROVED = 3;
    const STATUS_DECLINED = 4;
    const STATUS_PAID = 5;
    const STATUS_REVOKED = 6;
    const STATUS_CANCELED = 7;

    public static function getStatusNameList(): array
    {
        return [
            self::STATUS_SENT => \Yii::t('loan_application', 'Application Sent'),
            self::STATUS_IN_WORK => \Yii::t('loan_application', 'Application In Work'),
            self::STATUS_APPROVED => \Yii::t('loan_application', 'Application Approved'),
            self::STATUS_DECLINED => \Yii::t('loan_application', 'Application Declined'),
            self::STATUS_PAID => \Yii::t('loan_application', 'Application Paid'),
            self::STATUS_REVOKED => \Yii::t('loan_application', 'Application Revoked'),
            self::STATUS_CANCELED => \Yii::t('loan_application', 'Application Canceled')
        ];
    }

    public static function getStatusLabelList(): array
    {
        return [
            self::STATUS_SENT => 'badge badge-primary',
            self::STATUS_IN_WORK => 'badge badge-warning',
            self::STATUS_APPROVED => 'badge badge-success',
            self::STATUS_DECLINED => 'badge badge-danger',
            self::STATUS_PAID => 'badge badge-info',
            self::STATUS_REVOKED => 'badge badge-dark',
            self::STATUS_CANCELED => 'badge badge-danger'
        ];
    }

    public static function getName(int $id): string
    {
        return self::getStatusNameList()[$id] ?? 'Unknown status';
    }

    public static function getLabel(int $id): string
    {
        return self::getStatusLabelList()[$id] ?? '';
    }

    public static function getStatusLabel(int $status): string
    {
        return Html::tag('span', self::getName($status), [
            'class' => self::getLabel($status)
        ]);
    }
}