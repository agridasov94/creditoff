<?php

namespace common\src\entities\loanApplication\entity;

use common\components\validators\IsArrayValidator;
use common\src\entities\loanApplication\companies\entity\LoanApplicationCompanies;
use common\src\entities\loanApplicationAccess\entity\LoanApplicationAccessQuery;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

class LoanApplicationMultipleSearch extends LoanApplication
{
    public $clientPhone;
    public $clientEmail;
    public $companyId;
    public $companiesSelectedByClient;
    public $wasDeclined;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['la_id', 'la_number', 'la_product_id', 'la_owner_id', 'la_status', 'companiesSelectedByClient'], IsArrayValidator::class],
            [['la_id', 'la_product_id', 'la_owner_id', 'la_status', 'companyId',
                'la_loan_amount', 'la_loan_term', 'la_client_id', 'la_updated_user_id', 'companiesSelectedByClient'], 'each', 'rule' => ['integer']],
            [['la_number', 'la_client_name', 'la_client_idnp'], 'each', 'rule' => ['string']],

            [['la_manual_distribution', 'wasDeclined'], 'integer'],
            [['wasDeclined'], 'filter', 'filter' => 'intval', 'skipOnEmpty' => true],
            [['la_client_birth_date', 'la_updated_status_dt',
                'la_created_dt', 'la_updated_dt', 'clientPhone', 'clientEmail'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LoanApplication::find();
        $query->joinWith(['owner']);
//        $query->select('*');
//        $query->addSelect(['wasDeclined' => 'declined.laa_la_id']);
        $query->leftJoin(['declined' => LoanApplicationAccessQuery::findDeclinedLoanApplicationsQuery()], 'laa_la_id = la_id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['la_created_dt' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->companyId) {
            $query->andWhere(['partner_company_id' => $this->companyId]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'la_id' => $this->la_id,
            'la_product_id' => $this->la_product_id,
            'la_owner_id' => $this->la_owner_id,
            'la_status' => $this->la_status,
            'la_loan_amount' => $this->la_loan_amount,
            'la_loan_term' => $this->la_loan_term,
            'la_client_id' => $this->la_client_id,
            'date(la_client_birth_date)' => $this->la_client_birth_date,
            'date(la_updated_status_dt)' => $this->la_updated_status_dt,
            'date(la_created_dt)' => $this->la_created_dt,
            'date(la_updated_dt)' => $this->la_updated_dt,
            'la_updated_user_id' => $this->la_updated_user_id,
            'la_manual_distribution' => $this->la_manual_distribution,
        ]);

        if ($this->la_number) {
            foreach ($this->la_number as $key => $number) {
                if ($key > 0) {
                    $query->orFilterWhere(['like', 'la_number', $number]);
                } else {
                    $query->andFilterWhere(['like', 'la_number', $number]);
                }
            }
        }
        if ($this->la_client_name) {
            foreach ($this->la_client_name as $key => $name) {
                if ($key > 0) {
                    $query->orFilterWhere(['like', 'la_client_name', $name])->orFilterWhere(['like', 'la_client_name', $name]);
                } else {
                    $query->andFilterWhere(['like', 'la_client_name', $name])->orFilterWhere(['like', 'la_client_name', $name]);
                }
            }
        }

        if ($this->companiesSelectedByClient) {
            $query->innerJoin(LoanApplicationCompanies::tableName(), 'lac_la_id = la_id');
            $query->andWhere(['lac_partner_company_id' => $this->companiesSelectedByClient]);
        }

        if ($this->wasDeclined === 1) {
            $query->andWhere(['IS NOT', 'declined.laa_la_id', null]);
        } else if ($this->wasDeclined === 0) {
            $query->andWhere(['declined.laa_la_id' => null]);
        }

        $query
            ->andFilterWhere(['like', 'la_client_idnp', $this->la_client_idnp]);

        return $dataProvider;
    }
}
