<?php

namespace common\src\entities\loanApplication\entity;

use common\src\entities\client\entity\Client;
use common\src\entities\loanApplication\clientEmail\entity\LoanApplicationClientEmail;
use common\src\entities\loanApplication\clientEmployedData\entity\LoanApplicationClientEmployedData;
use common\src\entities\loanApplication\clientPassportData\entity\LoanApplicationClientPassportData;
use common\src\entities\loanApplication\clientPhone\entity\LoanApplicationClientPhone;
use common\src\entities\loanApplication\companies\entity\LoanApplicationCompanies;
use common\src\entities\loanApplication\log\entity\LoanApplicationLog;
use common\src\entities\product\entity\Product;
use common\src\entities\users\entity\Users;
use Yii;
use yii\base\Event;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Html;

/**
 * This is the model class for table "loan_application".
 *
 * @property int $la_id
 * @property string $la_number
 * @property int $la_product_id
 * @property int|null $la_owner_id
 * @property int $la_status
 * @property int $la_loan_amount
 * @property int $la_loan_term
 * @property int|null $la_client_id
 * @property string $la_client_name
 * @property string $la_client_birth_date
 * @property string $la_client_idnp
 * @property string|null $la_updated_status_dt
 * @property string|null $la_created_dt
 * @property string|null $la_updated_dt
 * @property int|null $la_updated_user_id
 *
 * @property Client $client
 * @property Users $owner
 * @property Product $product
 * @property Users $updatedUser
 * @property LoanApplicationLog[] $loanApplicationLogs
 * @property LoanApplicationCompanies[] $loanApplicationCompanies
 * @property LoanApplicationClientEmail[] $clientEmails
 * @property LoanApplicationClientPhone[] $clientPhones
 * @property LoanApplicationClientPassportData $clientPassportData
 * @property LoanApplicationClientEmployedData[] $clientEmployedData
 * @property bool $la_client_gender [tinyint(1)]
 * @property bool $la_client_civil_status [tinyint(1)]
 * @property bool $la_client_social_status [tinyint(1)]
 * @property bool $la_client_own_property [tinyint(1)]
 * @property string $la_client_children [json]
 * @property bool $la_manual_distribution [tinyint(1)]
 * @property int $la_distribution_iteration [smallint]
 */
class LoanApplication extends \yii\db\ActiveRecord
{
    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['la_created_dt'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['la_updated_dt'],
                ],
                'value' => date('Y-m-d H:i:s') //new Expression('NOW()'),
            ],
            'user' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => null,
                'updatedByAttribute' => 'la_updated_user_id',
            ],
            'date' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['la_client_birth_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['la_client_birth_date'],
                    ActiveRecord::EVENT_BEFORE_VALIDATE => ['la_client_birth_date'],
                ],
                'value' => function (Event $event) {
                    if ($this->la_client_birth_date) {
                        return (new \DateTime($this->la_client_birth_date))->format('Y-m-d');
                    }
                    return null;
                }
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'loan_application';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['la_number', 'la_product_id', 'la_status', 'la_loan_amount', 'la_loan_term', 'la_client_name', 'la_client_birth_date', 'la_client_idnp'], 'required'],
            [['la_product_id', 'la_owner_id', 'la_status', 'la_loan_amount', 'la_loan_term', 'la_client_id', 'la_updated_user_id', 'la_distribution_iteration'], 'integer'],
            [['la_client_birth_date', 'la_updated_status_dt', 'la_created_dt', 'la_updated_dt'], 'safe'],
            [['la_number'], 'string', 'max' => 10],
            [['la_client_birth_date'], 'date', 'format' => 'php:Y-m-d'],
            [['la_client_name'], 'string', 'max' => 100],
            [['la_client_idnp'], 'string', 'max' => 13],
            [['la_number'], 'unique'],
            [['la_status'], 'in', 'range' => array_keys(LoanApplicationStatus::getStatusNameList())],
            [['la_client_gender', 'la_client_civil_status', 'la_client_social_status'], 'integer'],
            [['la_client_own_property', 'la_manual_distribution'], 'boolean'],
            [['la_manual_distribution'], 'default', 'value' => 0],
            [['la_client_children'], 'safe'],
            [['la_client_gender'], 'in', 'range' => array_keys(Client::getGenderListName())],
            [['la_client_civil_status'], 'in', 'range' => array_keys(Client::getCivilStatusListName())],
            [['la_client_social_status'], 'in', 'range' => array_keys(Client::getSocialStatusListName())],
            [['la_client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::class, 'targetAttribute' => ['la_client_id' => 'c_id']],
            [['la_owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['la_owner_id' => 'id']],
            [['la_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::class, 'targetAttribute' => ['la_product_id' => 'p_id']],
            [['la_updated_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['la_updated_user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'la_id' => Yii::t('loan_application', 'ID'),
            'la_number' => Yii::t('loan_application', 'Number'),
            'la_product_id' => Yii::t('loan_application', 'Product'),
            'la_owner_id' => Yii::t('loan_application', 'Owner'),
            'la_status' => Yii::t('loan_application', 'Status'),
            'la_loan_amount' => Yii::t('loan_application', 'Loan Amount'),
            'la_loan_term' => Yii::t('loan_application', 'Loan Term'),
            'la_client_id' => Yii::t('loan_application', 'Client'),
            'la_client_name' => Yii::t('loan_application', 'Client Name'),
            'la_client_birth_date' => Yii::t('loan_application', 'Client Birth Date'),
            'la_client_idnp' => Yii::t('loan_application', 'Client Idnp'),
            'la_updated_status_dt' => Yii::t('loan_application', 'Updated Status Dt'),
            'la_created_dt' => Yii::t('loan_application', 'Created Dt'),
            'la_updated_dt' => Yii::t('loan_application', 'Updated Dt'),
            'la_updated_user_id' => Yii::t('loan_application', 'Updated User ID'),
            'la_client_gender' => Yii::t('loan_application', 'Client Gender'),
            'la_client_civil_status' => Yii::t('loan_application', 'Client Civil Status'),
            'la_client_social_status' => Yii::t('loan_application', 'Client Social Status'),
            'la_client_own_property' => Yii::t('loan_application', 'Client Has Own Property'),
            'la_client_children' => Yii::t('loan_application', 'Client Children'),
            'la_manual_distribution' => Yii::t('loan_application', 'Is Manual Distribution'),
            'la_distribution_iteration' => Yii::t('loan_application', 'Distribution iteration'),
        ];
    }

    public function beforeSave($insert)
    {
        if (array_key_exists('la_status', $this->dirtyAttributes)) {
            $this->la_updated_status_dt = date('Y-m-d H:i:s');
        }
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    /**
     * Gets query for [[LaClient]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\client\entity\Scope
     */
    public function getClient()
    {
        return $this->hasOne(Client::class, ['c_id' => 'la_client_id']);
    }

    /**
     * Gets query for [[LaOwner]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\users\entity\Scope
     */
    public function getOwner()
    {
        return $this->hasOne(Users::class, ['id' => 'la_owner_id']);
    }

    /**
     * Gets query for [[LaProduct]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\product\entity\Scope
     */
    public function getProduct()
    {
        return $this->hasOne(Product::class, ['p_id' => 'la_product_id']);
    }

    /**
     * Gets query for [[LaUpdatedUser]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\users\entity\Scope
     */
    public function getUpdatedUser()
    {
        return $this->hasOne(Users::class, ['id' => 'la_updated_user_id']);
    }

    /**
     * Gets query for [[LoanApplicationLogs]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\loanApplication\log\entity\Scope
     */
    public function getLoanApplicationLogs()
    {
        return $this->hasMany(LoanApplicationLog::class, ['lal_loan_application_id' => 'la_id']);
    }

    /**
     * Gets query for [[LoanApplicationLogs]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\loanApplication\companies\entity\Scope
     */
    public function getLoanApplicationCompanies()
    {
        return $this->hasMany(LoanApplicationCompanies::class, ['lac_la_id' => 'la_id']);
    }

    /**
     * {@inheritdoc}
     * @return Scope the active query used by this AR class.
     */
    public static function find()
    {
        return new Scope(get_called_class());
    }

    public function setStatus(int $id): void
    {
        $this->la_status = $id;
    }

    public function setOwner(int $id)
    {
        $this->la_owner_id = $id;
    }

    public function generateUid(): void
    {
        $bytes = random_bytes(5);
        $this->la_number = bin2hex($bytes);
    }

    public function getClientEmails()
    {
        return $this->hasMany(LoanApplicationClientEmail::class, ['lace_la_id' => 'la_id']);
    }

    public function getClientPhones()
    {
        return $this->hasMany(LoanApplicationClientPhone::class, ['lacp_la_id' => 'la_id']);
    }

    public function getClientPassportData()
    {
        return $this->hasOne(LoanApplicationClientPassportData::class, ['lacpd_la_id' => 'la_id']);
    }

    public function getClientEmployedData()
    {
        return $this->hasMany(LoanApplicationClientEmployedData::class, ['laced_la_id' => 'la_id']);
    }

    public function isNotInWorkStatus(): bool
    {
        return $this->la_status !== LoanApplicationStatus::STATUS_IN_WORK;
    }

    public function isSentStatus(): bool
    {
        return $this->la_status === LoanApplicationStatus::STATUS_SENT;
    }

    public function isInWorkStatus(): bool
    {
        return $this->la_status === LoanApplicationStatus::STATUS_IN_WORK;
    }

    public function isNotApproved(): bool
    {
        return $this->la_status !== LoanApplicationStatus::STATUS_APPROVED;
    }

    public function isApproved(): bool
    {
        return $this->la_status === LoanApplicationStatus::STATUS_APPROVED;
    }

    public function isSent(): bool
    {
        return $this->la_status === LoanApplicationStatus::STATUS_SENT;
    }

    public function setManualDistribution(): void
    {
        $this->la_manual_distribution = true;
    }

    public function increaseDistributionIteration(): void
    {
        ++$this->la_distribution_iteration;
    }

    public function isDeclined(): bool
    {
        return $this->la_status === LoanApplicationStatus::STATUS_DECLINED;
    }

    public function flushOwner(): void
    {
        $this->la_owner_id = null;
    }

    public function isCanceled(): bool
    {
        return $this->la_status === LoanApplicationStatus::STATUS_CANCELED;
    }

    public function canCancel(): bool
    {
        return $this->isSent() || $this->isInWorkStatus() || $this->isApproved();
    }

    public function cancel(): void
    {
        $this->la_status = LoanApplicationStatus::STATUS_CANCELED;
    }
}
