<?php

namespace common\src\entities\loanApplication\entity;

use common\src\exceptions\NotFoundException;

class LoanApplicationRepository
{
    public function save(LoanApplication $model): int
    {
        if (!$model->save()) {
            throw new \RuntimeException($model->getErrorSummary(true)[0]);
        }
        return $model->la_id;
    }

    public function findByNumber(string $number): LoanApplication
    {
        if ($loanApplication = LoanApplicationQuery::findByNumber($number)) {
            return $loanApplication;
        }
        throw new NotFoundException('Loan Application not found by number: ' . $number);
    }
}