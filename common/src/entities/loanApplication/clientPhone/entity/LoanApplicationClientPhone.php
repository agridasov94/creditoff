<?php

namespace common\src\entities\loanApplication\clientPhone\entity;

use common\src\entities\client\entity\Client;
use common\src\entities\client\entity\ClientQuery;
use common\src\entities\loanApplication\entity\LoanApplication;
use common\src\entities\loanApplication\entity\LoanApplicationQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "loan_application_client_phone".
 *
 * @property int $lacp_id
 * @property int|null $lacp_client_id
 * @property int $lacp_la_id
 * @property string $lacp_phone
 * @property string|null $lacp_created_dt
 *
 * @property Client $client
 * @property LoanApplication $loanApplication
 */
class LoanApplicationClientPhone extends \yii\db\ActiveRecord
{
    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['lacp_created_dt'],
                ],
                'value' => date('Y-m-d H:i:s') //new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'loan_application_client_phone';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lacp_client_id', 'lacp_la_id'], 'integer'],
            [['lacp_la_id', 'lacp_phone'], 'required'],
            [['lacp_created_dt'], 'safe'],
            [['lacp_phone'], 'string', 'max' => 30],
            [['lacp_client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::class, 'targetAttribute' => ['lacp_client_id' => 'c_id']],
            [['lacp_la_id'], 'exist', 'skipOnError' => true, 'targetClass' => LoanApplication::class, 'targetAttribute' => ['lacp_la_id' => 'la_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'lacp_id' => Yii::t('loan_application', 'ID'),
            'lacp_client_id' => Yii::t('loan_application', 'Client ID'),
            'lacp_la_id' => Yii::t('loan_application', 'Loan Application ID'),
            'lacp_phone' => Yii::t('loan_application', 'Phone'),
            'lacp_created_dt' => Yii::t('loan_application', 'Created Dt'),
        ];
    }

    /**
     * Gets query for [[LacpClient]].
     *
     * @return \yii\db\ActiveQuery|ClientQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::class, ['c_id' => 'lacp_client_id']);
    }

    /**
     * Gets query for [[LacpLa]].
     *
     * @return \yii\db\ActiveQuery|LoanApplicationQuery
     */
    public function getLoanApplication()
    {
        return $this->hasOne(LoanApplication::class, ['la_id' => 'lacp_la_id']);
    }

    /**
     * {@inheritdoc}
     * @return Scope the active query used by this AR class.
     */
    public static function find()
    {
        return new Scope(get_called_class());
    }
}
