<?php

namespace common\src\entities\loanApplication\clientPhone\entity;

class LoanApplicationClientPhoneQuery
{
    public static function removeByLoanApplicationId(int $id): void
    {
        LoanApplicationClientPhone::deleteAll(['lacp_la_id' => $id]);
    }

    /**
     * @param int $clientId
     * @param int $loanApplicationId
     * @return LoanApplicationClientPhone[]
     */
    public static function findAllByClientIdLoanApplicationId(int $clientId, int $loanApplicationId): array
    {
        return LoanApplicationClientPhone::find()
            ->byClientId($clientId)
            ->byLoanApplicationId($loanApplicationId)
            ->all();
    }
}