<?php

namespace common\src\entities\loanApplication\clientPhone\entity;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\src\entities\loanApplication\clientPhone\entity\LoanApplicationClientPhone;

/**
 * LoanApplicationClientPhoneSearch represents the model behind the search form of `common\src\entities\loanApplication\clientPhone\entity\LoanApplicationClientPhone`.
 */
class LoanApplicationClientPhoneSearch extends LoanApplicationClientPhone
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lacp_id', 'lacp_client_id', 'lacp_la_id'], 'integer'],
            [['lacp_phone', 'lacp_created_dt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LoanApplicationClientPhone::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'lacp_id' => $this->lacp_id,
            'lacp_client_id' => $this->lacp_client_id,
            'lacp_la_id' => $this->lacp_la_id,
            'lacp_created_dt' => $this->lacp_created_dt,
        ]);

        $query->andFilterWhere(['like', 'lacp_phone', $this->lacp_phone]);

        return $dataProvider;
    }
}
