<?php

namespace common\src\entities\loanApplication\clientPhone\entity;

/**
 * This is the ActiveQuery class for [[LoanApplicationClientPhone]].
 *
 * @see LoanApplicationClientPhone
 */
class Scope extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return LoanApplicationClientPhone[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return LoanApplicationClientPhone|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function byClientId(int $id): self
    {
        return $this->andWhere(['lacp_client_id' => $id]);
    }

    public function byLoanApplicationId(int $id): self
    {
        return $this->andWhere(['lacp_la_id' => $id]);
    }
}
