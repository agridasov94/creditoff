<?php

namespace common\src\entities\loanApplication\clientPhone\entity;

class LoanApplicationClientPhoneRepository
{
    public function save(LoanApplicationClientPhone $model): int
    {
        if (!$model->save()) {
            throw new \RuntimeException($model->getErrorSummary(true)[0]);
        }
        return $model->lacp_id;
    }
}