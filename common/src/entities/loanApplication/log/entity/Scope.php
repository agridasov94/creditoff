<?php

namespace common\src\entities\loanApplication\log\entity;

/**
 * This is the ActiveQuery class for [[LoanApplicationLog]].
 *
 * @see LoanApplicationLog
 */
class Scope extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return LoanApplicationLog[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return LoanApplicationLog|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
