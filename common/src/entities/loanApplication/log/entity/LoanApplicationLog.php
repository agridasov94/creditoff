<?php

namespace common\src\entities\loanApplication\log\entity;

use common\src\entities\loanApplication\entity\LoanApplication;
use common\src\entities\users\entity\Users;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "loan_application_log".
 *
 * @property int $lal_id
 * @property int $lal_loan_application_id
 * @property int|null $lal_owner_id
 * @property int|null $lal_status_before
 * @property int|null $lal_status_after
 * @property string|null $lal_start_dt
 * @property string|null $lal_end_dt
 *
 * @property LoanApplication $loanApplication
 * @property Users $owner
 */
class LoanApplicationLog extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'loan_application_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lal_loan_application_id'], 'required'],
            [['lal_loan_application_id', 'lal_owner_id', 'lal_status_before', 'lal_status_after'], 'integer'],
            [['lal_start_dt', 'lal_end_dt'], 'safe'],
            [['lal_loan_application_id'], 'exist', 'skipOnError' => true, 'targetClass' => LoanApplication::class, 'targetAttribute' => ['lal_loan_application_id' => 'la_id']],
            [['lal_owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['lal_owner_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'lal_id' => Yii::t('loan_application', 'ID'),
            'lal_loan_application_id' => Yii::t('loan_application', 'Loan Application ID'),
            'lal_owner_id' => Yii::t('loan_application', 'Owner'),
            'lal_status_before' => Yii::t('loan_application', 'Status Before'),
            'lal_status_after' => Yii::t('loan_application', 'Status After'),
            'lal_start_dt' => Yii::t('loan_application', 'Start Dt'),
            'lal_end_dt' => Yii::t('loan_application', 'End Dt'),
        ];
    }

    /**
     * Gets query for [[LalLoanApplication]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\loanApplication\entity\Scope
     */
    public function getLoanApplication()
    {
        return $this->hasOne(LoanApplication::class, ['la_id' => 'lal_loan_application_id']);
    }

    /**
     * Gets query for [[LalOwner]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\users\entity\Scope
     */
    public function getOwner()
    {
        return $this->hasOne(Users::class, ['id' => 'lal_owner_id']);
    }

    /**
     * {@inheritdoc}
     * @return Scope the active query used by this AR class.
     */
    public static function find()
    {
        return new Scope(get_called_class());
    }
}
