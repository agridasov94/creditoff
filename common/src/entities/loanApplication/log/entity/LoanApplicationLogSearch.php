<?php

namespace common\src\entities\loanApplication\log\entity;

use common\src\entities\loanApplication\entity\LoanApplication;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\src\entities\loanApplication\log\entity\LoanApplicationLog;

/**
 * LoanApplicationLogSearch represents the model behind the search form of `common\src\entities\loanApplication\log\entity\LoanApplicationLog`.
 */
class LoanApplicationLogSearch extends LoanApplicationLog
{
    public $number;
    public $companyId;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lal_id', 'lal_loan_application_id', 'lal_owner_id', 'lal_status_before', 'lal_status_after'], 'integer'],
            [['lal_start_dt', 'lal_end_dt', 'number'], 'safe'],
            [['companyId'], 'integer'],
            [['companyId'], 'filter', 'filter' => 'intval'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LoanApplicationLog::find()->select('*');
        $query->innerJoin(LoanApplication::tableName(), 'lal_loan_application_id = la_id');
        $query->joinWith('owner');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['lal_id' => SORT_DESC],
                'attributes' => [
                    'lal_id',
                    'number',
                    'lal_loan_application_id',
                    'lal_owner_id',
                    'lal_status_after',
                    'lal_status_before',
                    'lal_start_dt',
                    'lal_end_dt'
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->companyId) {
            $query->andWhere(['partner_company_id' => $this->companyId]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'lal_id' => $this->lal_id,
            'lal_loan_application_id' => $this->lal_loan_application_id,
            'lal_owner_id' => $this->lal_owner_id,
            'lal_status_before' => $this->lal_status_before,
            'lal_status_after' => $this->lal_status_after,
            'lal_start_dt' => $this->lal_start_dt,
            'lal_end_dt' => $this->lal_end_dt,
        ]);

        $query->andFilterWhere(['like', 'la_number', $this->number]);

        return $dataProvider;
    }
}
