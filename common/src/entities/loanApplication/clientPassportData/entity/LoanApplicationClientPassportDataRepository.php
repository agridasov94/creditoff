<?php

namespace common\src\entities\loanApplication\clientPassportData\entity;

class LoanApplicationClientPassportDataRepository
{
    public function save(LoanApplicationClientPassportData $model): int
    {
        if (!$model->save()) {
            throw new \RuntimeException($model->getErrorSummary(true)[0]);
        }
        return $model->lacpd_id;
    }
}