<?php

namespace common\src\entities\loanApplication\clientPassportData\entity;

use common\src\entities\client\entity\Client;
use common\src\entities\client\entity\ClientQuery;
use common\src\entities\loanApplication\entity\LoanApplication;
use common\src\entities\loanApplication\entity\LoanApplicationQuery;
use Yii;
use yii\base\Event;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "loan_application_client_passport_data".
 *
 * @property int $lacpd_id
 * @property int|null $lacpd_client_id
 * @property int|null $lacpd_la_id
 * @property int|null $lacpd_type
 * @property string|null $lacpd_series
 * @property string|null $lacpd_number
 * @property string|null $lacpd_issued_by
 * @property string|null $lacpd_issue_date
 * @property string|null $lacpd_issue_department_code
 * @property string|null $lacpd_expiration_date
 * @property string|null $lacpd_registered_address
 * @property string|null $lacpd_residence_address
 * @property string|null $lacpd_created_dt
 *
 * @property Client $client
 * @property LoanApplication $loanApplication
 */
class LoanApplicationClientPassportData extends \yii\db\ActiveRecord
{
    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['lacpd_created_dt'],
                ],
                'value' => date('Y-m-d H:i:s') //new Expression('NOW()'),
            ],
            'date' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['lacpd_issue_date'],
                ],
                'value' => function (Event $event) {
                    if ($this->lacpd_issue_date) {
                        $birthDate = new \DateTime($this->lacpd_issue_date);
                        return $birthDate->format('Y-m-d');
                    }
                    return null;
                }
            ],
            'expirationDate' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['lacpd_expiration_date'],
                ],
                'value' => function (Event $event) {
                    if ($this->lacpd_expiration_date) {
                        $birthDate = new \DateTime($this->lacpd_expiration_date);
                        return $birthDate->format('Y-m-d');
                    }
                    return null;
                }
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'loan_application_client_passport_data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lacpd_client_id', 'lacpd_la_id', 'lacpd_type'], 'integer'],
            [['lacpd_issue_date', 'lacpd_expiration_date', 'lacpd_created_dt'], 'safe'],
            [['lacpd_series', 'lacpd_number', 'lacpd_registered_address', 'lacpd_residence_address'], 'string', 'max' => 100],
            [['lacpd_issued_by', 'lacpd_issue_department_code'], 'string', 'max' => 50],
            [['lacpd_client_id', 'lacpd_la_id'], 'unique', 'targetAttribute' => ['lacpd_client_id', 'lacpd_la_id']],
            [['lacpd_client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::class, 'targetAttribute' => ['lacpd_client_id' => 'c_id']],
            [['lacpd_la_id'], 'exist', 'skipOnError' => true, 'targetClass' => LoanApplication::class, 'targetAttribute' => ['lacpd_la_id' => 'la_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'lacpd_id' => Yii::t('loan_application', 'ID'),
            'lacpd_client_id' => Yii::t('loan_application', 'Client'),
            'lacpd_la_id' => Yii::t('loan_application', 'Loan Application ID'),
            'lacpd_type' => Yii::t('loan_application', 'Type'),
            'lacpd_series' => Yii::t('loan_application', 'Series'),
            'lacpd_number' => Yii::t('loan_application', 'Number'),
            'lacpd_issued_by' => Yii::t('loan_application', 'Issued By'),
            'lacpd_issue_date' => Yii::t('loan_application', 'Issue Date'),
            'lacpd_issue_department_code' => Yii::t('loan_application', 'Issue Department Code'),
            'lacpd_expiration_date' => Yii::t('loan_application', 'Expiration Date'),
            'lacpd_registered_address' => Yii::t('loan_application', 'Registered Address'),
            'lacpd_residence_address' => Yii::t('loan_application', 'Residence Address'),
            'lacpd_created_dt' => Yii::t('loan_application', 'Created Dt'),
        ];
    }

    /**
     * Gets query for [[LacpdClient]].
     *
     * @return \yii\db\ActiveQuery|ClientQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::class, ['c_id' => 'lacpd_client_id']);
    }

    /**
     * Gets query for [[LacpdLa]].
     *
     * @return \yii\db\ActiveQuery|LoanApplicationQuery
     */
    public function getLoanApplication()
    {
        return $this->hasOne(LoanApplication::class, ['la_id' => 'lacpd_la_id']);
    }

    /**
     * {@inheritdoc}
     * @return Scope the active query used by this AR class.
     */
    public static function find()
    {
        return new Scope(get_called_class());
    }
}
