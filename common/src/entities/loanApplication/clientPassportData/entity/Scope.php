<?php

namespace common\src\entities\loanApplication\clientPassportData\entity;

/**
 * This is the ActiveQuery class for [[LoanApplicationClientPassportData]].
 *
 * @see LoanApplicationClientPassportData
 */
class Scope extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return LoanApplicationClientPassportData[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return LoanApplicationClientPassportData|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
