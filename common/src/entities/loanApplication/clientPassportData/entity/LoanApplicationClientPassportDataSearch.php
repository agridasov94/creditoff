<?php

namespace common\src\entities\loanApplication\clientPassportData\entity;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\src\entities\loanApplication\clientPassportData\entity\LoanApplicationClientPassportData;

/**
 * LoanApplicationClientPassportDataSearch represents the model behind the search form of `common\src\entities\loanApplication\clientPassportData\entity\LoanApplicationClientPassportData`.
 */
class LoanApplicationClientPassportDataSearch extends LoanApplicationClientPassportData
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lacpd_id', 'lacpd_client_id', 'lacpd_la_id', 'lacpd_type'], 'integer'],
            [['lacpd_series', 'lacpd_number', 'lacpd_issued_by', 'lacpd_issue_date', 'lacpd_issue_department_code', 'lacpd_expiration_date', 'lacpd_registered_address', 'lacpd_residence_address', 'lacpd_created_dt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LoanApplicationClientPassportData::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'lacpd_id' => $this->lacpd_id,
            'lacpd_client_id' => $this->lacpd_client_id,
            'lacpd_la_id' => $this->lacpd_la_id,
            'lacpd_type' => $this->lacpd_type,
            'lacpd_issue_date' => $this->lacpd_issue_date,
            'lacpd_expiration_date' => $this->lacpd_expiration_date,
            'lacpd_created_dt' => $this->lacpd_created_dt,
        ]);

        $query->andFilterWhere(['like', 'lacpd_series', $this->lacpd_series])
            ->andFilterWhere(['like', 'lacpd_number', $this->lacpd_number])
            ->andFilterWhere(['like', 'lacpd_issued_by', $this->lacpd_issued_by])
            ->andFilterWhere(['like', 'lacpd_issue_department_code', $this->lacpd_issue_department_code])
            ->andFilterWhere(['like', 'lacpd_registered_address', $this->lacpd_registered_address])
            ->andFilterWhere(['like', 'lacpd_residence_address', $this->lacpd_residence_address]);

        return $dataProvider;
    }
}
