<?php

namespace common\src\entities\userProductAccessLog\entity;

/**
 * This is the ActiveQuery class for [[UserProductAccessLog]].
 *
 * @see UserProductAccessLog
 */
class Scope extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return UserProductAccessLog[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return UserProductAccessLog|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
