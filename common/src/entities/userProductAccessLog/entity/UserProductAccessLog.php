<?php

namespace common\src\entities\userProductAccessLog\entity;

use backend\src\entities\usersProductAccess\entity\UsersProductAccess;
use common\src\entities\product\entity\Product;
use common\src\entities\users\entity\Users;
use Yii;

/**
 * This is the model class for table "user_product_access_log".
 *
 * @property int $upal_id
 * @property int $upal_user_id
 * @property int $upal_product_id
 * @property int|null $upal_request_price_from
 * @property int|null $upal_request_price_to
 * @property int|null $upal_status_from
 * @property int|null $upal_status_to
 * @property int|null $upal_action_by_user_id
 * @property string|null $upal_start_dt
 * @property string|null $upal_end_dt
 *
 * @property Users $actionByUser
 * @property Users $user
 * @property UsersProductAccess $usersProductAccess
 * @property Product $product
 */
class UserProductAccessLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_product_access_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['upal_user_id', 'upal_product_id'], 'required'],
            [['upal_user_id', 'upal_product_id', 'upal_request_price_from', 'upal_request_price_to', 'upal_status_from', 'upal_status_to', 'upal_action_by_user_id'], 'integer'],
            [['upal_start_dt', 'upal_end_dt'], 'safe'],
            [['upal_action_by_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['upal_action_by_user_id' => 'id']],
            [['upal_user_id', 'upal_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => UsersProductAccess::class, 'targetAttribute' => ['upal_user_id' => 'upa_user_id', 'upal_product_id' => 'upa_product_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'upal_id' => Yii::t('user_product_access_log', 'ID'),
            'upal_user_id' => Yii::t('user_product_access_log', 'User ID'),
            'upal_product_id' => Yii::t('user_product_access_log', 'Product ID'),
            'upal_request_price_from' => Yii::t('user_product_access_log', 'Request Price From'),
            'upal_request_price_to' => Yii::t('user_product_access_log', 'Request Price To'),
            'upal_status_from' => Yii::t('user_product_access_log', 'Status From'),
            'upal_status_to' => Yii::t('user_product_access_log', 'Status To'),
            'upal_action_by_user_id' => Yii::t('user_product_access_log', 'Action By User ID'),
            'upal_start_dt' => Yii::t('user_product_access_log', 'Start Dt'),
            'upal_end_dt' => Yii::t('user_product_access_log', 'End Dt'),
        ];
    }

    /**
     * Gets query for [[ActionByUser]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\users\entity\Scope
     */
    public function getActionByUser()
    {
        return $this->hasOne(Users::class, ['id' => 'upal_action_by_user_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\users\entity\Scope
     */
    public function getUser()
    {
        return $this->hasOne(Users::class, ['id' => 'upal_user_id']);
    }

    /**
     * Gets query for [[Product]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\product\entity\Scope
     */
    public function getProduct()
    {
        return $this->hasOne(Product::class, ['p_id' => 'upal_product_id']);
    }

    /**
     * Gets query for [[UsersProductAccess]].
     *
     * @return \yii\db\ActiveQuery|\backend\src\entities\usersProductAccess\entity\Scope
     */
    public function getUsersProductAccess()
    {
        return $this->hasOne(UsersProductAccess::class, ['upa_user_id' => 'upal_user_id', 'upa_product_id' => 'upal_product_id']);
    }

    /**
     * {@inheritdoc}
     * @return Scope the active query used by this AR class.
     */
    public static function find()
    {
        return new Scope(get_called_class());
    }
}
