<?php

namespace common\src\entities\userProductAccessLog\entity;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\src\entities\userProductAccessLog\entity\UserProductAccessLog;

/**
 * UserProductAccessLogSearch represents the model behind the search form of `common\src\entities\userProductAccessLog\entity\UserProductAccessLog`.
 */
class UserProductAccessLogSearch extends UserProductAccessLog
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['upal_id', 'upal_user_id', 'upal_product_id', 'upal_request_price_from', 'upal_request_price_to', 'upal_status_from', 'upal_status_to', 'upal_action_by_user_id'], 'integer'],
            [['upal_start_dt', 'upal_end_dt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserProductAccessLog::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'upal_id' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'upal_id' => $this->upal_id,
            'upal_user_id' => $this->upal_user_id,
            'upal_product_id' => $this->upal_product_id,
            'upal_request_price_from' => $this->upal_request_price_from,
            'upal_request_price_to' => $this->upal_request_price_to,
            'upal_status_from' => $this->upal_status_from,
            'upal_status_to' => $this->upal_status_to,
            'upal_action_by_user_id' => $this->upal_action_by_user_id,
            'date(upal_start_dt)' => $this->upal_start_dt,
            'date(upal_end_dt)' => $this->upal_end_dt,
        ]);

        return $dataProvider;
    }
}
