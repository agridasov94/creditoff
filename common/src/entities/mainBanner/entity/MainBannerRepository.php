<?php

namespace common\src\entities\mainBanner\entity;

class MainBannerRepository
{
    public function save(MainBanner $model): int
    {
        if (!$model->save()) {
            throw new \RuntimeException($model->getErrorSummary(true)[0]);
        }
        return $model->mb_id;
    }
}