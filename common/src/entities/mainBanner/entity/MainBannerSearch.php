<?php

namespace common\src\entities\mainBanner\entity;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\src\entities\mainBanner\entity\MainBanner;

/**
 * MainBannerSearch represents the model behind the search form of `common\src\entities\mainBanner\entity\MainBanner`.
 */
class MainBannerSearch extends MainBanner
{
    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['mb_id', 'mb_is_active', 'mb_is_deleted', 'mb_order', 'mb_file_type', 'mb_created_by', 'mb_updated_by'], 'integer'],
            [['mb_title', 'mb_subtitle', 'mb_link', 'mb_link_text', 'mb_description', 'mb_file_name', 'mb_lang_id', 'mb_created_dt', 'mb_updated_dt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MainBanner::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'mb_id' => $this->mb_id,
            'mb_is_active' => $this->mb_is_active,
            'mb_is_deleted' => $this->mb_is_deleted,
            'mb_order' => $this->mb_order,
            'mb_file_type' => $this->mb_file_type,
            'mb_created_by' => $this->mb_created_by,
            'mb_updated_by' => $this->mb_updated_by,
            'mb_created_dt' => $this->mb_created_dt,
            'mb_updated_dt' => $this->mb_updated_dt,
        ]);

        $query->andFilterWhere(['like', 'mb_title', $this->mb_title])
            ->andFilterWhere(['like', 'mb_subtitle', $this->mb_subtitle])
            ->andFilterWhere(['like', 'mb_link', $this->mb_link])
            ->andFilterWhere(['like', 'mb_link_text', $this->mb_link_text])
            ->andFilterWhere(['like', 'mb_description', $this->mb_description])
            ->andFilterWhere(['like', 'mb_file_name', $this->mb_file_name])
            ->andFilterWhere(['like', 'mb_lang_id', $this->mb_lang_id]);

        return $dataProvider;
    }
}
