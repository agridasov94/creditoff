<?php

namespace common\src\entities\mainBanner\entity;

class MainBannerQuery
{
    public static function find(string $language): array
    {
        return MainBanner::find()
            ->byLanguageId($language)
            ->isActive()
            ->notSoftDeleted()
            ->orderBy([
                'mb_order' => SORT_ASC,
                'mb_id' => SORT_DESC
            ])->all();
    }
}