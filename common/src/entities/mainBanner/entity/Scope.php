<?php

namespace common\src\entities\mainBanner\entity;

/**
 * This is the ActiveQuery class for [[MainBanner]].
 *
 * @see MainBanner
 */
class Scope extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return MainBanner[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return MainBanner|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function byLanguageId(string $id): self
    {
        return $this->andWhere(['mb_lang_id' => $id]);
    }

    public function notSoftDeleted(): self
    {
        return $this->andWhere(['<>', 'mb_is_deleted', '1']);
    }

    public function isActive(): self
    {
        return $this->andWhere(['mb_is_active' => 1]);
    }
}
