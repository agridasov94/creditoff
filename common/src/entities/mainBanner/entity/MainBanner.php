<?php

namespace common\src\entities\mainBanner\entity;

use common\src\entities\language\LanguageQuery;
use common\src\entities\mainBanner\useCase\CreateForm;
use common\src\entities\mainBanner\useCase\UpdateForm;
use common\src\entities\users\entity\Users;
use lajax\translatemanager\models\Language;
use Yii;

/**
 * This is the model class for table "main_banner".
 *
 * @property int $mb_id
 * @property string $mb_title
 * @property string|null $mb_subtitle
 * @property string $mb_link
 * @property string $mb_link_text
 * @property int $mb_is_active
 * @property int $mb_is_deleted
 * @property int $mb_order
 * @property string|null $mb_description
 * @property string $mb_file_name
 * @property int $mb_file_type
 * @property string $mb_lang_id
 * @property int|null $mb_created_by
 * @property int|null $mb_updated_by
 * @property string|null $mb_created_dt
 * @property string|null $mb_updated_dt
 *
 * @property Users $createdBy
 * @property Language $lang
 * @property-read string $encodedFilePath
 * @property Users $updatedBy
 */
class MainBanner extends \yii\db\ActiveRecord
{
    public const FILE_TYPE_IMAGE = 1;
    public const FILE_TYPE_VIDEO = 2;

    public const FILE_TYPE_LIST = [
        self::FILE_TYPE_IMAGE => 'Image',
        self::FILE_TYPE_VIDEO => 'Video',
    ];

    public const FILE_TYPE_EXTENSIONS = [
        self::FILE_TYPE_IMAGE => [
            'image/png',
            'image/jpg',
            'image/jpeg',
            'png',
            'jpg',
            'jpeg'
        ],
        self::FILE_TYPE_VIDEO => [
            'video/*',
            'ogv',
            'mp4',
        ]
    ];

    public function behaviors(): array
    {
        return [
            [
                'class' => \yii\behaviors\BlameableBehavior::class,
                'createdByAttribute' => 'mb_created_by',
                'updatedByAttribute' => 'mb_updated_by',
            ],
            [
                'class' => \yii\behaviors\TimestampBehavior::class,
                'createdAtAttribute' => 'mb_created_dt',
                'updatedAtAttribute' => 'mb_updated_dt',
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'main_banner';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['mb_title', 'mb_file_name', 'mb_file_type', 'mb_lang_id'], 'required'],
            [['mb_is_active', 'mb_is_deleted', 'mb_order', 'mb_file_type', 'mb_created_by', 'mb_updated_by'], 'integer'],
            [['mb_description'], 'string'],
            [['mb_created_dt', 'mb_updated_dt'], 'safe'],
            [['mb_title', 'mb_subtitle', 'mb_link', 'mb_link_text', 'mb_file_name'], 'string', 'max' => 255],
            [['mb_lang_id'], 'string', 'max' => 5],
            [['mb_created_by'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['mb_created_by' => 'id']],
            [['mb_lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::class, 'targetAttribute' => ['mb_lang_id' => 'language_id']],
            [['mb_updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['mb_updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'mb_id' => Yii::t('main_banner', 'ID'),
            'mb_title' => Yii::t('main_banner', 'Title'),
            'mb_subtitle' => Yii::t('main_banner', 'Subtitle'),
            'mb_link' => Yii::t('main_banner', 'Link'),
            'mb_link_text' => Yii::t('main_banner', 'Link Text'),
            'mb_is_active' => Yii::t('main_banner', 'Is Active'),
            'mb_is_deleted' => Yii::t('main_banner', 'Is Deleted'),
            'mb_order' => Yii::t('main_banner', 'Order'),
            'mb_description' => Yii::t('main_banner', 'Description'),
            'mb_file_name' => Yii::t('main_banner', 'File Name'),
            'mb_file_type' => Yii::t('main_banner', 'File Type'),
            'mb_lang_id' => Yii::t('main_banner', 'Lang ID'),
            'mb_created_by' => Yii::t('main_banner', 'Created By'),
            'mb_updated_by' => Yii::t('main_banner', 'Updated By'),
            'mb_created_dt' => Yii::t('main_banner', 'Created Dt'),
            'mb_updated_dt' => Yii::t('main_banner', 'Updated Dt'),
        ];
    }

    /**
     * Gets query for [[MbCreatedBy]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\users\entity\Scope
     */
    public function getCreatedBy()
    {
        return $this->hasOne(Users::class, ['id' => 'mb_created_by']);
    }

    /**
     * Gets query for [[MbLang]].
     *
     * @return \yii\db\ActiveQuery|LanguageQuery
     */
    public function getLang()
    {
        return $this->hasOne(Language::class, ['language_id' => 'mb_lang_id']);
    }

    /**
     * Gets query for [[MbUpdatedBy]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\users\entity\Scope
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(Users::class, ['id' => 'mb_updated_by']);
    }

    /**
     * {@inheritdoc}
     * @return Scope the active query used by this AR class.
     */
    public static function find()
    {
        return new Scope(get_called_class());
    }

    public function getEncodedFilePath(): string
    {
        return base64_encode($this->mb_lang_id . '_' . $this->mb_file_type);
    }

    public static function create(CreateForm $form): self
    {
        $self = new self();
        $self->mb_title = $form->title;
        $self->mb_subtitle = $form->subtitle;
        $self->mb_link = $form->link;
        $self->mb_link_text = $form->linkText;
        $self->mb_is_active = $form->isActive;
        $self->mb_is_deleted = 0;
        $self->mb_order = $form->order;
        $self->mb_description = $form->description;
        $self->mb_file_type = $form->fileType;
        $self->mb_lang_id = $form->langId;
        $self->setFileName($form->file->extension);
        return $self;
    }

    public function fillInByUpdateForm(UpdateForm $form): void
    {
        $this->mb_title = $form->title;
        $this->mb_subtitle = $form->subtitle;
        $this->mb_link = $form->link;
        $this->mb_link_text = $form->linkText;
        $this->mb_is_active = $form->isActive;
        $this->mb_order = $form->order;
        $this->mb_description = $form->description;
        $this->mb_file_type = $form->fileType;
        if ($form->file) {
            $this->setFileName($form->file->extension);
        }
    }

    public function setFileName(string $extension): void
    {
        $this->mb_file_name = md5($this->mb_lang_id . $this->mb_title) . '.' . $extension;
    }

    public function getFileFrontendPath(): string
    {
        return Yii::getAlias('@mainBannerFrontend') . '/';
    }

    public function getFileBackendPath(): string
    {
        return Yii::getAlias('@mainBannerBackend') . '/';
    }

    public function getPublicImagePath(): string
    {
        return '/images/main-banner/' . $this->mb_file_name;
    }

    public function getFileFrontendImagePath(): string
    {
        return $this->getFileFrontendPath() . $this->mb_file_name;
    }

    public function getFileBackendImagePath(): string
    {
        return $this->getFileBackendPath() . $this->mb_file_name;
    }

    public function getTypeName(): string
    {
        return self::FILE_TYPE_LIST[$this->mb_file_type] ?? 'Unknown type';
    }
}
