<?php

namespace common\src\entities\mainBanner\useCase;

use common\src\entities\mainBanner\entity\MainBanner;
use lajax\translatemanager\models\Language;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class UpdateForm extends Model
{
// set your attributes here from MainBanner entity class
    public $id;
    public $isActive;
    public $order;
    public $fileType;
    public $title;
    public $subtitle;
    public $link;
    public $linkText;
    public $description;
    public $langId;
    /**
     * @var UploadedFile
     */
    public $file;

    public function rules(): array
    {
        // describe your rules here for validation of your attributes from MainBanner entity class
        return [
            [['id', 'isActive', 'order', 'fileType', 'title', 'langId'], 'required'],
            [['id', 'isActive', 'order', 'fileType'], 'integer'],
            [['title', 'subtitle', 'link', 'linkText', 'description'], 'string', 'max' => 255],
            [['langId'], 'string', 'max' => 5],
            [['fileType'], 'in', 'range' => array_keys(MainBanner::FILE_TYPE_LIST)],
            [['file'], 'validateFile'],
            [['file'], 'file', 'maxSize' => 2 * 1024 * 1024, 'tooBig' => 'File size cannot exceed 2MB.', 'skipOnEmpty' => true],
            [['langId'], 'exist', 'skipOnError' => true, 'targetClass' => Language::class, 'targetAttribute' => ['langId' => 'language_id']],
        ];
    }

    public function formName(): string
    {
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'mb_id' => Yii::t('main_banner', 'ID'),
            'title' => Yii::t('main_banner', 'Title'),
            'subtitle' => Yii::t('main_banner', 'Subtitle'),
            'link' => Yii::t('main_banner', 'Link'),
            'linkText' => Yii::t('main_banner', 'Link Text'),
            'isActive' => Yii::t('main_banner', 'Is Active'),
            'order' => Yii::t('main_banner', 'Order'),
            'description' => Yii::t('main_banner', 'Description'),
            'fileType' => Yii::t('main_banner', 'File Type'),
            'langId' => Yii::t('main_banner', 'Lang ID'),
        ];
    }

    public function fillByModel(MainBanner $model)
    {
        $this->id = $model->mb_id;
        $this->title = $model->mb_title;
        $this->subtitle = $model->mb_subtitle;
        $this->link = $model->mb_link;
        $this->linkText = $model->mb_link_text;
        $this->isActive = $model->mb_is_active;
        $this->order = $model->mb_order;
        $this->description = $model->mb_description;
        $this->fileType = $model->mb_file_type;
        $this->langId = $model->mb_lang_id;
    }

    public function validateFile(): void
    {
        if (!in_array($this->file->extension, MainBanner::FILE_TYPE_EXTENSIONS[$this->fileType])) {
            $this->addError('fileType', Yii::t('backend.main_banner', 'Invalid file for selected file type'));
        }
    }
}