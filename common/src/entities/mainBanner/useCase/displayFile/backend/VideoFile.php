<?php

namespace common\src\entities\mainBanner\useCase\displayFile\backend;

use common\src\entities\mainBanner\useCase\displayFile\File;

class VideoFile extends File
{
    public function getViewFilePath(): string
    {
        return 'partial/_video_file';
    }
}