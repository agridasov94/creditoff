<?php

namespace common\src\entities\mainBanner\useCase\displayFile;

use common\src\entities\mainBanner\entity\MainBanner;
use Mpdf\Tag\Main;
use yii\web\View;

interface FileInterface
{
    public function __construct(MainBanner $banner, View $view);

    public function render(): string;

    public function getViewFilePath(): string;
}