<?php

namespace common\src\entities\mainBanner\useCase\displayFile;

use common\src\entities\mainBanner\entity\MainBanner;
use yii\web\View;

abstract class File implements FileInterface
{
    protected MainBanner $banner;

    protected View $view;

    public function __construct(MainBanner $banner, View $view)
    {
        $this->banner = $banner;
        $this->view = $view;
    }

    public function render(): string
    {
        return $this->view->render($this->getViewFilePath(), [
            'banner' => $this->banner
        ]);
    }
}