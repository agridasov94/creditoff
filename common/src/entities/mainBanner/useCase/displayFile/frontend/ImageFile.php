<?php

namespace common\src\entities\mainBanner\useCase\displayFile\frontend;

use common\src\entities\mainBanner\useCase\displayFile\File;

class ImageFile extends File
{

    public function getViewFilePath(): string
    {
        return '_image_file';
    }
}