<?php

namespace common\src\entities\mainBanner\useCase\displayFile;

use common\src\entities\mainBanner\entity\MainBanner;
use common\src\entities\mainBanner\useCase\displayFile\backend\ImageFile;
use common\src\entities\mainBanner\useCase\displayFile\backend\VideoFile;
use common\src\entities\mainBanner\useCase\displayFile\frontend\ImageFile as ImageFileAlias;
use yii\web\View;

/**
 * @var MainBanner $banner
 * @var int $side
 */
class DisplayFile
{
    public const BACKEND = 1;

    public const FRONTEND = 2;

    private const LIST_DISPLAY_CLASS = [
        self::BACKEND => [
            MainBanner::FILE_TYPE_IMAGE => ImageFile::class,
            MainBanner::FILE_TYPE_VIDEO => VideoFile::class
        ],
        self::FRONTEND => [
            MainBanner::FILE_TYPE_IMAGE => ImageFileAlias::class
        ]
    ];

    private MainBanner $banner;
    private int $side;

    public function __construct(MainBanner $banner, int $side)
    {
        $this->banner = $banner;
        $this->side = $side;
    }

    public function render(View $view): string
    {
        /** @var $class FileInterface */
        $class = \Yii::createObject($this->getDisplayFileClass(), [$this->banner, $view]);
        return $class->render();
    }

    private function getDisplayFileClass(): string
    {
        if ($class = (self::LIST_DISPLAY_CLASS[$this->side][$this->banner->mb_file_type] ?? null)) {
            return $class;
        }
        throw new \RuntimeException('Undefined file type');
    }
}