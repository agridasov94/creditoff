<?php

namespace common\src\entities\mainBanner\useCase;

use common\src\entities\language\LanguageQuery;
use common\src\entities\mainBanner\entity\MainBanner;
use common\src\entities\mainBanner\entity\MainBannerRepository;
use common\src\helpers\app\AppHelper;
use yii\helpers\FileHelper;

class MainBannerService
{
    private MainBannerRepository $repository;

    public function __construct(MainBannerRepository $repository)
    {
        $this->repository = $repository;
    }

    public function create(CreateForm $form)
    {
        $model = MainBanner::create($form);
        $transaction = \Yii::$app->db->beginTransaction();

        $imagesPathForRemoving = [];
        try {
            $bannerId = $this->repository->save($model);
            if ($form->file) {
                FileHelper::createDirectory($model->getFileFrontendPath(), 0777);
                $form->file->saveAs($model->getFileFrontendImagePath(), false);
                FileHelper::createDirectory($model->getFileBackendPath());
                if(!is_link($model->getFileBackendImagePath())) {
                    symlink($model->getFileFrontendImagePath(), $model->getFileBackendImagePath());
                }
                $imagesPathForRemoving[] = $model->getFileFrontendPath();
            }

            foreach (LanguageQuery::getListExcludingLang([$form->langId]) as $language => $langValue) {
                $form->langId = $language;
                $banner = MainBanner::create($form);
                $banner->mb_id = $bannerId;
                $this->repository->save($banner);
                if ($form->file) {
                    FileHelper::createDirectory($banner->getFileFrontendPath());
                    $form->file->saveAs($banner->getFileFrontendImagePath(), false);
                    FileHelper::createDirectory($banner->getFileBackendPath());
                    if(!is_link($banner->getFileBackendImagePath())) {
                        symlink($banner->getFileFrontendImagePath(), $banner->getFileBackendImagePath());
                    }
                    $imagesPathForRemoving[] = $model->getFileFrontendPath();
                }
            }
            if (file_exists($form->file->tempName)) {
                unlink($form->file->tempName);
            }

            $transaction->commit();
        } catch (\RuntimeException $e) {
            $transaction->rollBack();
            $form->addError('general', $e->getMessage());
            \Yii::error(AppHelper::throwableFormatter($e), 'MainBannerService::create::RuntimeException');
            foreach ($imagesPathForRemoving as $value) {
                FileHelper::removeDirectory($value);
            }
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $form->addError('general', 'Internal Server Error');
            \Yii::error(AppHelper::throwableLog($e, true), 'MainBannerService::create::Throwable');
            foreach ($imagesPathForRemoving as $value) {
                FileHelper::removeDirectory($value);
            }
            throw $e;
        }
    }

    public function update(UpdateForm $form, MainBanner $banner)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $oldFileFrontendPath = $banner->getFileFrontendImagePath();
            $oldFileBackendPath = $banner->getFileBackendImagePath();
            $banner->fillInByUpdateForm($form);
            $this->repository->save($banner);
            if ($form->file) {
                if (file_exists($oldFileFrontendPath)) {
                    FileHelper::unlink($oldFileFrontendPath);
                }
                if(is_link($oldFileBackendPath)) {
                    FileHelper::unlink($oldFileBackendPath);
                }
                $form->file->saveAs($banner->getFileFrontendImagePath());
                if(!is_link($banner->getFileBackendImagePath())) {
                    symlink($banner->getFileFrontendImagePath(), $banner->getFileBackendImagePath());
                }
            }

            $transaction->commit();
        } catch (\RuntimeException $e) {
            $transaction->rollBack();
            $form->addError('general', $e->getMessage());
            \Yii::error(AppHelper::throwableLog($e, true), 'MainBannerService::update::RuntimeException');
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $form->addError('general', 'Internal Server Error');
            \Yii::error(AppHelper::throwableLog($e, true), 'MainBannerService::update::Throwable');
        }
    }
}