<?php

namespace common\src\entities\loanApplicationAccess\entity;

use yii\db\ActiveQuery;

class LoanApplicationAccessQuery
{
    public static function removePendingByIds(array $ids): int
    {
        return LoanApplicationAccess::deleteAll([
            'laa_la_id' => $ids,
            'laa_status' => LoanApplicationAccessStatus::PENDING
        ]);
    }

    /**
     * @return LoanApplicationAccess[]
     */
    public static function findPendingByRequest(int $id): array
    {
        return self::pendingByRequestQuery($id)
            ->all();
    }

    public static function getCountPendingByRequest(int $id): int
    {
        return (int)self::pendingByRequestQuery($id)->count();
    }

    public static function pendingByRequestQuery(int $id): ActiveQuery
    {
        return LoanApplicationAccess::find()
            ->pending()
            ->byRequest($id);
    }

    public static function findAcceptedByUserId(int $loanApplicationId, int $userId): ?LoanApplicationAccess
    {
        return LoanApplicationAccess::find()
            ->byRequest($loanApplicationId)
            ->accepted()
            ->byUser($userId)
            ->one();
    }

    public static function existsPendingByUserId(int $loanApplicationId, int $userId): bool
    {
        return LoanApplicationAccess::find()
            ->byRequest($loanApplicationId)
            ->pending()
            ->byUser($userId)
            ->exists();
    }

    public static function existsDeclined(int $applicationId, int $userId): bool
    {
        return LoanApplicationAccess::find()->byRequest($applicationId)->byUser($userId)->declined()->exists();
    }

    public static function findDeclinedLoanApplicationsQuery(): Scope
    {
        return LoanApplicationAccess::find()
            ->select('laa_la_id')
            ->declined()
            ->groupBy('laa_la_id');
    }
}