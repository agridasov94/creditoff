<?php

namespace common\src\entities\loanApplicationAccess\entity;

use yii\helpers\Html;

class LoanApplicationAccessStatus
{
    public const PENDING = 1;
    public const ACCEPTED = 2;
    public const CANCELED = 3;
    public const DECLINED = 4;

    public const LABEL = [
        self::PENDING => 'badge badge-warning',
        self::ACCEPTED => 'badge badge-success',
        self::CANCELED => 'badge badge-danger',
        self::DECLINED => 'badge badge-danger',
    ];

    public static function getList()
    {
        return [
            self::PENDING => \Yii::t('loan_application', 'Pending'),
            self::ACCEPTED => \Yii::t('loan_application', 'Accepted'),
            self::CANCELED => \Yii::t('loan_application', 'Canceled'),
            self::DECLINED => \Yii::t('loan_application', 'Declined'),
        ];
    }

    public static function getLabelClass(int $id): ?string
    {
        return self::LABEL[$id] ?? null;
    }

    public static function getLabel(int $id): string
    {
        return Html::tag('span', self::getName($id), [
            'class' => self::getLabelClass($id)
        ]);
    }

    public static function getName($id): ?string
    {
        return self::getList()[$id] ?? null;
    }
}