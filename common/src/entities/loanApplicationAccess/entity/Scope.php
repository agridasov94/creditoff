<?php

namespace common\src\entities\loanApplicationAccess\entity;

/**
 * This is the ActiveQuery class for [[LoanApplicationAccess]].
 *
 * @see LoanApplicationAccess
 */
class Scope extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return LoanApplicationAccess[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return LoanApplicationAccess|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function pending(): self
    {
        return $this->andWhere(['laa_status' => LoanApplicationAccessStatus::PENDING]);
    }

    public function byRequest(int $id): self
    {
        return $this->andWhere(['laa_la_id' => $id]);
    }

    public function accepted(): self
    {
        return $this->andWhere(['laa_status' => LoanApplicationAccessStatus::ACCEPTED]);
    }

    public function byUser(int $id): self
    {
        return $this->andWhere(['laa_user_id' => $id]);
    }

    public function declined(): self
    {
        return $this->andWhere(['laa_status' => LoanApplicationAccessStatus::DECLINED]);
    }
}
