<?php

namespace common\src\entities\loanApplicationAccess\entity;

use common\src\entities\loanApplication\entity\LoanApplication;
use common\src\entities\users\entity\Users;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Html;

/**
 * This is the model class for table "loan_application_access".
 *
 * @property int $laa_id
 * @property int|null $laa_la_id
 * @property int|null $laa_user_id
 * @property int|null $laa_status
 * @property int|null $laa_action_by
 * @property string|null $laa_created_dt
 * @property string|null $laa_updated_dt
 * @property int|null $laa_created_user_id
 * @property int|null $laa_updated_user_id
 *
 * @property Users $createdUser
 * @property LoanApplication $loanApplication
 * @property Users $updatedUser
 * @property Users $user
 * @property string $laa_reason [varchar(500)]
 */
class LoanApplicationAccess extends \yii\db\ActiveRecord
{
    public const ACTION_BY_PARTNER = 1;
    public const ACTION_BY_USER = 2;
    public const ACTION_BY_SYSTEM = 3;

    public const ACTION_BY_LABEL_CLASS = [
        self::ACTION_BY_PARTNER => 'badge badge-primary',
        self::ACTION_BY_USER => 'badge badge-info',
        self::ACTION_BY_SYSTEM => 'badge badge-dark text-white',
    ];

    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['laa_created_dt'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['laa_updated_dt'],
                ],
                'value' => date('Y-m-d H:i:s') //new Expression('NOW()'),
            ],
            'user' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'laa_created_user_id',
                'updatedByAttribute' => 'laa_updated_user_id',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'loan_application_access';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['laa_la_id', 'laa_user_id', 'laa_status', 'laa_action_by', 'laa_created_user_id', 'laa_updated_user_id'], 'integer'],
            [['laa_created_dt', 'laa_updated_dt', 'laa_reason'], 'safe'],
            [['laa_reason'], 'string', 'max' => 500],
            [['laa_created_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['laa_created_user_id' => 'id']],
            [['laa_la_id'], 'exist', 'skipOnError' => true, 'targetClass' => LoanApplication::class, 'targetAttribute' => ['laa_la_id' => 'la_id']],
            [['laa_updated_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['laa_updated_user_id' => 'id']],
            [['laa_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['laa_user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'laa_id' => Yii::t('loan_application', 'ID'),
            'laa_la_id' => Yii::t('loan_application', 'Loan Application ID'),
            'laa_user_id' => Yii::t('loan_application', 'User ID'),
            'laa_status' => Yii::t('loan_application', 'Status'),
            'laa_action_by' => Yii::t('loan_application', 'Action By'),
            'laa_created_dt' => Yii::t('loan_application', 'Created Dt'),
            'laa_updated_dt' => Yii::t('loan_application', 'Updated Dt'),
            'laa_created_user_id' => Yii::t('loan_application', 'Created User ID'),
            'laa_updated_user_id' => Yii::t('loan_application', 'Updated User ID'),
            'laa_reason' => Yii::t('loan_application', 'Reason'),
        ];
    }

    /**
     * Gets query for [[LaaCreatedUser]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\users\entity\Scope
     */
    public function getCreatedUser()
    {
        return $this->hasOne(Users::class, ['id' => 'laa_created_user_id']);
    }

    /**
     * Gets query for [[LaaLa]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\loanApplication\entity\Scope
     */
    public function getLoanApplication()
    {
        return $this->hasOne(LoanApplication::class, ['la_id' => 'laa_la_id']);
    }

    /**
     * Gets query for [[LaaUpdatedUser]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\users\entity\Scope
     */
    public function getUpdatedUser()
    {
        return $this->hasOne(Users::class, ['id' => 'laa_updated_user_id']);
    }

    /**
     * Gets query for [[LaaUser]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\users\entity\Scope
     */
    public function getUser()
    {
        return $this->hasOne(Users::class, ['id' => 'laa_user_id']);
    }

    /**
     * {@inheritdoc}
     * @return Scope the active query used by this AR class.
     */
    public static function find()
    {
        return new Scope(get_called_class());
    }

    public function getStatusLabelByName(): string
    {
        return LoanApplicationAccessStatus::getLabel($this->laa_status);
    }

    public static function create(
        int $loanApplicationId,
        int $userId
    ): self {
        $self = new self();
        $self->laa_la_id = $loanApplicationId;
        $self->laa_user_id = $userId;
        return $self;
    }

    public function setStatusPending(): self
    {
        $this->laa_status = LoanApplicationAccessStatus::PENDING;
        return $this;
    }

    public function setStatusAccepted(): void
    {
        $this->laa_status = LoanApplicationAccessStatus::ACCEPTED;
    }

    public function setStatusCanceled(): void
    {
        $this->laa_status = LoanApplicationAccessStatus::CANCELED;
    }

    public function setStatusDeclined(): void
    {
        $this->laa_status = LoanApplicationAccessStatus::DECLINED;
    }

    public function setActionBySystem(): self
    {
        $this->laa_action_by = self::ACTION_BY_SYSTEM;
        return $this;
    }

    public function setActionByUser(): self
    {
        $this->laa_action_by = self::ACTION_BY_USER;
        return $this;
    }

    public static function getActionByList(): array
    {
        return [
            self::ACTION_BY_PARTNER => Yii::t('loan_application', 'By Partner'),
            self::ACTION_BY_USER => Yii::t('loan_application', 'By System User'),
            self::ACTION_BY_SYSTEM => Yii::t('loan_application', 'By System'),
        ];
    }

    public static function getActionByName(int $id): ?string
    {
        return self::getActionByList()[$id] ?? null;
    }

    public static function getActionByClass(int $id): ?string
    {
        return self::ACTION_BY_LABEL_CLASS[$id] ?? null;
    }

    public function getActionByLabelName(): string
    {
        return Html::tag('span', self::getActionByName($this->laa_action_by), [
            'class' => self::getActionByClass($this->laa_action_by)
        ]);
    }

    public function isEqualUser(int $id): bool
    {
        return $this->laa_user_id === $id;
    }

}
