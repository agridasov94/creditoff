<?php

namespace common\src\entities\loanApplicationAccess\entity;

class LoanApplicationAccessRepository
{
    public function save(LoanApplicationAccess $model): int
    {
        if (!$model->save()) {
            throw new \RuntimeException($model->getErrorSummary(true)[0]);
        }
        return $model->laa_id;
    }
}