<?php

namespace common\src\entities\loanApplicationAccess\entity;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\src\entities\loanApplicationAccess\entity\LoanApplicationAccess;

/**
 * LoanApplicationAccessSearch represents the model behind the search form of `common\src\entities\loanApplicationAccess\entity\LoanApplicationAccess`.
 */
class LoanApplicationAccessSearch extends LoanApplicationAccess
{
    public $companyId;
    public $productId;
    public $loanApplicationNumber;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['laa_id', 'laa_la_id', 'laa_user_id', 'laa_status', 'laa_action_by', 'laa_created_user_id', 'laa_updated_user_id'], 'integer'],
            [['laa_created_dt', 'laa_updated_dt', 'loanApplicationNumber'], 'safe'],
            [['companyId', 'productId'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LoanApplicationAccess::find();
        $query->joinWith(['user', 'loanApplication']);


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'laa_id' => SORT_DESC
                ],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->companyId) {
            $query->andWhere(['partner_company_id' => $this->companyId]);
        }

        if ($this->productId) {
            $query->andWhere(['p_id' => $this->productId]);
            $query->joinWith('loanApplication.product');
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'laa_id' => $this->laa_id,
            'laa_la_id' => $this->laa_la_id,
            'laa_user_id' => $this->laa_user_id,
            'laa_status' => $this->laa_status,
            'laa_action_by' => $this->laa_action_by,
            'date(laa_created_dt)' => $this->laa_created_dt,
            'date(laa_updated_dt)' => $this->laa_updated_dt,
            'laa_created_user_id' => $this->laa_created_user_id,
            'laa_updated_user_id' => $this->laa_updated_user_id,
        ]);
        $query->andFilterWhere(['like', 'la_number', $this->loanApplicationNumber]);

        return $dataProvider;
    }
}
