<?php

namespace common\src\entities\faq\entity;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\src\entities\faq\entity\Faq;

/**
 * FaqSearch represents the model behind the search form of `common\src\entities\faq\entity\Faq`.
 */
class FaqSearch extends Faq
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['f_id', 'f_created_user_id', 'f_updated_user_id'], 'integer'],
            [['f_lang_id', 'f_question', 'f_answer', 'f_created_dt', 'f_updated_dt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Faq::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'f_id' => $this->f_id,
            'f_created_dt' => $this->f_created_dt,
            'f_updated_dt' => $this->f_updated_dt,
            'f_created_user_id' => $this->f_created_user_id,
            'f_updated_user_id' => $this->f_updated_user_id,
        ]);

        $query->andFilterWhere(['like', 'f_lang_id', $this->f_lang_id])
            ->andFilterWhere(['like', 'f_question', $this->f_question])
            ->andFilterWhere(['like', 'f_answer', $this->f_answer]);

        return $dataProvider;
    }
}
