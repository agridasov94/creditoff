<?php

namespace common\src\entities\faq\entity;

use common\src\entities\language\LanguageQuery;
use common\src\entities\users\entity\Users;
use lajax\translatemanager\models\Language;
use Yii;

/**
 * This is the model class for table "faq".
 *
 * @property int $f_id
 * @property string $f_lang_id
 * @property string $f_question
 * @property string|null $f_answer
 * @property string|null $f_created_dt
 * @property string|null $f_updated_dt
 * @property int|null $f_created_user_id
 * @property int|null $f_updated_user_id
 *
 * @property Language $lang
 * @property Users $createdUser
 * @property Users $updatedUser
 */
class Faq extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'faq';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['f_lang_id', 'f_question'], 'required'],
            [['f_answer'], 'string'],
            [['f_created_dt', 'f_updated_dt'], 'safe'],
            [['f_created_user_id', 'f_updated_user_id'], 'integer'],
            [['f_lang_id'], 'string', 'max' => 5],
            [['f_question'], 'string', 'max' => 255],
            [['f_created_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['f_created_user_id' => 'id']],
            [['f_lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::class, 'targetAttribute' => ['f_lang_id' => 'language_id']],
            [['f_updated_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['f_updated_user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'f_id' => Yii::t('faq', 'ID'),
            'f_lang_id' => Yii::t('faq', 'Lang ID'),
            'f_question' => Yii::t('faq', 'Question'),
            'f_answer' => Yii::t('faq', 'Answer'),
            'f_created_dt' => Yii::t('faq', 'Created Dt'),
            'f_updated_dt' => Yii::t('faq', 'Updated Dt'),
            'f_created_user_id' => Yii::t('faq', 'Created User ID'),
            'f_updated_user_id' => Yii::t('faq', 'Updated User ID'),
        ];
    }

    /**
     * Gets query for [[FCreatedUser]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\users\entity\Scope
     */
    public function getCreatedUser()
    {
        return $this->hasOne(Users::class, ['id' => 'f_created_user_id']);
    }

    /**
     * Gets query for [[FLang]].
     *
     * @return \yii\db\ActiveQuery|LanguageQuery
     */
    public function getLang()
    {
        return $this->hasOne(Language::class, ['language_id' => 'f_lang_id']);
    }

    /**
     * Gets query for [[FUpdatedUser]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\users\entity\Scope
     */
    public function getUpdatedUser()
    {
        return $this->hasOne(Users::class, ['id' => 'f_updated_user_id']);
    }

    /**
     * {@inheritdoc}
     * @return Scope the active query used by this AR class.
     */
    public static function find()
    {
        return new Scope(get_called_class());
    }

    public static function create(string $question, string $answer, string $langId): self
    {
        $self = new self();
        $self->f_question = $question;
        $self->f_answer = $answer;
        $self->f_lang_id = $langId;
        return $self;
    }
}
