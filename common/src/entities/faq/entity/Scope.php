<?php

namespace common\src\entities\faq\entity;

/**
 * This is the ActiveQuery class for [[Faq]].
 *
 * @see Faq
 */
class Scope extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Faq[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Faq|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function byLangId(string $langId): self
    {
        return $this->andWhere(['f_lang_id' => $langId]);
    }
}
