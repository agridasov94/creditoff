<?php

namespace common\src\entities\faq\entity;

class FaqQuery
{
    /**
     * @param string $langId
     * @return Faq[]
     */
    public static function findAllByLang(string $langId): array
    {
        return Faq::find()->byLangId($langId)->all();
    }
}