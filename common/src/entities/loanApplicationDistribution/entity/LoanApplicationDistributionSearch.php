<?php

namespace common\src\entities\loanApplicationDistribution\entity;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\src\entities\loanApplicationDistribution\entity\LoanApplicationDistribution;

/**
 * LoanApplicationDistributionSearch represents the model behind the search form of `common\src\entities\loanApplicationDistribution\entity\LoanApplicationDistribution`.
 */
class LoanApplicationDistributionSearch extends LoanApplicationDistribution
{
    public $companyId;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lad_id', 'lad_product_id', 'lad_user_id', 'lad_queue_depth', 'lad_total_display_loan_application', 'lad_created_user_id'], 'integer'],
            [['lad_created_dt'], 'safe'],
            [['companyId'], 'integer'],
            [['companyId'], 'filter', 'filter' => 'intval'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LoanApplicationDistribution::find();
        $query->joinWith('user');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->companyId) {
            $query->andWhere(['partner_company_id' => $this->companyId]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'lad_id' => $this->lad_id,
            'lad_product_id' => $this->lad_product_id,
            'lad_user_id' => $this->lad_user_id,
            'lad_queue_depth' => $this->lad_queue_depth,
            'lad_total_display_loan_application' => $this->lad_total_display_loan_application,
            'lad_created_dt' => $this->lad_created_dt,
            'lad_created_user_id' => $this->lad_created_user_id,
        ]);

        return $dataProvider;
    }
}
