<?php

namespace common\src\entities\loanApplicationDistribution\entity;

class LoanApplicationDistributionRepository
{
    public function save(LoanApplicationDistribution $model): int
    {
        if (!$model->save()) {
            throw new \RuntimeException($model->getErrorSummary(true)[0]);
        }
        return $model->lad_id;
    }
}