<?php

namespace common\src\entities\loanApplicationDistribution\entity;

use common\src\entities\product\entity\Product;
use common\src\entities\users\entity\Users;
use Yii;
use yii\base\Event;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "loan_application_distribution".
 *
 * @property int $lad_id
 * @property int|null $lad_product_id
 * @property int|null $lad_user_id
 * @property int|null $lad_queue_depth
 * @property int|null $lad_total_display_loan_application
 * @property string|null $lad_created_dt
 * @property int|null $lad_created_user_id
 *
 * @property Users $createdUser
 * @property Product $product
 * @property Users $user
 */
class LoanApplicationDistribution extends \yii\db\ActiveRecord
{
    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['lad_created_dt'],
                ],
                'value' => date('Y-m-d H:i:s') //new Expression('NOW()'),
            ],
            'user' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'lad_created_user_id',
                'updatedByAttribute' => null,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'loan_application_distribution';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lad_product_id', 'lad_user_id', 'lad_total_display_loan_application', 'lad_created_user_id'], 'integer'],
            [['lad_queue_depth'], 'number'],
            [['lad_created_dt'], 'safe'],
            [['lad_created_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['lad_created_user_id' => 'id']],
            [['lad_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::class, 'targetAttribute' => ['lad_product_id' => 'p_id']],
            [['lad_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['lad_user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'lad_id' => Yii::t('loan_application', 'ID'),
            'lad_product_id' => Yii::t('loan_application', 'Product ID'),
            'lad_user_id' => Yii::t('loan_application', 'User ID'),
            'lad_queue_depth' => Yii::t('loan_application', 'Queue Depth'),
            'lad_total_display_loan_application' => Yii::t('loan_application', 'Total Display Loan Application'),
            'lad_created_dt' => Yii::t('loan_application', 'Created Dt'),
            'lad_created_user_id' => Yii::t('loan_application', 'Created User ID'),
        ];
    }

    /**
     * Gets query for [[LadCreatedUser]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\users\entity\Scope
     */
    public function getCreatedUser()
    {
        return $this->hasOne(Users::class, ['id' => 'lad_created_user_id']);
    }

    /**
     * Gets query for [[LadProduct]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\product\entity\Scope
     */
    public function getProduct()
    {
        return $this->hasOne(Product::class, ['p_id' => 'lad_product_id']);
    }

    /**
     * Gets query for [[LadUser]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\users\entity\Scope
     */
    public function getUser()
    {
        return $this->hasOne(Users::class, ['id' => 'lad_user_id']);
    }

    /**
     * {@inheritdoc}
     * @return Scope the active query used by this AR class.
     */
    public static function find()
    {
        return new Scope(get_called_class());
    }

    public static function create(
        int $productId,
        int $userId,
        ?float $queueDepth,
        int $totalLoanApplications
    ): self {
        $self = new self();
        $self->lad_product_id = $productId;
        $self->lad_user_id = $userId;
        $self->lad_queue_depth = $queueDepth;
        $self->lad_total_display_loan_application = $totalLoanApplications;
        return $self;
    }

    public function decreaseTotalDisplayLoanApplications(): void
    {
        if ($this->lad_total_display_loan_application > 0) {
            $this->lad_total_display_loan_application--;
        }
    }
}
