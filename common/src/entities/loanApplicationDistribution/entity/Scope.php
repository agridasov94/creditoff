<?php

namespace common\src\entities\loanApplicationDistribution\entity;

/**
 * This is the ActiveQuery class for [[LoanApplicationDistribution]].
 *
 * @see LoanApplicationDistribution
 */
class Scope extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return LoanApplicationDistribution[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return LoanApplicationDistribution|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
