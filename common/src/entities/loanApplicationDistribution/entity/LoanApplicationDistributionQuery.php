<?php

namespace common\src\entities\loanApplicationDistribution\entity;

use yii\db\Expression;

class LoanApplicationDistributionQuery
{
    public static function decreaseTotalDisplayLoanApplicationValue(int $productId, int $userId): int
    {
        return LoanApplicationDistribution::updateAllCounters([
            'lad_total_display_loan_application' => -1],
            new Expression('lad_product_id = :productId and lad_user_id = :userId and lad_total_display_loan_application > 0'),
            [
                'productId' => $productId,
                'userId' => $userId
            ]
        );
    }
}