<?php

namespace common\src\entities\clientNotification\entity;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\src\entities\clientNotification\entity\ClientNotification;

/**
 * ClientNotificationSearch represents the model behind the search form of `common\src\entities\clientNotification\entity\ClientNotification`.
 */
class ClientNotificationSearch extends ClientNotification
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cn_id', 'cn_client_id', 'cn_type_id', 'cn_new', 'cn_deleted', 'cn_popup', 'cn_popup_show', 'cn_created_by'], 'integer'],
            [['cn_unique_id', 'cn_title', 'cn_message', 'cn_read_dt', 'cn_created_dt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ClientNotification::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'upal_id' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cn_id' => $this->cn_id,
            'cn_client_id' => $this->cn_client_id,
            'cn_type_id' => $this->cn_type_id,
            'cn_new' => $this->cn_new,
            'cn_deleted' => $this->cn_deleted,
            'cn_popup' => $this->cn_popup,
            'cn_popup_show' => $this->cn_popup_show,
            'date(cn_read_dt)' => $this->cn_read_dt,
            'date(cn_created_dt)' => $this->cn_created_dt,
        ]);

        $query->andFilterWhere(['like', 'cn_unique_id', $this->cn_unique_id])
            ->andFilterWhere(['like', 'cn_title', $this->cn_title])
            ->andFilterWhere(['like', 'cn_message', $this->cn_message]);

        return $dataProvider;
    }
}
