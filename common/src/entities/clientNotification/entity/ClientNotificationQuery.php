<?php

namespace common\src\entities\clientNotification\entity;

class ClientNotificationQuery
{
    /**
     * @param int $clientId
     * @return ClientNotification[]
     */
    public static function findAllByClientId(int $clientId): array
    {
        return ClientNotification::find()->byClientId($clientId)->notDeleted()->all();
    }
}