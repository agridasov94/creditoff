<?php

namespace common\src\entities\clientNotification\entity;

/**
 * This is the ActiveQuery class for [[ClientNotification]].
 *
 * @see ClientNotification
 */
class Scope extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return ClientNotification[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return ClientNotification|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function byClientId(int $id): self
    {
        return $this->andWhere(['cn_client_id' => $id]);
    }

    public function notDeleted(): self
    {
        return $this->andWhere(['cn_deleted' => 0]);
    }
}
