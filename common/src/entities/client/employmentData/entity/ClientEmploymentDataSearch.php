<?php

namespace common\src\entities\client\employmentData\entity;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\src\entities\client\employmentData\entity\ClientEmploymentData;

/**
 * ClientEmploymentDataSearch represents the model behind the search form of `common\src\entities\client\employmentData\entity\ClientEmploymentData`.
 */
class ClientEmploymentDataSearch extends ClientEmploymentData
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ced_id', 'ced_client_id', 'ced_type', 'ced_created_user_id', 'ced_updated_user_id'], 'integer'],
            [['ced_work_phone', 'ced_organization_name', 'ced_position', 'ced_seniority', 'ced_official_wages', 'ced_unofficial_wages', 'ced_revenues_documented', 'ced_revenues_non_documented', 'ced_created_dt', 'ced_updated_dt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ClientEmploymentData::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ced_id' => $this->ced_id,
            'ced_client_id' => $this->ced_client_id,
            'ced_type' => $this->ced_type,
            'ced_created_user_id' => $this->ced_created_user_id,
            'ced_updated_user_id' => $this->ced_updated_user_id,
            'date(ced_created_dt)' => $this->ced_created_dt,
            'date(ced_updated_dt)' => $this->ced_updated_dt,
        ]);

        $query->andFilterWhere(['like', 'ced_work_phone', $this->ced_work_phone])
            ->andFilterWhere(['like', 'ced_organization_name', $this->ced_organization_name])
            ->andFilterWhere(['like', 'ced_position', $this->ced_position])
            ->andFilterWhere(['like', 'ced_seniority', $this->ced_seniority])
            ->andFilterWhere(['like', 'ced_official_wages', $this->ced_official_wages])
            ->andFilterWhere(['like', 'ced_unofficial_wages', $this->ced_unofficial_wages])
            ->andFilterWhere(['like', 'ced_revenues_documented', $this->ced_revenues_documented])
            ->andFilterWhere(['like', 'ced_revenues_non_documented', $this->ced_revenues_non_documented]);

        return $dataProvider;
    }
}
