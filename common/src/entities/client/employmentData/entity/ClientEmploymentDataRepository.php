<?php

namespace common\src\entities\client\employmentData\entity;

class ClientEmploymentDataRepository
{
    public function save(ClientEmploymentData $model): int
    {
        if (!$model->save()) {
            throw new \RuntimeException('Client Employment Data saving failed: ' . $model->getErrorSummary(true)[0]);
        }
        return $model->ced_id;
    }
}