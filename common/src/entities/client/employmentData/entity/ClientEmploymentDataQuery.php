<?php

namespace common\src\entities\client\employmentData\entity;

class ClientEmploymentDataQuery
{
    public static function removeByClientId(int $id): void
    {
        ClientEmploymentData::deleteAll(['ced_client_id' => $id]);
    }
}