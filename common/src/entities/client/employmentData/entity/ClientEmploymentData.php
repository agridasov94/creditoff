<?php

namespace common\src\entities\client\employmentData\entity;

use backend\src\useCase\clientCreate\ClientEmploymentDataForm;
use common\src\entities\client\entity\Client;
use common\src\entities\users\entity\Users;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "client_employment_data".
 *
 * @property int $ced_id
 * @property int $ced_client_id
 * @property int|null $ced_type
 * @property string|null $ced_work_phone
 * @property string|null $ced_organization_name
 * @property string|null $ced_position
 * @property string|null $ced_seniority
 * @property string|null $ced_official_wages
 * @property string|null $ced_unofficial_wages
 * @property string|null $ced_revenues_documented
 * @property string|null $ced_revenues_non_documented
 * @property int|null $ced_created_user_id
 * @property int|null $ced_updated_user_id
 * @property string|null $ced_created_dt
 * @property string|null $ced_updated_dt
 *
 * @property Users $createdUser
 * @property Users $updatedUser
 * @property Client $client
 */
class ClientEmploymentData extends \yii\db\ActiveRecord
{
    public const SCENARIO_ENCRYPT = 'encrypt';

    public const TYPE_OFFICIAL = 1;
    public const TYPE_UNOFFICIAL = 2;

    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['ced_created_dt'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['ced_updated_dt'],
                ],
                'value' => date('Y-m-d H:i:s') //new Expression('NOW()'),
            ],
            'user' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'ced_created_user_id',
                'updatedByAttribute' => 'ced_updated_user_id',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client_employment_data';
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_ENCRYPT] = ['ced_id', 'ced_client_id', 'ced_type', 'ced_created_user_id', 'ced_updated_user_id',
            'ced_created_dt', 'ced_updated_dt', 'ced_work_phone', 'ced_organization_name', 'ced_position',
            'ced_seniority', 'ced_official_wages', 'ced_unofficial_wages', 'ced_revenues_documented', 'ced_revenues_non_documented',
            'ced_created_user_id', 'ced_updated_user_id', 'ced_client_id'];
        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ced_client_id'], 'required'],
            [['ced_client_id', 'ced_type', 'ced_created_user_id', 'ced_updated_user_id'], 'integer'],
            [['ced_created_dt', 'ced_updated_dt'], 'safe'],
            [['ced_work_phone'], 'string', 'max' => 9, 'min' => 9, 'message' => \Yii::t('frontend', 'The phone number must be in the format 0XXXXXXXX and start with 07 or 06'), 'on' => self::SCENARIO_DEFAULT],
            [['ced_work_phone'], 'match', 'pattern' => '/^(07|06)([0-9]){7}$/', 'message' => \Yii::t('frontend', 'The phone number must be in the format 0XXXXXXXX and start with 07 or 06'), 'on' => self::SCENARIO_DEFAULT],
            [['ced_organization_name', 'ced_position'], 'string', 'max' => 100],
            [['ced_seniority'], 'string', 'max' => 10, 'on' => self::SCENARIO_DEFAULT],
            [['ced_official_wages', 'ced_unofficial_wages'], 'string', 'max' => 50],
            [['ced_revenues_documented', 'ced_revenues_non_documented'], 'string', 'max' => 255],
            [['ced_created_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['ced_created_user_id' => 'id']],
            [['ced_updated_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['ced_updated_user_id' => 'id']],
            [['ced_client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::class, 'targetAttribute' => ['ced_client_id' => 'c_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ced_id' => Yii::t('client', 'ID'),
            'ced_client_id' => Yii::t('client', 'Client ID'),
            'ced_type' => Yii::t('client', 'Type'),
            'ced_work_phone' => Yii::t('client', 'Work Phone'),
            'ced_organization_name' => Yii::t('client', 'Organization Name'),
            'ced_position' => Yii::t('client', 'Position'),
            'ced_seniority' => Yii::t('client', 'Seniority'),
            'ced_official_wages' => Yii::t('client', 'Official Wages'),
            'ced_unofficial_wages' => Yii::t('client', 'Unofficial Wages'),
            'ced_revenues_documented' => Yii::t('client', 'Revenues Documented'),
            'ced_revenues_non_documented' => Yii::t('client', 'Revenues Non Documented'),
            'ced_created_user_id' => Yii::t('client', 'Created User ID'),
            'ced_updated_user_id' => Yii::t('client', 'Updated User ID'),
            'ced_created_dt' => Yii::t('client', 'Created Dt'),
            'ced_updated_dt' => Yii::t('client', 'Updated Dt'),
        ];
    }

    /**
     * Gets query for [[CedCreatedUser]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\users\entity\Scope
     */
    public function getCreatedUser()
    {
        return $this->hasOne(Users::class, ['id' => 'ced_created_user_id']);
    }

    /**
     * Gets query for [[CedUpdatedUser]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\users\entity\Scope
     */
    public function getUpdatedUser()
    {
        return $this->hasOne(Users::class, ['id' => 'ced_updated_user_id']);
    }

    /**
     * Gets query for [[CedClient]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\client\entity\Scope
     */
    public function getClient()
    {
        return $this->hasOne(Client::class, ['c_id' => 'ced_client_id']);
    }

    /**
     * {@inheritdoc}
     * @return Scope the active query used by this AR class.
     */
    public static function find()
    {
        return new Scope(get_called_class());
    }

    public static function getTypeListName(): array
    {
        return [
            self::TYPE_OFFICIAL => Yii::t('client', 'Official'),
            self::TYPE_UNOFFICIAL => Yii::t('client', 'Not Official')
        ];
    }

    public static function create(ClientEmploymentDataForm $form): self
    {
        $self = new self();
        $self->ced_type = $form->type;
        $self->ced_work_phone = $form->workPhone;
        $self->ced_organization_name = $form->organizationName;
        $self->ced_position = $form->position;
        $self->ced_seniority = $form->seniority;
        $self->ced_official_wages = $form->officialWages;
        $self->ced_unofficial_wages = $form->unofficialWages;
        $self->ced_revenues_documented = $form->revenuesDocumented;
        $self->ced_revenues_non_documented = $form->revenuesNonDocumented;
        return $self;
    }

    public static function getTypeName(int $id): ?string
    {
        return self::getTypeListName()[$id] ?? null;
    }
}
