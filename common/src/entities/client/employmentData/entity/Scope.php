<?php

namespace common\src\entities\client\employmentData\entity;

/**
 * This is the ActiveQuery class for [[ClientEmploymentData]].
 *
 * @see ClientEmploymentData
 */
class Scope extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return ClientEmploymentData[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return ClientEmploymentData|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
