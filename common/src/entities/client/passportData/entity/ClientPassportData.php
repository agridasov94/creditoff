<?php

namespace common\src\entities\client\passportData\entity;

use backend\src\useCase\clientCreate\ClientPassportDataForm;
use common\src\entities\client\entity\Client;
use common\src\entities\users\entity\Users;
use Yii;
use yii\base\Event;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "client_passport_data".
 *
 * @property int $cpd_client_id
 * @property int|null $cpd_type
 * @property string|null $cpd_series
 * @property string|null $cpd_number
 * @property string|null $cpd_issued_by
 * @property string|null $cpd_issue_date
 * @property string|null $cpd_issue_department_code
 * @property string|null $cpd_expiration_date
 * @property string|null $cpd_registered_address
 * @property string|null $cpd_residence_address
 * @property int|null $cpd_created_user_id
 * @property int|null $cpd_updated_user_id
 * @property string|null $cpd_created_dt
 * @property string|null $cpd_updated_dt
 *
 * @property Client $client
 * @property Users $createdUser
 * @property Users $updatedUser
 */
class ClientPassportData extends \yii\db\ActiveRecord
{
    public const TYPE_PASSPORT = 1;
    public const TYPE_RESIDENCE_PERMIT = 2;
    public const TYPE_BULLETIN = 3;

    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['cpd_created_dt'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['cpd_updated_dt'],
                ],
                'value' => date('Y-m-d H:i:s') //new Expression('NOW()'),
            ],
            'user' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'cpd_created_user_id',
                'updatedByAttribute' => 'cpd_updated_user_id',
            ],
            'date_cpd_issue_date' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['cpd_issue_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['cpd_issue_date'],
                ],
                'value' => function (Event $event) {
                    if ($this->cpd_issue_date) {
                        $birthDate = new \DateTime($this->cpd_issue_date);
                        return $birthDate->format('Y-m-d');
                    }
                    return null;
                }
            ],
            'date_cpd_expiration_date' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['cpd_expiration_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['cpd_expiration_date'],
                ],
                'value' => function (Event $event) {
                    if ($this->cpd_expiration_date) {
                        $birthDate = new \DateTime($this->cpd_expiration_date);
                        return $birthDate->format('Y-m-d');
                    }
                    return null;
                }
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client_passport_data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cpd_type', 'cpd_created_user_id', 'cpd_updated_user_id'], 'integer'],
            [['cpd_issue_date', 'cpd_expiration_date', 'cpd_created_dt', 'cpd_updated_dt'], 'safe'],
            [['cpd_series', 'cpd_number', 'cpd_registered_address', 'cpd_residence_address'], 'string', 'max' => 100],
            [['cpd_issued_by', 'cpd_issue_department_code'], 'string', 'max' => 50],
            [['cpd_client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::class, 'targetAttribute' => ['cpd_client_id' => 'c_id']],
            [['cpd_created_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['cpd_created_user_id' => 'id']],
            [['cpd_updated_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['cpd_updated_user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cpd_client_id' => Yii::t('client', 'Client ID'),
            'cpd_type' => Yii::t('client', 'Type'),
            'cpd_series' => Yii::t('client', 'Series'),
            'cpd_number' => Yii::t('client', 'Number'),
            'cpd_issued_by' => Yii::t('client', 'Issued By'),
            'cpd_issue_date' => Yii::t('client', 'Issue Date'),
            'cpd_issue_department_code' => Yii::t('client', 'Issue Department Code'),
            'cpd_expiration_date' => Yii::t('client', 'Expiration Date'),
            'cpd_registered_address' => Yii::t('client', 'Registered Address'),
            'cpd_residence_address' => Yii::t('client', 'Residence Address'),
            'cpd_created_user_id' => Yii::t('client', 'Created User ID'),
            'cpd_updated_user_id' => Yii::t('client', 'Updated User ID'),
            'cpd_created_dt' => Yii::t('client', 'Created Dt'),
            'cpd_updated_dt' => Yii::t('client', 'Updated Dt'),
        ];
    }

    /**
     * Gets query for [[CpdClient]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\client\entity\Scope
     */
    public function getClient()
    {
        return $this->hasOne(Client::class, ['c_id' => 'cpd_client_id']);
    }

    /**
     * Gets query for [[CpdCreatedUser]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\users\entity\Scope
     */
    public function getCreatedUser()
    {
        return $this->hasOne(Users::class, ['id' => 'cpd_created_user_id']);
    }

    /**
     * Gets query for [[CpdUpdatedUser]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\users\entity\Scope
     */
    public function getUpdatedUser()
    {
        return $this->hasOne(Users::class, ['id' => 'cpd_updated_user_id']);
    }

    /**
     * {@inheritdoc}
     * @return Scope the active query used by this AR class.
     */
    public static function find()
    {
        return new Scope(get_called_class());
    }

    public static function getTypeListName(): array
    {
        return [
            self::TYPE_PASSPORT => Yii::t('client', 'Passport'),
            self::TYPE_RESIDENCE_PERMIT => Yii::t('client', 'Residence Permit'),
            self::TYPE_BULLETIN => Yii::t('client', 'Bulletin'),
        ];
    }

    public static function create(ClientPassportDataForm $form): self
    {
        $self = new self();
        $self->cpd_type = $form->type;
        $self->cpd_series = $form->series;
        $self->cpd_number = $form->number;
        $self->cpd_issued_by = $form->issuedBy;
        if (!empty($form->issueDate)) {
            $issueDate = new \DateTime($form->issueDate);
            $self->cpd_issue_date = $issueDate->format('Y-m-d');
        }
        $self->cpd_issue_department_code = $form->issueDepartmentCode;
        if (!empty($form->expirationDate)) {
            $expirationDate = new \DateTime($form->expirationDate);
            $self->cpd_expiration_date = $expirationDate->format('Y-m-d');
        }
        $self->cpd_registered_address = $form->registeredAddress;
        $self->cpd_residence_address = $form->residenceAddress;
        return $self;
    }

    public static function getTypeName(int $id): ?string
    {
        return self::getTypeListName()[$id] ?? null;
    }
}
