<?php

namespace common\src\entities\client\passportData\entity;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\src\entities\client\passportData\entity\ClientPassportData;

/**
 * ClientPassportDataSearch represents the model behind the search form of `common\src\entities\client\passportData\entity\ClientPassportData`.
 */
class ClientPassportDataSearch extends ClientPassportData
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cpd_client_id', 'cpd_type', 'cpd_created_user_id', 'cpd_updated_user_id'], 'integer'],
            [['cpd_series', 'cpd_number', 'cpd_issued_by', 'cpd_issue_date', 'cpd_issue_department_code', 'cpd_expiration_date', 'cpd_registered_address', 'cpd_residence_address', 'cpd_created_dt', 'cpd_updated_dt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ClientPassportData::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cpd_client_id' => $this->cpd_client_id,
            'cpd_type' => $this->cpd_type,
            'cpd_issue_date' => $this->cpd_issue_date,
            'cpd_expiration_date' => $this->cpd_expiration_date,
            'cpd_created_user_id' => $this->cpd_created_user_id,
            'cpd_updated_user_id' => $this->cpd_updated_user_id,
            'date(cpd_created_dt)' => $this->cpd_created_dt,
            'date(cpd_updated_dt)' => $this->cpd_updated_dt,
        ]);

        $query->andFilterWhere(['like', 'cpd_series', $this->cpd_series])
            ->andFilterWhere(['like', 'cpd_number', $this->cpd_number])
            ->andFilterWhere(['like', 'cpd_issued_by', $this->cpd_issued_by])
            ->andFilterWhere(['like', 'cpd_issue_department_code', $this->cpd_issue_department_code])
            ->andFilterWhere(['like', 'cpd_registered_address', $this->cpd_registered_address])
            ->andFilterWhere(['like', 'cpd_residence_address', $this->cpd_residence_address]);

        return $dataProvider;
    }
}
