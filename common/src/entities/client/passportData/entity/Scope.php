<?php

namespace common\src\entities\client\passportData\entity;

/**
 * This is the ActiveQuery class for [[ClientPassportData]].
 *
 * @see ClientPassportData
 */
class Scope extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return ClientPassportData[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return ClientPassportData|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
