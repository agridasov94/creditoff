<?php

namespace common\src\entities\client\passportData\entity;

class ClientPassportDataRepository
{
    public function save(ClientPassportData $model): int
    {
        if (!$model->save()) {
            throw new \RuntimeException('Client Passport Data saving failed: ' . $model->getErrorSummary(true)[0]);
        }
        return $model->cpd_client_id;
    }
}