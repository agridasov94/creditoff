<?php

namespace common\src\entities\client\entity;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\src\entities\client\entity\Client;

/**
 * ClientSearch represents the model behind the search form of `common\src\entities\client\entity\Client`.
 */
class ClientSearch extends Client
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['c_id', 'c_gender', 'c_civil_status', 'c_social_status', 'c_own_property', 'c_created_user_id', 'c_updated_user_id'], 'integer'],
            [['c_name', 'c_last_name', 'c_birth_date', 'c_idnp', 'c_children', 'c_created_dt', 'c_updated_dt', 'c_profile_status', 'c_is_activated', 'c_auth_key'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Client::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['c_id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'c_id' => $this->c_id,
            'c_birth_date' => $this->c_birth_date,
            'c_gender' => $this->c_gender,
            'c_civil_status' => $this->c_civil_status,
            'c_social_status' => $this->c_social_status,
            'c_own_property' => $this->c_own_property,
            'c_is_activated' => $this->c_is_activated,
            'c_created_user_id' => $this->c_created_user_id,
            'c_updated_user_id' => $this->c_updated_user_id,
            'date(c_created_dt)' => $this->c_created_dt,
            'date(c_updated_dt)' => $this->c_updated_dt,
        ]);

        $query->andFilterWhere(['like', 'c_name', $this->c_name])
            ->andFilterWhere(['like', 'c_last_name', $this->c_last_name])
            ->andFilterWhere(['like', 'c_idnp', $this->c_idnp])
            ->andFilterWhere(['like', 'c_auth_key', $this->c_auth_key])
            ->andFilterWhere(['like', 'c_children', $this->c_children]);

        return $dataProvider;
    }
}
