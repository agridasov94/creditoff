<?php

namespace common\src\entities\client\entity;

use backend\src\useCase\clientCreate\ClientCreateForm;
use common\src\entities\client\email\entity\ClientEmail;
use common\src\entities\client\employmentData\entity\ClientEmploymentData;
use common\src\entities\client\passportData\entity\ClientPassportData;
use common\src\entities\client\phone\entity\ClientPhone;
use common\src\entities\loanApplication\entity\LoanApplication;
use common\src\entities\users\entity\Users;
use Yii;
use yii\base\Event;
use yii\base\NotSupportedException;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "client".
 *
 * @property int $c_id
 * @property string $c_name
 * @property string $c_last_name
 * @property string $c_birth_date
 * @property string $c_idnp
 * @property int|null $c_gender
 * @property int|null $c_civil_status
 * @property int|null $c_social_status
 * @property int|null $c_own_property
 * @property string|null $c_children
 * @property bool $c_is_blocked [tinyint(1)]
 * @property int|null $c_created_user_id
 * @property int|null $c_updated_user_id
 * @property string|null $c_created_dt
 * @property string|null $c_updated_dt
 * @property bool $c_profile_status [tinyint]
 *
 * @property Users $createdUser
 * @property Users $updatedUser
 * @property ClientEmail[] $clientEmails
 * @property ClientEmploymentData[] $clientEmploymentData
 * @property ClientPassportData $clientPassportData
 * @property ClientPhone[] $clientPhones
 * @property LoanApplication[] $loanApplications
 * @property string $c_uid [varchar(15)]
 * @property string $c_password_hash [varchar(255)]
 * @property string $c_auth_key [varchar(32)]
 * @property bool $c_is_activated [tinyint(1)]
 * @property string $c_deleted_dt [datetime]
 */
class Client extends \yii\db\ActiveRecord implements IdentityInterface
{
    public const SCENARIO_ENCRYPT = 'encrypt';

    public const GENDER_MAN = 1;
    public const GENDER_WOMAN = 2;

    public const CIVIL_STATUS_MARRIED = 1;
    public const CIVIL_STATUS_UNMARRIED = 2;

    public const SOCIAL_STATUS_EMPLOYED = 1;
    public const SOCIAL_STATUS_STUDENT = 2;
    public const SOCIAL_STATUS_PENSIONER = 3;
    public const SOCIAL_STATUS_UNEMPLOYED = 4;

    public const PROFILE_STATUS_REGISTERED = 1;
    public const PROFILE_STATUS_ACTIVATED = 2;
    public const PROFILE_STATUS_REMOVED = 3;

    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['c_created_dt'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['c_updated_dt'],
                ],
                'value' => date('Y-m-d H:i:s') //new Expression('NOW()'),
            ],
            'date' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['c_birth_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['c_birth_date'],
                ],
                'value' => function (Event $event) {
                    if ($this->c_birth_date) {
                        $birthDate = new \DateTime($this->c_birth_date);
                        return $birthDate->format('Y-m-d');
                    }
                    return null;
                }
            ],
            'user' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'c_created_user_id',
                'updatedByAttribute' => 'c_updated_user_id',
            ],
        ];
    }

    public function afterFind()
    {
        if ($this->c_birth_date) {
            $birthDate = new \DateTime($this->c_birth_date);
            $this->c_birth_date = $birthDate->format('d-m-Y');
        }
        parent::afterFind();
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client';
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_ENCRYPT] = ['c_id', 'c_name', 'c_last_name', 'c_birth_date', 'c_idnp',
            'c_birth_date', 'c_children', 'c_created_dt', 'c_updated_dt', 'c_deleted_dt',
            'c_gender', 'c_civil_status', 'c_social_status', 'c_own_property', 'c_created_user_id',
            'c_updated_user_id', 'c_is_blocked', 'c_profile_status', 'c_auth_key', 'c_is_activated'];
        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['c_name', 'c_last_name', 'c_birth_date', 'c_idnp'], 'required'],
            [['c_birth_date', 'c_children', 'c_created_dt', 'c_updated_dt', 'c_deleted_dt'], 'safe'],
            [['c_gender', 'c_civil_status', 'c_social_status', 'c_own_property', 'c_created_user_id', 'c_updated_user_id'], 'integer'],
            [['c_name', 'c_last_name'], 'trim'],
            [['c_name', 'c_last_name'], 'string', 'max' => 70, 'min' => 2],
            [['c_name', 'c_last_name'], 'match', 'pattern' => '/^[a-zA-Z\s-]+$/'],
            [['c_idnp'], 'string', 'max' => 13, 'on' => self::SCENARIO_DEFAULT],
            [['c_birth_date'], 'date', 'format' => 'd-m-Y'],
            [['c_is_blocked', 'c_profile_status'], 'integer'],
            [['c_is_blocked'], 'default', 'value' => 0],
            [['c_auth_key'], 'string', 'max' => 32],
            [['c_is_activated'], 'boolean'],
            [['c_created_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['c_created_user_id' => 'id']],
            [['c_updated_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['c_updated_user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'c_id' => Yii::t('app', 'ID'),
            'c_name' => Yii::t('app', 'Name'),
            'c_last_name' => Yii::t('app', 'Last Name'),
            'c_birth_date' => Yii::t('app', 'Birth Date'),
            'c_idnp' => Yii::t('app', 'Idnp'),
            'c_gender' => Yii::t('app', 'Gender'),
            'c_civil_status' => Yii::t('app', 'Civil Status'),
            'c_social_status' => Yii::t('app', 'Social Status'),
            'c_own_property' => Yii::t('app', 'Own Property'),
            'c_children' => Yii::t('app', 'Children'),
            'c_is_blocked' => Yii::t('app', 'Is Blocked'),
            'c_created_user_id' => Yii::t('app', 'Created User'),
            'c_updated_user_id' => Yii::t('app', 'Updated User'),
            'c_created_dt' => Yii::t('app', 'Created Dt'),
            'c_updated_dt' => Yii::t('app', 'Updated Dt'),
            'c_is_activated' => Yii::t('app', 'Is Activated'),
            'c_auth_key' => Yii::t('app', 'Auth Key'),
        ];
    }

    /**
     * Gets query for [[CCreatedUser]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\users\entity\Scope
     */
    public function getCreatedUser()
    {
        return $this->hasOne(Users::class, ['id' => 'c_created_user_id']);
    }

    /**
     * Gets query for [[CUpdatedUser]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\users\entity\Scope
     */
    public function getUpdatedUser()
    {
        return $this->hasOne(Users::class, ['id' => 'c_updated_user_id']);
    }

    /**
     * Gets query for [[ClientEmails]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\client\email\entity\Scope
     */
    public function getClientEmails()
    {
        return $this->hasMany(ClientEmail::class, ['ce_client_id' => 'c_id']);
    }

    /**
     * Gets query for [[ClientEmploymentDatas]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\client\employmentData\entity\Scope
     */
    public function getClientEmploymentData()
    {
        return $this->hasMany(ClientEmploymentData::class, ['ced_client_id' => 'c_id']);
    }

    /**
     * Gets query for [[ClientPassportData]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\client\passportData\entity\Scope
     */
    public function getClientPassportData()
    {
        return $this->hasOne(ClientPassportData::class, ['cpd_client_id' => 'c_id']);
    }

    /**
     * Gets query for [[ClientPhones]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\client\phone\entity\Scope
     */
    public function getClientPhones()
    {
        return $this->hasMany(ClientPhone::class, ['cp_client_id' => 'c_id']);
    }

    /**
     * {@inheritdoc}
     * @return Scope the active query used by this AR class.
     */
    public static function find()
    {
        return new Scope(get_called_class());
    }

    public static function getGenderListName(): array
    {
        return [
            self::GENDER_MAN => Yii::t('client', 'Man'),
            self::GENDER_WOMAN => Yii::t('client', 'Woman')
        ];
    }

    public static function getCivilStatusListName(): array
    {
        return [
            self::CIVIL_STATUS_MARRIED => Yii::t('client', 'Married'),
            self::CIVIL_STATUS_UNMARRIED => Yii::t('client', 'Unmarried')
        ];
    }

    public static function getSocialStatusListName(): array
    {
        return [
            self::SOCIAL_STATUS_EMPLOYED => Yii::t('client', 'Employed'),
            self::SOCIAL_STATUS_STUDENT => Yii::t('client', 'Student'),
            self::SOCIAL_STATUS_PENSIONER => Yii::t('client', 'Pensioner'),
            self::SOCIAL_STATUS_UNEMPLOYED => Yii::t('client', 'Unemployed'),
        ];
    }

    public static function create(ClientCreateForm $form): self
    {
        $self = new self();
        $self->c_name = $form->name;
        $self->c_last_name = $form->lastName;
//        $date = new \DateTime($form->dateOfBirth);
//        $self->c_birth_date = $date->format('Y-m-d');
        $self->c_birth_date = $form->dateOfBirth;
        $self->c_idnp = $form->idnp;
        $self->c_gender = $form->gender;
        $self->c_civil_status = $form->civilStatus;
        $self->c_social_status = $form->socialStatus;
        $self->c_own_property = $form->ownProperty;
        $self->c_is_blocked = $form->isBlocked;
        $children = [];
        foreach ($form->children as $child) {
            if (!empty($child->dateOfBirth)) {
                $children[]['dateOfBirth'] = $child->dateOfBirth;
            }
        }
        $self->c_children = $children;
        $self->generateUid();
        return $self;
    }

    public static function getGenderName(int $id): ?string
    {
        return self::getGenderListName()[$id] ?? null;
    }

    public static function getCivilStatusName(int $id): ?string
    {
        return self::getCivilStatusListName()[$id] ?? null;
    }

    public static function getSocialStatusName(int $id): ?string
    {
        return self::getSocialStatusListName()[$id] ?? null;
    }

    public function getFullName(): string
    {
        return $this->c_name . ' ' . $this->c_last_name;
    }

    public static function findIdentity($id)
    {
        return static::findOne(['c_id' => $id, 'c_is_blocked' => false]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->c_auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->c_auth_key = Yii::$app->security->generateRandomString();
    }

    public function getProfileStatusList(): array
    {
        return [
            self::PROFILE_STATUS_REGISTERED => Yii::t('client', 'Registered'),
            self::PROFILE_STATUS_ACTIVATED => Yii::t('client', 'Activated'),
            self::PROFILE_STATUS_REMOVED => Yii::t('client', 'Removed')
        ];
    }

    public function generateUid(): void
    {
        $this->c_uid = uniqid();
    }

    public static function getActivationCodeKey(string $uid): string
    {
        return 'client_' . $uid . '_code';
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     * @throws \yii\base\Exception
     */
    public function setPassword(string $password): void
    {
        $this->c_password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    public function setActivatedStatus(): void
    {
        $this->c_profile_status = self::PROFILE_STATUS_ACTIVATED;
    }

    public function setNotActivated(): void
    {
        $this->c_is_activated = 0;
    }

    public function setActivated(): void
    {
        $this->c_is_activated = 1;
    }

    public function isActivated(): bool
    {
        return (bool)$this->c_is_activated;
    }

    public function isBlocked(): bool
    {
        return (bool)$this->c_is_blocked;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->c_password_hash);
    }

    public function getLoanApplications()
    {
        return $this->hasMany(LoanApplication::class, ['la_client_id' => 'c_id']);
    }
}
