<?php

namespace common\src\entities\client\entity;

use common\src\entities\client\phone\entity\ClientPhone;

class ClientQuery
{
    public static function findByUid(string $uid): ?Client
    {
        return Client::findOne(['c_uid' => $uid]);
    }

    public static function findByIdnp(string $idnp): ?Client
    {
        return Client::findOne(['c_idnp' => $idnp]);
    }

    public static function findByIdnpAndPartialPhoneNumber(string $idnp, string $partialPhoneNumber): ?array
    {
        return Client::find()->select(['cp_phone', 'c_uid'])
            ->innerJoin(ClientPhone::tableName(), 'cp_client_id = c_id')
            ->where(['c_idnp' => $idnp])
            ->andWhere(['LIKE', 'cp_phone', '%' . $partialPhoneNumber, false])
            ->asArray()->one();
    }
}