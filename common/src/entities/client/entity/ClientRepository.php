<?php

namespace common\src\entities\client\entity;

class ClientRepository
{
    public function save(Client $client): int
    {
        if (!$client->save()) {
            throw new \RuntimeException('Client saving failed: ' . $client->getErrorSummary(true)[0]);
        }
        return $client->c_id;
    }
}