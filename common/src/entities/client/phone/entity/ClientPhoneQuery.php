<?php

namespace common\src\entities\client\phone\entity;

class ClientPhoneQuery
{
    public static function removeByClientId(int $id): void
    {
        ClientPhone::deleteAll(['cp_client_id' => $id]);
    }
}