<?php

namespace common\src\entities\client\phone\entity;

/**
 * This is the ActiveQuery class for [[ClientPhone]].
 *
 * @see ClientPhone
 */
class Scope extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return ClientPhone[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return ClientPhone|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
