<?php

namespace common\src\entities\client\phone\entity;

use borales\extensions\phoneInput\PhoneInputValidator;
use common\src\entities\client\entity\Client;
use common\src\entities\users\entity\Users;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "client_phone".
 *
 * @property int $cp_id
 * @property int $cp_client_id
 * @property string $cp_phone
 * @property int|null $cp_created_user_id
 * @property int|null $cp_updated_user_id
 * @property string|null $cp_created_dt
 * @property string|null $cp_updated_dt
 *
 * @property Client $client
 * @property Users $createdUser
 * @property Users $updatedUser
 */
class ClientPhone extends \yii\db\ActiveRecord
{
    public const SCENARIO_ENCRYPT = 'encrypt';

    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['cp_created_dt'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['cp_updated_dt'],
                ],
                'value' => date('Y-m-d H:i:s') //new Expression('NOW()'),
            ],
            'user' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'cp_created_user_id',
                'updatedByAttribute' => 'cp_updated_user_id',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client_phone';
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_ENCRYPT] = ['cp_client_id', 'cp_phone', 'cp_created_user_id', 'cp_updated_user_id', 'cp_created_dt',
            'cp_updated_dt', 'cp_id'];
        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cp_client_id', 'cp_phone'], 'required'],
            [['cp_client_id', 'cp_created_user_id', 'cp_updated_user_id'], 'integer'],
            [['cp_created_dt', 'cp_updated_dt'], 'safe'],
            [['cp_phone'], 'string', 'max' => 9, 'min' => 9, 'message' => \Yii::t('frontend', 'The phone number must be in the format 0XXXXXXXX and start with 07 or 06'), 'on' => self::SCENARIO_DEFAULT],
            [['cp_phone'], 'match', 'pattern' => '/^(07|06)([0-9]){7}$/', 'message' => \Yii::t('frontend', 'The phone number must be in the format 0XXXXXXXX and start with 07 or 06'), 'on' => self::SCENARIO_DEFAULT],
            [['cp_client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::class, 'targetAttribute' => ['cp_client_id' => 'c_id']],
            [['cp_created_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['cp_created_user_id' => 'id']],
            [['cp_updated_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['cp_updated_user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cp_id' => Yii::t('client', 'ID'),
            'cp_client_id' => Yii::t('client', 'Client ID'),
            'cp_phone' => Yii::t('client', 'Phone'),
            'cp_created_user_id' => Yii::t('client', 'Created User ID'),
            'cp_updated_user_id' => Yii::t('client', 'Updated User ID'),
            'cp_created_dt' => Yii::t('client', 'Created Dt'),
            'cp_updated_dt' => Yii::t('client', 'Updated Dt'),
        ];
    }

    /**
     * Gets query for [[CpClient]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\client\entity\Scope
     */
    public function getClient()
    {
        return $this->hasOne(Client::class, ['c_id' => 'cp_client_id']);
    }

    /**
     * Gets query for [[CpCreatedUser]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\users\entity\Scope
     */
    public function getCreatedUser()
    {
        return $this->hasOne(Users::class, ['id' => 'cp_created_user_id']);
    }

    /**
     * Gets query for [[CpUpdatedUser]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\users\entity\Scope
     */
    public function getUpdatedUser()
    {
        return $this->hasOne(Users::class, ['id' => 'cp_updated_user_id']);
    }

    /**
     * {@inheritdoc}
     * @return Scope the active query used by this AR class.
     */
    public static function find()
    {
        return new Scope(get_called_class());
    }

    public static function create(string $phone): self
    {
        $self = new self();
        $self->cp_phone = $phone;
        return $self;
    }
}
