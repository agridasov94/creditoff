<?php

namespace common\src\entities\client\phone\entity;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\src\entities\client\phone\entity\ClientPhone;

/**
 * ClientPhoneSearch represents the model behind the search form of `common\src\entities\client\phone\entity\ClientPhone`.
 */
class ClientPhoneSearch extends ClientPhone
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cp_id', 'cp_client_id', 'cp_created_user_id', 'cp_updated_user_id'], 'integer'],
            [['cp_phone', 'cp_created_dt', 'cp_updated_dt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ClientPhone::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cp_id' => $this->cp_id,
            'cp_client_id' => $this->cp_client_id,
            'cp_created_user_id' => $this->cp_created_user_id,
            'cp_updated_user_id' => $this->cp_updated_user_id,
            'cp_created_dt' => $this->cp_created_dt,
            'cp_updated_dt' => $this->cp_updated_dt,
        ]);

        $query->andFilterWhere(['like', 'cp_phone', $this->cp_phone]);

        return $dataProvider;
    }
}
