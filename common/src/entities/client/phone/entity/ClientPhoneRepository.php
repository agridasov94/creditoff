<?php

namespace common\src\entities\client\phone\entity;

class ClientPhoneRepository
{
    public function save(ClientPhone $model): int
    {
        if (!$model->save()) {
            throw new \RuntimeException('Client Phone saving failed: ' . $model->getErrorSummary(true)[0]);
        }
        return $model->cp_id;
    }
}