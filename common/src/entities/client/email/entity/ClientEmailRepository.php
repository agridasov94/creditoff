<?php

namespace common\src\entities\client\email\entity;

class ClientEmailRepository
{
    public function save(ClientEmail $clientEmail): int
    {
        if (!$clientEmail->save()) {
            throw new \RuntimeException('Client Email saving failed: ' . $clientEmail->getErrorSummary(true)[0]);
        }
        return $clientEmail->ce_id;
    }
}