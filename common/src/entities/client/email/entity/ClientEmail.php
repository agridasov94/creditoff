<?php

namespace common\src\entities\client\email\entity;

use common\src\entities\client\entity\Client;
use common\src\entities\users\entity\Users;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "client_email".
 *
 * @property int $ce_id
 * @property int $ce_client_id
 * @property string $ce_email
 * @property int|null $ce_created_user_id
 * @property int|null $ce_updated_user_id
 * @property string|null $ce_created_dt
 * @property string|null $ce_updated_dt
 *
 * @property Client $client
 * @property Users $createdUser
 * @property Users $updatedUser
 */
class ClientEmail extends \yii\db\ActiveRecord
{
    public const SCENARIO_ENCRYPT = 'encrypt';

    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['ce_created_dt'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['ce_updated_dt'],
                ],
                'value' => date('Y-m-d H:i:s') //new Expression('NOW()'),
            ],
            'user' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'ce_created_user_id',
                'updatedByAttribute' => 'ce_updated_user_id',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client_email';
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_ENCRYPT] = ['ce_client_id', 'ce_email', 'ce_created_user_id', 'ce_updated_user_id', 'c_idnp', 'ce_id'];
        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ce_client_id', 'ce_email'], 'required'],
            [['ce_client_id', 'ce_created_user_id', 'ce_updated_user_id'], 'integer'],
            [['ce_created_dt', 'ce_updated_dt'], 'safe'],
            [['ce_email'], 'string', 'max' => 100],
            [['ce_email'], 'email', 'on' => self::SCENARIO_DEFAULT],
            [['ce_client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::class, 'targetAttribute' => ['ce_client_id' => 'c_id']],
            [['ce_created_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['ce_created_user_id' => 'id']],
            [['ce_updated_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['ce_updated_user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ce_id' => Yii::t('client', 'ID'),
            'ce_client_id' => Yii::t('client', 'Client ID'),
            'ce_email' => Yii::t('client', 'Email'),
            'ce_created_user_id' => Yii::t('client', 'Created User ID'),
            'ce_updated_user_id' => Yii::t('client', 'Updated User ID'),
            'ce_created_dt' => Yii::t('client', 'Created Dt'),
            'ce_updated_dt' => Yii::t('client', 'Updated Dt'),
        ];
    }

    /**
     * Gets query for [[CeClient]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\client\entity\Scope
     */
    public function getClient()
    {
        return $this->hasOne(Client::class, ['c_id' => 'ce_client_id']);
    }

    /**
     * Gets query for [[CeCreatedUser]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\users\entity\Scope
     */
    public function getCreatedUser()
    {
        return $this->hasOne(Users::class, ['id' => 'ce_created_user_id']);
    }

    /**
     * Gets query for [[CeUpdatedUser]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\users\entity\Scope
     */
    public function getUpdatedUser()
    {
        return $this->hasOne(Users::class, ['id' => 'ce_updated_user_id']);
    }

    /**
     * {@inheritdoc}
     * @return Scope the active query used by this AR class.
     */
    public static function find()
    {
        return new Scope(get_called_class());
    }

    public static function create(string $email): self
    {
        $self = new self();
        $self->ce_email = $email;
        return $self;
    }
}
