<?php

namespace common\src\entities\client\email\entity;

class ClientEmailQuery
{
    public static function removeByClientId(int $id): void
    {
        ClientEmail::deleteAll(['ce_client_id' => $id]);
    }
}