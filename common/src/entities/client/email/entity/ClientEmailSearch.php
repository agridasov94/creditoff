<?php

namespace common\src\entities\client\email\entity;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\src\entities\client\email\entity\ClientEmail;

/**
 * ClientEmailSearch represents the model behind the search form of `common\src\entities\client\email\entity\ClientEmail`.
 */
class ClientEmailSearch extends ClientEmail
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ce_id', 'ce_client_id', 'ce_created_user_id', 'ce_updated_user_id'], 'integer'],
            [['ce_email', 'ce_created_dt', 'ce_updated_dt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ClientEmail::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ce_id' => $this->ce_id,
            'ce_client_id' => $this->ce_client_id,
            'ce_created_user_id' => $this->ce_created_user_id,
            'ce_updated_user_id' => $this->ce_updated_user_id,
            'date(ce_created_dt)' => $this->ce_created_dt,
            'date(ce_updated_dt)' => $this->ce_updated_dt,
        ]);

        $query->andFilterWhere(['like', 'ce_email', $this->ce_email]);

        return $dataProvider;
    }
}
