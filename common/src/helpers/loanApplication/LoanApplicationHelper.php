<?php

namespace common\src\helpers\loanApplication;

use common\src\entities\loanApplication\entity\LoanApplication;
use Yii;
use yii\helpers\Html;

class LoanApplicationHelper
{
    public static function generateLoanApplicationSentData(LoanApplication $loanApplication): array
    {
        return [
            'number' => $loanApplication->la_number,
            'loan_amount' => $loanApplication->la_loan_amount,
            'loan_term' => $loanApplication->la_loan_term,
            'product' => $loanApplication->product->p_name
        ];
    }

    public static function getLoanApplicationSentBtn(string $number): string
    {
        return Html::a('<i class="fa fa-recycle"></i> ' . Yii::t('loan_application', 'Open Personal Data'), null, [
            'class' => 'btn btn-sm btn-info take',
            'data-loan-number' => $number
        ]);
    }
}