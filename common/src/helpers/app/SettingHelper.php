<?php

namespace common\src\helpers\app;

class SettingHelper
{
    public static function getStepInQueueVariables(): array
    {
        return \Yii::$app->params['settings']['step_in_queue_variables'] ?? [
            'c' => 20,
            'd' => 4,
            'i' => 10
        ];
    }

    public static function getContactUsEmailAddressSendTo(): string
    {
        return \Yii::$app->params['settings']['contact_us_email_address'] ?? '';
    }

    public static function getCriticalSmsBalanceAmount(): float
    {
        return (float)(\Yii::$app->params['settings']['sms_check_balance_amount']['critical_balance_amount'] ?? 0.00);
    }

    public static function getUserRolesToBeNotified(): array
    {
        return \Yii::$app->params['settings']['sms_check_balance_amount']['user_roles_to_be_notified'] ?? [];
    }

    public static function getSecondsLifetimeProfileActivationCode(): int
    {
        return (int)(\Yii::$app->params['settings']['seconds_lifetime_profile_activation_code'] ?? 120);
    }

    public static function getPrivacyPolicyFilePath(): string
    {
        return (string)(\Yii::$app->params['settings']['privacy_policy_file_path'] ?? '');
    }
}