<?php

use Dotenv\Dotenv;
use Dotenv\Repository\Adapter\EnvConstAdapter;
use Dotenv\Repository\Adapter\PutenvAdapter;
use Dotenv\Repository\RepositoryBuilder;

class EnvLoader
{
    private string $path = '/';
    private string $envFileName = '.env';

    public function __construct(?string $path = null, ?string $envFileName = null)
    {
        if ($path !== null) {
            $this->path = $path;
        }
        if ($envFileName !== null) {
            $this->envFileName = $envFileName;
        }
    }

    public function load(): void
    {
        $repository = RepositoryBuilder::createWithNoAdapters()
            ->addAdapter(EnvConstAdapter::class)
            ->addWriter(PutenvAdapter::class)
            ->immutable()
            ->make();

        $dotenv = Dotenv::create($repository, $this->path, $this->envFileName);
        $dotenv->load();
    }
}