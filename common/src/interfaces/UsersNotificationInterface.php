<?php

namespace common\src\interfaces;

interface UsersNotificationInterface
{
    public function createAndPublish(int $userId, string $title, string $message, int $type, bool $popup = true): void;

    public function getTypeWarning(): int;
}