<?php

namespace common\src\services\sms;

use common\components\sms\SmsComponent;
use common\src\entities\users\entity\UsersQuery;
use common\src\helpers\app\SettingHelper;
use common\src\interfaces\UsersNotificationInterface;

class SmsService
{
    public function checkBalance(UsersNotificationInterface $notificationService): void
    {
        /** @var $smsComponent SmsComponent */
        $smsComponent = \Yii::$app->sms;
        $result = $smsComponent->getBalance();

        if ($result->hasError()) {
            \Yii::warning([
                'apiRequestResult' => $result->getErrorMessage(),
                'apiRequestResultCode' => $result->getErrorCode()
            ], 'SmsService::checkBalance');
            return;
        }

        $balanceResult = $result->getResult();
        if (!isset($balanceResult['balance'])) {
            \Yii::warning([
                'apiRequestResult' => 'Index not found in api result',
            ], 'SmsService::checkBalance');
            return;
        }

        $balance = (float)$balanceResult['balance'];
        $criticalAmount = SettingHelper::getCriticalSmsBalanceAmount();
        if ($balance <= $criticalAmount) {
            $users = UsersQuery::getAllUsersByRoles(SettingHelper::getUserRolesToBeNotified());
            foreach ($users as $user) {
                $notificationService->createAndPublish(
                    $user->id,
                    \Yii::t('sms', 'SMS account balance'),
                    \Yii::t('sms', 'The balance on the account of the sms service is less than the critical value: {criticalValue}. Current balance: {currentBalance}', [
                        'criticalValue' => $criticalAmount,
                        'currentBalance' => $balance
                    ]),
                    $notificationService->getTypeWarning()
                );
            }
        }
    }
}