<?php

namespace common\src\services;

use backend\src\entities\usersProductAccess\entity\UsersProductAccess;
use backend\src\entities\usersProductAccess\entity\UsersProductAccessQuery;
use backend\src\helpers\WebSocketHelper;
use common\src\entities\loanApplication\entity\LoanApplication;
use common\src\entities\loanApplication\entity\LoanApplicationQuery;
use common\src\entities\loanApplication\entity\LoanApplicationRepository;
use common\src\entities\loanApplication\entity\LoanApplicationStatus;
use common\src\entities\loanApplicationAccess\entity\LoanApplicationAccess;
use common\src\entities\loanApplicationAccess\entity\LoanApplicationAccessQuery;
use common\src\entities\loanApplicationAccess\entity\LoanApplicationAccessRepository;
use common\src\entities\loanApplicationDistribution\entity\LoanApplicationDistribution;
use common\src\entities\loanApplicationDistribution\entity\LoanApplicationDistributionRepository;
use common\src\entities\product\entity\Product;
use common\src\entities\users\entity\Users;
use common\src\entities\users\entity\UsersQuery;
use common\src\helpers\loanApplication\LoanApplicationHelper;
use yii\helpers\ArrayHelper;

/**
 * @property-read LoanApplicationDistributionRepository $loanApplicationDistributionRepository
 * @property-read LoanApplicationAccessRepository $loanApplicationAccessRepository
 * @property-read LoanApplicationRepository $loanApplicationRepository
 */
class LoanApplicationDistributionService
{
    private LoanApplicationDistributionRepository $loanApplicationDistributionRepository;
    private LoanApplicationAccessRepository $loanApplicationAccessRepository;
    private LoanApplicationRepository $loanApplicationRepository;

    public function __construct(
        LoanApplicationDistributionRepository $loanApplicationDistributionRepository,
        LoanApplicationAccessRepository $loanApplicationAccessRepository,
        LoanApplicationRepository $loanApplicationRepository
    ) {
        $this->loanApplicationDistributionRepository = $loanApplicationDistributionRepository;
        $this->loanApplicationAccessRepository = $loanApplicationAccessRepository;
        $this->loanApplicationRepository = $loanApplicationRepository;
    }

    public function handle(Product $product): void
    {
        $availableUsers = UsersProductAccessQuery::findAllAvailableUsersByProduct($product->p_id);
        $applicationsDistribution = $this->calculateQueueDepth($availableUsers, $product->p_step_in_queue, $product->p_id);


        $availableLoanApplications = LoanApplicationQuery::findAllSentExceptDeclinedByProductId($product->p_id);
        \Yii::info([
            'debug' => __CLASS__,
            'availableLoanApplications' => count($availableLoanApplications)
        ], 'info\distribution');
        foreach ($availableLoanApplications as $key => $application) {
            foreach ($applicationsDistribution as $applicationDistribution) {
                if (LoanApplicationAccessQuery::existsDeclined($application->la_id, $applicationDistribution->lad_user_id)) {
                    $applicationDistribution->decreaseTotalDisplayLoanApplications();
                    continue;
                }

                if (($key + 1) > $applicationDistribution->lad_total_display_loan_application) {
                    continue;
                }
//                if (!LoanApplicationCompaniesQuery::applicationCanViewedByCompany($application->la_id, $applicationDistribution->user->partner_company_id)) {
//                    $applicationDistribution->decreaseTotalDisplayLoanApplications();
//                    continue;
//                }
                $access = LoanApplicationAccess::create($application->la_id, $applicationDistribution->lad_user_id)
                    ->setStatusPending()
                    ->setActionBySystem();
                $this->loanApplicationAccessRepository->save($access);
                WebSocketHelper::pub(
                    [WebSocketHelper::getLoanApplicationPubSubKey((int)$application->la_status, (int)$applicationDistribution->lad_user_id)],
                    'add_loan_application',
                    [
                        'data' => LoanApplicationHelper::generateLoanApplicationSentData($application),
                        'loanApplicationId' => $application->la_id,
                        'btnHtml' => LoanApplicationHelper::getLoanApplicationSentBtn($application->la_number)
                    ]
                );
            }
            $application->increaseDistributionIteration();
            $this->loanApplicationRepository->save($application);
        }
        foreach ($applicationsDistribution as $applicationDistribution) {
            if ($applicationDistribution->dirtyAttributes) {
                $this->loanApplicationDistributionRepository->save($applicationDistribution);
            }
        }

//        foreach ($applicationsDistribution as $applicationDistribution) {
//            $availableLoanApplications = LoanApplicationQuery::findAvailableForUser($product->p_id, $applicationDistribution->lad_total_display_loan_application);
//            foreach ($availableLoanApplications as $application) {
//                if (LoanApplicationAccessQuery::existsDeclined($application->la_id, $applicationDistribution->lad_user_id)) {
//                    $applicationDistribution->decreaseTotalDisplayLoanApplications();
//                    continue;
//                }
//                if (!LoanApplicationCompaniesQuery::applicationCanViewedByCompany($application->la_id, $applicationDistribution->user->partner_company_id)) {
//                    $applicationDistribution->decreaseTotalDisplayLoanApplications();
//                    continue;
//                }
//                $access = LoanApplicationAccess::create($application->la_id, $applicationDistribution->lad_user_id);
//                $access->setStatusPending();
//                $access->setActionBySystem();
//                $this->loanApplicationAccessRepository->save($access);
//                WebSocketHelper::pub(
//                    [WebSocketHelper::getLoanApplicationPubSubKey((int)$application->la_status, (int)$applicationDistribution->lad_user_id)],
//                    'add_loan_application',
//                    [
//                        'data' => LoanApplicationHelper::generateLoanApplicationSentData($application),
//                        'loanApplicationId' => $application->la_id,
//                        'btnHtml' => LoanApplicationHelper::getLoanApplicationSentBtn($application->la_number)
//                    ]
//                );
//            }
//            if ($applicationDistribution->dirtyAttributes) {
//                $this->loanApplicationDistributionRepository->save($applicationDistribution);
//            }
//        }
    }

    /**
     * @param UsersProductAccess[] $usersProductAccess
     */
    public function calculateQueueDepth(array $usersProductAccess, int $stepInQueue, int $productId): array
    {
        $totalSentLoanApplications = LoanApplicationQuery::countAllSentExceptDeclinedByProductId($productId);

        /** @var LoanApplicationDistribution[] $loanApplicationDistributionData */
        $loanApplicationDistributionData = [];
        $depthSum = 0;
        $totalUsersProductAccess = count($usersProductAccess)-1;
        foreach ($usersProductAccess as $key => $userProductAccess) {
            $nextUserRequestPrice = $usersProductAccess[$key + 1]->upa_request_price ?? 0;
//            if ($key === $totalUsersProductAccess) {
//                $depth = null;
//            } else {
                $depth = ($userProductAccess->upa_request_price - $nextUserRequestPrice) / $stepInQueue;
//            }

            $totalDisplayingApplications = 0;
            if ($key === 0 && $userProductAccess->upa_request_price > 0) {
                $totalDisplayingApplications = $totalSentLoanApplications;
                $depthSum += $depth;
            } elseif ($userProductAccess->upa_request_price > 0) {
                if ($totalSentLoanApplications > $depthSum) {
                    $totalDisplayingApplications = round($totalSentLoanApplications - $depthSum);
                }
                $depthSum += $depth;
            }

            $loanApplicationDistribution = LoanApplicationDistribution::create(
                $productId,
                $userProductAccess->upa_user_id,
                $depth,
                $totalDisplayingApplications
            );
            $this->loanApplicationDistributionRepository->save($loanApplicationDistribution);
            $loanApplicationDistributionData[$key] = $loanApplicationDistribution;
        }
        return $loanApplicationDistributionData;
    }

    public function removeCurrentDistributionsAndAccess(int $productId): void
    {
        $loanApplicationsIds = LoanApplicationQuery::findAvailableIdsForUser($productId, null);
        foreach ($loanApplicationsIds as $loanApplicationId) {
            $loanApplicationAccesses = LoanApplicationAccessQuery::findPendingByRequest($loanApplicationId);
            foreach ($loanApplicationAccesses as $applicationAccess) {
                WebSocketHelper::pub(
                    [WebSocketHelper::getLoanApplicationPubSubKey((int)$applicationAccess->loanApplication->la_status, (int)$applicationAccess->laa_user_id)],
                    'remove_loan_application',
                    [
                        'loanApplicationId' => $loanApplicationId,
                    ]
                );
            }
        }
        LoanApplicationAccessQuery::removePendingByIds($loanApplicationsIds);
        LoanApplicationDistribution::deleteAll(['lad_product_id' => $productId]);

    }

    public function setAccessForAllUsersToLoanApplication(LoanApplication $application, Users $currentUser): void
    {
        $users = UsersQuery::findAvailableUsersForLoanApplication($application->la_id, $application->la_product_id, $currentUser->partner_company_id);

        foreach ($users as $user) {
            $access = LoanApplicationAccess::create($application->la_id, $user->id)
                ->setStatusPending()
                ->setActionBySystem();
            $this->loanApplicationAccessRepository->save($access);
        }

        $application->setStatus(LoanApplicationStatus::STATUS_SENT);
        $application->flushOwner();
        $application->increaseDistributionIteration();
        $this->loanApplicationRepository->save($application);
    }
}