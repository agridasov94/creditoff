<?php

namespace common\src\services\product;

use backend\src\entities\usersProductAccess\entity\UsersProductAccessQuery;
use common\src\entities\product\entity\Product;
use common\src\entities\product\entity\ProductRepository;
use common\src\helpers\app\SettingHelper;
use frontend\components\jobs\LoanApplicationDistributionJob;

/**
 * @var ProductRepository $productRepository
 */
class ProductService
{
    private ProductRepository $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function calculateStepInQueue(Product $product): void
    {
        $maxPrices = UsersProductAccessQuery::findTwoMaxPricesForRequestByProduct($product->p_id);
        $maxPrice = $maxPrices[0] ?? 0;
        $secondPrice = $maxPrices[1] ?? 0;
        $variables = $product->getStepInQueueVariables();
        $c = $variables['c'] ?? 0;
        $d = $variables['d'] ?? 0;
        $i = $variables['i'] ?? 0;


        if (($maxPrice - $secondPrice) > $c) {
            $step = $d;
        } else {
            $step = $i;
        }

        \Yii::info([
            'c' => $c,
            'd' => $d,
            'i' => $i,
            'maxPrice' => $maxPrice,
            'minPrice' => $secondPrice,
            'step' => $step
        ], 'info\stepinqueue');

        $product->p_step_in_queue = $step;
        $this->productRepository->save($product);
    }
}