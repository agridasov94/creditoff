<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@blogFrontend', dirname(dirname(__DIR__)) . '/frontend/web/images/blog/');
Yii::setAlias('@blogBackend', dirname(dirname(__DIR__)) . '/backend/web/images/blog/');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@backendRuntime', dirname(dirname(__DIR__)) . '/backend/runtime');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@mainBannerFrontend', dirname(dirname(__DIR__)) . '/frontend/web/images/main-banner/');
Yii::setAlias('@mainBannerBackend', dirname(dirname(__DIR__)) . '/backend/web/images/main-banner/');
