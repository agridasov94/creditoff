<?php

use yii\queue\beanstalk\Queue;

return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(__DIR__, 2) . '/vendor',
    'bootstrap' => [
        'queue_loan_application_distribution',
        'queue_sms_job',
        \common\components\SettingsBootstrap::class,
        \common\bootstrap\LoanApplicationLogger::class,
        \common\bootstrap\UserProductAccessLogger::class
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'redis' => [
            'class' => \yii\redis\Connection::class,
            'hostname' => $_ENV['REDIS_HOST'],
            'unixSocket' => null,
            'port' => $_ENV['REDIS_PORT'],
            'database' => 0,
            'password' => null
        ],
        'sms' => [
            'class' => \common\components\sms\SmsComponent::class,
            'baseHost' => 'https://api.sms.md',
            'tokenApiKey' => $_ENV['SMS_TOKEN_API_KEY'],
            'sendUrl' => '/v1/send',
            'checkBalanceUrl' => '/v1/balance',
            'alias' => $_ENV['SMS_ALIAS']
        ],
        'queue_loan_application_distribution' => [
            'class' => Queue::class,
            'host' => $_ENV['QUEUE_LOAN_APPLICATION_DISTRIBUTION_HOST'],
            'port' => $_ENV['QUEUE_LOAN_APPLICATION_DISTRIBUTION_PORT'],
            'tube' => 'queue_loan_application_distribution',
        ],
        'queue_sms_job' => [
            'class' => Queue::class,
            'host' => $_ENV['QUEUE_SMS_HOST'],
            'port' => $_ENV['QUEUE_SMS_PORT'],
            'tube' => 'queue_sms_job'
        ]
    ],

];
