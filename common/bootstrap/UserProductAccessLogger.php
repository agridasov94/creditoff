<?php

namespace common\bootstrap;

use backend\src\auth\Auth;
use backend\src\entities\usersProductAccess\entity\UsersProductAccess;
use common\src\entities\userProductAccessLog\entity\UserProductAccessLog;
use yii\base\BootstrapInterface;
use yii\base\Event;
use yii\db\ActiveRecord;
use yii\db\AfterSaveEvent;

class UserProductAccessLogger extends AbstractLogger implements BootstrapInterface
{
    /**
     * @param \yii\base\Application $app
     */
    public function bootstrap($app): void
    {
        $func =  static function (AfterSaveEvent $event) {
            $oldAttr = self::getOldAttrs($event);
            $newAttr = self::getNewAttrs($event);

            if (empty($oldAttr) && empty($newAttr)) {
                return;
            }

            $log = new UserProductAccessLog();
            $userId = $event->sender->attributes['upa_user_id'];
            $productId = $event->sender->attributes['upa_product_id'];
            $lastLog = UserProductAccessLog::find()
                ->where(['upal_user_id' => $userId, 'upal_product_id' => $productId])
                ->orderBy(['upal_id' => SORT_DESC])
                ->one();
            $endDateTime = date('Y-m-d H:i:s');
            if ($lastLog) {
                $lastLog->upal_end_dt = $endDateTime;
                $lastLog->save();
            }
            $statusBefore = $oldAttr['upa_status'] ?? ($event->sender->attributes['upa_status'] ?? null);
            $statusAfter = $newAttr['upa_status'] ?? ($event->sender->attributes['upa_status'] ?? null);
            $requestPriceBefore = $oldAttr['upa_request_price'] ?? ($event->sender->attributes['upa_request_price'] ?? null);
            $requestPriceAfter = $newAttr['upa_request_price'] ?? ($event->sender->attributes['upa_request_price'] ?? null);
            $log->upal_user_id = (int)$userId;
            $log->upal_product_id = (int)$productId;
            $log->upal_request_price_from = $requestPriceBefore;
            $log->upal_request_price_to = $requestPriceAfter;
            $log->upal_status_from = $statusBefore;
            $log->upal_status_to = $statusAfter;
            $log->upal_action_by_user_id = Auth::id();
            $log->upal_start_dt = $endDateTime;
            if (!$log->save()) {
                \Yii::error($log->getErrorSummary(true)[0], 'UserProductAccessLogger');
            }
        };

        Event::on(UsersProductAccess::class, ActiveRecord::EVENT_AFTER_UPDATE, $func);
        Event::on(UsersProductAccess::class, ActiveRecord::EVENT_AFTER_INSERT, $func);
    }
}