<?php

namespace common\bootstrap;

use common\src\entities\loanApplication\entity\LoanApplication;
use common\src\entities\loanApplication\log\entity\LoanApplicationLog;
use yii\base\BootstrapInterface;
use yii\base\Event;
use yii\db\ActiveRecord;
use yii\db\AfterSaveEvent;

class LoanApplicationLogger extends AbstractLogger implements BootstrapInterface
{
    /**
     * @param \yii\base\Application $app
     */
    public function bootstrap($app): void
    {
        $func =  static function (AfterSaveEvent $event) {
            $oldAttr = self::getOldAttrs($event);
            $newAttr = self::getNewAttrs($event);

            if (empty($oldAttr) && empty($newAttr)) {
                return;
            }

            $log = new LoanApplicationLog();
            $statusBefore = $oldAttr['la_status'] ?? null;
            $statusAfter = $newAttr['la_status'] ?? null;
            $pkName = $event->sender::primaryKey()[0];
            $loanApplicationId = $event->sender->attributes[$pkName];
            $lastLog = LoanApplicationLog::find()
                ->where(['lal_loan_application_id' => $loanApplicationId])
                ->orderBy(['lal_id' => SORT_DESC])
                ->one();
            $endDateTime = date('Y-m-d H:i:s');
            if ($lastLog) {
                $lastLog->lal_end_dt = $endDateTime;
                $lastLog->save();
            }
            $log->lal_owner_id = $event->sender->attributes['la_owner_id'] ?? null;
            $log->lal_status_after = $statusAfter;
            $log->lal_status_before = $statusBefore;
            $log->lal_start_dt = $endDateTime;
            $log->lal_loan_application_id = $loanApplicationId;
            if (!$log->save()) {
                \Yii::error($log->getErrorSummary(true)[0], 'LoanApplicationLogger');
            }
        };

        Event::on(LoanApplication::class, ActiveRecord::EVENT_AFTER_UPDATE, $func);
        Event::on(LoanApplication::class, ActiveRecord::EVENT_AFTER_INSERT, $func);
    }
}