<?php

namespace common\bootstrap;

use yii\db\ActiveRecord;
use yii\db\AfterSaveEvent;

class AbstractLogger
{
    /**
     * @param AfterSaveEvent $event
     * @return string
     */
    protected static function getNewAttrs(AfterSaveEvent $event): array
    {
        $newAttr = [];
        foreach ($event->changedAttributes as $key => $attribute) {
            if (array_key_exists($key, $event->sender->attributes)) {
                $newAttr[$key] = $event->sender->attributes[$key];
            }
        }
        return $newAttr;
    }

    /**
     * @param AfterSaveEvent $event
     * @return array
     */
    protected static function getOldAttrs(AfterSaveEvent $event): array
    {
        if ($event->name === ActiveRecord::EVENT_AFTER_INSERT) {
            $oldAttr = [];
        } else {
            $oldAttr = $event->changedAttributes;
        }
        return $oldAttr;
    }
}