<?php

namespace common\components\sms;

use yii\httpclient\Response;

class SmsResult
{
    private bool $_hasError = false;

    private ?int $_errorCode = null;

    private string $_errorMessage = '';

    private Response $response;

    private array $result = [];

    public function __construct(Response $response)
    {
        $this->response = $response;
    }

    public function handle(): self
    {
        if ($this->response->isOk) {
            $this->result = (array)$this->response->data;
            return $this;
        }

        try {
            $result = json_decode($this->response->content, true, 512, JSON_THROW_ON_ERROR);
        } catch (\Throwable $e) {
            $this->_hasError = true;
            $this->_errorCode = -1;
            $this->_errorMessage = \Yii::t('common', 'Error occurred while trying to parse response from sms service');
            return $this;
        }

        $this->_hasError = true;
        $this->_errorCode = $result['code'] ?? 0;
        $this->_errorMessage = $result['message'] ?? 'Unknown message';
        return $this;
    }

    public function hasError(): bool
    {
        return $this->_hasError;
    }

    public function getErrorMessage(): string
    {
        return $this->_errorMessage;
    }

    public function getErrorCode(): ?int
    {
        return $this->_errorCode;
    }

    public function getResult(): array
    {
        return $this->result;
    }
}