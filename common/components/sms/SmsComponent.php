<?php

namespace common\components\sms;

use yii\httpclient\Client;
use yii\httpclient\CurlTransport;
use yii\httpclient\Response;

class SmsComponent extends \yii\base\Component
{
    public $baseHost;
    public $tokenApiKey;
    public $sendUrl;
    public $checkBalanceUrl;
    public $alias;

    public function send(string $toPhoneNumber, string $message, string $method = 'GET', ?string $time = null): SmsResult
    {
        $client = new Client([
            'transport' => CurlTransport::class,
            'responseConfig' => [
                'format' => Client::FORMAT_JSON
            ]
        ]);

        $params = [
            'from' => $this->alias,
            'to' => $toPhoneNumber,
            'message' => $message,
            'token' => $this->tokenApiKey
        ];

        if ($time) {
            $params['time'] = $time;
        }

        $result = $client->createRequest()
            ->setMethod($method)
            ->setUrl($this->_getFullUrl($this->sendUrl))
            ->setData($params)
            ->setOptions([
                CURLOPT_CONNECTTIMEOUT => 5,
                CURLOPT_TIMEOUT => 30,
            ])
            ->send();

        return (new SmsResult($result))->handle();
    }

    public function generateActivationCode(int $length = 5): string
    {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[random_int(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function getBalance(): SmsResult
    {
        $client = new Client([
            'transport' => CurlTransport::class,
            'responseConfig' => [
                'format' => Client::FORMAT_JSON
            ]
        ]);

        $params = [
            'token' => $this->tokenApiKey
        ];

        $result = $client->createRequest()
            ->setMethod('GET')
            ->setUrl($this->_getFullUrl($this->checkBalanceUrl))
            ->setData($params)
            ->setOptions([
                CURLOPT_CONNECTTIMEOUT => 5,
                CURLOPT_TIMEOUT => 30,
            ])
            ->send();

        return (new SmsResult($result))->handle();
    }

    private function _getFullUrl(string $endpoint): string
    {
        return $this->baseHost . $endpoint;
    }
}