<?php

namespace common\components\jobs\test;

class TestJob extends \yii\base\BaseObject implements \yii\queue\JobInterface
{
    public function execute($queue)
    {
        \Yii::info('job executed', 'info\TestJob');
    }
}