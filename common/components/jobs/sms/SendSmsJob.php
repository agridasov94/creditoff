<?php

namespace common\components\jobs\sms;

use common\components\sms\SmsComponent;
use common\src\helpers\app\AppHelper;

class SendSmsJob extends \yii\base\BaseObject implements \yii\queue\JobInterface
{
    private string $toPhoneNumber;
    private string $message;
    private string $method;
    private ?string $time;

    public function __construct(string $toPhoneNumber, string $message, string $method = 'GET', ?string $time = null, $config = [])
    {
        parent::__construct($config);
        $this->toPhoneNumber = $toPhoneNumber;
        $this->message = $message;
        $this->method = $method;
        $this->time = $time;
    }

    public function execute($queue)
    {
        /** @var $sms SmsComponent */
        $sms = \Yii::$app->sms;

        $sendSms = $sms->send($this->toPhoneNumber, $this->message, $this->method, $this->time);

        if ($sendSms->hasError()) {
            \Yii::warning([
                'message' => 'SMS component has error',
                'code' => $sendSms->getErrorCode(),
                'componentMessage' => $sendSms->getErrorMessage(),
            ], 'SendSmsJob');
        }
    }
}