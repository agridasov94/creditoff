<?php

namespace common\components\i18n;

use backend\src\helpers\purifier\Purifier;
use common\src\entities\users\entity\Users;
use yii\helpers\Html;

class Formatter extends \yii\i18n\Formatter
{
    public function asUsername($value)
    {
        if (!$value) {
            return $this->nullDisplay;
        }

        if (is_string($value)) {
            $name = $value;
        } elseif ($value instanceof Users) {
            $name = $value->username;
        } elseif (is_int($value)) {
            if ($entity = Users::find()->select(['username'])->where(['id' => $value])->cache(3600)->one()) {
                $name = $entity->username;
            } else {
                return \Yii::t('app', 'Not Found');
            }
        } else {
            throw new \InvalidArgumentException('user must be Employee|int|string|null');
        }

        return Html::tag('i', '', ['class' => 'fa fa-user']) . ' ' . Html::encode($name);
    }

    /**
     * @param $dateTime
     * @param string $format
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function asByUserDateTime($dateTime, $format = 'php:d-M-Y [H:i]'): string
    {
        if (!$dateTime) {
            return $this->nullDisplay;
        }
        return Html::tag('i', '', ['class' => 'fa fa-calendar']) . ' ' . $this->asDatetime(strtotime($dateTime), $format);
    }

    public function asByUserDate($dateTime, $format = 'php:d-M-Y'): string
    {
        if (!$dateTime) {
            return $this->nullDisplay;
        }
        return Html::tag('i', '', ['class' => 'fa fa-calendar']) . ' ' . $this->asDate(strtotime($dateTime), $format);
    }

    /**
     * @param $value
     * @return string
     */
    public function asBooleanByLabel($value): string
    {
        if ($value === null) {
            return $this->nullDisplay;
        }
        if ($value) {
            return Html::tag('span', \Yii::t('yii', 'Yes'), ['class' => 'badge badge-success']);
        }
        return Html::tag('span', \Yii::t('yii', 'No'), ['class' => 'badge badge-danger']);
    }

    public function asTextWithLinks($value): string
    {
        if ($value === null) {
            return $this->nullDisplay;
        }

        return Purifier::purify($value);
    }
}