<?php

namespace backend\bootstrap;

use backend\src\helpers\WebSocketContainer;
use backend\widgets\pagination\Pagination;
use yii\base\BootstrapInterface;
use yii\rbac\CheckAccessInterface;
use yii\widgets\LinkPager;

class SetUp implements BootstrapInterface
{
    public function bootstrap($app)
    {
        $container = \Yii::$container;
        $container->set(LinkPager::class, Pagination::class);
        $container->setSingleton(CheckAccessInterface::class, static function () use ($app) {
            return $app->authManager;
        });
        $container->setSingleton(WebSocketContainer::class, WebSocketContainer::class);
    }
}