PNotify.defaults.styling = 'angeler';
PNotify.defaults.icons = 'angeler';
// PNotify.defaults.addClass = 'angeler-extended';
if (typeof window.stackPaginate === 'undefined') {
    window.stackPaginate = new PNotify.Stack({
        dir1: 'down',
        dir2: 'left',
        firstpos1: 25,
        firstpos2: 25,
        modal: false
    });
}

function createNotify (title, message, type) {
    if (type === 'warning') {
        type = 'notice';
    }
    PNotify.alert({
        title: title,
        text: message,
        stack: window.stackPaginate,
        type: type,
        destroy: true,
        icon: true,
        modules: new Map([
            ...PNotify.defaultModules,
            [PNotifyPaginate, {}],
        ]),
        delay: 2000,
        mouse_reset: false,
        textTrusted: true
    });
}

function createNotifyByObject(obj)
{
    if (obj.type === 'warning') {
        obj.type = 'notice';
    }
    let options = {
        stack: window.stackPaginate,
        destroy: true,
        icon: true,
        modules: new Map([
            ...PNotify.defaultModules,
            [PNotifyPaginate, {}],
        ]),
        delay: 2000,
        mouse_reset: false,
        textTrusted: true
    };
    options = $.extend(true, options, obj);
    PNotify.alert(options);
}

function createDesktopNotify(id, title, message, type, desktopMessage)
{
    PNotify.alert({
        title: title,
        text: message,
        type: type,
        destroy: true,
        modules: new Map([
            ...PNotify.defaultModules,
            [PNotifyDesktop, {
                fallback: true,
                text: desktopMessage,
                tag: 'notification-popup-showed-id-' + id
            }],
        ]),
        delay: 4000,
        mouse_reset: false,
        textTrusted: true
    });
}

function addLoanApplication(elemId, id, btnHtml, data)
{
    let tbody = $('#'+elemId).find('tbody');

    let existsRow = tbody.find('tr');
    if (existsRow.length === 1 && existsRow.find('td').length === 1) {
        existsRow.remove();
    }

    let tr = $('<tr />');
    tr.attr('data-key', id);
    let btnTd = $('<td />');
    btnTd.html(btnHtml);
    tr.append(btnTd);
    for(key in data) {
        let td = $('<td />');
        td.text(data[key]);
        tr.append(td);
    }
    tbody.append(tr);
}

function removeLoanApplication(elemId, applicationId)
{
    $('#'+elemId).find('tr[data-key="'+applicationId+'"]').remove();
}

function soundNotification(fileName = 'button_tiny', volume = 0.3) {
    let audio = new Audio('/js/sounds/' + fileName + '.mp3');
    audio.volume = volume;
    // audio.play();
}

function notificationShow(element) {
    let url = '/users-notifications/view-notification?id=' + $(element).data('id');
    let title = $(element).data('title');
    let modal = $('#modal-lg');
    $.get(url,
        function (data) {
            modal.find('.modal-title').html(title);
            modal.find('.modal-body').html(data);
            modal.modal();
        }
    );
    return false;
}

function notificationInit(data) {
    let command = null;

    try {
        command = data['command'];
    } catch (error) {
        console.error('Invalid data command');
        console.error(error);
        return;
    }

    if (command === 'add') {
        try {
            notificationAddMessage(
                data['id'],
                data['title'],
                data['time'],
                data['message'],
                data['type'],
                data['popup'],
                data['notifyMessage'],
                data['notifyDesktopMessage']
            );
        } catch (error) {
            console.error('Invalid data for command = add');
            console.error(error);
            return;
        }
    } else if (command === 'delete') {
        try {
            notificationDeleteMessage(data['id']);
        } catch (error) {
            console.error('Invalid data for command = delete');
            console.error(error);
            return;
        }
    } else if (command === 'delete_all') {
        notificationDeleteAllMessages();
    }

    notificationUpdateTime();
}

function notificationAddMessage(id, title, time, message, type, popup, notifyMessage, notifyDesktopMessage) {
    // if (notificationIsExist(id)) {
    //     console.error('Message Id: ' + id + ' already exist on UI list');
    //     return;
    // }

    let text = '<li data-id="' + id + '"> '
        + '<a href="javascript:;" onclick="notificationShow(this);" id="notification-menu-element-show" data-title="' + title + '" data-id="' + id + '">'
        + '<span class="glyphicon glyphicon-info-sign"> </span> '
        + '<span>'
        + '<span>' + title + '</span>'
        + '<span class="time" data-time="' + time + '">' + time + '</span>'
        + '</span>'
        + '<span class="message">' + message +'<br></span>'
        + '</a>'
        + '</li>';

    $("#notification-menu").prepend(text);
    notificationCounterIncrement();
    $( "#notification-menu li").each(function(e) {
        //remove 10th  element
        if (e === 10) {
            let messageId = $(this).data('id');
            if (messageId) {
                $(this).remove();
            }
        }
    });
    if (popup) {
        notificationPNotify(type, title, notifyMessage, notifyDesktopMessage);
    }
}

function notificationUpdateTime() {
    $( "#notification-menu li .time").each(function() {
        $(this).text(notificationTimeDifference(new Date(), new Date($(this).data('time') * 1000)));
    });
}

function notificationCounterIncrement() {
    $(".notification-counter").each(function() {
        let count = $(this).text();
        if (count) {
            count = parseInt(count);
            count++;
        } else {
            count = 1;
        }
        $(".notification-counter").text(count);
        return false;
    });
}

function notificationPNotify(type, title, message) {
    createNotify(title, message, type);
    if (document.visibilityState === 'visible') {
        soundNotification();
    }
}

function notificationTimeDifference(current, previous) {
    let msPerMinute = 60 * 1000;
    let msPerHour = msPerMinute * 60;
    let msPerDay = msPerHour * 24;
    let msPerMonth = msPerDay * 30;
    let msPerYear = msPerDay * 365;

    let elapsed = current - previous;

    if (elapsed < msPerMinute) {
        return Math.round(elapsed/1000) + ' seconds ago';
    } else if (elapsed < msPerHour) {
        return Math.round(elapsed/msPerMinute) + ' minutes ago';
    } else if (elapsed < msPerDay ) {
        return Math.round(elapsed/msPerHour ) + ' hours ago';
    } else if (elapsed < msPerMonth) {
        return Math.round(elapsed/msPerDay) + ' days ago';
    } else if (elapsed < msPerYear) {
        return Math.round(elapsed/msPerMonth) + ' months ago';
    } else {
        return Math.round(elapsed/msPerYear ) + ' years ago';
    }
}

function notificationDeleteMessage(id) {
    let isDeleted = false;
    let notif = $('#notification-menu li[data-id="'+id+'"]');
    if (notif.length) {
        let messageId = notif.data('id');
        if (messageId && messageId === id) {
            isDeleted = true;
            notif.remove();
            notificationCounterDecrement();
            return false;
        }
    }
    if (!isDeleted) {
        console.error('Message Id: ' + id + ' not found');
    }
}

function notificationCounterDecrement() {
    $(".notification-counter").each(function() {
        let count = $(this).text();
        if (count) {
            count = parseInt(count);
            if (count > 1) {
                count--;
            } else {
                count = '';
            }
        } else {
            count = '';
        }
        $(".notification-counter").text(count);
        return false;
    });
}

function notificationDeleteAllMessages() {
    let isDeleted = false;
    $( "#notification-menu li").each(function(e) {
        let messageId = $(this).data('id');
        if (messageId) {
            isDeleted = true;
            $(this).remove();
        }
    });
    notificationCounterReset();
}

function notificationCounterReset() {
    $(".notification-counter").text('');
}

$(document).on('pjax:complete', function(event, state, type, obj) {
    if (obj.push === true) {
        window.history.pushState({}, null, obj.requestUrl);
    }
});