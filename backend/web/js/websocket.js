function wsInitConnect(wsUrl, reconnectInterval, userId, onlineObj)
{
    try {
        //const socket = new WebSocket(wsUrl);
        var socket = new ReconnectingWebSocket(wsUrl, null, {debug: false, reconnectInterval: reconnectInterval});
        window.socket = socket;

        socket.onopen = function (e) {
            console.info('Socket Status: ' + socket.readyState + ' (Open)');
            onlineObj.attr('title', 'Online Connection: opened').find('i').removeClass('danger').addClass('warning');
        };

        socket.onmessage = function (e) {
            // onlineObj.find('i').removeClass('danger').removeClass('success').addClass('warning');
            console.info('socket.onmessage');
            try {
                var obj = JSON.parse(e.data); // $.parseJSON( e.data );
                console.log(obj);
            } catch (error) {
                console.error('Invalid JSON data on socket.onmessage');
                console.error(e.data);
            }

            try {
                if (typeof obj.cmd !== 'undefined') {
                    if (obj.cmd === 'initConnection') {
                        onlineObj.attr('title', 'Online Connection: true').find('i').removeClass('warning').removeClass('danger').addClass('success');
                        // if (typeof obj.uc_id !== 'undefined') {
                        //     if (obj.uc_id > 0) {
                        //     } else {
                        //         onlineObj.attr('title', 'Timeout DB connection: restart service').find('i').removeClass('danger').removeClass('success').addClass('warning');
                        //     }
                        // }
                    }

                    if (obj.cmd === 'userNotInit') {
                        window.location.href = '/site/logout';
                    }

                    if (obj.cmd === 'add_loan_application') {
                        addLoanApplication('loanApplicationSent', obj.loanApplicationId, obj.btnHtml, obj.data);
                    }

                    if (obj.cmd === 'remove_loan_application') {
                        removeLoanApplication('loanApplicationSent', obj.loanApplicationId);
                    }

                    if (obj.cmd === 'notification') {
                        if (typeof obj.notification !== 'undefined') {
                            if (userId == obj.notification.userId) {
                                if (typeof notificationInit === 'undefined') {
                                    console.warn('not found notificationInit method');
                                } else {
                                    notificationInit(obj.notification);
                                }
                            } else {
                                console.error('connecting user Id not equal notification user Id');
                            }
                        } else {
                            updatePjaxNotify();
                        }
                    }
                }
                // onlineObj.find('i').removeClass('danger').removeClass('warning').addClass('success');
            } catch (error) {
                console.error('Error in functions - socket.onmessage');
                console.error(error);
            }

        };

        socket.onclose = function (event) {
            if (event.wasClean) {
                console.log('Connection closed success (Close)');
            } else {
                console.error('Disconnect (Error)'); // Example kill process of server
            }
            onlineObj.attr('title', 'Disconnect').find('i').removeClass('success').addClass('danger');
        };

        socket.onerror = function (event) {
            //if (socket.readyState == 1) {
            console.log('Socket error: ' + event.message);
            //}
            onlineObj.attr('title', 'Online Connection: false').find('i').removeClass('success').addClass('danger');
            window.socketConnectionId = null;
        };
    } catch (error) {
        onlineObj.attr('title', 'Online Connection: error').find('i').removeClass('success').addClass('danger');
        console.error(error);
        window.socketConnectionId = null;
    }
}