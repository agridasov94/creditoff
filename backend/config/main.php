<?php

use common\components\i18n\Formatter;
use common\src\entities\client\entity\Client;
use yii\bootstrap4\BootstrapAsset;
use lajax\translatemanager\Module;
use yii\i18n\DbMessageSource;
use common\src\entities\users\entity\Users;
use yii\log\DbTarget;
use yii\web\UserEvent;
use yii2mod\rbac\filters\AccessControl;

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => [
        'log',
        \backend\bootstrap\SetUp::class,
        'multiLanguage',
        'translatemanager',
    ],
    'modules' => [
        'translatemanager' => [
            'class' => Module::class,
            'layout' => '@backend/views/layouts/main',
            'as access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['root'],
                    ]
                ]
            ],
            'allowedIPs' => ['*']
        ],
        'rbac' => [
            'class' => \yii2mod\rbac\Module::class,
            'as access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['root'],
                    ]
                ]
            ],
        ],
        'gridview' => ['class' => \kartik\grid\Module::class]
    ],
    'language' => 'ro',
    'components' => [
        'request' => [
            'class' => \sharnhorst\yii2\multiLanguage\MultiLangRequest::class,
            'csrfParam' => '_csrf-backend',
            'baseUrl' => ''
        ],
        'user' => [
            'identityClass' => Users::class,
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true, 'domain' => $params['domain']],
            'on afterLogin' => static function (UserEvent $event) {
                Yii::$app->user->identity->loginHandler();
            },
            'on afterLogout' => static function (UserEvent $event) {
                $event->identity->logoutHandler();
            }
        ],
        'frontendUser' => [
            'class' => \yii\web\User::class,
            'identityClass' => Client::class,
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true, 'domain' => $params['domain'] ?? ''],
//            'on afterLogin' => static function (UserEvent $event) {
//                Yii::$app->frontendUser->identity->loginHandler();
//            },
//            'on afterLogout' => static function (UserEvent $event) {
//                $event->identity->logoutHandler();
//            },
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
            'cookieParams' => [
                'domain' => $params['domain'],
                'httpOnly' => true
            ]
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                'db-error' => [
                    'class' => DbTarget::class,
                    'levels' => ['error', 'warning'],
                    'except' => [
                        'yii\web\HttpException:404',
                        'yii\web\HttpException:403',
                        'yii\web\HttpException:400',
                    ],
                    'logVars' => [],
                ],
                'db-info' => [
                    'class' => DbTarget::class,
                    'levels' => ['info'],
                    'except' => [
                        'yii\web\HttpException:404',
                    ],
                    'logVars' => [],
                    'categories' => ['info\*', 'log\*']
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            "class" => \sharnhorst\yii2\multiLanguage\MultiLangUrlManager::class,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'baseUrl' => '/',
            'scriptUrl' => '/'
        ],
        'assetManager' => [
            'forceCopy' => false,
            'hashCallback' => static function ($path) {
                return hash('md4', $path);
            },
            'basePath' => '@webroot/assets',
            'baseUrl' => '/assets',
            'bundles' => [
                \yii\bootstrap\BootstrapAsset::class => [
                    'class' => BootstrapAsset::class,
                ]
            ]
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => DbMessageSource::class,
                    'db' => 'db',
                    'sourceLanguage' => 'xx-XX', // Developer language
                    'sourceMessageTable' => '{{%language_source}}',
                    'messageTable' => '{{%language_translate}}',
                    'cachingDuration' => 86400,
                    'enableCaching' => false,
                ],
            ],
        ],
        "multiLanguage" => [
            "class" => \sharnhorst\yii2\multiLanguage\MultiLangComponent::class,
            'langs' => ['ru', 'en', 'ro'],
            'default_lang' => 'ro',         //Language to which no language settings are added.
            'lang_param_name' => 'lang',
        ],
        'formatter' => [
            'class' => Formatter::class,
            'dateFormat' => 'php:d-M-Y', //'dd.MM.yyyy',
            'datetimeFormat' => 'php:d-M-Y [H:i]',
            'timeFormat' => 'php:H:i',
            //'decimalSeparator' => ',',
            //'thousandSeparator' => ' ',
            //'currencyCode' => 'USD',
        ],
        'authManager' => [
            'class' => \yii\rbac\DbManager::class,
            'cache' => 'cache'
        ],
        'crypt' => [
            'class' => \backend\components\CryptComponent::class,
            'method'    => 'aes-256-cbc',
            'password'  => '',
            'iv'        => '',
        ],
    ],
    'as access' => [
        'class' => \yii\filters\AccessControl::class,
        'except' => ['site/login', 'site/error'],
        'rules' => [
            [
                'allow' => true,
                'roles' => ['@'],
            ],
        ],
    ],
    'container' => [
        'definitions' => [
            yii\grid\GridView::class => [
                'options' => ['class' => 'table-responsive'],
            ],
        ],
    ],
    'params' => $params,
];
