<?php
return [
    'adminEmail' => 'admin@example.com',
    'frontendHost' => '',
    'domain' => '',
    'webSocketServer' => [
        'connectionUrl' => '',
    ],
    'backendHost' => '',
    'bsDependencyEnabled' => false,
];
