<?php

namespace backend\components;

class CryptComponent extends \yii\base\Component
{
    public $iv;
    public $password;
    public $method;

    public function encrypt(string $data, string $key): string
    {
        return openssl_encrypt($data, $this->method, $this->password . $key, 0, $this->iv) ?: '';
    }

    public function decrypt(string $data, string $key): string
    {
        return openssl_decrypt($data, $this->method, $this->password . $key, 0, $this->iv) ?: '';
    }
}
