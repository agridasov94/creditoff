<?php

namespace backend\components\grid;

use backend\widgets\UserSelect2Widget;
use common\src\entities\users\entity\Users;
use common\src\entities\users\entity\UsersQuery;
use yii\base\Model;
use yii\grid\DataColumn;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * Class UserSelect2Column
 *
 * @property int $userId
 * @property string $url
 * @property string $relation
 * @property int $minimumInputLength
 * @property int $delay
 * @property string $placeholder
 * @property array $data
 *
 * Ex.
[
'class' => \common\components\grid\UserSelect2Column::class,
'attribute' => 'ugs_updated_user_id',
'relation' => 'updatedUser',
'url' => 'employee/list-ajax',
],
 *
 */
class UserSelect2Column extends DataColumn
{
    public $format = 'userName';
    public $url;
    public $relation;
    public $useRelation = true;
    public $minimumInputLength = 1;
    public $delay = 300;
    public $placeholder = '';
    public $data = [];
    public $useAjax = false;
    public $dataCellValueAttribute;

    public function init(): void
    {
        parent::init();

        if ($this->useRelation && empty($this->relation)) {
            throw new \InvalidArgumentException('relation must be set.');
        }

        if (empty($this->url)) {
            $this->url = Url::to(['/users/list-ajax']);
        }
        $model = $this->grid->filterModel;
        if ($this->filter !== false && $model instanceof Model && $this->attribute !== null && $model->isAttributeActive($this->attribute)) {
            $userId = (int) $model->getAttribute($this->attribute);
            if ($userId) {
                $user = Users::find()->select(['id', 'username'])->where(['id' => $userId])->cache(3600)->one();
                if ($user) {
                    $this->data[$user->id] = $user->username . ' (' . $user->id . ')';
                }
            }
            if ($this->dataCellValueAttribute) {
                $this->label = $model->getAttributeLabel($this->dataCellValueAttribute);
            }
        }
        if (!$this->useAjax) {
            $this->data = UsersQuery::getList();
        }
    }

    /**
     * @param mixed $model
     * @param mixed $key
     * @param int $index
     * @return string|null
     */
    public function getDataCellValue($model, $key, $index)
    {
        if ($this->useRelation) {
            if ($model->{$this->attribute} && ($user = $model->{$this->relation})) {
                /** @var Users $user */
                return $user->username;
            }
        } else if (!$this->useRelation) {
            if ($this->dataCellValueAttribute) {
                if ($model->{$this->dataCellValueAttribute}) {
                    return $model->{$this->dataCellValueAttribute};
                }
            } else {
                if ($model->{$this->attribute}) {
                    return $model->{$this->attribute};
                }
            }
        }
        return null;
    }

    /**
     * @return array|false|string|null
     * @throws \Exception
     */
    protected function renderFilterCellContent()
    {
        if (is_string($this->filter)) {
            return $this->filter;
        }

        $widgetOptions = [
            'model' => $this->grid->filterModel,
            'attribute' => $this->attribute,
            'data' => $this->data,
            'options' => ['placeholder' => $this->placeholder],
            'useAjax' => $this->useAjax
        ];

        if ($this->useAjax) {
            $widgetOptions = ArrayHelper::merge([
                'pluginOptions' => [
                    'minimumInputLength' => $this->minimumInputLength,
                    'ajax' => [
                        'url' => $this->url,
                        'delay' => $this->delay
                    ],
                ],
            ], $widgetOptions);
        }

        return UserSelect2Widget::widget($widgetOptions);
    }
}
