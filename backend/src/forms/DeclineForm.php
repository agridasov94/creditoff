<?php

namespace backend\src\forms;

use Yii;

class DeclineForm extends \yii\base\Model
{
    public $loanApplicationId;
    public $reason;

    public function rules(): array
    {
        return [
            [['loanApplicationId', 'reason'], 'required'],
            [['loanApplicationId'], 'integer'],
            [['reason'], 'string', 'max' => 500]
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'loanApplicationId' => Yii::t('loan_application', 'Loan application id'),
            'reason' => Yii::t('loan_application', 'Reason')
        ];
    }
}