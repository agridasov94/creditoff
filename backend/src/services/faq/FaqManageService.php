<?php

namespace backend\src\services\faq;

use backend\src\repositories\faq\FaqRepository;
use backend\src\useCase\faq\create\FAQCreateForm;
use common\src\entities\faq\entity\Faq;
use common\src\entities\language\LanguageQuery;

/**
 * @property-read FaqRepository $repository
 */
class FaqManageService
{
    private FaqRepository $repository;

    public function __construct(FaqRepository $repository)
    {
        $this->repository = $repository;
    }

    public function create(FAQCreateForm $form): Faq
    {
        $faq = Faq::create($form->question, $form->answer, $form->langId);

        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $this->repository->save($faq);

            foreach (LanguageQuery::getListExcludingLang([$form->langId]) as $language => $langValue) {
                $form->langId = $language;
                $model = Faq::create($form->question, $form->answer, $language);
                $this->repository->save($model);
            }

            $transaction->commit();
        } catch (\RuntimeException $e) {
            $transaction->rollBack();
        } catch (\Throwable $e) {
            $transaction->rollBack();
        }

        return $faq;
    }
}