<?php

namespace backend\src\services\blog;

use backend\src\repositories\blog\BlogPostRepository;
use backend\src\useCase\blogCreate\BlogPostCreateForm;
use common\src\entities\blog\posts\entity\BlogPosts;
use common\src\entities\language\LanguageQuery;
use common\src\helpers\app\AppHelper;
use yii\helpers\FileHelper;

/**
 * @property-read  BlogPostRepository $blogPostRepository
 */
class BlogManageService
{
    private BlogPostRepository $blogPostRepository;

    public function __construct(BlogPostRepository $blogPostRepository)
    {
        $this->blogPostRepository = $blogPostRepository;
    }

    public function create(BlogPostCreateForm $form): void
    {
        $blogPost = BlogPosts::create($form);
        $blogPost->setPreviewImage($blogPost->bp_alias . '.' . $form->preview->extension);

        $imagesPathForRemoving = [];
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $this->blogPostRepository->save($blogPost);
            if ($form->preview) {
                FileHelper::createDirectory($blogPost->getPreviewFrontendPath());
                $form->preview->saveAs($blogPost->getPreviewFrontendImagePath(), false);
                FileHelper::createDirectory($blogPost->getPreviewBackendPath());
                if(!is_link($blogPost->getPreviewBackendImagePath())) {
                    symlink($blogPost->getPreviewFrontendImagePath(), $blogPost->getPreviewBackendImagePath());
                }
                $imagesPathForRemoving[] = $blogPost->getPreviewFrontendPath();
            }

            $blogPostId = $blogPost->bp_id;

            foreach (LanguageQuery::getListExcludingLang([$form->langId]) as $language => $langValue) {
                $form->langId = $language;
                $blogPost = BlogPosts::create($form);
                $blogPost->bp_id = $blogPostId;
                $blogPost->setPreviewImage($blogPost->bp_alias . '.' . $form->preview->extension);
                $this->blogPostRepository->save($blogPost);
                if ($form->preview) {
                    FileHelper::createDirectory($blogPost->getPreviewFrontendPath());
                    $form->preview->saveAs($blogPost->getPreviewFrontendImagePath(), false);
                    FileHelper::createDirectory($blogPost->getPreviewBackendPath());
                    if(!is_link($blogPost->getPreviewBackendImagePath())) {
                        symlink($blogPost->getPreviewFrontendImagePath(), $blogPost->getPreviewBackendImagePath());
                    }
                    $imagesPathForRemoving[] = $blogPost->getPreviewFrontendPath();
                }
            }
            if (file_exists($form->preview->tempName)) {
                unlink($form->preview->tempName);
            }
            $transaction->commit();
        } catch (\RuntimeException $e) {
            $transaction->rollBack();
            $form->addError('general', $e->getMessage());
            \Yii::error(AppHelper::throwableFormatter($e), 'BlogManageService::create::RuntimeException');
            foreach ($imagesPathForRemoving as $value) {
                FileHelper::removeDirectory($value);
            }
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $form->addError('general', 'Internal Server Error');
            \Yii::error(AppHelper::throwableLog($e, true), 'BlogManageService::create::Throwable');
            foreach ($imagesPathForRemoving as $value) {
                FileHelper::removeDirectory($value);
            }
        }
    }

    public function update(BlogPosts $blogPosts): void
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            if ($blogPosts->save() && $blogPosts->preview) {
                FileHelper::createDirectory($blogPosts->getPreviewFrontendPath());
                if (file_exists($blogPosts->getPreviewFrontendImagePath())) {
                    unlink($blogPosts->getPreviewFrontendImagePath());
                }
                $blogPosts->preview->saveAs($blogPosts->getPreviewFrontendImagePath());
                FileHelper::createDirectory($blogPosts->getPreviewBackendPath());
                if (file_exists($blogPosts->getPreviewBackendImagePath())) {
                    unlink($blogPosts->getPreviewBackendImagePath());
                }
                if(!is_link($blogPosts->getPreviewBackendImagePath())) {
                    symlink($blogPosts->getPreviewFrontendImagePath(), $blogPosts->getPreviewBackendImagePath());
                }
            }


            $transaction->commit();
        } catch (\RuntimeException $e) {
            $transaction->rollBack();
            $blogPosts->addError('general', $e->getMessage());
            \Yii::error(AppHelper::throwableLog($e, true), 'BlogManageService::update::Throwable');
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $blogPosts->addError('general', 'Internal Server Error');
            \Yii::error(AppHelper::throwableLog($e, true), 'BlogManageService::update::Throwable');
        }
    }
}