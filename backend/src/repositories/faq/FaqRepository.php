<?php

namespace backend\src\repositories\faq;

use common\src\entities\faq\entity\Faq;

class FaqRepository
{
    public function save(Faq $model): array
    {
        if (!$model->save()) {
            throw new \RuntimeException($model->getErrorSummary(true)[0]);
        }
        return $model->getPrimaryKey(true);
    }
}