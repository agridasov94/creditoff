<?php

namespace backend\src\repositories\blog;

use common\src\entities\blog\posts\entity\BlogPosts;

class BlogPostRepository
{
    public function save(BlogPosts $blogPosts, bool $runValidation = true): int
    {
        if (!$blogPosts->save($runValidation)) {
            throw new \RuntimeException($blogPosts->getErrorSummary(true)[0]);
        }
        return $blogPosts->bp_id;
    }
}
