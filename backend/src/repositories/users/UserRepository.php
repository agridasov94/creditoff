<?php

namespace backend\src\repositories\users;

use common\src\entities\users\entity\Users;

class UserRepository
{
    public function save(Users $model): int
    {
        if (!$model->save()) {
            throw new \RuntimeException('Users saving failed: ' . $model->getErrorSummary(true)[0]);
        }
        return $model->id;
    }
}