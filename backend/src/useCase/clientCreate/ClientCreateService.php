<?php

namespace backend\src\useCase\clientCreate;

use common\src\entities\client\email\entity\ClientEmail;
use common\src\entities\client\email\entity\ClientEmailRepository;
use common\src\entities\client\employmentData\entity\ClientEmploymentData;
use common\src\entities\client\employmentData\entity\ClientEmploymentDataRepository;
use common\src\entities\client\entity\Client;
use common\src\entities\client\entity\ClientRepository;
use common\src\entities\client\passportData\entity\ClientPassportData;
use common\src\entities\client\passportData\entity\ClientPassportDataRepository;
use common\src\entities\client\phone\entity\ClientPhone;
use common\src\entities\client\phone\entity\ClientPhoneRepository;
use common\src\helpers\app\AppHelper;

/**
 * @property-read ClientRepository $clientRepository
 * @property-read ClientEmploymentDataRepository $clientEmploymentDataRepository
 * @property-read ClientPassportDataRepository $clientPassportDataRepository
 * @property-read ClientEmailRepository $clientEmailRepository
 * @property-read ClientPhoneRepository $clientPhoneRepository
 */
class ClientCreateService
{
    private ClientRepository $clientRepository;
    private ClientEmploymentDataRepository $clientEmploymentDataRepository;
    private ClientPassportDataRepository $clientPassportDataRepository;
    private ClientEmailRepository $clientEmailRepository;
    private ClientPhoneRepository $clientPhoneRepository;

    public function __construct(
        ClientRepository $clientRepository,
        ClientEmploymentDataRepository $clientEmploymentDataRepository,
        ClientPassportDataRepository $clientPassportDataRepository,
        ClientEmailRepository $clientEmailRepository,
        ClientPhoneRepository $clientPhoneRepository
    ) {
        $this->clientRepository = $clientRepository;
        $this->clientEmploymentDataRepository = $clientEmploymentDataRepository;
        $this->clientPassportDataRepository = $clientPassportDataRepository;
        $this->clientEmailRepository = $clientEmailRepository;
        $this->clientPhoneRepository = $clientPhoneRepository;
    }

    /**
     * @throws \yii\db\Exception
     * @throws \Throwable|\RuntimeException
     */
    public function create(ClientCreateForm $form): Client
    {
        $client = Client::create($form);
        $employmentData = [];
        foreach ($form->employmentData as $employmentDataForm) {
            $employmentData[] = ClientEmploymentData::create($employmentDataForm);
        }
        $passportDataModel = ClientPassportData::create($form->passportData);
        $emailModels = [];
        foreach ($form->emails as $emailForm) {
            if (!empty($emailForm->email)) {
                $emailModels[] = ClientEmail::create($emailForm->email);
            }
        }

        $phoneModels = [];
        foreach ($form->phones as $phoneForm) {
            if (!empty($phoneForm->phone)) {
                $phoneModels[] = ClientPhone::create($phoneForm->phone);
            }
        }

        $transaction = \Yii::$app->db->beginTransaction();

        try {
            $clientId = $this->clientRepository->save($client);

            foreach ($employmentData as $employmentDataModel) {
                $employmentDataModel->ced_client_id = $clientId;
                $this->clientEmploymentDataRepository->save($employmentDataModel);
            }

            $passportDataModel->cpd_client_id = $clientId;
            $this->clientPassportDataRepository->save($passportDataModel);

            foreach ($emailModels as $emailModel) {
                $emailModel->ce_client_id = $clientId;
                $this->clientEmailRepository->save($emailModel);
            }

            foreach ($phoneModels as $phoneModel) {
                $phoneModel->cp_client_id = $clientId;
                $this->clientPhoneRepository->save($phoneModel);
            }

            $transaction->commit();

            return $client;
        } catch (\RuntimeException $e) {
            $transaction->rollBack();
            \Yii::error(AppHelper::throwableLog($e), 'ClientCreateService::create::RuntimeException');
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            \Yii::error('Internal Server Error', 'ClientCreateService::create::Throwable');
            throw $e;
        }
    }
}