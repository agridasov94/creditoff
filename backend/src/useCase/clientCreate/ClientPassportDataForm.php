<?php

namespace backend\src\useCase\clientCreate;

use Yii;
use yii\base\Model;

class ClientPassportDataForm extends Model
{
    public $type;
    public $series;
    public $number;
    public $issuedBy;
    public $issueDate;
    public $issueDepartmentCode;
    public $expirationDate;
    public $registeredAddress;
    public $residenceAddress;

    public function rules(): array
    {
        return [
            [['type'], 'integer'],
            [['issueDate', 'expirationDate'], 'safe'],
            [['series', 'number', 'registeredAddress', 'residenceAddress'], 'string', 'max' => 100],
            [['issuedBy', 'issueDepartmentCode'], 'string', 'max' => 50],
            [['issueDate', 'expirationDate'], 'date', 'format' => 'php:d-m-Y']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'client_id' => Yii::t('client', 'Client ID'),
            'type' => Yii::t('client', 'Type'),
            'series' => Yii::t('client', 'Series'),
            'number' => Yii::t('client', 'Number'),
            'issuedBy' => Yii::t('client', 'Issued By'),
            'issueDate' => Yii::t('client', 'Issue Date'),
            'issueDepartmentCode' => Yii::t('client', 'Issue Department Code'),
            'expirationDate' => Yii::t('client', 'Expiration Date'),
            'registeredAddress' => Yii::t('client', 'Registered Address'),
            'residenceAddress' => Yii::t('client', 'Residence Address'),
        ];
    }

    public static function getFormName(): string
    {
        return 'ClientPassportDataForm';
    }
}