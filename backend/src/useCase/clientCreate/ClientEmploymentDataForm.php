<?php

namespace backend\src\useCase\clientCreate;

use common\src\entities\client\entity\Client;
use Yii;
use yii\base\Model;

class ClientEmploymentDataForm extends Model
{
    public $type;
    public $workPhone;
    public $organizationName;
    public $position;
    public $seniority;
    public $officialWages;
    public $unofficialWages;
    public $revenuesDocumented;
    public $revenuesNonDocumented;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type'], 'integer'],
            [['workPhone'], 'string', 'max' => 30],
            [['organizationName', 'position'], 'string', 'max' => 100],
            [['seniority'], 'string', 'max' => 10],
            [['officialWages', 'unofficialWages'], 'string', 'max' => 50],
            [['revenuesDocumented', 'revenuesNonDocumented'], 'string', 'max' => 255],
        ];
    }

    public static function getFormName(): string
    {
        return 'ClientEmploymentDataForm';
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('client', 'ID'),
            'type' => Yii::t('client', 'Type'),
            'workPhone' => Yii::t('client', 'Work Phone'),
            'organizationName' => Yii::t('client', 'Organization Name'),
            'position' => Yii::t('client', 'Position'),
            'seniority' => Yii::t('client', 'Seniority'),
            'officialWages' => Yii::t('client', 'Official Wages'),
            'unofficialWages' => Yii::t('client', 'Unofficial Wages'),
            'revenuesDocumented' => Yii::t('client', 'Revenues Documented'),
            'revenuesNonDocumented' => Yii::t('client', 'Revenues Non Documented'),
        ];
    }
}