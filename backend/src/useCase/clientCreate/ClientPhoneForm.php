<?php

namespace backend\src\useCase\clientCreate;

use borales\extensions\phoneInput\PhoneInputBehavior;
use borales\extensions\phoneInput\PhoneInputValidator;
use yii\base\Model;

class ClientPhoneForm extends Model
{
    public $phone;
    public $code = '+373';

    public function behaviors()
    {
        return [
            'phoneInput' => PhoneInputBehavior::class,
        ];
    }

    public function rules(): array
    {
        return [
            [['phone'], 'string', 'max' => 9, 'min' => 9, 'message' => \Yii::t('frontend', 'The phone number must be in the format 0XXXXXXXX and start with 07 or 06')],
            [['phone'], 'match', 'pattern' => '/^(07|06)([0-9]){7}$/', 'message' => \Yii::t('frontend', 'The phone number must be in the format 0XXXXXXXX and start with 07 or 06')],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'phone' => \Yii::t('client', 'Phone')
        ];
    }

    public static function getFormName(): string
    {
        return 'ClientPhoneForm';
    }
}