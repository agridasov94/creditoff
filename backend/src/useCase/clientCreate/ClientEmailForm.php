<?php

namespace backend\src\useCase\clientCreate;

use yii\base\Model;

class ClientEmailForm extends Model
{
    public $email;

    public function rules()
    {
        return [
          [['email'], 'email', 'skipOnEmpty' => true]
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'email' => \Yii::t('client', 'Email')
        ];
    }

    public static function getFormName(): string
    {
        return 'ClientEmailForm';
    }
}