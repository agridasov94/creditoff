<?php

namespace backend\src\useCase\clientCreate;

use common\src\entities\client\entity\Client;
use elisdn\compositeForm\CompositeForm;

/**
 * @property ChildrenForm[] $children
 * @property ClientEmploymentDataForm[] $employmentData
 * @property ClientPassportDataForm $passportData
 * @property ClientEmailForm[] $emails
 * @property ClientPhoneForm[] $phones
 */
class ClientCreateForm extends CompositeForm
{
    public $name;
    public $lastName;
    public $dateOfBirth;
    public $idnp;
    public $gender;
    public $civilStatus;
    public $socialStatus;
    public $ownProperty;
    public $isBlocked;

    public function __construct(
        int $countChildrenForm = 1,
        int $countEmploymentData = 0,
        int $countEmails = 0,
        int $countPhones = 0,
        $config = []
    ) {
        $children = [];
        for ($i = 0; $i < $countChildrenForm; $i++) {
            $children[] = new ChildrenForm();
        }
        $this->children = $children;
        $employmentData = [];
        for ($i = 0; $i < $countEmploymentData; $i++) {
            $employmentData[] = new ClientEmploymentDataForm();
        }
        $this->employmentData = $employmentData;
        $this->passportData = new ClientPassportDataForm();

        $emails = [];
        for ($i = 0; $i < $countEmails; $i++) {
            $emails[] = new ClientEmailForm();
        }
        $this->emails = $emails;
        $phones = [];
        for ($i = 0; $i < $countPhones; $i++) {
            $phones[] = new ClientPhoneForm();
        }
        $this->phones = $phones;

        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['name', 'lastName', 'dateOfBirth', 'idnp', 'gender'], 'required'],
            [['name', 'lastName'], 'string', 'max' => 70],
            [['dateOfBirth'], 'date', 'format' => 'php:d-m-Y'],
            [['idnp'], 'string', 'max' => 13],
            [['gender', 'civilStatus', 'socialStatus'], 'integer'],
            [['gender'], 'in', 'range' => array_keys(Client::getGenderListName())],
            [['civilStatus'], 'in', 'range' => array_keys(Client::getCivilStatusListName())],
            [['socialStatus'], 'in', 'range' => array_keys(Client::getSocialStatusListName())],
            [['ownProperty'], 'boolean'],
            [['isBlocked'], 'boolean'],
        ];
    }

    protected function internalForms(): array
    {
        return ['children', 'employmentData', 'passportData', 'emails', 'phones'];
    }
}