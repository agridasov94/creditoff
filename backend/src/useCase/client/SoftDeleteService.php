<?php

namespace backend\src\useCase\client;

use backend\components\CryptComponent;
use common\src\entities\client\email\entity\ClientEmail;
use common\src\entities\client\email\entity\ClientEmailRepository;
use common\src\entities\client\employmentData\entity\ClientEmploymentData;
use common\src\entities\client\employmentData\entity\ClientEmploymentDataRepository;
use common\src\entities\client\entity\Client;
use common\src\entities\client\entity\ClientRepository;
use common\src\entities\client\passportData\entity\ClientPassportDataRepository;
use common\src\entities\client\phone\entity\ClientPhone;
use common\src\entities\client\phone\entity\ClientPhoneRepository;
use common\src\helpers\app\AppHelper;

class SoftDeleteService
{
    private CryptComponent $crypt;
    private ClientRepository $clientRepository;
    private ClientEmailRepository $clientEmailRepository;
    private ClientPhoneRepository $clientPhoneRepository;
    private ClientEmploymentDataRepository $clientEmploymentDataRepository;
    private ClientPassportDataRepository $clientPassportDataRepository;

    private string $errorMessage;

    public function __construct(
        ClientRepository $clientRepository,
        ClientEmailRepository $clientEmailRepository,
        ClientPhoneRepository $clientPhoneRepository,
        ClientEmploymentDataRepository $clientEmploymentDataRepository,
        ClientPassportDataRepository $clientPassportDataRepository
    ) {
        $this->crypt = \Yii::$app->crypt;
        $this->clientRepository = $clientRepository;
        $this->clientEmailRepository = $clientEmailRepository;
        $this->clientPhoneRepository = $clientPhoneRepository;
        $this->clientEmploymentDataRepository = $clientEmploymentDataRepository;
        $this->clientPassportDataRepository = $clientPassportDataRepository;
    }

    public function encryptData(Client $client): bool
    {
        $client->setScenario(Client::SCENARIO_ENCRYPT);
        $client->c_name = $this->crypt->encrypt($client->c_name, $client->c_uid);
        $client->c_last_name = $this->crypt->encrypt($client->c_last_name, $client->c_uid);
        $client->c_idnp = $this->crypt->encrypt($client->c_idnp, $client->c_uid);
        $client->c_deleted_dt = date('Y-m-d H:i:s');

        $clientEmails = $client->clientEmails;
        $clientPhones = $client->clientPhones;
        $clientEmploymentData = $client->clientEmploymentData;
        $clientPassportData = $client->clientPassportData;

        $transaction = \Yii::$app->db->beginTransaction();
        try {

            $this->clientRepository->save($client);

            foreach ($clientEmails ?? [] as $clientEmail) {
                $clientEmail->setScenario(ClientEmail::SCENARIO_ENCRYPT);
                $clientEmail->ce_email = $this->crypt->encrypt($clientEmail->ce_email, $client->c_uid);
                $this->clientEmailRepository->save($clientEmail);
            }

            foreach ($clientPhones ?? [] as $clientPhone) {
                $clientPhone->setScenario(ClientPhone::SCENARIO_ENCRYPT);
                $clientPhone->cp_phone = $this->crypt->encrypt($clientPhone->cp_phone, $client->c_uid);
                $this->clientPhoneRepository->save($clientPhone);
            }

            foreach ($clientEmploymentData ?? [] as $employmentData) {
                $employmentData->setScenario(ClientEmploymentData::SCENARIO_ENCRYPT);
                $employmentData->ced_work_phone = $this->crypt->encrypt($employmentData->ced_work_phone, $client->c_uid);
                $employmentData->ced_organization_name = $this->crypt->encrypt($employmentData->ced_organization_name, $client->c_uid);
                $employmentData->ced_position = $this->crypt->encrypt($employmentData->ced_position, $client->c_uid);
                $employmentData->ced_seniority = $this->crypt->encrypt($employmentData->ced_seniority, $client->c_uid);
                $employmentData->ced_official_wages = $this->crypt->encrypt($employmentData->ced_official_wages, $client->c_uid);
                $employmentData->ced_unofficial_wages = $this->crypt->encrypt($employmentData->ced_unofficial_wages, $client->c_uid);
                $employmentData->ced_revenues_documented = $this->crypt->encrypt($employmentData->ced_revenues_documented, $client->c_uid);
                $employmentData->ced_revenues_non_documented = $this->crypt->encrypt($employmentData->ced_revenues_non_documented, $client->c_uid);
                $this->clientEmploymentDataRepository->save($employmentData);
            }

            if ($clientPassportData) {
                $clientPassportData->cpd_series = $this->crypt->encrypt($clientPassportData->cpd_series, $client->c_uid);
                $clientPassportData->cpd_number = $this->crypt->encrypt($clientPassportData->cpd_number, $client->c_uid);
                $clientPassportData->cpd_issued_by = $this->crypt->encrypt($clientPassportData->cpd_issued_by, $client->c_uid);
                $clientPassportData->cpd_issue_department_code = $this->crypt->encrypt($clientPassportData->cpd_issue_department_code, $client->c_uid);
                $clientPassportData->cpd_registered_address = $this->crypt->encrypt($clientPassportData->cpd_registered_address, $client->c_uid);
                $clientPassportData->cpd_residence_address = $this->crypt->encrypt($clientPassportData->cpd_residence_address, $client->c_uid);
                $this->clientPassportDataRepository->save($clientPassportData);
            }

            $transaction->commit();
            return true;
        } catch (\RuntimeException $e) {
            $transaction->rollBack();
            $this->setErrorMessage($e->getMessage());
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $this->setErrorMessage(\Yii::t('backend', 'Internal Server Error'));
            \Yii::error(AppHelper::throwableLog($e, true), 'backend::SoftDeleteService::encrypt');
        }
        return false;
    }

    public function decryptData(Client $client): bool
    {
        $client->c_name = $this->crypt->decrypt($client->c_name, $client->c_uid);
        $client->c_last_name = $this->crypt->decrypt($client->c_last_name, $client->c_uid);
        $client->c_idnp = $this->crypt->decrypt($client->c_idnp, $client->c_uid);
        $client->c_deleted_dt = null;

        $clientEmails = $client->clientEmails;
        $clientPhones = $client->clientPhones;
        $clientEmploymentData = $client->clientEmploymentData;
        $clientPassportData = $client->clientPassportData;

        $transaction = \Yii::$app->db->beginTransaction();
        try {

            $this->clientRepository->save($client);

            foreach ($clientEmails ?? [] as $clientEmail) {
                $clientEmail->ce_email = $this->crypt->decrypt($clientEmail->ce_email, $client->c_uid);
                $this->clientEmailRepository->save($clientEmail);
            }

            foreach ($clientPhones ?? [] as $clientPhone) {
                $clientPhone->cp_phone = $this->crypt->decrypt($clientPhone->cp_phone, $client->c_uid);
                $this->clientPhoneRepository->save($clientPhone);
            }

            foreach ($clientEmploymentData ?? [] as $employmentData) {
                $employmentData->ced_work_phone = $this->crypt->decrypt($employmentData->ced_work_phone, $client->c_uid);
                $employmentData->ced_organization_name = $this->crypt->decrypt($employmentData->ced_organization_name, $client->c_uid);
                $employmentData->ced_position = $this->crypt->decrypt($employmentData->ced_position, $client->c_uid);
                $employmentData->ced_seniority = $this->crypt->decrypt($employmentData->ced_seniority, $client->c_uid);
                $employmentData->ced_official_wages = $this->crypt->decrypt($employmentData->ced_official_wages, $client->c_uid);
                $employmentData->ced_unofficial_wages = $this->crypt->decrypt($employmentData->ced_unofficial_wages, $client->c_uid);
                $employmentData->ced_revenues_documented = $this->crypt->decrypt($employmentData->ced_revenues_documented, $client->c_uid);
                $employmentData->ced_revenues_non_documented = $this->crypt->decrypt($employmentData->ced_revenues_non_documented, $client->c_uid);
                $this->clientEmploymentDataRepository->save($employmentData);
            }

            if ($clientPassportData) {
                $clientPassportData->cpd_series = $this->crypt->decrypt($clientPassportData->cpd_series, $client->c_uid);
                $clientPassportData->cpd_number = $this->crypt->decrypt($clientPassportData->cpd_number, $client->c_uid);
                $clientPassportData->cpd_issued_by = $this->crypt->decrypt($clientPassportData->cpd_issued_by, $client->c_uid);
                $clientPassportData->cpd_issue_department_code = $this->crypt->decrypt($clientPassportData->cpd_issue_department_code, $client->c_uid);
                $clientPassportData->cpd_registered_address = $this->crypt->decrypt($clientPassportData->cpd_registered_address, $client->c_uid);
                $clientPassportData->cpd_residence_address = $this->crypt->decrypt($clientPassportData->cpd_residence_address, $client->c_uid);
                $this->clientPassportDataRepository->save($clientPassportData);
            }

            $transaction->commit();
            return true;
        } catch (\RuntimeException $e) {
            $transaction->rollBack();
            $this->setErrorMessage($e->getMessage());
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $this->setErrorMessage(\Yii::t('backend', 'Internal Server Error'));
            \Yii::error(AppHelper::throwableLog($e, true), 'backend::SoftDeleteService::decrypt');
        }
        return false;
    }

    public function getErrorMessage(): string
    {
        return $this->errorMessage;
    }

    protected function setErrorMessage(string $message): void
    {
        $this->errorMessage = $message;
    }
}
