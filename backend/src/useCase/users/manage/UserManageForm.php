<?php

namespace backend\src\useCase\users\manage;

use backend\src\entities\partnerCompany\entity\PartnerCompany;
use backend\src\entities\partnerCompany\position\entity\PartnerCompanyPosition;
use borales\extensions\phoneInput\PhoneInputValidator;
use common\src\entities\product\entity\ProductQuery;
use common\src\entities\users\entity\Users;
use Yii;
use yii\base\Model;
use yii\helpers\VarDumper;

class UserManageForm extends Model
{
    public const SCENARIO_UPDATE = 'update';
    public const SCENARIO_UPDATE_MY_PROFILE = 'update_my_profile';

    public $id;
    public $username;
    public $firstName;
    public $lastName;
    public $password;
    public $rePassword;
    public $email;
    public $phone;
    public $status;
    public $partnerCompanyId;
    public $partnerCompanyPositionId;
    public $roles;
    public $products;

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_UPDATE] = ['id', 'username', 'firstName', 'lastName', 'email',
            'phone', 'status', 'partnerCompanyId', 'password', 'rePassword',
            'partnerCompanyId', 'partnerCompanyPositionId', 'roles', 'products'];
        $scenarios[self::SCENARIO_UPDATE_MY_PROFILE] = ['id', 'username', 'firstName', 'lastName', 'email',
            'phone', 'status', 'partnerCompanyId', 'password', 'rePassword',
            'partnerCompanyId', 'partnerCompanyPositionId', 'roles', 'products'];
        return $scenarios;
    }

    public function rules(): array
    {
        return [
            [['id'], 'integer'],
            [['id'], 'required', 'on' => self::SCENARIO_UPDATE],
            [['username', 'firstName', 'lastName', 'email',
                'phone', 'roles'], 'required', 'on' => [self::SCENARIO_DEFAULT, self::SCENARIO_UPDATE]],
            [['password', 'rePassword'], 'required', 'on' => self::SCENARIO_DEFAULT],
            [['rePassword'], 'required', 'on' => self::SCENARIO_UPDATE_MY_PROFILE, 'when' => function () {
                return !empty($this->password);
            }],
            [['email', 'phone'], 'required', 'on' => self::SCENARIO_UPDATE_MY_PROFILE],
            [['username', 'email', 'firstName', 'lastName', 'phone'], 'trim'],
            [['username', 'email'], 'string', 'max' => 255],
            ['username', 'unique', 'targetClass' => Users::class, 'filter' => ['<>', 'id', $this->id], 'on' => self::SCENARIO_UPDATE, 'message' => Yii::t('users', 'This username has already been taken.')],
            ['username', 'unique', 'targetClass' => Users::class, 'on' => self::SCENARIO_DEFAULT, 'message' => Yii::t('users', 'This username has already been taken.')],
            ['username', 'string', 'min' => 2, 'max' => 255],
            [['firstName', 'lastName'], 'trim'],
            [['firstName', 'lastName'], 'string', 'max' => 70, 'min' => 2],
            [['firstName', 'lastName'], 'match', 'pattern' => '/^[a-zA-Z\s-]+$/'],
            [['email'], 'email'],
            [['phone'], 'string', 'max' => 9, 'min' => 9, 'message' => \Yii::t('frontend', 'The phone number must be in the format 0XXXXXXXX and start with 07 or 06')],
            [['phone'], 'match', 'pattern' => '/^(07|06)([0-9]){7}$/', 'message' => \Yii::t('frontend', 'The phone number must be in the format 0XXXXXXXX and start with 07 or 06')],
            [['status'], 'integer'],
            [['status'], 'default', 'value' => null],
            [['status'], 'in', 'range' => array_keys(Users::getStatusNameList())],
            [['password', 'rePassword'], 'string', 'min' => 8],
            ['rePassword', 'compare', 'compareAttribute' => 'password'],

            [['partnerCompanyId', 'partnerCompanyPositionId'], 'default', 'value' => null],
            [['partnerCompanyId', 'partnerCompanyPositionId'], 'integer'],
            [['partnerCompanyId', 'partnerCompanyPositionId'], 'filter', 'filter' => 'intval' ,'skipOnEmpty' => true],
            [['partnerCompanyId'], 'exist', 'skipOnEmpty' => true, 'skipOnError' => true, 'targetClass' => PartnerCompany::class, 'targetAttribute' => ['partnerCompanyId' => 'pc_id']],
            [['partnerCompanyPositionId'], 'exist', 'skipOnEmpty' => true, 'skipOnError' => true, 'targetClass' => PartnerCompanyPosition::class, 'targetAttribute' => ['partnerCompanyPositionId' => 'pcp_id']],
            [['id'], 'exist', 'skipOnEmpty' => false, 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['id' => 'id'], 'on' => self::SCENARIO_UPDATE],

            [['roles'], 'each', 'rule' => ['string']],
            [['roles'], 'each', 'rule' => ['in', 'range' => array_keys(Users::getAllRoles())]],

            [['products'], 'default', 'value' => []],
            [['products'], 'each', 'rule' => ['integer']],
            [['products'], 'each', 'rule' => ['in', 'range' => array_keys(ProductQuery::getList())]],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => Yii::t('users', 'Username'),
            'firstName' => Yii::t('users', 'First Name'),
            'lastName' => Yii::t('users', 'Last Name'),
            'password' => Yii::t('users', 'Password'),
            'rePassword' => Yii::t('users', 'Repeat Password'),
            'email' => Yii::t('users', 'Email'),
            'phone' => Yii::t('users', 'Phone'),
            'status' => Yii::t('users', 'Status'),
            'partnerCompanyId' => Yii::t('partner', 'Partner Company'),
            'partnerCompanyPositionId' => Yii::t('partner', 'Partner Company Position'),
            'products' => Yii::t('partner', 'Porduct Access'),
        ];
    }

    public function fillInByModel(Users $model): void
    {
        $this->id = $model->id;
        $this->username = $model->username;
        $this->firstName = $model->first_name;
        $this->lastName = $model->last_name;
        $this->email = $model->email;
        $this->phone = $model->phone;
        $this->status = $model->status;
        $this->partnerCompanyId = $model->partner_company_id;
        $this->partnerCompanyPositionId = $model->partner_company_position_id;
        $this->roles = $model->getRoles();
        $products = $model->usersProductAccess;
        $productAccess = [];
        foreach ($products as $product) {
            $productAccess[] = $product->upa_product_id;
        }
        $this->products = $productAccess;
    }
}
