<?php

namespace backend\src\useCase\users\manage;

use backend\src\entities\usersProductAccess\entity\UsersProductAccess;
use backend\src\entities\usersProductAccess\UserProductAccessRepository;
use backend\src\repositories\users\UserRepository;
use common\models\Employee;
use common\src\entities\product\entity\ProductQuery;
use common\src\entities\users\entity\Users;
use common\src\helpers\app\AppHelper;
use frontend\components\jobs\LoanApplicationDistributionJob;
use yii\db\Transaction;
use yii\helpers\ArrayHelper;

/**
 * @property-read UserRepository $userRepository
 * @property-read UserProductAccessRepository $userProductAccessRepository
 */
class UsersManageService
{
    private UserRepository $userRepository;
    private UserProductAccessRepository $userProductAccessRepository;

    public function __construct(UserRepository $userRepository, UserProductAccessRepository $userProductAccessRepository)
    {
        $this->userRepository = $userRepository;
        $this->userProductAccessRepository = $userProductAccessRepository;
    }

    public function create(UserManageForm $form): ?Users
    {
        $user = Users::create(
            $form->username,
            $form->firstName,
            $form->lastName,
            $form->email,
            $form->phone,
            $form->status,
            $form->partnerCompanyId,
            $form->partnerCompanyPositionId
        );
        $user->setPassword($form->password);
        $user->generateAuthKey();

        $transaction = new Transaction(['db' => \Yii::$app->db]);
        try {
            $transaction->begin();
            $userId = $this->userRepository->save($user);
            $auth = \Yii::$app->authManager;
            foreach ($form->roles as $role) {
                $authorRole = $auth->getRole($role);
                $auth->assign($authorRole, $userId);
            }

            foreach ($form->products as $productId) {
                $product = ProductQuery::getProductById($productId);
                $userProductAccess = UsersProductAccess::create($userId, $productId, $product->p_min_request_price ?? 0, UsersProductAccess::STATUS_ACTIVE);
                $this->userProductAccessRepository->save($userProductAccess);
            }

            $transaction->commit();

            foreach ($form->products as $productId) {
                $job = new LoanApplicationDistributionJob($productId);
                /** @var $queue \yii\queue\beanstalk\Queue */
                $queue = \Yii::$app->queue_loan_application_distribution;
                $queue->push($job);
            }

            return $user;
        } catch (\RuntimeException $e) {
            $transaction->rollBack();
            $form->addError('general', $e->getMessage());
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $form->addError('general', 'Internal Server Error');
            \Yii::error(AppHelper::throwableLog($e, true), 'UserManageService::create::Throwable');
        }
        return null;
    }

    public function update(UserManageForm $form, Users $model, int $userId): void
    {
        $model->username = $form->username;
        $model->first_name = $form->firstName;
        $model->last_name = $form->lastName;
        $model->email = $form->email;
        $model->phone = $form->phone;
        if (!empty($form->password)) {
            $model->setPassword($form->password);
        }
        $model->status = $form->status;
        $model->partner_company_id = $form->partnerCompanyId;
        $model->partner_company_position_id = $form->partnerCompanyPositionId;
        $model->updated_dt = date('Y-m-d H:i:s');
        $model->updated_user_id = $userId;

        $transaction = new Transaction(['db' => \Yii::$app->db]);
        try {
            $transaction->begin();
            $this->userRepository->save($model);

            $currentUserProductAccess = UsersProductAccess::findAll(['upa_user_id' => $model->id]);
            $currentUserProductAccess = ArrayHelper::map($currentUserProductAccess, 'upa_product_id', 'upa_product_id');
            $productIds = [];
            if (empty($form->products)) {
                UsersProductAccess::deleteAll(['upa_user_id' => $model->id]);
                $productIds = $currentUserProductAccess;
            } else {
                foreach ($form->products ?? [] as $productId) {
                    if (!in_array($productId, $currentUserProductAccess, false)) {
                        $product = ProductQuery::getProductById($productId);
                        $userProductAccess = UsersProductAccess::create(
                            $model->id,
                            $productId,
                            $product->p_min_request_price ?? 0,
                            UsersProductAccess::STATUS_ACTIVE
                        );
                        $this->userProductAccessRepository->save($userProductAccess);
                        $currentUserProductAccess[$userProductAccess->upa_product_id] = $userProductAccess->upa_product_id;
                        $productIds[$userProductAccess->upa_product_id] = $userProductAccess->upa_product_id;
                    }
                }
                foreach ($currentUserProductAccess as $productId) {
                    if (!in_array($productId, $form->products, false)) {
                        UsersProductAccess::deleteAll(['upa_user_id' => $model->id, 'upa_product_id' => $productId]);
                        $productIds[$productId] = $productId;
                    }
                }
            }

            $availableRoles = Users::getAllRoles();
            $userRoles = $form->roles ?? [];
            foreach ($userRoles as $keyItem => $roleItem) {
                if (!array_key_exists($roleItem, $availableRoles)) {
                    unset($userRoles[$keyItem]);
                }
            }
            $auth = \Yii::$app->authManager;
            $auth->revokeAll($model->id);
            foreach ($form->roles as $role) {
                $authorRole = $auth->getRole($role);
                $auth->assign($authorRole, $model->id);
            }

            $transaction->commit();

            foreach ($productIds as $productId) {
                $job = new LoanApplicationDistributionJob($productId);
                /** @var $queue \yii\queue\beanstalk\Queue */
                $queue = \Yii::$app->queue_loan_application_distribution;
                $queue->push($job);
            }
        } catch (\RuntimeException $e) {
            $transaction->rollBack();
            $form->addError('general', $e->getMessage());
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $form->addError('general', 'Internal Server Error');
            \Yii::error(AppHelper::throwableLog($e, true), 'UserManageService::update::Throwable');
        }
    }
}