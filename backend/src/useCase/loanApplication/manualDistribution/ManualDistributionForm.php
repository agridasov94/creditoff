<?php

namespace backend\src\useCase\loanApplication\manualDistribution;

use common\components\validators\IsArrayValidator;
use yii\base\Model;

/**
 * @property array $usersIds
 * @property int $loanApplicationId
 */
class ManualDistributionForm extends Model
{
    public $usersIds;
    public $loanApplicationId;

    public function rules(): array
    {
        return [
            [['usersIds'], 'required', 'message' => \Yii::t('backend', 'You have not selected any user')],
            [['loanApplicationId'], 'required'],
            [['usersIds'], IsArrayValidator::class, 'skipOnEmpty' => true]
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'usersIds' => \Yii::t('backend', 'User'),
            'loanApplicationId' => \Yii::t('backend', 'Loan Application Id')
        ];
    }

    public function formName(): string
    {
        return '';
    }
}