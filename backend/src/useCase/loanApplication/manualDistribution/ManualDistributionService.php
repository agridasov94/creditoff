<?php

namespace backend\src\useCase\loanApplication\manualDistribution;

use common\src\entities\loanApplication\entity\LoanApplication;
use common\src\entities\loanApplication\entity\LoanApplicationRepository;
use common\src\entities\loanApplication\entity\LoanApplicationStatus;
use common\src\entities\loanApplicationAccess\entity\LoanApplicationAccess;
use common\src\entities\loanApplicationAccess\entity\LoanApplicationAccessRepository;
use common\src\helpers\app\AppHelper;
use yii\db\Transaction;

/**
 * @property-read LoanApplicationRepository $loanApplicationRepository
 * @property-read LoanApplicationAccessRepository $loanApplicationAccessRepository
 */
class ManualDistributionService
{
    private LoanApplicationRepository $loanApplicationRepository;
    private LoanApplicationAccessRepository $loanApplicationAccessRepository;

    public function __construct(
        LoanApplicationRepository $loanApplicationRepository,
        LoanApplicationAccessRepository $loanApplicationAccessRepository
    ) {
        $this->loanApplicationRepository = $loanApplicationRepository;
        $this->loanApplicationAccessRepository = $loanApplicationAccessRepository;
    }

    public function handle(ManualDistributionForm $form, LoanApplication $request): void
    {
        $transaction = new Transaction(['db' => \Yii::$app->db]);

        try {
            $transaction->begin();

            foreach ($form->usersIds as $usersId) {
                $loanApplicationAccess = LoanApplicationAccess::create($request->la_id, (int)$usersId)
                    ->setStatusPending()
                    ->setActionByUser();
                $this->loanApplicationAccessRepository->save($loanApplicationAccess);
            }

            $request->setManualDistribution();
            $request->increaseDistributionIteration();
            $request->setStatus(LoanApplicationStatus::STATUS_SENT);
            $this->loanApplicationRepository->save($request);

            $transaction->commit();
        } catch (\RuntimeException $e) {
            $transaction->rollBack();
            $form->addError('general', $e->getMessage());
        } catch (\Throwable $e) {
            $transaction->rollBack();
            \Yii::error(AppHelper::throwableLog($e, true), 'ManualDistributionService::handle::Throwable');
            $form->addError('general', \Yii::t('backend', 'Internal Server Error'));
        }
    }
}