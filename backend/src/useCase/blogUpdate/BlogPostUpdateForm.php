<?php

namespace backend\src\useCase\blogUpdate;

use common\src\entities\blog\categories\entity\BlogCategories;
use common\src\entities\blog\posts\entity\BlogPosts;
use common\src\entities\users\entity\Users;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * @property int $id
 * @property string $langId
 * @property int $categoryId
 * @property string $title
 * @property string|null $shortDescription
 * @property string $content
 * @property int $authorId
 * @property int $active
 */
class BlogPostUpdateForm extends Model
{
    public $id;
    public $langId;
    public $categoryId;
    public $title;
    public $shortDescription;
    public $content;
    public $authorId;
    public $active;
    public $alias;
    public $url;
    /**
     * @var UploadedFile
     */
    public $preview;

    public function rules(): array
    {
        return [
            [['id', 'langId', 'categoryId', 'title', 'authorId', 'content', 'url'], 'required'],
            [['categoryId', 'authorId', 'active'], 'integer'],
            [['content'], 'string'],
            [['langId'], 'string', 'max' => 5],
            [['title'], 'string', 'max' => 150],
            [['shortDescription', 'url'], 'string', 'max' => 255],
            [['langId', 'url'], 'unique', 'skipOnError' => true, 'targetClass' => BlogPosts::class, 'targetAttribute' => ['langId' => 'bp_lang_id', 'url' => 'bp_url']],
            [['authorId'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['authorId' => 'id']],
            [['categoryId'], 'exist', 'skipOnError' => true, 'targetClass' => BlogCategories::class, 'targetAttribute' => ['categoryId' => 'bc_id']],
            [['preview'], 'file', 'skipOnEmpty' => false, 'extensions' => ['png', 'jpg', 'jpeg']]
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'langId' => \Yii::t('backend.blog_posts', 'Language'),
            'categoryId' => \Yii::t('backend.blog_posts', 'Category'),
            'title' => \Yii::t('backend.blog_posts', 'Title'),
            'shortDescription' => \Yii::t('backend.blog_posts', 'Short Description'),
            'content' => \Yii::t('backend.blog_posts', 'Main content'),
            'authorId' => \Yii::t('backend.blog_posts', 'Author'),
            'active' => \Yii::t('backend.blog_posts', 'Enable'),
            'preview' => \Yii::t('backend.blog_posts', 'Preview'),
            'url' => \Yii::t('backend.blog_posts', 'URL Address'),
        ];
    }

    public function fillInByModel(BlogPosts $model)
    {
        $this->id = $model->bp_id;
        $this->langId = $model->bp_lang_id;
        $this->categoryId = $model->bp_category_id;
        $this->title = $model->bp_title;
        $this->shortDescription = $model->bp_short_description;
        $this->content = $model->bp_content;
        $this->url = $model->bp_url;
    }
}