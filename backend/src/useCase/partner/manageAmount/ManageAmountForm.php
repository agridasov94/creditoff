<?php

namespace backend\src\useCase\partner\manageAmount;

use backend\src\entities\partnerCompany\entity\PartnerCompany;
use yii\base\Model;

class ManageAmountForm extends Model
{
    public $partnerCompanyId;
    public $amount;
    public $bonus;

    public function rules(): array
    {
        return [
            [['partnerCompanyId'], 'required'],
            [['partnerCompanyId', 'amount', 'bonus'], 'integer'],
            [['partnerCompanyId'], 'exist', 'skipOnError' => true, 'targetClass' => PartnerCompany::class, 'targetAttribute' => ['partnerCompanyId' => 'pc_id']],
            [['partnerCompanyId', 'amount', 'bonus'], 'filter', 'filter' => 'intval'],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'partnerCompanyId' => \Yii::t('partner', 'Partner Company'),
            'amount' => \Yii::t('partner', 'Amount'),
            'bonus' => \Yii::t('partner', 'Bonus'),
        ];
    }
}