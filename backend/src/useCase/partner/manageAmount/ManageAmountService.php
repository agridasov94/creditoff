<?php

namespace backend\src\useCase\partner\manageAmount;

use backend\src\entities\partnerCompany\amountHistory\entity\PartnerCompanyAmountHistory;
use backend\src\entities\partnerCompany\amountHistory\entity\PartnerCompanyAmountHistoryRepository;
use backend\src\entities\partnerCompany\entity\PartnerCompany;
use backend\src\entities\partnerCompany\entity\PartnerCompanyRepository;
use common\src\entities\product\entity\ProductQuery;
use common\src\exceptions\NotFoundException;
use common\src\helpers\app\AppHelper;
use frontend\components\jobs\LoanApplicationDistributionJob;
use Yii;
use yii\db\Transaction;

/**
 * @property-read PartnerCompanyRepository $repository
 * @property-read PartnerCompanyAmountHistoryRepository $companyAmountHistoryRepository
 */
class ManageAmountService
{
    private PartnerCompanyRepository $repository;
    private PartnerCompanyAmountHistoryRepository $companyAmountHistoryRepository;

    public function __construct(PartnerCompanyRepository $repository, PartnerCompanyAmountHistoryRepository $companyAmountHistoryRepository)
    {
        $this->repository = $repository;
        $this->companyAmountHistoryRepository = $companyAmountHistoryRepository;
    }

    public function editPartnerAmountAndBonus(ManageAmountForm $form): void
    {
        $transaction = new Transaction(['db' => \Yii::$app->db]);
        try {
            $partnerCompany = PartnerCompany::findOne($form->partnerCompanyId);
            if (!$partnerCompany) {
                throw new NotFoundException('Partner Company not found by id: ' . $form->partnerCompanyId);
            }

            $amountBefore = $partnerCompany->pc_total_amount;
            $bonusBefore = $partnerCompany->pc_total_bonus_amount;

            $partnerCompany->pc_total_amount += (int)$form->amount;
            $partnerCompany->pc_total_bonus_amount += (int)$form->bonus;

            $history = PartnerCompanyAmountHistory::createByActionManage(
                $form->partnerCompanyId,
                (int)$form->amount,
                (int)$form->bonus,
                (int)$amountBefore,
                $partnerCompany->pc_total_amount,
                (int)$bonusBefore,
                $partnerCompany->pc_total_bonus_amount
            );

            $transaction->begin();
            $this->repository->save($partnerCompany);
            $this->companyAmountHistoryRepository->save($history);
            $transaction->commit();

            if ($amountBefore !== $partnerCompany->pc_total_amount || $bonusBefore !== $partnerCompany->pc_total_bonus_amount) {
                $productIds = ProductQuery::getListIdsByUsersAssignedToCompany($partnerCompany->pc_id);
                foreach ($productIds as $productId) {
                    $job = new LoanApplicationDistributionJob((int)$productId);
                    /** @var $queue \yii\queue\beanstalk\Queue */
                    $queue = \Yii::$app->queue_loan_application_distribution;
                    $queue->push($job);
                }
            }
        } catch (\RuntimeException | \DomainException $e) {
            $transaction->rollBack();
            Yii::error(AppHelper::throwableLog($e), 'PartnerCompanyController::actionManageAmount::RuntimeException|DomainException');
            $form->addError('general', $e->getMessage());
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $form->addError('general', 'Internal Server Error');
            Yii::error(AppHelper::throwableLog($e, true), 'PartnerCompanyController::actionManageAmount::Throwable');
        }
    }
}