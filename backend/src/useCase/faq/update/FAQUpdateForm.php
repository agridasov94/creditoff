<?php

namespace backend\src\useCase\faq\update;

use common\src\entities\faq\entity\Faq;
use yii\base\Model;

class FAQUpdateForm extends Model
{
    public $langId;
    public $question;
    public $answer;

    public function rules(): array
    {
        return [
            [['answer', 'question', 'langId'], 'required'],
            [['answer', 'question'], 'trim'],
            [['answer'], 'string'],
            [['question'], 'string', 'max' => 255],
            [['langId'], 'string', 'max' => 5],
            [['langId', 'question'], 'unique', 'skipOnError' => true, 'targetClass' => Faq::class, 'targetAttribute' => ['langId' => 'f_lang_id', 'question' => 'f_question']],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'question' => \Yii::t('faq', 'Question'),
            'answer' => \Yii::t('faq', 'Answer'),
        ];
    }
}