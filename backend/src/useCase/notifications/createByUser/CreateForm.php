<?php

namespace backend\src\useCase\notifications\createByUser;

use backend\src\entities\usersNotifications\entity\UsersNotifications;
use common\src\entities\users\entity\Users;

class CreateForm extends \yii\base\Model
{
    public $users;
    public $type;
    public $title;
    public $message;
    public $popup;

    public function rules(): array
    {
        return [
            [['users', 'type', 'title', 'message'], 'required'],
            [['popup'], 'boolean'],
            [['users'], 'common\components\validators\IsArrayValidator'],
            [['type'], 'in', 'range' => array_keys(UsersNotifications::getTypeList())],
            [['title'], 'string', 'max' => 255],
            [['users'], 'each', 'rule' => ['filter', 'filter' => 'intval']],
            [['users'], 'each', 'rule' => ['exist', 'targetClass' => Users::class, 'targetAttribute' => 'id'], 'skipOnEmpty' => true, 'skipOnError' => true],
        ];
    }

    public function formName(): string
    {
        return '';
    }
}