<?php

namespace backend\src\helpers;

use common\src\helpers\app\AppHelper;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class WebSocketHelper
{
    public const LOAN_APPLICATION_SUBSCRIBE_LIST = [
        'loan-application/sent',
        'loan-application/in-work',
    ];

    public static function subscribeToLoanApplicationChannel(?int $loanStatusId, int $userId, &$subList): void
    {
        if ($loanStatusId && ArrayHelper::isIn(Yii::$app->controller->action->uniqueId, self::LOAN_APPLICATION_SUBSCRIBE_LIST)) {
            $subList[] = self::getLoanApplicationPubSubKey($loanStatusId, $userId);
        }
    }

    public static function pub(array $channels, string $command, array $data = [])
    {
        $redis = \Yii::$app->redis;
        if ($command) {
            $data['cmd'] = $command;
        }
        $jsonData = $data;
        try {
            if ($channels) {
                foreach ($channels as $channel) {
                    $redis->publish($channel, Json::encode($jsonData));
                }
                return true;
            }
        } catch (\Throwable $throwable) {
            Yii::error(AppHelper::throwableFormatter($throwable), 'WebSocketHelper:pub:redis');
        }
        return false;
    }

    public static function getLoanApplicationPubSubKey(int $status, int $userId): string
    {
        return 'loan-application-' . $status . '-' . $userId;
    }

    public static function getUserPubSubKey(int $userId): string
    {
        return 'user-' . $userId;
    }
}