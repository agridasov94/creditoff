<?php

namespace backend\src\helpers;

use common\src\entities\users\entity\Users;

class DateHelper
{
    /**
     * @param int $time
     * @return string
     */
    public static function convertTimeFromUserDtToUTC(int $time, Users $user): string
    {
        $dateTime = '';

        if ($time >= 0) {
            $timezone = $user->timezone;
            $dateTime = date('Y-m-d H:i:s', $time);

            try {
                if ($timezone) {
                    $date = new \DateTime($dateTime, new \DateTimeZone($timezone));
                    $date->setTimezone(new \DateTimeZone('UTC'));
                    $dateTime = $date->format('Y-m-d H:i:s');
                }
            } catch (\Throwable $throwable) {
                $dateTime = '';
            }
        }

        return $dateTime;
    }
}