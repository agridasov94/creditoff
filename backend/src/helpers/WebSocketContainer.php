<?php

namespace backend\src\helpers;

class WebSocketContainer
{
    private ?int $loanApplicationStatus = null;

    public function setLoanApplicationStatus(int $id): void
    {
        $this->loanApplicationStatus = $id;
    }

    public function getLoanApplicationStatus(): ?int
    {
        return $this->loanApplicationStatus;
    }
}