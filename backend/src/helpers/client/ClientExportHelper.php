<?php

namespace backend\src\helpers\client;

use backend\components\grid\DateTimeColumn;
use backend\components\grid\UserSelect2Column;
use common\src\entities\client\email\entity\ClientEmail;
use common\src\entities\client\employmentData\entity\ClientEmploymentData;
use common\src\entities\client\entity\Client;
use common\src\entities\client\passportData\entity\ClientPassportData;
use common\src\entities\client\phone\entity\ClientPhone;
use yii\helpers\ArrayHelper;

class ClientExportHelper
{
    private ClientEmail $clientEmail;
    private ClientPhone $clientPhone;
    private ClientPassportData $clientPassportData;
    private ClientEmploymentData $clientEmploymentData;

    public function __construct(
        ClientEmail $clientEmail,
        ClientPhone $clientPhone,
        ClientPassportData $clientPassportData,
        ClientEmploymentData $clientEmploymentData
    ) {
        $this->clientEmail = $clientEmail;
        $this->clientPhone = $clientPhone;
        $this->clientPassportData = $clientPassportData;
        $this->clientEmploymentData = $clientEmploymentData;
    }

    public function getAdditionalExportColumnsByClient(Client $client): array
    {
        $finalData = [
            $this->clientEmail->getAttributeLabel('ce_email') => $this->getEmails($client),
            $this->clientPhone->getAttributeLabel('cp_phone') => $this->getPhones($client),
        ];
        return ArrayHelper::merge(ArrayHelper::merge($finalData, $this->getClientPassportData($client)), $this->getEmploymentDataColumns());
    }

    public function getAdditionalExportColumns(): array
    {
        $finalData = [
            $this->getEmailColumn(),
            $this->getPhoneColumn()
        ];
        return ArrayHelper::merge(ArrayHelper::merge($finalData, $this->getPassportDataColumns()), $this->getEmploymentDataColumns());
    }

    private function getEmailColumn(): array
    {
        return [
            'attribute' => 'ce_email',
            'label' => $this->clientEmail->getAttributeLabel('ce_email'),
            'value' => function (Client $client) {
                return $this->getEmails($client);
            }
        ];
    }

    private function getPhoneColumn(): array
    {
        return [
            'attribute' => 'cp_phone',
            'label' => $this->clientPhone->getAttributeLabel('cp_phone'),
            'value' => function (Client $client) {
                return $this->getPhones($client);
            }
        ];
    }

    private function getPassportDataColumns(): array
    {
        return [
            [
                'attribute' => 'cpd_type',
                'value' => static function (Client $model) {
                    if ($model->clientPassportData) {
                        return $model->clientPassportData->cpd_type ? ClientPassportData::getTypeName($model->clientPassportData->cpd_type) : null;
                    }
                    return null;
                },
                'label' => $this->clientPassportData->getAttributeLabel('cpd_type')
            ],
            [
                'attribute' => 'cpd_series',
                'label' => $this->clientPassportData->getAttributeLabel('cpd_series')
            ],
            [
                'attribute' => 'cpd_number',
                'label' => $this->clientPassportData->getAttributeLabel('cpd_number')
            ],
            [
                'attribute' => 'cpd_issued_by',
                'label' => $this->clientPassportData->getAttributeLabel('cpd_issued_by')
            ],
            [
                'class' => DateTimeColumn::class,
                'attribute' => 'cpd_issue_date',
                'format' => 'byUserDate',
                'label' => $this->clientPassportData->getAttributeLabel('cpd_issue_date')
            ],
            [
                'attribute' => 'cpd_issue_department_code',
                'label' => $this->clientPassportData->getAttributeLabel('cpd_issue_department_code')
            ],
            [
                'class' => DateTimeColumn::class,
                'attribute' => 'cpd_expiration_date',
                'format' => 'byUserDate',
                'label' => $this->clientPassportData->getAttributeLabel('cpd_expiration_date')
            ],
            [
                'attribute' => 'cpd_registered_address',
                'label' => $this->clientPassportData->getAttributeLabel('cpd_registered_address')
            ],
            [
                'attribute' => 'cpd_residence_address',
                'label' => $this->clientPassportData->getAttributeLabel('cpd_residence_address')
            ]
        ];
    }

    private function getEmploymentDataColumns(): array
    {
        return [
            [
                'attribute' => 'ced_type',
                'label' => $this->clientEmploymentData->getAttributeLabel('ced_type'),
                'value' => static function (Client $client) {
                    if ($client->clientEmploymentData) {
                        $types = [];
                        foreach ($client->clientEmploymentData as $clientEmploymentData) {
                            $types[] = $clientEmploymentData->ced_type ? ClientEmploymentData::getTypeName($clientEmploymentData->ced_type) : null;
                        }
                        return implode(', ', $types);
                    }
                    return null;
                }
            ],
            [
                'attribute' => 'ced_work_phone',
                'label' => $this->clientEmploymentData->getAttributeLabel('ced_work_phone')
            ],
            [
                'attribute' => 'ced_organization_name',
                'label' => $this->clientEmploymentData->getAttributeLabel('ced_organization_name')
            ],
            [
                'attribute' => 'ced_position',
                'label' => $this->clientEmploymentData->getAttributeLabel('ced_position')
            ],
            [
                'attribute' => 'ced_seniority',
                'label' => $this->clientEmploymentData->getAttributeLabel('ced_seniority')
            ],
            [
                'attribute' => 'ced_official_wages',
                'label' => $this->clientEmploymentData->getAttributeLabel('ced_official_wages')
            ],
            [
                'attribute' => 'ced_unofficial_wages',
                'label' => $this->clientEmploymentData->getAttributeLabel('ced_unofficial_wages')
            ],
            [
                'attribute' => 'ced_revenues_documented',
                'label' => $this->clientEmploymentData->getAttributeLabel('ced_revenues_documented')
            ],
            [
                'attribute' => 'ced_revenues_non_documented',
                'label' => $this->clientEmploymentData->getAttributeLabel('ced_revenues_non_documented')
            ]
        ];
    }

    private function getEmails(Client $client): string
    {
        $emails = [];
        foreach ($client->clientEmails as $clientEmail) {
            $emails[] = $clientEmail->ce_email;
        }
        return implode(', ', $emails);
    }

    private function getPhones(Client $client): string
    {
        $phones = [];
        foreach ($client->clientPhones as $clientPhone) {
            $phones[] = $clientPhone->cp_phone;
        }
        return implode(', ', $phones);
    }

    private function getClientPassportData(Client $client): array
    {
        if ($client->clientPassportData) {
            return $client->clientPassportData->toArray();
        }
        return $this->clientPassportData->toArray();
    }
}
