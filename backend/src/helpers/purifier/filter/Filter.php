<?php

namespace backend\src\helpers\purifier\filter;

interface Filter
{
    public function filter(?string $content): ?string;
}
