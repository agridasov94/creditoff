<?php

namespace backend\src\helpers\purifier\filter;

use yii\bootstrap4\Html;

/**
 * Class FilterShortCodeToLink
 *
 * @property string|null $content
 * @property string |null $host
 */
class FilterShortCodeToLink implements Filter
{
    private $host;
    private $content;

    public function __construct()
    {
        $this->host = \Yii::$app->params['backendHost'];
    }

    /**
     * Ex. {case-10-qwerty} => <a href="https://....com/cases/view/qwerty">10</a>
     *
     * @param string|null $content
     * @return string|null
     */
    public function filter(?string $content): ?string
    {
        if ($content === null) {
            return null;
        }
        $this->content = $content;
        $this->processNotification();
        return $this->content;
    }

    private function processNotification(): void
    {
        $this->content = preg_replace_callback('|id:([\d]+)|', function ($matches) {
            return Html::a($matches[1], $this->host . '/setting/view?id=' . $matches[1]);
        }, $this->content);

        $this->content = preg_replace_callback('/from(.*?)to/', function ($matches) {
            $detectPhone = self::detectPhones($matches[1]);
            if ($detectPhone !== $matches[1]) {
                return '<br><pre><code>from: ' . $detectPhone . '</pre></code>to';
            }
            return $matches[0];
        }, $this->content);

        $this->content = preg_replace_callback('/to(.*?)by/', function ($matches) {
            return '<br><pre><code>to: ' . $matches[1] . '</pre></code>by';
        }, $this->content);
    }
}
