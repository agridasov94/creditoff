<?php

namespace backend\src\helpers\purifier;

use backend\src\helpers\purifier\filter\Filter;
use backend\src\helpers\purifier\filter\FilterShortCodeToLink;

class PurifierFilter
{
    public static function shortCodeToLink(): Filter
    {
        return new FilterShortCodeToLink();
    }
}
