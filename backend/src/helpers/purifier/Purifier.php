<?php

namespace backend\src\helpers\purifier;

use backend\src\helpers\purifier\filter\Filter;

class Purifier
{
    public static function purify(?string $content, Filter ...$filters): ?string
    {
        if ($content === null) {
            return null;
        }

        if (!$filters) {
            $filters[] = PurifierFilter::shortCodeToLink();
        }

        foreach ($filters as $filter) {
            $content = $filter->filter($content);
        }

        return $content;
    }
}