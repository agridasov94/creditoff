<?php

namespace backend\src\entities\usersProductAccess;

use backend\src\entities\usersProductAccess\entity\UsersProductAccess;

class UserProductAccessRepository
{
    public function save(UsersProductAccess $model): UsersProductAccess
    {
        if (!$model->save()) {
            throw new \RuntimeException('Users Product Access saving failed: ' . $model->getErrorSummary(true)[0]);
        }
        return $model;
    }
}