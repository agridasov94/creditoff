<?php

namespace backend\src\entities\usersProductAccess\entity;

use backend\src\entities\partnerCompany\entity\PartnerCompany;
use common\src\entities\loanApplicationAccess\entity\LoanApplicationAccess;
use common\src\entities\users\entity\Users;
use yii\helpers\ArrayHelper;

class UsersProductAccessQuery
{
    public static function getAccessPriceLessThen(int $productId, int $price): ?UsersProductAccess
    {
        return UsersProductAccess::find()
            ->where(['upa_product_id' => $productId])
            ->andWhere(['<', 'upa_request_price', $price])
            ->orderBy(['upa_request_price' => SORT_ASC])
            ->one();
    }

    /**
     * @param int $userId
     * @return UsersProductAccess[]
     */
    public static function findByUserId(int $userId): array
    {
        return UsersProductAccess::find()->byUserId($userId)->all();
    }

    public static function findByUserAndProductIds(int $userId, int $productId): ?UsersProductAccess
    {
        return UsersProductAccess::find()->byUserId($userId)->byProductId($productId)->one();
    }

    public static function findActiveByUserAndProductId(int $userId, int $productId): ?UsersProductAccess
    {
        return UsersProductAccess::find()->byUserId($userId)->byProductId($productId)->active()->one();
    }

    public static function findTwoMaxPricesForRequestByProduct(int $productId): array
    {
        $data = UsersProductAccess::find()->select(['upa_request_price'])->byProductId($productId)->limit(2)->orderBy(['upa_request_price' => SORT_DESC])->asArray()->all();
        return ArrayHelper::getColumn($data, 'upa_request_price');
    }

    /**
     * @param int $productId
     * @return UsersProductAccess[]
     */
    public static function findAllAvailableUsersByProduct(int $productId): array
    {
        return UsersProductAccess::find()
            ->join('inner join', Users::tableName(), 'id = upa_user_id')
            ->join('inner join', PartnerCompany::tableName(), 'pc_id = partner_company_id')
            ->byProductId($productId)
            ->active()
            ->andWhere(['>', 'upa_request_price', 0])
            ->andWhere(['IS NOT', 'upa_request_price', null])
            ->andWhere(['or', 'pc_total_amount >= upa_request_price', 'pc_total_bonus_amount >= upa_request_price'])
            ->orderBy(['upa_request_price' => SORT_DESC])
            ->all();
    }
}