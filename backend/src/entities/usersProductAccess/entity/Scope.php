<?php

namespace backend\src\entities\usersProductAccess\entity;

use common\src\entities\loanApplicationAccess\entity\LoanApplicationAccessStatus;

/**
 * This is the ActiveQuery class for [[UsersProductAccess]].
 *
 * @see UsersProductAccess
 */
class Scope extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return UsersProductAccess[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return UsersProductAccess|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function byProductId(int $id): self
    {
        return $this->andWhere(['upa_product_id' => $id]);
    }

    public function priceLargerThen(int $price): self
    {
        return $this->andWhere(['>', 'min(upa_request_price)', $price]);
    }

    public function byUserId(int $userId): self
    {
        return $this->andWhere(['upa_user_id' => $userId]);
    }

    public function active(): self
    {
        return $this->andWhere(['upa_status' => UsersProductAccess::STATUS_ACTIVE]);
    }
}
