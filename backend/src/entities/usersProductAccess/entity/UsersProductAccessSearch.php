<?php

namespace backend\src\entities\usersProductAccess\entity;

use backend\src\entities\partnerCompany\entity\PartnerCompany;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * UsersProductAccessSearch represents the model behind the search form of `backend\src\entities\usersProductAccess\entity\UsersProductAccess`.
 */
class UsersProductAccessSearch extends UsersProductAccess
{
    public $companyId;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['upa_user_id', 'upa_product_id', 'upa_created_user_id', 'upa_updated_user_id', 'upa_request_price', 'upa_status'], 'integer'],
            [['upa_created_dt', 'upa_updated_dt'], 'safe'],
            ['upa_status', 'in', 'range' => array_keys(self::getStatusListName())],
            [['companyId'], 'integer'],
            [['companyId'], 'filter', 'filter' => 'intval'],
        ];
    }

    public function attributeLabels(): array
    {
        return ArrayHelper::merge([
            'companyId' => \Yii::t('company', 'Company')
        ], parent::attributeLabels());
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UsersProductAccess::find();
        $query->joinWith('user');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->companyId) {
            $query->andWhere(['partner_company_id' => $this->companyId]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'upa_user_id' => $this->upa_user_id,
            'upa_product_id' => $this->upa_product_id,
            'date(upa_created_dt)' => $this->upa_created_dt,
            'date(upa_updated_dt)' => $this->upa_updated_dt,
            'upa_created_user_id' => $this->upa_created_user_id,
            'upa_updated_user_id' => $this->upa_updated_user_id,
            'upa_status' => $this->upa_status,
            'upa_request_price' => $this->upa_request_price
        ]);

        return $dataProvider;
    }
}
