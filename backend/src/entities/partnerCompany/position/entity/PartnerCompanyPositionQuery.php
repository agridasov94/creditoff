<?php

namespace backend\src\entities\partnerCompany\position\entity;

use yii\helpers\ArrayHelper;

class PartnerCompanyPositionQuery
{
    public static function getList(): array
    {
        $models = PartnerCompanyPosition::find()->select(['pcp_id', 'pcp_name'])->asArray()->all() ?? [];
        return ArrayHelper::map($models, 'pcp_id', 'pcp_name');
    }
}