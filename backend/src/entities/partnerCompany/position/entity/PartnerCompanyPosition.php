<?php

namespace backend\src\entities\partnerCompany\position\entity;

use common\src\entities\users\entity\Users;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "partner_company_position".
 *
 * @property int $pcp_id
 * @property string|null $pcp_name
 * @property int|null $pcp_created_user_id
 * @property int|null $pcp_updated_user_id
 * @property string|null $pcp_created_dt
 * @property string|null $pcp_updated_dt
 *
 * @property Users $createdUser
 * @property Users $updatedUser
 */
class PartnerCompanyPosition extends \yii\db\ActiveRecord
{
    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['pcp_created_dt'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['pcp_updated_dt'],
                ],
                'value' => date('Y-m-d H:i:s') //new Expression('NOW()'),
            ],
            'user' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'pcp_created_user_id',
                'updatedByAttribute' => 'pcp_updated_user_id',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'partner_company_position';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pcp_created_user_id', 'pcp_updated_user_id'], 'integer'],
            [['pcp_created_dt', 'pcp_updated_dt'], 'safe'],
            [['pcp_name'], 'string', 'max' => 50],
            [['pcp_created_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['pcp_created_user_id' => 'id']],
            [['pcp_updated_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['pcp_updated_user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'pcp_id' => Yii::t('partner', 'ID'),
            'pcp_name' => Yii::t('partner', 'Name'),
            'pcp_created_user_id' => Yii::t('partner', 'Created User'),
            'pcp_updated_user_id' => Yii::t('partner', 'Updated User'),
            'pcp_created_dt' => Yii::t('partner', 'Created Dt'),
            'pcp_updated_dt' => Yii::t('partner', 'Updated Dt'),
        ];
    }

    /**
     * Gets query for [[PcpCreatedUser]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\users\entity\Scope
     */
    public function getCreatedUser()
    {
        return $this->hasOne(Users::class, ['id' => 'pcp_created_user_id']);
    }

    /**
     * Gets query for [[PcpUpdatedUser]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\users\entity\Scope
     */
    public function getUpdatedUser()
    {
        return $this->hasOne(Users::class, ['id' => 'pcp_updated_user_id']);
    }

    /**
     * {@inheritdoc}
     * @return Scope the active query used by this AR class.
     */
    public static function find()
    {
        return new Scope(get_called_class());
    }
}
