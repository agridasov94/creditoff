<?php

namespace backend\src\entities\partnerCompany\position\entity;

/**
 * This is the ActiveQuery class for [[PartnerCompanyPosition]].
 *
 * @see PartnerCompanyPosition
 */
class Scope extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return PartnerCompanyPosition[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return PartnerCompanyPosition|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
