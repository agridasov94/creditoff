<?php

namespace backend\src\entities\partnerCompany\position\entity;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\src\entities\partnerCompany\position\entity\PartnerCompanyPosition;

/**
 * PartnerCompanyPositionSearch represents the model behind the search form of `backend\src\entities\partnerCompany\position\entity\PartnerCompanyPosition`.
 */
class PartnerCompanyPositionSearch extends PartnerCompanyPosition
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pcp_id', 'pcp_created_user_id', 'pcp_updated_user_id'], 'integer'],
            [['pcp_name', 'pcp_created_dt', 'pcp_updated_dt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PartnerCompanyPosition::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'pcp_id' => $this->pcp_id,
            'pcp_created_user_id' => $this->pcp_created_user_id,
            'pcp_updated_user_id' => $this->pcp_updated_user_id,
            'date(pcp_created_dt)' => $this->pcp_created_dt,
            'date(pcp_updated_dt)' => $this->pcp_updated_dt,
        ]);

        $query->andFilterWhere(['like', 'pcp_name', $this->pcp_name]);

        return $dataProvider;
    }
}
