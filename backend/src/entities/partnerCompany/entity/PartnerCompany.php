<?php

namespace backend\src\entities\partnerCompany\entity;

use common\src\entities\users\entity\Users;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "partner_company".
 *
 * @property int $pc_id
 * @property string|null $pc_name
 * @property string|null $pc_fiscal_code
 * @property int|null $pc_total_amount
 * @property int|null $pc_total_bonus_amount
 * @property int|null $pc_is_blocked
 * @property int|null $pc_created_user_id
 * @property int|null $pc_updated_user_id
 * @property string|null $pc_created_dt
 * @property string|null $pc_updated_dt
 *
 * @property Users $createdUser
 * @property Users $updatedUser
 */
class PartnerCompany extends \yii\db\ActiveRecord
{

    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['pc_created_dt'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['pc_updated_dt'],
                ],
                'value' => date('Y-m-d H:i:s') //new Expression('NOW()'),
            ],
            'user' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'pc_created_user_id',
                'updatedByAttribute' => 'pc_updated_user_id',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'partner_company';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pc_total_amount', 'pc_total_bonus_amount', 'pc_is_blocked', 'pc_created_user_id', 'pc_updated_user_id'], 'integer'],
            [['pc_created_dt', 'pc_updated_dt'], 'safe'],
            [['pc_name'], 'string', 'max' => 100],
            [['pc_fiscal_code'], 'string', 'max' => 50],
            [['pc_created_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['pc_created_user_id' => 'id']],
            [['pc_updated_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['pc_updated_user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'pc_id' => Yii::t('partner', 'ID'),
            'pc_name' => Yii::t('partner', 'Name'),
            'pc_fiscal_code' => Yii::t('partner', 'Fiscal Code'),
            'pc_total_amount' => Yii::t('partner', 'Total Amount'),
            'pc_total_bonus_amount' => Yii::t('partner', 'Total Bonus Amount'),
            'pc_is_blocked' => Yii::t('partner', 'Is Blocked'),
            'pc_created_user_id' => Yii::t('partner', 'Created User ID'),
            'pc_updated_user_id' => Yii::t('partner', 'Updated User ID'),
            'pc_created_dt' => Yii::t('partner', 'Created Dt'),
            'pc_updated_dt' => Yii::t('partner', 'Updated Dt'),
        ];
    }

    /**
     * Gets query for [[PcCreatedUser]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\users\entity\Scope
     */
    public function getCreatedUser()
    {
        return $this->hasOne(Users::class, ['id' => 'pc_created_user_id']);
    }

    /**
     * Gets query for [[PcUpdatedUser]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\users\entity\Scope
     */
    public function getUpdatedUser()
    {
        return $this->hasOne(Users::class, ['id' => 'pc_updated_user_id']);
    }

    /**
     * {@inheritdoc}
     * @return Scope the active query used by this AR class.
     */
    public static function find()
    {
        return new Scope(get_called_class());
    }

    public function withdrawalBonusFunds(int $withdrawalAmount): void
    {
        $this->pc_total_bonus_amount -= $withdrawalAmount;
    }

    public function withdrawalMainFunds(int $withdrawalAmount): void
    {
        $this->pc_total_amount -= $withdrawalAmount;
    }

    public function isBonusAmountNegative(): bool
    {
        return $this->pc_total_bonus_amount < 0;
    }

    public function isMainAmountNegative(): bool
    {
        return $this->pc_total_amount < 0;
    }

    public function resetWithdrawalBonusFunds(): void
    {
        $this->pc_total_bonus_amount = $this->getOldAttribute('pc_total_bonus_amount');
    }

    public function resetWithdrawalMainFunds(): void
    {
        $this->pc_total_amount = $this->getOldAttribute('pc_total_amount');
    }
}
