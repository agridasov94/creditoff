<?php

namespace backend\src\entities\partnerCompany\entity;

use backend\src\entities\usersProductAccess\entity\UsersProductAccess;
use common\src\entities\users\entity\Users;
use yii\helpers\ArrayHelper;

class PartnerCompanyQuery
{
    public static function getList(): array
    {
        $models = PartnerCompany::find()->select(['pc_id', 'pc_name'])->all();
        return ArrayHelper::map($models, 'pc_id', 'pc_name');
    }

    public static function getListByProduct(int $productId): array
    {
        $query = PartnerCompany::find()
            ->select(['pc_id', 'pc_name'])
            ->join('inner join', Users::tableName(), 'partner_company_id = pc_id')
            ->join('inner join', UsersProductAccess::tableName(), 'upa_user_id = id')
            ->andWhere(['upa_product_id' => $productId])
            ->andWhere(['pc_is_blocked' => false])
            ->groupBy(['pc_id', 'pc_name'])
            ->asArray()->all();
        return ArrayHelper::map($query, 'pc_id', 'pc_name');
    }
}