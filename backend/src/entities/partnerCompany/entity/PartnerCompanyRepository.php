<?php

namespace backend\src\entities\partnerCompany\entity;

class PartnerCompanyRepository
{
    public function save(PartnerCompany $model): int
    {
        if (!$model->save()) {
            throw new \RuntimeException('Partner Company saving failed: ' . $model->getErrorSummary(true)[0]);
        }
        return $model->pc_id;
    }
}