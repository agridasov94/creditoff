<?php

namespace backend\src\entities\partnerCompany\entity;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\src\entities\partnerCompany\entity\PartnerCompany;

/**
 * PartnerCompanySearch represents the model behind the search form of `backend\src\entities\partnerCompany\entity\PartnerCompany`.
 */
class PartnerCompanySearch extends PartnerCompany
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pc_id', 'pc_total_amount', 'pc_total_bonus_amount', 'pc_is_blocked', 'pc_created_user_id', 'pc_updated_user_id'], 'integer'],
            [['pc_name', 'pc_fiscal_code', 'pc_created_dt', 'pc_updated_dt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PartnerCompany::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'pc_id' => $this->pc_id,
            'pc_total_amount' => $this->pc_total_amount,
            'pc_total_bonus_amount' => $this->pc_total_bonus_amount,
            'pc_is_blocked' => $this->pc_is_blocked,
            'pc_created_user_id' => $this->pc_created_user_id,
            'pc_updated_user_id' => $this->pc_updated_user_id,
            'date(pc_created_dt)' => $this->pc_created_dt,
            'date(pc_updated_dt)' => $this->pc_updated_dt,
        ]);

        $query->andFilterWhere(['like', 'pc_name', $this->pc_name])
            ->andFilterWhere(['like', 'pc_fiscal_code', $this->pc_fiscal_code]);

        return $dataProvider;
    }
}
