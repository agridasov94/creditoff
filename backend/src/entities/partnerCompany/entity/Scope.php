<?php

namespace backend\src\entities\partnerCompany\entity;

/**
 * This is the ActiveQuery class for [[PartnerCompany]].
 *
 * @see PartnerCompany
 */
class Scope extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return PartnerCompany[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return PartnerCompany|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
