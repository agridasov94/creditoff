<?php

namespace backend\src\entities\partnerCompany\amountHistory\entity;

/**
 * This is the ActiveQuery class for [[PartnerCompanyAmountHistory]].
 *
 * @see PartnerCompanyAmountHistory
 */
class Scope extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return PartnerCompanyAmountHistory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return PartnerCompanyAmountHistory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
