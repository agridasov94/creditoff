<?php

namespace backend\src\entities\partnerCompany\amountHistory\entity;

class PartnerCompanyAmountHistoryRepository
{
    public function save(PartnerCompanyAmountHistory $model): int
    {
        if (!$model->save()) {
            throw new \RuntimeException('Partner company amount history saving failed: ' . $model->getErrorSummary(true)[0]);
        }
        return $model->pcah_id;
    }
}