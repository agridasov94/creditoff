<?php

namespace backend\src\entities\partnerCompany\amountHistory\entity;

use backend\src\entities\partnerCompany\entity\PartnerCompany;
use common\src\entities\users\entity\Users;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "partner_company_amount_history".
 *
 * @property int $pcah_id
 * @property int $pcah_partner_company_id
 * @property int|null $pcah_action
 * @property string|null $pcah_action_dt
 * @property int|null $pcah_amount_value
 * @property int|null $pcah_bonus_value
 * @property int|null $pcah_amount_before
 * @property int|null $pcah_amount_after
 * @property int|null $pcah_bonus_before
 * @property int|null $pcah_bonus_after
 * @property int|null $pcah_created_user_id
 *
 * @property Users $createdUser
 * @property PartnerCompany $partnerCompany
 */
class PartnerCompanyAmountHistory extends \yii\db\ActiveRecord
{
    public const ACTION_CRUD = 1;
    public const ACTION_MANAGE = 2;
    public const ACTION_REQUEST = 3;

    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['pcah_action_dt'],
                ],
                'value' => date('Y-m-d H:i:s') //new Expression('NOW()'),
            ],
            'user' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'pcah_created_user_id',
                'updatedByAttribute' => null
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'partner_company_amount_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pcah_partner_company_id'], 'required'],
            [['pcah_partner_company_id', 'pcah_action', 'pcah_amount_value', 'pcah_bonus_value', 'pcah_amount_before', 'pcah_amount_after', 'pcah_bonus_before', 'pcah_bonus_after', 'pcah_created_user_id'], 'integer'],
            [['pcah_action_dt'], 'safe'],
            [['pcah_created_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['pcah_created_user_id' => 'id']],
            [['pcah_partner_company_id'], 'exist', 'skipOnError' => true, 'targetClass' => PartnerCompany::class, 'targetAttribute' => ['pcah_partner_company_id' => 'pc_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'pcah_id' => Yii::t('partner', 'ID'),
            'pcah_partner_company_id' => Yii::t('partner', 'Partner Company'),
            'pcah_action' => Yii::t('partner', 'Action'),
            'pcah_action_dt' => Yii::t('partner', 'Action Dt'),
            'pcah_amount_value' => Yii::t('partner', 'Amount Value'),
            'pcah_bonus_value' => Yii::t('partner', 'Bonus Value'),
            'pcah_amount_before' => Yii::t('partner', 'Amount Before'),
            'pcah_amount_after' => Yii::t('partner', 'Amount After'),
            'pcah_bonus_before' => Yii::t('partner', 'Bonus Before'),
            'pcah_bonus_after' => Yii::t('partner', 'Bonus After'),
            'pcah_created_user_id' => Yii::t('partner', 'Created User'),
        ];
    }

    /**
     * Gets query for [[PcahCreatedUser]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\users\entity\Scope
     */
    public function getCreatedUser()
    {
        return $this->hasOne(Users::class, ['id' => 'pcah_created_user_id']);
    }

    /**
     * Gets query for [[PcahPartnerCompany]].
     *
     * @return \yii\db\ActiveQuery|\backend\src\entities\partnerCompany\entity\Scope
     */
    public function getPartnerCompany()
    {
        return $this->hasOne(PartnerCompany::class, ['pc_id' => 'pcah_partner_company_id']);
    }

    /**
     * {@inheritdoc}
     * @return Scope the active query used by this AR class.
     */
    public static function find()
    {
        return new Scope(get_called_class());
    }

    public static function getActionListName(): array
    {
        return [
            self::ACTION_CRUD => Yii::t('partner', 'Triggered by CRUD module'),
            self::ACTION_MANAGE => Yii::t('partner', 'Triggered by Manage amount/bonus module'),
            self::ACTION_REQUEST => Yii::t('partner', 'Triggered by Request')
        ];
    }

    public function getActionName(): ?string
    {
        return self::getActionListName()[$this->pcah_action] ?? null;
    }

    public static function create(
        int $partnerCompanyId,
        int $amountValue,
        int $bonusValue,
        int $amountBefore,
        int $amountAfter,
        int $bonusBefore,
        int $bonusAfter
    ): self {
        $self = new self();
        $self->pcah_partner_company_id = $partnerCompanyId;
        $self->pcah_amount_value = $amountValue;
        $self->pcah_bonus_value = $bonusValue;
        $self->pcah_amount_before = $amountBefore;
        $self->pcah_amount_after = $amountAfter;
        $self->pcah_bonus_before = $bonusBefore;
        $self->pcah_bonus_after = $bonusAfter;
        return $self;
    }

    public static function createByActionManage(
        int $partnerCompanyId,
        int $amountValue,
        int $bonusValue,
        int $amountBefore,
        int $amountAfter,
        int $bonusBefore,
        int $bonusAfter
    ): self {
        $self = self::create(
            $partnerCompanyId,
            $amountValue,
            $bonusValue,
            $amountBefore,
            $amountAfter,
            $bonusBefore,
            $bonusAfter,
        );
        $self->pcah_action = self::ACTION_MANAGE;
        return $self;
    }

    public static function createByActionCrud(
        int $partnerCompanyId,
        int $amountValue,
        int $bonusValue,
        int $amountBefore,
        int $amountAfter,
        int $bonusBefore,
        int $bonusAfter
    ): self {
        $self = self::create(
            $partnerCompanyId,
            $amountValue,
            $bonusValue,
            $amountBefore,
            $amountAfter,
            $bonusBefore,
            $bonusAfter,
        );
        $self->pcah_action = self::ACTION_CRUD;
        return $self;
    }

    public static function createByActionRequest(
        int $partnerCompanyId,
        int $amountValue,
        int $bonusValue,
        int $amountBefore,
        int $amountAfter,
        int $bonusBefore,
        int $bonusAfter
    ): self
    {
        $self = self::create(
            $partnerCompanyId,
            $amountValue,
            $bonusValue,
            $amountBefore,
            $amountAfter,
            $bonusBefore,
            $bonusAfter,
        );
        $self->pcah_action = self::ACTION_REQUEST;
        return $self;
    }
}
