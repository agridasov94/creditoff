<?php

namespace backend\src\entities\partnerCompany\amountHistory\entity;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\src\entities\partnerCompany\amountHistory\entity\PartnerCompanyAmountHistory;

/**
 * PartnerCompanyAmountHistorySearch represents the model behind the search form of `backend\src\entities\partnerCompany\amountHistory\entity\PartnerCompanyAmountHistory`.
 */
class PartnerCompanyAmountHistorySearch extends PartnerCompanyAmountHistory
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pcah_id', 'pcah_partner_company_id', 'pcah_action', 'pcah_amount_value', 'pcah_bonus_value', 'pcah_amount_before', 'pcah_amount_after', 'pcah_bonus_before', 'pcah_bonus_after', 'pcah_created_user_id'], 'integer'],
            [['pcah_action_dt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PartnerCompanyAmountHistory::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['pcah_id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'pcah_id' => $this->pcah_id,
            'pcah_partner_company_id' => $this->pcah_partner_company_id,
            'pcah_action' => $this->pcah_action,
            'date(pcah_action_dt)' => $this->pcah_action_dt,
            'pcah_amount_value' => $this->pcah_amount_value,
            'pcah_bonus_value' => $this->pcah_bonus_value,
            'pcah_amount_before' => $this->pcah_amount_before,
            'pcah_amount_after' => $this->pcah_amount_after,
            'pcah_bonus_before' => $this->pcah_bonus_before,
            'pcah_bonus_after' => $this->pcah_bonus_after,
            'pcah_created_user_id' => $this->pcah_created_user_id,
        ]);

        return $dataProvider;
    }
}
