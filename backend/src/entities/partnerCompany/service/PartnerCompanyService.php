<?php

namespace backend\src\entities\partnerCompany\service;

use backend\src\entities\partnerCompany\amountHistory\entity\PartnerCompanyAmountHistory;
use backend\src\entities\partnerCompany\amountHistory\entity\PartnerCompanyAmountHistoryRepository;
use backend\src\entities\partnerCompany\entity\PartnerCompany;
use backend\src\entities\partnerCompany\entity\PartnerCompanyRepository;

class PartnerCompanyService
{
    private PartnerCompanyRepository $repository;
    private PartnerCompanyAmountHistoryRepository $historyRepository;

    public function __construct(
        PartnerCompanyRepository $repository,
        PartnerCompanyAmountHistoryRepository $historyRepository
    ) {
        $this->repository = $repository;
        $this->historyRepository = $historyRepository;
    }

    public function withdrawalFunds(PartnerCompany $partnerCompany, int $withdrawalAmount): void
    {
        $amountBefore = $partnerCompany->pc_total_amount;
        $bonusBefore = $partnerCompany->pc_total_bonus_amount;

        $partnerCompany->withdrawalBonusFunds($withdrawalAmount);
        if ($partnerCompany->isBonusAmountNegative()) {
            $partnerCompany->resetWithdrawalBonusFunds();
            $partnerCompany->withdrawalMainFunds($withdrawalAmount);
            if ($partnerCompany->isMainAmountNegative()) {
                $partnerCompany->resetWithdrawalMainFunds();
                throw new \RuntimeException(\Yii::t('loan_application', 'Insufficient funds in the company account'));
            }
        }
        $this->repository->save($partnerCompany);

        $history = PartnerCompanyAmountHistory::createByActionRequest(
            $partnerCompany->pc_id,
            $withdrawalAmount,
            0,
            $amountBefore,
            $partnerCompany->pc_total_amount,
            $bonusBefore,
            $partnerCompany->pc_total_bonus_amount
        );
        $this->historyRepository->save($history);
    }
}