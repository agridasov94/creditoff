<?php

namespace backend\src\entities\usersNotifications\entity;

use backend\src\helpers\purifier\Purifier;
use backend\src\helpers\purifier\PurifierFilter;
use backend\src\helpers\ViewHelper;
use yii\helpers\Html;
use yii\helpers\StringHelper;

class NotificationMessage
{
    private const COMMAND_ADD = 'add';
    private const COMMAND_DELETE = 'delete';
    private const COMMAND_DELETE_ALL = 'delete_all';

    /**
     * @param UsersNotifications $ntf
     * @return array
     */
    public static function add(UsersNotifications $ntf): array
    {
        $message = Purifier::purify($ntf->un_message, PurifierFilter::shortCodeToLink());
        return [
            'notification' => [
                'command' => self::COMMAND_ADD,
                'userId' => $ntf->un_user_id,
                'id' => $ntf->un_id,
                'title' => Html::encode($ntf->un_title),
                'time' => strtotime($ntf->un_created_dt),
                'message' => StringHelper::truncate(ViewHelper::stripHtmlTags($message), 80, '...'),
                'type' => UsersNotifications::getNotifyType($ntf->un_type_id),
                'popup' => (int)$ntf->un_popup,
                'notifyMessage' => (int)$ntf->un_popup ? str_replace(["\r\n", "\n", '"'], ['', '', '\"'], strip_tags($message)) : '',
                'notifyDesktopMessage' => (int)$ntf->un_popup ? str_replace('"', '\"', strip_tags($message)) : '',
            ]
        ];
    }

    public static function delete(UsersNotifications $ntf): array
    {
        return [
            'notification' => [
                'command' => self::COMMAND_DELETE,
                'userId' => $ntf->un_user_id,
                'id' => $ntf->un_id,
            ]
        ];
    }

    public static function deleteAll(int $user_id): array
    {
        return [
            'notification' => [
                'command' => self::COMMAND_DELETE_ALL,
                'userId' => $user_id,
            ]
        ];
    }
}