<?php

namespace backend\src\entities\usersNotifications\entity;

/**
 * This is the ActiveQuery class for [[UsersNotifications]].
 *
 * @see UsersNotifications
 */
class Scope extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return UsersNotifications[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return UsersNotifications|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
