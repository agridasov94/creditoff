<?php

namespace backend\src\entities\usersNotifications\entity;

use common\src\entities\users\entity\Users;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "users_notifications".
 *
 * @property int $un_id
 * @property string|null $un_unique_id
 * @property int $un_user_id
 * @property int $un_type_id
 * @property string|null $un_title
 * @property string|null $un_message
 * @property int|null $un_new
 * @property int|null $un_deleted
 * @property int|null $un_popup
 * @property int|null $un_popup_show
 * @property string|null $un_read_dt
 * @property string|null $un_created_dt
 *
 * @property Users $user
 */
class UsersNotifications extends \yii\db\ActiveRecord
{
    public const TYPE_SUCCESS = 1;
    public const TYPE_INFO = 2;
    public const TYPE_WARNING = 3;
    public const TYPE_DANGER = 4;

    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['un_created_dt'],
                ],
                'value' => date('Y-m-d H:i:s') //new Expression('NOW()'),
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users_notifications';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['un_user_id', 'un_type_id'], 'required'],
            [['un_user_id', 'un_type_id', 'un_new', 'un_deleted', 'un_popup', 'un_popup_show'], 'integer'],
            [['un_message'], 'string'],
            [['un_read_dt', 'un_created_dt'], 'safe'],
            [['un_unique_id'], 'string', 'max' => 40],
            [['un_title'], 'string', 'max' => 100],
            [['un_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['un_user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'un_id' => Yii::t('user_notifications', 'ID'),
            'un_unique_id' => Yii::t('user_notifications', 'Unique ID'),
            'un_user_id' => Yii::t('user_notifications', 'User'),
            'un_type_id' => Yii::t('user_notifications', 'Type'),
            'un_title' => Yii::t('user_notifications', 'Title'),
            'un_message' => Yii::t('user_notifications', 'Message'),
            'un_new' => Yii::t('user_notifications', 'New'),
            'un_deleted' => Yii::t('user_notifications', 'Deleted'),
            'un_popup' => Yii::t('user_notifications', 'Popup'),
            'un_popup_show' => Yii::t('user_notifications', 'Popup Show'),
            'un_read_dt' => Yii::t('user_notifications', 'Read Date'),
            'un_created_dt' => Yii::t('user_notifications', 'Created Date'),
        ];
    }

    /**
     * Gets query for [[UnUser]].
     *
     * @return \yii\db\ActiveQuery|\common\src\entities\users\entity\Scope
     */
    public function getUser()
    {
        return $this->hasOne(Users::class, ['id' => 'un_user_id']);
    }

    /**
     * {@inheritdoc}
     * @return Scope the active query used by this AR class.
     */
    public static function find()
    {
        return new Scope(get_called_class());
    }

    /**
     * @param int $type_id
     * @return string
     */
    public static function getType(int $type_id): ?string
    {
        return self::getTypeList()[$type_id] ?? null;
    }

    /**
     * @return mixed
     */
    public static function getTypeList()
    {
        $list[self::TYPE_SUCCESS]   = Yii::t('users_notifications', 'Success');
        $list[self::TYPE_INFO]      = Yii::t('users_notifications', 'Info');
        $list[self::TYPE_WARNING]   = Yii::t('users_notifications', 'Warning');
        $list[self::TYPE_DANGER]    = Yii::t('users_notifications', 'Danger');

        return $list;
    }

    /**
     * @param $id
     * @return string
     */
    public static function getNotifyType($id): string
    {
        switch ($id) {
            case self::TYPE_SUCCESS:
                $str = 'success';
                break;
            case self::TYPE_INFO:
                $str = 'info';
                break;
            case self::TYPE_WARNING:
                $str = 'notice';
                break;
            case self::TYPE_DANGER:
                $str = 'error';
                break;
            default:
                $str = 'info';
        }
        return $str;
    }

    public static function create(
        int $userId = 0,
        string $title = '',
        string $message = '',
        int $type = 1,
        bool $popup = true
    ): self {
        $md5Hash = md5($message . $userId);
        $model = new self();
        $model->un_user_id = $userId;
        $model->un_title = $title;
        $model->un_message = $message;
        $model->un_type_id = $type;
        $model->un_popup = (int)$popup;
        $model->un_unique_id = $md5Hash;
        $model->un_new = 1;
        return $model;
    }

    public function isMustPopupShow(): bool
    {
        return $this->un_popup && !$this->un_popup_show;
    }

    public function isRead(): bool
    {
        return (bool)$this->un_read_dt;
    }

    public function setRead(\DateTimeImmutable $readDateTime): void
    {
        $this->un_read_dt = $readDateTime->format('Y-m-d H:i:s');
        $this->un_new = 0;
    }

    public function isOwner(int $id): bool
    {
        return $this->un_user_id === $id;
    }

    public function setDeleted(): void
    {
        $this->un_deleted = 1;
    }
}
