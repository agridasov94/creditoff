<?php

namespace backend\src\entities\usersNotifications\entity;

class UsersNotificationsQuery
{
    /**
     * @param int $user_id
     * @return mixed
     */
    public static function findNewCount(int $user_id = 0)
    {
        return UsersNotifications::find()->where(['un_user_id' => $user_id, 'un_new' => true, 'un_deleted' => false])->count();
    }

    /**
     * @param int $user_id
     * @return array|UsersNotifications[]
     */
    public static function findNew(int $user_id = 0)
    {
        return UsersNotifications::find()->where(['un_user_id' => $user_id, 'un_new' => true, 'un_deleted' => false])->limit(10)->orderBy(['un_id' => SORT_DESC])->all();
    }
}