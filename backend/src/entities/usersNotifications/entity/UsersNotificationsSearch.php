<?php

namespace backend\src\entities\usersNotifications\entity;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * UsersNotificationsSearch represents the model behind the search form of `backend\src\entities\usersNotifications\entity\UsersNotifications`.
 */
class UsersNotificationsSearch extends UsersNotifications
{
    public $datetime_start;
    public $datetime_end;
    public $date_range;
    public $companyId;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['un_id', 'un_user_id', 'un_type_id', 'un_new', 'un_deleted', 'un_popup', 'un_popup_show'], 'integer'],
            [['un_unique_id', 'un_title', 'un_message', 'un_read_dt', 'un_created_dt'], 'safe'],
            [['datetime_start', 'datetime_end'], 'safe'],
            [['date_range'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],
            [['un_created_dt', 'un_read_dt'], 'date', 'format' => 'php:Y-m-d'],
            [['companyId'], 'integer'],
            [['companyId'], 'filter', 'filter' => 'intval'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UsersNotifications::find()->with('user');
        $query->joinWith('user');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['un_id' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 40,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }

        if (empty($this->un_created_dt) && isset($params['UsersNotificationsSearch']['date_range'])) {
            $query->andFilterWhere(['>=', 'un_created_dt', $this->datetime_start])
                ->andFilterWhere(['<=', 'un_created_dt', $this->datetime_end]);
        }

        if ($this->un_created_dt) {
            $query->andFilterWhere(['>=', 'un_created_dt', $this->un_created_dt])
                ->andFilterWhere(['<=', 'un_created_dt', $this->un_created_dt]);
        }

        if ($this->un_read_dt) {
            $query->andFilterWhere(['>=', 'un_read_dt', $this->un_read_dt])
                ->andFilterWhere(['<=', 'un_read_dt', $this->un_read_dt]);
        }

        if ($this->companyId) {
            $query->andWhere(['partner_company_id' => $this->companyId]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'un_id' => $this->un_id,
            'un_user_id' => $this->un_user_id,
            'un_type_id' => $this->un_type_id,
            'un_new' => $this->un_new,
            'un_deleted' => $this->un_deleted,
            'un_popup' => $this->un_popup,
            'un_popup_show' => $this->un_popup_show,
        ]);

        $query->andFilterWhere(['like', 'un_title', $this->un_title])
            ->andFilterWhere(['like', 'un_message', $this->un_message]);

        return $dataProvider;
    }
}
