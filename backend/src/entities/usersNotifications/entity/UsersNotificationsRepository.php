<?php

namespace backend\src\entities\usersNotifications\entity;

class UsersNotificationsRepository
{
    public function save(UsersNotifications $model): int
    {
        if (!$model->save()) {
            throw new \RuntimeException($model->getErrorSummary(true)[0]);
        }
        return $model->un_id;
    }
}