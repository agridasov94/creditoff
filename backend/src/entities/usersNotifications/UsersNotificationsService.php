<?php

namespace backend\src\entities\usersNotifications;

use backend\src\entities\usersNotifications\entity\NotificationMessage;
use backend\src\entities\usersNotifications\entity\UsersNotifications;
use backend\src\entities\usersNotifications\entity\UsersNotificationsRepository;
use backend\src\helpers\purifier\Purifier;
use backend\src\helpers\purifier\PurifierFilter;
use backend\src\helpers\ViewHelper;
use backend\src\helpers\WebSocketHelper;
use common\src\interfaces\UsersNotificationInterface;
use yii\helpers\Html;
use yii\helpers\StringHelper;

/**
 * @property-read UsersNotificationsRepository $repository
 */
class UsersNotificationsService implements UsersNotificationInterface
{
    private UsersNotificationsRepository $repository;

    public function __construct(UsersNotificationsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function createAndPublish(int $userId, string $title, string $message, int $type, bool $popup = true): void
    {
        $notification = UsersNotifications::create($userId, $title, $message, $type, $popup);
        $this->repository->save($notification);
        WebSocketHelper::pub([WebSocketHelper::getUserPubSubKey($userId)], 'notification', NotificationMessage::add($notification));
    }

    public function makeRead(UsersNotifications $model): void
    {
        $model->setRead(new \DateTimeImmutable());
        $this->repository->save($model);
        $dataNotification = NotificationMessage::delete($model);
        WebSocketHelper::pub([WebSocketHelper::getUserPubSubKey($model->un_user_id)], 'notification', $dataNotification);
    }

    public function setDeleted(UsersNotifications $model): void
    {
        $model->setDeleted();
        $this->repository->save($model);
        $dataNotification = NotificationMessage::delete($model);
        WebSocketHelper::pub([WebSocketHelper::getUserPubSubKey($model->un_user_id)], 'notification', $dataNotification);
    }

    public function getTypeWarning(): int
    {
        return UsersNotifications::TYPE_WARNING;
    }
}