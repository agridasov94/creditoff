<?php

namespace backend\assets;

use yii\web\AssetBundle;

class NotifyAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        ['js/pnotify/pnotify5.custom.min.css']
    ];

    public $js = [
        ['js/pnotify/pnotify5.custom.min.js'],
        ['js/pnotify/pnotify5.bootstrap4.min.js'],
        ['js/pnotify/pnotify5.fontawesome.min.js'],
        ['js/pnotify/pnotify5.desktop.min.js'],
        ['js/pnotify/pnotify5.paginate.min.js'],
    ];

    public $depends = [
//        'yii\web\YiiAsset',
//        'yii\web\JqueryAsset',
//        'yii\bootstrap4\BootstrapAsset',
//        AppAsset::class
    ];

    public $jsOptions = [
        'position' => \yii\web\View::POS_END
    ];
}