<?php
namespace backend\assets;

use yii\web\AssetBundle;

class GlyphiconAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/glyphicon/only_glyphicons.min.css',
    ];
}