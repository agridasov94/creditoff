<?php

namespace backend\assets;

use yii\bootstrap4\BootstrapPluginAsset;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css',
        'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css',
        'css/custom.min.css',
        'css/site.css',
    ];
    public $js = [
        'js/custom.min.js',
        'js/reconnecting-websocket.min.js',
        'js/websocket.js',
        'js/app.js'
    ];
    public $depends = [
        JqueryAsset::class,
        \yii\bootstrap4\BootstrapAsset::class,
        BootstrapPluginAsset::class,
        GlyphiconAsset::class,
        NotifyAsset::class
    ];
}
