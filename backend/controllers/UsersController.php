<?php

namespace backend\controllers;

use backend\src\auth\Auth;
use backend\src\useCase\users\manage\UserManageForm;
use backend\src\useCase\users\manage\UsersManageService;
use common\src\entities\users\entity\Users;
use common\src\entities\users\entity\UsersSearch;
use common\src\forms\login\LoginForm;
use common\src\helpers\app\AppHelper;
use frontend\src\forms\login\LoginPartnerForm;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;
use yii2mod\rbac\filters\AccessControl;

/**
 * @property-read UsersManageService $usersManageService
 */
class UsersController extends AuthController
{
    private UsersManageService $usersManageService;

    public function __construct($id, $module, UsersManageService $usersManageService, $config = [])
    {
        $this->usersManageService = $usersManageService;
        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors(): array
    {
        $behaviors = [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['list-ajax'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
        return ArrayHelper::merge(parent::behaviors(), $behaviors);
    }

    public function actionIndex()
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
   public function actionView($id)
   {
       return $this->render('view', [
           'model' => $this->findModel($id),
       ]);
   }

    /**
     * Creates a new Users model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserManageForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user = $this->usersManageService->create($model);
            if ($user !== null && !$model->hasErrors()) {
                return $this->redirect(['/users/view', 'id' => $user->id]);
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }
    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $userForm = new UserManageForm();
        $userForm->setScenario(UserManageForm::SCENARIO_UPDATE);
        $userForm->fillInByModel($model);
        if ($userForm->load(Yii::$app->request->post()) && $userForm->validate()) {
            $this->usersManageService->update($userForm, $model, Auth::id());
            if (!$userForm->hasErrors()) {
                return $this->redirect(['/users/view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $userForm,
            'user' => $model
        ]);
    }
    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        try {
            $this->findModel($id)->delete();
        } catch (yii\db\Exception $e) {
            Yii::error(AppHelper::throwableLog($e), 'backend::UsersController::DbException');
            if (mb_strpos($e->getMessage(), 'a foreign key constraint fails')) {
                Yii::$app->session->setFlash('warning', Yii::t('backend', 'Unable to delete a user due to the fact that he has dependencies in the system'));
            }
        }
        return $this->redirect(['index']);
    }
    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException(Yii::t('users', 'The requested page does not exist.'));
    }

    /**
     * @param string|null $q
     * @param int|null $id
     * @return array
     */
    public function actionListAjax(?string $q = null, ?int $id = null): array
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $out = ['results' => ['id' => '', 'text' => '', 'selection' => '']];

        if ($q !== null) {
            $query = Users::find();
            $data = $query->select(['id', 'text' => 'username'])
                ->where(['like', 'username', $q])
                ->orWhere(['id' => (int) $q])
                ->limit(20)
                //->indexBy('id')
                ->asArray()
                ->all();

            if ($data) {
                foreach ($data as $n => $item) {
                    $text = $item['text'] . ' (' . $item['id'] . ')';
                    $data[$n]['text'] = $this->formatText($text, $q);
                    $data[$n]['selection'] = $item['text'];
                }
            }

            $out['results'] = $data; //array_values($data);
        } elseif ($id > 0) {
            $user = Users::findOne($id);
            $out['results'] = ['id' => $id, 'text' => $user ? $user->username : '', 'selection' => $user ? $user->username : ''];
        }
        return $out;
    }

    public function actionBlock($id)
    {
        $model = $this->findModel($id);
        $model->setInActiveStatus();
        if ($model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('users', 'User: {username} was blocked', [
                'username' => $model->username
            ]));
            return $this->redirect(['/users/index']);
        }

        Yii::$app->session->setFlash('error', $model->getErrorSummary(true)[0]);
        return $this->goBack();
    }

    public function actionSwitch()
    {
        $user_id = Yii::$app->request->get('id');
        $user = Users::findOne($user_id);
        if ($user) {
            if (Yii::$app->user->login($user)) {
                (new LoginPartnerForm)->sendWsIdentityCookie(Yii::$app->user->identity, 0);
            } else {
                echo 'Not logined';
                exit;
            }
        }
        return $this->goHome();
    }

    /**
     * @param string $str
     * @param string $term
     * @return string
     */
    private function formatText(string $str, string $term): string
    {
        return preg_replace('~' . $term . '~i', '<b style="color: #e15554"><u>$0</u></b>', $str);
    }
}