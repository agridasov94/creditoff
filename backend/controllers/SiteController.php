<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\src\forms\login\LoginForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'logout' => ['post'],
//                ],
//            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'view' => '@backend/views/site/error',
                'layout' => '@backend/views/layouts/error'
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        return $this->redirect(Yii::$app->params['frontendHost'] . '/partner/login');
    }

    /**
     * Logout action.
     *
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        Yii::$app->user->identity->logoutHandler();
        Yii::$app->user->logout();
        $this->removeWsIdentityCookie();
        return $this->redirect(Yii::$app->params['frontendHost'] . '/partner/login');
    }

    /**
     * @throws \yii\base\InvalidConfigException
     */
    private function removeWsIdentityCookie(): void
    {
        $identityCookie = Yii::$app->params['backWsIdentityCookie'] ?? [];
        Yii::$app->getResponse()->getCookies()->remove(Yii::createObject(array_merge($identityCookie, [
            'class' => 'yii\web\Cookie',
        ])));
    }
}
