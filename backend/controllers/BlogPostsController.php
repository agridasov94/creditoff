<?php

namespace backend\controllers;

use backend\src\auth\Auth;
use backend\src\services\blog\BlogManageService;
use backend\src\useCase\blogCreate\BlogPostCreateForm;
use common\src\helpers\app\AppHelper;
use Yii;
use common\src\entities\blog\posts\entity\BlogPosts;
use common\src\entities\blog\posts\entity\BlogPostsSearch;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii2mod\rbac\filters\AccessControl;

/**
 * BlogPostsController implements the CRUD actions for BlogPosts model.
 *
 * @property BlogManageService $blogManageService
 */
class BlogPostsController extends AuthController
{
    private BlogManageService $blogManageService;

    public function __construct($id, $module, BlogManageService $blogManageService, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->blogManageService = $blogManageService;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['generate-url'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
        return ArrayHelper::merge(parent::behaviors(), $behaviors);
    }

    /**
     * Lists all BlogPosts models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BlogPostsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BlogPosts model.
     * @param integer $bp_id
     * @param string $bp_lang_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($bp_id, $bp_lang_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($bp_id, $bp_lang_id),
        ]);
    }

    /**
     * Creates a new BlogPosts model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BlogPostCreateForm();

        if ($model->load(Yii::$app->request->post())) {
            $model->preview = UploadedFile::getInstance($model, 'preview');
            if ($model->validate()) {
                $this->blogManageService->create($model);

                if (!$model->hasErrors()) {
                    Yii::$app->session->setFlash('success', Yii::t('backend.blog_posts', 'Blog Post created successfully'));
                    return $this->redirect(['index']);
                }
            }
        }

        if (empty($model->authorId)) {
            $model->authorId = Auth::id();
        }
        if (empty($model->langId)) {
            $model->langId = Yii::$app->language;
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing BlogPosts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $bp_id
     * @param string $bp_lang_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($bp_id, $bp_lang_id)
    {
        $model = $this->findModel($bp_id, $bp_lang_id);

        if ($model->load(Yii::$app->request->post())) {
            $model->preview = UploadedFile::getInstance($model, 'preview');

            if ($model->validate()) {
                $this->blogManageService->update($model);
            }

            if (!$model->hasErrors()) {
                Yii::$app->session->setFlash('success', Yii::t('backend.blog_posts', 'Blog Post updated successfully'));
                return $this->redirect(['view', 'bp_id' => $model->bp_id, 'bp_lang_id' => $model->bp_lang_id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing BlogPosts model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $bp_id
     * @param string $bp_lang_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($bp_id, $bp_lang_id)
    {
        $model = $this->findModel($bp_id, $bp_lang_id);
        $model->delete();

        FileHelper::removeDirectory($model->getPreviewFrontendPath());
        FileHelper::removeDirectory($model->getPreviewBackendPath());

        return $this->redirect(['index']);
    }

    /**
     * Finds the BlogPosts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $bp_id
     * @param string $bp_lang_id
     * @return BlogPosts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($bp_id, $bp_lang_id)
    {
        if (($model = BlogPosts::findOne(['bp_id' => $bp_id, 'bp_lang_id' => $bp_lang_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('backend.blog_posts', 'The requested page does not exist.'));
    }

    public function actionGenerateUrl()
    {
        $title = Yii::$app->request->get('title');
        return AppHelper::stringToUrl($title, 255);
    }
}
