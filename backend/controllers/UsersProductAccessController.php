<?php

namespace backend\controllers;

use Yii;
use backend\src\entities\usersProductAccess\entity\UsersProductAccess;
use backend\src\entities\usersProductAccess\entity\UsersProductAccessSearch;
use backend\controllers\AuthController;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UsersProductAccessController implements the CRUD actions for UsersProductAccess model.
 */
class UsersProductAccessController extends AuthController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
        return ArrayHelper::merge(parent::behaviors(), $behaviors);
    }

    /**
     * Lists all UsersProductAccess models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsersProductAccessSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UsersProductAccess model.
     * @param integer $upa_user_id
     * @param integer $upa_product_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($upa_user_id, $upa_product_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($upa_user_id, $upa_product_id),
        ]);
    }

    /**
     * Creates a new UsersProductAccess model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UsersProductAccess();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'upa_user_id' => $model->upa_user_id, 'upa_product_id' => $model->upa_product_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing UsersProductAccess model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $upa_user_id
     * @param integer $upa_product_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($upa_user_id, $upa_product_id)
    {
        $model = $this->findModel($upa_user_id, $upa_product_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'upa_user_id' => $model->upa_user_id, 'upa_product_id' => $model->upa_product_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing UsersProductAccess model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $upa_user_id
     * @param integer $upa_product_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($upa_user_id, $upa_product_id)
    {
        $this->findModel($upa_user_id, $upa_product_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UsersProductAccess model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $upa_user_id
     * @param integer $upa_product_id
     * @return UsersProductAccess the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($upa_user_id, $upa_product_id)
    {
        if (($model = UsersProductAccess::findOne(['upa_user_id' => $upa_user_id, 'upa_product_id' => $upa_product_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('users', 'The requested page does not exist.'));
    }
}
