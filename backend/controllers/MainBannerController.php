<?php

namespace backend\controllers;

use common\src\entities\mainBanner\useCase\CreateForm;
use common\src\entities\mainBanner\useCase\MainBannerService;
use common\src\entities\mainBanner\useCase\UpdateForm;
use common\src\helpers\app\AppHelper;
use Yii;
use common\src\entities\mainBanner\entity\MainBanner;
use common\src\entities\mainBanner\entity\MainBannerSearch;
use backend\controllers\AuthController;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * MainBannerController implements the CRUD actions for MainBanner model.
 */
class MainBannerController extends AuthController
{
    private MainBannerService $bannerService;

    public function __construct($id, $module, MainBannerService $bannerService, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->bannerService = $bannerService;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
        return ArrayHelper::merge(parent::behaviors(), $behaviors);
    }

    /**
     * Lists all MainBanner models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MainBannerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MainBanner model.
     * @param integer $mb_id
     * @param string $mb_lang_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($mb_id, $mb_lang_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($mb_id, $mb_lang_id),
        ]);
    }

    /**
     * Creates a new MainBanner model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $form = new CreateForm();

        if ($form->load(Yii::$app->request->post())) {
            $form->file = UploadedFile::getInstance($form, 'file');
            if ($form->validate()) {
                try {
                    $this->bannerService->create($form);

                    Yii::$app->session->setFlash('success', Yii::t('backend.main_banner', 'Banner for main block created successfully'));
                    return $this->redirect(['index']);
                } catch (\RuntimeException $e) {
                    Yii::error(AppHelper::throwableLog($e), 'MainBannerController:actionCreate:RuntimeException');
                } catch (\Throwable $e) {
                    Yii::error(AppHelper::throwableLog($e), 'MainBannerController:actionCreate:Throwable');
                }
            }
        }

        if (empty($form->langId)) {
            $form->langId = Yii::$app->language;
        }

        return $this->render('create', [
            'model' => $form,
        ]);
    }

    /**
     * Updates an existing MainBanner model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $mb_id
     * @param string $mb_lang_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($mb_id, $mb_lang_id)
    {
        $model = $this->findModel($mb_id, $mb_lang_id);

        $form = new UpdateForm();
        if ($form->load(Yii::$app->request->post())) {
            $form->file = UploadedFile::getInstance($form, 'file');

            if ($form->validate()) {
                $this->bannerService->update($form, $model);
            }

            if (!$form->hasErrors()) {
                Yii::$app->session->setFlash('success', Yii::t('backend.blog_posts', 'Blog Post updated successfully'));
                return $this->redirect(['view', 'mb_id' => $mb_id, 'mb_lang_id' => $mb_lang_id]);
            }
        }

        $form->fillByModel($model);

        return $this->render('update', [
            'model' => $form,
            'banner' => $model
        ]);
    }

    /**
     * Deletes an existing MainBanner model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $mb_id
     * @param string $mb_lang_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($mb_id, $mb_lang_id)
    {
        $model = $this->findModel($mb_id, $mb_lang_id);
        $model->delete();

        FileHelper::unlink($model->getFileFrontendImagePath());
        FileHelper::unlink($model->getFileBackendImagePath());

        return $this->redirect(['index']);
    }

    /**
     * Finds the MainBanner model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $mb_id
     * @param string $mb_lang_id
     * @return MainBanner the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($mb_id, $mb_lang_id)
    {
        if (($model = MainBanner::findOne(['mb_id' => $mb_id, 'mb_lang_id' => $mb_lang_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('main_banner', 'The requested page does not exist.'));
    }
}
