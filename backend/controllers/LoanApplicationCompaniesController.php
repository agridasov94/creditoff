<?php

namespace backend\controllers;

use Yii;
use common\src\entities\loanApplication\companies\entity\LoanApplicationCompanies;
use common\src\entities\loanApplication\companies\entity\LoanApplicationCompaniesSearch;
use backend\controllers\AuthController;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LoanApplicationCompaniesController implements the CRUD actions for LoanApplicationCompanies model.
 */
class LoanApplicationCompaniesController extends AuthController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
        return ArrayHelper::merge(parent::behaviors(), $behaviors);
    }

    /**
     * Lists all LoanApplicationCompanies models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LoanApplicationCompaniesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LoanApplicationCompanies model.
     * @param integer $lac_la_id
     * @param integer $lac_partner_company_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($lac_la_id, $lac_partner_company_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($lac_la_id, $lac_partner_company_id),
        ]);
    }

    /**
     * Creates a new LoanApplicationCompanies model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LoanApplicationCompanies();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'lac_la_id' => $model->lac_la_id, 'lac_partner_company_id' => $model->lac_partner_company_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing LoanApplicationCompanies model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $lac_la_id
     * @param integer $lac_partner_company_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($lac_la_id, $lac_partner_company_id)
    {
        $model = $this->findModel($lac_la_id, $lac_partner_company_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'lac_la_id' => $model->lac_la_id, 'lac_partner_company_id' => $model->lac_partner_company_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing LoanApplicationCompanies model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $lac_la_id
     * @param integer $lac_partner_company_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($lac_la_id, $lac_partner_company_id)
    {
        $this->findModel($lac_la_id, $lac_partner_company_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LoanApplicationCompanies model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $lac_la_id
     * @param integer $lac_partner_company_id
     * @return LoanApplicationCompanies the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($lac_la_id, $lac_partner_company_id)
    {
        if (($model = LoanApplicationCompanies::findOne(['lac_la_id' => $lac_la_id, 'lac_partner_company_id' => $lac_partner_company_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('loan_application', 'The requested page does not exist.'));
    }
}
