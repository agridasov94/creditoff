<?php

namespace backend\controllers;

use backend\src\auth\Auth;
use backend\src\repositories\users\UserRepository;
use backend\src\useCase\users\manage\UserManageForm;
use common\src\entities\users\entity\Users;
use common\src\helpers\app\AppHelper;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class ProfileController extends AuthController
{
    private UserRepository $userRepository;

    public function __construct($id, $module, UserRepository $userRepository, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->userRepository = $userRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
        return ArrayHelper::merge(parent::behaviors(), $behaviors);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionManage(): string
    {
        $user = $this->findModel(Auth::id());

        $manageForm = new UserManageForm();
        $manageForm->setScenario(UserManageForm::SCENARIO_UPDATE_MY_PROFILE);
        if (\Yii::$app->request->isPost && $manageForm->load(\Yii::$app->request->post()) && $manageForm->validate()) {
            $user->email = $manageForm->email;
            $user->phone = $manageForm->phone;
            if (!empty($manageForm->password)) {
                $user->setPassword($manageForm->password);
            }
            try {
                $this->userRepository->save($user);
                \Yii::$app->session->setFlash('success', \Yii::t('profile', 'Profile updated successfully'));
                $manageForm = new UserManageForm();
                $manageForm->setScenario(UserManageForm::SCENARIO_UPDATE_MY_PROFILE);
            } catch (\RuntimeException $e) {
                $manageForm->addError('general', $e->getMessage());
            } catch (\Throwable $e) {
                $manageForm->addError('general', \Yii::t('backend', 'Internal Server Error'));
                \Yii::error(AppHelper::throwableLog($e, true), 'ProfileController::profile::manage');
            }
        }
        $manageForm->fillInByModel($user);

        return $this->render('manage', [
            'user' => $user,
            'manageForm' => $manageForm
        ]);
    }

    protected function findModel($id): Users
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException(\Yii::t('users', 'The requested page does not exist.'));
    }
}