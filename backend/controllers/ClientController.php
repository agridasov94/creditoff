<?php

namespace backend\controllers;

use backend\src\helpers\client\ClientExportHelper;
use backend\src\useCase\client\SoftDeleteService;
use backend\src\useCase\clientCreate\ChildrenForm;
use backend\src\useCase\clientCreate\ClientCreateForm;
use backend\src\useCase\clientCreate\ClientCreateService;
use backend\src\useCase\clientCreate\ClientEmailForm;
use backend\src\useCase\clientCreate\ClientEmploymentDataForm;
use backend\src\useCase\clientCreate\ClientPassportDataForm;
use backend\src\useCase\clientCreate\ClientPhoneForm;
use backend\src\useCase\clientUpdate\ClientUpdateForm;
use backend\src\useCase\clientUpdate\ClientUpdateService;
use frontend\src\forms\login\LoginPartnerForm;
use Yii;
use common\src\entities\client\entity\Client;
use common\src\entities\client\entity\ClientSearch;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii2mod\rbac\filters\AccessControl;

/**
 * ClientCrudController implements the CRUD actions for Client model.
 *
 * @property-read ClientCreateService $clientCreateService
 * @property-read ClientUpdateService $clientUpdateService
 * @property-read SoftDeleteService $softDeleteService
 * @property-read ClientExportHelper $clientExportHelper
 */
class ClientController extends AuthController
{
    private ClientCreateService $clientCreateService;
    private ClientUpdateService $clientUpdateService;
    private SoftDeleteService $softDeleteService;
    private ClientExportHelper $clientExportHelper;

    public function __construct(
        $id,
        $module,
        ClientCreateService $clientCreateService,
        ClientUpdateService $clientUpdateService,
        SoftDeleteService $softDeleteService,
        ClientExportHelper $clientExportHelper,
        $config = []
    ) {
        $this->clientCreateService = $clientCreateService;
        $this->clientUpdateService = $clientUpdateService;
        $this->softDeleteService = $softDeleteService;
        $this->clientExportHelper = $clientExportHelper;
        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['list-ajax'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
        return ArrayHelper::merge(parent::behaviors(), $behaviors);
    }

    /**
     * Lists all Client models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ClientSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'additionalExportColumns' => $this->clientExportHelper->getAdditionalExportColumns()
        ]);
    }

    /**
     * Displays a single Client model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Client model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $post = Yii::$app->request->post();

        $model = new ClientCreateForm(
            count($post[ChildrenForm::getFormName()] ?? []),
            count($post[ClientEmploymentDataForm::getFormName()] ?? []),
            count($post[ClientEmailForm::getFormName()] ?? []),
            count($post[ClientPhoneForm::getFormName()] ?? [])
        );

        if (isset($post[ChildrenForm::getFormName()])) {
            $post[ChildrenForm::getFormName()] = array_values($post[ChildrenForm::getFormName()]);
        }
        if (isset($post[ClientEmailForm::getFormName()])) {
            $post[ClientEmailForm::getFormName()] = array_values($post[ClientEmailForm::getFormName()]);
        }
        if (isset($post[ClientPhoneForm::getFormName()])) {
            $post[ClientPhoneForm::getFormName()] = array_values($post[ClientPhoneForm::getFormName()]);
        }
        if (isset($post[ClientEmploymentDataForm::getFormName()])) {
            $post[ClientEmploymentDataForm::getFormName()] = array_values($post[ClientEmploymentDataForm::getFormName()]);
        }

        if ($model->load($post) && $model->validate()) {
            try {
                $client = $this->clientCreateService->create($model);
                return $this->redirect(['view', 'id' => $client->c_id]);
            } catch (\Throwable $e) {
                $model->addError('general', $e->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Client model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $clientUpdateForm = new ClientUpdateForm($model);

        if ($clientUpdateForm->load(Yii::$app->request->post()) && $clientUpdateForm->validate()) {
            $this->clientUpdateService->update($clientUpdateForm, Yii::$app->request->post());
            if (!$clientUpdateForm->hasErrors()) {
                return $this->redirect(['view', 'id' => $model->c_id]);
            }
        }

        return $this->render('update', [
            'model' => $clientUpdateForm,
        ]);
    }

    /**
     * Deletes an existing Client model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $client = $this->findModel($id);

        if ($client->loanApplications) {
            Yii::$app->session->setFlash('error', Yii::t('client', 'Unable to delete a customer due to existing loan applications from the customer'));
        } else {
            $client->delete();
        }

        return $this->redirect(['index']);
    }

    public function actionSoftDelete($id)
    {
        $client = $this->findModel($id);
        if (!empty($client->c_deleted_dt)) {
            Yii::$app->session->setFlash('warning', Yii::t('client', 'Client already deleted'));
            return $this->redirect(['index']);
        }
        if (!$this->softDeleteService->encryptData($client)) {
            Yii::$app->session->setFlash('error', $this->softDeleteService->getErrorMessage());
        } else {
            Yii::$app->session->setFlash('success', Yii::t('client', 'User is soft deleted. All his sensitive data is encrypted.'));
        }
        return $this->redirect(['index']);
    }

    public function actionRestoreData($id)
    {
        $client = $this->findModel($id);
        if (empty($client->c_deleted_dt)) {
            Yii::$app->session->setFlash('warning', Yii::t('client', 'Clients data already restored'));
            return $this->redirect(['index']);
        }
        if (!$this->softDeleteService->decryptData($client)) {
            Yii::$app->session->setFlash('error', $this->softDeleteService->getErrorMessage());
        } else {
            Yii::$app->session->setFlash('success', Yii::t('client', 'User is restored. All his sensitive data is decrypted.'));
        }
        return $this->redirect(['index']);
    }

    public function actionSwitch()
    {
        $clientId = Yii::$app->request->get('id');
        $client = Client::findOne($clientId);
        if ($client) {
            Yii::$app->session->setName('advanced-frontend');
            if (Yii::$app->frontendUser->login($client, 0)) {
                (new LoginPartnerForm())->sendWsIdentityCookie($client, 0);
            } else {
                Yii::$app->session->setFlash('error', Yii::t('frontend', 'Cannot login to the client profile.'));
                return $this->redirect(['/client/index']);
            }
        }
        return $this->redirect(Yii::$app->params['frontendHost'] . '/cabinet/loan-application-list');
    }

    /**
     * @param string|null $q
     * @param int|null $id
     * @return array
     */
    public function actionListAjax(?string $q = null, ?int $id = null): array
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $out = ['results' => ['id' => '', 'text' => '', 'selection' => '']];

        if ($q !== null) {
            $query = Client::find();
            $data = $query->select(['id' => 'c_id', 'text' => new Expression('CONCAT(c_name, " ", c_last_name)')])
                ->where(['like', new Expression('CONCAT(c_name, " ", c_last_name)'), $q])
                ->orWhere(['c_id' => (int) $q])
                ->limit(20)
                //->indexBy('id')
                ->asArray()
                ->all();

            if ($data) {
                foreach ($data as $n => $item) {
                    $text = $item['text'] . ' (' . $item['id'] . ')';
                    $data[$n]['text'] = $this->formatText($text, $q);
                    $data[$n]['selection'] = $item['text'];
                }
            }

            $out['results'] = $data; //array_values($data);
        } elseif ($id > 0) {
            $client = Client::findOne($id);
            $fullName = $client ? $client->getFullName() : '';
            $out['results'] = ['id' => $id, 'text' => $fullName, 'selection' => $fullName];
        }
        return $out;
    }

    /**
     * Finds the Client model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Client the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Client::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('backend.client', 'The requested page does not exist.'));
    }

    /**
     * @param string $str
     * @param string $term
     * @return string
     */
    private function formatText(string $str, string $term): string
    {
        return preg_replace('~' . $term . '~i', '<b style="color: #e15554"><u>$0</u></b>', $str);
    }
}
