<?php

namespace backend\controllers;

use common\src\entities\language\LanguageQuery;
use common\src\helpers\app\AppHelper;
use Yii;
use common\src\entities\blog\categories\entity\BlogCategories;
use common\src\entities\blog\categories\entity\BlogCategoriesSearch;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii2mod\rbac\filters\AccessControl;

/**
 * BlogCategoriesController implements the CRUD actions for BlogCategories model.
 */
class BlogCategoriesController extends AuthController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['generate-url'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
        return ArrayHelper::merge(parent::behaviors(), $behaviors);
    }

    /**
     * Lists all BlogCategories models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BlogCategoriesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BlogCategories model.
     * @param integer $bc_id
     * @param string $bc_lang_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($bc_id, $bc_lang_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($bc_id, $bc_lang_id),
        ]);
    }

    /**
     * Creates a new BlogCategories model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BlogCategories();

        if ($model->load(Yii::$app->request->post())) {
            try {
                $activeLanguages = LanguageQuery::getList();
                foreach ($activeLanguages as $langId => $langAbbrv) {
                    $model->bc_lang_id = $langId;
                    if ($model->save()) {
                        $model->setIsNewRecord(true);
                    } else {
                        throw new \RuntimeException($model->getErrorSummary(true));
                    }
                }
                Yii::$app->session->setFlash('success', Yii::t('backend', 'Category successfully created'));
                return $this->redirect(['view', 'bc_id' => $model->bc_id, 'bc_lang_id' => $model->bc_lang_id]);
            } catch (\RuntimeException | Exception $e) {
                $model->addError('general', $e->getMessage());
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing BlogCategories model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $bc_id
     * @param string $bc_lang_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($bc_id, $bc_lang_id)
    {
        $model = $this->findModel($bc_id, $bc_lang_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('backend', 'Category successfully updated'));
            return $this->redirect(['view', 'bc_id' => $model->bc_id, 'bc_lang_id' => $model->bc_lang_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing BlogCategories model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $bc_id
     * @param string $bc_lang_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($bc_id, $bc_lang_id)
    {
        $this->findModel($bc_id, $bc_lang_id)->delete();
        Yii::$app->session->setFlash('success', Yii::t('backend', 'Category successfully deleted'));
        return $this->redirect(['index']);
    }

    /**
     * Finds the BlogCategories model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $bc_id
     * @param string $bc_lang_id
     * @return BlogCategories the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($bc_id, $bc_lang_id)
    {
        if (($model = BlogCategories::findOne(['bc_id' => $bc_id, 'bc_lang_id' => $bc_lang_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('backend', 'The requested page does not exist.'));
    }

    public function actionGenerateUrl()
    {
        $name = Yii::$app->request->get('name');
        return AppHelper::stringToUrl($name, 255);
    }
}
