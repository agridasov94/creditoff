<?php

namespace backend\controllers;

use backend\src\auth\Auth;
use backend\src\entities\usersNotifications\entity\NotificationMessage;
use backend\src\entities\usersNotifications\UsersNotificationsService;
use backend\src\helpers\WebSocketHelper;
use backend\src\useCase\notifications\createByUser\CreateForm;
use Yii;
use backend\src\entities\usersNotifications\entity\UsersNotifications;
use backend\src\entities\usersNotifications\entity\UsersNotificationsSearch;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UsersNotificationsController implements the CRUD actions for UsersNotifications model.
 */
class UsersNotificationsController extends AuthController
{
    private UsersNotificationsService $usersNotificationsService;

    public function __construct($id, $module, UsersNotificationsService $usersNotificationsService, $config = [])
    {
        $this->usersNotificationsService = $usersNotificationsService;
        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
        return ArrayHelper::merge(parent::behaviors(), $behaviors);
    }

    /**
     * Lists all UsersNotifications models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsersNotificationsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UsersNotifications model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UsersNotifications model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $form = new CreateForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                foreach ($form->users as $userId) {
                    $this->usersNotificationsService->createAndPublish($userId, $form->title, $form->message, $form->type, (int)$form->popup);
                }
                Yii::$app->session->setFlash('success', Yii::t('users_notifications', 'Notifications successfully created and sent to users'));
                return $this->redirect('index');
            } catch (\RuntimeException $e) {
                $form->addError('general', $e->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $form,
        ]);
    }

    /**
     * Updates an existing UsersNotifications model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->un_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing UsersNotifications model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $dataNotification = NotificationMessage::delete($model);
        $userId = $model->un_user_id;
        $model->delete();
        WebSocketHelper::pub([WebSocketHelper::getUserPubSubKey($userId)], 'notification', $dataNotification);
        return $this->redirect(['index']);
    }

    public function actionMyNotifications()
    {
        $searchModel = new UsersNotificationsSearch();

        $params = Yii::$app->request->queryParams;
        $params['UsersNotificationsSearch']['un_deleted'] = 0;
        $params['UsersNotificationsSearch']['un_user_id'] = Auth::id();

        $dataProvider = $searchModel->search($params);

        return $this->render('my-notifications', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return \yii\web\Response
     */
    public function actionAllRead()
    {
        $userId = Auth::id();
        UsersNotifications::updateAll(['un_new' => false, 'un_read_dt' => date('Y-m-d H:i:s')], ['un_read_dt' => null, 'un_user_id' => $userId]);
        $dataNotification = NotificationMessage::deleteAll($userId);
        WebSocketHelper::pub([WebSocketHelper::getUserPubSubKey($userId)], 'notification', $dataNotification);
        Yii::$app->session->setFlash('success', Yii::t('users_notifications', Yii::t('users_notifications', 'All notifications are marked as read')));
        return $this->redirect(['my-notifications']);
    }

    /**
     * @return \yii\web\Response
     */
    public function actionAllDelete()
    {
        $userId = Auth::id();
        UsersNotifications::updateAll(['un_deleted' => true], ['un_deleted' => false, 'un_user_id' => $userId]);
        $dataNotification = NotificationMessage::deleteAll($userId);
        WebSocketHelper::pub([WebSocketHelper::getUserPubSubKey($userId)], 'notification', $dataNotification);
        Yii::$app->session->setFlash('success', Yii::t('users_notifications', Yii::t('users_notifications', 'All notifications are deleted')));
        return $this->redirect(['my-notifications']);
    }

    /**
     * @param $id
     * @return string
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionViewNotification($id)
    {
        $model = $this->findModel($id);
        if (!$model->isOwner(Auth::id())) {
            throw new ForbiddenHttpException('Access denied.');
        }

        if (!$model->isRead()) {
            $this->usersNotificationsService->makeRead($model);
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('view-popup', [
                'model' => $model,
            ]);
        }

        return $this->render('view-notification', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws ForbiddenHttpException
     */
    public function actionSoftDelete($id)
    {
        $model = $this->findModel($id);
        if (!$model->isOwner(Auth::id())) {
            throw new ForbiddenHttpException('Access denied.');
        }
        $this->usersNotificationsService->setDeleted($model);
        Yii::$app->session->setFlash('success', Yii::t('users_notifications', Yii::t('users_notifications', 'Notification was successfully deleted')));
        return $this->redirect(['my-notifications']);
    }

    /**
     * Finds the UsersNotifications model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UsersNotifications the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UsersNotifications::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('users_notifications', 'The requested page does not exist.'));
    }
}
