<?php

namespace backend\controllers;

use backend\src\entities\partnerCompany\amountHistory\entity\PartnerCompanyAmountHistory;
use backend\src\useCase\partner\manageAmount\ManageAmountForm;
use backend\src\useCase\partner\manageAmount\ManageAmountService;
use common\src\entities\product\entity\ProductQuery;
use common\src\helpers\app\AppHelper;
use frontend\components\jobs\LoanApplicationDistributionJob;
use Yii;
use backend\src\entities\partnerCompany\entity\PartnerCompany;
use backend\src\entities\partnerCompany\entity\PartnerCompanySearch;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PartnerCompanyController implements the CRUD actions for PartnerCompany model.
 *
 * @property-read ManageAmountService $manageAmountService
 */
class PartnerCompanyController extends AuthController
{
    private ManageAmountService $manageAmountService;

    public function __construct($id, $module, ManageAmountService $manageAmountService, $config = [])
    {
        $this->manageAmountService = $manageAmountService;
        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
        return ArrayHelper::merge(parent::behaviors(), $behaviors);
    }

    /**
     * Lists all PartnerCompany models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PartnerCompanySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PartnerCompany model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PartnerCompany model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PartnerCompany();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->pc_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing PartnerCompany model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $amountBefore = $model->pc_total_amount;
        $bonusBefore = $model->pc_total_bonus_amount;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($amountBefore != $model->pc_total_amount || $bonusBefore != $model->pc_total_bonus_amount) {
                $amountDiff = $model->pc_total_amount - $amountBefore;
                $bonusDiff = $model->pc_total_bonus_amount - $bonusBefore;
                $history = PartnerCompanyAmountHistory::createByActionCrud(
                    $model->pc_id,
                    $amountDiff,
                    $bonusDiff,
                    $amountBefore,
                    $model->pc_total_amount,
                    $bonusBefore,
                    $model->pc_total_bonus_amount
                );
                $history->save();

                if ($amountBefore !== $model->pc_total_amount || $bonusBefore !== $model->pc_total_bonus_amount) {
                    $productIds = ProductQuery::getListIdsByUsersAssignedToCompany($model->pc_id);
                    foreach ($productIds as $productId) {
                        $job = new LoanApplicationDistributionJob((int)$productId);
                        /** @var $queue \yii\queue\beanstalk\Queue */
                        $queue = \Yii::$app->queue_loan_application_distribution;
                        $queue->push($job);
                    }
                }
            }

            return $this->redirect(['view', 'id' => $model->pc_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing PartnerCompany model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionManageAmount()
    {
        $form = new ManageAmountForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $this->manageAmountService->editPartnerAmountAndBonus($form);
            if (!$form->hasErrors()) {
                Yii::$app->session->setFlash('success', Yii::t('partner', 'Amount/Bonus value updated'));
                return $this->redirect(['partner-company/view', 'id' => $form->partnerCompanyId]);
            }
        }

        $models = PartnerCompany::find()->select(['id' => 'pc_id', 'amount' => 'pc_total_amount', 'bonus' => 'pc_total_bonus_amount'])->asArray()->all();
        $models = ArrayHelper::index($models, null, 'id');

        return $this->render('manage-amount', [
            'manageAmountForm' => $form,
            'models' => $models
        ]);
    }

    /**
     * Finds the PartnerCompany model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PartnerCompany the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PartnerCompany::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('partner', 'The requested page does not exist.'));
    }
}
