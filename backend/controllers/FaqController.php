<?php

namespace backend\controllers;

use backend\src\services\faq\FaqManageService;
use backend\src\useCase\faq\create\FAQCreateForm;
use Yii;
use common\src\entities\faq\entity\Faq;
use common\src\entities\faq\entity\FaqSearch;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FaqController implements the CRUD actions for Faq model.
 */
class FaqController extends AuthController
{
    private FaqManageService $faqManageService;

    public function __construct($id, $module, FaqManageService $faqManageService, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->faqManageService = $faqManageService;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
        return ArrayHelper::merge(parent::behaviors(), $behaviors);
    }

    /**
     * Lists all Faq models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FaqSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Faq model.
     * @param integer $f_id
     * @param string $f_lang_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($f_id, $f_lang_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($f_id, $f_lang_id),
        ]);
    }

    /**
     * Creates a new Faq model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FAQCreateForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $faq = $this->faqManageService->create($model);
            return $this->redirect(['view', 'f_id' => $faq->f_id, 'f_lang_id' => $faq->f_lang_id]);
        }

        if (empty($model->langId)) {
            $model->langId = Yii::$app->language;
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Faq model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $f_id
     * @param string $f_lang_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($f_id, $f_lang_id)
    {
        $model = $this->findModel($f_id, $f_lang_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'f_id' => $model->f_id, 'f_lang_id' => $model->f_lang_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Faq model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $f_id
     * @param string $f_lang_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($f_id, $f_lang_id)
    {
        $this->findModel($f_id, $f_lang_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Faq model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $f_id
     * @param string $f_lang_id
     * @return Faq the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($f_id, $f_lang_id)
    {
        if (($model = Faq::findOne(['f_id' => $f_id, 'f_lang_id' => $f_lang_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('faq', 'The requested page does not exist.'));
    }
}
