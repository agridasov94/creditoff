<?php

namespace backend\controllers;

use backend\src\auth\Auth;
use backend\src\entities\partnerCompany\service\PartnerCompanyService;
use backend\src\entities\usersProductAccess\entity\UsersProductAccessQuery;
use backend\src\forms\DeclineForm;
use backend\src\helpers\client\ClientExportHelper;
use backend\src\helpers\WebSocketContainer;
use backend\src\useCase\loanApplication\manualDistribution\ManualDistributionForm;
use backend\src\useCase\loanApplication\manualDistribution\ManualDistributionService;
use common\src\entities\loanApplication\entity\LoanApplicationMultipleSearch;
use common\src\entities\loanApplication\entity\LoanApplicationQuery;
use common\src\entities\loanApplication\entity\LoanApplicationRepository;
use common\src\entities\loanApplication\entity\LoanApplicationStatus;
use common\src\entities\loanApplicationAccess\entity\LoanApplicationAccessQuery;
use common\src\entities\loanApplicationAccess\entity\LoanApplicationAccessRepository;
use common\src\entities\loanApplicationDistribution\entity\LoanApplicationDistribution;
use common\src\entities\loanApplicationDistribution\entity\LoanApplicationDistributionQuery;
use common\src\helpers\app\AppHelper;
use common\src\services\LoanApplicationDistributionService;
use frontend\components\jobs\LoanApplicationDistributionJob;
use Yii;
use common\src\entities\loanApplication\entity\LoanApplication;
use common\src\entities\loanApplication\entity\LoanApplicationSearch;
use yii\db\Transaction;
use yii\helpers\ArrayHelper;
use yii\redis\Connection;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LoanApplicationController implements the CRUD actions for LoanApplication model.
 *
 * @property-read LoanApplicationRepository LoanApplicationRepository
 * @property-read LoanApplicationAccessRepository $loanApplicationAccessRepository
 * @property-read ManualDistributionService $manualDistributionService
 */
class LoanApplicationController extends AuthController
{
    private const RESERVE_REQUEST_TO_PROCESS_KEY = 'reserve_request_to_process_key_';

    private LoanApplicationRepository $loanApplicationRepository;
    private LoanApplicationAccessRepository $loanApplicationAccessRepository;
    private ManualDistributionService $manualDistributionService;
    private ClientExportHelper $clientExportHelper;
    private LoanApplicationDistributionService $applicationDistributionService;
    private PartnerCompanyService $partnerCompanyService;

    public function __construct(
        $id,
        $module,
        LoanApplicationRepository $loanApplicationRepository,
        LoanApplicationAccessRepository $loanApplicationAccessRepository,
        ManualDistributionService $manualDistributionService,
        ClientExportHelper $clientExportHelper,
        LoanApplicationDistributionService $applicationDistributionService,
        PartnerCompanyService $partnerCompanyService,
        $config = []
    ) {
        parent::__construct($id, $module, $config);
        $this->loanApplicationRepository = $loanApplicationRepository;
        $this->loanApplicationAccessRepository = $loanApplicationAccessRepository;
        $this->manualDistributionService = $manualDistributionService;
        $this->clientExportHelper = $clientExportHelper;
        $this->applicationDistributionService = $applicationDistributionService;
        $this->partnerCompanyService = $partnerCompanyService;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
        return ArrayHelper::merge(parent::behaviors(), $behaviors);
    }

    /**
     * Lists all LoanApplication models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LoanApplicationMultipleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LoanApplication model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LoanApplication model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LoanApplication();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->la_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing LoanApplication model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->la_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing LoanApplication model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionSent()
    {
        $searchModel = new LoanApplicationSearch();
        $dataProvider = $searchModel->findSentLoanApplicationForProcessing(Auth::id(), Yii::$app->request->queryParams);

        $webSocketContainer = Yii::$container->get(WebSocketContainer::class);
        $webSocketContainer->setLoanApplicationStatus(LoanApplicationStatus::STATUS_SENT);

        return $this->render('status/sent', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionTake()
    {
        if (!Yii::$app->request->isPost) {
            throw new BadRequestHttpException('Method not allowed');
        }
        $number = (string)Yii::$app->request->post('number');
        $request = LoanApplicationQuery::findByNumber($number);
        if (!$request) {
            throw new BadRequestHttpException(Yii::t('loan_application', 'Loan Application not found'));
        }

        $curUserId = Auth::id();

        $result = [
            'error' => false,
            'message' => ''
        ];

        $transaction = new Transaction(['db' => Yii::$app->db]);
        try {
            if (!$request->isSentStatus()) {
                throw new \RuntimeException(Yii::t('loan_application', 'Loan Application is not in "Sent" status'));
            }

            $this->guardCanProcessApplication($curUserId, $request->la_id);

            $request->setStatus(LoanApplicationStatus::STATUS_IN_WORK);
            $request->setOwner($curUserId);
            $loanApplicationAccesses = LoanApplicationAccessQuery::findPendingByRequest($request->la_id);
            $company = Auth::user()->partnerCompany;
            if (!$company) {
                throw new \RuntimeException(Yii::t('loan_application', 'The user does not have a company set up'));
            }
            $userProductAccess = UsersProductAccessQuery::findActiveByUserAndProductId($curUserId, $request->la_product_id);
            if (!$userProductAccess) {
                throw new \RuntimeException(Yii::t('loan_application', 'You cannot take this request due to you '));
            }

            $transaction->begin();
            $this->loanApplicationRepository->save($request);
            foreach ($loanApplicationAccesses as $access) {
                if ($access->isEqualUser($curUserId)) {
                    $access->setStatusAccepted();
                    $access->setActionByUser();
                } else {
                    $access->setStatusCanceled();
                    $access->setActionBySystem();
                }
                $this->loanApplicationAccessRepository->save($access);
            }
            $this->partnerCompanyService->withdrawalFunds($company, $userProductAccess->upa_request_price);
            $transaction->commit();
            $job = new LoanApplicationDistributionJob($request->la_product_id);
            /** @var $queue \yii\queue\beanstalk\Queue */
            $queue = \Yii::$app->queue_loan_application_distribution;
            $queue->push($job);

            $this->removeApplicationActionReservation($request->la_id);
            $result['message'] = Yii::t('loan_application', 'Loan Application succssfully accepted and changed status to "In Work".');
        } catch (\RuntimeException $e) {
            $transaction->rollBack();
            $result['error'] = true;
            $result['message'] = $e->getMessage();
        } catch (\Throwable $e) {
            $transaction->rollBack();
            Yii::error(AppHelper::throwableFormatter($e), 'LoanApplicationController::actionTake::Throwable');
            $result['error'] = true;
            $result['message'] = Yii::t('loan_application', Yii::t('backend', 'Internal Server Error'));
        }

        return $this->asJson($result);
    }

    public function actionInWork()
    {
        $search = new LoanApplicationSearch();
        $search->la_owner_id = Auth::id();
        $dataProvider = $search->searchByStatus(Yii::$app->request->queryParams, LoanApplicationStatus::STATUS_IN_WORK);

        $webSocketContainer = Yii::$container->get(WebSocketContainer::class);
        $webSocketContainer->setLoanApplicationStatus(LoanApplicationStatus::STATUS_IN_WORK);

        return $this->render('status/in_work', [
            'searchModel' => $search,
            'dataProvider' => $dataProvider,
            'additionalExportColumns' => $this->clientExportHelper->getAdditionalExportColumns()
        ]);
    }

    public function actionApproved()
    {
        $search = new LoanApplicationSearch();
        $search->la_owner_id = Auth::id();
        $dataProvider = $search->searchByStatus(Yii::$app->request->queryParams, LoanApplicationStatus::STATUS_APPROVED);

        $webSocketContainer = Yii::$container->get(WebSocketContainer::class);
        $webSocketContainer->setLoanApplicationStatus(LoanApplicationStatus::STATUS_APPROVED);

        return $this->render('status/approved', [
            'searchModel' => $search,
            'dataProvider' => $dataProvider,
            'additionalExportColumns' => $this->clientExportHelper->getAdditionalExportColumns()
        ]);
    }

    public function actionDeclined()
    {
        $search = new LoanApplicationSearch();
        $search->la_owner_id = Auth::id();
        $dataProvider = $search->searchDeclined(Yii::$app->request->queryParams, Auth::id());

        $webSocketContainer = Yii::$container->get(WebSocketContainer::class);
        $webSocketContainer->setLoanApplicationStatus(LoanApplicationStatus::STATUS_DECLINED);

        return $this->render('status/declined', [
            'searchModel' => $search,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionPaid()
    {
        $search = new LoanApplicationSearch();
        $search->la_owner_id = Auth::id();
        $dataProvider = $search->searchByStatus(Yii::$app->request->queryParams, LoanApplicationStatus::STATUS_PAID);

        $webSocketContainer = Yii::$container->get(WebSocketContainer::class);
        $webSocketContainer->setLoanApplicationStatus(LoanApplicationStatus::STATUS_PAID);

        return $this->render('status/paid', [
            'searchModel' => $search,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionRevoked()
    {
        $search = new LoanApplicationSearch();
        $search->la_owner_id = Auth::id();
        $dataProvider = $search->searchByStatus(Yii::$app->request->queryParams, LoanApplicationStatus::STATUS_REVOKED);

        $webSocketContainer = Yii::$container->get(WebSocketContainer::class);
        $webSocketContainer->setLoanApplicationStatus(LoanApplicationStatus::STATUS_REVOKED);

        return $this->render('status/revoked', [
            'searchModel' => $search,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionApproveInWork()
    {
        if (!Yii::$app->request->isPost) {
            throw new BadRequestHttpException('Method not allowed');
        }

        $number = (string)Yii::$app->request->post('number');
        $curUserId = Auth::id();
        $request = LoanApplicationQuery::findByOwnerAndNumber($number, $curUserId);
        if (!$request) {
            throw new BadRequestHttpException(Yii::t('loan_application', 'Loan Application not found'));
        }
        if ($request->isNotInWorkStatus()) {
            throw new BadRequestHttpException(Yii::t('loan_application', 'Loan Application not in status "In Work"'));
        }

        $result = [
            'error' => false,
            'message' => ''
        ];

        $request->setStatus(LoanApplicationStatus::STATUS_APPROVED);
        try {
            $this->loanApplicationRepository->save($request);
            $result['message'] = Yii::t('frontend', 'Application was approved');
        } catch (\RuntimeException $e) {
            $result['error'] = true;
            $result['message'] = $e->getMessage();
        }

        return $this->asJson($result);
    }

    public function actionDeclineInWork()
    {
        $form = new DeclineForm();

        if (Yii::$app->request->isPost && Yii::$app->request->isPjax) {
            if ($form->load(Yii::$app->request->post()) && $form->validate()) {
                $request = LoanApplication::findOne((int)$form->loanApplicationId);
                if (!$request) {
                    throw new BadRequestHttpException(Yii::t('loan_application', 'Loan Application not found'));
                }
                if ($request->isNotInWorkStatus()) {
                    throw new BadRequestHttpException(Yii::t('loan_application', 'Loan Application not in status "In Work"'));
                }

                $loanApplicationAccess = LoanApplicationAccessQuery::findAcceptedByUserId($request->la_id, Auth::id());
                if (!$loanApplicationAccess) {
                    $form->addError('general', Yii::t('loan_application', 'Cannot find Loan Application Access'));
                } else {
                    $request->setStatus(LoanApplicationStatus::STATUS_DECLINED);
                    $this->loanApplicationRepository->save($request);
//                    $request->setStatus(LoanApplicationStatus::STATUS_SENT);
//                    $this->loanApplicationRepository->save($request);
                    $loanApplicationAccess->setStatusDeclined();
                    $loanApplicationAccess->laa_reason = $form->reason;
                    $this->loanApplicationAccessRepository->save($loanApplicationAccess);
                    $this->applicationDistributionService->setAccessForAllUsersToLoanApplication($request, Auth::user());

//                    $job = new LoanApplicationDistributionJob($request->la_product_id);
//                    /** @var $queue \yii\queue\beanstalk\Queue */
//                    $queue = \Yii::$app->queue_loan_application_distribution;
//                    $queue->push($job);
                    return '<script>$("#modal-sm").modal("hide");$("tr[data-key=\"'.$request->la_id.'\"]").remove();createNotify("Info", "'.Yii::t('loan_application', 'Loan Applcation was declined').'");</script>';
                }
            }
        } else {
            $number = (string)Yii::$app->request->get('number');
            $curUserId = Auth::id();
            $request = LoanApplicationQuery::findByOwnerAndNumber($number, $curUserId);
            if (!$request) {
                throw new BadRequestHttpException(Yii::t('loan_application', 'Loan Application not found'));
            }
            if ($request->isNotInWorkStatus()) {
                throw new BadRequestHttpException(Yii::t('loan_application', 'Loan Application not in status "In Work"'));
            }
            $form->loanApplicationId = $request->la_id;
        }
        return $this->renderAjax('partial/_decline_form', [
            'model' => $form
        ]);
    }

    public function actionDeclineApproved()
    {
        $form = new DeclineForm();

        if (Yii::$app->request->isPost && Yii::$app->request->isPjax) {
            if ($form->load(Yii::$app->request->post()) && $form->validate()) {
                $request = LoanApplication::findOne((int)$form->loanApplicationId);
                if (!$request) {
                    throw new BadRequestHttpException(Yii::t('loan_application', 'Loan Application not found'));
                }
                if ($request->isNotApproved()) {
                    throw new BadRequestHttpException(Yii::t('loan_application', 'Loan Application not in status "In Work"'));
                }

                $loanApplicationAccess = LoanApplicationAccessQuery::findAcceptedByUserId($request->la_id, Auth::id());
                if (!$loanApplicationAccess) {
                    $form->addError('general', Yii::t('loan_application', 'Cannot find Loan Application Access'));
                } else {
                    $request->setStatus(LoanApplicationStatus::STATUS_DECLINED);
                    $this->loanApplicationRepository->save($request);
                    $loanApplicationAccess->setStatusDeclined();
                    $loanApplicationAccess->laa_reason = $form->reason;
                    $this->loanApplicationAccessRepository->save($loanApplicationAccess);
                    return '<script>$("#modal-sm").modal("hide");$("tr[data-key=\"'.$request->la_id.'\"]").remove();createNotify("Info", "'.Yii::t('loan_application', 'Loan Applcation was declined').'");</script>';
                }
            }
        } else {
            $number = (string)Yii::$app->request->get('number');
            $curUserId = Auth::id();
            $request = LoanApplicationQuery::findByOwnerAndNumber($number, $curUserId);
            if (!$request) {
                throw new BadRequestHttpException(Yii::t('loan_application', 'Loan Application not found'));
            }
            if ($request->isNotApproved()) {
                throw new BadRequestHttpException(Yii::t('loan_application', 'Loan Application not in status "In Work"'));
            }
            $form->loanApplicationId = $request->la_id;
        }
        return $this->renderAjax('partial/_decline_form', [
            'model' => $form
        ]);
    }

    public function actionPay()
    {
        if (!Yii::$app->request->isPost) {
            throw new BadRequestHttpException('Method not allowed');
        }

        $number = (string)Yii::$app->request->post('number');
        $curUserId = Auth::id();
        $request = LoanApplicationQuery::findByOwnerAndNumber($number, $curUserId);
        if (!$request) {
            throw new BadRequestHttpException(Yii::t('loan_application', 'Loan Application not found'));
        }
        if ($request->isNotApproved()) {
            throw new BadRequestHttpException(Yii::t('loan_application', 'Loan Application not in status "Approved"'));
        }

        $result = [
            'error' => false,
            'message' => ''
        ];

        $request->setStatus(LoanApplicationStatus::STATUS_PAID);
        try {
            $this->loanApplicationRepository->save($request);

            $result['message'] = Yii::t('loan_application', 'Loan application was paid');
        } catch (\RuntimeException $e) {
            $result['error'] = true;
            $result['message'] = $e->getMessage();
        }

        return $this->asJson($result);
    }

    public function actionDetails()
    {
        $number = (string)Yii::$app->request->get('number');
        $request = LoanApplicationQuery::findByNumber($number);
        if (!$request) {
            throw new BadRequestHttpException(Yii::t('loan_application', 'Loan Application not found'));
        }
        return $this->renderAjax('partial/_loan_application_details', [
            'request' => $request
        ]);
    }

    public function actionAjaxAssignUsers()
    {
        $form = new ManualDistributionForm();

        $number = (string)Yii::$app->request->get('number');
        $request = LoanApplicationQuery::findByNumber($number);
        if (!$request) {
            throw new BadRequestHttpException(Yii::t('loan_application', 'Loan Application not found'));
        }
        $form->loanApplicationId = $request->la_id;

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $this->manualDistributionService->handle($form, $request);
            if (!$form->hasErrors()) {
                return '<script>$("#modal-md").modal("hide");createNotify("'.Yii::t('backend', 'Success').'", "'.Yii::t('backend', 'Users successfully asigned to loan application').'", "success"); $.pjax.reload({container: "#pjax-loan-application-list", async: false});</script>';
            }
        }

        $availableUsers = UsersProductAccessQuery::findAllAvailableUsersByProduct($request->la_product_id);

        return $this->renderAjax('partial/_assign_users', [
            'model' => $form,
            'availableUsers' => $availableUsers,
            'request' => $request
        ]);
    }

    /**
     * Finds the LoanApplication model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LoanApplication the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LoanApplication::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('loan_application', 'The requested page does not exist.'));
    }

    private function guardCanProcessApplication(int $userId, int $loanApplicationId, int $seconds = 20): void
    {
        /** @var Connection $redis */
        $redis = Yii::$app->redis;
        $key = self::RESERVE_REQUEST_TO_PROCESS_KEY . $loanApplicationId;
        $redis->setnx($key, $userId);
        $value = $redis->get($key);
        if ((int)$value === $userId) {
            $redis->expire($key, $seconds);
        } else {
            throw new \RuntimeException(Yii::t('loan_application', 'This loan application is already being processed by other partner.'));
        }
    }

    private function removeApplicationActionReservation(int $loanApplicationId): void
    {
        /** @var Connection $redis */
        $redis = Yii::$app->redis;
        $key = self::RESERVE_REQUEST_TO_PROCESS_KEY . $loanApplicationId;
        $redis->del($key);
    }
}
