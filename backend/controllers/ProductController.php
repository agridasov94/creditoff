<?php

namespace backend\controllers;

use backend\src\auth\Auth;
use backend\src\entities\usersProductAccess\entity\UsersProductAccess;
use backend\src\entities\usersProductAccess\entity\UsersProductAccessQuery;
use backend\src\entities\usersProductAccess\entity\UsersProductAccessSearch;
use common\src\entities\product\entity\ProductQuery;
use common\src\helpers\app\SettingHelper;
use common\src\services\product\ProductService;
use frontend\components\jobs\LoanApplicationDistributionJob;
use Yii;
use common\src\entities\product\entity\Product;
use common\src\entities\product\entity\ProductSearch;
use backend\controllers\AuthController;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProductController implements the CRUD actions for Product model.
 *
 * @property-read ProductService $productService
 */
class ProductController extends AuthController
{
    private ProductService $productService;

    public function __construct($id, $module, ProductService $productService, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->productService = $productService;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
        return ArrayHelper::merge(parent::behaviors(), $behaviors);
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->p_id]);
        }

        $model->p_settings = json_encode(['step_in_queue_variables' => SettingHelper::getStepInQueueVariables()]);

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $oldStepInQueue = $model->p_step_in_queue;
        $oldSettingData = $model->p_settings;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->productService->calculateStepInQueue($model);
            $this->runDistributionIfChangedStepIfChangedAttributes(
                (int)$model->p_id,
                $oldSettingData,
                $model->p_settings,
                $model->p_step_in_queue,
                $oldStepInQueue
            );
            return $this->redirect(['view', 'id' => $model->p_id]);
        }

        if (empty($model->getStepInQueueVariables())) {
            $model->p_settings = json_encode(['step_in_queue_variables' => SettingHelper::getStepInQueueVariables()]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionManageMyProductPrices()
    {
        if (Yii::$app->request->isPost && array_key_exists('hasEditable', Yii::$app->request->post())) {
            $productId = Yii::$app->request->post('productId');
            $requestPrice = Yii::$app->request->post('requestPrice');
            $status = Yii::$app->request->post('status');
            $model = UsersProductAccessQuery::findByUserAndProductIds(Auth::id(), (int)$productId);
            if (!$model) {
                return $this->asJson([
                    'output' => '',
                    'message' => Yii::t('product', 'Not found product')
                ]);
            }

            $editPrice = Yii::$app->request->post('editPrice');
            $editStatus = Yii::$app->request->post('editStatus');

            if ($editPrice) {
                $model->upa_request_price = $requestPrice;
            } else if ($editStatus) {
                $model->upa_status = $status;
            } else {
                return $this->asJson([
                    'output' => '',
                    'message' => Yii::t('product', 'Unknown changable parameter')
                ]);
            }

            if (!$model->save()) {
                return $this->asJson([
                    'output' => '',
                    'message' => $model->getErrorSummary(true)[0]
                ]);
            }

            if ($editPrice) {
                $this->productService->calculateStepInQueue($model->product);
            }

            $job = new LoanApplicationDistributionJob($model->product->p_id);
            /** @var $queue \yii\queue\beanstalk\Queue */
            $queue = \Yii::$app->queue_loan_application_distribution;
            $queue->push($job);

            return $this->asJson([
                'output' => '',
                'message' => ''
            ]);
        }

        $searchModel = new UsersProductAccessSearch();
        $searchModel->upa_user_id = Auth::id();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('manage_product_prices', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUsersProductPrices()
    {
        if (Yii::$app->request->isPost && array_key_exists('hasEditable', Yii::$app->request->post())) {
            $productId = Yii::$app->request->post('productId');
            $requestPrice = Yii::$app->request->post('requestPrice');
            $status = Yii::$app->request->post('status');
            $userId = Yii::$app->request->post('userId');
            $model = UsersProductAccessQuery::findByUserAndProductIds((int)$userId, (int)$productId);
            if (!$model) {
                return $this->asJson([
                    'output' => '',
                    'message' => Yii::t('product', 'Not found product')
                ]);
            }

            $editPrice = Yii::$app->request->post('editPrice');
            $editStatus = Yii::$app->request->post('editStatus');

            if ($editPrice) {
                $model->upa_request_price = $requestPrice;
            } else if ($editStatus) {
                $model->upa_status = $status;
            } else {
                return $this->asJson([
                    'output' => '',
                    'message' => Yii::t('product', 'Unknown changable parameter')
                ]);
            }

            if (!$model->save()) {
                return $this->asJson([
                    'output' => '',
                    'message' => $model->getErrorSummary(true)[0]
                ]);
            }

            if ($editPrice) {
                $this->productService->calculateStepInQueue($model->product);
            }

            $job = new LoanApplicationDistributionJob($model->product->p_id);
            /** @var $queue \yii\queue\beanstalk\Queue */
            $queue = \Yii::$app->queue_loan_application_distribution;
            $queue->push($job);

            return $this->asJson([
                'output' => '',
                'message' => ''
            ]);
        }

        $searchModel = new UsersProductAccessSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('users_product_prices', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('product', 'The requested page does not exist.'));
    }

    private function runDistributionIfChangedStepIfChangedAttributes($productId, $oldValue, $newValue, $curStepInQueue, $oldStepInQueue)
    {
        if (($curStepInQueue !== $oldStepInQueue) || (md5(serialize($oldValue)) !== md5(serialize($newValue)))) {
            $job = new LoanApplicationDistributionJob($productId);
            /** @var $queue \yii\queue\beanstalk\Queue */
            $queue = \Yii::$app->queue_loan_application_distribution;
            $queue->push($job);
        }
    }
}
