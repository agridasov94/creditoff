<?php

use common\src\entities\mainBanner\entity\MainBanner;
use common\src\entities\mainBanner\useCase\displayFile\DisplayFile;
use common\src\entities\mainBanner\useCase\UpdateForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model UpdateForm */
/* @var $banner MainBanner */
/* @var $form yii\widgets\ActiveForm */
?>

    <div class="main-banner-form">
        <?php $form = ActiveForm::begin(); ?>
        <div class="row">
            <div class="col-md-6">
                <?= $form->errorSummary($model, ['class' => 'alert alert-danger', 'role' => 'alert']) ?>

                <?= $form->field($model, 'id')->hiddenInput()->label(false) ?>

                <?= $form->field($model, 'langId')->hiddenInput()->label(false) ?>

                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'subtitle')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'linkText')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'isActive')->checkbox() ?>

                <?= $form->field($model, 'order')->input('number') ?>

                <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('main_banner', 'Save'), ['class' => 'btn btn-success']) ?>
                </div>

            </div>
            <div class="col-md-6">

                <?= $form->field($model, 'fileType')->dropDownList(MainBanner::FILE_TYPE_LIST, ['prompt' => '--', 'id' => 'file-type']) ?>

                <div id="preview-image">
                    <?= $banner->mb_file_name ? (new DisplayFile($banner, DisplayFile::BACKEND))->render($this) : '' ?>
                </div>

                <?= $form->field($model, 'file')->fileInput(['id' => 'upload-preview']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

<?php
$fileTypeExtensions = json_encode(MainBanner::FILE_TYPE_EXTENSIONS);
$js = <<<JS
let fileTypeExtensions = JSON.parse('$fileTypeExtensions');
$('#file-type').on('change', function (e) {
    let val = $(this).val();
    let fileInput = $('#upload-preview');
    if (val in fileTypeExtensions) {
        fileInput.prop('accept', fileTypeExtensions[val].join());
    } else {
        fileInput.removeAttr('accept');
    }
});

$('#upload-preview').on('change', function () {
    const [file] = this.files;
    let img = $('<img />');
    img.attr('src', URL.createObjectURL(file));
    img.css('max-width', '100%');
    $('#preview-image').html(img);
});
JS;
$this->registerJs($js);

