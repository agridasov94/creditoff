<?php
/**
 * @var MainBanner $banner
 */

use common\src\entities\mainBanner\entity\MainBanner;
use yii\helpers\Html;

?>

<div style="width: 100%">
    <?= Html::tag('video', '', [
        'src' => $banner->getPublicImagePath(),
        'controls' => true,
    ]) ?>
</div>

