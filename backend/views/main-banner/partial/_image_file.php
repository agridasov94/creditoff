<?php
/**
 * @var MainBanner $banner
 */

use common\src\entities\mainBanner\entity\MainBanner;
use yii\helpers\Html;

?>

<?= Html::img($banner->getPublicImagePath(), ['alt' => $banner->mb_title, 'width' => '100%']) ?>
