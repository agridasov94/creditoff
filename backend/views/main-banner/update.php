<?php

use common\src\entities\mainBanner\entity\MainBanner;
use common\src\entities\mainBanner\useCase\UpdateForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model UpdateForm */
/* @var $banner MainBanner */

$this->title = Yii::t('main_banner', 'Update Main Banner: {name}', [
    'name' => $banner->mb_title,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('main_banner', 'Main Banners'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $banner->mb_title, 'url' => ['view', 'mb_id' => $banner->mb_id, 'mb_lang_id' => $banner->mb_lang_id]];
$this->params['breadcrumbs'][] = Yii::t('main_banner', 'Update');
?>
<div class="main-banner-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_update', [
        'model' => $model,
        'banner' => $banner
    ]) ?>

</div>
