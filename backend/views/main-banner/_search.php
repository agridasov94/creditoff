<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\src\entities\mainBanner\entity\MainBannerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="main-banner-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'mb_id') ?>

    <?= $form->field($model, 'mb_title') ?>

    <?= $form->field($model, 'mb_subtitle') ?>

    <?= $form->field($model, 'mb_link') ?>

    <?= $form->field($model, 'mb_link_text') ?>

    <?php // echo $form->field($model, 'mb_is_active') ?>

    <?php // echo $form->field($model, 'mb_is_deleted') ?>

    <?php // echo $form->field($model, 'mb_order') ?>

    <?php // echo $form->field($model, 'mb_description') ?>

    <?php // echo $form->field($model, 'mb_file_name') ?>

    <?php // echo $form->field($model, 'mb_file_type') ?>

    <?php // echo $form->field($model, 'mb_lang_id') ?>

    <?php // echo $form->field($model, 'mb_created_by') ?>

    <?php // echo $form->field($model, 'mb_updated_by') ?>

    <?php // echo $form->field($model, 'mb_created_dt') ?>

    <?php // echo $form->field($model, 'mb_updated_dt') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('main_banner', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('main_banner', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
