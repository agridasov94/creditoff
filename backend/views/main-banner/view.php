<?php

use common\src\entities\mainBanner\entity\MainBanner;
use common\src\entities\mainBanner\useCase\displayFile\DisplayFile;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\src\entities\mainBanner\entity\MainBanner */

$this->title = $model->mb_title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main_banner', 'Main Banners'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="main-banner-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('main_banner', 'Update'), ['update', 'mb_id' => $model->mb_id, 'mb_lang_id' => $model->mb_lang_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('main_banner', 'Delete'), ['delete', 'mb_id' => $model->mb_id, 'mb_lang_id' => $model->mb_lang_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('main_banner', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
<div class="row">
    <div class="container">
        <div class="col-md-6">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'mb_id',
                    [
                        'attribute' => 'mb_lang_id',
                        'value' => static function (MainBanner $model) {
                            return ucfirst($model->lang->language_id);
                        }
                    ],
                    'mb_title:ntext',
                    'mb_subtitle:ntext',
                    'mb_link',
                    'mb_link_text',
                    'mb_is_active:booleanByLabel',
                    'mb_is_deleted:booleanByLabel',
                    'mb_order',
                    'mb_description:ntext',
                    'mb_file_name',
                    [
                        'attribute' => 'mb_file_type',
                        'value' => fn (MainBanner $banner): string => $banner->getTypeName(),
                    ],
                    'mb_created_by:username',
                    'mb_updated_by:username',
                    'mb_created_dt:byUserDateTime',
                    'mb_updated_dt:byUserDateTime',
                ],
            ]); ?>
        </div>
        <div class="col-md-6">
            <?= (new DisplayFile($model, DisplayFile::BACKEND))->render($this) ?>
        </div>
    </div>
</div>

</div>
