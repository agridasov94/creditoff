<?php

use backend\components\grid\DateTimeColumn;
use backend\components\grid\UserSelect2Column;
use common\src\entities\language\LanguageQuery;
use common\src\entities\mainBanner\entity\MainBanner;
use common\widgets\FlashAlert;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\src\entities\mainBanner\entity\MainBannerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('main_banner', 'Main Banners');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main-banner-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('main_banner', 'Create Main Banner'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= FlashAlert::widget() ?>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'mb_id',
            [
                'attribute' => 'mb_lang_id',
                'options' => [
                    'width' => '100px'
                ],
                'filter' => LanguageQuery::getList()
            ],
            'mb_title',
            [
                'attribute' => 'mb_file_type',
                'value' => fn (MainBanner $banner): string => $banner->getTypeName(),
                'filter' => MainBanner::FILE_TYPE_LIST
            ],
            'mb_is_active:booleanByLabel',
            'mb_order',
            'mb_is_deleted:booleanByLabel',
            [
                'class' => UserSelect2Column::class,
                'attribute' => 'mb_created_by',
                'relation' => 'createdBy',
                'placeholder' => '',
            ],
            [
                'class' => UserSelect2Column::class,
                'attribute' => 'mb_updated_by',
                'relation' => 'updatedBy',
                'placeholder' => '',
            ],
            [
                'class' => DateTimeColumn::class,
                'attribute' => 'mb_created_dt',
                'format' => 'byUserDateTime'
            ],
            [
                'class' => DateTimeColumn::class,
                'attribute' => 'mb_updated_dt',
                'format' => 'byUserDateTime'
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
