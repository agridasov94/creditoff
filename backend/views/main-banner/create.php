<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \common\src\entities\mainBanner\useCase\CreateForm */

$this->title = Yii::t('main_banner', 'Create Main Banner');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main_banner', 'Main Banners'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main-banner-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
