<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\src\entities\userProductAccessLog\entity\UserProductAccessLog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-product-access-log-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'upal_user_id')->textInput() ?>

    <?= $form->field($model, 'upal_product_id')->textInput() ?>

    <?= $form->field($model, 'upal_request_price_from')->textInput() ?>

    <?= $form->field($model, 'upal_request_price_to')->textInput() ?>

    <?= $form->field($model, 'upal_status_from')->textInput() ?>

    <?= $form->field($model, 'upal_status_to')->textInput() ?>

    <?= $form->field($model, 'upal_action_by_user_id')->textInput() ?>

    <?= $form->field($model, 'upal_start_dt')->textInput() ?>

    <?= $form->field($model, 'upal_end_dt')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('user_product_access_log', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
