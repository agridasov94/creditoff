<?php

use backend\src\entities\usersProductAccess\entity\UsersProductAccess;
use common\src\entities\userProductAccessLog\entity\UserProductAccessLog;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\src\entities\userProductAccessLog\entity\UserProductAccessLog */

$this->title = $model->upal_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('user_product_access_log', 'User Product Access Logs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="user-product-access-log-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('user_product_access_log', 'Update'), ['update', 'id' => $model->upal_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('user_product_access_log', 'Delete'), ['delete', 'id' => $model->upal_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('user_product_access_log', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'upal_id',
            'upal_user_id:username',
            [
                'attribute' => 'upal_product_id',
                'value' => static function (UserProductAccessLog $model) {
                    return Html::a('(' . $model->upal_product_id . ') ' . $model->product->p_name, ['/product/view', 'id' => $model->upal_product_id], [
                        'target' => '_blank',
                        'data-pjax' => 0
                    ]);
                },
                'format' => 'raw'
            ],
            'upal_request_price_from',
            'upal_request_price_to',
            [
                'attribute' => 'upal_status_from',
                'value' => static function (UserProductAccessLog $model) {
                    return $model->upal_status_from ? (UsersProductAccess::getStatusLabelList()[$model->upal_status_from] ?? UsersProductAccess::getStatusName($model->upal_status_from)) : null;
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'upal_status_to',
                'value' => static function (UserProductAccessLog $model) {
                    return $model->upal_status_to ? (UsersProductAccess::getStatusLabelList()[$model->upal_status_to] ?? UsersProductAccess::getStatusName($model->upal_status_to)) : null;
                },
                'format' => 'raw',
            ],
            'upal_action_by_user_id:username',
            'upal_start_dt:byUserDateTime',
            'upal_end_dt:byUserDateTime',
        ],
    ]) ?>

</div>
