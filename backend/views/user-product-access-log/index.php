<?php

use backend\src\entities\usersProductAccess\entity\UsersProductAccess;
use common\src\entities\product\entity\ProductQuery;
use common\src\entities\userProductAccessLog\entity\UserProductAccessLog;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\src\entities\userProductAccessLog\entity\UserProductAccessLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('user_product_access_log', 'User Product Price Logs');
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    [
        'attribute' => 'upal_id',
        'options' => [
            'width' => '100px'
        ]
    ],
    [
        'class' => \backend\components\grid\UserSelect2Column::class,
        'attribute' => 'upal_user_id',
        'relation' => 'user',
        'options' => [
            'width' => '250px'
        ]
    ],
    [
        'attribute' => 'upal_product_id',
        'value' => static function (UserProductAccessLog $model) {
            return Html::a('(' . $model->upal_product_id . ') ' . $model->product->p_name, ['/product/view', 'id' => $model->upal_product_id], [
                'target' => '_blank',
                'data-pjax' => 0
            ]);
        },
        'format' => 'raw',
        'options' => [
            'width' => '250px'
        ],
        'filter' => ProductQuery::getList()
    ],
    [
        'attribute' => 'upal_request_price_from',
        'options' => [
            'width' => '100px'
        ]
    ],
    [
        'attribute' => 'upal_request_price_to',
        'options' => [
            'width' => '100px'
        ]
    ],
    [
        'attribute' => 'upal_status_from',
        'value' => static function (UserProductAccessLog $model) {
            return $model->upal_status_from ? (UsersProductAccess::getStatusLabelList()[$model->upal_status_from] ?? UsersProductAccess::getStatusName($model->upal_status_from)) : null;
        },
        'format' => 'raw',
        'filter' => UsersProductAccess::getStatusListName(),
        'options' => [
            'width' => '100px'
        ]
    ],
    [
        'attribute' => 'upal_status_to',
        'value' => static function (UserProductAccessLog $model) {
            return $model->upal_status_to ? (UsersProductAccess::getStatusLabelList()[$model->upal_status_to] ?? UsersProductAccess::getStatusName($model->upal_status_to)) : null;
        },
        'format' => 'raw',
        'filter' => UsersProductAccess::getStatusListName(),
        'options' => [
            'width' => '100px'
        ]
    ],
    [
        'class' => \backend\components\grid\UserSelect2Column::class,
        'attribute' => 'upal_action_by_user_id',
        'relation' => 'actionByUser',
        'options' => [
            'width' => '250px'
        ]
    ],
    [
        'class' => \backend\components\grid\DateTimeColumn::class,
        'attribute' => 'upal_start_dt',
        'format' => 'byUserDateTime',
        'options' => [
            'width' => '200px'
        ]
    ],
    [
        'class' => \backend\components\grid\DateTimeColumn::class,
        'attribute' => 'upal_end_dt',
        'format' => 'byUserDateTime',
        'options' => [
            'width' => '200px'
        ]
    ],
    [
        'class' => 'yii\grid\ActionColumn',
        'template' => '{view} {delete}',
        'options' => [
            'width' => '50px'
        ]
    ],
];
?>
<div class="user-product-access-log-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>

    <?php if(Yii::$app->request->isPjax): ?>
        <script>
            $('.export-wrapper .dropdown-menu a').each(function (i, e) {
                $(document).off('click.exportmenu', '#' + $(e).attr('id'));
            });
        </script>
    <?php endif; ?>
    <?= \kartik\export\ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'dropdownOptions' => [
            'label' => 'Export All',
            'class' => 'btn btn-outline-secondary btn-default'
        ],
        'container' => [
            'class' => 'export-wrapper'
        ],
        'showConfirmAlert' => false,
        'filename' => Yii::t('export', 'User_product_price') . '_' . date('Y-m-d_H:i:s')
    ]) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns
    ]); ?>

    <?php Pjax::end(); ?>

</div>
