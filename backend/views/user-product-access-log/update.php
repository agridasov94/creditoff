<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\src\entities\userProductAccessLog\entity\UserProductAccessLog */

$this->title = Yii::t('user_product_access_log', 'Update User Product Access Log: {name}', [
    'name' => $model->upal_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('user_product_access_log', 'User Product Access Logs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->upal_id, 'url' => ['view', 'id' => $model->upal_id]];
$this->params['breadcrumbs'][] = Yii::t('user_product_access_log', 'Update');
?>
<div class="user-product-access-log-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
