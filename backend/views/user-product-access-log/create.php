<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\src\entities\userProductAccessLog\entity\UserProductAccessLog */

$this->title = Yii::t('user_product_access_log', 'Create User Product Access Log');
$this->params['breadcrumbs'][] = ['label' => Yii::t('user_product_access_log', 'User Product Access Logs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-product-access-log-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
