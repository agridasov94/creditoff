<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\src\entities\userProductAccessLog\entity\UserProductAccessLogSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-product-access-log-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'upal_id') ?>

    <?= $form->field($model, 'upal_user_id') ?>

    <?= $form->field($model, 'upal_product_id') ?>

    <?= $form->field($model, 'upal_request_price_from') ?>

    <?= $form->field($model, 'upal_request_price_to') ?>

    <?php // echo $form->field($model, 'upal_status_from') ?>

    <?php // echo $form->field($model, 'upal_status_to') ?>

    <?php // echo $form->field($model, 'upal_action_by_user_id') ?>

    <?php // echo $form->field($model, 'upal_start_dt') ?>

    <?php // echo $form->field($model, 'upal_end_dt') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('user_product_access_log', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('user_product_access_log', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
