<?php

use backend\components\grid\DateTimeColumn;
use backend\components\grid\UserSelect2Column;
use backend\src\entities\partnerCompany\entity\PartnerCompanyQuery;
use backend\widgets\UserSelect2Widget;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\src\entities\partnerCompany\entity\PartnerCompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('partner', 'Partner Companies');
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    [
        'attribute' => 'pc_id',
        'options' => [
            'width' => '100px'
        ]
    ],
    [
        'attribute' => 'pc_id',
        'filter' => PartnerCompanyQuery::getList(),
        'label' => (new \backend\src\entities\partnerCompany\entity\PartnerCompany())->getAttributeLabel('pc_name'),
        'value' => static function (\backend\src\entities\partnerCompany\entity\PartnerCompany $model) {
            return $model->pc_name;
        }
    ],
    'pc_fiscal_code',
    'pc_total_amount',
    'pc_total_bonus_amount',
    [
        'attribute' => 'pc_is_blocked',
        'format' => 'booleanByLabel',
        'filter' => [0 => Yii::t('backend', 'No'), 1 => Yii::t('frontend', 'Yes')],
    ],
    [
        'class' => DateTimeColumn::class,
        'attribute' => 'pc_created_dt',
        'format' => 'byUserDateTime'
    ],
    [
        'class' => DateTimeColumn::class,
        'attribute' => 'pc_updated_dt',
        'format' => 'byUserDateTime'
    ],
    [
        'class' => UserSelect2Column::class,
        'attribute' => 'pc_created_user_id',
        'relation' => 'createdUser',
        'placeholder' => '',
    ],
    [
        'class' => UserSelect2Column::class,
        'attribute' => 'pc_updated_user_id',
        'relation' => 'updatedUser',
        'placeholder' => '',
    ],

    ['class' => 'yii\grid\ActionColumn'],
];
?>
<h4><?= Html::encode($this->title) ?></h4>

<p>
    <?= Html::a(Yii::t('partner', 'Create Partner Company'), ['create'], ['class' => 'btn btn-success']) ?>
</p>
<div class="partner-company-index">

    <?php Pjax::begin(['id' => 'partner-company']); ?>

    <?php if(Yii::$app->request->isPjax): ?>
        <script>
            $('.export-wrapper .dropdown-menu a').each(function (i, e) {
                $(document).off('click.exportmenu', '#' + $(e).attr('id'));
            });
        </script>
    <?php endif; ?>
    <?= \kartik\export\ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'dropdownOptions' => [
            'label' => 'Export All',
            'class' => 'btn btn-outline-secondary btn-default'
        ],
        'container' => [
            'class' => 'export-wrapper'
        ],
        'showConfirmAlert' => false,
        'filename' => Yii::t('export', 'Partner_company') . '_' . date('Y-m-d_H:i:s')
    ]) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
    ]); ?>

    <?php Pjax::end(); ?>

</div>
