<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\src\entities\partnerCompany\entity\PartnerCompany */

$this->title = $model->pc_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('partner', 'Partner Companies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="partner-company-view">

    <h4><?= Html::encode($this->title) ?></h4>

    <p>
        <?= Html::a(Yii::t('partner', 'Update'), ['update', 'id' => $model->pc_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('partner', 'Delete'), ['delete', 'id' => $model->pc_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('partner', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= \common\widgets\FlashAlert::widget() ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'pc_id',
            'pc_name',
            'pc_fiscal_code',
            'pc_total_amount',
            'pc_total_bonus_amount',
            'pc_is_blocked:booleanByLabel',
            'pc_created_user_id:username',
            'pc_updated_user_id:username',
            'pc_created_dt:byUserDateTime',
            'pc_updated_dt:byUserDateTime',
        ],
    ]) ?>

</div>
