<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\src\entities\partnerCompany\entity\PartnerCompany */

$this->title = Yii::t('partner', 'Create Partner Company');
$this->params['breadcrumbs'][] = ['label' => Yii::t('partner', 'Partner Companies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="partner-company-create">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
