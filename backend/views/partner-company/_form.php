<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\src\entities\partnerCompany\entity\PartnerCompany */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="row">
    <div class="col-md-2">
        <div class="partner-company-form">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'pc_name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'pc_fiscal_code')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'pc_total_amount')->input('number') ?>

            <?= $form->field($model, 'pc_total_bonus_amount')->input('number') ?>

            <?= $form->field($model, 'pc_is_blocked')->checkbox() ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('partner', 'Save'), ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
