<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\src\entities\partnerCompany\entity\PartnerCompanySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="partner-company-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'pc_id') ?>

    <?= $form->field($model, 'pc_name') ?>

    <?= $form->field($model, 'pc_fiscal_code') ?>

    <?= $form->field($model, 'pc_total_amount') ?>

    <?= $form->field($model, 'pc_total_bonus_amount') ?>

    <?php // echo $form->field($model, 'pc_is_blocked') ?>

    <?php // echo $form->field($model, 'pc_created_user_id') ?>

    <?php // echo $form->field($model, 'pc_updated_user_id') ?>

    <?php // echo $form->field($model, 'pc_created_dt') ?>

    <?php // echo $form->field($model, 'pc_updated_dt') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('partner', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('partner', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
