<?php
/* @var $this yii\web\View */
/* @var $manageAmountForm \backend\src\useCase\partner\manageAmount\ManageAmountForm */
/* @var $models array */

use backend\src\entities\partnerCompany\entity\PartnerCompanyQuery;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

$this->title = Yii::t('partner', 'Manage Amount');
$this->params['breadcrumbs'][] = $this->title;
?>
<h4><?= Html::encode($this->title) ?></h4>

<div class="row">
    <div class="col-md-2">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->errorSummary($manageAmountForm) ?>

        <?= $form->field($manageAmountForm, 'partnerCompanyId')->dropdownList(PartnerCompanyQuery::getList(), [
            'prompt' => '---',
            'id' => 'partnerCompanyId'
        ]) ?>

        <div id="total-amount" style="font-weight: bold;"></div>

        <?= $form->field($manageAmountForm, 'amount')->input('number', [
            'id' => 'amount'
        ]) ?>

        <div id="total-bonus" style="font-weight: bold;"></div>

        <?= $form->field($manageAmountForm, 'bonus')->input('number', [
            'id' => 'bonus'
        ]) ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('partner', 'Submit'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>

<?php

$modelsEncoded = json_encode($models);
$totalAmountTxt = Yii::t('partner', 'Total Amount');
$totalBonusTxt = Yii::t('partner', 'Total Bonus');
$js = <<<JS
let models = JSON.parse('$modelsEncoded');
let bonus = 0;
let amount  = 0;
let totalAmountTxt = '$totalAmountTxt';
let totalBonusTxt = '$totalBonusTxt';
$('#partnerCompanyId').on('change', function (e) {
   let value = $(this).val(); 
   
   if (value !== '') {
       bonus = getPartnerBonus(value);
       amount = getPartnerAmount(value);
       
       let currentValueAmount = $('#amount').val() || 0;
       let currentValueBonus = $('#bonus').val() || 0;
       
       amount = parseInt(amount) + parseInt(currentValueAmount);
       bonus = parseInt(bonus) + parseInt(currentValueBonus);
       
       renderAmount(amount);
       renderBonus(bonus);
   } else {
       $('#total-amount').html('');
       $('#total-bonus').html('');
   }
});
$('#amount').on('keyup change', function (e) {
    let selectedPartner = $('#partnerCompanyId').val(); 
    let partnerAmount = getPartnerAmount(selectedPartner);
    let value = $(this).val() || 0;
    
    amount = parseInt(partnerAmount) + parseInt(value);
    renderAmount(amount);
});
$('#bonus').on('keyup change', function (e) {
    let selectedPartner = $('#partnerCompanyId').val(); 
    let partnerBonus = getPartnerBonus(selectedPartner);
    let value = $(this).val() || 0;
    
    bonus = parseInt(partnerBonus) + parseInt(value);
    renderBonus(bonus);
});
function getPartnerAmount(id) {
    return parseInt(models[id][0]['amount']) || 0;
}
function getPartnerBonus(id) {
    return parseInt(models[id][0]['bonus']) || 0;
}
function renderAmount(amount) {
    let pTotalAmount = document.createElement('p');
    pTotalAmount.innerText = totalAmountTxt + ': ' + amount;
    $('#total-amount').html(pTotalAmount);
}
function renderBonus(bonus) {
    let pTotalBonus = document.createElement('p');
    pTotalBonus.innerText = totalBonusTxt + ': ' + bonus;
    $('#total-bonus').html(pTotalBonus);
}
JS;
$this->registerJs($js);
