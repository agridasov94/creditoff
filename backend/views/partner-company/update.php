<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\src\entities\partnerCompany\entity\PartnerCompany */

$this->title = Yii::t('partner', 'Update Partner Company: {name}', [
    'name' => $model->pc_name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('partner', 'Partner Companies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pc_name, 'url' => ['view', 'id' => $model->pc_id]];
$this->params['breadcrumbs'][] = Yii::t('partner', 'Update');
?>
<div class="partner-company-update">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
