<?php

use backend\components\grid\DateTimeColumn;
use backend\src\entities\partnerCompany\entity\PartnerCompanyQuery;
use backend\src\entities\usersNotifications\entity\UsersNotifications;
use common\widgets\FlashAlert;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\src\entities\usersNotifications\entity\UsersNotificationsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('users_notifications', 'Users Notifications');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-notifications-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= FlashAlert::widget() ?>

    <p>
        <?= Html::a(Yii::t('users', 'Create Notification'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'un_id',
            'un_unique_id',
            [
                'class' => \backend\components\grid\UserSelect2Column::class,
                'attribute' => 'un_user_id',
                'relation' => 'user',
                'placeholder' => 'Select User',
            ],
            [
                'attribute' => 'companyId',
                'value' => static function (UsersNotifications $model) {
                    return $model->user->partnerCompany->pc_name ?? null;
                },
                'filter' => PartnerCompanyQuery::getList(),
                'label' => Yii::t('company', 'Company')
            ],
            [
                'attribute' => 'un_type_id',
                'value' => function (UsersNotifications $model) {
                    return '<span class="label label-default">' . UsersNotifications::getType($model->un_type_id) . '</span>';
                },
                'format' => 'raw',
                'filter' => UsersNotifications::getTypeList()
            ],
            'un_title',
            'un_new:booleanByLabel',
            'un_deleted:booleanByLabel',
            [
                'class' => DateTimeColumn::class,
                'attribute' => 'un_read_dt'
            ],
            [
                'class' => DateTimeColumn::class,
                'attribute' => 'un_created_dt'
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {delete}'
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
