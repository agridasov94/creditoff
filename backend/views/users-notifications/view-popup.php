<?php

use backend\src\entities\usersNotifications\entity\UsersNotifications;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model UsersNotifications */

$this->title = 'Notification - ' . $model->un_title;

?>

<div class="notifications-view">

    <h1><i class="fa fa-bell-o"></i> <?= Html::encode($model->un_title) ?></h1>

    <div class="row">
        <div class="col-md-5">
            <pre><?php echo $model->un_title; ?></pre>
            <pre><?php echo Yii::$app->formatter->asTextWithLinks($model->un_message); ?></pre>
        </div>
        <div class="col-md-7">

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    //'n_title',
                    'n_id',
                    [
                        'attribute' => 'n_type_id',
                        'value' => function (UsersNotifications $model) {
                            return UsersNotifications::getType($model->un_type_id);
                        }
                    ],

                    //'n_message:ntext',
                    //'n_new:boolean',
                    [
                        'attribute' => 'n_read_dt',
                        'value' => static function (UsersNotifications $model) {
                            return $model->un_read_dt ? '<i class="fa fa-calendar"></i> ' . Yii::$app->formatter->asDatetime(strtotime($model->un_read_dt)) : '-';
                        },
                        'format' => 'raw'
                    ],

                    [
                        'attribute' => 'n_created_dt',
                        'value' => static function (UsersNotifications $model) {
                            return '<i class="fa fa-calendar"></i> ' . Yii::$app->formatter->asDatetime(strtotime($model->un_created_dt));
                        },
                        'format' => 'raw'
                    ],
                ],
            ]) ?>
        </div>

    </div>

</div>