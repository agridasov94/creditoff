<?php

use backend\src\entities\usersNotifications\entity\UsersNotifications;
use common\widgets\FlashAlert;
use dosamigos\datepicker\DatePicker;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel \backend\src\entities\usersNotifications\entity\UsersNotificationsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('notifications', 'My Notifications');
$this->params['breadcrumbs'][] = $this->title;
$gridId = 'notifications-list-gv';
$pjaxId = 'my-notifications-pjax';

?>
    <div class="notifications-list">

        <h1><i class="fa fa-bell-o"></i> <?= Html::encode($this->title) ?></h1>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= FlashAlert::widget() ?>
        <p>
            <?= Html::a('<i class="fa fa-check"></i> Make Read All', ['all-read'], [
                'class' => 'btn btn-info',
                'data' => [
                    'confirm' => Yii::t('notifications', 'Are you sure you want to mark read all notifications?'),
                    'method' => 'post',
                ],
            ]) ?>

            <?= Html::a('<i class="fa fa-times"></i> Delete All', ['all-delete'], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('notifications', 'Are you sure you want to delete all notifications?'),
                    'method' => 'post',
                ],
            ]) ?>
        </p>

        <?php
        $columns = [
//            ['class' => 'yii\grid\SerialColumn', 'contentOptions' => ['class' => 'serial-td']],

            //'n_id',

            //'n_user_id',

            //'n_title',
            [
                'attribute' => 'un_type_id',
                //'format' => 'html',
                'value' => function (UsersNotifications $model) {
                    return Html::tag('small', UsersNotifications::getType($model->un_type_id));
                },
                'format' => 'raw',
                'filter' => UsersNotifications::getTypeList()
            ],

            [
                'attribute' => 'un_title',
                'value' => static function (UsersNotifications $model) {
                    return Html::a($model->un_title, ['/notifications/view', 'id' => $model->un_id], ['data-pjax' => 0]);
                },
                'format' => 'raw',
                'contentOptions' => ['class' => 'group-td']
            ],

            //'n_message:ntextWithPurify',
            'un_message:textWithLinks',
            //'n_message:raw',

            'un_new:booleanByLabel',

            //'n_deleted:boolean',
            //'n_popup:boolean',
            //'n_popup_show:boolean',

//        [
//            'attribute' => 'n_read_dt',
//            'value' => static function (UsersNotifications $model) {
//                return $model->n_read_dt ? '<i class="fa fa-calendar"></i> ' . Yii::$app->formatter->asDatetime(strtotime($model->n_read_dt)) : '-';
//            },
//            'format' => 'raw',
//            'filter' => DatePicker::widget([
//                'model' => $searchModel,
//                'attribute' => 'n_read_dt',
//                'clientOptions' => [
//                    'autoclose' => true,
//                    'format' => 'yyyy-mm-dd',
//                ],
//                'options' => [
//                    'autocomplete' => 'off',
//                    'placeholder' => 'Choose Date'
//                ],
//            ]),
//        ],

            [
                'attribute' => 'un_created_dt',
                'value' => static function (UsersNotifications $model) {
                    return '<i class="fa fa-calendar"></i> ' . Yii::$app->formatter->asDatetime(strtotime($model->un_created_dt));
                },
                'format' => 'raw',
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'un_created_dt',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                    ],
                    'options' => [
                        'autocomplete' => 'off',
                        'placeholder' => 'Choose Date'
                    ],
                ]),
            ],

            //['class' => 'yii\grid\ActionColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                //'controller' => 'order-shipping',
                'template' => '{view} {soft-delete}',

                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<i class="glyphicon glyphicon-search"></i>', $url, [
                            'title' => Yii::t('notifications', 'View'), 'data-pjax' => 0
                        ]);
                    },
                    'soft-delete' => function ($url, $model) {
                        return Html::a('<i class="glyphicon glyphicon-remove-circle"></i>', $url, [
                            'title' => Yii::t('notifications', 'Delete'),
                            'data' => [
                                'confirm' => Yii::t('notifications', 'Are you sure you want to delete this message?'),
                                //'method' => 'post',
                            ],
                        ]);
                    }
                ],
            ],
        ];
        ?>
        <?php Pjax::begin(['id' => $pjaxId]); ?>
        <?= \yii\grid\GridView::widget([
            'id' => $gridId,
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
//            'tableOptions' => ['class' => 'table table-bordered table-condensed table-responsive tbl-notif-list'],
            'rowOptions' => function (UsersNotifications $model, $index, $widget, $grid) {
                /*if($model->n_type_id == 4) {
                    return ['style' => 'background-color:#f2dede'];
                }
                if($model->n_type_id == 3) {
                    return ['style' => 'background-color:#fcf8e3'];
                }
                if($model->n_type_id == 1) {
                    return ['style' =>  'background-color:#dff0d8'];
                }
                if($model->n_type_id == 2) {
                    return ['style' =>  'background-color:#d9edf7'];
                }*/

                if ($model->un_new) {
                    return ['class' =>  'bold'];
                }
            },
            'columns' => $columns,
        ]); ?>
        <?php Pjax::end(); ?>
    </div>

<?php
$css = <<<CSS
.tbl-notif-list.table-striped > tbody > tr:nth-child(2n+1) > td, 
.tbl-notif-list.table-striped > tbody > tr:nth-child(2n+1) > th,
.tbl-notif-list.table-striped tbody tr:nth-of-type(odd) {
    background-color: initial !important;
}
.tbl-notif-list .bold {
font-weight: bold;
}
CSS;
$this->registerCss($css);
