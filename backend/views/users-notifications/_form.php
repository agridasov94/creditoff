<?php

use backend\src\entities\usersNotifications\entity\UsersNotifications;
use backend\src\useCase\notifications\createByUser\CreateForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model CreateForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-notifications-form">

    <div class="row">
        <div class="col-md-3">
            <?php Pjax::begin(['enableReplaceState' => false, 'enablePushState' => false]) ?>

            <?php $form = ActiveForm::begin(['options' => ['data-pjax' => 1]]); ?>

            <?= $form->field($model, 'users')->widget(\backend\widgets\UserSelect2Widget::class, [
                'options' => ['multiple' => true]
            ]) ?>

            <?= $form->field($model, 'type')->dropDownList(UsersNotifications::getTypeList(), ['prompt' => '--']) ?>

            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'message')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'popup')->checkbox() ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('users_notifications', 'Create'), ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
