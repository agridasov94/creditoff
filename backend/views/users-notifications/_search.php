<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\src\entities\usersNotifications\entity\UsersNotificationsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-notifications-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'un_id') ?>

    <?= $form->field($model, 'un_unique_id') ?>

    <?= $form->field($model, 'un_user_id') ?>

    <?= $form->field($model, 'un_type_id') ?>

    <?= $form->field($model, 'un_title') ?>

    <?php // echo $form->field($model, 'un_message') ?>

    <?php // echo $form->field($model, 'un_new') ?>

    <?php // echo $form->field($model, 'un_deleted') ?>

    <?php // echo $form->field($model, 'un_popup') ?>

    <?php // echo $form->field($model, 'un_popup_show') ?>

    <?php // echo $form->field($model, 'un_read_dt') ?>

    <?php // echo $form->field($model, 'un_created_dt') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('users_notifications', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('users_notifications', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
