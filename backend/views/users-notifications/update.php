<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\src\entities\usersNotifications\entity\UsersNotifications */

$this->title = Yii::t('users_notifications', 'Update Users Notifications: {name}', [
    'name' => $model->un_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('users_notifications', 'Users Notifications'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->un_id, 'url' => ['view', 'id' => $model->un_id]];
$this->params['breadcrumbs'][] = Yii::t('users_notifications', 'Update');
?>
<div class="users-notifications-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
