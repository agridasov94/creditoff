<?php

use backend\src\entities\usersNotifications\entity\UsersNotifications;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\src\entities\usersNotifications\entity\UsersNotifications */

$this->title = $model->un_title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('users_notifications', 'Users Notifications'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="users-notifications-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('users_notifications', 'Delete'), ['delete', 'id' => $model->un_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('users_notifications', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'un_id',
            'un_unique_id',
            'un_user_id:username',
            [
                'attribute' => 'un_type_id',
                'value' => static function (UsersNotifications $model) {
                    return UsersNotifications::getType($model->un_type_id);
                }
            ],
            'un_title',
            'un_message:ntext',
            'un_new:booleanByLabel',
            'un_deleted:booleanByLabel',
            'un_popup:booleanByLabel',
            'un_popup_show:booleanByLabel',
            'un_read_dt:byUserDateTime',
            'un_created_dt:byUserDateTime',
        ],
    ]) ?>

</div>
