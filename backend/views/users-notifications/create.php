<?php

use backend\src\useCase\notifications\createByUser\CreateForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model CreateForm */

$this->title = Yii::t('users_notifications', 'Create Users Notifications');
$this->params['breadcrumbs'][] = ['label' => Yii::t('users_notifications', 'Users Notifications'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-notifications-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
