<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\src\entities\client\phone\entity\ClientPhone */

$this->title = $model->cp_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend.client', 'Client Phones'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="client-phone-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('backend.client', 'Update'), ['update', 'id' => $model->cp_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('backend.client', 'Delete'), ['delete', 'id' => $model->cp_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend.client', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'cp_id',
            'cp_client_id',
            'cp_phone',
            'cp_created_user_id',
            'cp_updated_user_id',
            'cp_created_dt',
            'cp_updated_dt',
        ],
    ]) ?>

</div>
