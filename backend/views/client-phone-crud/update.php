<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\src\entities\client\phone\entity\ClientPhone */

$this->title = Yii::t('backend.client', 'Update Client Phone: {name}', [
    'name' => $model->cp_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend.client', 'Client Phones'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cp_id, 'url' => ['view', 'id' => $model->cp_id]];
$this->params['breadcrumbs'][] = Yii::t('backend.client', 'Update');
?>
<div class="client-phone-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
