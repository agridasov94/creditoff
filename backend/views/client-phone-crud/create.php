<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\src\entities\client\phone\entity\ClientPhone */

$this->title = Yii::t('backend.client', 'Create Client Phone');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend.client', 'Client Phones'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-phone-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
