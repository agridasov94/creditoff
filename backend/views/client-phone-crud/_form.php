<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\src\entities\client\phone\entity\ClientPhone */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="client-phone-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cp_client_id')->textInput() ?>

    <?= $form->field($model, 'cp_phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cp_created_user_id')->textInput() ?>

    <?= $form->field($model, 'cp_updated_user_id')->textInput() ?>

    <?= $form->field($model, 'cp_created_dt')->textInput() ?>

    <?= $form->field($model, 'cp_updated_dt')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend.client', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
