<?php

use backend\components\grid\DateTimeColumn;
use backend\components\grid\UserSelect2Column;
use common\src\entities\client\email\entity\ClientEmail;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\src\entities\client\email\entity\ClientEmailSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend.client', 'Client Emails');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-email-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('backend.client', 'Create Client Email'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'ce_id',
                'options' => [
                    'width' => '100px'
                ]
            ],
            [
                'attribute' => 'ce_client_id',
                'value' => static function (ClientEmail $model) {
                    return $model->client->getFullName();
                }
            ],
            'ce_email:email',
            [
                'class' => DateTimeColumn::class,
                'attribute' => 'ce_created_dt',
                'format' => 'byUserDateTime'
            ],
            [
                'class' => DateTimeColumn::class,
                'attribute' => 'ce_updated_dt',
                'format' => 'byUserDateTime'
            ],
            [
                'class' => UserSelect2Column::class,
                'attribute' => 'ce_created_user_id',
                'relation' => 'createdUser',
                'placeholder' => '',
            ],
            [
                'class' => UserSelect2Column::class,
                'attribute' => 'ce_updated_user_id',
                'relation' => 'updatedUser',
                'placeholder' => '',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
