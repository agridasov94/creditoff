<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\src\entities\client\email\entity\ClientEmail */

$this->title = $model->ce_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend.client', 'Client Emails'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="client-email-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('backend.client', 'Update'), ['update', 'id' => $model->ce_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('backend.client', 'Delete'), ['delete', 'id' => $model->ce_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend.client', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ce_id',
            'ce_client_id',
            'ce_email:email',
            'ce_created_user_id',
            'ce_updated_user_id',
            'ce_created_dt',
            'ce_updated_dt',
        ],
    ]) ?>

</div>
