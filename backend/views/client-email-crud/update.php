<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\src\entities\client\email\entity\ClientEmail */

$this->title = Yii::t('backend.client', 'Update Client Email: {name}', [
    'name' => $model->ce_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend.client', 'Client Emails'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ce_id, 'url' => ['view', 'id' => $model->ce_id]];
$this->params['breadcrumbs'][] = Yii::t('backend.client', 'Update');
?>
<div class="client-email-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
