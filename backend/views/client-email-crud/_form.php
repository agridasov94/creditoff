<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\src\entities\client\email\entity\ClientEmail */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="client-email-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ce_client_id')->textInput() ?>

    <?= $form->field($model, 'ce_email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ce_created_user_id')->textInput() ?>

    <?= $form->field($model, 'ce_updated_user_id')->textInput() ?>

    <?= $form->field($model, 'ce_created_dt')->textInput() ?>

    <?= $form->field($model, 'ce_updated_dt')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend.client', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
