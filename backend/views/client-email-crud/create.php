<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\src\entities\client\email\entity\ClientEmail */

$this->title = Yii::t('backend.client', 'Create Client Email');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend.client', 'Client Emails'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-email-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
