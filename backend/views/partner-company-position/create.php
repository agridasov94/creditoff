<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\src\entities\partnerCompany\position\entity\PartnerCompanyPosition */

$this->title = Yii::t('partner', 'Create Partner Company Position');
$this->params['breadcrumbs'][] = ['label' => Yii::t('partner', 'Partner Company Positions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="partner-company-position-create">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
