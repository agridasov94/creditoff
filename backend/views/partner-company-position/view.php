<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\src\entities\partnerCompany\position\entity\PartnerCompanyPosition */

$this->title = $model->pcp_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('partner', 'Partner Company Positions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="partner-company-position-view">

    <h4><?= Html::encode($this->title) ?></h4>

    <p>
        <?= Html::a(Yii::t('partner', 'Update'), ['update', 'id' => $model->pcp_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('partner', 'Delete'), ['delete', 'id' => $model->pcp_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('partner', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'pcp_id',
            'pcp_name',
            'pcp_created_user_id:username',
            'pcp_updated_user_id:username',
            'pcp_created_dt:byUserDateTime',
            'pcp_updated_dt:byUserDateTime',
        ],
    ]) ?>

</div>
