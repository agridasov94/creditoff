<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\src\entities\partnerCompany\position\entity\PartnerCompanyPosition */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">
    <div class="col-md-2">
        <div class="partner-company-position-form">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'pcp_name')->textInput(['maxlength' => true]) ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('partner', 'Save'), ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
