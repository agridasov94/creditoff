<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\src\entities\partnerCompany\position\entity\PartnerCompanyPosition */

$this->title = Yii::t('partner', 'Update Partner Company Position: {name}', [
    'name' => $model->pcp_name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('partner', 'Partner Company Positions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pcp_name, 'url' => ['view', 'id' => $model->pcp_id]];
$this->params['breadcrumbs'][] = Yii::t('partner', 'Update');
?>
<div class="partner-company-position-update">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
