<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\src\entities\partnerCompany\position\entity\PartnerCompanyPositionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="partner-company-position-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'pcp_id') ?>

    <?= $form->field($model, 'pcp_name') ?>

    <?= $form->field($model, 'pcp_created_user_id') ?>

    <?= $form->field($model, 'pcp_updated_user_id') ?>

    <?= $form->field($model, 'pcp_created_dt') ?>

    <?php // echo $form->field($model, 'pcp_updated_dt') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('partner', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('partner', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
