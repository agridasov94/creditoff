<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplication\clientPhone\entity\LoanApplicationClientPhone */

$this->title = Yii::t('loan_application', 'Create Loan Application Client Phone');
$this->params['breadcrumbs'][] = ['label' => Yii::t('loan_application', 'Loan Application Client Phones'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="loan-application-client-phone-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
