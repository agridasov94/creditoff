<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplication\clientPhone\entity\LoanApplicationClientPhone */

$this->title = Yii::t('loan_application', 'Update Loan Application Client Phone: {name}', [
    'name' => $model->lacp_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('loan_application', 'Loan Application Client Phones'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->lacp_id, 'url' => ['view', 'id' => $model->lacp_id]];
$this->params['breadcrumbs'][] = Yii::t('loan_application', 'Update');
?>
<div class="loan-application-client-phone-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
