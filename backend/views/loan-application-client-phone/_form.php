<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplication\clientPhone\entity\LoanApplicationClientPhone */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loan-application-client-phone-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'lacp_client_id')->textInput() ?>

    <?= $form->field($model, 'lacp_la_id')->textInput() ?>

    <?= $form->field($model, 'lacp_phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lacp_created_dt')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('loan_application', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
