<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplication\clientPhone\entity\LoanApplicationClientPhoneSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loan-application-client-phone-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'lacp_id') ?>

    <?= $form->field($model, 'lacp_client_id') ?>

    <?= $form->field($model, 'lacp_la_id') ?>

    <?= $form->field($model, 'lacp_phone') ?>

    <?= $form->field($model, 'lacp_created_dt') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('loan_application', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('loan_application', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
