<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\src\entities\partnerCompany\amountHistory\entity\PartnerCompanyAmountHistory */

$this->title = Yii::t('partner', 'Create Partner Company Amount History');
$this->params['breadcrumbs'][] = ['label' => Yii::t('partner', 'Partner Company Amount Histories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="partner-company-amount-history-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
