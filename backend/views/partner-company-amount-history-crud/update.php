<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\src\entities\partnerCompany\amountHistory\entity\PartnerCompanyAmountHistory */

$this->title = Yii::t('partner', 'Update Partner Company Amount History: {name}', [
    'name' => $model->pcah_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('partner', 'Partner Company Amount Histories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pcah_id, 'url' => ['view', 'id' => $model->pcah_id]];
$this->params['breadcrumbs'][] = Yii::t('partner', 'Update');
?>
<div class="partner-company-amount-history-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
