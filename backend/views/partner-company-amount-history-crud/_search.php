<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\src\entities\partnerCompany\amountHistory\entity\PartnerCompanyAmountHistorySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="partner-company-amount-history-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'pcah_id') ?>

    <?= $form->field($model, 'pcah_partner_company_id') ?>

    <?= $form->field($model, 'pcah_action') ?>

    <?= $form->field($model, 'pcah_action_dt') ?>

    <?= $form->field($model, 'pcah_amount_value') ?>

    <?php // echo $form->field($model, 'pcah_bonus_value') ?>

    <?php // echo $form->field($model, 'pcah_amount_before') ?>

    <?php // echo $form->field($model, 'pcah_amount_after') ?>

    <?php // echo $form->field($model, 'pcah_bonus_before') ?>

    <?php // echo $form->field($model, 'pcah_bonus_after') ?>

    <?php // echo $form->field($model, 'pcah_created_user_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('partner', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('partner', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
