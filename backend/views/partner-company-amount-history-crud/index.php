<?php

use backend\components\grid\DateTimeColumn;
use backend\components\grid\UserSelect2Column;
use backend\src\entities\partnerCompany\amountHistory\entity\PartnerCompanyAmountHistory;
use backend\src\entities\partnerCompany\entity\PartnerCompanyQuery;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\src\entities\partnerCompany\amountHistory\entity\PartnerCompanyAmountHistorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('partner', 'Partner Company Amount Histories');
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    [
        'attribute' => 'pcah_id',
        'options' => [
            'width' => '100px'
        ]
    ],
    [
        'attribute' => 'pcah_partner_company_id',
        'value' => static function (PartnerCompanyAmountHistory $model) {
            return $model->partnerCompany->pc_name . ' (' . $model->pcah_partner_company_id . ')';
        },
        'filter' => PartnerCompanyQuery::getList()
    ],
    [
        'attribute' => 'pcah_action',
        'value' => static function (PartnerCompanyAmountHistory $model) {
            return $model->getActionName();
        },
        'filter' => PartnerCompanyAmountHistory::getActionListName()
    ],
    [
        'class' => DateTimeColumn::class,
        'attribute' => 'pcah_action_dt',
        'format' => 'byUserDateTime'
    ],
    'pcah_amount_value',
    'pcah_bonus_value',
    'pcah_amount_before',
    'pcah_amount_after',
    'pcah_bonus_before',
    'pcah_bonus_after',
    [
        'class' => UserSelect2Column::class,
        'attribute' => 'pcah_created_user_id',
        'relation' => 'createdUser',
        'placeholder' => '',
    ],

    ['class' => 'yii\grid\ActionColumn'],
];
?>
<div class="partner-company-amount-history-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>

    <?php if(Yii::$app->request->isPjax): ?>
        <script>
            $('.export-wrapper .dropdown-menu a').each(function (i, e) {
                $(document).off('click.exportmenu', '#' + $(e).attr('id'));
            });
        </script>
    <?php endif; ?>
    <?= \kartik\export\ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'dropdownOptions' => [
            'label' => 'Export All',
            'class' => 'btn btn-outline-secondary btn-default'
        ],
        'container' => [
            'class' => 'export-wrapper'
        ],
        'showConfirmAlert' => false,
        'filename' => Yii::t('export', 'Partner_company_amount_history') . '_' . date('Y-m-d_H:i:s')
    ]) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns
    ]); ?>

    <?php Pjax::end(); ?>

</div>
