<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\src\entities\partnerCompany\amountHistory\entity\PartnerCompanyAmountHistory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="partner-company-amount-history-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'pcah_partner_company_id')->textInput() ?>

    <?= $form->field($model, 'pcah_action')->textInput() ?>

    <?= $form->field($model, 'pcah_action_dt')->textInput() ?>

    <?= $form->field($model, 'pcah_amount_value')->textInput() ?>

    <?= $form->field($model, 'pcah_bonus_value')->textInput() ?>

    <?= $form->field($model, 'pcah_amount_before')->textInput() ?>

    <?= $form->field($model, 'pcah_amount_after')->textInput() ?>

    <?= $form->field($model, 'pcah_bonus_before')->textInput() ?>

    <?= $form->field($model, 'pcah_bonus_after')->textInput() ?>

    <?= $form->field($model, 'pcah_created_user_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('partner', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
