<?php

use backend\src\entities\partnerCompany\amountHistory\entity\PartnerCompanyAmountHistory;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\src\entities\partnerCompany\amountHistory\entity\PartnerCompanyAmountHistory */

$this->title = $model->pcah_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('partner', 'Partner Company Amount Histories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="partner-company-amount-history-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('partner', 'Update'), ['update', 'id' => $model->pcah_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('partner', 'Delete'), ['delete', 'id' => $model->pcah_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('partner', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'pcah_id',
            [
                'attribute' => 'pcah_partner_company_id',
                'value' => static function (PartnerCompanyAmountHistory $model) {
                    return $model->partnerCompany->pc_name . ' (' . $model->pcah_id . ')';
                }
            ],
            [
                'attribute' => 'pcah_action',
                'value' => static function (PartnerCompanyAmountHistory $model) {
                    return $model->getActionName();
                }
            ],
            'pcah_action_dt:byUserDateTime',
            'pcah_amount_value',
            'pcah_bonus_value',
            'pcah_amount_before',
            'pcah_amount_after',
            'pcah_bonus_before',
            'pcah_bonus_after',
            'pcah_created_user_id:username',
        ],
    ]) ?>

</div>
