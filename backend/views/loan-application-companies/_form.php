<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplication\companies\entity\LoanApplicationCompanies */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loan-application-companies-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'lac_la_id')->textInput() ?>

    <?= $form->field($model, 'lac_partner_company_id')->textInput() ?>

    <?= $form->field($model, 'lac_created_dt')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('loan_application', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
