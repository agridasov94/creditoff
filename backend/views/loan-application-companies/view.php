<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplication\companies\entity\LoanApplicationCompanies */

$this->title = $model->lac_la_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('loan_application', 'Loan Application Companies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="loan-application-companies-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('loan_application', 'Update'), ['update', 'lac_la_id' => $model->lac_la_id, 'lac_partner_company_id' => $model->lac_partner_company_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('loan_application', 'Delete'), ['delete', 'lac_la_id' => $model->lac_la_id, 'lac_partner_company_id' => $model->lac_partner_company_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('loan_application', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'lac_la_id',
            'lac_partner_company_id',
            'lac_created_dt',
        ],
    ]) ?>

</div>
