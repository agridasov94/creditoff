<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplication\companies\entity\LoanApplicationCompanies */

$this->title = Yii::t('loan_application', 'Update Loan Application Companies: {name}', [
    'name' => $model->lac_la_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('loan_application', 'Loan Application Companies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->lac_la_id, 'url' => ['view', 'lac_la_id' => $model->lac_la_id, 'lac_partner_company_id' => $model->lac_partner_company_id]];
$this->params['breadcrumbs'][] = Yii::t('loan_application', 'Update');
?>
<div class="loan-application-companies-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
