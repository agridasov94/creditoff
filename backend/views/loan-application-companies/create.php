<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplication\companies\entity\LoanApplicationCompanies */

$this->title = Yii::t('loan_application', 'Create Loan Application Companies');
$this->params['breadcrumbs'][] = ['label' => Yii::t('loan_application', 'Loan Application Companies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="loan-application-companies-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
