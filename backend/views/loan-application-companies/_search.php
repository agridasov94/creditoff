<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplication\companies\entity\LoanApplicationCompaniesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loan-application-companies-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'lac_la_id') ?>

    <?= $form->field($model, 'lac_partner_company_id') ?>

    <?= $form->field($model, 'lac_created_dt') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('loan_application', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('loan_application', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
