<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \backend\src\useCase\users\manage\UserManageForm */
/* @var $user \common\src\entities\users\entity\Users */

$this->title = Yii::t('users', 'Update Users: {name}', [
    'name' => $user->getFullName(),
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('users', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $user->getFullName(), 'url' => ['view', 'id' => $user->id]];
$this->params['breadcrumbs'][] = Yii::t('users', 'Update');
?>
<div class="users-update">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
