<?php

use common\src\entities\users\entity\Users;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\src\entities\users\entity\Users */

$this->title = $model->getFullName();
$this->params['breadcrumbs'][] = ['label' => Yii::t('users', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="users-view">

    <h4><?= Html::encode($this->title) ?></h4>

    <p>
        <?= Html::a(Yii::t('users', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('users', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('users', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="row">
        <div class="col-md-4">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'username',
                    'first_name',
                    'last_name',
                    'auth_key',
                    'email:email',
                    'phone',
                    [
                        'attribute' => 'status',
                        'value' => static function (Users $model) {
                            return $model->getStatusName();
                        },
                    ],
                    'is_online:booleanByLabel',
                    [
                        'attribute' => 'partner_company_id',
                        'value' => static function (Users $model) {
                            return $model->partner_company_id ? $model->partnerCompany->pc_name : null;
                        },
                    ],
                    [
                        'attribute' => 'partner_company_position_id',
                        'value' => static function (Users $model) {
                            return $model->partner_company_position_id ? $model->partnerCompanyPosition->pcp_name : null;
                        },
                    ],
                    [
                        'attribute' => 'roles',
                        'value' => static function (Users $model) {
                            return implode(',', $model->getRoles());
                        },
                        'label' => Yii::t('users', 'Roles')
                    ],

                    [
                        'attribute' => 'products',
                        'value' => static function (Users $model) {
                            $userProductAccess = $model->usersProductAccess;
                            $access = [];
                            foreach ($userProductAccess as $productAccess) {
                                $access[] = $productAccess->product->p_name;
                            }
                            return implode(',', $access) ?: null;
                        },
                        'label' => Yii::t('users', 'Product Access')
                    ],
                    'verification_token',
                    'created_dt:byUserDateTime',
                    'updated_dt:byUserDateTime',
                    'last_login_dt:byUserDateTime',
                    'created_user_id:username',
                    'updated_user_id:username',
                ],
            ]) ?>
        </div>
    </div>
</div>
