<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \backend\src\useCase\users\manage\UserManageForm */

$this->title = Yii::t('users', 'Create User');
$this->params['breadcrumbs'][] = ['label' => Yii::t('users', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-create">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
