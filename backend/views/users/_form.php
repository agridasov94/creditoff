<?php

use backend\src\entities\partnerCompany\entity\PartnerCompanyQuery;
use backend\src\entities\partnerCompany\position\entity\PartnerCompanyPositionQuery;
use borales\extensions\phoneInput\PhoneInput;
use common\src\entities\product\entity\ProductQuery;
use common\src\entities\users\entity\Users;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \backend\src\useCase\users\manage\UserManageForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">
    <?php $form = ActiveForm::begin(); ?>

        <div class="row">
            <div class="col-md-4">
                <?= $form->errorSummary($model, ['class' => 'alert alert-danger']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-2">
                <?= $form->field($model, 'id')->hiddenInput()->label(false) ?>

                <?= $form->field($model, 'username')->textInput() ?>

                <?= $form->field($model, 'firstName')->textInput() ?>

                <?= $form->field($model, 'lastName')->textInput() ?>

                <?= $form->field($model, 'password', [
                    'options' => [
                        'class' => 'form-group'
                    ]
                ])->passwordInput(['autocomplete' => 'new-password'])?>

                <?= $form->field($model, 'rePassword', [
                    'options' => [
                        'class' => 'form-group'
                    ]
                ])->passwordInput(['autocomplete' => 'new-password'])?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'phone')->input('text') ?>

                <?= $form->field($model, 'email')->input('email') ?>

                <?= $form->field($model, 'status')->dropDownList(Users::getStatusNameList(), ['prompt' => '---']) ?>

                <?= $form->field($model, 'partnerCompanyId')->dropDownList(PartnerCompanyQuery::getList(), ['prompt' => '---']) ?>

                <?= $form->field($model, 'partnerCompanyPositionId')->dropDownList(PartnerCompanyPositionQuery::getList(), ['prompt' => '---']) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'roles')->widget(Select2::class, [
                    'data' => Users::getAllRoles(),
                    'size' => Select2::SIZE_SMALL,
                    'options' => ['placeholder' => Yii::t('users', 'Select user roles'), 'multiple' => true],
                    'pluginOptions' => ['allowClear' => true],
                ]) ?>

                <?= $form->field($model, 'products')->widget(Select2::class, [
                    'data' => ProductQuery::getList(),
                    'size' => Select2::SIZE_SMALL,
                    'options' => ['placeholder' => Yii::t('users', 'Select product'), 'multiple' => true],
                    'pluginOptions' => ['allowClear' => true],
                ]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('users', 'Save'), ['class' => 'btn btn-success']) ?>
                </div>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
</div>
