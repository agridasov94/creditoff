<?php

use backend\components\grid\DateTimeColumn;
use backend\components\grid\UserSelect2Column;
use backend\src\auth\Auth;
use backend\src\entities\partnerCompany\entity\PartnerCompanyQuery;
use backend\src\entities\partnerCompany\position\entity\PartnerCompanyPositionQuery;
use common\src\entities\users\entity\Users;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\src\entities\users\entity\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('users', 'Users');
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    [
        'attribute' => 'id',
        'options' => [
            'width' => '70px'
        ]
    ],
    [
        'class' => UserSelect2Column::class,
        'attribute' => 'id',
        'useRelation' => false,
        'options' => [
            'width' => '200px'
        ],
        'dataCellValueAttribute' => 'username',
    ],
    'first_name',
    'last_name',
    [
        'value' => static function (Users $model) {
            return implode(', ', $model->getRoles(true));
        }
    ],
    'email:email',
    'phone',
    [
        'attribute' => 'status',
        'value' => static function (Users $model) {
            return $model->getStatusNameLabel();
        },
        'filter' => Users::getStatusNameList(),
        'format' => 'raw'
    ],
    'is_online:booleanByLabel',
    [
        'attribute' => 'partner_company_id',
        'value' => static function (Users $model) {
            return $model->partner_company_id ? $model->partnerCompany->pc_name : null;
        },
        'filter' => PartnerCompanyQuery::getList()
    ],
    [
        'attribute' => 'partner_company_position_id',
        'value' => static function (Users $model) {
            return $model->partner_company_position_id ? $model->partnerCompanyPosition->pcp_name : null;
        },
        'filter' => PartnerCompanyPositionQuery::getList()
    ],
    [
        'class' => DateTimeColumn::class,
        'attribute' => 'created_dt',
        'format' => 'byUserDateTime'
    ],
    [
        'class' => DateTimeColumn::class,
        'attribute' => 'updated_dt',
        'format' => 'byUserDateTime'
    ],
    [
        'class' => DateTimeColumn::class,
        'attribute' => 'last_login_dt',
        'format' => 'byUserDateTime'
    ],
    [
        'class' => UserSelect2Column::class,
        'attribute' => 'created_user_id',
        'relation' => 'createdUser',
        'placeholder' => '',
    ],
    [
        'class' => UserSelect2Column::class,
        'attribute' => 'updated_user_id',
        'relation' => 'updatedUser',
        'placeholder' => '',
    ],

    [
        'class' => 'yii\grid\ActionColumn',
        'buttons' => [
            'block' => static function ($url) {
                $content = Html::tag('i', '', ['class' => 'fas fa-ban']);
                return Html::a($content, $url, ['title' => Yii::t('users', 'Block User'), 'class' => 'block_user']);
            },
            'switch' => static function ($url, Users $model, $key) {
                return Html::a(
                    '<span class="fa fa-sign-in text-warning"></span>',
                    ['users/switch', 'id' => $model->id],
                    ['title' => Yii::t('users', 'switch User'), 'data' => [
                        'confirm' => Yii::t('users', 'Are you sure you want to switch user?'),
                        //'method' => 'get',
                    ],
                    ]
                );
            },
        ],
        'visibleButtons' => [
            'block' => static function (Users $model) {
                return !$model->isDeletedOrDeactivated();
            },
            'switch' => static function (Users $model, $key, $index) {
                return Auth::can('/users/switch');
            },
        ],
        'template' => '{view} {update} {delete} {block} {switch}'
    ],
];
?>
<div class="users-index">

    <h4><?= Html::encode($this->title) ?></h4>

    <p>
        <?= Html::a(Yii::t('users', 'Create Users'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= \common\widgets\FlashAlert::widget() ?>

    <?php Pjax::begin(); ?>

    <?php if(Yii::$app->request->isPjax): ?>
        <script>
            $('.export-wrapper .dropdown-menu a').each(function (i, e) {
                $(document).off('click.exportmenu', '#' + $(e).attr('id'));
            });
        </script>
    <?php endif; ?>
    <?= \kartik\export\ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'dropdownOptions' => [
            'label' => 'Export All',
            'class' => 'btn btn-outline-secondary btn-default'
        ],
        'container' => [
            'class' => 'export-wrapper'
        ],
        'showConfirmAlert' => false,
        'filename' => Yii::t('export', 'Users') . '_' . date('Y-m-d_H:i:s')
    ]) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns
    ]); ?>

    <?php Pjax::end(); ?>

</div>
<?php
$confirmText = Yii::t('users', 'Confirm user lock');
$js = <<<JS
$('.block_user').on('click', function (e) {
    e.preventDefault();
    let url = $(this).attr('href');
    if (confirm('$confirmText')) {
        window.location.href = url;
    }
});
JS;
$this->registerJs($js);
