<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\src\entities\client\passportData\entity\ClientPassportDataSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="client-passport-data-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'cpd_client_id') ?>

    <?= $form->field($model, 'cpd_type') ?>

    <?= $form->field($model, 'cpd_series') ?>

    <?= $form->field($model, 'cpd_number') ?>

    <?= $form->field($model, 'cpd_issued_by') ?>

    <?php // echo $form->field($model, 'cpd_issue_date') ?>

    <?php // echo $form->field($model, 'cpd_issue_department_code') ?>

    <?php // echo $form->field($model, 'cpd_expiration_date') ?>

    <?php // echo $form->field($model, 'cpd_registered_address') ?>

    <?php // echo $form->field($model, 'cpd_residence_address') ?>

    <?php // echo $form->field($model, 'cpd_created_user_id') ?>

    <?php // echo $form->field($model, 'cpd_updated_user_id') ?>

    <?php // echo $form->field($model, 'cpd_created_dt') ?>

    <?php // echo $form->field($model, 'cpd_updated_dt') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend.client', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('backend.client', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
