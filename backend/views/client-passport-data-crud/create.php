<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\src\entities\client\passportData\entity\ClientPassportData */

$this->title = Yii::t('backend.client', 'Create Client Passport Data');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend.client', 'Client Passport Datas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-passport-data-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
