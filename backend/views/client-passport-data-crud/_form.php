<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\src\entities\client\passportData\entity\ClientPassportData */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="client-passport-data-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cpd_client_id')->textInput() ?>

    <?= $form->field($model, 'cpd_type')->textInput() ?>

    <?= $form->field($model, 'cpd_series')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cpd_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cpd_issued_by')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cpd_issue_date')->textInput() ?>

    <?= $form->field($model, 'cpd_issue_department_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cpd_expiration_date')->textInput() ?>

    <?= $form->field($model, 'cpd_registered_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cpd_residence_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cpd_created_user_id')->textInput() ?>

    <?= $form->field($model, 'cpd_updated_user_id')->textInput() ?>

    <?= $form->field($model, 'cpd_created_dt')->textInput() ?>

    <?= $form->field($model, 'cpd_updated_dt')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend.client', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
