<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\src\entities\client\passportData\entity\ClientPassportData */

$this->title = Yii::t('backend.client', 'Update Client Passport Data: {name}', [
    'name' => $model->cpd_client_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend.client', 'Client Passport Datas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cpd_client_id, 'url' => ['view', 'id' => $model->cpd_client_id]];
$this->params['breadcrumbs'][] = Yii::t('backend.client', 'Update');
?>
<div class="client-passport-data-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
