<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\src\entities\client\passportData\entity\ClientPassportData */

$this->title = $model->cpd_client_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend.client', 'Client Passport Datas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="client-passport-data-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('backend.client', 'Update'), ['update', 'id' => $model->cpd_client_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('backend.client', 'Delete'), ['delete', 'id' => $model->cpd_client_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend.client', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'cpd_client_id',
            'cpd_type',
            'cpd_series',
            'cpd_number',
            'cpd_issued_by',
            'cpd_issue_date',
            'cpd_issue_department_code',
            'cpd_expiration_date',
            'cpd_registered_address',
            'cpd_residence_address',
            'cpd_created_user_id',
            'cpd_updated_user_id',
            'cpd_created_dt',
            'cpd_updated_dt',
        ],
    ]) ?>

</div>
