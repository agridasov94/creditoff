<?php

use backend\components\grid\DateTimeColumn;
use backend\components\grid\UserSelect2Column;
use common\src\entities\client\passportData\entity\ClientPassportData;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\src\entities\client\passportData\entity\ClientPassportDataSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend.client', 'Client Passport Datas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-passport-data-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('backend.client', 'Create Client Passport Data'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'cpd_client_id',
                'value' => static function (ClientPassportData $model) {
                    return $model->client->getFullName() . ' (' . $model->cpd_client_id . ')';
                }
            ],
            [
                'attribute' => 'cpd_type',
                'value' => static function (ClientPassportData $model) {
                    return $model->cpd_type ? ClientPassportData::getTypeName($model->cpd_type) : null;
                },
                'filter' => ClientPassportData::getTypeListName()
            ],
            'cpd_series',
            'cpd_number',
            'cpd_issued_by',
            [
                'class' => DateTimeColumn::class,
                'attribute' => 'cpd_issue_date',
                'format' => 'byUserDate'
            ],
            'cpd_issue_department_code',
            [
                'class' => DateTimeColumn::class,
                'attribute' => 'cpd_expiration_date',
                'format' => 'byUserDate'
            ],
            'cpd_registered_address',
            'cpd_residence_address',
            [
                'class' => DateTimeColumn::class,
                'attribute' => 'cpd_created_dt',
                'format' => 'byUserDateTime'
            ],
            [
                'class' => DateTimeColumn::class,
                'attribute' => 'cpd_updated_dt',
                'format' => 'byUserDateTime'
            ],
            [
                'class' => UserSelect2Column::class,
                'attribute' => 'cpd_created_user_id',
                'relation' => 'createdUser',
                'placeholder' => '',
            ],
            [
                'class' => UserSelect2Column::class,
                'attribute' => 'cpd_updated_user_id',
                'relation' => 'updatedUser',
                'placeholder' => '',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
