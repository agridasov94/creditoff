<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\src\entities\product\entity\ProductSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'p_id') ?>

    <?= $form->field($model, 'p_name') ?>

    <?= $form->field($model, 'p_is_active') ?>

    <?= $form->field($model, 'p_min_credit_amount') ?>

    <?= $form->field($model, 'p_max_credit_amount') ?>

    <?php // echo $form->field($model, 'p_min_credit_term') ?>

    <?php // echo $form->field($model, 'p_max_credit_term') ?>

    <?php // echo $form->field($model, 'p_min_request_price') ?>

    <?php // echo $form->field($model, 'p_created_user_id') ?>

    <?php // echo $form->field($model, 'p_updated_user_id') ?>

    <?php // echo $form->field($model, 'p_created_dt') ?>

    <?php // echo $form->field($model, 'p_updated_dt') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('product', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('product', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
