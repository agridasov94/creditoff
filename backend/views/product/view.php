<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\src\entities\product\entity\Product */

$this->title = $model->p_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('product', 'Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="product-view">

    <h4><?= Html::encode($this->title) ?></h4>

    <p>
        <?= Html::a(Yii::t('product', 'Update'), ['update', 'id' => $model->p_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('product', 'Delete'), ['delete', 'id' => $model->p_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('product', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'p_id',
            'p_name',
            'p_is_active:booleanByLabel',
            'p_min_credit_amount',
            'p_max_credit_amount',
            'p_min_credit_term',
            'p_max_credit_term',
            'p_min_request_price',
            'p_step_in_queue',
            'p_sort_order',
            'p_step_in_queue_auto_calculate:booleanByLabel',
            'p_created_user_id:username',
            'p_updated_user_id:username',
            'p_created_dt:byUserDateTime',
            'p_updated_dt:byUserDateTime',
        ],
    ]) ?>

</div>
