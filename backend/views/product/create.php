<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\src\entities\product\entity\Product */

$this->title = Yii::t('product', 'Create Product');
$this->params['breadcrumbs'][] = ['label' => Yii::t('product', 'Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-create">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
