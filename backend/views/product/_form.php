<?php

use kdn\yii2\JsonEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\src\entities\product\entity\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-md-2">
        <div class="product-form">


            <?= $form->field($model, 'p_name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'p_is_active')->checkbox() ?>

            <?= $form->field($model, 'p_min_credit_amount')->input('number') ?>

            <?= $form->field($model, 'p_max_credit_amount')->input('number') ?>

            <?= $form->field($model, 'p_min_credit_term')->input('number') ?>

            <?= $form->field($model, 'p_max_credit_term')->input('number') ?>

            <?= $form->field($model, 'p_min_request_price')->input('number') ?>

            <?= $form->field($model, 'p_step_in_queue')->input('number') ?>

            <?= $form->field($model, 'p_sort_order')->input('number') ?>

            <?= $form->field($model, 'p_step_in_queue_auto_calculate')->checkbox() ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('product', 'Save'), ['class' => 'btn btn-success']) ?>
            </div>


        </div>
    </div>
    <div class="col-md-3">
        <?php
            try {
                echo $form->field($model, 'p_settings')->widget(
                    JsonEditor::class,
                    [
                        'clientOptions' => [
                            'modes' => ['code', 'form', 'tree', 'view'], //'text',
                            'mode' => 'tree'
                        ],
                        //'collapseAll' => ['view'],
                        'expandAll' => ['tree', 'form'],
                    ]
                );
            } catch (Exception $exception) {
                echo $form->field($model, 'p_settings')->textarea(['rows' => 5]);//->label($model->s_name);
            }
        ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
