<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\src\entities\product\entity\Product */

$this->title = Yii::t('product', 'Update Product: {name}', [
    'name' => $model->p_name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('product', 'Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->p_name, 'url' => ['view', 'id' => $model->p_id]];
$this->params['breadcrumbs'][] = Yii::t('product', 'Update');
?>
<div class="product-update">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
