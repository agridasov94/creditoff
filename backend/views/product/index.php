<?php

use backend\components\grid\DateTimeColumn;
use backend\components\grid\UserSelect2Column;
use common\src\entities\product\entity\Product;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\src\entities\product\entity\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('product', 'Products');
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    [
        'attribute' => 'p_id',
        'options' => [
            'width' => '100px'
        ]
    ],
    'p_name',
    [
        'attribute' => 'p_is_active',
        'format' => 'booleanByLabel',
        'filter' => [0 => Yii::t('backend', 'No'), 1 => Yii::t('frontend', 'Yes')],
    ],
    'p_min_credit_amount',
    'p_max_credit_amount',
    'p_min_credit_term',
    'p_max_credit_term',
    'p_min_request_price',
    'p_step_in_queue',
    'p_sort_order',
    [
        'attribute' => 'p_step_in_queue_auto_calculate',
        'filter' => [0 => Yii::t('backend', 'No'), 1 => Yii::t('frontend', 'Yes')],
        'format' => 'booleanByLabel'
    ],
    [
        'class' => DateTimeColumn::class,
        'attribute' => 'p_created_dt',
        'format' => 'byUserDateTime'
    ],
    [
        'class' => DateTimeColumn::class,
        'attribute' => 'p_updated_dt',
        'format' => 'byUserDateTime'
    ],
    [
        'class' => UserSelect2Column::class,
        'attribute' => 'p_created_user_id',
        'relation' => 'createdUser',
        'placeholder' => '',
    ],
    [
        'class' => UserSelect2Column::class,
        'attribute' => 'p_updated_user_id',
        'relation' => 'updatedUser',
        'placeholder' => '',
    ],
];

$additionalExportColumn = [
    [
        'label' => Yii::t('product', 'Step In Queue: C'),
        'value' => static function (Product $model) {
            return $model->getStepInQueueVariables()['c'] ?? null;
        },
    ],
    [
        'label' => Yii::t('product', 'Step In Queue: D'),
        'value' => static function (Product $model) {
            return $model->getStepInQueueVariables()['d'] ?? null;
        },
    ],
    [
        'label' => Yii::t('product', 'Step In Queue: I'),
        'value' => static function (Product $model) {
            return $model->getStepInQueueVariables()['i'] ?? null;
        },
    ],
];
?>
<div class="product-index">

    <h4><?= Html::encode($this->title) ?></h4>

    <p>
        <?= Html::a(Yii::t('product', 'Create Product'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>

    <?php if(Yii::$app->request->isPjax): ?>
        <script>
            $('.export-wrapper .dropdown-menu a').each(function (i, e) {
                $(document).off('click.exportmenu', '#' + $(e).attr('id'));
            });
        </script>
    <?php endif; ?>
    <?= \kartik\export\ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => \yii\helpers\ArrayHelper::merge($gridColumns, $additionalExportColumn),
        'dropdownOptions' => [
            'label' => 'Export All',
            'class' => 'btn btn-outline-secondary btn-default'
        ],
        'container' => [
            'class' => 'export-wrapper'
        ],
        'showConfirmAlert' => false,
        'filename' => Yii::t('export', 'Product') . '_' . date('Y-m-d_H:i:s')
    ]) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => \yii\helpers\ArrayHelper::merge($gridColumns, [[
            'attribute' => 'p_settings',
            'value' => static function (Product $model) {

                $val = Html::encode($model->p_settings);

                if ($model->p_settings && $decodedData = @json_decode($model->p_settings, true, 512, JSON_THROW_ON_ERROR)) {
                    $truncatedStr = '<pre><small>' .
                        StringHelper::truncate(
                            VarDumper::dumpAsString($decodedData),
                            10,
                            '...',
                            null,
                            false
                        ) . '</small></pre>';
                    $detailData   = VarDumper::dumpAsString($decodedData, 10, true);
                    $detailBox    = '<div id="detail_' . $model->p_id . '" style="display: none;">' . $detailData . '</div>';
                    $detailBtn    = ' <i class="fas fa-eye green showDetail" style="cursor: pointer;" data-idt="' . $model->p_id . '"></i>';
                    $val          = $truncatedStr . $detailBox . $detailBtn;
                }
                return $val;
            },
            'format' => 'raw',
        ], ['class' => 'yii\grid\ActionColumn'],
        ])
    ]); ?>

    <?php Pjax::end(); ?>

</div>
<?php
yii\bootstrap4\Modal::begin([
    'title' => 'Log detail',
    'id' => 'modal',
    'size' => \yii\bootstrap4\Modal::SIZE_LARGE,
]);
yii\bootstrap4\Modal::end();


$jsCode = <<<JS
    $(document).on('click', '.showDetail', function(){
        
        let recordId = $(this).data('idt');
        let detailEl = $('#detail_' + recordId);
        let modalBodyEl = $('#modal .modal-body');
        
        modalBodyEl.html(detailEl.html()); 
        $('#modal-label').html('Details (' + recordId + ')');       
        $('#modal').modal('show');
        return false;
    });
JS;

$this->registerJs($jsCode, \yii\web\View::POS_READY);