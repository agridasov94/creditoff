<?php

use kartik\editable\Editable;
use backend\src\auth\Auth;
use backend\src\entities\usersProductAccess\entity\UsersProductAccess;
use common\src\entities\product\entity\ProductQuery;
use yii\bootstrap4\Alert;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\src\entities\usersProductAccess\entity\UsersProductAccessSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('users', 'Manage Product Prices');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-product-access-index">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= Alert::widget([
        'body' => Yii::t('product', 'Each price change entails redistribution of credit requests between partners'),
        'options' => [
            'class' => 'alert alert-info'
        ],
        'closeButton' => false
    ]) ?>

    <?php Pjax::begin(['id' => 'manage-product-price-pjax']); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'upa_product_id',
                'value' => static function (UsersProductAccess $model) {
                    return $model->product->p_name;
                },
                'filter' => ProductQuery::getListByUserId(Auth::id())
            ],
            [
                'attribute' => 'upa_request_price',
                'value' => static function (UsersProductAccess $model) {
                    if ($model->isActive()) {
                        return Editable::widget([
                            'name' => 'requestPrice',
                            'asPopover' => false,
                            'value' => $model->upa_request_price,
                            'size' => 'sm',
                            'options' => ['class'=>'form-control'],
                            'header' => Yii::t('product', '{attribute}', [
                                'attribute' => $model->getAttributeLabel('upa_request_price')
                            ]),
                            'additionalData' => [
                                'productId' => $model->upa_product_id,
                                'editPrice' => true
                            ]
                        ]);
                    }

                    return $model->upa_request_price;
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'minimalPrice',
                'value' => static function (UsersProductAccess $model) {
                    return $model->product->p_min_request_price;
                },
                'label' => Yii::t('product', 'Minimum available price for credit request')
            ],
            [
                'attribute' => 'upa_status',
                'value' => static function (UsersProductAccess $model) {
                    return Editable::widget([
                        'name' => 'status',
                        'data' => UsersProductAccess::getStatusListName(),
                        'value' => $model->getStatusLabel(),
                        'inputType' => Editable::INPUT_DROPDOWN_LIST,
                        'asPopover' => false,
                        'additionalData' => [
                            'productId' => $model->upa_product_id,
                            'editStatus' => true
                        ],
                        'displayValueConfig'=> UsersProductAccess::getStatusLabelList(),
                        'pluginEvents' => [
                            'editableSuccess' => new \yii\web\JsExpression('
                                function() {$.pjax.reload({container: "#manage-product-price-pjax", push: false, replace: false});}
                            ')
                        ],
                        'pjaxContainerId' => 'manage-product-price-pjax'
                    ]);
                },
                'format' => 'raw',
                'filter' => UsersProductAccess::getStatusListName()
            ],
//            [
//                'attribute' => 'upa_status',
//                'value' => static function (UsersProductAccess $model) {
//                    return $model->getStatusLabel();
//                },
//                'format' => 'raw',
//                'filter' => UsersProductAccess::getStatusListName()
//            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
