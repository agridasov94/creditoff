<?php

use backend\components\grid\UserSelect2Column;
use backend\src\entities\partnerCompany\entity\PartnerCompanyQuery;
use kartik\editable\Editable;
use backend\src\auth\Auth;
use backend\src\entities\usersProductAccess\entity\UsersProductAccess;
use common\src\entities\product\entity\ProductQuery;
use yii\bootstrap4\Alert;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\src\entities\usersProductAccess\entity\UsersProductAccessSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('users', 'Manage Product Prices');
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    [
        'class' => UserSelect2Column::class,
        'attribute' => 'upa_user_id',
        'relation' => 'user',
        'placeholder' => '',
        'options' => [
            'width' => '300px'
        ]
    ],
    [
        'attribute' => 'companyId',
        'value' => static function (UsersProductAccess $model) {
            return $model->user->partnerCompany->pc_name ?? null;
        },
        'filter' => PartnerCompanyQuery::getList(),
        'label' => Yii::t('company', 'Company')
    ],
    [
        'attribute' => 'upa_product_id',
        'value' => static function (UsersProductAccess $model) {
            return $model->product->p_name;
        },
        'filter' => ProductQuery::getList()
    ],
    [
        'attribute' => 'upa_request_price',
        'value' => static function (UsersProductAccess $model) {
            if ($model->isActive() && (int)Yii::$app->request->post('export', 0) !== 1) {
                return Editable::widget([
                    'name' => 'requestPrice',
                    'asPopover' => false,
                    'value' => $model->upa_request_price,
                    'size' => 'sm',
                    'options' => ['class'=>'form-control'],
                    'header' => Yii::t('product', '{attribute}', [
                        'attribute' => $model->getAttributeLabel('upa_request_price')
                    ]),
                    'additionalData' => [
                        'productId' => $model->upa_product_id,
                        'editPrice' => true,
                        'userId' => $model->upa_user_id
                    ]
                ]);
            }

            return $model->upa_request_price;
        },
        'format' => 'raw',
        'options' => [
            'width' => '200px'
        ]
    ],
    [
        'attribute' => 'minimalPrice',
        'value' => static function (UsersProductAccess $model) {
            return $model->product->p_min_request_price;
        },
        'label' => Yii::t('product', 'Minimum available price for credit request'),
        'options' => [
            'width' => '200px'
        ]
    ],
    [
        'attribute' => 'upa_status',
        'value' => static function (UsersProductAccess $model) {
            if ((int)Yii::$app->request->post('export', 0) === 1) {
                return $model->getStatusLabel();
            }
            return Editable::widget([
                'name' => 'status',
                'data' => UsersProductAccess::getStatusListName(),
                'value' => $model->getStatusLabel(),
                'inputType' => Editable::INPUT_DROPDOWN_LIST,
                'asPopover' => false,
                'additionalData' => [
                    'productId' => $model->upa_product_id,
                    'editStatus' => true,
                    'userId' => $model->upa_user_id
                ],
                'displayValueConfig'=> UsersProductAccess::getStatusLabelList(),
                'pluginEvents' => [
                    'editableSuccess' => new \yii\web\JsExpression('
                                function() {$.pjax.reload({container: "#manage-product-price-pjax", push: false, replace: false});}
                            ')
                ],
                'pjaxContainerId' => 'manage-product-price-pjax'
            ]);
        },
        'format' => 'raw',
        'filter' => UsersProductAccess::getStatusListName()
    ],
//            [
//                'attribute' => 'upa_status',
//                'value' => static function (UsersProductAccess $model) {
//                    return $model->getStatusLabel();
//                },
//                'format' => 'raw',
//                'filter' => UsersProductAccess::getStatusListName()
//            ],
];
?>
<div class="users-product-access-index">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= Alert::widget([
        'body' => Yii::t('product', 'Each price change entails redistribution of credit requests between partners'),
        'options' => [
            'class' => 'alert alert-info'
        ],
        'closeButton' => false
    ]) ?>

    <?php Pjax::begin(['id' => 'manage-product-price-pjax']); ?>

    <?php if(Yii::$app->request->isPjax): ?>
        <script>
            $('.export-wrapper .dropdown-menu a').each(function (i, e) {
                $(document).off('click.exportmenu', '#' + $(e).attr('id'));
            });
        </script>
    <?php endif; ?>
    <?= \kartik\export\ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'dropdownOptions' => [
            'label' => 'Export All',
            'class' => 'btn btn-outline-secondary btn-default'
        ],
        'container' => [
            'class' => 'export-wrapper'
        ],
        'showConfirmAlert' => false,
        'filename' => Yii::t('export', 'Users_product_prices') . '_' . date('Y-m-d_H:i:s'),
        'exportRequestParam' => 'export'
    ]) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns
    ]); ?>

    <?php Pjax::end(); ?>

</div>

<?php
$css = <<<CSS
.btn-group.open .dropdown-menu {
display: block;
}
CSS;
$this->registerCss($css);

