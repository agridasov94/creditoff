<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\src\entities\blog\posts\entity\BlogPostsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="blog-posts-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'bp_id') ?>

    <?= $form->field($model, 'bp_lang_id') ?>

    <?= $form->field($model, 'bp_category_id') ?>

    <?= $form->field($model, 'bp_title') ?>

    <?= $form->field($model, 'bp_short_description') ?>

    <?php // echo $form->field($model, 'bp_content') ?>

    <?php // echo $form->field($model, 'bp_author_id') ?>

    <?php // echo $form->field($model, 'bp_alias') ?>

    <?php // echo $form->field($model, 'bp_active') ?>

    <?php // echo $form->field($model, 'bp_created_user_id') ?>

    <?php // echo $form->field($model, 'bp_updated_user_id') ?>

    <?php // echo $form->field($model, 'bp_created_dt') ?>

    <?php // echo $form->field($model, 'bp_updated_dt') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend.blog_posts', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('backend.blog_posts', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
