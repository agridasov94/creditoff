<?php

use backend\components\grid\DateTimeColumn;
use backend\components\grid\UserSelect2Column;
use common\src\entities\blog\categories\entity\BlogCategoriesQuery;
use common\src\entities\language\LanguageQuery;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\src\entities\blog\posts\entity\BlogPostsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend.blog_posts', 'Blog Posts');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blog-posts-index">

    <?= \common\widgets\FlashAlert::widget() ?>

    <h3><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a(Yii::t('backend.blog_posts', 'Create Blog Posts'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'bp_id',
                'options' => [
                    'width' => '100px'
                ]
            ],
            [
                'attribute' => 'bp_lang_id',
                'options' => [
                    'width' => '100px'
                ],
                'filter' => LanguageQuery::getList()
            ],
            [
                'attribute' => 'bp_category_id',
                'value' => static function (\common\src\entities\blog\posts\entity\BlogPosts $model) {
                    return $model->category->bc_name;
                },
                'filter' => BlogCategoriesQuery::getListByLang(Yii::$app->language)
            ],
            'bp_title',
            [
                'class' => UserSelect2Column::class,
                'attribute' => 'bp_author_id',
                'relation' => 'author',
                'placeholder' => '',
            ],
            'bp_alias',
            [
                'attribute' => 'bp_active',
                'format' => 'booleanByLabel',
                'filter' => [0 => Yii::t('backend', 'No'), 1 => Yii::t('frontend', 'Yes')],
            ],
            [
                'class' => UserSelect2Column::class,
                'attribute' => 'bp_created_user_id',
                'relation' => 'createdUser',
                'placeholder' => '',
            ],
            [
                'class' => UserSelect2Column::class,
                'attribute' => 'bp_updated_user_id',
                'relation' => 'updatedUser',
                'placeholder' => '',
            ],
            [
                'class' => DateTimeColumn::class,
                'attribute' => 'bp_created_dt',
                'format' => 'byUserDateTime'
            ],
            [
                'class' => DateTimeColumn::class,
                'attribute' => 'bp_updated_dt',
                'format' => 'byUserDateTime'
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
