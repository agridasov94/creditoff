<?php

use backend\widgets\UserSelect2Widget;
use common\src\entities\blog\categories\entity\BlogCategoriesQuery;
use common\src\entities\users\entity\Users;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \backend\src\useCase\blogCreate\BlogPostCreateForm */
/* @var $form yii\widgets\ActiveForm */

$user = $model->authorId ? Users::findOne($model->authorId) : null;

$css = <<<CSS
#blogpostcreateform-content-container {
    height: 100%;
}
CSS;
$this->registerCss($css);
?>

<div class="blog-posts-form">

    <div class="row">
        <div class="col-md-12">
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

            <div class="row">
                <div class="col-md-3">
                    <?= $form->errorSummary($model, ['class' => 'alert alert-danger', 'role' => 'alert']) ?>

                    <?= $form->field($model, 'langId')->hiddenInput()->label(false) ?>

                    <?= $form->field($model, 'categoryId')->dropDownList(BlogCategoriesQuery::getListByLang(Yii::$app->language), ['prompt' => '---']) ?>

                    <?= $form->field($model, 'title')->textInput(['maxlength' => true, 'id' => 'title']) ?>

                    <?= $form->field($model, 'url')->textInput(['maxlength' => true, 'id' => 'url-address']) ?>

                    <?= $form->field($model, 'shortDescription')->textarea(['maxlength' => true, 'rows' => 5]) ?>

                    <?= $form->field($model, 'authorId')->widget(UserSelect2Widget::class, [
                        'data' => $user ? [
                            $user->id => $user->username
                        ] : [],
                    ]) ?>

                    <?= $form->field($model, 'active')->checkbox() ?>

                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('backend.blog_posts', 'Save'), ['class' => 'btn btn-success']) ?>
                    </div>

                </div>
                <div class="col-md-9">
                    <div id="preview-image">
                        <?= $model->preview ? Html::img($model->preview) : '' ?>
                    </div>
                    <?= $form->field($model, 'preview')->fileInput(['id' => 'upload-preview']) ?>

                    <?= $form->field($model, 'content')->widget(\kartik\editors\Summernote::class, [
                        'options' => [
                            'rows' => 6,
                            'readonly' => false,
                        ],
                        'useKrajeePresets' => true,
                    ]) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>

    </div>

</div>

<?php

$generateUrl = \yii\helpers\Url::to(['/blog-posts/generate-url']);
$js = <<<JS
$(document).ready( function () {
    $('#upload-preview').on('change', function () {
        const [file] = this.files;
        let img = $('<img />');
        img.attr('src', URL.createObjectURL(file));
        img.css('max-width', '100%');
        $('#preview-image').html(img);
    });
    
    $(document).on('change', '#title', function () {
        let title = $(this).val();
        $.get('$generateUrl' + '?title=' + title, function (url) {
            $('#url-address').val(url);
        });
    });
});
JS;
$this->registerJs($js);

