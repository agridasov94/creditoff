<?php

use backend\widgets\UserSelect2Widget;
use common\src\entities\blog\categories\entity\BlogCategoriesQuery;
use common\src\entities\users\entity\Users;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \common\src\entities\blog\posts\entity\BlogPosts */
/* @var $form yii\widgets\ActiveForm */

$user = $model->bp_author_id ? Users::findOne($model->bp_author_id) : null;
?>

    <div class="blog-posts-form">

        <div class="row">
            <div class="col-md-12">
                <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

                <div class="row">
                    <div class="col-md-3">
                        <?= $form->errorSummary($model, ['class' => 'alert alert-danger', 'role' => 'alert']) ?>

                        <?= $form->field($model, 'bp_id')->hiddenInput()->label(false) ?>
                        <?= $form->field($model, 'bp_lang_id')->hiddenInput()->label(false) ?>
                        <?= $form->field($model, 'bp_alias')->hiddenInput()->label(false) ?>
                        <?= $form->field($model, 'bp_preview_image')->hiddenInput()->label(false) ?>

                        <?= $form->field($model, 'bp_category_id')->dropDownList(BlogCategoriesQuery::getListByLang(Yii::$app->language), ['prompt' => '---']) ?>

                        <?= $form->field($model, 'bp_title')->textInput(['maxlength' => true, 'id' => 'title']) ?>

                        <?= $form->field($model, 'bp_url')->textInput(['maxlength' => true, 'id' => 'url-address']) ?>

                        <?= $form->field($model, 'bp_short_description')->textarea(['maxlength' => true, 'rows' => 5]) ?>

                        <?= $form->field($model, 'bp_author_id')->widget(UserSelect2Widget::class, [
                            'data' => $user ? [
                                $user->id => $user->username
                            ] : [],
                        ]) ?>

                        <?= $form->field($model, 'bp_active')->checkbox() ?>

                        <div class="form-group">
                            <?= Html::submitButton(Yii::t('backend.blog_posts', 'Save'), ['class' => 'btn btn-success']) ?>
                        </div>

                    </div>
                    <div class="col-md-9">
                        <div id="preview-image">
                            <?= $model->bp_preview_image ? Html::img($model->getPublicPreviewPath(), [
                                'alt' => $model->bp_title,
                                'style' => 'max-width: 100%'
                            ]) : '' ?>
                        </div>
                        <?= $form->field($model, 'preview')->fileInput(['id' => 'upload-preview']) ?>

                        <?= $form->field($model, 'bp_content')->widget(\kartik\editors\Summernote::class, [
                            'options' => [
                                'rows' => 6,
                                'readonly' => false,
                            ],
                            'useKrajeePresets' => true,
                        ]) ?>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>

        </div>

    </div>

<?php
$generateUrl = \yii\helpers\Url::to(['/blog-posts/generate-url']);
$js = <<<JS
$(document).ready( function () {
    $('#upload-preview').on('change', function () {
        const [file] = this.files;
        let img = $('<img />');
        img.attr('src', URL.createObjectURL(file));
        img.css('max-width', '100%');
        $('#preview-image').html(img);
    });
    $(document).on('change', '#title', function () {
        let title = $(this).val();
        $.get('$generateUrl' + '?title=' + title, function (url) {
            $('#url-address').val(url);
        });
    });
});
JS;
$this->registerJs($js);

