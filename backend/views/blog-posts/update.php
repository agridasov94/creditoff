<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\src\entities\blog\posts\entity\BlogPosts */

$this->title = Yii::t('backend.blog_posts', 'Update Blog Posts: {name}', [
    'name' => $model->bp_title,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend.blog_posts', 'Blog Posts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->bp_id, 'url' => ['view', 'bp_id' => $model->bp_id, 'bp_lang_id' => $model->bp_lang_id]];
$this->params['breadcrumbs'][] = Yii::t('backend.blog_posts', 'Update');
?>
<div class="blog-posts-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form_update', [
        'model' => $model,
    ]) ?>

</div>
