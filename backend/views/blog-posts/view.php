<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\src\entities\blog\posts\entity\BlogPosts */

$this->title = $model->bp_title . ' - ' . ucfirst($model->bp_lang_id);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend.blog_posts', 'Blog Posts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="blog-posts-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= \common\widgets\FlashAlert::widget() ?>

    <p>
        <?= Html::a(Yii::t('backend.blog_posts', 'Update'), ['update', 'bp_id' => $model->bp_id, 'bp_lang_id' => $model->bp_lang_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('backend.blog_posts', 'Delete'), ['delete', 'bp_id' => $model->bp_id, 'bp_lang_id' => $model->bp_lang_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend.blog_posts', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="row">
        <div class="col-md-4">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'bp_id',
                    [
                        'attribute' => 'bp_lang_id',
                        'value' => static function (\common\src\entities\blog\posts\entity\BlogPosts $model) {
                            return ucfirst($model->language->language_id);
                        }
                    ],
                    [
                        'attribute' => 'bp_category_id',
                        'value' => static function (\common\src\entities\blog\posts\entity\BlogPosts $model) {
                            return $model->category->bc_name ?? '--';
                        }
                    ],
                    'bp_title',
                    'bp_url:ntext',
                    'bp_short_description',
//            'bp_content:ntext',
                    'bp_author_id:username',
//            'bp_alias',
                    [
                        'attribute' => 'bp_active',
                        'format' => 'booleanByLabel',
                    ],
                    'bp_created_user_id:username',
                    'bp_updated_user_id:username',
                    'bp_created_dt:byUserDateTime',
                    'bp_updated_dt:byUserDateTime',
                ],
            ]) ?>
        </div>
        <div class="col-md-8">
            <div>
                <?= Html::img($model->getPublicPreviewPath(), [
                    'alt' => $model->bp_title,
                    'style' => 'max-width: 100%'
                ]) ?>
            </div>
            <div style="margin-top: 20px;">
                <?= \kartik\editors\Summernote::widget([
                    'options' => [
                        'rows' => 6,
                        'readonly' => false,
                    ],
                    'useKrajeePresets' => true,
                    'value' => $model->bp_content,
                    'name' => 'content'
                ]) ?>
            </div>
        </div>
    </div>

</div>
