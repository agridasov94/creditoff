<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \backend\src\useCase\blogCreate\BlogPostCreateForm */

$this->title = Yii::t('backend.blog_posts', 'Create Blog Posts');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend.blog_posts', 'Blog Posts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blog-posts-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
