<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplication\clientEmail\entity\LoanApplicationClientEmail */

$this->title = Yii::t('loan_application', 'Update Loan Application Client Email: {name}', [
    'name' => $model->lace_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('loan_application', 'Loan Application Client Emails'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->lace_id, 'url' => ['view', 'id' => $model->lace_id]];
$this->params['breadcrumbs'][] = Yii::t('loan_application', 'Update');
?>
<div class="loan-application-client-email-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
