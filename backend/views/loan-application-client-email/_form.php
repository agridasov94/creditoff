<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplication\clientEmail\entity\LoanApplicationClientEmail */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loan-application-client-email-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'lace_client_id')->textInput() ?>

    <?= $form->field($model, 'lace_la_id')->textInput() ?>

    <?= $form->field($model, 'lace_email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lace_created_dt')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('loan_application', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
