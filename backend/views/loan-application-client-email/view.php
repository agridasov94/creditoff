<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplication\clientEmail\entity\LoanApplicationClientEmail */

$this->title = $model->lace_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('loan_application', 'Loan Application Client Emails'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="loan-application-client-email-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('loan_application', 'Update'), ['update', 'id' => $model->lace_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('loan_application', 'Delete'), ['delete', 'id' => $model->lace_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('loan_application', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'lace_id',
            'lace_client_id',
            'lace_la_id',
            'lace_email:email',
            'lace_created_dt',
        ],
    ]) ?>

</div>
