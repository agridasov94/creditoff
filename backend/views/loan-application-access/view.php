<?php

use common\src\entities\loanApplicationAccess\entity\LoanApplicationAccess;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplicationAccess\entity\LoanApplicationAccess */

$this->title = $model->laa_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('loan_application', 'Loan Application Accesses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="loan-application-access-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('loan_application', 'Update'), ['update', 'id' => $model->laa_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('loan_application', 'Delete'), ['delete', 'id' => $model->laa_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('loan_application', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'laa_id',
            'laa_la_id',
            'laa_user_id:username',
            [
                'attribute' => 'laa_status',
                'value' => static function (LoanApplicationAccess $model) {
                    return $model->getStatusLabelByName();
                },
                'format' => 'raw',
            ],
            'laa_reason:ntext',
            [
                'attribute' => 'laa_action_by',
                'value' => static function (LoanApplicationAccess $model) {
                    return $model->getActionByLabelName();
                },
                'format' => 'raw',
            ],
            'laa_created_dt:byUserDateTime',
            'laa_updated_dt:byUserDateTime',
            'laa_created_user_id:username',
            'laa_updated_user_id:username',
        ],
    ]) ?>

</div>
