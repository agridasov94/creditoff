<?php

use backend\components\grid\DateTimeColumn;
use backend\components\grid\UserSelect2Column;
use backend\src\entities\partnerCompany\entity\PartnerCompanyQuery;
use common\src\entities\loanApplicationAccess\entity\LoanApplicationAccess;
use common\src\entities\loanApplicationAccess\entity\LoanApplicationAccessStatus;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\src\entities\loanApplicationAccess\entity\LoanApplicationAccessSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('loan_application', 'Loan Application Accesses');
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    [
        'attribute' => 'laa_id',
        'options' => [
            'style' => 'width: 100px;'
        ]
    ],
    [
        'attribute' => 'laa_la_id',
        'options' => [
            'style' => 'width: 100px;'
        ]
    ],
    [
        'attribute' => 'loanApplicationNumber',
        'options' => [
            'style' => 'width: 100px;'
        ],
        'value' => static function (LoanApplicationAccess $model) {
            return $model->loanApplication->la_number;
        }
    ],
    [
        'attribute' => 'productId',
        'options' => [
            'style' => 'width: 150px;'
        ],
        'filter' => \common\src\entities\product\entity\ProductQuery::getList(),
        'value' => static function (LoanApplicationAccess $model) {
            return $model->loanApplication->product->p_name ?? null;
        },
    ],
    [
        'class' => UserSelect2Column::class,
        'attribute' => 'laa_user_id',
        'relation' => 'user',
        'placeholder' => '',
        'options' => [
            'style' => 'width: 200px;'
        ]
    ],
    [
        'attribute' => 'companyId',
        'value' => static function (LoanApplicationAccess $model) {
            return $model->user->partnerCompany->pc_name ?? null;
        },
        'filter' => PartnerCompanyQuery::getList(),
        'label' => Yii::t('company', 'Company')
    ],
    [
        'attribute' => 'laa_status',
        'value' => static function (LoanApplicationAccess $model) {
            return $model->getStatusLabelByName();
        },
        'format' => 'raw',
        'filter' => LoanApplicationAccessStatus::getList()
    ],
    'laa_reason:ntext',
    [
        'attribute' => 'laa_action_by',
        'value' => static function (LoanApplicationAccess $model) {
            return $model->getActionByLabelName();
        },
        'format' => 'raw',
        'filter' => LoanApplicationAccess::getActionByList()
    ],
    [
        'class' => DateTimeColumn::class,
        'attribute' => 'laa_created_dt',
        'format' => 'byUserDateTime'
    ],
    [
        'class' => DateTimeColumn::class,
        'attribute' => 'laa_updated_dt',
        'format' => 'byUserDateTime'
    ],
    [
        'class' => UserSelect2Column::class,
        'attribute' => 'laa_created_user_id',
        'relation' => 'updatedUser',
        'placeholder' => '',
        'options' => [
            'style' => 'width: 200px;'
        ]
    ],
    [
        'class' => UserSelect2Column::class,
        'attribute' => 'laa_updated_user_id',
        'relation' => 'updatedUser',
        'placeholder' => '',
        'options' => [
            'style' => 'width: 200px;'
        ]
    ],
    ['class' => 'yii\grid\ActionColumn'],
];
?>
<div class="loan-application-access-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>

    <?php if(Yii::$app->request->isPjax): ?>
        <script>
            $('.export-wrapper .dropdown-menu a').each(function (i, e) {
                $(document).off('click.exportmenu', '#' + $(e).attr('id'));
            });
        </script>
    <?php endif; ?>
    <?= \kartik\export\ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'dropdownOptions' => [
            'label' => 'Export All',
            'class' => 'btn btn-outline-secondary btn-default'
        ],
        'container' => [
            'class' => 'export-wrapper'
        ],
        'showConfirmAlert' => false,
        'filename' => Yii::t('export', 'Loan_application_access') . '_' . date('Y-m-d_H:i:s')
    ]) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns
    ]); ?>

    <?php Pjax::end(); ?>

</div>
