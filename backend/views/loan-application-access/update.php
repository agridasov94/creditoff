<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplicationAccess\entity\LoanApplicationAccess */

$this->title = Yii::t('loan_application', 'Update Loan Application Access: {name}', [
    'name' => $model->laa_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('loan_application', 'Loan Application Accesses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->laa_id, 'url' => ['view', 'id' => $model->laa_id]];
$this->params['breadcrumbs'][] = Yii::t('loan_application', 'Update');
?>
<div class="loan-application-access-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
