<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplicationAccess\entity\LoanApplicationAccessSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loan-application-access-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'laa_id') ?>

    <?= $form->field($model, 'laa_la_id') ?>

    <?= $form->field($model, 'laa_user_id') ?>

    <?= $form->field($model, 'laa_status') ?>

    <?= $form->field($model, 'laa_action_by') ?>

    <?php // echo $form->field($model, 'laa_created_dt') ?>

    <?php // echo $form->field($model, 'laa_updated_dt') ?>

    <?php // echo $form->field($model, 'laa_created_user_id') ?>

    <?php // echo $form->field($model, 'laa_updated_user_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('loan_application', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('loan_application', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
