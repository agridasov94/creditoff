<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplicationAccess\entity\LoanApplicationAccess */

$this->title = Yii::t('loan_application', 'Create Loan Application Access');
$this->params['breadcrumbs'][] = ['label' => Yii::t('loan_application', 'Loan Application Accesses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="loan-application-access-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
