<?php

use backend\widgets\UserSelect2Widget;
use common\src\entities\users\entity\UsersQuery;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplicationAccess\entity\LoanApplicationAccess */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loan-application-access-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'laa_la_id')->textInput() ?>

            <?= $form->field($model, 'laa_user_id')->widget(UserSelect2Widget::class, [
                'data' => \common\src\entities\users\entity\UsersQuery::getList()
            ]) ?>

            <?= $form->field($model, 'laa_status')->textInput() ?>

            <?= $form->field($model, 'laa_action_by')->widget(UserSelect2Widget::class, [
                'data' => UsersQuery::getList()
            ]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('loan_application', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
