<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\src\entities\clientNotification\entity\ClientNotification */

$this->title = Yii::t('client_notification', 'Update Client Notification: {name}', [
    'name' => $model->cn_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('client_notification', 'Client Notifications'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cn_id, 'url' => ['view', 'id' => $model->cn_id]];
$this->params['breadcrumbs'][] = Yii::t('client_notification', 'Update');
?>
<div class="client-notification-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
