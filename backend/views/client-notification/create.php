<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\src\entities\clientNotification\entity\ClientNotification */

$this->title = Yii::t('client_notification', 'Create Client Notification');
$this->params['breadcrumbs'][] = ['label' => Yii::t('client_notification', 'Client Notifications'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-notification-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
