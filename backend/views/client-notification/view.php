<?php

use common\src\entities\clientNotification\entity\ClientNotification;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\src\entities\clientNotification\entity\ClientNotification */

$this->title = $model->cn_title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('client_notification', 'Client Notifications'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="client-notification-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('client_notification', 'Update'), ['update', 'id' => $model->cn_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('client_notification', 'Delete'), ['delete', 'id' => $model->cn_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('client_notification', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'cn_id',
            'cn_unique_id',
            [
                'attribute' => 'cn_client_id',
                'value' => static function (ClientNotification $model) {
                    return Html::a('('.$model->cn_client_id.') ' . $model->client->getFullName(), ['/client/view', 'id' => $model->cn_client_id], [
                        'target' => '_blank',
                        'data-pjax' => 0
                    ]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'cn_type_id',
                'value' => static function (ClientNotification $model) {
                    return '<span class="label label-default">' . ClientNotification::getType($model->cn_type_id) . '</span>';
                },
                'format' => 'raw',
            ],
            'cn_title',
            'cn_message:ntext',
            'cn_new:booleanByLabel',
            'cn_deleted:booleanByLabel',
            'cn_popup',
            'cn_popup_show',
            'cn_read_dt:byUserDateTime',
            'cn_created_dt:byUserDateTime',
            'cn_created_by:username'
        ],
    ]) ?>

</div>
