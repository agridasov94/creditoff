<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\src\entities\clientNotification\entity\ClientNotificationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="client-notification-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'cn_id') ?>

    <?= $form->field($model, 'cn_unique_id') ?>

    <?= $form->field($model, 'cn_client_id') ?>

    <?= $form->field($model, 'cn_type_id') ?>

    <?= $form->field($model, 'cn_title') ?>

    <?php // echo $form->field($model, 'cn_message') ?>

    <?php // echo $form->field($model, 'cn_new') ?>

    <?php // echo $form->field($model, 'cn_deleted') ?>

    <?php // echo $form->field($model, 'cn_popup') ?>

    <?php // echo $form->field($model, 'cn_popup_show') ?>

    <?php // echo $form->field($model, 'cn_read_dt') ?>

    <?php // echo $form->field($model, 'cn_created_dt') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('client_notification', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('client_notification', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
