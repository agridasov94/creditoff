<?php

use common\src\entities\clientNotification\entity\ClientNotification;
use common\src\entities\users\entity\UsersQuery;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\src\entities\clientNotification\entity\ClientNotification */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'cn_client_id')->widget(\backend\widgets\ClientSelect2Widget::class, [
                    'useAjax' => true
            ]) ?>

            <?= $form->field($model, 'cn_type_id')->dropDownList(ClientNotification::getTypeList(), ['prompt' => '---']) ?>

            <?= $form->field($model, 'cn_new')->checkbox() ?>

            <?= $form->field($model, 'cn_deleted')->checkbox() ?>

            <?= $form->field($model, 'cn_created_by')->widget(\backend\widgets\UserSelect2Widget::class,[
                'data' => UsersQuery::getList()
            ]) ?>
        </div>


        <div class="col-md-6">
            <?= $form->field($model, 'cn_title')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'cn_message')->textarea(['rows' => 6]) ?>
        </div>

        <div class="col-md-12">
            <?= Html::submitButton(Yii::t('client_notification', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
