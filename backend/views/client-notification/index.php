<?php

use backend\components\grid\DateTimeColumn;
use backend\components\grid\UserSelect2Column;
use common\src\entities\clientNotification\entity\ClientNotification;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\src\entities\clientNotification\entity\ClientNotificationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('client_notification', 'Client Notifications');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-notification-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('client_notification', 'Create Client Notification'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'cn_id',
                'options' => [
                    'width' => '100px'
                ]
            ],
            'cn_unique_id',
            [
                'attribute' => 'cn_client_id',
                'value' => static function (ClientNotification $model) {
                    return Html::a('('.$model->cn_client_id.') ' . $model->client->getFullName(), ['/client/view', 'id' => $model->cn_client_id], [
                        'target' => '_blank',
                        'data-pjax' => 0
                    ]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'cn_type_id',
                'value' => static function (ClientNotification $model) {
                    return '<span class="label label-default">' . ClientNotification::getType($model->cn_type_id) . '</span>';
                },
                'format' => 'raw',
                'filter' => ClientNotification::getTypeList()
            ],
            'cn_title',
            'cn_new:booleanByLabel',
            'cn_deleted:booleanByLabel',
            [
                'class' => DateTimeColumn::class,
                'attribute' => 'cn_read_dt'
            ],
            [
                'class' => DateTimeColumn::class,
                'attribute' => 'cn_created_dt'
            ],
            [
                'class' => UserSelect2Column::class,
                'attribute' => 'cn_created_by',
                'relation' => 'user',
                'placeholder' => '',
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {delete}'
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
