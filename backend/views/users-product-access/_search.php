<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\src\entities\usersProductAccess\entity\UsersProductAccessSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-product-access-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'upa_user_id') ?>

    <?= $form->field($model, 'upa_product_id') ?>

    <?= $form->field($model, 'upa_created_dt') ?>

    <?= $form->field($model, 'upa_updated_dt') ?>

    <?= $form->field($model, 'upa_created_user_id') ?>

    <?php // echo $form->field($model, 'upa_updated_user_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('users', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('users', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
