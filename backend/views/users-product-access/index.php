<?php

use backend\components\grid\DateTimeColumn;
use backend\components\grid\UserSelect2Column;
use backend\src\entities\partnerCompany\entity\PartnerCompanyQuery;
use backend\src\entities\usersProductAccess\entity\UsersProductAccess;
use common\src\entities\product\entity\ProductQuery;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\src\entities\usersProductAccess\entity\UsersProductAccessSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('users', 'Users Product Accesses');
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    [
        'class' => UserSelect2Column::class,
        'attribute' => 'upa_user_id',
        'relation' => 'user',
        'placeholder' => '',
    ],
    [
        'attribute' => 'companyId',
        'value' => static function (UsersProductAccess $model) {
            return $model->user->partnerCompany->pc_name ?? null;
        },
        'filter' => PartnerCompanyQuery::getList(),
        'label' => Yii::t('company', 'Company')
    ],
    [
        'attribute' => 'upa_product_id',
        'value' => static function (UsersProductAccess $model) {
            return $model->product->p_name;
        },
        'filter' => ProductQuery::getList()
    ],
    'upa_request_price',
    [
        'attribute' => 'upa_status',
        'value' => static function (UsersProductAccess $model) {
            return $model->getStatusLabel();
        },
        'format' => 'raw',
        'filter' => UsersProductAccess::getStatusListName()
    ],
    [
        'class' => DateTimeColumn::class,
        'attribute' => 'upa_created_dt',
        'format' => 'byUserDateTime'
    ],
    [
        'class' => DateTimeColumn::class,
        'attribute' => 'upa_updated_dt',
        'format' => 'byUserDateTime'
    ],
    [
        'class' => UserSelect2Column::class,
        'attribute' => 'upa_created_user_id',
        'relation' => 'createdUser',
        'placeholder' => '',
    ],
    [
        'class' => UserSelect2Column::class,
        'attribute' => 'upa_updated_user_id',
        'relation' => 'updatedUser',
        'placeholder' => '',
    ],

    ['class' => 'yii\grid\ActionColumn'],
];
?>
<div class="users-product-access-index">

    <h4><?= Html::encode($this->title) ?></h4>

    <p>
        <?= Html::a(Yii::t('users', 'Create Users Product Access'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>

    <?php if(Yii::$app->request->isPjax): ?>
        <script>
            $('.export-wrapper .dropdown-menu a').each(function (i, e) {
                $(document).off('click.exportmenu', '#' + $(e).attr('id'));
            });
        </script>
    <?php endif; ?>
    <?= \kartik\export\ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'dropdownOptions' => [
            'label' => 'Export All',
            'class' => 'btn btn-outline-secondary btn-default'
        ],
        'container' => [
            'class' => 'export-wrapper'
        ],
        'showConfirmAlert' => false,
        'filename' => Yii::t('export', 'Users_product_access') . '_' . date('Y-m-d_H:i:s')
    ]) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns
    ]); ?>

    <?php Pjax::end(); ?>

</div>
