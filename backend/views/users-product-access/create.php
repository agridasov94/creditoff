<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\src\entities\usersProductAccess\entity\UsersProductAccess */

$this->title = Yii::t('users', 'Create Users Product Access');
$this->params['breadcrumbs'][] = ['label' => Yii::t('users', 'Users Product Accesses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-product-access-create">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
