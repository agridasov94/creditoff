<?php

use backend\src\entities\usersProductAccess\entity\UsersProductAccess;
use backend\widgets\UserSelect2Widget;
use common\src\entities\product\entity\ProductQuery;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\src\entities\usersProductAccess\entity\UsersProductAccess */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-product-access-form">

    <div class="row">
        <div class="col-md-2">
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'upa_user_id')->widget(UserSelect2Widget::class) ?>

            <?= $form->field($model, 'upa_product_id')->dropDownList(ProductQuery::getList(), [
                'prompt' => '---'
            ]) ?>

            <?= $form->field($model, 'upa_request_price')->input('number') ?>

            <?= $form->field($model, 'upa_status')->dropDownList(UsersProductAccess::getStatusListName(), ['prompt' => '---']) ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('users', 'Save'), ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>
