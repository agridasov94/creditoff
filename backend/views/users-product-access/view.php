<?php

use backend\src\entities\usersProductAccess\entity\UsersProductAccess;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\src\entities\usersProductAccess\entity\UsersProductAccess */

$this->title = $model->upa_user_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('users', 'Users Product Accesses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="users-product-access-view">

    <h4><?= Html::encode($this->title) ?></h4>

    <p>
        <?= Html::a(Yii::t('users', 'Update'), ['update', 'upa_user_id' => $model->upa_user_id, 'upa_product_id' => $model->upa_product_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('users', 'Delete'), ['delete', 'upa_user_id' => $model->upa_user_id, 'upa_product_id' => $model->upa_product_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('users', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'upa_user_id:username',
            [
                'attribute' => 'upa_product_id',
                'value' => static function (UsersProductAccess $model) {
                    return $model->product->p_name;
                },
            ],
            'upa_request_price',
            [
                'attribute' => 'upa_status',
                'value' => static function (UsersProductAccess $model) {
                    return $model->getStatusLabel();
                },
                'format' => 'raw'
            ],
            'upa_created_dt:byUserDateTime',
            'upa_updated_dt:byUserDateTime',
            'upa_created_user_id:username',
            'upa_updated_user_id:username',
        ],
    ]) ?>

</div>
