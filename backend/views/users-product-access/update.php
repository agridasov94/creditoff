<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\src\entities\usersProductAccess\entity\UsersProductAccess */

$this->title = Yii::t('users', 'Update Users Product Access: {name}', [
    'name' => $model->upa_user_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('users', 'Users Product Accesses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->upa_user_id, 'url' => ['view', 'upa_user_id' => $model->upa_user_id, 'upa_product_id' => $model->upa_product_id]];
$this->params['breadcrumbs'][] = Yii::t('users', 'Update');
?>
<div class="users-product-access-update">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
