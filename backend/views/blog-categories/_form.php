<?php

use common\src\entities\language\LanguageQuery;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\src\entities\blog\categories\entity\BlogCategories */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="blog-categories-form">
    <div class="row">
        <div class="col-md-4">
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->errorSummary($model, ['class' => 'alert alert-danger', 'role' => 'alert']) ?>

            <?php if ($model->bc_id): ?>
                <?= $form->field($model, 'bc_lang_id')->dropDownList(LanguageQuery::getList(), ['prompt' => '---']) ?>
            <?php endif; ?>

            <?= $form->field($model, 'bc_name')->textInput(['maxlength' => true, 'id' => 'name']) ?>

            <?= $form->field($model, 'bc_url')->textInput(['maxlength' => true, 'id' => 'url-address']) ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<?php
$generateUrl = \yii\helpers\Url::to(['/blog-categories/generate-url']);
$js = <<<JS
$(document).on('change', '#name', function () {
    let name = $(this).val();
    $.get('$generateUrl' + '?name=' + name, function (url) {
        $('#url-address').val(url);
    });
});
JS;
$this->registerJs($js);
