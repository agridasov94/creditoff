<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\src\entities\blog\categories\entity\BlogCategories */

$this->title = Yii::t('backend', 'Create Blog Categories');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Blog Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blog-categories-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
