<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\src\entities\blog\categories\entity\BlogCategoriesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="blog-categories-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'bc_id') ?>

    <?= $form->field($model, 'bc_lang_id') ?>

    <?= $form->field($model, 'bc_name') ?>

    <?= $form->field($model, 'bc_created_dt') ?>

    <?= $form->field($model, 'bc_updated_dt') ?>

    <?php // echo $form->field($model, 'bc_created_user_id') ?>

    <?php // echo $form->field($model, 'bc_updated_user_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('backend', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
