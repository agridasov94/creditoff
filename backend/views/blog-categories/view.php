<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\src\entities\blog\categories\entity\BlogCategories */

$this->title = $model->bc_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Blog Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="blog-categories-view">

    <?= \yii\bootstrap\Alert::widget() ?>

    <h3><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a(Yii::t('backend', 'Update'), ['update', 'bc_id' => $model->bc_id, 'bc_lang_id' => $model->bc_lang_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('backend', 'Delete'), ['delete', 'bc_id' => $model->bc_id, 'bc_lang_id' => $model->bc_lang_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="row">
        <div class="col-md-6">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'bc_id',
                    'bc_lang_id',
                    'bc_name',
                    'bc_url:ntext',
                    'bc_created_dt',
                    'bc_updated_dt',
                    'bc_created_user_id',
                    'bc_updated_user_id',
                ],
            ]) ?>
        </div>
    </div>
</div>
