<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\src\entities\blog\categories\entity\BlogCategories */

$this->title = Yii::t('backend', 'Update Blog Categories: {name}', [
    'name' => $model->bc_name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Blog Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->bc_id, 'url' => ['view', 'bc_id' => $model->bc_id, 'bc_lang_id' => $model->bc_lang_id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="blog-categories-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
