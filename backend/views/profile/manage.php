<?php
/**
 * @var $this \yii\web\View
 * @var $user Users
 * @var $manageForm \backend\src\useCase\users\manage\UserManageForm
 */

use common\src\entities\users\entity\Users;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

$this->title = 'My profile - ' . $user->getFullName();

$this->params['breadcrumbs'][] = ['label' => 'Dashboard', 'url' => ['/']];
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?=\yii\helpers\Html::encode($this->title)?></h1>

<?= \common\widgets\FlashAlert::widget() ?>

<div class="row">
    <div class="col-sm-6">
        <?php Pjax::begin(['enablePushState' => false, 'enableReplaceState' => false, 'timeout' => 2000]) ?>
        <?php $form = ActiveForm::begin(['enableClientValidation' => false]) ?>
        <?= $form->field($manageForm, 'id')->hiddenInput()->label(false) ?>
        <div class="well">
            <?php if ($manageForm->hasErrors()): ?>
                <div class="row">
                    <div class="col-md-12">
                        <?= $form->errorSummary($manageForm, ['class' => 'alert alert-danger', 'role' => 'alert']) ?>
                    </div>
                </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-sm-6">

                    <?= $form->field($manageForm, 'email')->input('email') ?>

                    <?= $form->field($manageForm, 'phone')->input('number') ?>
                </div>
                <div class="col-sm-6">
                    <?= $form->field($manageForm, 'password', [
                        'options' => [
                            'class' => 'form-group'
                        ]
                    ])->passwordInput(['autocomplete' => 'new-password'])?>

                    <?= $form->field($manageForm, 'rePassword', [
                        'options' => [
                            'class' => 'form-group'
                        ]
                    ])->passwordInput(['autocomplete' => 'new-password'])?>
                </div>
            </div>
        </div>

        <div class="form-group">
            <?= \yii\helpers\Html::submitButton('Save Profile', ['class' => 'btn btn-primary']) ?>
        </div>
        <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
    <div class="col-sm-6">
        <?= \yii\widgets\DetailView::widget([
            'model' => $user,
            'attributes' => [
                'username',
                [
                    'value' => static function (Users $model) {
                        return $model->getFullName();
                    },
                    'label' => Yii::t('users', 'Name')
                ],
                [
                    'attribute' => 'status',
                    'value' => static function (Users $model) {
                        return $model->getStatusNameLabel();
                    },
                    'format' => 'raw'
                ],
                'is_online:booleanByLabel',
                [
                    'attribute' => 'partner_company_id',
                    'value' => static function (Users $model) {
                        return $model->partner_company_id ? $model->partnerCompany->pc_name : null;
                    },
                ],
                [
                    'attribute' => 'partner_company_position_id',
                    'value' => static function (Users $model) {
                        return $model->partner_company_position_id ? $model->partnerCompanyPosition->pcp_name : null;
                    },
                ],
                'last_login_dt:byUserDateTime'
            ]
        ]) ?>
    </div>
</div>
