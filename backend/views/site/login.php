<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\src\forms\login\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;

$this->title = 'Login';
?>

<div class="animate form login_form">
    <section class="login_content">
        <?php Pjax::begin(['enablePushState' => false, 'enableReplaceState' => false]) ?>
        <?php $form = ActiveForm::begin(['id' => 'login-form', 'options' => ['data-pjax' => 1]]); ?>
            <h1><?= Html::encode($this->title) ?></h1>

            <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'placeholder' => 'Username', 'required' => ''])->label(false) ?>

            <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Password', 'required' => ''])->label(false) ?>

            <div class="form-group">
                <?= Html::submitButton('Login', ['class' => 'btn btn-default', 'name' => 'login-button', 'data-pjax' => 1]) ?>
            </div>

            <div class="clearfix"></div>

            <div class="separator">
                <div class="clearfix"></div>
                <br />

                <div>
                    <p>©<?= date('Y') ?> All Rights Reserved. Privacy and Terms</p>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
        <?php Pjax::end(); ?>
    </section>
</div>