<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplication\clientPassportData\entity\LoanApplicationClientPassportDataSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loan-application-client-passport-data-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'lacpd_id') ?>

    <?= $form->field($model, 'lacpd_client_id') ?>

    <?= $form->field($model, 'lacpd_la_id') ?>

    <?= $form->field($model, 'lacpd_type') ?>

    <?= $form->field($model, 'lacpd_series') ?>

    <?php // echo $form->field($model, 'lacpd_number') ?>

    <?php // echo $form->field($model, 'lacpd_issued_by') ?>

    <?php // echo $form->field($model, 'lacpd_issue_date') ?>

    <?php // echo $form->field($model, 'lacpd_issue_department_code') ?>

    <?php // echo $form->field($model, 'lacpd_expiration_date') ?>

    <?php // echo $form->field($model, 'lacpd_registered_address') ?>

    <?php // echo $form->field($model, 'lacpd_residence_address') ?>

    <?php // echo $form->field($model, 'lacpd_created_dt') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('loan_application', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('loan_application', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
