<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplication\clientPassportData\entity\LoanApplicationClientPassportData */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loan-application-client-passport-data-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'lacpd_client_id')->textInput() ?>

    <?= $form->field($model, 'lacpd_la_id')->textInput() ?>

    <?= $form->field($model, 'lacpd_type')->textInput() ?>

    <?= $form->field($model, 'lacpd_series')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lacpd_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lacpd_issued_by')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lacpd_issue_date')->textInput() ?>

    <?= $form->field($model, 'lacpd_issue_department_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lacpd_expiration_date')->textInput() ?>

    <?= $form->field($model, 'lacpd_registered_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lacpd_residence_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lacpd_created_dt')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('loan_application', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
