<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplication\clientPassportData\entity\LoanApplicationClientPassportData */

$this->title = $model->lacpd_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('loan_application', 'Loan Application Client Passport Datas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="loan-application-client-passport-data-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('loan_application', 'Update'), ['update', 'id' => $model->lacpd_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('loan_application', 'Delete'), ['delete', 'id' => $model->lacpd_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('loan_application', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'lacpd_id',
            'lacpd_client_id',
            'lacpd_la_id',
            'lacpd_type',
            'lacpd_series',
            'lacpd_number',
            'lacpd_issued_by',
            'lacpd_issue_date',
            'lacpd_issue_department_code',
            'lacpd_expiration_date',
            'lacpd_registered_address',
            'lacpd_residence_address',
            'lacpd_created_dt',
        ],
    ]) ?>

</div>
