<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplication\clientPassportData\entity\LoanApplicationClientPassportData */

$this->title = Yii::t('loan_application', 'Create Loan Application Client Passport Data');
$this->params['breadcrumbs'][] = ['label' => Yii::t('loan_application', 'Loan Application Client Passport Datas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="loan-application-client-passport-data-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
