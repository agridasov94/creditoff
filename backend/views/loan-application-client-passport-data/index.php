<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\src\entities\loanApplication\clientPassportData\entity\LoanApplicationClientPassportDataSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('loan_application', 'Loan Application Client Passport Datas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="loan-application-client-passport-data-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('loan_application', 'Create Loan Application Client Passport Data'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'lacpd_id',
            'lacpd_client_id',
            'lacpd_la_id',
            'lacpd_type',
            'lacpd_series',
            //'lacpd_number',
            //'lacpd_issued_by',
            //'lacpd_issue_date',
            //'lacpd_issue_department_code',
            //'lacpd_expiration_date',
            //'lacpd_registered_address',
            //'lacpd_residence_address',
            //'lacpd_created_dt',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
