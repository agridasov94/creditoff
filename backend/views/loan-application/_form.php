<?php

use backend\widgets\UserSelect2Widget;
use borales\extensions\phoneInput\PhoneInput;
use common\src\entities\client\entity\Client;
use common\src\entities\loanApplication\entity\LoanApplicationStatus;
use common\src\entities\product\entity\ProductQuery;
use kartik\date\DatePicker;
use unclead\multipleinput\MultipleInput;
use unclead\multipleinput\MultipleInputColumn;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplication\entity\LoanApplication */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>
<div class="loan-application-form">
<div class="row">
    <div class="col-md-2">

            <?= $form->field($model, 'la_number')->textInput() ?>

            <?= $form->field($model, 'la_product_id')->dropDownList(ProductQuery::getList()) ?>

            <?= $form->field($model, 'la_owner_id')->widget(UserSelect2Widget::class, [
                'data' => \common\src\entities\users\entity\UsersQuery::getList()
            ]) ?>

            <?= $form->field($model, 'la_status')->dropDownList(LoanApplicationStatus::getStatusNameList()) ?>

            <?= $form->field($model, 'la_loan_amount')->textInput() ?>

            <?= $form->field($model, 'la_loan_term')->textInput() ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('loan_application', 'Save'), ['class' => 'btn btn-success']) ?>
            </div>
    </div>
    <div class="col-md-2">

            <?= $form->field($model, 'la_client_id')->textInput() ?>

            <?= $form->field($model, 'la_client_name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'la_client_birth_date')->widget(DatePicker::class) ?>

            <?= $form->field($model, 'la_client_idnp')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'la_client_gender')->dropDownList(Client::getGenderListName(), ['prompt' => '--']) ?>

            <?= $form->field($model, 'la_client_civil_status')->dropDownList(Client::getCivilStatusListName(), ['prompt' => '--']) ?>

            <?= $form->field($model, 'la_client_social_status')->dropDownList(Client::getSocialStatusListName(), ['prompt' => '--']) ?>

            <?= $form->field($model, 'la_client_own_property')->radioList(['0' => Yii::t('frontend', 'No'), '1' => Yii::t('frontend', 'Yes')]) ?>

            <?= $form->field($model, 'la_manual_distribution')->checkbox() ?>

            <?= $form->field($model, 'la_distribution_iteration')->input('number') ?>

            <div class="form-group">
                <label for=""><?= Yii::t('client', 'Child Date Of Birth') ?></label>
                <?= MultipleInput::widget([
                    'name' => $model->formName(),
                    'columns' => [
                        [
                            'type' => DatePicker::class,
                            'name' => 'la_children',
                            'options' => [
                                'pluginOptions' => [
                                    'format' => 'dd-mm-yyyy'
                                ],
                                'options' => [
                                    'style' => 'margin-bottom: 10px;'
                                ]
                            ],
                        ],
                        [
                            'type' => MultipleInputColumn::TYPE_HIDDEN_INPUT,
                            'name' => 'confirm'
                        ]
                    ],
                    'value' => $model->la_client_children,
                    'iconSource' => MultipleInput::ICONS_SOURCE_FONTAWESOME,
                    'addButtonOptions' => [
                        'class' => 'btn btn-success'
                    ],
                ]) ?>
            </div>

        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
