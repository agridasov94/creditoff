<?php

use common\src\entities\loanApplication\entity\LoanApplicationStatus;
use common\src\entities\product\entity\ProductQuery;
use common\src\entities\users\entity\UsersQuery;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplication\entity\LoanApplicationMultipleSearch */
/* @var $form yii\widgets\ActiveForm */

$usersList = UsersQuery::getList();
?>

<div class="x_panel">
    <div class="x_title">
        <h2><i class="fa fa-search"></i> <?= Yii::t('loan_application', 'Filter Form') ?></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="tab-content">
            <div class="tab-pane fade active show" id="client" role="tabpanel" aria-labelledby="client-tab">
                <div class="row">

                    <div class="col-md-12">
                        <?php $form = ActiveForm::begin([
                            'action' => ['index'],
                            'method' => 'get',
                            'options' => [
                                'data-pjax' => 1
                            ],
                        ]); ?>

                        <div class="row">
                            <div class="col-md-3">
                                <?= $form->field($model, 'la_id')->widget(Select2::class, [
                                    'options' => ['placeholder' => Yii::t('backend', 'Enter id'), 'multiple' => true],
                                    'pluginOptions' => [
                                        'tags' => true,
                                        'tokenSeparators' => [',', ' '],
                                        'maximumInputLength' => 10
                                    ],
                                ]) ?>

                                <?= $form->field($model, 'la_number')->widget(Select2::class, [
                                    'options' => ['placeholder' => Yii::t('backend', 'Enter application number'), 'multiple' => true],
                                    'pluginOptions' => [
                                        'tags' => true,
                                        'tokenSeparators' => [',', ' '],
                                        'maximumInputLength' => 10
                                    ],
                                ]) ?>

                                <?= $form->field($model, 'la_product_id')->widget(Select2::class, [
                                    'options' => ['placeholder' => Yii::t('backend', 'Select Product'), 'multiple' => true],
                                    'pluginOptions' => [
                                        'tags' => true,
                                        'tokenSeparators' => [',', ' '],
                                        'maximumInputLength' => 10
                                    ],
                                    'data' => ProductQuery::getList()
                                ]) ?>

                                <?= $form->field($model, 'la_owner_id')->widget(Select2::class, [
                                    'options' => ['placeholder' => Yii::t('backend', 'Enter Owner'), 'multiple' => true],
                                    'pluginOptions' => [
                                        'tags' => true,
                                        'tokenSeparators' => [',', ' '],
                                        'maximumInputLength' => 10
                                    ],
                                    'data' => $usersList
                                ]) ?>

                                <?= $form->field($model, 'la_status')->widget(Select2::class, [
                                    'options' => ['placeholder' => Yii::t('backend', 'Enter Status'), 'multiple' => true],
                                    'pluginOptions' => [
                                        'tags' => true,
                                        'tokenSeparators' => [',', ' '],
                                        'maximumInputLength' => 10
                                    ],
                                    'data' => LoanApplicationStatus::getStatusNameList()
                                ]) ?>
                            </div>

                            <div class="col-md-3">
                                <?= $form->field($model, 'la_manual_distribution')->dropDownList([
                                        0 => Yii::t('backend', 'No'),
                                    1 => Yii::t('backend', 'Yes')
                                ], ['prompt' => '--']) ?>

                                <?= $form->field($model, 'la_loan_amount')->widget(Select2::class, [
                                    'options' => ['placeholder' => Yii::t('backend', 'Loan Amount'), 'multiple' => true],
                                    'pluginOptions' => [
                                        'tags' => true,
                                        'tokenSeparators' => [',', ' '],
                                        'maximumInputLength' => 10
                                    ],
                                ]) ?>

                                <?= $form->field($model, 'la_loan_term')->widget(Select2::class, [
                                    'options' => ['placeholder' => Yii::t('backend', 'Loan Term'), 'multiple' => true],
                                    'pluginOptions' => [
                                        'tags' => true,
                                        'tokenSeparators' => [',', ' '],
                                        'maximumInputLength' => 10
                                    ],
                                ]) ?>

                                <?= $form->field($model, 'la_client_id')->widget(Select2::class, [
                                    'options' => ['placeholder' => Yii::t('backend', 'Client Id'), 'multiple' => true],
                                    'pluginOptions' => [
                                        'tags' => true,
                                        'tokenSeparators' => [',', ' '],
                                        'maximumInputLength' => 10
                                    ],
                                ]) ?>

                                <?= $form->field($model, 'la_client_name')->widget(Select2::class, [
                                    'options' => ['placeholder' => Yii::t('backend', 'Enter client name'), 'multiple' => true],
                                    'pluginOptions' => [
                                        'tags' => true,
                                        'tokenSeparators' => [',', ' '],
                                        'maximumInputLength' => 10
                                    ],
                                ]) ?>

                            </div>
                            <div class="col-md-3">
                                <?= $form->field($model, 'la_client_idnp')->widget(Select2::class, [
                                    'options' => ['placeholder' => Yii::t('backend', 'Enter client IDNP'), 'multiple' => true],
                                    'pluginOptions' => [
                                        'tags' => true,
                                        'tokenSeparators' => [',', ' '],
                                        'maximumInputLength' => 10
                                    ],
                                ]) ?>

                                <?= $form->field($model, 'la_client_birth_date')->widget(\dosamigos\datepicker\DatePicker::class, [
                                    'clientOptions' => [
                                        'autoclose' => true,
                                        'format' => 'yyyy-mm-dd',
                                        'clearBtn' => true,
                                        'startDate' => date('Y-m-d', strtotime('2018-01-01')),
                                        'endDate' => date('Y-m-d'),
                                    ],
                                    'options' => [
                                        'autocomplete' => 'off',
                                        'placeholder' => 'Choose Date',
                                        'readonly' => 'readonly'
                                    ],
                                    'containerOptions' => [
                                        'class' => (array_key_exists('la_client_birth_date', $model->errors)) ? 'has-error' : null,
                                    ],
                                    'clientEvents' => [
                                        'clearDate' => 'function (e) {$(e.target).find("input").change();}',
                                    ],
                                ]) ?>

                                <?= $form->field($model, 'la_created_dt')->widget(\dosamigos\datepicker\DatePicker::class, [
                                    'clientOptions' => [
                                        'autoclose' => true,
                                        'format' => 'yyyy-mm-dd',
                                        'clearBtn' => true,
                                        'startDate' => date('Y-m-d', strtotime('2018-01-01')),
                                        'endDate' => date('Y-m-d'),
                                    ],
                                    'options' => [
                                        'autocomplete' => 'off',
                                        'placeholder' => 'Choose Date',
                                        'readonly' => 'readonly'
                                    ],
                                    'containerOptions' => [
                                        'class' => (array_key_exists('la_created_dt', $model->errors)) ? 'has-error' : null,
                                    ],
                                    'clientEvents' => [
                                        'clearDate' => 'function (e) {$(e.target).find("input").change();}',
                                    ],
                                ]) ?>

                                <?= $form->field($model, 'la_updated_dt')->widget(\dosamigos\datepicker\DatePicker::class, [
                                    'clientOptions' => [
                                        'autoclose' => true,
                                        'format' => 'yyyy-mm-dd',
                                        'clearBtn' => true,
                                        'startDate' => date('Y-m-d', strtotime('2018-01-01')),
                                        'endDate' => date('Y-m-d'),
                                    ],
                                    'options' => [
                                        'autocomplete' => 'off',
                                        'placeholder' => 'Choose Date',
                                        'readonly' => 'readonly'
                                    ],
                                    'containerOptions' => [
                                        'class' => (array_key_exists('la_updated_dt', $model->errors)) ? 'has-error' : null,
                                    ],
                                    'clientEvents' => [
                                        'clearDate' => 'function (e) {$(e.target).find("input").change();}',
                                    ],
                                ]) ?>

                                <?= $form->field($model, 'la_updated_status_dt')->widget(\dosamigos\datepicker\DatePicker::class, [
                                    'clientOptions' => [
                                        'autoclose' => true,
                                        'format' => 'yyyy-mm-dd',
                                        'clearBtn' => true,
                                        'startDate' => date('Y-m-d', strtotime('2018-01-01')),
                                        'endDate' => date('Y-m-d'),
                                    ],
                                    'options' => [
                                        'autocomplete' => 'off',
                                        'placeholder' => 'Choose Date',
                                        'readonly' => 'readonly'
                                    ],
                                    'containerOptions' => [
                                        'class' => (array_key_exists('la_updated_dt', $model->errors)) ? 'has-error' : null,
                                    ],
                                    'clientEvents' => [
                                        'clearDate' => 'function (e) {$(e.target).find("input").change();}',
                                    ],
                                ]) ?>
                            </div>

                            <div class="col-md-3">
                                <?= $form->field($model, 'la_updated_user_id')->widget(Select2::class, [
                                    'options' => ['placeholder' => Yii::t('backend', 'Updated by user'), 'multiple' => true],
                                    'pluginOptions' => [
                                        'tags' => true,
                                        'tokenSeparators' => [',', ' '],
                                        'maximumInputLength' => 10
                                    ],
                                    'data' => $usersList
                                ]) ?>

                                <?= $form->field($model, 'companyId')->widget(Select2::class, [
                                    'options' => ['placeholder' => Yii::t('backend', 'Company'), 'multiple' => true],
                                    'pluginOptions' => [
                                        'tags' => true,
                                        'tokenSeparators' => [',', ' '],
                                        'maximumInputLength' => 10
                                    ],
                                    'data' => \backend\src\entities\partnerCompany\entity\PartnerCompanyQuery::getList()
                                ]) ?>

                                <?= $form->field($model, 'companiesSelectedByClient')->widget(Select2::class, [
                                    'options' => ['placeholder' => Yii::t('backend', 'Companies'), 'multiple' => true],
                                    'pluginOptions' => [
                                        'tags' => true,
                                        'tokenSeparators' => [',', ' '],
                                        'maximumInputLength' => 10
                                    ],
                                    'data' => \backend\src\entities\partnerCompany\entity\PartnerCompanyQuery::getList()
                                ]) ?>

                                <?= $form->field($model, 'wasDeclined')->dropDownList([
                                    0 => Yii::t('backend', 'No'),
                                    1 => Yii::t('backend', 'Yes')
                                ], ['prompt' => '--']) ?>
                            </div>
                        </div>

                        <?php // echo $form->field($model, 'la_loan_amount') ?>

                        <?php // echo $form->field($model, 'la_loan_term') ?>

                        <?php // echo $form->field($model, 'la_client_id') ?>

                        <?php // echo $form->field($model, 'la_client_name') ?>

                        <?php // echo $form->field($model, 'la_client_birth_date') ?>

                        <?php // echo $form->field($model, 'la_client_idnp') ?>

                        <?php // echo $form->field($model, 'la_updated_status_dt') ?>

                        <?php // echo $form->field($model, 'la_created_dt') ?>

                        <?php // echo $form->field($model, 'la_updated_dt') ?>

                        <?php // echo $form->field($model, 'la_updated_user_id') ?>

                        <div class="form-group">
                            <?= Html::submitButton(Yii::t('loan_application', 'Search'), ['class' => 'btn btn-primary']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
