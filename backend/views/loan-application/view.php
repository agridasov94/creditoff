<?php

use common\src\entities\client\entity\Client;
use common\src\entities\loanApplication\entity\LoanApplication;
use common\src\entities\loanApplication\entity\LoanApplicationStatus;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplication\entity\LoanApplication */

$this->title = $model->la_number;
$this->params['breadcrumbs'][] = ['label' => Yii::t('loan_application', 'Loan Applications'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="loan-application-view">

    <h4><?= Html::encode($this->title) ?></h4>

    <p>
        <?= Html::a(Yii::t('loan_application', 'Update'), ['update', 'id' => $model->la_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('loan_application', 'Delete'), ['delete', 'id' => $model->la_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('loan_application', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'la_id',
            'la_number',
            [
                'attribute' => 'la_product_id',
                'value' => static function (LoanApplication $model) {
                    return $model->product->p_name . ' (' . $model->la_product_id . ')';
                },
            ],
            'la_owner_id:username',
            [
                'attribute' => 'la_status',
                'value' => static function (LoanApplication $model) {
                    return LoanApplicationStatus::getStatusLabel($model->la_status);
                },
                'format' => 'raw'
            ],
            'la_loan_amount',
            'la_loan_term',
            'la_client_id',
            'la_client_name',
            'la_client_birth_date:byUserDate',
            'la_client_idnp',
            'la_distribution_iteration',
            [
                'attribute' => 'la_client_gender',
                'value' => function (LoanApplication $model) {
                    return $model->la_client_gender ? Client::getGenderName($model->la_client_gender) : null;
                }
            ],
            [
                'attribute' => 'la_client_civil_status',
                'value' => function (LoanApplication $model) {
                    return $model->la_client_civil_status ? Client::getCivilStatusName($model->la_client_civil_status) : null;
                }
            ],
            [
                'attribute' => 'la_client_social_status',
                'value' => function (LoanApplication $model) {
                    return $model->la_client_social_status ? Client::getSocialStatusName($model->la_client_social_status) : null;
                }
            ],
            [
                'attribute' => 'la_client_own_property',
                'format' => 'booleanByLabel',
            ],
            [
                'attribute' => 'la_client_children',
                'value' => static function (LoanApplication $model) {
                    return $model->la_client_children ? \yii\helpers\VarDumper::dumpAsString($model->la_client_children) : null;
                }
            ],
            'la_updated_status_dt:byUserDateTime',
            'la_created_dt:byUserDateTime',
            'la_updated_dt:byUserDateTime',
            'la_updated_user_id:username',
        ],
    ]) ?>

</div>
