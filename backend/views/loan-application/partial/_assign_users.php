<?php
/**
 * @var LoanApplication $request
 * @var UsersProductAccess[] $availableUsers
 * @var ManualDistributionForm $model
 */

use backend\src\entities\usersProductAccess\entity\UsersProductAccess;
use backend\src\useCase\loanApplication\manualDistribution\ManualDistributionForm;
use common\src\entities\loanApplication\entity\LoanApplication;
use common\src\entities\loanApplicationAccess\entity\LoanApplicationAccessQuery;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;

$pjaxId = 'assign-users-loan-application-pjax';
$showSubmitButton = false;
?>

<?php Pjax::begin(['enablePushState' => false, 'enableReplaceState' => false, 'id' => $pjaxId]) ?>
<?php $form = ActiveForm::begin(['options' => ['data-pjax' => 1]]) ?>

<?= $form->errorSummary($model) ?>

<?= $form->field($model, 'loanApplicationId')->hiddenInput()->label(false) ?>

<table class="table table-bordered">
    <thead>
        <tr>
            <th><?= Yii::t('backend', 'Users') ?></th>
            <th><?= Yii::t('backend', 'Select User') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($availableUsers as $availableUser): ?>
            <tr>
                <td><?= $availableUser->user->getFullName() ?></td>
                <td>
                    <?php if (!LoanApplicationAccessQuery::existsPendingByUserId($request->la_id, $availableUser->upa_user_id)): ?>
                        <?php $showSubmitButton = true; ?>
                        <?= Html::checkbox('usersIds[]', false, ['value' => $availableUser->upa_user_id]) ?>
                    <?php else: ?>
                        <i class="fa fa-warning yellow"></i> <?= Yii::t('backend', 'User Already has access to loan application') ?>
                    <?php endif; ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="text-center">
    <?php if ($showSubmitButton): ?>
        <?= Html::submitButton(Yii::t('backend', 'Submit'), [
            'class' => 'btn btn-success btn-sm'
        ]) ?>
    <?php endif; ?>
    <?= Html::button(Yii::t('backend', 'Close'), [
        'class' => 'btn btn-secondary btn-sm',
        'data-dismiss' => 'modal'
    ]) ?>
</div>
<?php ActiveForm::end() ?>
<?php Pjax::end() ?>
