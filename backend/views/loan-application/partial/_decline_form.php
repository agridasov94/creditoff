<?php
/**
 * @var $this \yii\web\View
 * @var $model \backend\src\forms\DeclineForm
 */

use yii\bootstrap4\ActiveForm;
use yii\widgets\Pjax;

$pjaxId = 'pjax-reason';
?>

<script>pjaxOffFormSubmit('<?= $pjaxId ?>')</script>
<?php Pjax::begin(['id' => $pjaxId, 'enableReplaceState' => false, 'enablePushState' => false, 'timeout' => 2000]) ?>

    <?php $form = ActiveForm::begin([
        'options' => [
            'data-pjax' => 1
        ]
    ]) ?>

        <?= $form->errorSummary($model) ?>

        <?= $form->field($model, 'loanApplicationId')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'reason')->textarea() ?>

        <div class="row">
            <div class="col-md-12 text-center">
                <?= \yii\helpers\Html::submitButton(Yii::t('backend', 'Submit'), [
                    'class' => 'btn btn-sm btn-success',
                    'id' => 'submit-reason'
                ]) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>

<?php Pjax::end() ?>

<?php
$js = <<<JS
$(document).off('pjax:beforeSend', '#{$pjaxId}').on('pjax:beforeSend', '#{$pjaxId}', function (obj, xhr, data) {
    let btnObj = $('#submit-reason');
    btnObj.html('<i class="fa fa-spin fa-spinner"></i>');
    btnObj.addClass('disabled').prop('disabled', true);
});
JS;
$this->registerJs($js);
