<?php
/**
 * @var $this \yii\web\View
 * @var $request LoanApplication
 */

use backend\src\helpers\ViewHelper;
use common\src\entities\client\employmentData\entity\ClientEmploymentData;
use common\src\entities\client\entity\Client;
use common\src\entities\client\passportData\entity\ClientPassportData;
use common\src\entities\loanApplication\clientEmployedData\entity\LoanApplicationClientEmployedData;
use common\src\entities\loanApplication\clientPassportData\entity\LoanApplicationClientPassportData;
use common\src\entities\loanApplication\entity\LoanApplication;
use common\src\entities\loanApplication\entity\LoanApplicationStatus;
use yii\helpers\Json;
use yii\widgets\DetailView;

$employedData = new \yii\data\ActiveDataProvider([
    'pagination' => false
]);
$employedData->setModels($request->clientEmployedData);
?>

<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
    <li class="nav-item" role="presentation">
        <a class="nav-link active" id="details-tab" data-toggle="pill" href="#details" role="tab" aria-controls="details" aria-selected="true"><?= Yii::t('loan_application', 'Loan Application Details') ?></a>
    </li>
    <li class="nav-item" role="presentation">
        <a class="nav-link" id="personal-tab" data-toggle="pill" href="#personal" role="tab" aria-controls="personal" aria-selected="false"><?= Yii::t('loan_application', 'Client Personal Info') ?></a>
    </li>
    <li class="nav-item" role="presentation">
        <a class="nav-link" id="employed-tab" data-toggle="pill" href="#employed" role="tab" aria-controls="employed" aria-selected="false"><?= Yii::t('loan_application', 'Client Employed Data') ?></a>
    </li>
    <li class="nav-item" role="presentation">
        <a class="nav-link" id="passport-tab" data-toggle="pill" href="#passport" role="tab" aria-controls="passport" aria-selected="false"><?= Yii::t('loan_application', 'Client Passport Data') ?></a>
    </li>
</ul>
<div class="tab-content" id="pills-tabContent">
    <div class="tab-pane fade show active" id="details" role="tabpanel" aria-labelledby="details-tab">
        <?= DetailView::widget([
            'model' => $request,
            'attributes' => [
                'la_id',
                'la_number',
                [
                    'attribute' => 'la_product_id',
                    'value' => static function (LoanApplication $model) {
                        return $model->product->p_name . ' (' . $model->la_product_id . ')';
                    },
                ],
                'la_owner_id:username',
                [
                    'attribute' => 'la_status',
                    'value' => static function (LoanApplication $model) {
                        return LoanApplicationStatus::getStatusLabel($model->la_status);
                    },
                    'format' => 'raw'
                ],
                'la_loan_amount',
                'la_loan_term',
                'la_updated_status_dt:byUserDateTime',
                'la_created_dt:byUserDateTime',
                'la_updated_dt:byUserDateTime',
                'la_updated_user_id:username',
            ],
        ]) ?>
    </div>
    <div class="tab-pane fade" id="personal" role="tabpanel" aria-labelledby="personal-tab">
        <?= DetailView::widget([
            'model' => $request,
            'attributes' => [
                'la_client_id',
                'la_client_name',
                'la_client_birth_date:byUserDate',
                'la_client_idnp',
                [
                    'attribute' => 'la_client_id',
                    'value' => function (LoanApplication $model) {
                        $emails = $model->clientEmails;
                        $data = \yii\helpers\ArrayHelper::getColumn($emails, 'lace_email');
                        return implode(', ', $data) ?: null;
                    },
                    'label' => Yii::t('client', 'Emails')
                ],
                [
                    'attribute' => 'la_client_id',
                    'value' => function (LoanApplication $model) {
                        $emails = $model->clientPhones;
                        $data = \yii\helpers\ArrayHelper::getColumn($emails, 'lacp_phone');
                        return implode(', ', $data) ?: null;
                    },
                    'label' => Yii::t('client', 'Phones')
                ],
                [
                    'attribute' => 'la_client_gender',
                    'value' => function (LoanApplication $model) {
                        return $model->la_client_gender ? Client::getGenderName($model->la_client_gender) : null;
                    }
                ],
                [
                    'attribute' => 'la_client_civil_status',
                    'value' => function (LoanApplication $model) {
                        return $model->la_client_civil_status ? Client::getCivilStatusName($model->la_client_civil_status) : null;
                    }
                ],
                [
                    'attribute' => 'la_client_social_status',
                    'value' => function (LoanApplication $model) {
                        return $model->la_client_social_status ? Client::getSocialStatusName($model->la_client_social_status) : null;
                    }
                ],
                [
                    'attribute' => 'la_client_own_property',
                    'format' => 'booleanByLabel',
                ],
                [
                    'attribute' => 'la_client_children',
                    'value' => static function (LoanApplication $model) {
                        return $model->la_client_children ? ViewHelper::displayChildrenData($model->la_client_children) : null;
                    },
                    'format' => 'raw'
                ],
            ],
        ]) ?>
    </div>
    <div class="tab-pane fade" id="employed" role="tabpanel" aria-labelledby="employed-tab">
        <?= \yii\grid\GridView::widget([
            'dataProvider' => $employedData,
            'tableOptions' => ['class' => 'table table-bordered table-condensed table-responsive'],
            'columns' => [
                [
                    'attribute' => 'laced_type',
                    'value' => static function (LoanApplicationClientEmployedData $model) {
                        return $model->laced_type ? ClientEmploymentData::getTypeName($model->laced_type) : null;
                    }
                ],
                'laced_work_phone',
                'laced_organization_name',
                'laced_position',
                'laced_seniority',
                'laced_official_wages',
                'laced_unofficial_wages',
                'laced_revenues_documented',
                'laced_revenues_non_documented',
            ],
        ]); ?>
    </div>
    <div class="tab-pane fade" id="passport" role="tabpanel" aria-labelledby="passport-tab">
        <?php if($request->clientPassportData): ?>
        <?= DetailView::widget([
            'model' => $request->clientPassportData,
            'attributes' => [
                [
                    'attribute' => 'lacpd_type',
                    'value' => static function (LoanApplicationClientPassportData $model) {
                        return $model->lacpd_type ? ClientPassportData::getTypeName($model->lacpd_type) : null;
                    },
                ],
                'lacpd_series',
                'lacpd_number',
                'lacpd_issued_by',
                'lacpd_issue_date',
                'lacpd_issue_department_code',
                'lacpd_expiration_date',
                'lacpd_registered_address',
                'lacpd_residence_address',
                'lacpd_created_dt',
            ],
        ]) ?>
        <?php else: ?>
            <?= \yii\bootstrap4\Alert::widget([
                'body' => Yii::t('product', 'Passport data is not set'),
                'options' => [
                    'class' => 'alert alert-warning'
                ],
                'closeButton' => false
            ]) ?>
        <?php endif; ?>
    </div>
</div>

<?php
$js = <<<JS
$('#pills-tab a').on('click', function (event) {
  event.preventDefault()
  $(this).tab('show')
})
JS;
$this->registerJs($js);

