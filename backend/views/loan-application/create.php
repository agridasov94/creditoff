<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplication\entity\LoanApplication */

$this->title = Yii::t('loan_application', 'Create Loan Application');
$this->params['breadcrumbs'][] = ['label' => Yii::t('loan_application', 'Loan Applications'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="loan-application-create">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
