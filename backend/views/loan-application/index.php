<?php

use backend\components\grid\DateTimeColumn;
use backend\components\grid\UserSelect2Column;
use backend\src\entities\partnerCompany\entity\PartnerCompanyQuery;
use common\src\entities\loanApplication\entity\LoanApplication;
use common\src\entities\loanApplication\entity\LoanApplicationQuery;
use common\src\entities\loanApplication\entity\LoanApplicationStatus;
use common\src\entities\loanApplicationAccess\entity\LoanApplicationAccessQuery;
use common\src\entities\product\entity\ProductQuery;
use kartik\export\ExportMenu;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\src\entities\loanApplication\entity\LoanApplicationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('loan_application', 'Loan Applications');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="loan-application-index">

    <h4><?= Html::encode($this->title) ?></h4>

<!--    <p>-->
        <?php // Html::a(Yii::t('loan_application', 'Create Loan Application'), ['create'], ['class' => 'btn btn-success']) ?>
<!--    </p>-->

    <?php Pjax::begin(['id' => 'pjax-loan-application-list']); ?>
    <?= $this->render('_search', ['model' => $searchModel]); ?>

<?php
$gridColumns = [
    [
        'class' => 'yii\grid\ActionColumn',
        'buttons' => [
            'view' => static function ($value, LoanApplication $model) {
                return Html::a('<i class="fas fa-eye"></i> ' . Yii::t('loan_application', 'Open Details'), null, [
                    'class' => 'btn btn-sm btn-info details',
                    'data-loan-number' => $model->la_number
                ]);
            },
            'assignUsers' => static function ($value, LoanApplication $model) {
                return Html::a('<i class="fas fa-user-check"></i> ' . Yii::t('loan_application', 'Assign Users'), null, [
                    'class' => 'btn btn-sm btn-success assign-user',
                    'data-loan-number' => $model->la_number
                ]);
            },
            'edit' => static function ($value, LoanApplication $model) {
                return Html::a('<i class="fa fa-pencil"></i> ' . Yii::t('loan_application', 'Edit'), ['/loan-application/update', 'id' => $model->la_id], [
                    'class' => 'btn btn-sm btn-warning',
                    'data-pjax' => 0
                ]);
            }
        ],
        'template' => '{view} {assignUsers} {edit}',
        'options' => [
            'width' => '150px;',
        ],
        'visibleButtons' => [
            'view' => true,
            'assignUsers' => static function (LoanApplication $model) {
                return $model->isSent() || $model->isDeclined();
            },
            'edit' => true
        ]
    ],
    [
        'attribute' => 'la_id',
        'options' => [
            'width' => '50px'
        ]
    ],
    'la_number',
    [
        'attribute' => 'la_product_id',
        'value' => static function (LoanApplication $model) {
            return $model->product->p_name;
        },
        'filter' => ProductQuery::getList()
    ],
    [
        'class' => UserSelect2Column::class,
        'attribute' => 'la_owner_id',
        'relation' => 'owner',
        'placeholder' => '',
    ],
    [
        'attribute' => 'companyId',
        'value' => static function (LoanApplication $model) {
            return $model->owner->partnerCompany->pc_name ?? null;
        },
        'filter' => PartnerCompanyQuery::getList(),
        'label' => Yii::t('company', 'Company')
    ],
    [
        'attribute' => 'la_status',
        'value' => static function (LoanApplication $model) {
            return LoanApplicationStatus::getStatusLabel($model->la_status);
        },
        'filter' => LoanApplicationStatus::getStatusNameList(),
        'format' => 'raw',
        'options' => [
            'width' => '50px'
        ]
    ],
    [
        'attribute' => 'la_manual_distribution',
        'filter' => [0 => Yii::t('backend', 'No'), 1 => Yii::t('backend', 'Yes')],
        'format' => 'booleanByLabel'
    ],
    'la_loan_amount',
    'la_loan_term',
    'la_client_id',
    'la_client_name',
    [
        'value' => static function (LoanApplication $model) {
            $names = [];
            foreach ($model->loanApplicationCompanies as $company) {
                $names[] = $company->partnerCompany->pc_name;
            }
            return implode(', ', $names);
        },
        'label' => Yii::t('loan_application', 'companies selected by the client')
    ],
    [
        'class' => DateTimeColumn::class,
        'attribute' => 'la_client_birth_date',
        'format' => 'byUserDate'
    ],
    'la_client_idnp',
    'la_distribution_iteration',
    [
        'label' => Yii::t('loan_application', 'Number of partners who see the application'),
        'value' => static function (LoanApplication $model) {
            return LoanApplicationAccessQuery::getCountPendingByRequest($model->la_id);
        }
    ],
    [
        'label' => Yii::t('loan_application', 'Was Declined'),
        'value' => static function (LoanApplication $model) {
            return LoanApplicationQuery::isApplicationWasDeclined($model->la_id);
        },
        'format' => 'booleanByLabel'
    ],
    [
        'class' => DateTimeColumn::class,
        'attribute' => 'la_updated_status_dt',
        'format' => 'byUserDateTime'
    ],
    [
        'class' => DateTimeColumn::class,
        'attribute' => 'la_created_dt',
        'format' => 'byUserDateTime'
    ],
    [
        'class' => DateTimeColumn::class,
        'attribute' => 'la_updated_dt',
        'format' => 'byUserDateTime'
    ],
    [
        'class' => UserSelect2Column::class,
        'attribute' => 'la_updated_user_id',
        'relation' => 'updatedUser',
        'placeholder' => '',
    ],
]
?>

    <?php if(Yii::$app->request->isPjax): ?>
    <script>
        $('.export-wrapper .dropdown-menu a').each(function (i, e) {
            $(document).off('click.exportmenu', '#' + $(e).attr('id'));
        });
    </script>
    <?php endif; ?>
    <?= ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'dropdownOptions' => [
            'label' => 'Export All',
            'class' => 'btn btn-outline-secondary btn-default'
        ],
        'container' => [
            'class' => 'export-wrapper'
        ],
        'showConfirmAlert' => false,
        'filename' => Yii::t('export', 'Loan_application') . '_' . date('Y-m-d_H:i:s')
    ]) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => $gridColumns
    ]); ?>

    <?php Pjax::end(); ?>

</div>
<?php
$detailsUrl = \yii\helpers\Url::to(['/loan-application/details']);
$modalTitle = Yii::t('loan_application', 'Loan Application Details');
$modalAssignUsersTitle = Yii::t('loan_application', 'Assign Users');
$assignUsersUrl = \yii\helpers\Url::to(['/loan-application/ajax-assign-users']);
$js = <<<JS
$(document).ready(function () {
   $(document).on('click', '.details', function (e) {
       e.preventDefault();
        let btn = $(this);
        let btnHtml = btn.html();
        let number = btn.data('loan-number');
        let modal = $('#modal-lg');
        btn.html('<span class="spinner-border spinner-border-sm"></span> Loading').prop("disabled", true);
        modal.find('.modal-title').html('{$modalTitle}');
        modal.find('.modal-body').html('').load('{$detailsUrl}?number=' + number, function(response, status, xhr ) {
            btn.html(btnHtml).prop("disabled", false);
            if (xhr.status !== 200) {
                setTimeout(function () {
                    modal.modal('hide');
                }, 500);
                createNotify('Error', xhr.responseText, 'error');
                return false;
            }
            modal.modal({
              backdrop: 'static',
              show: true
            });
        });
   });
   $(document).on('click', '.assign-user', function (e) {
       e.preventDefault();
        let btn = $(this);
        let btnHtml = btn.html();
        let number = btn.data('loan-number');
        let modal = $('#modal-md');
        btn.html('<span class="spinner-border spinner-border-sm"></span> Loading').prop("disabled", true);
        modal.find('.modal-title').html('{$modalAssignUsersTitle}');
        modal.find('.modal-body').html('').load('{$assignUsersUrl}?number=' + number, function(response, status, xhr ) {
            btn.html(btnHtml).prop("disabled", false);
            if (xhr.status !== 200) {
                setTimeout(function () {
                    modal.modal('hide');
                }, 500);
                createNotify('Error', xhr.responseText, 'error');
                return false;
            }
            modal.modal({
              backdrop: 'static',
              show: true
            });
        });
   }); 
});
JS;
$this->registerJs($js);

