<?php

use backend\components\grid\DateTimeColumn;
use backend\components\grid\UserSelect2Column;
use common\src\entities\loanApplication\entity\LoanApplication;
use common\src\entities\loanApplication\entity\LoanApplicationStatus;
use common\src\entities\product\entity\ProductQuery;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\src\entities\loanApplication\entity\LoanApplicationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('loan_application', 'Loan Applications in status "Sent"');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="loan-application-index">

    <h4><?= Html::encode($this->title) ?></h4>

    <?php Pjax::begin(['id' => 'pjax-sent-loan-application']); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'id' => 'loanApplicationSent',
        'columns' => [
            [
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'processLoanApplication' => static function ($value, LoanApplication $model) {
                        return Html::a('<i class="fas fa-recycle"></i> ' . Yii::t('loan_application', 'Open Personal Data'), null, [
                            'class' => 'btn btn-sm btn-info take',
                            'data-loan-number' => $model->la_number
                        ]);
                    }
                ],
                'template' => '{processLoanApplication}',
                'options' => [
                    'width' => '200px'
                ]
            ],
            [
                'attribute' => 'la_number',
                'options' => [
                    'width' => '200px'
                ]
            ],
            [
                'attribute' => 'la_loan_amount',
                'options' => [
                    'width' => '200px'
                ]
            ],
            [
                'attribute' => 'la_loan_term',
                'options' => [
                    'width' => '200px'
                ]
            ],
            [
                'attribute' => 'la_product_id',
                'value' => static function (LoanApplication $model) {
                    return $model->product->p_name;
                },
                'filter' => ProductQuery::getList()
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>

<?php
$openPersonalDataUrl = Url::to(['/loan-application/take']);
$modalTitle = Yii::t('loan_application', 'Client Personal Data');
$js = <<<JS
$(document).ready(function () {
    $(document).on('click', '.take', function (e) {
        e.preventDefault();
        let btn = $(this);
        let btnHtml = btn.html();
        let number = btn.data('loan-number');
        let modal = $('#modal-lg');
        btn.html('<span class="spinner-border spinner-border-sm"></span> Loading').prop("disabled", true);
        $.post('{$openPersonalDataUrl}', {number: number}, function (data, status, xhr) {
            if (xhr.status !== 200) {
               createNotify('Error', xhr.responseText, 'error');
               return false;
            }
            if (data.error) {
                createNotify('Error', data.message, 'error');
                return false;
            }
            createNotify('Success', data.message, 'success');
            btn.closest('tr').remove();
            let totalItems = $('.loan-application-index').find('.summary b').text();
            $('.loan-application-index').find('.summary b').text(totalItems - 1);
        }, 'json').fail(function (xhr) {
            createNotify('Error', xhr.responseText, 'error');
        }).always(function () {
            btn.html(btnHtml).prop("disabled", false);
        });
//        modal.modal('show');
//        modal.find('.modal-title').html('{$modalTitle}');
//        modal.find('.modal-body').html('').load('{$openPersonalDataUrl}?number=' + number, function(response, status, xhr ) {
//            btn.html(btnHtml).prop("disabled", false);
//            if (xhr.status !== 200) {
//                setTimeout(function () {
//                    modal.modal('hide');
//                }, 500);
//                createNotify('Error', xhr.responseText, 'error');
//                return false;
//            }
//        });
    });
    
    
});
JS;
$this->registerJs($js);

