<?php

use backend\components\grid\DateTimeColumn;
use backend\components\grid\UserSelect2Column;
use common\src\entities\client\entity\Client;
use common\src\entities\loanApplication\entity\LoanApplication;
use common\src\entities\loanApplication\entity\LoanApplicationStatus;
use common\src\entities\product\entity\ProductQuery;
use kartik\export\ExportMenu;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\src\entities\loanApplication\entity\LoanApplicationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $additionalExportColumns array */

$this->title = Yii::t('loan_application', 'Loan Applications in status "In Work"');
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    'c_id',
    'c_name',
    'c_last_name',
    'c_birth_date:byUserDate',
    'c_idnp',
    [
        'attribute' => 'c_is_activated',
        'format' => 'booleanByLabel',
    ],
    [
        'attribute' => 'c_gender',
        'value' => static function (Client $model) {
            return $model->c_gender ? Client::getGenderName($model->c_gender) : null;
        }
    ],
    [
        "attribute" => 'c_civil_status',
        'value' => static function (Client $model) {
            return $model->c_civil_status ? Client::getCivilStatusName($model->c_civil_status) : null;
        }
    ],
    [
        'attribute' => 'c_social_status',
        'value' => static function (Client $model) {
            return $model->c_social_status ? Client::getSocialStatusName($model->c_social_status) : null;
        }
    ],
    'c_created_user_id:username',
    'c_updated_user_id:username',
    'c_created_dt:byUserDateTime',
    'c_updated_dt:byUserDateTime',
];
?>
    <div class="loan-application-index">

        <h4><?= Html::encode($this->title) ?></h4>

        <?php Pjax::begin(); ?>

        <?php if(Yii::$app->request->isPjax): ?>
            <script>
                $('.export-wrapper .dropdown-menu a').each(function (i, e) {
                    $(document).off('click.exportmenu', '#' + $(e).attr('id'));
                });
            </script>
        <?php endif; ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'attribute' => 'la_number',
                    'options' => [
                        'width' => '200px'
                    ]
                ],
                [
                    'attribute' => 'la_loan_amount',
                    'options' => [
                        'width' => '100px'
                    ]
                ],
                [
                    'attribute' => 'la_loan_term',
                    'options' => [
                        'width' => '100px'
                    ]
                ],
                [
                    'attribute' => 'la_product_id',
                    'value' => static function (LoanApplication $model) {
                        return $model->product->p_name;
                    },
                    'filter' => ProductQuery::getList()
                ],
                'la_client_name',
                'la_client_birth_date:byUserDate',
                'la_client_idnp',
                [
                    'attribute' => 'clientPhone',
                    'value' => static function (LoanApplication $model) {
                        $phones = ArrayHelper::getColumn($model->clientPhones ?? [], 'lacp_phone');
                        return implode(', ', $phones) ?: null;
                    }
                ],
                [
                    'attribute' => 'clientEmail',
                    'value' => static function (LoanApplication $model) {
                        $emails = ArrayHelper::getColumn($model->clientEmails ?? [], 'lace_email');
                        return implode(', ', $emails) ?: null;
                    }
                ],
                [
                    'class' => DateTimeColumn::class,
                    'attribute' => 'la_updated_status_dt',
                    'format' => 'byUserDateTime'
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'buttons' => [
//                        'processLoanApplication' => static function ($value, LoanApplication $model) {
//                            return Html::a('<i class="fas fa-recycle"></i> ' . Yii::t('loan_application', 'Open Personal Data'), null, [
//                                'class' => 'btn btn-sm btn-info take',
//                                'data-loan-number' => $model->la_number
//                            ]);
//                        }
                        'view' => static function ($value, LoanApplication $model) {
                            return Html::a('<i class="fas fa-eye"></i> ' . Yii::t('loan_application', 'Open Details'), null, [
                                'class' => 'btn btn-sm btn-info details',
                                'data-loan-number' => $model->la_number
                            ]);
                        },
                        'approve' => static function ($value, LoanApplication $model) {
                            return Html::a('<i class="fas fa-recycle"></i> ' . Yii::t('loan_application', 'Approve'), null, [
                                'class' => 'btn btn-sm btn-success approve',
                                'data-loan-number' => $model->la_number
                            ]);
                        },
                        'decline' => static function ($value, LoanApplication $model) {
                            return Html::a('<i class="fas fa-close"></i> ' . Yii::t('loan_application', 'Decline'), null, [
                                'class' => 'btn btn-sm btn-danger decline',
                                'data-loan-number' => $model->la_number
                            ]);
                        },
                        'export' => static function ($value, LoanApplication $model) use ($gridColumns, $additionalExportColumns) {

                            $query = Client::find();
                            $dataProvider = new \yii\data\ActiveDataProvider([
                                'query' => $query,
                                'pagination' => [
                                    'pageSize' => 1
                                ]
                            ]);
                            $query->andFilterWhere(['c_id' => $model->la_client_id]);
                            return ExportMenu::widget([
                                'dataProvider' => $dataProvider,
                                'columns' => ArrayHelper::merge($gridColumns, $additionalExportColumns),
                                'dropdownOptions' => [
                                    'label' => 'Export Client Data',
                                    'class' => 'btn btn-outline-secondary btn-default btn-sm'
                                ],
                                'container' => [
                                    'class' => 'export-wrapper'
                                ],
                                'showConfirmAlert' => false,
                                'filename' => Yii::t('export', 'Client') . '_' . date('Y-m-d_H:i:s'),
                                'exportRequestParam' => 'export',
                                'toggleDataOptions' => [
                                    'class' => 'btn btn-outline-secondary btn-default btn-sm'
                                ],
                                'columnSelectorOptions' => [
                                    'class' => 'btn btn-outline-secondary btn-default btn-sm'
                                ],
                                'containerOptions' => [
                                    'style' => 'display: flex;'
                                ],
                                'clearBuffers' => true
                            ]);
                        }
                    ],
                    'template' => '{view} {approve} {decline} {export}',
                    'options' => [
                        'width' => '200px'
                    ]
                ],
            ],
        ]); ?>

        <?php Pjax::end(); ?>

    </div>

<?php
$detailsUrl = \yii\helpers\Url::to(['/loan-application/details']);
$approveUrl = \yii\helpers\Url::to(['/loan-application/approve-in-work']);
$declineUrl = \yii\helpers\Url::to(['/loan-application/decline-in-work']);
$confirmDeclineText = Yii::t('loan_application', 'Confirm rejection of loan application');
$modalTitle = Yii::t('loan_application', 'Loan Application Details');
$modalDeclineTitle = Yii::t('loan_application', 'Loan Application Decline Reason');
$js = <<<JS
$(document).ready(function () {
    $(document).on('click', '.details', function (e) {
       e.preventDefault();
        let btn = $(this);
        let btnHtml = btn.html();
        let number = btn.data('loan-number');
        let modal = $('#modal-lg');
        btn.html('<span class="spinner-border spinner-border-sm"></span> Loading').prop("disabled", true);
        modal.find('.modal-title').html('{$modalTitle}');
        modal.find('.modal-body').html('').load('{$detailsUrl}?number=' + number, function(response, status, xhr ) {
            btn.html(btnHtml).prop("disabled", false);
            if (xhr.status !== 200) {
                setTimeout(function () {
                    modal.modal('hide');
                }, 500);
                createNotify('Error', xhr.responseText, 'error');
                return false;
            }
            modal.modal({
              backdrop: 'static',
              show: true
            });
        });
   }); 
    
    $(document).on('click', '.approve', function (e) {
        e.preventDefault();
        let btn = $(this);
        let btnHtml = btn.html();
        let number = btn.data('loan-number');
        btn.html('<span class="spinner-border spinner-border-sm"></span> Loading').prop("disabled", true);
        $.post('{$approveUrl}', {number: number}, function (data, status, xhr) {
            if (xhr.status !== 200) {
               createNotify('Error', xhr.responseText, 'error');
               return false;
            }
           createNotify('Success', data.message, 'success');
           btn.closest('tr').remove();
           let totalItems = $('.loan-application-index').find('.summary b').text();
           $('.loan-application-index').find('.summary b').text(totalItems - 1);
        }, 'json').fail(function (xhr) {
            createNotify('Error', xhr.responseText, 'error');
        }).always(function () {
            btn.html(btnHtml).prop("disabled", false);
        });
    });
    
    $(document).on('click', '.decline', function (e) {
        e.preventDefault();
        let btn = $(this);
        let btnHtml = btn.html();
        let number = btn.data('loan-number');
        let modal = $('#modal-sm');
        if (confirm('{$confirmDeclineText}')) {
            btn.html('<span class="spinner-border spinner-border-sm"></span> Loading').prop("disabled", true);
            modal.modal('show');
            modal.find('.modal-title').html('{$modalTitle}');
            modal.find('.modal-body').html('').load('{$declineUrl}?number=' + number, function(response, status, xhr ) {
                btn.html(btnHtml).prop("disabled", false);
                if (xhr.status !== 200) {
                    setTimeout(function () {
                        modal.modal('hide');
                    }, 500);
                    createNotify('Error', xhr.responseText, 'error');
                    return false;
                }
            });
//            $.post('{$declineUrl}', {number: number}, function (data, status, xhr) {
//                if (xhr.status !== 200) {
//                   createNotify('Error', xhr.responseText, 'error');
//                   return false;
//                }
//               createNotify('Success', data.message, 'success');
//               btn.closest('tr').remove();
//               let totalItems = $('.loan-application-index').find('.summary b').text();
//               $('.loan-application-index').find('.summary b').text(totalItems - 1);
//            }, 'json').fail(function (xhr) {
//                createNotify('Error', xhr.responseText, 'error');
//            }).always(function () {
//                btn.html(btnHtml).prop("disabled", false);
//            });
        }
    });
});
JS;
$this->registerJs($js);

