<?php

use backend\components\grid\DateTimeColumn;
use backend\components\grid\UserSelect2Column;
use common\src\entities\loanApplication\entity\LoanApplication;
use common\src\entities\loanApplication\entity\LoanApplicationStatus;
use common\src\entities\product\entity\ProductQuery;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\src\entities\loanApplication\entity\LoanApplicationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('loan_application', 'Loan Applications in status "Declined"');
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="loan-application-index">

        <h4><?= Html::encode($this->title) ?></h4>

        <?php Pjax::begin(); ?>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'attribute' => 'la_number',
                    'options' => [
                        'width' => '200px'
                    ]
                ],
                [
                    'attribute' => 'la_loan_amount',
                    'options' => [
                        'width' => '100px'
                    ]
                ],
                [
                    'attribute' => 'la_loan_term',
                    'options' => [
                        'width' => '100px'
                    ]
                ],
                [
                    'attribute' => 'la_product_id',
                    'value' => static function (LoanApplication $model) {
                        return $model->product->p_name;
                    },
                    'filter' => ProductQuery::getList()
                ],
                'la_client_name',
                'la_client_birth_date:byUserDate',
                'la_client_idnp',
                [
                    'attribute' => 'clientPhone',
                    'value' => static function (LoanApplication $model) {
                        $phones = \yii\helpers\ArrayHelper::getColumn($model->clientPhones ?? [], 'lacp_phone');
                        return implode(', ', $phones) ?: null;
                    }
                ],
                [
                    'attribute' => 'clientEmail',
                    'value' => static function (LoanApplication $model) {
                        $emails = \yii\helpers\ArrayHelper::getColumn($model->clientEmails ?? [], 'lace_email');
                        return implode(', ', $emails) ?: null;
                    }
                ],
                [
                    'class' => DateTimeColumn::class,
                    'attribute' => 'la_updated_status_dt',
                    'format' => 'byUserDateTime'
                ],
            ],
        ]); ?>

        <?php Pjax::end(); ?>

    </div>
