<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplication\entity\LoanApplication */

$this->title = Yii::t('loan_application', 'Update Loan Application: {name}', [
    'name' => $model->la_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('loan_application', 'Loan Applications'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->la_id, 'url' => ['view', 'id' => $model->la_id]];
$this->params['breadcrumbs'][] = Yii::t('loan_application', 'Update');
?>
<div class="loan-application-update">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
