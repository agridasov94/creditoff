<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\src\entities\faq\entity\Faq */

$this->title = $model->f_question;
$this->params['breadcrumbs'][] = ['label' => Yii::t('faq', 'Faqs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="faq-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('faq', 'Update'), ['update', 'f_id' => $model->f_id, 'f_lang_id' => $model->f_lang_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('faq', 'Delete'), ['delete', 'f_id' => $model->f_id, 'f_lang_id' => $model->f_lang_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('faq', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'f_id',
            'f_lang_id',
            'f_question',
            'f_answer:ntext',
            'f_created_dt',
            'f_updated_dt',
            'f_created_user_id',
            'f_updated_user_id',
        ],
    ]) ?>

</div>
