<?php

use backend\components\grid\DateTimeColumn;
use backend\components\grid\UserSelect2Column;
use common\src\entities\faq\entity\Faq;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\src\entities\faq\entity\FaqSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('faq', 'Faqs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faq-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('faq', 'Create Faq'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'f_id',
                'options' => [
                    'width' => '100px'
                ]
            ],
            [
                'attribute' => 'f_lang_id',
                'options' => [
                    'width' => '100px'
                ],
                'filter' => \common\src\entities\language\LanguageQuery::getList()
            ],
            'f_question',
            'f_answer:ntext',
            [
                'attribute' => 's_value',
                'value' => static function (Faq $model) {
                    $val = Html::encode($model->f_answer);
                    $truncatedStr = '<pre><small>' .
                        StringHelper::truncate(
                            VarDumper::dumpAsString($val),
                            1200,
                            '...',
                            null,
                            false
                        ) . '</small></pre>';
                    $detailBox    = '<div id="detail_' . $model->f_id . '_' . $model->f_lang_id . '" style="display: none;">' . $val . '</div>';
                    $detailBtn    = ' <i class="fas fa-eye green showDetail" style="cursor: pointer;" data-idt="' . $model->f_id . '_' . $model->f_lang_id . '" data-title="' . $model->f_question . ' - '  . $model->f_lang_id . '"></i>';
                    return $truncatedStr . $detailBox . $detailBtn;
                },
                'format' => 'raw',
                //'filter' => false
            ],
            [
                'class' => UserSelect2Column::class,
                'attribute' => 'f_created_user_id',
                'relation' => 'createdUser',
                'placeholder' => '',
            ],
            [
                'class' => UserSelect2Column::class,
                'attribute' => 'f_updated_user_id',
                'relation' => 'updatedUser',
                'placeholder' => '',
            ],
            [
                'class' => DateTimeColumn::class,
                'attribute' => 'f_created_dt',
                'format' => 'byUserDateTime'
            ],
            [
                'class' => DateTimeColumn::class,
                'attribute' => 'f_updated_dt',
                'format' => 'byUserDateTime'
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>


<?php
yii\bootstrap4\Modal::begin([
    'title' => 'Log detail',
    'id' => 'modal',
    'size' => \yii\bootstrap4\Modal::SIZE_LARGE,
]);
yii\bootstrap4\Modal::end();


$jsCode = <<<JS
    $(document).on('click', '.showDetail', function(){
        
        let recordId = $(this).data('idt');
        let title = $(this).data('title');
        let detailEl = $('#detail_' + recordId);
        let modalBodyEl = $('#modal .modal-body');
        
        modalBodyEl.html(detailEl.html()); 
        $('#modal-label').html('Details (' + title + ')');       
        $('#modal').modal('show');
        return false;
    });
JS;

$this->registerJs($jsCode, \yii\web\View::POS_READY);
