<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \common\src\entities\faq\entity\Faq */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">
    <div class="col-md-6">
        <div class="faq-form">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'f_question')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'f_answer')->textarea(['rows' => 6]) ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('faq', 'Save'), ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
