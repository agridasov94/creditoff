<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\src\entities\faq\entity\FaqSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="faq-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'f_id') ?>

    <?= $form->field($model, 'f_lang_id') ?>

    <?= $form->field($model, 'f_question') ?>

    <?= $form->field($model, 'f_answer') ?>

    <?= $form->field($model, 'f_created_dt') ?>

    <?php // echo $form->field($model, 'f_updated_dt') ?>

    <?php // echo $form->field($model, 'f_created_user_id') ?>

    <?php // echo $form->field($model, 'f_updated_user_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('faq', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('faq', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
