<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\src\entities\faq\entity\Faq */

$this->title = Yii::t('faq', 'Update Faq: {name}', [
    'name' => $model->f_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('faq', 'Faqs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->f_id, 'url' => ['view', 'f_id' => $model->f_id, 'f_lang_id' => $model->f_lang_id]];
$this->params['breadcrumbs'][] = Yii::t('faq', 'Update');
?>
<div class="faq-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_update', [
        'model' => $model,
    ]) ?>

</div>
