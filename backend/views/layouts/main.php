<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\NotifyAsset;
use backend\src\auth\Auth;
use backend\src\helpers\WebSocketContainer;
use backend\src\helpers\WebSocketHelper;
use backend\widgets\notifications\NotificationWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

//NotifyAsset::register($this);
\backend\assets\AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="nav-md">
<?php $this->beginBody() ?>

<div class="container body">
    <?php /*
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'Home', 'url' => ['/site/index']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    */ ?>
    <?php /* Alert::widget() */ ?>
    <div class="main_container">
        <?= \backend\widgets\navbar\NavBarMenu::widget() ?>

        <div class="top_nav">
            <div class="nav_menu">
                <div class="nav toggle">
                    <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                </div>
                <nav class="nav navbar-nav">
                    <ul class=" navbar-right">
                        <li class="nav-item dropdown open" style="padding-left: 15px;">
                            <a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                                <img src="/images/pngwing.com.png" alt=""><?= Yii::$app->user->getIdentity()->username ?>
                            </a>
                            <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
<!--                                <a class="dropdown-item"  href="javascript:;"> Profile</a>-->
<!--                                <a class="dropdown-item"  href="javascript:;">-->
<!--                                    <span class="badge bg-red pull-right">50%</span>-->
<!--                                    <span>Settings</span>-->
<!--                                </a>-->
<!--                                <a class="dropdown-item"  href="javascript:;">Help</a>-->
                                <?= Html::a('<i class="fa fa-user pull-right"></i> My Profile', Url::to(['/profile/manage']), [
                                    'class' => 'dropdown-item'
                                ]) ?>
                                <?= Html::beginForm(['/site/logout'], 'post', [
                                        'class' => 'logout-form'
                                ]) ?>
                                    <?= Html::submitButton('<i class="fa fa-sign-out pull-right"></i> Log Out', [
                                        'class' => 'dropdown-item'
                                    ]) ?>
                                <?= Html::endForm() ?>
                            </div>
                        </li>

                        <li class="nav-item dropdown open" style="padding-left: 15px;">
                            <a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                                <?= ucfirst(Yii::$app->language) ?>
                            </a>
                            <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown" style="width: auto; min-width: auto">
                                <?php foreach (Yii::$app->multiLanguage->langs as $lang): ?>
                                    <div>
                                        <?= $lang !== Yii::$app->language ? Html::a(ucfirst($lang), Url::to([Yii::$app->request->getUrl(), 'lang' => $lang]), [
                                            'class' => 'dropdown-item',
                                            'style' => 'padding: 12px 15px'
                                        ]) : '' ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </li>

                        <li style="margin-left: 15px;">
                            <a href="javascript:;" class="info-number" title="Online Connection" id="online-connection-indicator">
                                <i class="fa fa-plug"></i>
                            </a>
                        </li>

                        <?= NotificationWidget::widget(['userId' => Auth::id()]) ?>
                    </ul>
                </nav>
            </div>
        </div>

        <div class="right_col" role="main">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                'itemTemplate' => '<li class="breadcrumb-item">{link}</li>',
                'activeItemTemplate' => '<li class="breadcrumb-item active">{link}</li>',
            ]) ?>
            <?= $content ?>
        </div>

        <footer>
            <div class="pull-right">
                &copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?>
            </div>
            <div class="clearfix"></div>
        </footer>
    </div>
</div>

<?= $this->render('_modals') ?>
<?php $this->endBody() ?>
<?php

$subList = [];

$webSocketContainer = Yii::$container->get(WebSocketContainer::class);
WebSocketHelper::subscribeToLoanApplicationChannel($webSocketContainer->getLoanApplicationStatus(), Auth::id(), $subList);
$params = [
    'sub_list' => $subList
];

$wsUrl = (Yii::$app->request->isSecureConnection ? 'wss' : 'ws') . '://' . Yii::$app->params['webSocketServer']['connectionUrl'] . '/?' . http_build_query($params);

?>
<script>
$(document).ready(function () {
    let wsUrl = '<?= $wsUrl ?>';
    let userId = <?= Auth::id() ?>;
    wsInitConnect(wsUrl, 10000, userId, $('#online-connection-indicator'));
});
</script>
</body>
</html>
<?php $this->endPage() ?>
