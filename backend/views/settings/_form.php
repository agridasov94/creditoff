<?php

use common\src\entities\settings\entity\Settings;
use kdn\yii2\JsonEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\src\entities\settings\entity\Settings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="settings-form">

    <div class="col-md-9">
        <?php $form = ActiveForm::begin(); ?>

        <div class="col-md-9">

            <?php //= $form->field($model, 's_key')->textInput(['maxlength' => true]) ?>

            <?php //= $form->field($model, 's_name')->textInput(['maxlength' => true]) ?>

            <?php //= $form->field($model, 's_type')->textInput(['maxlength' => true]) ?>

            <?php
            if ($model->s_type === Settings::TYPE_STRING) {
                echo $form->field($model, 's_value')->textInput();
            } else if ($model->s_type === Settings::TYPE_BOOL) {
                echo $form->field($model, 's_value')->checkbox();//->label($model->s_name);
            } else if ($model->s_type === Settings::TYPE_INT) {
                echo $form->field($model, 's_value')->input('number');//->label($model->s_name);
            } else if ($model->s_type === Settings::TYPE_DOUBLE) {
                echo $form->field($model, 's_value')->input('number', ['step' => 0.01]);//->label($model->s_name);
            } else if ($model->s_type === Settings::TYPE_ARRAY) {
                try {
                    echo $form->field($model, 's_value')->widget(
                        JsonEditor::class,
                        [
                            'clientOptions' => [
                                'modes' => ['code', 'form', 'tree', 'view'], //'text',
                                'mode' => 'tree'
                            ],
                            //'collapseAll' => ['view'],
                            'expandAll' => ['tree', 'form'],
                        ]
                    );
                } catch (Exception $exception) {
                    echo $form->field($model, 's_value')->textarea(['rows' => 5]);//->label($model->s_name);
                }
            } else {
//                echo $form->field($model, 's_value')->textInput(['maxlength' => true])->label($model->s_name);
                echo $form->field($model, 's_value')->widget(
                    JsonEditor::class,
                    [
                        'clientOptions' => [
                            'modes' => ['code', 'form', 'tree', 'view'], //'text',
                            'mode' => 'tree'
                        ],
                        //'collapseAll' => ['view'],
                        'expandAll' => ['tree', 'form'],
                    ]
                );
            }
            ?>

            <?php //= $form->field($model, 's_value')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 's_description')->textarea() ?>

            <?php //= $form->field($model, 's_updated_dt')->textInput() ?>

            <?php //= $form->field($model, 's_updated_user_id')->textInput() ?>

            <div class="form-group">
                <?= Html::submitButton('Save Value', ['class' => 'btn btn-success']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

</div>
