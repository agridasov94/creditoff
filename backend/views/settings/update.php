<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\src\entities\settings\entity\Settings */

$this->title = Yii::t('settings', 'Update Settings: {name}', [
    'name' => $model->s_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('settings', 'Settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->s_id, 'url' => ['view', 'id' => $model->s_id]];
$this->params['breadcrumbs'][] = Yii::t('settings', 'Update');
?>
<div class="settings-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
