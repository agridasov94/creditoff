<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\src\entities\loanApplication\clientEmployedData\entity\LoanApplicationClientEmployedDataSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('loan_application', 'Loan Application Client Employed Datas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="loan-application-client-employed-data-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('loan_application', 'Create Loan Application Client Employed Data'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'laced_id',
            'laced_client_id',
            'laced_la_id',
            'laced_type',
            'laced_work_phone',
            //'laced_organization_name',
            //'laced_position',
            //'laced_seniority',
            //'laced_official_wages',
            //'laced_unofficial_wages',
            //'laced_revenues_documented',
            //'laced_revenues_non_documented',
            //'laced_created_dt',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
