<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplication\clientEmployedData\entity\LoanApplicationClientEmployedDataSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loan-application-client-employed-data-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'laced_id') ?>

    <?= $form->field($model, 'laced_client_id') ?>

    <?= $form->field($model, 'laced_la_id') ?>

    <?= $form->field($model, 'laced_type') ?>

    <?= $form->field($model, 'laced_work_phone') ?>

    <?php // echo $form->field($model, 'laced_organization_name') ?>

    <?php // echo $form->field($model, 'laced_position') ?>

    <?php // echo $form->field($model, 'laced_seniority') ?>

    <?php // echo $form->field($model, 'laced_official_wages') ?>

    <?php // echo $form->field($model, 'laced_unofficial_wages') ?>

    <?php // echo $form->field($model, 'laced_revenues_documented') ?>

    <?php // echo $form->field($model, 'laced_revenues_non_documented') ?>

    <?php // echo $form->field($model, 'laced_created_dt') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('loan_application', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('loan_application', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
