<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplication\clientEmployedData\entity\LoanApplicationClientEmployedData */

$this->title = $model->laced_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('loan_application', 'Loan Application Client Employed Datas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="loan-application-client-employed-data-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('loan_application', 'Update'), ['update', 'id' => $model->laced_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('loan_application', 'Delete'), ['delete', 'id' => $model->laced_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('loan_application', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'laced_id',
            'laced_client_id',
            'laced_la_id',
            'laced_type',
            'laced_work_phone',
            'laced_organization_name',
            'laced_position',
            'laced_seniority',
            'laced_official_wages',
            'laced_unofficial_wages',
            'laced_revenues_documented',
            'laced_revenues_non_documented',
            'laced_created_dt',
        ],
    ]) ?>

</div>
