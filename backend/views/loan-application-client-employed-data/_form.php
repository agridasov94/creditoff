<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplication\clientEmployedData\entity\LoanApplicationClientEmployedData */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loan-application-client-employed-data-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'laced_client_id')->textInput() ?>

    <?= $form->field($model, 'laced_la_id')->textInput() ?>

    <?= $form->field($model, 'laced_type')->textInput() ?>

    <?= $form->field($model, 'laced_work_phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'laced_organization_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'laced_position')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'laced_seniority')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'laced_official_wages')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'laced_unofficial_wages')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'laced_revenues_documented')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'laced_revenues_non_documented')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'laced_created_dt')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('loan_application', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
