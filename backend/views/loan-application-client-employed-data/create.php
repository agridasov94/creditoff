<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplication\clientEmployedData\entity\LoanApplicationClientEmployedData */

$this->title = Yii::t('loan_application', 'Create Loan Application Client Employed Data');
$this->params['breadcrumbs'][] = ['label' => Yii::t('loan_application', 'Loan Application Client Employed Datas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="loan-application-client-employed-data-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
