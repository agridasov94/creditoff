<?php

use backend\src\useCase\clientUpdate\ChildrenForm;
use borales\extensions\phoneInput\PhoneInput;
use common\src\entities\client\employmentData\entity\ClientEmploymentData;
use common\src\entities\client\entity\Client;
use common\src\entities\client\passportData\entity\ClientPassportData;
use kartik\date\DatePicker;
use unclead\multipleinput\MultipleInput;
use unclead\multipleinput\MultipleInputColumn;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \backend\src\useCase\clientUpdate\ClientUpdateForm */
/* @var $form yii\widgets\ActiveForm */

$labelStyle = 'color: #73879C; font-size: 13px; font-weight: 100;';
?>
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('client', 'Create Client') ?></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->errorSummary($model, ['class' => 'alert alert-danger']) ?>

        <?= $form->field($model->client, 'c_id')->hiddenInput(['maxlength' => true])->label(false) ?>
        <?= $form->field($model->clientPassportData, 'cpd_client_id')->hiddenInput()->label(false) ?>

        <ul class="nav nav-tabs bar_tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#client" role="tab" aria-controls="client" aria-selected="false"><?= Yii::t('client', 'Create Data') ?></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="employed-data-tab" data-toggle="tab" href="#employed-data" role="tab" aria-controls="employed-data" aria-selected="false"><?= Yii::t('client', 'Employed data') ?></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="passport-data-tab" data-toggle="tab" href="#passport-data" role="tab" aria-controls="passport-data" aria-selected="false"><?= Yii::t('client', 'Passport data') ?></a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane fade active show" id="client" role="tabpanel" aria-labelledby="client-tab">
                <div class="row">
                    <div class="col-md-2">
                        <?= $form->field($model->client, 'c_name')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model->client, 'c_last_name')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model->client, 'c_birth_date')->widget(DatePicker::class, [
                            'pluginOptions' => [
                                'format' => 'dd-mm-yyyy'
                            ]
                        ]) ?>

                        <?= $form->field($model->client, 'c_idnp')->input('number') ?>
                    </div>

                    <div class="col-md-2">
                        <?= $form->field($model->client, 'c_gender')->dropDownList(Client::getGenderListName(), [
                            'prompt' => '---'
                        ]) ?>

                        <?= $form->field($model->client, 'c_civil_status')->dropDownList(Client::getCivilStatusListName(), [
                            'prompt' => '---'
                        ]) ?>

                        <?= $form->field($model->client, 'c_social_status')->dropDownList(Client::getSocialStatusListName(), [
                            'prompt' => '---'
                        ]) ?>

                        <?= $form->field($model->client, 'c_own_property')->checkbox() ?>

                        <?= $form->field($model->client, 'c_is_blocked')->checkbox() ?>
                    </div>

                    <div class="col-md-3">
                        <label for=""><?= Yii::t('client', 'Child Date Of Birth') ?></label>
                        <?= MultipleInput::widget([
                            'name' => ChildrenForm::getFormName(),
                            'columns' => [
                                [
                                    'type' => DatePicker::class,
                                    'name' => 'dateOfBirth',
                                    'options' => [
                                        'pluginOptions' => [
                                            'format' => 'dd-mm-yyyy'
                                        ],
                                    ],
                                ],
                                [
                                    'type' => MultipleInputColumn::TYPE_HIDDEN_INPUT,
                                    'name' => 'confirm'
                                ]
                            ],
                            'value' => $model->children,
                        ]) ?>

                        <label for=""><?= Yii::t('client', 'Email') ?></label>
                        <?= MultipleInput::widget([
                            'name' => 'ClientEmail',
                            'columns' => [
                                [
                                    'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                                    'name' => 'ce_email',
                                    'options' => [
                                        'type' => 'email'
                                    ]
                                ],
                                [
                                    'type' => MultipleInputColumn::TYPE_HIDDEN_INPUT,
                                    'name' => 'ce_id'
                                ]
                            ],
                            'value' => $model->clientEmail,
                        ]) ?>

                        <label for=""><?= Yii::t('client', 'Phone') ?></label>
                        <?= MultipleInput::widget([
                            'name' => 'ClientPhone',
                            'columns' => [
                                [
                                    'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                                    'name' => 'cp_phone',
                                    'options' => [
                                        'type' => 'number',
//                                        'style' => 'margin-bottom: 10px'
                                    ]
                                ],
                                [
                                    'type' => MultipleInputColumn::TYPE_HIDDEN_INPUT,
                                    'name' => 'cp_id'
                                ]
                            ],
                            'iconSource' => MultipleInput::ICONS_SOURCE_FONTAWESOME,
                            'value' => $model->clientPhone,
                        ]) ?>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="employed-data" role="tabpanel" aria-labelledby="employed-data-tab">
                <div class="row">
                    <div class="col-md-12">
                        <?= MultipleInput::widget([
                            'name' => 'ClientEmploymentData',
                            'columns' => [
                                [
                                    'type' => MultipleInputColumn::TYPE_DROPDOWN,
                                    'name' => 'ced_type',
                                    'items' => ClientEmploymentData::getTypeListName(),
                                    'title' => Html::label(Yii::t('client', 'Type'), '', ['style' => $labelStyle]),
                                    'options' => [
                                        'prompt' => '---',
                                    ],
                                ],
                                [
                                    'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                                    'name' => 'ced_work_phone',
                                    'options' => [
                                        'type' => 'number',
//                                        'style' => 'margin-bottom: 10px'
                                    ],
                                    'title' => Html::label(Yii::t('client', 'Work Phone'), '', ['style' => $labelStyle]),
                                ],
                                [
                                    'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                                    'name' => 'ced_organization_name',
                                    'title' => Html::label(Yii::t('client', 'Organization Name'), '', ['style' => $labelStyle]),
                                ],
                                [
                                    'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                                    'name' => 'ced_position',
                                    'title' => Html::label(Yii::t('client', 'Position'), '', ['style' => $labelStyle]),
                                ],
                                [
                                    'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                                    'name' => 'ced_seniority',
                                    'title' => Html::label(Yii::t('client', 'Seniority'), '', ['style' => $labelStyle]),
                                ],
                                [
                                    'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                                    'name' => 'ced_official_wages',
                                    'title' => Html::label(Yii::t('client', 'Official Wages'), '', ['style' => $labelStyle]),
                                ],
                                [
                                    'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                                    'name' => 'ced_unofficial_wages',
                                    'title' => Html::label(Yii::t('client', 'Unofficial Wages'), '', ['style' => $labelStyle]),
                                ],
                                [
                                    'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                                    'name' => 'ced_revenues_documented',
                                    'title' => Html::label(Yii::t('client', 'Revenues Documented'), '', ['style' => $labelStyle]),
                                ],
                                [
                                    'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                                    'name' => 'ced_revenues_non_documented',
                                    'title' => Html::label(Yii::t('client', 'Revenues Non Documented'), '', ['style' => $labelStyle]),
                                ],
                                [
                                    'type' => MultipleInputColumn::TYPE_HIDDEN_INPUT,
                                    'name' => 'ced_id',
                                ],
                            ],
                            'value' => $model->clientEmploymentData,
                        ]) ?>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="passport-data" role="tabpanel" aria-labelledby="passport-data-tab">
                <div class="row">
                    <div class="col-md-2">
                        <?= $form->field($model->clientPassportData, 'cpd_type')->dropDownList(ClientPassportData::getTypeListName(), ['prompt' => '---']) ?>

                        <?= $form->field($model->clientPassportData, 'cpd_series')->textInput() ?>

                        <?= $form->field($model->clientPassportData, 'cpd_number')->textInput() ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($model->clientPassportData, 'cpd_issued_by')->textInput() ?>

                        <?= $form->field($model->clientPassportData, 'cpd_issue_date')->widget(DatePicker::class, [
                            'pluginOptions' => [
                                'format' => 'dd-mm-yyyy'
                            ],
                        ]) ?>

                        <?= $form->field($model->clientPassportData, 'cpd_issue_department_code')->textInput() ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($model->clientPassportData, 'cpd_expiration_date')->widget(DatePicker::class, [
                            'pluginOptions' => [
                                'format' => 'dd-mm-yyyy'
                            ],
                        ]) ?>

                        <?= $form->field($model->clientPassportData, 'cpd_registered_address')->textInput() ?>

                        <?= $form->field($model->clientPassportData, 'cpd_residence_address')->textInput() ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>