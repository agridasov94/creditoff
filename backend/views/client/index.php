<?php

use backend\src\auth\Auth;
use common\src\entities\client\entity\Client;
use common\widgets\FlashAlert;
use kartik\export\ExportMenu;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\src\entities\client\entity\ClientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $additionalExportColumns array */

$this->title = Yii::t('backend.client', 'Clients');
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    'c_id',
    'c_name',
    'c_last_name',
    'c_birth_date:byUserDate',
    'c_idnp',
    [
        'attribute' => 'c_is_activated',
        'format' => 'booleanByLabel',
        'filter' => [0 => Yii::t('backend', 'No'), 1 => Yii::t('frontend', 'Yes')],
    ],
    [
        'attribute' => 'c_gender',
        'value' => static function (Client $model) {
            return $model->c_gender ? Client::getGenderName($model->c_gender) : null;
        }
    ],
    [
        "attribute" => 'c_civil_status',
        'value' => static function (Client $model) {
            return $model->c_civil_status ? Client::getCivilStatusName($model->c_civil_status) : null;
        }
    ],
    [
        'attribute' => 'c_social_status',
        'value' => static function (Client $model) {
            return $model->c_social_status ? Client::getSocialStatusName($model->c_social_status) : null;
        }
    ],
    'c_created_user_id:username',
    'c_updated_user_id:username',
    'c_created_dt:byUserDateTime',
    'c_updated_dt:byUserDateTime',

    [
        'class' => 'yii\grid\ActionColumn',
        'buttons' => [
            'switch' => static function ($url, Client $model, $key) {
                return Html::a(
                    '<span class="fa fa-sign-in text-warning"></span>',
                    ['client/switch', 'id' => $model->c_id],
                    ['title' => Yii::t('users', 'switch Client'), 'data' => [
                        'confirm' => Yii::t('users', 'Are you sure you want to login to the client profile?'),
                    ],
                    ]
                );
            },
            'softDelete' => static function ($url, Client $model, $key) {
                return Html::a(
                    '<span class="fa fa-eraser"></span>',
                    ['client/soft-delete', 'id' => $model->c_id],
                    ['title' => Yii::t('users', 'Soft Delete Client'), 'data' => [
                        'confirm' => Yii::t('users', 'Are you sure you want to soft delete client profile?'),
                    ],
                    ]
                );
            },
            'restoreData' => static function ($url, Client $model, $key) {
                return Html::a(
                    '<span class="fa fa-undo"></span>',
                    ['client/restore-data', 'id' => $model->c_id],
                    ['title' => Yii::t('users', 'Restore Client Data'), 'data' => [
                        'confirm' => Yii::t('users', 'Are you sure you want to restore client data?'),
                    ],
                    ]
                );
            },
        ],
        'visibleButtons' => [
            'switch' => static function (Client $model, $key, $index) {
                return Auth::can('/client/switch');
            },
            'softDelete' => static function (Client $model, $key, $index) {
                return empty($model->c_deleted_dt);
            },
            'restoreData' => static function (Client $model, $key, $index) {
                return !empty($model->c_deleted_dt);
            }
        ],
        'template' => '{view} <br> {update} <br> {switch} <br> <hr style="margin: 5px 0"> {delete} <br> {softDelete} {restoreData}'
    ],
];
?>
<div class="client-index">

    <h3><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a(Yii::t('backend.client', 'Create Client'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= FlashAlert::widget() ?>

    <?php Pjax::begin(); ?>

    <?php if(Yii::$app->request->isPjax): ?>
        <script>
            $('.export-wrapper .dropdown-menu a').each(function (i, e) {
                $(document).off('click.exportmenu', '#' + $(e).attr('id'));
            });
        </script>
    <?php endif; ?>
    <?= ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => ArrayHelper::merge($gridColumns, $additionalExportColumns),
        'dropdownOptions' => [
            'label' => 'Export All',
            'class' => 'btn btn-outline-secondary btn-default'
        ],
        'container' => [
            'class' => 'export-wrapper'
        ],
        'showConfirmAlert' => false,
        'filename' => Yii::t('export', 'Client') . '_' . date('Y-m-d_H:i:s'),
    ]) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns
    ]); ?>

    <?php Pjax::end(); ?>

</div>
