<?php

use backend\src\useCase\clientUpdate\ClientUpdateForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model ClientUpdateForm */

$this->title = Yii::t('backend.client', 'Update Client: {name}', [
    'name' => $model->client->getFullName(),
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend.client', 'Clients'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->client->c_id, 'url' => ['view', 'id' => $model->client->c_id]];
$this->params['breadcrumbs'][] = Yii::t('backend.client', 'Update');
?>
<div class="client-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form_update', [
        'model' => $model,
    ]) ?>

</div>
