<?php

use backend\src\helpers\ViewHelper;
use common\src\entities\client\employmentData\entity\ClientEmploymentData;
use common\src\entities\client\entity\Client;
use common\src\entities\client\passportData\entity\ClientPassportData;
use yii\helpers\Html;
use yii\helpers\VarDumper;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\src\entities\client\entity\Client */

$this->title = $model->getFullName();
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend.client', 'Clients'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="client-view">

    <h3><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a(Yii::t('backend.client', 'Update'), ['update', 'id' => $model->c_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('backend.client', 'Delete'), ['delete', 'id' => $model->c_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend.client', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="row">
        <div class="col-md-3">
            <h4><?= Yii::t('client', 'Client Info') ?></h4>
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'c_id',
                    'c_name',
                    'c_last_name',
                    'c_birth_date:byUserDate',
                    'c_idnp',
                    [
                        'attribute' => 'c_gender',
                        'value' => static function (Client $model) {
                            return $model->c_gender ? Client::getGenderName($model->c_gender) : null;
                        }
                    ],
                    [
                        "attribute" => 'c_civil_status',
                        'value' => static function (Client $model) {
                            return $model->c_civil_status ? Client::getCivilStatusName($model->c_civil_status) : null;
                        }
                    ],
                    [
                        'attribute' => 'c_social_status',
                        'value' => static function (Client $model) {
                            return $model->c_social_status ? Client::getSocialStatusName($model->c_social_status) : null;
                        }
                    ],
                    [
                        'attribute' => 'c_own_property',
                        'format' => 'booleanByLabel',
                    ],
                    [
                        'attribute' => 'c_is_blocked',
                        'format' => 'booleanByLabel',
                    ],
                    [
                        'attribute' => 'c_is_activated',
                        'format' => 'booleanByLabel',
                    ],
                    [
                        'attribute' => 'c_children',
                        'value' => static function (Client $model) {
                            return $model->c_children ? ViewHelper::displayChildrenData($model->c_children) : null;
                        },
                        'format' => 'raw'
                    ],
                    'c_created_user_id:username',
                    'c_updated_user_id:username',
                    'c_created_dt:byUserDateTime',
                    'c_updated_dt:byUserDateTime',
                    'c_deleted_dt:byUserDateTime',
                ],
            ]) ?>
        </div>

        <div class="col-md-3">
            <h4><?= Yii::t('client', 'Employment Data') ?></h4>
            <?php foreach ($model->clientEmploymentData as $employmentData): ?>
                <div>
                    <?= DetailView::widget([
                        'model' => $employmentData,
                        'attributes' => [
                            'ced_id',
                            [
                                'attribute' => 'ced_type',
                                'value' => static function (ClientEmploymentData $model) {
                                    return $model->ced_type ? ClientEmploymentData::getTypeName($model->ced_type) : null;
                                }
                            ],
                            'ced_work_phone',
                            'ced_organization_name',
                            'ced_position',
                            'ced_seniority',
                            'ced_official_wages',
                            'ced_unofficial_wages',
                            'ced_revenues_documented',
                            'ced_revenues_non_documented',
                            'ced_created_user_id:username',
                            'ced_updated_user_id:username',
                            'ced_created_dt:byUserDateTime',
                            'ced_updated_dt:byUserDateTime',
                        ]
                    ]) ?>
                </div>
            <?php endforeach; ?>
        </div>

        <div class="col-md-3">
            <h4><?= Yii::t('client', 'Passport Data') ?></h4>
            <div>
                <?= DetailView::widget([
                    'model' => $model->clientPassportData,
                    'attributes' => [
                        [
                            'attribute' => 'cpd_type',
                            'value' => static function (ClientPassportData $model) {
                                return $model->cpd_type ? ClientPassportData::getTypeName($model->cpd_type) : null;
                            }
                        ],
                        'cpd_series',
                        'cpd_number',
                        'cpd_issued_by',
                        'cpd_issue_date:byUserDate',
                        'cpd_issue_department_code',
                        'cpd_expiration_date:byUserDate',
                        'cpd_registered_address',
                        'cpd_residence_address',
                        'cpd_created_user_id:username',
                        'cpd_updated_user_id:username',
                        'cpd_created_dt:byUserDateTime',
                        'cpd_updated_dt:byUserDateTime',
                    ]
                ]) ?>
            </div>
        </div>

        <div class="col-md-3">
            <?php if ($model->clientEmails): ?>
                <div>
                    <h4><?= Yii::t('client', 'Email') ?></h4>
                    <?php foreach($model->clientEmails as $clientEmail): ?>
                        <?= DetailView::widget([
                            'model' => $clientEmail,
                            'attributes' => [
                                'ce_id',
                                'ce_email:email',
                                'ce_created_user_id:username',
                                'ce_updated_user_id:username',
                                'ce_created_dt:byUserDateTime',
                                'ce_updated_dt:byUserDateTime',
                            ]
                        ]) ?>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
            <?php if ($model->clientPhones): ?>
                <div>
                    <h4><?= Yii::t('client', 'Phone') ?></h4>
                    <?php foreach($model->clientPhones as $clientPhone): ?>
                        <?= DetailView::widget([
                            'model' => $clientPhone,
                            'attributes' => [
                                'cp_id',
                                'cp_phone',
                                'cp_created_user_id:username',
                                'cp_updated_user_id:username',
                                'cp_created_dt:byUserDateTime',
                                'cp_updated_dt:byUserDateTime',
                            ]
                        ]) ?>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
