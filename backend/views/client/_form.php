<?php

use backend\src\useCase\clientCreate\ChildrenForm;
use backend\src\useCase\clientCreate\ClientEmailForm;
use backend\src\useCase\clientCreate\ClientEmploymentDataForm;
use backend\src\useCase\clientCreate\ClientPhoneForm;
use borales\extensions\phoneInput\PhoneInput;
use common\src\entities\client\employmentData\entity\ClientEmploymentData;
use common\src\entities\client\entity\Client;
use common\src\entities\client\passportData\entity\ClientPassportData;
use kartik\date\DatePicker;
use unclead\multipleinput\MultipleInput;
use unclead\multipleinput\MultipleInputColumn;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \backend\src\useCase\clientCreate\ClientCreateForm */
/* @var $form yii\widgets\ActiveForm */

$labelStyle = 'color: #73879C; font-size: 13px; font-weight: 100;';
?>

<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('client', 'Create Client') ?></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->errorSummary($model, ['class' => 'alert alert-danger']) ?>

        <ul class="nav nav-tabs bar_tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#client" role="tab" aria-controls="client" aria-selected="false"><?= Yii::t('client', 'Create Data') ?></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="employed-data-tab" data-toggle="tab" href="#employed-data" role="tab" aria-controls="employed-data" aria-selected="false"><?= Yii::t('client', 'Employed data') ?></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="passport-data-tab" data-toggle="tab" href="#passport-data" role="tab" aria-controls="passport-data" aria-selected="false"><?= Yii::t('client', 'Passport data') ?></a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane fade active show" id="client" role="tabpanel" aria-labelledby="client-tab">
                <div class="row">
                    <div class="col-md-2">
                        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'lastName')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'dateOfBirth')->widget(DatePicker::class, [
                            'model' => $model,
                            'name' => 'dateOfBirth',
                            'attribute' => 'dateOfBirth',
                            'pluginOptions' => [
                                'format' => 'dd-mm-yyyy'
                            ]
                        ]) ?>

                        <?= $form->field($model, 'idnp')->input('number') ?>
                    </div>

                    <div class="col-md-2">
                        <?= $form->field($model, 'gender')->dropDownList(Client::getGenderListName(), [
                            'prompt' => '---'
                        ]) ?>

                        <?= $form->field($model, 'civilStatus')->dropDownList(Client::getCivilStatusListName(), [
                            'prompt' => '---'
                        ]) ?>

                        <?= $form->field($model, 'socialStatus')->dropDownList(Client::getSocialStatusListName(), [
                            'prompt' => '---'
                        ]) ?>

                        <?= $form->field($model, 'ownProperty')->checkbox() ?>

                        <?= $form->field($model, 'isBlocked')->checkbox() ?>
                    </div>

                    <div class="col-md-3">
                        <label for=""><?= Yii::t('client', 'Child Date Of Birth') ?></label>
                        <?= MultipleInput::widget([
                            'name' => ChildrenForm::getFormName(),
                            'columns' => [
                                [
                                    'type' => DatePicker::class,
                                    'name' => 'dateOfBirth',
                                    'options' => [
                                        'pluginOptions' => [
                                            'format' => 'dd-mm-yyyy'
                                        ],
                                    ],
                                ],
                                [
                                    'type' => MultipleInputColumn::TYPE_HIDDEN_INPUT,
                                    'name' => 'hidden'
                                ]
                            ],
                            'value' => Yii::$app->request->post()[ChildrenForm::getFormName()] ?? [],
                        ]) ?>

                        <label for=""><?= Yii::t('client', 'Email') ?></label>
                        <?= MultipleInput::widget([
                            'name' => ClientEmailForm::getFormName(),
                            'columns' => [
                                [
                                    'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                                    'name' => 'email',
                                    'options' => [
                                        'type' => 'email'
                                    ]
                                ],
                                [
                                    'type' => MultipleInputColumn::TYPE_HIDDEN_INPUT,
                                    'name' => 'hidden'
                                ]
                            ],
                            'value' => Yii::$app->request->post()[ClientEmailForm::getFormName()] ?? [],
                        ]) ?>

                        <label for=""><?= Yii::t('client', 'Phone') ?></label>
                        <?= MultipleInput::widget([
                            'name' => ClientPhoneForm::getFormName(),
                            'columns' => [
                                [
                                    'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                                    'name' => 'phone',
                                    'options' => [
                                        'type' => 'number',
//                                        'style' => 'margin-bottom: 10px'
                                    ]
                                ],
                                [
                                    'type' => MultipleInputColumn::TYPE_HIDDEN_INPUT,
                                    'name' => 'hidden'
                                ]
                            ],
                            'iconSource' => MultipleInput::ICONS_SOURCE_FONTAWESOME,
                            'value' => Yii::$app->request->post()[ClientPhoneForm::getFormName()] ?? [],
                        ]) ?>


                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="employed-data" role="tabpanel" aria-labelledby="employed-data-tab">
                <div class="row">
                    <div class="col-md-12">
                        <?= MultipleInput::widget([
                            'name' => ClientEmploymentDataForm::getFormName(),
                            'columns' => [
                                [
                                    'type' => MultipleInputColumn::TYPE_DROPDOWN,
                                    'name' => 'type',
                                    'items' => ClientEmploymentData::getTypeListName(),
                                    'title' => Html::label(Yii::t('client', 'Type'), '', ['style' => $labelStyle]),
                                    'options' => [
                                        'prompt' => '---',
                                    ],
                                ],
                                [
                                    'type' => PhoneInput::class,
                                    'name' => 'workPhone',
                                    'options' => [
                                        'jsOptions' => [
                                            'nationalMode' => false,
                                            'preferredCountries' => ['md'],
                                            'customContainer' => 'intl-tel-input',
                                            'onlyCountries' => ['md']
                                        ],
                                        'options' => [
                                            'onkeydown' => '
                                                return !validationField.validate(event);
                                            ',
                                            'onkeyup' => '
                                                var value = $(this).val();
                                                $(this).val(value.replace(/[^0-9\+]+/g, ""));
                                            '
                                        ]
                                    ],
                                    'title' => Html::label(Yii::t('client', 'Work Phone'), '', ['style' => $labelStyle]),
                                ],
                                [
                                    'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                                    'name' => 'organizationName',
                                    'title' => Html::label(Yii::t('client', 'Organization Name'), '', ['style' => $labelStyle]),
                                ],
                                [
                                    'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                                    'name' => 'position',
                                    'title' => Html::label(Yii::t('client', 'Position'), '', ['style' => $labelStyle]),
                                ],
                                [
                                    'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                                    'name' => 'seniority',
                                    'title' => Html::label(Yii::t('client', 'Seniority'), '', ['style' => $labelStyle]),
                                ],
                                [
                                    'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                                    'name' => 'officialWages',
                                    'title' => Html::label(Yii::t('client', 'Official Wages'), '', ['style' => $labelStyle]),
                                ],
                                [
                                    'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                                    'name' => 'unofficialWages',
                                    'title' => Html::label(Yii::t('client', 'Unofficial Wages'), '', ['style' => $labelStyle]),
                                ],
                                [
                                    'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                                    'name' => 'revenuesDocumented',
                                    'title' => Html::label(Yii::t('client', 'Revenues Documented'), '', ['style' => $labelStyle]),
                                ],
                                [
                                    'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                                    'name' => 'revenuesNonDocumented',
                                    'title' => Html::label(Yii::t('client', 'Revenues Non Documented'), '', ['style' => $labelStyle]),
                                ],
                            ],
                            'value' => Yii::$app->request->post()[ClientEmploymentDataForm::getFormName()] ?? [],
                        ]) ?>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="passport-data" role="tabpanel" aria-labelledby="passport-data-tab">
                <div class="row">
                    <div class="col-md-2">
                        <?= $form->field($model->passportData, 'type')->dropDownList(ClientPassportData::getTypeListName(), ['prompt' => '---']) ?>

                        <?= $form->field($model->passportData, 'series')->textInput() ?>

                        <?= $form->field($model->passportData, 'number')->textInput() ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($model->passportData, 'issuedBy')->textInput() ?>

                        <?= $form->field($model->passportData, 'issueDate')->widget(DatePicker::class, [
                            'pluginOptions' => [
                                'format' => 'dd-mm-yyyy'
                            ],
                        ]) ?>

                        <?= $form->field($model->passportData, 'issueDepartmentCode')->textInput() ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($model->passportData, 'expirationDate')->widget(DatePicker::class, [
                            'pluginOptions' => [
                                'format' => 'dd-mm-yyyy'
                            ],
                        ]) ?>

                        <?= $form->field($model->passportData, 'registeredAddress')->textInput() ?>

                        <?= $form->field($model->passportData, 'residenceAddress')->textInput() ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>