<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \backend\src\useCase\clientCreate\ClientCreateForm */

$this->title = Yii::t('backend.client', 'Create Client');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend.client', 'Clients'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
