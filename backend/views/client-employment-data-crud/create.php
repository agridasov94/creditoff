<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\src\entities\client\employmentData\entity\ClientEmploymentData */

$this->title = Yii::t('backend.client', 'Create Client Employment Data');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend.client', 'Client Employment Datas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-employment-data-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
