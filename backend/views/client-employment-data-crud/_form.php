<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\src\entities\client\employmentData\entity\ClientEmploymentData */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="row">
    <div class="col-md-4">
        <div class="client-employment-data-form">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'ced_client_id')->textInput() ?>

            <?= $form->field($model, 'ced_type')->textInput() ?>

            <?= $form->field($model, 'ced_work_phone')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'ced_organization_name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'ced_position')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'ced_seniority')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'ced_official_wages')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'ced_unofficial_wages')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'ced_revenues_documented')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'ced_revenues_non_documented')->textInput(['maxlength' => true]) ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('backend.client', 'Save'), ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
