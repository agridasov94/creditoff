<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\src\entities\client\employmentData\entity\ClientEmploymentData */

$this->title = Yii::t('backend.client', 'Update Client Employment Data: {name}', [
    'name' => $model->ced_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend.client', 'Client Employment Datas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ced_id, 'url' => ['view', 'id' => $model->ced_id]];
$this->params['breadcrumbs'][] = Yii::t('backend.client', 'Update');
?>
<div class="client-employment-data-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
