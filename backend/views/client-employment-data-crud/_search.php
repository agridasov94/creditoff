<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\src\entities\client\employmentData\entity\ClientEmploymentDataSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="client-employment-data-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'ced_id') ?>

    <?= $form->field($model, 'ced_client_id') ?>

    <?= $form->field($model, 'ced_type') ?>

    <?= $form->field($model, 'ced_work_phone') ?>

    <?= $form->field($model, 'ced_organization_name') ?>

    <?php // echo $form->field($model, 'ced_position') ?>

    <?php // echo $form->field($model, 'ced_seniority') ?>

    <?php // echo $form->field($model, 'ced_official_wages') ?>

    <?php // echo $form->field($model, 'ced_unofficial_wages') ?>

    <?php // echo $form->field($model, 'ced_revenues_documented') ?>

    <?php // echo $form->field($model, 'ced_revenues_non_documented') ?>

    <?php // echo $form->field($model, 'ced_created_user_id') ?>

    <?php // echo $form->field($model, 'ced_updated_user_id') ?>

    <?php // echo $form->field($model, 'ced_created_dt') ?>

    <?php // echo $form->field($model, 'ced_updated_dt') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend.client', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('backend.client', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
