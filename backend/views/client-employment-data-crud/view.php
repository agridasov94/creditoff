<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\src\entities\client\employmentData\entity\ClientEmploymentData */

$this->title = $model->ced_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend.client', 'Client Employment Datas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="client-employment-data-view">

    <h3><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a(Yii::t('backend.client', 'Update'), ['update', 'id' => $model->ced_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('backend.client', 'Delete'), ['delete', 'id' => $model->ced_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend.client', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ced_id',
            'ced_client_id',
            'ced_type',
            'ced_work_phone',
            'ced_organization_name',
            'ced_position',
            'ced_seniority',
            'ced_official_wages',
            'ced_unofficial_wages',
            'ced_revenues_documented',
            'ced_revenues_non_documented',
            'ced_created_user_id',
            'ced_updated_user_id',
            'ced_created_dt',
            'ced_updated_dt',
        ],
    ]) ?>

</div>
