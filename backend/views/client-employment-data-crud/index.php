<?php

use backend\components\grid\DateTimeColumn;
use backend\components\grid\UserSelect2Column;
use common\src\entities\client\employmentData\entity\ClientEmploymentData;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\src\entities\client\employmentData\entity\ClientEmploymentDataSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend.client', 'Client Employment Datas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-employment-data-index">

    <h3><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a(Yii::t('backend.client', 'Create Client Employment Data'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'ced_id',
            [
                'attribute' => 'ced_client_id',
                'value' => static function (ClientEmploymentData $model) {
                    return $model->client->c_name . ' ' . $model->client->c_last_name;
                }
            ],
            [
                'attribute' => 'ced_type',
                'value' => static function (ClientEmploymentData $model) {
                    return $model->ced_type ? ClientEmploymentData::getTypeName($model->ced_type) : null;
                }
            ],
            'ced_work_phone',
            'ced_organization_name',
            'ced_position',
            'ced_seniority',
            'ced_official_wages',
            'ced_unofficial_wages',
            'ced_revenues_documented',
            'ced_revenues_non_documented',
            [
                'class' => DateTimeColumn::class,
                'attribute' => 'ced_created_dt',
                'format' => 'byUserDateTime'
            ],
            [
                'class' => DateTimeColumn::class,
                'attribute' => 'ced_updated_dt',
                'format' => 'byUserDateTime'
            ],
            [
                'class' => UserSelect2Column::class,
                'attribute' => 'ced_created_user_id',
                'relation' => 'createdUser',
                'placeholder' => '',
            ],
            [
                'class' => UserSelect2Column::class,
                'attribute' => 'ced_updated_user_id',
                'relation' => 'updatedUser',
                'placeholder' => '',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
