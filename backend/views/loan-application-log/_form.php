<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplication\log\entity\LoanApplicationLog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loan-application-log-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'lal_loan_application_id')->textInput() ?>

    <?= $form->field($model, 'lal_owner_id')->textInput() ?>

    <?= $form->field($model, 'lal_status_before')->textInput() ?>

    <?= $form->field($model, 'lal_status_after')->textInput() ?>

    <?= $form->field($model, 'lal_start_dt')->textInput() ?>

    <?= $form->field($model, 'lal_end_dt')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('loan_application', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
