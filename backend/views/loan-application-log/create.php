<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplication\log\entity\LoanApplicationLog */

$this->title = Yii::t('loan_application', 'Create Loan Application Log');
$this->params['breadcrumbs'][] = ['label' => Yii::t('loan_application', 'Loan Application Logs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="loan-application-log-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
