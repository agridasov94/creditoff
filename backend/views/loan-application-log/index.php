<?php

use backend\components\grid\DateTimeColumn;
use backend\components\grid\UserSelect2Column;
use backend\src\entities\partnerCompany\entity\PartnerCompanyQuery;
use common\src\entities\loanApplication\entity\LoanApplicationStatus;
use common\src\entities\loanApplication\log\entity\LoanApplicationLog;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\src\entities\loanApplication\log\entity\LoanApplicationLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('loan_application', 'Loan Application Logs');
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    [
        'attribute' => 'lal_id',
        'options' => [
            'width' => '100px'
        ]
    ],
    'lal_loan_application_id',
    [
        'attribute' => 'number',
        'value' => static function (LoanApplicationLog $model) {
            return $model->loanApplication->la_number;
        }
    ],
    [
        'class' => UserSelect2Column::class,
        'attribute' => 'lal_owner_id',
        'relation' => 'owner',
        'placeholder' => '',
    ],
    [
        'attribute' => 'companyId',
        'value' => static function (LoanApplicationLog $model) {
            return $model->owner->partnerCompany->pc_name ?? null;
        },
        'filter' => PartnerCompanyQuery::getList(),
        'label' => Yii::t('company', 'Company')
    ],
    [
        'attribute' => 'lal_status_before',
        'value' => static function (LoanApplicationLog $model) {
            return $model->lal_status_before ? LoanApplicationStatus::getStatusLabel($model->lal_status_before) : null;
        },
        'filter' => LoanApplicationStatus::getStatusNameList(),
        'format' => 'raw'
    ],
    [
        'attribute' => 'lal_status_after',
        'value' => static function (LoanApplicationLog $model) {
            return $model->lal_status_after ? LoanApplicationStatus::getStatusLabel($model->lal_status_after) : null;
        },
        'filter' => LoanApplicationStatus::getStatusNameList(),
        'format' => 'raw'
    ],
    [
        'class' => DateTimeColumn::class,
        'attribute' => 'lal_start_dt',
        'format' => 'byUserDateTime'
    ],
    [
        'class' => DateTimeColumn::class,
        'attribute' => 'lal_end_dt',
        'format' => 'byUserDateTime'
    ],

    ['class' => 'yii\grid\ActionColumn'],
];
?>
<div class="loan-application-log-index">

    <h1><?= Html::encode($this->title) ?></h1>

<!--    <p>-->
        <?php // Html::a(Yii::t('loan_application', 'Create Loan Application Log'), ['create'], ['class' => 'btn btn-success']) ?>
<!--    </p>-->

    <?php Pjax::begin(); ?>

    <?php if(Yii::$app->request->isPjax): ?>
        <script>
            $('.export-wrapper .dropdown-menu a').each(function (i, e) {
                $(document).off('click.exportmenu', '#' + $(e).attr('id'));
            });
        </script>
    <?php endif; ?>
    <?= \kartik\export\ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'dropdownOptions' => [
            'label' => 'Export All',
            'class' => 'btn btn-outline-secondary btn-default'
        ],
        'container' => [
            'class' => 'export-wrapper'
        ],
        'showConfirmAlert' => false,
        'filename' => Yii::t('export', 'Loan_application_logs') . '_' . date('Y-m-d_H:i:s')
    ]) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns
    ]); ?>

    <?php Pjax::end(); ?>

</div>
