<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplication\log\entity\LoanApplicationLog */

$this->title = Yii::t('loan_application', 'Update Loan Application Log: {name}', [
    'name' => $model->lal_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('loan_application', 'Loan Application Logs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->lal_id, 'url' => ['view', 'id' => $model->lal_id]];
$this->params['breadcrumbs'][] = Yii::t('loan_application', 'Update');
?>
<div class="loan-application-log-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
