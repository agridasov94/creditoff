<?php

use common\src\entities\loanApplication\entity\LoanApplicationStatus;
use common\src\entities\loanApplication\log\entity\LoanApplicationLog;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplication\log\entity\LoanApplicationLog */

$this->title = $model->lal_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('loan_application', 'Loan Application Logs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="loan-application-log-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('loan_application', 'Update'), ['update', 'id' => $model->lal_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('loan_application', 'Delete'), ['delete', 'id' => $model->lal_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('loan_application', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'lal_id',
            'lal_loan_application_id',
            'lal_owner_id:username',
            [
                'attribute' => 'lal_status_before',
                'value' => static function (LoanApplicationLog $model) {
                    return LoanApplicationStatus::getStatusLabel($model->lal_status_before);
                },
                'filter' => LoanApplicationStatus::getStatusNameList()
            ],
            [
                'attribute' => 'lal_status_before',
                'value' => static function (LoanApplicationLog $model) {
                    return LoanApplicationStatus::getStatusLabel($model->lal_status_before);
                },
                'filter' => LoanApplicationStatus::getStatusNameList()
            ],
            'lal_start_dt:byUserDateTime',
            'lal_end_dt:byUserDateTime',
        ],
    ]) ?>

</div>
