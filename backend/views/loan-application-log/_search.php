<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplication\log\entity\LoanApplicationLogSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loan-application-log-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'lal_id') ?>

    <?= $form->field($model, 'lal_loan_application_id') ?>

    <?= $form->field($model, 'lal_owner_id') ?>

    <?= $form->field($model, 'lal_status_before') ?>

    <?= $form->field($model, 'lal_status_after') ?>

    <?php // echo $form->field($model, 'lal_start_dt') ?>

    <?php // echo $form->field($model, 'lal_end_dt') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('loan_application', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('loan_application', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
