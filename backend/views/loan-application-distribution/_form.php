<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplicationDistribution\entity\LoanApplicationDistribution */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loan-application-distribution-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'lad_product_id')->textInput() ?>

    <?= $form->field($model, 'lad_user_id')->textInput() ?>

    <?= $form->field($model, 'lad_queue_depth')->textInput() ?>

    <?= $form->field($model, 'lad_total_display_loan_application')->textInput() ?>

    <?= $form->field($model, 'lad_created_dt')->textInput() ?>

    <?= $form->field($model, 'lad_created_user_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('loan_application', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
