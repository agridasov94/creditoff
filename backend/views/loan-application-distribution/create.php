<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplicationDistribution\entity\LoanApplicationDistribution */

$this->title = Yii::t('loan_application', 'Create Loan Application Distribution');
$this->params['breadcrumbs'][] = ['label' => Yii::t('loan_application', 'Loan Application Distributions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="loan-application-distribution-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
