<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplicationDistribution\entity\LoanApplicationDistributionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loan-application-distribution-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'lad_id') ?>

    <?= $form->field($model, 'lad_product_id') ?>

    <?= $form->field($model, 'lad_user_id') ?>

    <?= $form->field($model, 'lad_queue_depth') ?>

    <?= $form->field($model, 'lad_total_display_loan_application') ?>

    <?php // echo $form->field($model, 'lad_created_dt') ?>

    <?php // echo $form->field($model, 'lad_created_user_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('loan_application', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('loan_application', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
