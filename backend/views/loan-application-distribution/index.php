<?php

use backend\components\grid\DateTimeColumn;
use backend\components\grid\UserSelect2Column;
use backend\src\entities\partnerCompany\entity\PartnerCompanyQuery;
use common\src\entities\loanApplicationDistribution\entity\LoanApplicationDistribution;
use common\src\entities\product\entity\ProductQuery;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\src\entities\loanApplicationDistribution\entity\LoanApplicationDistributionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('loan_application', 'Loan Application Distributions');
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    [
        'attribute' => 'lad_id',
        'options' => [
            'style' => 'width: 100px;'
        ]
    ],
    [
        'attribute' => 'lad_product_id',
        'value' => static function (LoanApplicationDistribution $model) {
            return $model->product->p_name;
        },
        'filter' => ProductQuery::getList()
    ],
    [
        'class' => UserSelect2Column::class,
        'attribute' => 'lad_user_id',
        'relation' => 'user',
        'placeholder' => '',
        'options' => [
            'style' => 'width: 200px;'
        ]
    ],
    [
        'attribute' => 'companyId',
        'value' => static function (LoanApplicationDistribution $model) {
            return $model->user->partnerCompany->pc_name ?? null;
        },
        'filter' => PartnerCompanyQuery::getList(),
        'label' => Yii::t('company', 'Company')
    ],
    'lad_queue_depth',
    'lad_total_display_loan_application',
    [
        'class' => DateTimeColumn::class,
        'attribute' => 'lad_created_dt',
        'format' => 'byUserDate'
    ],
    [
        'class' => UserSelect2Column::class,
        'attribute' => 'lad_created_user_id',
        'relation' => 'createdUser',
        'placeholder' => '',
    ],


    ['class' => 'yii\grid\ActionColumn'],
];
?>
<div class="loan-application-distribution-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('loan_application', 'Create Loan Application Distribution'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>

    <?php if(Yii::$app->request->isPjax): ?>
        <script>
            $('.export-wrapper .dropdown-menu a').each(function (i, e) {
                $(document).off('click.exportmenu', '#' + $(e).attr('id'));
            });
        </script>
    <?php endif; ?>
    <?= \kartik\export\ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'dropdownOptions' => [
            'label' => 'Export All',
            'class' => 'btn btn-outline-secondary btn-default'
        ],
        'container' => [
            'class' => 'export-wrapper'
        ],
        'showConfirmAlert' => false,
        'filename' => Yii::t('export', 'Loan_application_distribution') . '_' . date('Y-m-d_H:i:s')
    ]) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns
    ]); ?>

    <?php Pjax::end(); ?>

</div>
