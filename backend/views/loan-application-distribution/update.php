<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplicationDistribution\entity\LoanApplicationDistribution */

$this->title = Yii::t('loan_application', 'Update Loan Application Distribution: {name}', [
    'name' => $model->lad_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('loan_application', 'Loan Application Distributions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->lad_id, 'url' => ['view', 'id' => $model->lad_id]];
$this->params['breadcrumbs'][] = Yii::t('loan_application', 'Update');
?>
<div class="loan-application-distribution-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
