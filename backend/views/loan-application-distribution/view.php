<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\src\entities\loanApplicationDistribution\entity\LoanApplicationDistribution */

$this->title = $model->lad_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('loan_application', 'Loan Application Distributions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="loan-application-distribution-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('loan_application', 'Update'), ['update', 'id' => $model->lad_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('loan_application', 'Delete'), ['delete', 'id' => $model->lad_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('loan_application', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'lad_id',
            'lad_product_id',
            'lad_user_id',
            'lad_queue_depth',
            'lad_total_display_loan_application',
            'lad_created_dt',
            'lad_created_user_id',
        ],
    ]) ?>

</div>
