<?php
/** @var $menuItems array */

use yii\helpers\Html;
?>

<div class="col-md-3 left_col">
    <div class="left_col scroll-view">

        <div class="navbar nav_title" style="border: 0;">
            <?= Html::a('<i class="fa fa-paw"></i> ' . Html::tag('span', Yii::t('backend', 'Creditoff')), ['/'], [
                'class' => 'site_title'
            ]) ?>
        </div>

        <div class="clearfix"></div>

        <div class="profile clearfix">
            <div class="profile_pic">
                <img src="/images/pngwing.com.png" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <span><?= Yii::t('backend', 'Welcome') ?>,</span>
                <h2><?= Yii::$app->user->getIdentity()->username ?></h2>
            </div>
        </div>

        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <?= \backend\widgets\navbar\Menu::widget([
                    'items' => $menuItems,
                    'encodeLabels' => false,
                    'activateParents' => true,
                    'linkTemplate' => '<a href="{url}" {attributes} data-pjax="0">{icon}<span>{label}</span>{badge}</a>'
                ]); ?>
            </div>
        </div>

        <div class="sidebar-footer hidden-small">
            <?= Html::a('<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>', ['/settings'], [
                'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => Yii::t('backend', 'System Settings')
            ]) ?>

            <?= Html::a('<i class="fa fa-bug"></i>', ['/log'], [
                'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => Yii::t('backend', 'System Logs')
            ]) ?>

            <?= Html::a('<span class="glyphicon glyphicon-off" aria-hidden="true"></span>', ['/site/logout'], [
                'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => Yii::t('backend', 'Logout')
            ]) ?>
        </div>
    </div>
</div>