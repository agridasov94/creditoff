<?php

namespace backend\widgets\navbar;

use Yii;
use yii\base\Widget;
use yii\helpers\Url;

class NavBarMenu extends Widget
{
    public function run()
    {
        $menuItems = [];

        $menuItems[] = [
            'label' => Yii::t('backend', 'Dashboard'),
            'url' => Url::to(['/']),
            'icon' => 'home'
        ];
        $menuItems[] = [
            'label' => Yii::t('partner', 'Partners'),
            'url' => 'javascript:',
            'icon' => 'building',
            'items' => [
                ['label' => Yii::t('partner', 'Companies'), 'url' => Url::to(['/partner-company/index'])],
                ['label' => Yii::t('partner', 'Manage Amount'), 'url' => Url::to(['/partner-company/manage-amount'])],
                ['label' => Yii::t('partner', 'Amount/Bonus History CRUD'), 'url' => Url::to(['/partner-company-amount-history-crud/index'])],
                ['label' => Yii::t('partner', 'Position'), 'url' => Url::to(['/partner-company-position/index'])],
            ]
        ];
        $menuItems[] = [
            'label' => Yii::t('backend.client', 'Clients'),
            'url' => Url::to(['/client/index']),
            'icon' => 'users',
        ];
        $menuItems[] = [
            'label' => Yii::t('backend.client', 'Clients CRUD'),
            'url' => 'javascript:',
            'icon' => 'users',
            'items' => [
                ['label' => Yii::t('backend', 'Client Employment Data'), 'url' => Url::to(['/client-employment-data-crud/index'])],
                ['label' => Yii::t('backend', 'Client Passport Data'), 'url' => Url::to(['/client-passport-data-crud/index'])],
                ['label' => Yii::t('backend', 'Client Email'), 'url' => Url::to(['/client-email-crud/index'])],
                ['label' => Yii::t('backend', 'Client Phone'), 'url' => Url::to(['/client-phone-crud/index'])],
                ['label' => Yii::t('backend', 'Client Notification'), 'url' => Url::to(['/client-notification/index'])],
            ]
        ];
        $menuItems[] = [
            'label' => Yii::t('product', 'Products',),
//            'url' => Url::to(['/product/index']),
            'url' => 'javascript:',
            'icon' => 'list-alt',
            'items' => [
                ['label' => Yii::t('product', 'Products List'), 'url' => Url::to(['/product/index'])],
                ['label' => Yii::t('product', 'Manage My Products Prices'), 'url' => Url::to(['/product/manage-my-product-prices'])],
                ['label' => Yii::t('product', 'Users Products Prices'), 'url' => Url::to(['/product/users-product-prices'])],
                ['label' => Yii::t('product', 'User Product Price Log'), 'url' => Url::to(['/user-product-access-log/index'])]
            ]
        ];

        $menuItems[] = ['label' => Yii::t('users', 'My Notifications'), 'url' => Url::to(['/users-notifications/my-notifications']), 'icon' => 'bell'];

        $menuItems[] = ['icon' => 'list-alt', 'label' => Yii::t('loan_application', 'All Applications list'), 'url' => Url::to(['/loan-application/index'])];
        $menuItems[] = ['icon' => 'recycle yellow', 'label' => Yii::t('loan_application', 'Loan Application Sent'), 'url' => Url::to(['/loan-application/sent'])];
        $menuItems[] = ['icon' => 'briefcase blue', 'label' => Yii::t('loan_application', 'Loan Application In Work'), 'url' => Url::to(['/loan-application/in-work'])];
        $menuItems[] = ['icon' => 'check-square-o green', 'label' => Yii::t('loan_application', 'Loan Application Approved'), 'url' => Url::to(['/loan-application/approved'])];
        $menuItems[] = ['icon' => 'ban red', 'label' => Yii::t('loan_application', 'Loan Application Declined'), 'url' => Url::to(['/loan-application/declined'])];
        $menuItems[] = ['icon' => 'money green', 'label' => Yii::t('loan_application', 'Loan Application Paid'), 'url' => Url::to(['/loan-application/paid'])];
        $menuItems[] = ['icon' => 'chain-broken red', 'label' => Yii::t('loan_application', 'Loan Application Revoked'), 'url' => Url::to(['/loan-application/revoked'])];

        $menuItems[] = [
            'label' => Yii::t('loan_application', 'Loan Applications',),
//            'url' => Url::to(['/product/index']),
            'url' => 'javascript:',
            'icon' => 'list-alt',
            'items' => [
                ['label' => Yii::t('loan_application', 'Loan Application History'), 'url' => Url::to(['/loan-application-log/index'])],
                ['label' => Yii::t('loan_application', 'Distribution'), 'url' => Url::to(['/loan-application-distribution/index'])],
                ['label' => Yii::t('loan_application', 'Loan Application Access'), 'url' => Url::to(['/loan-application-access/index'])],
                ['label' => Yii::t('loan_application', 'Additional Pages'), 'url' => 'javascript:', 'items' => [
                    ['label' => Yii::t('loan_application', 'Client Email'), 'url' => Url::to(['/loan-application-client-email/index'])],
                    ['label' => Yii::t('loan_application', 'Client Phone'), 'url' => Url::to(['/loan-application-client-phone/index'])],
                    ['label' => Yii::t('loan_application', 'Client Employed Data'), 'url' => Url::to(['/loan-application-client-employed-data/index'])],
                    ['label' => Yii::t('loan_application', 'Client Passport Data'), 'url' => Url::to(['/loan-application-client-passport-data/index'])],
                    ['label' => Yii::t('loan_application', 'Companies relation'), 'url' => Url::to(['/loan-application-companies/index'])],
                ]]
            ]
        ];
        $menuItems[] = [
            'label' => Yii::t('users', 'Users'),
            'url' => 'javascript:',
            'icon' => 'users',
            'items' => [
                ['label' => Yii::t('users', 'Users List'), 'url' => Url::to(['/users/index'])],
                ['label' => Yii::t('users', 'Users Product Access'), 'url' => Url::to(['/users-product-access/index'])],
                ['label' => Yii::t('users', 'Notifications'), 'url' => Url::to(['/users-notifications/index'])],
            ]
        ];
        $menuItems[] = [
            'label' => Yii::t('backend', 'Blog'),
            'url' => 'javascript:',
            'icon' => 'th-list',
            'items' => [
                ['label' => Yii::t('backend', 'Categories'), 'url' => Url::to(['/blog-categories/index']), 'icon' => 'list'],
                ['label' => Yii::t('backend', 'Posts'), 'url' => Url::to(['/blog-posts/index']), 'icon' => 'list'],
            ]
        ];
        $menuItems[] = [
            'label' => Yii::t('backend', 'FAQ'),
            'url' => Url::to(['/faq']),
            'icon' => 'question-circle',
        ];
        $menuItems[] = [
            'label' => Yii::t('backend', 'Main Banner'),
            'url' => Url::to(['/main-banner']),
            'icon' => 'th-large',
        ];
        $menuItems[] = [
            'label' => Yii::t('backend', 'System Tools'),
            'url' => 'javascript:',
            'icon' => 'folder',
            'items' => [
                [
                    'label' => Yii::t('backend', 'System Logs'),
                    'url' => Url::to(['/log', 'lang' => Yii::$app->language]),
                    'icon' => 'bug'
                ],
                [
                    'label' => Yii::t('backend', 'System Settings'),
                    'url' => Url::to(['/settings', 'lang' => Yii::$app->language]),
                    'icon' => 'cog'
                ],
                [
                    'label' => Yii::t('backend.translate_manager', 'Translate Manager'),
                    'url' => 'javascript:',
                    'icon' => 'th-list',
                    'items' => [
                        ['label' => Yii::t('backend.translate_manager', 'Langauge List'), 'url' => Url::to(['/translatemanager/language/list']), 'icon' => 'list'],
                        ['label' => Yii::t('backend.translate_manager', 'Scan'), 'url' => Url::to(['/translatemanager/language/scan']), 'icon' => 'barcode'],
                        ['label' => Yii::t('backend.translate_manager', 'Optimizer'), 'url' => Url::to(['/translatemanager/language/optimizer']), 'icon' => 'barcode'],
                        ['label' => Yii::t('backend.translate_manager', 'Create'), 'url' => Url::to(['/translatemanager/language/create']), 'icon' => 'plus'],
                    ]
                ],
                [
                    'label' => 'RBAC',
                    'url' => 'javascript:',
                    'icon' => 'th-list',
                    'items' => [
                        ['label' => Yii::t('backend.rbac', 'List'), 'url' => Url::to(['/rbac'])],
                        ['label' => Yii::t('backend.rbac', 'Route'), 'url' => Url::to(['/rbac/route'])],
                        ['label' => Yii::t('backend.rbac', 'Permission'), 'url' => Url::to(['/rbac/permission'])],
                        ['label' => Yii::t('backend.rbac', 'Assignment'), 'url' => Url::to(['/rbac/assignment'])],
                        ['label' => Yii::t('backend.rbac', 'Role'), 'url' => Url::to(['/rbac/role'])],
                    ]
                ]
            ]

        ];

        return $this->render('nav_bar_menu', [
            'menuItems' => $menuItems
        ]);
    }
}
