<?php

namespace backend\widgets\pagination;

use yii\widgets\LinkPager;

class Pagination extends LinkPager
{
    public $pageCssClass = 'page-item';

    public $prevPageCssClass = 'page-item';

    public $linkOptions = [
        'class' => 'page-link'
    ];

    public $disabledListItemSubTagOptions = ['class' => 'page-link'];
}