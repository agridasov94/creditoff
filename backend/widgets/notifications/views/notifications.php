<?php

use backend\src\entities\usersNotifications\entity\UsersNotifications;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;

/* @var UsersNotifications[] $notifications */
/* @var integer $count */
/** @var View $this */

if (!$count) {
    $count = null;
}

?>

<?php Pjax::begin(['id' => 'notify-pjax', 'timeout' => false, 'enablePushState' => false, 'enableReplaceState' => false, 'options' => [
    'tag' => 'li',
    'class' => 'dropdown open notif',
    'role' => 'presentation',
]])?>

<?php $pNotifiers = null; ?>

    <a href="javascript:;" class="dropdown-toggle info-number" title="Notifications" data-toggle="dropdown"
       aria-expanded="false">
        <i class="fa fa-bell-o"></i><span class="badge bg-green notification-counter"><?= $count ?></span>
    </a>

    <ul id="notification-menu" class="dropdown-menu list-unstyled msg_list" role="menu" x-placement="bottom-end">
        <?php foreach ($notifications as $notification) : ?>
            <?= $this->render('item', [
                'id' => $notification->un_id,
                'title' => $notification->un_title,
                'createdDt' => $notification->un_created_dt,
                'message' => $notification->un_message
            ]) ?>
            <?php
            if ($notification->isMustPopupShow()) {
                $title = Html::encode($notification->un_title);
                $type = UsersNotifications::getNotifyType($notification->un_type_id);
                $message = str_replace(["\r\n", "\n", '"'], ['', '', '\"'], $notification->un_message);
                $desktopMessage = str_replace('"', '\"', strip_tags($notification->un_message));
                $pNotifiers .= "notificationPNotify('" . $type . "', '" . $title . "', '" . $message . "', '" . $desktopMessage . "');" . PHP_EOL;
            }
            ?>
        <?php endforeach; ?>

        <?php $this->registerJs($pNotifiers, View::POS_END); ?>

        <li>
            <div class="text-center">
                <?= Html::a('<i class="fa fa-search"></i> <strong>See all Notifications</strong>', ['/users-notifications/my-notifications']) ?>
            </div>
        </li>
    </ul>

<?php yii\widgets\Pjax::end() ?>

<?php

$notifyUrl = Url::to(['/notifications/pjax-notify']);

$js = <<<JS

const notifyUrl = '$notifyUrl';
function updatePjaxNotify() {
    $.pjax.reload({url: notifyUrl, container : '#notify-pjax', push: false, replace: false, timeout: 10000, scrollTo: false, async: false});
}

$("#notify-pjax").on("pjax:beforeSend", function() {
    $('#notify-pjax .info-number i').removeClass('fa-comment-o').addClass('fa-spin fa-spinner');
});

$("#notify-pjax").on("pjax:complete", function() {
    $('#notify-pjax .info-number i').removeClass('fa-spin fa-spinner').addClass('fa-comment-o');
});

$("#notify-pjax").on('pjax:timeout', function(event) {
    $('#notify-pjax .info-number i').removeClass('fa-spin fa-spinner').addClass('fa-comment-o');
    event.preventDefault()
});
JS;

$this->registerJs($js, View::POS_END);
