<?php

use backend\src\helpers\purifier\Purifier;
use backend\src\helpers\purifier\PurifierFilter;
use backend\src\helpers\ViewHelper;
use yii\helpers\Html;
use yii\helpers\StringHelper;

/** @var int $id */
/** @var string $title */
/** @var string $createdDt */
/** @var string $message */

$time = strtotime($createdDt);

?>

<li data-id="<?= $id ?>">
    <a href="javascript:;" onclick="notificationShow(this);" id="notification-menu-element-show" data-title="<?= Html::encode($title) ?>" data-id="<?= $id ?>">
        <span class="glyphicon glyphicon-info-sign"> </span>
        <span>
            <span><?= Html::encode($title) ?></span>
            <span class="time" data-time="<?= $time ?>"><?= Yii::$app->formatter->asRelativeTime($time) ?></span>
        </span>
        <span class="message"><?= StringHelper::truncate(ViewHelper::stripHtmlTags(Purifier::purify($message, PurifierFilter::shortCodeToLink())), 80, '...') ?><br></span>
    </a>
</li>
