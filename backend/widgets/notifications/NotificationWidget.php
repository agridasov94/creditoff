<?php

namespace backend\widgets\notifications;

use backend\src\entities\usersNotifications\entity\UsersNotifications;
use backend\src\entities\usersNotifications\entity\UsersNotificationsQuery;
use yii\bootstrap\Widget;

/**
 * Class NotificationWidget
 *
 * @property int $userId
 */
class NotificationWidget extends Widget
{
    public $userId;

    public function run(): string
    {
        $result = [
            'count' => UsersNotificationsQuery::findNewCount($this->userId),
            'notifications' => UsersNotificationsQuery::findNew($this->userId),
        ];

        $notifications = $this->processPopupNotifications($result['notifications']);

        return $this->render('notifications', [
            'notifications' => $notifications,
            'count' => $result['count'],
        ]);
    }

    /**
     * @param UsersNotifications[] $notifications
     * @return UsersNotifications[]
     */
    private function processPopupNotifications(array $notifications): array
    {
        $clones = [];
        foreach ($notifications as $notification) {
            $clones[] = clone $notification;
            if ($notification->isMustPopupShow()) {
                $notification->un_popup_show = true;
                $notification->save();
            }
        }
        return $clones;
    }
}
