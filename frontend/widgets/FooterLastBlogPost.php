<?php

namespace frontend\widgets;

use common\src\entities\blog\posts\entity\BlogPostsQuery;

class FooterLastBlogPost extends \yii\base\Widget
{
    public string $lang = '';

    public function run()
    {
        $blogPosts = BlogPostsQuery::findLastPosts($this->lang);

        return $this->render('footer_last_blog_post', [
            'blogPosts' => $blogPosts
        ]);
    }
}