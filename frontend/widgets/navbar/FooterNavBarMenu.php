<?php

namespace frontend\widgets\navbar;

use Yii;
use yii\base\Widget;
use yii\helpers\Url;

class FooterNavBarMenu extends Widget
{
    public function run()
    {
        $menuItems = [];

        $menuItems[] = [
            'label' => Yii::t('frontend', 'Home'),
            'url' => Url::to(['/']),
        ];
        $menuItems[] = [
            'label' => Yii::t('frontend', 'Blog'),
            'url' => Url::to(['/blog']),
        ];
        $menuItems[] = [
            'label' => Yii::t('frontend', 'About us'),
            'url' => Url::to(['/site/about-us']),
        ];
        $menuItems[] = [
            'label' => Yii::t('frontend', 'FAQ'),
            'url' => Url::to(['/site/questions-and-answers']),
        ];
        $menuItems[] = [
            'label' => Yii::t('frontend', 'Contact Us'),
            'url' => Url::to(['site/contact']),
        ];

        return $this->render('footer_quick_links', [
            'menuItems' => $menuItems
        ]);
    }
}