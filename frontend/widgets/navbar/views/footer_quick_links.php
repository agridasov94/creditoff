<?php
/**
 * @var $this \yii\web\View
 * @var $menuItems array
 */

use yii\helpers\Html;

?>

<h6 class="text-uppercase"><?= Yii::t('frontend', 'Quick Links') ?></h6>
<div class="row" style="max-width: 270px;">
    <div class="col-6">
        <ul class="list-marked-variant-2">
            <?php foreach ($menuItems as $key => $item): ?>
                <?= Html::tag('li', Html::a($item['label'], $item['url'])) ?>
            <?php unset($menuItems[$key]); if ($key === 3) {break;} endforeach;?>
        </ul>
    </div>
    <div class="col-6">
        <ul class="list-marked-variant-2">
            <?php foreach ($menuItems as $key => $item): ?>
                <?= Html::tag('li', Html::a($item['label'], $item['url'])) ?>
            <?php endforeach;?>
        </ul>
    </div>
</div>
