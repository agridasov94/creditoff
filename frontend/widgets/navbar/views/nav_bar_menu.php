<?php
/** @var $menuItems array */

use frontend\widgets\navbar\Menu;

?>

<div class="rd-navbar-inner">
    <div class="rd-navbar-group">
        <div class="rd-navbar-panel">
            <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button><a class="rd-navbar-brand brand" href="/"><img src="/images/logo-light-113x45.png" alt="" width="113" height="45"/></a>
        </div>
        <div class="rd-navbar-group-asside">
            <div class="rd-navbar-nav-wrap">
                <div class="rd-navbar-nav-inner mob-menu">

                    <?= Menu::widget([
                        'items' => $menuItems,
                        'encodeLabels' => false,
                        'activateParents' => true,
                    ]); ?>

                </div>
            </div>
        </div>
    </div>
</div>
<?php
if (!Yii::$app->user->isGuest) {
$js = <<<JS
(function () {
    let logoutBtn = document.getElementById('logout');
    logoutBtn.addEventListener('click', function () {
        document.getElementById('logoutForm').submit();
    });
})();
JS;
$this->registerJs($js);
}
