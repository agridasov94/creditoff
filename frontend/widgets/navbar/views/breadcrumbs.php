<?php

use common\src\entities\clientNotification\entity\ClientNotification;
use yii\helpers\Html;

/**
 * @var $this \yii\web\View
 * @var $links array
 * @var $displayShortLoanFormLink bool
 * @var $notifications ClientNotification[]
 */

$totalLinks = count($links) - 1;
$totalNotifications = count($notifications);
?>
<section class="breadcrumb_links section-top-15 section-bottom-15">
    <div class="container">
        <div class="breadcrubs_wrapper">
            <ul class="breadcrumb">
                <?php foreach ($links as $key => $link): ?>
                    <li><?= $link ?></li>
                    <?php if($key !== $totalLinks): ?>
                        <i class="fa fa-chevron-right" aria-hidden="true"></i>
                    <?php endif; ?>
                <?php endforeach; ?>              
            </ul>
            <div class="breadcr_btn" id="notif_conect">
                <?php if ($displayShortLoanFormLink): ?>
                    <?= Html::a(Yii::t('frontend', 'Short Form Request'), \yii\helpers\Url::to(['/loan-request/create'])) ?>
                <?php endif; ?>
                <?php if (Yii::$app->user->identity): ?>
                    <button class="notification dropdown" onclick="dropMenu()">
                        <span class="icon icon-md icon-primary fa-bell notif"></span>
                    </button>
                        <?php if ($totalNotifications): ?>
                            <span class="badge"><?= $totalNotifications ?></span>
                        <?php endif; ?>
                        <div class="dropdown-content" id="myDropdown">
                            <?php if ($notifications): ?>
                                <?php foreach ($notifications as $notification): ?>
                                    <!-- Standart Notification -->
                                    <div class="Message">
                                        <div class="Message-icon">
                                            <i class="fa fa-bell-o"></i>
                                        </div>
                                        <div class="Message-body">
                                            <p><?= ucfirst(ClientNotification::getNotifyType($notification->cn_type_id)) . ': ' . $notification->cn_title ?></p>
                                            <p class="u-italic"><?= $notification->cn_message ?></p>
                                            <div class="notif_date"><span><?= $notification->cn_created_dt ?><span></div>
                                        </div>
                                        <button class="Message-close js-messageClose"><i class="fa fa-times"></i></button>
                                    </div>
                                   <!--  Green notification -->
                                    <div class="Message Message--green">
                                        <div class="Message-icon">
                                            <i class="fa fa-check"></i>
                                        </div>
                                        <div class="Message-body">
                                            <p><?= ucfirst(ClientNotification::getNotifyType($notification->cn_type_id)) . ': ' . $notification->cn_title ?></p>
                                            <p class="u-italic"><?= $notification->cn_message ?></p>
                                            <div class="notif_date"><span><?= $notification->cn_created_dt ?><span></div>

                                        </div>
                                        <button class="Message-close js-messageClose"><i class="fa fa-times"></i></button>
                                    </div>
                                    <!--  Red Norification -->
                                    <div class="Message Message--red">
                                        <div class="Message-icon">
                                            <i class="fa fa-times"></i>
                                        </div>
                                        <div class="Message-body">
                                            <p><?= ucfirst(ClientNotification::getNotifyType($notification->cn_type_id)) . ': ' . $notification->cn_title ?></p>
                                            <p class="u-italic"><?= $notification->cn_message ?></p>
                                            <div class="notif_date"><span><?= $notification->cn_created_dt ?><span></div>

                                        </div>
                                        <button class="Message-close js-messageClose"><i class="fa fa-times"></i></button>
                                    </div>
                                    <!-- YEllow  Notification -->
                                    <div class="Message Message--orange">
                                        <div class="Message-icon">
                                            <i class="fa fa-exclamation"></i>
                                        </div>
                                        <div class="Message-body">
                                            <p><?= ucfirst(ClientNotification::getNotifyType($notification->cn_type_id)) . ': ' . $notification->cn_title ?></p>
                                            <p class="u-italic"><?= $notification->cn_message ?></p>
                                            <div class="notif_date"><span><?= $notification->cn_created_dt ?><span></div>
                                        </div>
                                        <button class="Message-close js-messageClose"><i class="fa fa-times"></i></button>
                                    </div>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <p class="not-exists-notifications"><?= Yii::t('frontend', 'You dont have notifications.') ?></p>
                            <?php endif; ?>
                        </div>
                    </a>               
                </div>
            <?php endif; ?>
        </div>  
    </div>
</section>

<?php

$js = <<<JS
function closeMessage(el) {
  el.addClass('is-hidden');
}

$('.js-messageClose').on('click', function(e) {
  closeMessage($(this).closest('.Message'));
});

$('#js-helpMe').on('click', function(e) {
  alert('Help you we will, young padawan');
  closeMessage($(this).closest('.Message'));
});


$(document).ready(function() {
  setTimeout(function() {
    closeMessage($('#js-timer'));
  }, 5000);
});

$(document).click(function(event){
    if(!$(event.target).closest("#notif_conect").length){
        document.getElementById("myDropdown").classList.remove("show");
    }
});
JS;
$this->registerJs($js, yii\web\View::POS_END);
?>

<script>
function dropMenu() {
  document.getElementById("myDropdown").classList.add("show");
}
</script>