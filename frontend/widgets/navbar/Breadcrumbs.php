<?php

namespace frontend\widgets\navbar;

use common\src\entities\clientNotification\entity\ClientNotification;
use common\src\entities\clientNotification\entity\ClientNotificationQuery;
use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class Breadcrumbs extends Widget
{
    public bool $displayShortLoanFormLink = true;

    public function run()
    {
        $links = [];
        if (str_contains(Yii::$app->request->url, 'cabinet')) {
            $links[] = Html::a(Yii::t('frontend', 'Home'), '/cabinet/loan-application-list');
        } else {
            $links[] = Html::a(Yii::t('frontend', 'Home'), Yii::$app->homeUrl);
        }
        foreach ($this->getView()->params['breadcrumbs'] ?? [] as $breadcrumb) {
            $links[] = $this->renderItem($breadcrumb);
        }

        $notifications = [];
        if (!Yii::$app->user->isGuest) {
            $notifications = ClientNotificationQuery::findAllByClientId(Yii::$app->user->id);
        }

        return $this->render('breadcrumbs', [
            'links' => $links,
            'displayShortLoanFormLink' => $this->displayShortLoanFormLink,
            'notifications' => $notifications
        ]);
    }

    private function renderItem($item)
    {
        if (is_array($item)) {
            return Html::a($item['label'], $item['url']);
        }
        return $item;
    }
}