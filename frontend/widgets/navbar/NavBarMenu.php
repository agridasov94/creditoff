<?php

namespace frontend\widgets\navbar;

use common\src\entities\client\entity\Client;
use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;

class NavBarMenu extends Widget
{
    public function run()
    {
        $menuItems = [];

//        $menuItems[] = [
//            'label' => Yii::t('frontend', 'Home'),
//            'url' => Url::to(['/']),
//        ];
      
        $menuItems[] = [
            'label' => Yii::t('frontend', 'Blog'),
            'url' => Url::to(['/blog']),
        ];
        $menuItems[] = [
            'label' => Yii::t('frontend', 'About us'),
            'url' => Url::to(['/about-us']),
        ];
        $menuItems[] = [
            'label' => Yii::t('frontend', 'FAQ'),
            'url' => Url::to(['/faq']),
        ];
        $menuItems[] = [
            'label' => Yii::t('frontend', 'Contact Us'),
            'url' => Url::to(['/contact']),
        ];

        $languages = [
            'label' => ucfirst(Yii::$app->language),
            'url' => 'javascript:',
            'items' => [],
            'options' => [
                'class' => 'rd-navbar--has-megamenu rd-navbar-submenu'
            ],
            'submenuTemplate' => "\n<ul class=\"rd-navbar-dropdown langs\">\n{items}\n</ul>\n"
        ];
        foreach (Yii::$app->multiLanguage->langs as $lang) {
            if ($lang !== Yii::$app->language) {
                $languages['items'][] = [
                    'label' => ucfirst($lang),
                    'url' => Url::to([Yii::$app->request->getUrl(), 'lang' => $lang])
                ];
            }
        }
        $menuItems[] = $languages;
       

        if (Yii::$app->user->isGuest) {
            $menuItems[] = [
                'template' => '<a  href="#"><span class="icon icon-md icon-primary fa-user no-border"></span><span  class="main-title">' . Yii::t('frontend', 'Cabinet Personal') . '</span></a>',
                'options' => [
                    'class' => 'button drop-btn'
                ],
                'items' => [
                    [
                        'label' => Yii::t('frontend', 'Access Partner'),
                        'url' => Url::to(['partner/login'])
                    ],
                    [
                        'label' => Yii::t('frontend', 'Access Client'),
                        'url' => Url::to(['client/login'])
                    ],
                ],
                'submenuTemplate' => "\n<ul class=\"rd-navbar-dropdown\">\n{items}\n</ul>\n"
            ];
        } else {
            /** @var $client Client **/
            $client = Yii::$app->user->identity;
            $menuItems[] = [
                'template' => '<a  href="#"><span class="icon icon-md icon-primary fa-user no-border"></span><span  class="main-title">' . $client->getFullName() . '</span></a>',
                'options' => [
                    'class' => 'button drop-btn'
                ],
                'items' => [
                    [
                        'label' => Yii::t('frontend', 'Loan request list'),
                        'url' => Url::to(['cabinet/loan-application-list'])
                    ],
                    [
                        'label' => Yii::t('frontend', 'Personal data'),
                        'url' => Url::to(['cabinet/personal-data'])
                    ],
                    [
                        'label' => Yii::t('frontend', 'Logout'),
                        'url' => Url::to(['client/logout']),
                        'isLogout' => true,
                        'attributes' => [
                            'id' => 'logout'
                        ],
                        'options'=> [
                            'class' =>'log_out_link'
                        ],
                    ],
                ],
                'submenuTemplate' => "\n<ul class=\"rd-navbar-dropdown\">\n{items}\n</ul>\n"
            ];
        }
         
        $menuItems[] = [
            'template' => Yii::t('frontend', '<a href="#">Notifications<span class="icon icon-md icon-primary fa-bell notif_gamburg"></span><span class="badge_gamburger">3</span></a>'),  
            'options' => [
                'class' => 'button drop-btn notific_vision',
            ],
            'items'=>[
                [
                    'label' => Yii::t('frontend', 
                    '<div class="Message">
                        <div class="Message-icon">
                            <i class="fa fa-bell-o"></i>
                        </div>
                        <div class="Message-body">
                            <p>This is a simple, but friendly, notification.</p>
                            <p class="u-italic">It will disappear within a few seconds.</p>
                        </div>
                        <button class="Message-close js-messageClose"><i class="fa fa-times"></i></button>
                    </div>'),
                ],
                 [
                    'label' => Yii::t('frontend', ' 
                    <div class="Message Message--green">
                        <div class="Message-icon">
                            <i class="fa fa-check"></i>
                        </div>
                        <div class="Message-body">
                            <p>This is a message telling you that everything is a-okay!</p>
                            <p>Good job, and good riddance.</p>
                        </div>
                        <button class="Message-close js-messageClose"><i class="fa fa-times"></i></button>
                    </div>
                '),
                    
                ], 
                [
                    'label' => Yii::t('frontend', 
                    '<div class="Message">
                        <div class="Message-icon">
                            <i class="fa fa-bell-o"></i>
                        </div>
                        <div class="Message-body">
                            <p>This is a simple, but friendly, notification.</p>
                            <p class="u-italic">It will disappear within a few seconds.</p>
                        </div>
                        <button class="Message-close js-messageClose"><i class="fa fa-times"></i></button>
                    </div>'),
                ],
                [
                    'label' => Yii::t('frontend', 
                    '<div class="Message">
                        <div class="Message-icon">
                            <i class="fa fa-bell-o"></i>
                        </div>
                        <div class="Message-body">
                            <p>This is a simple, but friendly, notification.</p>
                            <p class="u-italic">It will disappear within a few seconds.</p>
                        </div>
                        <button class="Message-close js-messageClose"><i class="fa fa-times"></i></button>
                    </div>'),
                ],
                [
                    'label' => Yii::t('frontend', 
                    '<div class="Message">
                        <div class="Message-icon">
                            <i class="fa fa-bell-o"></i>
                        </div>
                        <div class="Message-body">
                            <p>This is a simple, but friendly, notification.</p>
                            <p class="u-italic">It will disappear within a few seconds.</p>
                        </div>
                        <button class="Message-close js-messageClose"><i class="fa fa-times"></i></button>
                    </div>'),
                ],
            ],
            'submenuTemplate' => "\n<ul class=\"rd-navbar-dropdown\">\n{items}\n</ul>\n"
            
        ];

        return $this->render('nav_bar_menu', [
            'menuItems' => $menuItems
        ]);
    }
}
