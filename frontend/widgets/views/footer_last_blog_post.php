<?php
/**
 * @var $this \yii\web\View
 * @var $blogPosts \common\src\entities\blog\posts\entity\BlogPosts[]
 */

use yii\helpers\Html;

?>
<h6 class="text-uppercase"><?= Yii::t('frontend', 'Last Posts') ?></h6>
<?php foreach($blogPosts as $blogPost): ?>
<article class="post post-preview post-preview-inverse">
    <a href="<?= $blogPost->getUrl() ?>">
        <div class="unit flex-row unit-spacing-lg">
            <div class="unit-left">
                <figure class="post-image"><img src="<?= $blogPost->getPublicPreviewPath() ?>" alt="<?= Html::encode($blogPost->bp_title) ?>" style="width: 70px; height: auto;"/>
                </figure>
            </div>
            <div class="unit-body">
                <div class="post-header">
                    <p><?= Html::encode($blogPost->bp_title) ?></p>
                </div>
                <div class="post-meta">
                    <ul class="list-meta">
                        <li>
                            <time datetime="2021-02-04">Feb 4, 2021</time>
                        </li>
                        <li><?= Html::encode($blogPost->category->bc_name) ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </a>
</article>
<?php endforeach; ?>