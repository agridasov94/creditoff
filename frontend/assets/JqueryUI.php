<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class JqueryUI extends AssetBundle
{
    public $css = [
        '//code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css'
    ];

    public $js = [
        'https://code.jquery.com/ui/1.13.1/jquery-ui.js'
    ];

    public $depends = [
        AppAsset::class
    ];
}