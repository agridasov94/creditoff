<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '//fonts.googleapis.com/css?family=Arvo:400,700%7COpen+Sans:300,300italic,400,400italic,700italic,800%7CUbuntu:500',
        '/css/bootstrap.css',
        '/css/fonts.css',
        '/css/style.css',
    ];
    public $js = [
        '/js/core.min.js',
        '/js/script.js'
    ];
    public $depends = [
    ];
}
