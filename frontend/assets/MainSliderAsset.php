<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class MainSliderAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        '/css/main_slider.css',
        '/css/short_form.css',
    ];

    public $js = [];

    public $depends = [
        AppAsset::class
    ];
}
