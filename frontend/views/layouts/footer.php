<?php

use frontend\widgets\FooterLastBlogPost;
use frontend\widgets\navbar\FooterNavBarMenu;

?>
<footer class="page-foot bg-ebony-clay context-dark">
    <div class="section-40 section-md-75">
        <div class="container">
            <div class="row justify-content-sm-center">
                <div class="col-sm-9 col-md-11 col-xl-12">
                    <div class="row row-50">
                        <div class="col-md-6 col-lg-10 col-xl-3">
                            <div class="inset-xl-right-20" style="max-width: 510px;"><a class="brand brand-inverse" href="index.html"><img src="/images/logo-light-113x45.png" alt="" width="113" height="45"/></a>
                                <p class="text-shuttle-gray"><?= Yii::t('frontend', 'Footer Text About company') ?></p>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 col-xl-3">
                            <?= FooterLastBlogPost::widget([
                                'lang' => Yii::$app->language
                            ]) ?>
                        </div>
                        <div class="col-md-6 col-lg-4 col-xl-3">
                            <?= FooterNavBarMenu::widget() ?>
                        </div>
                        <div class="col-md-6 col-lg-4 col-xl-3">
                            <h6 class="text-uppercase"><?= Yii::t('frontend', 'Contact us') ?></h6>
                            <address class="contact-info text-start">
                                <div class="unit flex-row unit-spacing-md align-items-center">
                                    <div class="unit-left"><span class="icon icon-xs text-shuttle-gray fa-phone"></span></div>
                                    <div class="unit-body"><a class="link-white" href="tel:<?= Yii::t('frontend', '+1 (409) 987–5874') ?>"><?= Yii::t('frontend', '+1 (409) 987–5874') ?></a></div>
                                </div>
                                <div class="unit flex-row unit-spacing-md align-items-center">
                                    <div class="unit-left"><span class="icon icon-xs text-shuttle-gray fa fa-envelope-o"></span></div>
                                    <div class="unit-body"><a class="link-white" href="mailto:<?= Yii::t('frontend' ,'info@demolink.org') ?>"><?= Yii::t('frontend' ,'info@demolink.org') ?></a></div>
                                </div>
                                <div class="unit flex-row unit-spacing-md">
                                    <div class="unit-left"><span class="icon icon-xs text-shuttle-gray fa-map-marker"></span></div>
                                    <div class="unit-body"><a class="link-white d-inline" href="#"><?= Yii::t('frontend' , '6036 Richmond hwy,<br>Alexandria, VA USA 22303') ?></a></div>
                                </div>
                            </address>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <hr>
    </div>
    <div class="page-foot section section-35 bg-ebony-clay context-dark">
        <div class="container text-center">
            <div class="row row-15 flex-row-md-reverse justify-content-md-between align-items-md-center">
                <div class="col-md-6 text-md-end">
                    <div class="group-sm group-middle">
                        <p class="fst-italictext-white"><?= Yii::t('frontend', 'Follow Us') ?>:</p>
                        <ul class="list-inline list-inline-reset">
                            <li><a class="icon icon-round icon-pciked-bluewood icon-xxs-smallest fa fa-facebook" href="#"></a></li>
                            <li><a class="icon icon-round icon-pciked-bluewood icon-xxs-smallest fa fa-twitter" href="#"></a></li>
                            <li><a class="icon icon-round icon-pciked-bluewood icon-xxs-smallest fa fa-google-plus" href="#"></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 text-md-start">
                    <p class="rights text-white"><?= Yii::t('frontend', '<span class="copyright-year"></span><span>&nbsp;&#169;&nbsp;</span><span>Investment Smart.&nbsp;</span><a class="link-white-v2" href="privacy-policy.html">Privacy Policy</a>') ?></p>
                </div>
            </div>
        </div>
    </div>
</footer>