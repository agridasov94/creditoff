<?php

use frontend\widgets\navbar\NavBarMenu;

?>
<header class="page-head">
    <div class="rd-navbar-wrap">
        <nav class="rd-navbar rd-navbar-corporate-light" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-static" data-xl-layout="rd-navbar-static" data-xl-device-layout="rd-navbar-static" data-xxl-layout="rd-navbar-static" data-xxl-device-layout="rd-navbar-static" data-lg-stick-up-offset="53px" data-xl-stick-up-offset="53px" data-xxl-stick-up-offset="53px" data-lg-stick-up="true" data-xl-stick-up="true" data-xxl-stick-up="true">
            <div class="bg-ebony-clay context">
                <div class="rd-navbar-inner">
                    <div class="rd-navbar-aside-wrap">
                        <div class="rd-navbar-aside">
                            <div class="rd-navbar-aside-toggle" data-rd-navbar-toggle=".rd-navbar-aside"><span></span></div>
                            <div class="rd-navbar-aside-content">
                                <ul class="rd-navbar-aside-group list-units">
                                    <li>
                                        <div class="unit flex-row unit-spacing-xs align-items-center">
                                            <div class="unit-left"><span class="icon icon-xxs icon-primary fa-map-marker"></span></div>
                                            <div class="unit-body"><a class="link-secondary" href="#"><?= Yii::t('frontend', '267 Park Avenue New York, NY 90210') ?></a></div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="unit flex-row unit-spacing-xs align-items-center">
                                            <div class="unit-left"><span class="icon icon-xxs icon-primary fa-clock-o"></span></div>
                                            <div class="unit-body"><span class="time"><?= Yii::t('frontend', 'Mon – Sat: 9:00am–18:00pm. Sunday CLOSED') ?></span></div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="unit flex-row unit-spacing-xs align-items-center">
                                            <div class="unit-left"><span class="icon icon-xxs icon-primary fa-phone"></span></div>
                                            <div class="unit-body"><a class="link-secondary" href="tel:#">+1 (409) 987–5874</a></div>
                                        </div>
                                    </li>
                                </ul>
                                    <div class="rd-navbar-aside-group">
                                        <ul class="list-inline list-inline-reset">
                                            <li><a class="icon icon-round icon-pciked-bluewood icon-xxs-smallest fa fa-facebook" href="#"></a></li>
                                            <li><a class="icon icon-round icon-pciked-bluewood icon-xxs-smallest fa fa-twitter" href="#"></a></li>
                                            <li class="li-icon-pd"><a class="icon icon-round icon-pciked-bluewood icon-xxs-smallest fa fa-google-plus" href="#"></a></li>
                                            
                                        </ul>
                                             
                                    </div>
                                    
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?= NavBarMenu::widget() ?>

        </nav>
    </div>
</header>