<div class="page-loader page-loader-variant-1">
    <div><a class="brand brand-md brand-inverse" href="index.html"><img src="/images/logo-light-113x45.png" alt="" width="113" height="45"/></a>
        <div class="page-loader-body">
            <div id="spinningSquaresG">
                <div class="spinningSquaresG" id="spinningSquaresG_1"></div>
                <div class="spinningSquaresG" id="spinningSquaresG_2"></div>
                <div class="spinningSquaresG" id="spinningSquaresG_3"></div>
                <div class="spinningSquaresG" id="spinningSquaresG_4"></div>
                <div class="spinningSquaresG" id="spinningSquaresG_5"></div>
                <div class="spinningSquaresG" id="spinningSquaresG_6"></div>
                <div class="spinningSquaresG" id="spinningSquaresG_7"></div>
                <div class="spinningSquaresG" id="spinningSquaresG_8"></div>
            </div>
        </div>
    </div>
</div>