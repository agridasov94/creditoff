<?php
/**
 * @var $this \yii\web\View
 * @var $form \yii\widgets\ActiveForm
 * @var $loanRequestForm \frontend\src\useCase\loanRequest\shortForm\LoanRequestCreateForm
 * @var $isActive bool
 * @var $product \common\src\entities\product\entity\Product|null
 */

use backend\src\entities\partnerCompany\entity\PartnerCompanyQuery;
use common\src\entities\product\entity\ProductQuery;
use frontend\assets\JqueryUI;
use yii\helpers\Html;

JqueryUI::register($this);
?>
    <div class="row">
        <div class="col-md-12">
            <?= $form->errorSummary($loanRequestForm->loanDetails, ['class' => 'alert alert-danger']) ?>
        </div>
    </div>
    <div class="row row-30">
        <div class="col-md-6">
            <div class="form-wrap">
                <?= $form->field($loanRequestForm->loanDetails, 'productId')->dropDownList(ProductQuery::getList(), [
                    'class' => 'form-select',
                    'prompt' => $loanRequestForm->loanDetails->getAttributeLabel('productId'),
                    'id' => 'loan_type',
                    'required' => true
                ]) ?>
            </div>            
        </div>
        <div class="col-md-6">
        <?= $form->field($loanRequestForm->loanDetails, 'term')->input('number', ['class' => 'form-input form-control-has-validation', 'id' => 'loan_term', 'min' => $product->p_min_credit_term ?? 0, 'max' => $product->p_max_credit_term ?? 0]) ?>
            <div id="slider-range-term" style="margin-top: 20px;"></div>
        </div> 
    </div>
    <div class="row row-30">
        <div class="col-md-6">
            <?= $form->field($loanRequestForm->loanDetails, 'amount')->input('number', ['class' => 'form-input form-control-has-validation', 'id' => 'loan_amount', 'min' => $product->p_min_credit_amount ?? 0, 'max' => $product->p_max_credit_amount ?? 0]) ?>
            <div id="slider-range-min" style="margin-top: 20px;"></div>

           <?php /* $form->field($loanRequestForm->loanDetails, 'companies', [
               'options' => [
                    'class' => 'form-wrap'
                ],
                'template' => '{label}{input}<span class="form-validation">{error}</span>'
            ])->dropDownList($product && $product->p_id ? PartnerCompanyQuery::getListByProduct($product->p_id) : [], [
                'class' => 'form-select',
                'id' => 'companies',
                'data-multiple' => "1",
                'name' => $loanRequestForm->loanDetails->formName() . '[companies][]'
            ]) */ ?>

        </div>
    
        
<!--        <div class="col-md-6">-->
<!--            <div class="form-wrap">-->
<!--                <input class="form-input form-control-has-validation"  type="text" name="IDNP"><span class="form-validation"></span>-->
<!--                <label class="form-label rd-input-label">IDNP</label>-->
<!--            </div>-->
<!--        </div>-->
<!---->
<!--        <div class="col-md-6">-->
<!--            <div class="form-wrap">-->
<!--                <input class="form-input" type="tel" id="phone" name="phone" placeholder="+373" pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}">-->
<!--            </div>-->
<!---->
<!--        </div>-->
<!--        <div class="col-md-6">-->
<!--            <div class="form-wrap">-->
<!--                <input class="form-input form-control-has-validation" id="feedback-2-email" type="email" name="email" data-constraints="@Email @Required"><span class="form-validation"></span>-->
<!--                <label class="form-label rd-input-label" for="feedback-2-email">Email</label>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="col-12">-->
<!--            <div class="form-wrap has-error">-->
<!--                <textarea class="form-input form-control-has-validation" id="feedback-2-message" name="message" data-constraints="@Required"></textarea>-->
<!--                <label class="form-label rd-input-label" for="feedback-2-message">Комментарий</label>-->
<!--            </div>-->
<!--        </div>-->
    </div>

<?php
$getProductDataUrl = \yii\helpers\Url::to(['/loan-request/ajax-term-range']);
$amount = $loanRequestForm->loanDetails->amount ?? 0;
$maxAmount = $product->p_max_credit_amount ?? 0;
$minAmount = $product->p_min_credit_amount ?? 0;
$minTerm = $product->p_min_credit_term ?? 0;
$maxTerm = $product->p_max_credit_term ?? 0;
$term = $loanRequestForm->loanDetails->term ?? 0;
$js = <<<JS
$(document).ready( function () {
    let sliderRangeMinAmount = $("#slider-range-min");
    let sliderRangerTerm = $("#slider-range-term");
    let inputLoanAmount = $('#loan_amount');
    let inputLoanTerm = $('#loan_term');
    let loanType = $('#loan_type');
    let minAmount = $minAmount;
    let maxAmount = $maxAmount;
    let minTerm = $minTerm;
    let maxTerm = $maxTerm;
    
    function initSlider(elem, min, max, value, inputElem) {
        elem.slider({
            range: "min",
            value: value,
            min: min,
            max: max,
            slide: function( event, ui ) {
                inputElem.val(ui.value);
            }
        });
    }
    
    inputLoanAmount.attr('min', $minAmount).attr('max', $maxAmount).val($amount);
    inputLoanTerm.attr('min', $minTerm).attr('max', $maxAmount).val($term);
    initSlider(sliderRangeMinAmount, $minAmount, $maxAmount, $amount, inputLoanAmount)
    initSlider(sliderRangerTerm, $minTerm, $maxTerm, $term, inputLoanTerm)
    
    inputLoanAmount.on('keyup change', function(){
        sliderRangeMinAmount.slider('destroy');
        initSlider(sliderRangeMinAmount, minAmount, maxAmount, inputLoanAmount.val(), inputLoanAmount);
    });
    
    inputLoanTerm.on('keyup change', function(){
        sliderRangerTerm.slider('destroy');
        initSlider(sliderRangerTerm, minTerm, maxTerm, $('#loan_term').val(), inputLoanTerm);
    });
    
    loanType.select2({
        theme: loanType.attr('data-custom-theme') ? loanType.attr('data-custom-theme') : "bootstrap",
        multiple: !!loanType.attr('data-multiple')
    }).next().addClass(loanType.attr("class").match(/(input-sm)|(input-lg)|($)/i).toString().replace(new RegExp(",", 'g'), " "));
    
    $('#loan_type').on('change', function () {
        let submitBtn = $('#confirm_loan_request');
        submitBtn.addClass('disabled').prop('disabled', true);
        let val = $(this).val();
        
        $.ajax({
            url: '$getProductDataUrl',
            data: {productId: val, getCompanies: true},
            type: 'post',
            dataType: 'json',
            success: function (data) {
                minAmount = data.minAmount;
                maxAmount = data.maxAmount;
                minTerm = data.minTerm;
                maxTerm = data.maxTerm;
                inputLoanAmount.attr('min', data.minAmount).attr('max', data.maxAmount).val(data.minAmount);
                inputLoanTerm.attr('min', data.minTerm).attr('max', data.maxTerm).val(data.minTerm);
                initSlider(sliderRangeMinAmount, data.minAmount, data.maxAmount, data.minAmount, inputLoanAmount);
                initSlider(sliderRangerTerm, data.minTerm, data.maxTerm, data.minTerm, inputLoanTerm);
                submitBtn.removeClass('disabled').prop('disabled', false);
                if (data.companies) {
                    $('#companies').html('');
                    for (key in data.companies) {
                        $('#companies').append($('<option selected />').val(key).text(data.companies[key]));
                    };
                    // initSleect2();
                    $('#companies').val(Object.keys(data.companies || {}));
                    $('#companies').trigger('change');
                }
            }
        })
    });
});
JS;
$this->registerJs($js, \yii\web\View::POS_END);
