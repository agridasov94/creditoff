<?php
/**
 * @var $this \yii\web\View
 * @var $form \yii\widgets\ActiveForm
 * @var $loanRequestForm \frontend\src\useCase\loanRequest\shortForm\LoanRequestCreateForm
 * @var $isActive bool
 * @var $product \common\src\entities\product\entity\Product|null
 */

use backend\src\entities\partnerCompany\entity\PartnerCompanyQuery;
use common\src\entities\client\entity\Client;
use yii\helpers\Html;

?>

<div class="row">
    <div class="col-md-12">
        <?= $form->errorSummary($loanRequestForm->personalData, ['class' => 'alert alert-danger']) ?>
    </div>
</div>
<?= $this->render('_details', [
    'form' => $form,
    'loanRequestForm' => $loanRequestForm,
    'product' => $product
]) ?>
<div id="personal_data" class="target">
    <div class="row row-30">
        <div class="col-md-6">
            <?= $form->field($loanRequestForm->personalData, 'firstName', [
                'options' => [
                    'class' => 'form-wrap'
                ],
                'template' => '{label}{input}<span class="form-validation">{error}</span>'
            ])
                ->textInput(['class' => 'form-input form-control-has-validation', 'minlength' => true, 'maxlength' => true])
            ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($loanRequestForm->personalData, 'lastName', [
                'options' => [
                    'class' => 'form-wrap'
                ],
                'template' => '{label}{input}<span class="form-validation">{error}</span>'
            ])
                ->textInput(['class' => 'form-input form-control-has-validation', 'minlength' => true, 'maxlength' => true])
            ?>
        </div>
        <div class="col-md-6">
            <div class="form-wrap">
                <?= $form->field($loanRequestForm->personalData, 'dateOfBirth', [
                    'options' => [
                        'class' => 'form-wrap'
                    ],
                    ])->widget(\kartik\date\DatePicker::class, [
                        'pluginOptions' => [
                            'format' => 'dd-mm-yyyy',
                            'autoclose' => true
                        ],
                        'options' => [
                                'class' => 'form-control form-control-has-validation'
                            ]
                        ]) ?>
            </div>
        </div>
        <div class="col-md-6">
            <?= $form->field($loanRequestForm->personalData, 'idnp', [
                'options' => [
                    'class' => 'form-wrap'
                ],
                'template' => '{label}{input}<span class="form-validation">{error}</span>'
            ])
                ->textInput(['class' => 'form-input form-control-has-validation', 'minlength' => true, 'maxlength' => true])
            ?>
        </div>

        <div class="col-md-6">
            <div class="form-wrap">
                <?= $form->field($loanRequestForm->personalData, 'phoneNumber')->input('text') ?>
            </div>
        </div>
        <div class="col-md-6">
            <?= $form->field($loanRequestForm->personalData, 'email', [
                'options' => [
                    'class' => 'form-wrap'
                ],
                'template' => '{label}{input}<span class="form-validation">{error}</span>'
            ])
                ->input('email', ['class' => 'form-input form-control-has-validation', 'minlength' => true, 'maxlength' => true])
            ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($loanRequestForm->personalData, 'gender', [
                'options' => [
                    'class' => 'form-wrap',
                    'id' => 'gender'
                ],
                'template' => '{label}{input}<span class="form-validation">{error}</span>'
            ])->dropDownList(Client::getGenderListName(), [
                'class' => 'form-select form-control-has-validation',
//                'required' => true,
                'style' => 'margin-top: 0'
            ]) ?>
        </div>
        <div class="col-12">
            <?= $form->field($loanRequestForm->personalData, 'comment', [
                'options' => [
                    'class' => 'form-wrap'
                ],
            ])
            ->textarea(['class' => 'form-input form-control-has-validation'])
            ->label(null, ['class' => 'form-label rd-input-label'])
            ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($loanRequestForm->personalData, 'policy', [
                'template' => '{input} {label}'
            ])->checkbox([
                'class' => 'checkbox',
//                'required' => true
            ], false)->label(null, ['class' => 'lab-set-sec']) ?>
        </div>
    </div>
<!--    <div class="row row-offset-5">-->
<!--        <div class="col-md-6">-->
            <?php /* \yii\helpers\Html::button(Yii::t('frontend', 'Next Step'), [
                'class' => 'button button-ebony-clay-outline button-block btn-next-step'
            ]) */ ?>
<!--        </div>-->
<!--    </div>-->
    <div class="row">
        <div class="col-md-4 offset-md-4">
            <?= \yii\helpers\Html::submitButton(Yii::t('frontend', 'Create profile'), [
                'class' => 'button button-ebony-clay-outline button-block',
                'id' => 'submit-create-loan-application-btn'
            ]) ?>
        </div>
    </div>
</div>

<?php
if (Yii::$app->request->isPjax) {
    $js = <<<JS
(function () {
let selectFilter = $('select');
for (var i = 0; i < selectFilter.length; i++) {
    var select = $(selectFilter[i]);
    select.select2({
        theme: select.attr('data-custom-theme') ? select.attr('data-custom-theme') : "bootstrap",
        multiple: !!select.attr('data-multiple')
    }).next().addClass(select.attr("class").match(/(input-sm)|(input-lg)|($)/i).toString().replace(new RegExp(",", 'g'), " "));
}
})();
JS;
    $this->registerJs($js);
}