<?php
/**
 * @var $this \yii\web\View
 * @var $handleRequestForm \frontend\src\useCase\loanRequest\shortForm\HandleRequestForm
 * @var $codeExpired bool
 * @var $expirationTime int
 */

use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

$displayFormObjectsClass = $expirationTime ? '' : 'hidden';
$displayResendCodeObjectsClass = !$expirationTime ? '' : 'hidden';
?>
<div id="handle_request" class="target target-active">
    <?= \common\widgets\Alert::widget() ?>

    <?php if($codeExpired): ?>
        <?= \yii\bootstrap4\Alert::widget([
            'body' => Yii::t('frontend', 'Verification code expired'),
            'options' => [
                'class' => 'alert alert-warning'
            ],
            'closeButton' => false
        ]) ?>
    <?php endif; ?>

    <?= \yii\bootstrap4\Alert::widget([
        'body' => Yii::t('frontend', 'This is the last step. Set a password for your profile and submit the form by entering an activation code that will be sent to your phone via SMS.'),
        'options' => [
            'class' => 'alert alert-info'
        ],
        'closeButton' => false
    ]) ?>

    <?php Pjax::begin(); ?>
    <?php $form = ActiveForm::begin(['options' => ['class' => 'rd-mailform', 'data-pjax' => 1], 'enableClientScript' => false]) ?>
        <?= $form->field($handleRequestForm, 'clientUid')->hiddenInput()->label(false) ?>
        <div class="row">
            <div class="col-md-12">
                <?= $form->errorSummary($handleRequestForm, ['class' => 'alert alert-danger']) ?>
            </div>
        </div>
        <div class="row form-object <?= $displayFormObjectsClass ?>">
            <div class="col-md-4 offset-md-4">
                <?= $form->field($handleRequestForm, 'password')->input('password', ['class' => 'form-input form-control-has-validation']) ?>
            </div>
        </div>
        <div class="row form-object <?= $displayFormObjectsClass ?>">
            <div class="col-md-4 offset-md-4">
                <?= $form->field($handleRequestForm, 'rePassword')->input('password', ['class' => 'form-input form-control-has-validation']) ?>
            </div>
        </div>
        <div class="row form-object <?= $displayFormObjectsClass ?>">
            <div class="col-md-4 offset-md-4">
                <?= $form->field($handleRequestForm, 'activationCode')->input('text', ['class' => 'form-input form-control-has-validation']) ?>
            </div>
        </div>
        <div class="row resend-code-object <?= $displayResendCodeObjectsClass ?>">
            <div class="col-md-4 offset-md-4">
                <?= \yii\helpers\Html::button(Yii::t('frontend', 'Resend Verifiaction Code'), [
                    'class' => 'button button-ebony-clay-outline button-block',
                    'id' => 'resend-verification-code'
                ]) ?>
            </div>
        </div>
        <div class="row form-object <?= $displayFormObjectsClass ?>" data-id="countdown">
            <div class="col-md-4 offset-md-4">
                <span>
                    <span data-id="title"><?= Yii::t('frontend', 'Resend after:') ?></span>
                    <span data-id="time"><?= $expirationTime ?? 0 ?></span>
                    <span><?= Yii::t('frontend', 'Sec.') ?></span>
                </span>
            </div>
        </div>
        <div class="row form-object <?= $displayFormObjectsClass ?>">
            <div class="col-md-4 offset-md-4">
                <?= \yii\helpers\Html::submitButton(Yii::t('frontend', 'Create profile'), [
                    'class' => 'button button-ebony-clay-outline button-block'
                ]) ?>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
<?php
$js = <<<JS
var countDownEl = document.querySelector('[data-id="countdown"]');
var timeEl = countDownEl.querySelector('[data-id="time"]');
var expirationTime = {$expirationTime};
window.countDown = function (time) {
    timeEl.textContent = time;
    if (--time > 0) {
        setTimeout(function () {
            countDown(time);
        }, 1000);
        window.startCountDown = true;
    } else {
        $(countDownEl).fadeOut(150, function () {
            $('.form-object').addClass('hidden');
            $('.resend-code-object').removeClass('hidden');
        });
    }
};
if (expirationTime) {
    countDown(expirationTime);
}
JS;
$this->registerJs($js);
?>
    <?php Pjax::end(); ?>
</div>

<?php

$url = \yii\helpers\Url::to(['/loan-request/ajax-resend-verification-code']);
$js = <<<JS
$(document).ready(function () {
    $('#resend-verification-code').on('click', function (e) {
        e.preventDefault();
        let ciud = '{$handleRequestForm->clientUid}';
        let btn = $(this);
        let btnHtml = btn.html();
        btn.html('<i class="fa fa-spin fa-spinner"></i>');
        btn.addClass('disabled').prop('disabled', true);
        $('.resend-verification-code').remove();
        $.ajax({
            url: '{$url}',
            type: 'post',
            data: {cuid: ciud},
            dataType: 'json',
            success: function (data) {
                if (data.error) {
                    let alertError = $('<div class="alert alert-danger alert resend-verification-code"></div>');
                    alertError.html(data.message);
                    $('.rd-mailform').prepend(alertError);
                } else {
                    let alertSuccess = $('<div class="alert alert-success alert resend-verification-code"></div>');
                    alertSuccess.html(data.message);
                    $('.rd-mailform').prepend(alertSuccess);
                    $('.form-object').removeClass('hidden');
                    $('.resend-code-object').addClass('hidden');
                    window.countDown(data.timeExpiration);
                    $('[data-id="countdown"]').show();
                }
            },
            error: function (xhr) {
                let alertError = $('<div class="alert alert-danger alert resend-verification-code"></div>');
                alertError.html(xhr.responseText);
                $('.rd-mailform').prepend(alertError);
            },
            complete: function () {
                btn.html(btnHtml);
                btn.removeClass('disabled').prop('disabled', false);
            }
        });
    });
});
JS;
$this->registerJs($js);

