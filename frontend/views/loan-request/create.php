<?php
/**
 * @var $this \yii\web\View
 * @var $loanRequestForm LoanRequestCreateForm
 * @var $product \common\src\entities\product\entity\Product|null
 */

use frontend\src\useCase\loanRequest\shortForm\LoanRequestCreateForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

$pjaxId = 'short-form-create-loan-request-pjax';
$recaptchaSiteKey = Yii::$app->reCaptcha3->site_key;
?>
<section class="section-top-60 section-md-top-100 section-bottom-60 section-md-bottom-100 bg-whisperapprox" id="short-form-loan-request">
    <div class="container">
        <div class="row">
<!--            <div class="col-md-2">-->
<!--                <div class="left_menu">-->
<!--                    <div class="menu_content styled_menu" >-->
<!--                        <ul class="nav_menu ">-->
<!--                            <li>-->
<!--                                <a class="links" href="#">Personal Data</a>-->
<!--                            </li>-->
<!---->
<!--                            <li class="list_menu has_child_menu">-->
<!--                                <a class="links  " href="#">-->
<!--                                    Сredit request <span class="fa fa-angle-down"></span>-->
<!--                                </a>-->
<!--                                <ul class="child_content">-->
<!--                                    <li><a href="#">Option1</a></li>-->
<!--                                    <li><a href="#">Option2</a></li>-->
<!--                                </ul>-->
<!--                            </li>-->
<!--                            <li>-->
<!--                                <a class="links" href="#">Create request</a>-->
<!--                            </li>-->
<!--                        </ul>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
            <?php Pjax::begin(['id' => $pjaxId, 'enableReplaceState' => false, 'enablePushState' => false]) ?>
            <div class="col-md-12">
                <div class="row">
                    <div class="progressbar-wrapper">
                        <ul class="progressbar">
                            <?php foreach (LoanRequestCreateForm::getStepList() as $key => $label): ?>
                                <li class="<?= $loanRequestForm->getProgressbarClassByStep($key) ?>" data-target="<?= $key ?>"><?= $label ?></li>
                            <?php endforeach; ?>
<!--                            <li class="active" data-target="personal_data">Персональные данные</li>-->
<!--                            <li data-target="handle_request">Обработка запроса на кредит</li>-->
<!--                            <li data-target="payment_money">Выдача денег</li>-->
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?php $form = ActiveForm::begin(['id' => 'loan-application-form', 'enableClientValidation' => false, 'options' => ['data-pjax' => 1]]) ?>
                        <?= $form->field($loanRequestForm, 'reCaptcha', ['options' => ['id' => 'recaptcha']])->widget(\kekaadrenalin\recaptcha3\ReCaptchaWidget::class) ?>
                        <?= $this->render('partial/_personal_data', [
                            'form' => $form,
                            'loanRequestForm' => $loanRequestForm,
                            'product' => $product
                        ]) ?>
<!--                        --><?php //= $this->render('partial/_details', [
//                            'form' => $form,
//                            'loanRequestForm' => $loanRequestForm,
//                            'product' => $product
//                        ]) ?>
<!--                            <div id="handle_request" class="target"></div>-->
<!--                            <div id="payment_money" class="target"></div>-->
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
<?php
$companies = json_encode($loanRequestForm->loanDetails->companies);
$js = <<<JS
$('.progressbar li.step').on('click', function () {
    let target = $(this).data('target');
    $('.progressbar li.step').removeClass('active');
    $(this).addClass('active');
    $('.target').hide();
    $('#'+target).show();
});
$('.btn-next-step').on('click', function (e) {
    e.preventDefault();
    $('.target').hide();
    $(this).closest('.target').next().show();
})

$(document).ready( function () {
    let target = $('.progressbar li.step.active').data('target');
    $('.target').hide();
    $('#'+target).show();
});

$(document).off('click', '#submit-create-loan-application-btn}').on('click', 'submit-create-loan-application-btn', function (obj, xhr, data) {
    e.preventDefault();
    if (!$('#recaptcha').val()) {
        grecaptcha.ready(function () {
            grecaptcha.execute('{$recaptchaSiteKey}', {action: 'homepage'}).then(function (token) {
                $('#recaptcha').val(token);
                $('#loan-application-form').submit();
            });
        });
        return false;
    } else {
        return true;
    }
});

//( function () {
//let companiesVal = JSON.parse('$companies');
//window.initSleect2 = function () {
//    let companies = $('#companies');
//    companies.select2({
//        theme: companies.attr('data-custom-theme') ? companies.attr('data-custom-theme') : "bootstrap",
//        multiple: !!companies.attr('data-multiple')
//    }).next().addClass(companies.attr("class").match(/(input-sm)|(input-lg)|($)/i).toString().replace(new RegExp(",", 'g'), " "));
//    if (Array.isArray(companiesVal)) {
//        companies.val((companiesVal));
//    } else {
//        companies.val(Object.keys(companiesVal || {}));
//    }
//    companies.trigger('change');
//}
//initSleect2();
//})();
JS;
$this->registerJs($js);
?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</section>

<script>
    // const drop = document.querySelector(".child_content"),
    //     menu_cont = document.querySelector(".list_menu"),
    //     nav_menu = document.querySelectorAll(".has_child_menu");
    //
    // function showmenuContent() {
    //     this.classList.toggle('opened');
    //     this.querySelector('.child_content').classList.toggle("show_sub_menu");
    // }
    //
    // nav_menu.forEach( (elem) => {
    //     elem.addEventListener('click', showmenuContent);
    // });
</script>

<?php
$js = <<<JS
setInterval(() => $('#loanrequestcreateform-recaptcha').val(""), 15000);
JS;
$this->registerJs($js);


