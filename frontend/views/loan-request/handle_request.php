<?php
/**
 * @var $this \yii\web\View
 * @var $handleRequestForm \frontend\src\useCase\loanRequest\shortForm\HandleRequestForm
 * @var $codeExpired bool
 * @var $expirationTime int
 */
?>
<section class="section-top-60 section-md-top-100 section-bottom-60 section-md-bottom-100 bg-whisperapprox" id="short-form-loan-request">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="progressbar-wrapper">
                        <ul class="progressbar">
                            <li class="done"><?= \Yii::t('frontend', 'Details') ?></li>
                            <li class="done"><?= \Yii::t('frontend', 'Personal Data') ?></li>
                            <li class="active" data-target="handle_request"><?= \Yii::t('frontend', 'Handle Request') ?></li>
                            <li><?= \Yii::t('frontend', 'Payment of Money') ?></li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?= $this->render('partial/_handle_request', [
                            'handleRequestForm' => $handleRequestForm,
                            'codeExpired' => $codeExpired,
                            'expirationTime' => $expirationTime
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


