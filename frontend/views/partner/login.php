<?php
/**
 * @var \yii\web\View $this
 * @var \frontend\src\forms\login\LoginPartnerForm $model
 */

use yii\widgets\ActiveForm;
?>
<section class="section section-single novi-background bg-gray-darker novi-background" style="background-image: url(/images/bg-login.jpg);">
    <div class="section-single-inner">
        <header class="section-single-header">
            <div class="page-head-inner">
                <div class="container text-center"><a class="brand brand-md brand-inverse" href="/"><img src="/images/logo-light-113x45.png" alt="" width="113" height="45"/></a></div>
            </div>
        </header>
        <div class="section-single-main">
            <div class="container">
                <div class="row justify-content-md-center">
                    <div class="col-md-7 col-lg-5 col-xl-4">
                        <div class="block-shadow">
                            <div class="block-inner">
                                <p class="text-uppercase text-bold text-dark"><?= Yii::t('frontend', 'Login to your account') ?></p><span class="novi-icon icon icon-xl icon-black block-icon material-icons-lock_open"></span>
                            </div>
                            <?php $form = ActiveForm::begin([
                                'options' => [
                                    'class' => 'form-modern form-darker'
                                ]
                            ]) ?>
                            <?= $form->field($model, 'reCaptcha')->widget(\kekaadrenalin\recaptcha3\ReCaptchaWidget::class) ?>
                                <div class="block-inner">
                                    <?= $form->field($model, 'login', [
                                        'options' => [
                                            'class' => 'form-wrap'
                                        ]
                                    ])->textInput([
                                        'class' => 'form-input',
                                    ])->label(null, [
                                        'class' => 'form-label'
                                    ])->error([
                                        'class' => 'form-validation'
                                    ]) ?>

                                    <?= $form->field($model, 'password', [
                                        'options' => [
                                            'class' => 'form-wrap'
                                        ]
                                    ])->passwordInput([
                                        'class' => 'form-input',
                                    ])->label(null, [
                                        'class' => 'form-label'
                                    ])->error([
                                        'class' => 'form-validation'
                                    ]) ?>
                                    <div class="form-wrap">
                                        <?= \yii\helpers\Html::a(Yii::t('frontend', 'Forgot your password?'), ['/partner/restore-password'], [
                                            'class' => 'text-gray-05'
                                        ]) ?>
                                    </div>
                                </div>
                                <button class="button button-primary button-block" style="border-top-right-radius: 0 !important;border-top-left-radius: 0 !important;border-color: #ffffff94;" type="submit"><?= Yii::t('frontend', 'Sign in') ?></button>
                            <?php ActiveForm::end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-single-footer">
            <div class="container text-center">
                <div class="row">
                    <div class="col-12">
                        <p class="rights"><span><?= Yii::t('frontend', 'Investment Smart') ?></span><span>&nbsp;&#169;&nbsp;</span><span class="copyright-year"></span><span><?= Yii::t('frontend', 'All Rights Reserved') ?></span><br class="d-md-none"><a class="link-primary-inverse" href="#"><?= Yii::t('frontend', 'Terms of Use') ?></a><span><?= Yii::t('frontend', 'and') ?></span><a class="link-primary-inverse" href="privacy-policy.html"><?= Yii::t('frontend', 'Privacy Policy') ?></a></p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>