<?php
/**
 * @var $this \yii\web\View
 * @var $loanApplications \common\src\entities\loanApplication\entity\LoanApplication[]
 **/

use common\src\entities\loanApplication\entity\LoanApplicationStatus;
use yii\helpers\Html;
use yii\helpers\Url;

$this->params['breadcrumbs'][] = Yii::t('frontend', 'Loan request list');
?>

<section class="section-top-60 section-md-top-100 section-bottom-60 section-md-bottom-100 bg-whisperapprox">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center"><?= Yii::t('frontend', 'Manage your Loan request list') ?></h3>
            </div>
        </div>

        <?= \common\widgets\Alert::widget() ?>

        <div class="row">
            <div class="col-md-12">
                <a href="<?= Url::to(['cabinet/loan-application-create']) ?>" class="btn btn-primary create_btn"><i class="fa fa-plus"></i> <?= Yii::t('frontend', 'Create request') ?></a>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="table">
                    <table class="styled-table">
                        <thead>
                        <tr>
                            <th><?= Yii::t('frontend', 'Loan Application number') ?></th>
                            <th><?= Yii::t('frontend', 'Amount') ?></th>
                            <th><?= Yii::t('frontend', 'Term') ?></th>
                            <th><?= Yii::t('frontend', 'Loan Type') ?></th>
                            <th><?= Yii::t('frontend', 'Date') ?></th>
                            <th><?= Yii::t('frontend', 'Stage') ?></th>
                            <th><?= Yii::t('frontend', 'Lender') ?></th>
                            <th><?= Yii::t('frontend', 'Actions') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php foreach($loanApplications as $loanApplication): ?>
                                <tr>
                                    <td><?= $loanApplication->la_number ?></td>
                                    <td><?= Yii::t('frontend', '{amount} Lei', [
                                        'amount' => $loanApplication->la_loan_amount
                                    ])?></td>
                                    <td><?= Yii::t('frontend', '{term} Month', [
                                        'term' => $loanApplication->la_loan_term
                                    ]) ?></td>
                                    <td><?= $loanApplication->product->p_name ?></td>
                                    <td><?= Yii::$app->formatter->asByUserDate($loanApplication->la_created_dt) ?></td>
                                    <td><?= LoanApplicationStatus::getName($loanApplication->la_status) ?></td>
                                    <td>
                                        <?= \frontend\src\helper\LoanApplicationHelper::getLoanApplicationLender($loanApplication, '<br>') ?>
                                    </td>
                                    <td class="td-loan-application-list">
                                        <a href="javascript:" class="loan-application-item-menu">
                                            <span class="fa fa-bars"></span>
                                        </a>
                                        <div class="dropdown-content">
                                            <?= Html::a('<i class="fa fa-eye"></i> ' . Yii::t('frontend', 'View Details'), "#", [
                                                'class' => 'view-loan-application',
                                                'application-number' => $loanApplication->la_number
                                            ]) ?>
                                            <?php if ($loanApplication->isSent()): ?>
                                                <div>
                                                    <?= Html::a('<i class="fa fa-pencil-square"></i> ' . Yii::t('frontend', 'Edit loan application'), Url::to(['/cabinet/update-loan-application', 'applicationNumber' => $loanApplication->la_number])) ?>
                                                </div>
                                            <?php endif; ?>
                                            <?php if ($loanApplication->isSent() || $loanApplication->isInWorkStatus() || $loanApplication->isApproved()): ?>
                                                <div>
                                                    <?= Html::a('<i class="fa fa-ban"></i> ' . Yii::t('frontend', 'Cancel loan application'), "#", [
                                                        'class' => 'cancel-loan-application',
                                                        'application-number' => $loanApplication->la_number
                                                    ]) ?>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
</section>

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModal" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <div class="modal-body">
                <p><?= Yii::t('frontend', 'Сonfirm the cancellation of the application') ?></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><?= Yii::t('frontend', 'Cancel') ?></button>
                <a class="btn btn-primary" id="cancel-loan-application" href="#"><?= Yii::t('frontend', 'Confirm') ?></a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="viewLoanApplicationModal" tabindex="-1" aria-labelledby="viewLoanApplicationModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><?= Yii::t('frontend', 'Close') ?></button>
            </div>
        </div>
    </div>
</div>
<?php

$url = Url::to(['/cabinet/loan-application/cancel']);
$viewUrl = Url::to(['/cabinet/loan-application/view']);
$js = <<<JS
var modalEl = document.getElementById('exampleModal');
var modalViewEl = document.getElementById('viewLoanApplicationModal');
var number;
modalEl.addEventListener('hidden.bs.modal', function (event) {
    $('#exampleModal .modal-body').find('.alert').remove();
});
var modal = new bootstrap.Modal(modalEl);
var modalView = new bootstrap.Modal(modalViewEl);
$(document).on('click', '.cancel-loan-application', function (e) {
    e.preventDefault();
    number = $(this).attr('application-number');
    modal.show();
});
$(document).on('click', '.view-loan-application', function (e) {
    e.preventDefault();
    number = $(this).attr('application-number');
    let btn = $(this);
    let btnHtml = btn.html();
    btn.html('<i class="fa fa-spin fa-spinner"></i>');
    btn.addClass('disabled').prop('disabled', true);
    $.ajax({
        url: "$viewUrl",
        type: 'post',
        data: {number: number},
        dataType: 'html',
        success: function (data) {
            $('#viewLoanApplicationModal .modal-body').html(data);
            modalView.show();
        },
        error: function (xhr) {
            alert(xhr.responseText);
        },
        complete: function () {
            btn.html(btnHtml);
            btn.removeClass('disabled').prop('disabled', false);
        }
    });
});
$(document).on('click', '#cancel-loan-application', function (e) {
    e.preventDefault();
    let btn = $(this);
    let btnHtml = btn.html();
    btn.html('<i class="fa fa-spin fa-spinner"></i>');
    btn.addClass('disabled').prop('disabled', true);
    $('#exampleModal .modal-body').find('.alert').remove();
    let alert = $('<div class="alert alert-danger" role="alert"></div>');
    $.ajax({
        url: "$url",
        type: 'post',
        data: {number: number},
        dataType: 'json',
        success: function (data) {
            if (data.error) {
                alert.html(data.message);
                $('#exampleModal .modal-body').append(alert);
                return;
            }
            location.reload();
        },
        error: function (xhr) {
            alert.html(xhr.responseText);
            $('#exampleModal .modal-body').append(alert[0].outerHTML);
        },
        complete: function () {
            btn.html(btnHtml);
            btn.removeClass('disabled').prop('disabled', false);
        }
    });
});
$('.loan-application-item-menu').on('click', function (e) {
    e.preventDefault();
    $('.td-loan-application-list .dropdown-content').removeClass('show');
    $(this).closest('.td-loan-application-list').find('.dropdown-content').toggleClass("show");
});
$(document).click(function(event){
    if(!$(event.target).closest(".td-loan-application-list").length){
        $('.td-loan-application-list .dropdown-content').removeClass('show');
        $(this).closest('.td-loan-application-list').find('.dropdown-content').removeClass("show");
    }
});
JS;

$this->registerJs($js, \yii\web\View::POS_END);
