<?php
/**
 * @var $this \yii\web\View
 * @var $fullRequestForm LoanRequestFullForm
 */

use frontend\src\useCase\loanRequest\fullForm\LoanRequestFullForm;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

$pjaxId = 'full-form-create-loan-request-pjax';
?>


<?php Pjax::begin(['id' => $pjaxId, 'enableReplaceState' => false, 'enablePushState' => false]) ?>
<section class="section-top-60 section-md-top-100 section-bottom-60 section-md-bottom-100 bg-whisperapprox" id="short-form-loan-request">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="progressbar-wrapper">
                        <ul class="progressbar">
                            <?php foreach (LoanRequestFullForm::getStepList() as $key => $label): ?>
                                <li class="<?= $fullRequestForm->getProgressbarClassByStep($key) ?>" data-target="<?= $key ?>"><?= $label ?></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <?php $form = ActiveForm::begin(['options' => ['data-pjax' => 1, 'novalidate' => true], 'enableClientScript' => false, 'enableClientValidation' => false, 'enableAjaxValidation' => false]) ?>
                        <div class="row">
                            <div class="col-md-12">
                                <?= $form->errorSummary($fullRequestForm, ['class' => 'alert alert-danger']) ?>
                            </div>
                        </div>
                        <?= $this->render('partial/fullForm/_details', [
                            'form' => $form,
                            'fullRequestForm' => $fullRequestForm,
                        ]) ?>
                        <?= $this->render('partial/fullForm/_personal_info', [
                            'form' => $form,
                            'fullRequestForm' => $fullRequestForm,
                        ]) ?>

                        <?= $this->render('partial/fullForm/_employed_data', [
                            'form' => $form,
                            'fullRequestForm' => $fullRequestForm,
                        ]) ?>

                        <?= $this->render('partial/fullForm/_passport_data', [
                            'form' => $form,
                            'fullRequestForm' => $fullRequestForm,
                        ]) ?>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
$companies = json_encode($fullRequestForm->loanDetails->companies);
$js = <<<JS
$(document).ready( function () {
    $(document).on('click', '.progressbar li.step', function () {
        let target = $(this).data('target');
        $('.progressbar li.step').removeClass('active');
        $(this).addClass('active');
        $('.target').hide();
        $('#'+target).show();
    });
    
    
    if ($('.progressbar li.step').hasClass('error')) {
        $('.progressbar li.step.active').removeClass('active');
        $('.progressbar li.step.error').addClass('active');
        let target = $('.progressbar li.step.error').data('target');
        $('.target').hide();
        $('#'+target).show();
    } else {
        let target = $('.progressbar li.step.active').data('target');
        $('.target').hide();
        $('#'+target).show();
    }
    
    $(document).on('click', '.btn-next-step', function (e) {
        e.preventDefault();
        $('.target').hide();
        let target = $(this).closest('.target').next();
        let targetId = target.attr('id');
        $('.progressbar li').removeClass('active');
        $('.progressbar li[data-target="'+targetId+'"]').addClass('active');
        target.show();
    });
    
( function () {
//let companiesVal = JSON.parse('$companies');
//function initSleect2() {
//    let companies = $('#companies');
//    companies.select2({
//        theme: companies.attr('data-custom-theme') ? companies.attr('data-custom-theme') : "bootstrap",
//        multiple: !!companies.attr('data-multiple')
//    }).next().addClass(companies.attr("class").match(/(input-sm)|(input-lg)|($)/i).toString().replace(new RegExp(",", 'g'), " "));
//    if (Array.isArray(companiesVal)) {
//        companies.val((companiesVal));
//    } else {
//        companies.val(Object.keys(companiesVal || {}));
//    }
//    companies.trigger('change');
//}
// initSleect2();
})();
});
JS;
$this->registerJs($js);
?>
<?php Pjax::end(); ?>
