<?php
/**
 * @var $this \yii\web\View
 * @var $editClientDataForm \frontend\src\useCase\client\editPersonalData\EditClientDataForm
 * @var $form \yii\bootstrap4\ActiveForm
 */

use borales\extensions\phoneInput\PhoneInput;
use common\src\entities\client\entity\Client;
use frontend\src\useCase\client\editPersonalData\ChildrenForm;
use kartik\date\DatePicker;
use unclead\multipleinput\MultipleInput;
use unclead\multipleinput\MultipleInputColumn;

?>

<div class="row">
    <div class="col-md-6">
        <?= $form->field($editClientDataForm->client, 'c_name', [
            'options' => [
                'class' => 'form-wrap'
            ],
        ])->textInput([
            'readonly' => true,
            'class' => 'form-control form-control-has-validation bg-gray-light',
            'name' => 'name'
        ]) ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($editClientDataForm->client, 'c_last_name', [
            'options' => [
                'class' => 'form-wrap'
            ],
        ])->textInput([
            'readonly' => true,
            'class' => 'form-control form-control-has-validation bg-gray-light',
            'name' => 'lastname'
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <?= $form->field($editClientDataForm->client, 'c_birth_date', [
            'options' => [
                'class' => 'form-wrap'
            ],
            'template' => '{label}{input}<span class="form-validation">{error}</span>',
        ])->textInput([
            'readonly' => true,
            'class' => 'form-control form-control-has-validation bg-gray-light',
            'name' => 'birth_date'
        ]) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($editClientDataForm->client, 'c_idnp', [
            'options' => [
                'class' => 'form-wrap'
            ],
            'template' => '{label}{input}<span class="form-validation">{error}</span>'
        ])->textInput([
            'readonly' => true,
            'class' => 'form-control form-control-has-validation bg-gray-light',
            'name' => 'idnp'
        ])
        ?>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <?= $form->field($editClientDataForm->client, 'c_gender', [
            'options' => [
                'class' => 'form-wrap'
            ],
            'template' => '{label}{input}<span class="form-validation">{error}</span>'
        ])->textInput([
            'readonly' => true,
            'class' => 'form-control form-control-has-validation bg-gray-light',
            'value' => Client::getGenderName((int)$editClientDataForm->client->c_gender),
            'name' => 'gender'
        ])
        ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($editClientDataForm->client, 'c_civil_status', [
            'options' => [
                'class' => 'form-wrap'
            ],
            'template' => '{label}{input}<span class="form-validation">{error}</span>'
        ])->dropdownList(Client::getCivilStatusListName(), [
            'prompt' => '---',
            'class' => 'form-control form-control-has-validation',
            'value' => $editClientDataForm->client->c_civil_status ? $editClientDataForm->client->c_civil_status : null
        ])
        ?>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <label for=""><?= Yii::t('client', 'Email') ?></label>
        <?= MultipleInput::widget([
            'name' => 'ClientEmail',
            'columns' => [
                [
                    'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                    'name' => 'ce_email',
                    'options' => [
                        'type' => 'email',
                        'style' => 'margin-bottom: 10px'
                    ]
                ],
                [
                    'type' => MultipleInputColumn::TYPE_HIDDEN_INPUT,
                    'name' => 'ce_id'
                ]
            ],
            'value' => $editClientDataForm->clientEmail,
            'iconSource' => MultipleInput::ICONS_SOURCE_FONTAWESOME,
            'addButtonOptions' => [
                'class' => 'btn btn-success'
            ]
        ]) ?>
    </div>

    <div class="col-md-6">
        <label for=""><?= Yii::t('client', 'Phone') ?></label>
        <?= MultipleInput::widget([
            'name' => 'ClientPhone',
            'columns' => [
                [
                    'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                    'name' => 'cp_phone',
                    'options' => [
                        'type' => 'number',
                        'style' => 'margin-bottom: 10px'
                    ]
//                    'type' => PhoneInput::class,
//                    'name' => 'cp_phone',
//                    'options' => [
//                        'jsOptions' => [
//                            'nationalMode' => false,
//                            'preferredCountries' => ['md'],
//                            'customContainer' => 'intl-tel-input',
//                            'onlyCountries' => ['md']
//                        ],
//                        'options' => [
//                            'onkeydown' => '
//                                                return !validationField.validate(event);
//                                            ',
//                            'onkeyup' => '
//                                                var value = $(this).val();
//                                                $(this).val(value.replace(/[^0-9\+]+/g, ""));
//                                            ',
//                        ]
//                    ]
                ],
                [
                    'type' => MultipleInputColumn::TYPE_HIDDEN_INPUT,
                    'name' => 'cp_id'
                ]
            ],
            'value' => $editClientDataForm->clientPhone,
            'iconSource' => MultipleInput::ICONS_SOURCE_FONTAWESOME,
            'addButtonOptions' => [
                'class' => 'btn btn-success'
            ]
        ]) ?>
    </div>
</div>

<div class="row" style="margin-top: 25px">
    <div class="col-md-6">
        <?= $form->field($editClientDataForm->client, 'c_social_status', [
            'options' => [
                'class' => 'form-wrap'
            ],
            'template' => '{label}{input}<span class="form-validation">{error}</span>'
        ])->dropdownList(Client::getSocialStatusListName(), [
            'prompt' => '---',
            'class' => 'form-control form-control-has-validation',
            'value' => $editClientDataForm->client->c_social_status ? $editClientDataForm->client->c_social_status : null
        ])
        ?>
    </div>

    <div class="col-md-6">
        <label for=""><?= Yii::t('client', 'Child Date Of Birth') ?></label>
        <?= MultipleInput::widget([
            'name' => ChildrenForm::getFormName(),
            'columns' => [
                [
                    'type' => DatePicker::class,
                    'name' => 'dateOfBirth',
                    'options' => [
                        'pluginOptions' => [
                            'format' => 'dd-mm-yyyy',
                            'autoclose' => true
                        ],
                        'options' => [
                            'style' => 'margin-bottom: 10px;'
                        ]
                    ],
                ],
                [
                    'type' => MultipleInputColumn::TYPE_HIDDEN_INPUT,
                    'name' => 'confirm'
                ]
            ],
            'value' => $editClientDataForm->children,
            'iconSource' => MultipleInput::ICONS_SOURCE_FONTAWESOME,
            'addButtonOptions' => [
                'class' => 'btn btn-success'
            ],
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <?= $form->field($editClientDataForm->client, 'c_own_property')->radioList(['0' => Yii::t('frontend', 'No'), '1' => Yii::t('frontend', 'Yes')]) ?>
    </div>
</div>
