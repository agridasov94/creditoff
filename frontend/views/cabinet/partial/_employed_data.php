<?php
/**
 * @var $this \yii\web\View
 * @var $editClientDataForm \frontend\src\useCase\client\editPersonalData\EditClientDataForm
 * @var $form \yii\bootstrap4\ActiveForm
 */

use borales\extensions\phoneInput\PhoneInput;
use common\src\entities\client\employmentData\entity\ClientEmploymentData;
use unclead\multipleinput\MultipleInput;
use unclead\multipleinput\MultipleInputColumn;
use yii\helpers\Html;
$labelStyle = 'color: #73879C; font-size: 13px; font-weight: 100;';
?>

<div class="row employed_data">
    <div class="col-md-12">
        <?= MultipleInput::widget([
            'id' => 'employed_data_multiple_input',
            'name' => 'ClientEmploymentData',
            'rendererClass' => \unclead\multipleinput\renderers\DivRenderer::class,
            'columns' => [
                [
                    'type' => MultipleInputColumn::TYPE_DROPDOWN,
                    'name' => 'ced_type',
                    'items' => ClientEmploymentData::getTypeListName(),
                    'title' => Html::label(Yii::t('client', 'Type'), '', ['style' => $labelStyle]),
                    'options' => [
                        'prompt' => '---',
                    ],
                    'columnOptions' => [
                        'class' => 'col-md-6'
                    ]
                ],
                [
                    'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                    'name' => 'ced_work_phone',
                    'options' => [
                        'type' => 'number',
                        'style' => 'margin-bottom: 10px'
                    ],
//                    'type' => PhoneInput::class,
//                    'name' => 'ced_work_phone',
//                    'options' => [
//                        'jsOptions' => [
//                            'nationalMode' => false,
//                            'preferredCountries' => ['md'],
//                            'customContainer' => 'intl-tel-input',
//                            'onlyCountries' => ['md']
//                        ],
//                        'options' => [
//                            'onkeydown' => '
//                                                return !validationField.validate(event);
//                                            ',
//                            'onkeyup' => '
//                                                var value = $(this).val();
//                                                $(this).val(value.replace(/[^0-9\+]+/g, ""));
//                                            '
//                        ]
//                    ],
                    'title' => Html::label(Yii::t('client', 'Work Phone'), '', ['style' => $labelStyle]),
                    'columnOptions' => [
                        'class' => 'col-md-6'
                    ]
                ],
                [
                    'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                    'name' => 'ced_organization_name',
                    'title' => Html::label(Yii::t('client', 'Organization Name'), '', ['style' => $labelStyle]),
                    'columnOptions' => [
                        'class' => 'col-md-6'
                    ]
                ],
                [
                    'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                    'name' => 'ced_position',
                    'title' => Html::label(Yii::t('client', 'Position'), '', ['style' => $labelStyle]),
                    'columnOptions' => [
                        'class' => 'col-md-6'
                    ]
                ],
                [
                    'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                    'name' => 'ced_seniority',
                    'title' => Html::label(Yii::t('client', 'Seniority'), '', ['style' => $labelStyle]),
                    'columnOptions' => [
                        'class' => 'col-md-6'
                    ]
                ],
                [
                    'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                    'name' => 'ced_official_wages',
                    'title' => Html::label(Yii::t('client', 'Official Wages'), '', ['style' => $labelStyle]),
                    'columnOptions' => [
                        'class' => 'col-md-6'
                    ]
                ],
                [
                    'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                    'name' => 'ced_unofficial_wages',
                    'title' => Html::label(Yii::t('client', 'Unofficial Wages'), '', ['style' => $labelStyle]),
                    'columnOptions' => [
                        'class' => 'col-md-6'
                    ]
                ],
                [
                    'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                    'name' => 'ced_revenues_documented',
                    'title' => Html::label(Yii::t('client', 'Revenues Documented'), '', ['style' => $labelStyle]),
                    'columnOptions' => [
                        'class' => 'col-md-6'
                    ]
                ],
                [
                    'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                    'name' => 'ced_revenues_non_documented',
                    'title' => Html::label(Yii::t('client', 'Revenues Non Documented'), '', ['style' => $labelStyle]),
                    'columnOptions' => [
                        'class' => 'col-md-6'
                    ]
                ],
                [
                    'type' => MultipleInputColumn::TYPE_HIDDEN_INPUT,
                    'name' => 'ced_id',
                ],
            ],
            'layoutConfig' => [
                'wrapperClass' => '',
                'labelClass' => 'col-md-12',
                'buttonAddClass' => ''
            ],
            'rowOptions' => [
                'class' => 'row'
            ],
            'addButtonOptions' => [
                'class' => 'btn btn-success',
                'label' => '<i class="fa fa-plus"></i> ' . Yii::t('frontend', 'Add employed data')
            ],
            'removeButtonOptions' => [
                'class' => 'btn btn-danger',
                'label' => '<i class="fa fa-times"></i> ' . Yii::t('frontend', 'Remove')
            ],
            'addButtonPosition' => MultipleInput::POS_HEADER,
            'value' => $editClientDataForm->clientEmploymentData,
        ]) ?>
    </div>
</div>

<?php
$js = <<<JS
$('#employed_data_multiple_input').on('afterAddRow', function(e, row, currentIndex) {
    let div = $('<div />');
    div.addClass('d-flex justify-content-end');
    $(row).find('.js-input-remove').detach().prependTo($(row).closest('.multiple-input-list__item'));
    $(row).find('.js-input-remove').wrap(div);
})
JS;
$this->registerJs($js);

