<?php
/**
 * @var View $this
 * @var LoanApplication $loanApplication
 */

use common\src\entities\loanApplication\entity\LoanApplication;
use common\src\entities\loanApplication\entity\LoanApplicationStatus;
use frontend\src\helper\LoanApplicationHelper;
use yii\web\View;

?>


<div class="tabs-custom tabs-horizontal tabs-minimal" id="tabs-4">
    <ul class="nav nav-tabs justify-content-center text-center">
        <li><a class="active" href="#tab-1" data-bs-toggle="tab"><?= Yii::t('frontend', 'Loan Application Info') ?></a></li>
        <li><a href="#tab-2" data-bs-toggle="tab">Loan Application Status History</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade show active" id="tab-1">
            <table class="styled-table">
                <tbody class="gridview">
                    <tr>
                        <th><?= Yii::t('frontend', 'Loan Application ID') ?></th>
                        <td><?= $loanApplication->la_number ?></td>
                    </tr>
                    <tr>
                        <th><?= Yii::t('frontend', 'Amount') ?></th>
                        <td>
                            <?= Yii::t('frontend', '{amount} Lei', [
                                'amount' => $loanApplication->la_loan_amount
                            ])?>
                        </td>
                    </tr>
                    <tr>
                        <th><?= Yii::t('frontend', 'Term') ?></th>
                        <td>
                            <?= Yii::t('frontend', '{term} Month', [
                                'term' => $loanApplication->la_loan_term
                            ]) ?>
                        </td>
                    </tr>
                    <tr>
                        <th><?= Yii::t('frontend', 'Loan Type') ?></th>
                        <td>
                            <?= $loanApplication->product->p_name ?>
                        </td>
                    </tr>
                    <tr>
                        <th><?= Yii::t('frontend', 'Date') ?></th>
                        <td>
                            <?= Yii::$app->formatter->asByUserDate($loanApplication->la_created_dt) ?>
                        </td>
                    </tr>
                    <tr>
                        <th><?= Yii::t('frontend', 'Stage') ?></th>
                        <td><?= LoanApplicationStatus::getName($loanApplication->la_status) ?></td>
                    </tr>
                    <tr>
                        <th><?= Yii::t('frontend', 'Lender') ?></th>
                        <td>
                            <?= LoanApplicationHelper::getLoanApplicationLender($loanApplication, '<br>') ?>
                        </td>
                    </tr>
                    <tr>
                        <th><?= Yii::t('frontend', 'Client Name') ?></th>
                        <td><?= $loanApplication->client->getFullName() ?></td>
                    </tr>
                    <tr>
                        <th><?= Yii::t('frontend', 'Client IDNP') ?></th>
                        <td><?= $loanApplication->la_client_idnp ?></td>
                    </tr>
                    <tr>
                        <th><?= Yii::t('frontend', 'Client Date of Birth') ?></th>
                        <td>
                            <?= Yii::$app->formatter->asByUserDate($loanApplication->la_client_birth_date) ?>
                        </td>
                    </tr>
                    <tr>
                        <th><?= Yii::t('frontend', 'Client Phone') ?></th>
                        <td><?= LoanApplicationHelper::getClientPhones($loanApplication, '<br>') ?></td>
                    </tr>
                    <tr>
                        <th><?= Yii::t('frontend', 'Client Email') ?></th>
                        <td><?= LoanApplicationHelper::getClientEmails($loanApplication, '<br>') ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="tab-pane fade" id="tab-2">
            <table class="styled-table">
                <thead>
                    <tr>
                        <th><?= Yii::t('frontend', 'Loan Application status') ?></th>
                        <th><?= Yii::t('frontend', 'Date') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($loanApplication->loanApplicationLogs as $loanApplicationLog): ?>
                        <tr>
                            <td><?= $loanApplicationLog->lal_status_after ? LoanApplicationStatus::getName($loanApplicationLog->lal_status_after) : '--' ?></td>
                            <td><?= Yii::$app->formatter->asByUserDate($loanApplicationLog->lal_start_dt) ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

