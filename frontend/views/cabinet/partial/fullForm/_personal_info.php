<?php
/**
 * @var $this \yii\web\View
 * @var $fullRequestForm \frontend\src\useCase\loanRequest\fullForm\LoanRequestFullForm
 * @var $form \yii\bootstrap4\ActiveForm
 */

use borales\extensions\phoneInput\PhoneInput;
use common\src\entities\client\entity\Client;
use frontend\src\useCase\client\editPersonalData\ChildrenForm;
use kartik\date\DatePicker;
use unclead\multipleinput\MultipleInput;
use unclead\multipleinput\MultipleInputColumn;

?>

<div id="personal_data" class="target">
    <div class="row">
        <div class="col-md-12">
            <?= \yii\bootstrap4\Alert::widget([
                'body' => Yii::t('frontend', 'Fill out the complete form and increase your chances of getting a loan by 78%'),
                'options' => [
                    'class' => 'alert alert-info'
                ],
                'closeButton' => false
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($fullRequestForm->personalData, 'firstName', [
                'options' => [
                    'class' => 'form-wrap'
                ],
            ])->textInput([
                'readonly' => true,
                'class' => 'form-control form-control-has-validation bg-gray-light'
            ]) ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($fullRequestForm->personalData, 'lastName', [
                'options' => [
                    'class' => 'form-wrap'
                ],
            ])->textInput([
                'readonly' => true,
                'class' => 'form-control form-control-has-validation bg-gray-light',
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($fullRequestForm->personalData, 'dateOfBirth', [
                'options' => [
                    'class' => 'form-wrap'
                ],
                'template' => '{label}{input}<span class="form-validation">{error}</span>',
            ])->textInput([
                'readonly' => true,
                'class' => 'form-control form-control-has-validation bg-gray-light',
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($fullRequestForm->personalData, 'idnp', [
                'options' => [
                    'class' => 'form-wrap'
                ],
                'template' => '{label}{input}<span class="form-validation">{error}</span>'
            ])->textInput([
                'readonly' => true,
                'class' => 'form-control form-control-has-validation bg-gray-light',
            ])
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($fullRequestForm->personalData, 'gender', [
                'options' => [
                    'class' => 'form-wrap'
                ],
                'template' => '{label}{input}<span class="form-validation">{error}</span>'
            ])->textInput([
                'readonly' => true,
                'class' => 'form-control form-control-has-validation bg-gray-light',
                'value' => Client::getGenderName((int)$fullRequestForm->personalData->gender),
            ])
            ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($fullRequestForm->personalData, 'civilStatus', [
                'options' => [
                    'class' => 'form-wrap'
                ],
                'template' => '{label}{input}<span class="form-validation">{error}</span>'
            ])->dropdownList(Client::getCivilStatusListName(), [
                'prompt' => '---',
                'class' => 'form-control form-control-has-validation',
                'value' => $fullRequestForm->personalData->civilStatus ? $fullRequestForm->personalData->civilStatus : null
            ])
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <label for=""><?= Yii::t('client', 'Email') ?></label>
            <?= MultipleInput::widget([
                'name' => 'ClientEmail',
                'columns' => [
                    [
                        'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                        'name' => 'ce_email',
                        'options' => [
                            'type' => 'email',
                            'style' => 'margin-bottom: 10px'
                        ]
                    ],
                    [
                        'type' => MultipleInputColumn::TYPE_HIDDEN_INPUT,
                        'name' => 'ce_id'
                    ]
                ],
                'value' => $fullRequestForm->clientEmail,
                'iconSource' => MultipleInput::ICONS_SOURCE_FONTAWESOME,
                'addButtonOptions' => [
                    'class' => 'btn btn-success'
                ],
            ]) ?>
        </div>

        <div class="col-md-6">
            <label for=""><?= Yii::t('client', 'Phone') ?></label>
            <?= MultipleInput::widget([
                'name' => 'ClientPhone',
                'columns' => [
                    [
                        'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                        'name' => 'cp_phone',
                        'options' => [
                            'type' => 'number',
                            'style' => 'margin-bottom: 10px'
                        ]
                    ],
                    [
                        'type' => MultipleInputColumn::TYPE_HIDDEN_INPUT,
                        'name' => 'cp_id'
                    ]
                ],
                'value' => $fullRequestForm->clientPhone,
                'iconSource' => MultipleInput::ICONS_SOURCE_FONTAWESOME,
                'addButtonOptions' => [
                    'class' => 'btn btn-success'
                ]
            ]) ?>
        </div>
    </div>

    <div class="row" style="margin-top: 25px">
        <div class="col-md-6">
            <?= $form->field($fullRequestForm->personalData, 'socialStatus', [
                'options' => [
                    'class' => 'form-wrap'
                ],
                'template' => '{label}{input}<span class="form-validation">{error}</span>'
            ])->dropdownList(Client::getSocialStatusListName(), [
                'prompt' => '---',
                'class' => 'form-control form-control-has-validation',
                'value' => $fullRequestForm->personalData->socialStatus ? $fullRequestForm->personalData->socialStatus : null
            ])
            ?>
        </div>

        <div class="col-md-6">
            <label for=""><?= Yii::t('client', 'Child Date Of Birth') ?></label>
            <?= MultipleInput::widget([
                'name' => ChildrenForm::getFormName(),
                'columns' => [
                    [
                        'type' => DatePicker::class,
                        'name' => 'dateOfBirth',
                        'options' => [
                            'pluginOptions' => [
                                'format' => 'dd-mm-yyyy',
                                'autoclose' => true
                            ],
                            'options' => [
                                'style' => 'margin-bottom: 10px;'
                            ]
                        ],
                    ],
                    [
                        'type' => MultipleInputColumn::TYPE_HIDDEN_INPUT,
                        'name' => 'confirm'
                    ]
                ],
                'value' => $fullRequestForm->children,
                'iconSource' => MultipleInput::ICONS_SOURCE_FONTAWESOME,
                'addButtonOptions' => [
                    'class' => 'btn btn-success'
                ],
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($fullRequestForm->personalData, 'ownProperty')->radioList(['0' => Yii::t('frontend', 'No'), '1' => Yii::t('frontend', 'Yes')]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($fullRequestForm, 'updatePersonalInfoData')->checkbox() ?>
        </div>
    </div>
    <div class="row row-offset-5">
        <div class="col-md-6">
            <?= \yii\helpers\Html::button(Yii::t('frontend', 'Next Step'), [
                'class' => 'button button-ebony-clay-outline button-block btn-next-step'
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= \yii\helpers\Html::submitButton(Yii::t('frontend', 'Create request'), [
                'class' => 'button button-ebony-clay-outline button-block'
            ]) ?>
        </div>
    </div>
</div>
