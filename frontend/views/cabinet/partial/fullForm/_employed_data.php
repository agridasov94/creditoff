<?php
/**
 * @var $this \yii\web\View
 * @var $fullRequestForm \frontend\src\useCase\loanRequest\fullForm\LoanRequestFullForm
 * @var $form \yii\bootstrap4\ActiveForm
 */

use borales\extensions\phoneInput\PhoneInput;
use common\src\entities\client\employmentData\entity\ClientEmploymentData;
use unclead\multipleinput\MultipleInput;
use unclead\multipleinput\MultipleInputColumn;
use unclead\multipleinput\renderers\DivRenderer;
use yii\bootstrap4\Alert;
use yii\helpers\Html;

$labelStyle = 'color: #73879C; font-size: 13px; font-weight: 100;';
?>

<div id="employed_data" class="target">
    <div class="row">
        <div class="col-md-12">
            <?= Alert::widget([
                'body' => Yii::t('frontend', 'Fill out the complete form and increase your chances of getting a loan by 56%'),
                'options' => [
                    'class' => 'alert alert-info'
                ],
                'closeButton' => false
            ]) ?>
        </div>
    </div>
    <div class="row employed_data">
        <div class="col-md-12">
            <?= MultipleInput::widget([
                'id' => 'employed_data_multiple_input',
                'name' => 'ClientEmploymentData',
                'rendererClass' => DivRenderer::class,
                'columns' => [
                    [
                        'type' => MultipleInputColumn::TYPE_DROPDOWN,
                        'name' => 'ced_type',
                        'items' => ClientEmploymentData::getTypeListName(),
                        'title' => Html::label(Yii::t('client', 'Type'), '', ['style' => $labelStyle]),
                        'options' => [
                            'prompt' => '---',
                        ],
                        'columnOptions' => [
                            'class' => 'col-md-6'
                        ]
                    ],
                    [
                        'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                        'name' => 'ced_work_phone',
                        'options' => [
                            'type' => 'number',
                            'style' => 'margin-bottom: 10px'
                        ],
                        'title' => Html::label(Yii::t('client', 'Work Phone'), '', ['style' => $labelStyle]),
                        'columnOptions' => [
                            'class' => 'col-md-6'
                        ]
                    ],
                    [
                        'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                        'name' => 'ced_organization_name',
                        'title' => Html::label(Yii::t('client', 'Organization Name'), '', ['style' => $labelStyle]),
                        'columnOptions' => [
                            'class' => 'col-md-6'
                        ]
                    ],
                    [
                        'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                        'name' => 'ced_position',
                        'title' => Html::label(Yii::t('client', 'Position'), '', ['style' => $labelStyle]),
                        'columnOptions' => [
                            'class' => 'col-md-6'
                        ]
                    ],
                    [
                        'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                        'name' => 'ced_seniority',
                        'title' => Html::label(Yii::t('client', 'Seniority'), '', ['style' => $labelStyle]),
                        'columnOptions' => [
                            'class' => 'col-md-6'
                        ]
                    ],
                    [
                        'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                        'name' => 'ced_official_wages',
                        'title' => Html::label(Yii::t('client', 'Official Wages'), '', ['style' => $labelStyle]),
                        'columnOptions' => [
                            'class' => 'col-md-6'
                        ]
                    ],
                    [
                        'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                        'name' => 'ced_unofficial_wages',
                        'title' => Html::label(Yii::t('client', 'Unofficial Wages'), '', ['style' => $labelStyle]),
                        'columnOptions' => [
                            'class' => 'col-md-6'
                        ]
                    ],
                    [
                        'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                        'name' => 'ced_revenues_documented',
                        'title' => Html::label(Yii::t('client', 'Revenues Documented'), '', ['style' => $labelStyle]),
                        'columnOptions' => [
                            'class' => 'col-md-6'
                        ]
                    ],
                    [
                        'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
                        'name' => 'ced_revenues_non_documented',
                        'title' => Html::label(Yii::t('client', 'Revenues Non Documented'), '', ['style' => $labelStyle]),
                        'columnOptions' => [
                            'class' => 'col-md-6'
                        ]
                    ],
                    [
                        'type' => MultipleInputColumn::TYPE_HIDDEN_INPUT,
                        'name' => 'ced_id',
                    ],
                ],
                'layoutConfig' => [
                    'wrapperClass' => '',
                    'labelClass' => 'col-md-12',
                    'buttonAddClass' => ''
                ],
                'rowOptions' => [
                    'class' => 'row'
                ],
                'addButtonOptions' => [
                    'class' => 'btn btn-success',
                    'label' => '<i class="fa fa-plus"></i> ' . Yii::t('frontend', 'Add employed data')
                ],
                'removeButtonOptions' => [
                    'class' => 'btn btn-danger',
                    'label' => '<i class="fa fa-times"></i> ' . Yii::t('frontend', 'Remove')
                ],
                'addButtonPosition' => MultipleInput::POS_HEADER,
                'value' => $fullRequestForm->clientEmploymentData,
            ]) ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($fullRequestForm, 'updateEmployedData')->checkbox() ?>
        </div>
    </div>
    <div class="row row-offset-5">
        <div class="col-md-6">
            <?= \yii\helpers\Html::button(Yii::t('frontend', 'Next Step'), [
                'class' => 'button button-ebony-clay-outline button-block btn-next-step'
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= \yii\helpers\Html::submitButton(Yii::t('frontend', 'Create request'), [
                'class' => 'button button-ebony-clay-outline button-block'
            ]) ?>
        </div>
    </div>
</div>
<?php
$js = <<<JS
$('#employed_data_multiple_input').on('afterAddRow', function(e, row, currentIndex) {
    let div = $('<div />');
    div.addClass('d-flex justify-content-end');
    $(row).find('.js-input-remove').detach().prependTo($(row).closest('.multiple-input-list__item'));
    $(row).find('.js-input-remove').wrap(div);
});
$(document).ready(function () {
    let div = $('<div />');
    div.addClass('d-flex justify-content-end');
    $('#employed_data_multiple_input .js-input-remove').each(function(i, e) {
        let wrapper = $(e).closest('.multiple-input-list__item');
        $(e).detach().prependTo(wrapper);
        $(wrapper).find('.js-input-remove').wrap(div);
    });
});
JS;
$this->registerJs($js);