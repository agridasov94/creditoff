<?php
/**
 * @var $this \yii\web\View
 * @var $fullRequestForm \frontend\src\useCase\loanRequest\fullForm\LoanRequestFullForm
 * @var $form \yii\bootstrap4\ActiveForm
 */

use common\src\entities\client\passportData\entity\ClientPassportData;
use yii\bootstrap4\Alert;

?>

<div id="passport_data" class="target">
    <div class="row">
        <div class="col-md-12">
            <?= Alert::widget([
                'body' => Yii::t('frontend', 'Fill out the complete form and increase your chances of getting a loan by 15%'),
                'options' => [
                    'class' => 'alert alert-info'
                ],
                'closeButton' => false
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($fullRequestForm->clientPassportData, 'cpd_type', [
                'options' => [
                    'class' => 'form-wrap'
                ],
            ])->dropDownList(ClientPassportData::getTypeListName(), [
                'class' => 'form-control form-control-has-validation',
                'prompt' => '---'
            ]) ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($fullRequestForm->clientPassportData, 'cpd_series', [
                'options' => [
                    'class' => 'form-wrap'
                ],
            ])->textInput([
                'class' => 'form-control form-control-has-validation'
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($fullRequestForm->clientPassportData, 'cpd_number', [
                'options' => [
                    'class' => 'form-wrap'
                ],
            ])->textInput([
                'class' => 'form-control form-control-has-validation'
            ]) ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($fullRequestForm->clientPassportData, 'cpd_issued_by', [
                'options' => [
                    'class' => 'form-wrap'
                ],
            ])->textInput([
                'class' => 'form-control form-control-has-validation'
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($fullRequestForm->clientPassportData, 'cpd_issue_date', [
                'options' => [
                    'class' => 'form-wrap'
                ],
            ])->widget(\kartik\date\DatePicker::class, [
                'pluginOptions' => [
                    'format' => 'dd-mm-yyyy',
                    'autoclose' => true
                ],
                'options' => [
                    'class' => 'form-control form-control-has-validation'
                ]
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($fullRequestForm->clientPassportData, 'cpd_issue_department_code', [
                'options' => [
                    'class' => 'form-wrap'
                ],
            ])->textInput([
                'class' => 'form-control form-control-has-validation'
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($fullRequestForm->clientPassportData, 'cpd_expiration_date', [
                'options' => [
                    'class' => 'form-wrap'
                ],
            ])->widget(\kartik\date\DatePicker::class, [
                'pluginOptions' => [
                    'format' => 'dd-mm-yyyy',
                    'autoclose' => true
                ],
                'options' => [
                    'class' => 'form-control form-control-has-validation'
                ]
            ]) ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($fullRequestForm->clientPassportData, 'cpd_registered_address', [
                'options' => [
                    'class' => 'form-wrap'
                ],
            ])->textInput([
                'class' => 'form-control form-control-has-validation'
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($fullRequestForm->clientPassportData, 'cpd_residence_address', [
                'options' => [
                    'class' => 'form-wrap'
                ],
            ])->textInput([
                'class' => 'form-control form-control-has-validation'
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($fullRequestForm, 'updatePassportData', [
                'options' => [
                    'class' => 'form-wrap',
                    'style' => 'padding-top: 20px;'
                ],
            ])->checkbox() ?>
        </div>
    </div>

    <div class="row row-offset-5">
        <div class="col-md-6">
            <?= \yii\helpers\Html::submitButton(Yii::t('frontend', 'Create request'), [
                'class' => 'button button-ebony-clay-outline button-block'
            ]) ?>
        </div>
    </div>
</div>



