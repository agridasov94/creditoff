<?php
/**
 * @var $this \yii\web\View
 * @var $form \yii\widgets\ActiveForm
 * @var $fullRequestForm \frontend\src\useCase\loanRequest\fullForm\LoanRequestFullForm
 */

use backend\src\entities\partnerCompany\entity\PartnerCompanyQuery;
use common\src\entities\product\entity\ProductQuery;
use frontend\assets\JqueryUI;

JqueryUI::register($this);
?>
    <div id="details" class="target">
        <div class="row row-30">
            <div class="col-md-6">
                <div class="form-wrap">
                    <?= $form->field($fullRequestForm->loanDetails, 'productId')->dropDownList(ProductQuery::getList(), [
                        'class' => 'form-select',
                        'prompt' => $fullRequestForm->loanDetails->getAttributeLabel('productId'),
                        'id' => 'loan_type',
                        'novalidate' => true
                    ]) ?>
                </div>
            </div>
<!--            <div class="col-md-6">-->
                <?php /* $form->field($fullRequestForm->loanDetails, 'companies', [
                    'options' => [
                        'class' => 'form-wrap'
                    ],
                    'template' => '{label}{input}<span class="form-validation">{error}</span>'
                ])->dropDownList(PartnerCompanyQuery::getListByProduct((int)$fullRequestForm->loanDetails->productId), [
                    'class' => 'form-select',
                    'id' => 'companies',
                    'data-multiple' => "1",
                    'novalidate' => true,
                    'name' => $fullRequestForm->loanDetails->formName() . '[companies][]'
                ]) */ ?>
<!--            </div>-->
            <div class="col-md-6">
                <?= $form->field($fullRequestForm->loanDetails, 'term')->input('number', ['class' => 'form-input form-control-has-validation', 'id' => 'loan_term', 'min' => 0, 'max' => 0]) ?>
                <div id="slider-range-term" style="margin-top: 20px;"></div>
            </div>
            <div class="col-md-6">
                <?= $form->field($fullRequestForm->loanDetails, 'amount')->input('number', ['class' => 'form-input form-control-has-validation', 'id' => 'loan_amount', 'min' => 0, 'max' => 0]) ?>
                <div id="slider-range-min" style="margin-top: 20px;"></div>
            </div>
        </div>
        <div class="row row-offset-5">
            <div class="col-md-6">
                <?= \yii\helpers\Html::button(Yii::t('frontend', 'Next Step'), [
                    'class' => 'button button-ebony-clay-outline button-block btn-next-step'
                ]) ?>
            </div>
        </div>
    </div>

<?php
$getProductDataUrl = \yii\helpers\Url::to(['/loan-request/ajax-term-range']);
$amount = $fullRequestForm->loanDetails->amount ?? 0;
$product = $fullRequestForm->loanDetails->getProduct();
$maxAmount = $product ? $product->p_max_credit_amount ?? 0 : 0;
$minAmount = $product ? $product->p_min_credit_amount ?? 0 : 0;
$minTerm = $product ? $product->p_max_credit_term ?? 0 : 0;
$maxTerm = $product ? $product->p_min_credit_term ?? 0 : 0;
$term = $fullRequestForm->loanDetails->term ?? 0;
$js = <<<JS
$(document).ready( function () {
    let sliderRangeMinAmount = $("#slider-range-min");
    let sliderRangerTerm = $("#slider-range-term");
    let inputLoanAmount = $('#loan_amount');
    let inputLoanTerm = $('#loan_term');
    let loanType = $('#loan_type');
    let minAmount = $minAmount;
    let maxAmount = $maxAmount;
    let minTerm = $minTerm;
    let maxTerm = $maxTerm;
    
    function initSlider(elem, min, max, value, inputElem) {
        elem.slider({
            range: "min",
            value: value,
            min: min,
            max: max,
            slide: function( event, ui ) {
                inputElem.val(ui.value);
            }
        });
    }
    
    inputLoanAmount.on('keyup change', function(){
        sliderRangeMinAmount.slider('destroy');
        initSlider(sliderRangeMinAmount, minAmount, maxAmount, inputLoanAmount.val(), inputLoanAmount);
    });
    
    inputLoanTerm.on('keyup change', function(){
        sliderRangerTerm.slider('destroy');
        initSlider(sliderRangerTerm, minTerm, maxTerm, inputLoanTerm.val(), inputLoanTerm);
    });
    
    inputLoanAmount.attr('min', minAmount).attr('max', maxAmount).val($amount);
    inputLoanTerm.attr('min', minTerm).attr('max', maxTerm).val($term);
    initSlider(sliderRangeMinAmount, $minAmount, $maxAmount, $amount, inputLoanAmount)
    initSlider(sliderRangerTerm, $minTerm, $maxTerm, $term, inputLoanTerm)
    
    loanType.select2({
        theme: loanType.attr('data-custom-theme') ? loanType.attr('data-custom-theme') : "bootstrap",
        multiple: !!loanType.attr('data-multiple')
    }).next().addClass(loanType.attr("class").match(/(input-sm)|(input-lg)|($)/i).toString().replace(new RegExp(",", 'g'), " "));
    
    $('#loan_type').on('change', function () {
        let submitBtn = $('#confirm_loan_request');
        submitBtn.addClass('disabled').prop('disabled', true);
        let val = $(this).val();
        
        $.ajax({
            url: '$getProductDataUrl',
            data: {productId: val, getCompanies: true},
            type: 'post',
            dataType: 'json',
            success: function (data) {
                minAmount = data.minAmount;
                maxAmount = data.maxAmount;
                minTerm = data.minTerm;
                maxTerm = data.maxTerm;
                inputLoanAmount.attr('min', data.minAmount).attr('max', data.maxAmount).val(data.minAmount);
                inputLoanTerm.attr('min', data.minTerm).attr('max', data.maxTerm).val(data.minTerm);
                initSlider(sliderRangeMinAmount, data.minAmount, data.maxAmount, data.minAmount, inputLoanAmount);
                initSlider(sliderRangerTerm, data.minTerm, data.maxTerm, data.minTerm, inputLoanTerm);
                submitBtn.removeClass('disabled').prop('disabled', false);
                // if (data.companies) {
                //     $('#companies').html('');
                //     for (key in data.companies) {
                //         $('#companies').append($('<option>').val(key).text(data.companies[key]));
                //     };
                //     // initSleect2();
                //     if (Array.isArray(data.companies)) {
                //         $('#companies').val((data.companies));
                //     } else {
                //         $('#companies').val(Object.keys(data.companies || {}));
                //     }
                //     $('#companies').trigger('change');
                // }
            }
        })
    });
    
    function initSleect2() {
        let companies = $('#companies');
        companies.select2({
            theme: companies.attr('data-custom-theme') ? companies.attr('data-custom-theme') : "bootstrap",
            multiple: !!companies.attr('data-multiple')
        }).next().addClass(companies.attr("class").match(/(input-sm)|(input-lg)|($)/i).toString().replace(new RegExp(",", 'g'), " "));
    }
    // initSleect2();
});
JS;
$this->registerJs($js, \yii\web\View::POS_END);
