<?php
/**
 * @var $this \yii\web\View
 * @var $editClientDataForm EditClientDataForm
 * @var $form ActiveForm
 */

use common\src\entities\client\passportData\entity\ClientPassportData;
use frontend\src\useCase\client\editPersonalData\EditClientDataForm;
use yii\bootstrap4\ActiveForm;

?>

<div class="row">
    <div class="col-md-6">
        <?= $form->field($editClientDataForm->clientPassportData, 'cpd_type', [
            'options' => [
                'class' => 'form-wrap'
            ],
        ])->dropDownList(ClientPassportData::getTypeListName(), [
            'class' => 'form-control form-control-has-validation',
            'prompt' => '---'
        ]) ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($editClientDataForm->clientPassportData, 'cpd_series', [
            'options' => [
                'class' => 'form-wrap'
            ],
        ])->textInput([
            'class' => 'form-control form-control-has-validation'
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <?= $form->field($editClientDataForm->clientPassportData, 'cpd_number', [
            'options' => [
                'class' => 'form-wrap'
            ],
        ])->textInput([
            'class' => 'form-control form-control-has-validation'
        ]) ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($editClientDataForm->clientPassportData, 'cpd_issued_by', [
            'options' => [
                'class' => 'form-wrap'
            ],
        ])->textInput([
            'class' => 'form-control form-control-has-validation'
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <?= $form->field($editClientDataForm->clientPassportData, 'cpd_issue_date', [
            'options' => [
                'class' => 'form-wrap'
            ],
        ])->widget(\kartik\date\DatePicker::class, [
            'pluginOptions' => [
                'format' => 'dd-mm-yyyy',
                'autoclose' => true
            ],
            'options' => [
                'class' => 'form-control form-control-has-validation'
            ]
        ]) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($editClientDataForm->clientPassportData, 'cpd_expiration_date', [
            'options' => [
                'class' => 'form-wrap'
            ],
        ])->widget(\kartik\date\DatePicker::class, [
            'pluginOptions' => [
                'format' => 'dd-mm-yyyy',
                'autoclose' => true
            ],
            'options' => [
                'class' => 'form-control form-control-has-validation'
            ]
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <?= $form->field($editClientDataForm->clientPassportData, 'cpd_registered_address', [
            'options' => [
                'class' => 'form-wrap'
            ],
        ])->textInput([
            'class' => 'form-control form-control-has-validation'
        ]) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($editClientDataForm->clientPassportData, 'cpd_residence_address', [
            'options' => [
                'class' => 'form-wrap'
            ],
        ])->textInput([
            'class' => 'form-control form-control-has-validation'
        ]) ?>
    </div>
</div>