<?php
/**
 * @var $this \yii\web\View
 * @var $client \common\src\entities\client\entity\Client
 * @var $editClientDataForm EditClientDataForm
 */

use frontend\src\useCase\client\editPersonalData\EditClientDataForm;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

$this->params['breadcrumbs'][] = Yii::t('frontend', 'Personal Data');
?>


<section class="section-top-60 section-md-top-90 section-bottom-60 section-md-bottom-90 bg-whisperapprox">
    <div class="container">
        <h3 class="text-center"><?= Yii::t('frontend', 'Manage your personal data') ?></h3>
        <div class="row">
            <div class="col-md-12">
                <?= \common\widgets\Alert::widget() ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12">
                <?php Pjax::begin() ?>
                <div class="tabs-custom tabs-vertical tabs-corporate" id="tabs-2">
                    <ul class="nav nav-tabs">
                        <li><a id="personal" class="active" href="#tabs-2-1" data-bs-toggle="tab"><?= Yii::t('frontend', 'Personal info') ?></a></li>
                        <li><a id="employed" href="#tabs-2-2" data-bs-toggle="tab"><?= Yii::t('frontend', 'Employed data') ?></a></li>
                        <li><a id="passport" href="#tabs-2-3" data-bs-toggle="tab"><?= Yii::t('frontend', 'Passport Data') ?></a></li>
                    </ul>
                    <div class="tab-content">
                            <?php $form = ActiveForm::begin(['options' => ['class' => 'rd-mailform', 'data-pjax' => 1], 'enableClientScript' => false]) ?>

                                <?= $form->errorSummary($editClientDataForm, ['class' => 'alert alert-danger']) ?>

                                <div class="tab-pane fade show active" id="tabs-2-1">
                                    <?= $this->render('partial/_personal_info', [
                                        'editClientDataForm' => $editClientDataForm,
                                        'form' => $form
                                    ]) ?>
                                </div>
                                <div class="tab-pane fade" id="tabs-2-2">
                                    <?= $this->render('partial/_employed_data', [
                                        'editClientDataForm' => $editClientDataForm,
                                        'form' => $form
                                    ]) ?>
                                </div>
                                <div class="tab-pane fade" id="tabs-2-3">
                                    <?= $this->render('partial/_passport_data', [
                                        'editClientDataForm' => $editClientDataForm,
                                        'form' => $form
                                    ]) ?>
                                </div>
                                <div style="margin-top: 10px">
                                    <?= Html::submitButton(Yii::t('frontend', 'Submit'), [
                                        'class' => 'button button-ebony-clay-outline button-block'
                                    ]) ?>
                                </div>
                            <?php ActiveForm::end(); ?>
                    </div>
                </div>
<?php if ($editClientDataForm->personalInfoHasErrors()): ?>
<?php
$js = <<<JS
$('.nav-tabs a').removeClass('active');
$('.tab-pane').removeClass('show', 'active');
$('#tabs-2-1').addClass('show', 'active');
$('#personal').addClass('active');
JS;
$this->registerJs($js);
?>
<?php elseif ($editClientDataForm->employedDataHasErrors()): ?>
<?php
$js = <<<JS
$('.nav-tabs a').removeClass('active');
$('.tab-pane').removeClass('show', 'active');
$('#tabs-2-2').addClass('show', 'active');
$('#employed').addClass('active');
JS;
$this->registerJs($js);
?>
<?php elseif ($editClientDataForm->passportDataHasErrors()): ?>
<?php
$js = <<<JS
$('.nav-tabs a').removeClass('active');
$('.tab-pane').removeClass('show', 'active');
$('#tabs-2-3').addClass('show', 'active');
$('#passport').addClass('active');
JS;
$this->registerJs($js);
?>
<?php endif; ?>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
</section>

<?php if (Yii::$app->session->hasFlash('updatedPersonalInfo')): ?>
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            
            <div class="modal-body">
                <p><?= Yii::t('frontend', 'Submit a new loan application?') ?></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><?= Yii::t('frontend', 'Cancel') ?></button>
                <a class="btn btn-primary" href="<?= Url::to(['/cabinet/loan-application-create']) ?>"><?= Yii::t('frontend', 'Create request') ?></a>
            </div>
        </div>
    </div>
</div>
<?php
$js = <<<JS
(function () {
var modal = new bootstrap.Modal(document.getElementById('exampleModal'));
setTimeout(function () {
    modal.show();
}, 1500);
})();
JS;
$this->registerJs($js);
?>
<?php endif; ?>