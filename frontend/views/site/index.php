<?php

use common\src\entities\mainBanner\entity\MainBanner;
use frontend\assets\MainSliderAsset;
use frontend\src\useCase\loanRequest\shortForm\ShortFormLoanRequest;

/* @var $this yii\web\View */
/* @var $shortFormLoanRequest ShortFormLoanRequest */
/* @var $banners MainBanner[] */

$this->title = 'CreditOff - Main Page';
MainSliderAsset::register($this);
?>

<?= $this->render('partial/_main_slider', ['shortFormLoanRequest' => $shortFormLoanRequest, 'banners' => $banners]) ?>

<?= $this->render('partial/_our_services') ?>

<?= $this->render('partial/_our_mission') ?>

<?= $this->render('partial/_our_team') ?>

<?= $this->render('partial/_facts_and_numbers') ?>

<?= $this->render('partial/_calculator') ?>

<?= $this->render('partial/_testimonials') ?>

<?= $this->render('partial/_latest_news') ?>

<?= $this->render('partial/_partners') ?>