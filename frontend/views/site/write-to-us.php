<section class="section-40 section-md-60 section-lg-90 section-xl-120 bg-gray-dark page-title-wrap overlay-5" style="background-image: url(/images/bg-image-4.jpg);">
    <div class="container">
        <div class="page-title text-center">
            <h2><?= Yii::t('frontend', 'Contacts') ?></h2>
        </div>
    </div>
</section>

<section class="section-60 section-md-top-90 section-md-bottom-100">
    <div class="container">
        <div class="row row-50 justify-content-lg-between">
            <div class="col-lg-5 col-xl-4">
                <div class="inset-lg-right-15 inset-xl-right-0">
                    <div class="row row-30 row-md-40">
                        <div class="col-md-10 col-lg-12">
                            <h3><?= Yii::t('frontend', 'How to Find Us') ?></h3>
                            <p><?= Yii::t('frontend', 'If you have any questions, just fill in the contact form, and we will answer you shortly. If you are living nearby, come visit LawExpert at one of our comfortable offices.') ?></p>
                        </div>
                        <div class="col-md-6 col-lg-12">
                            <h5><?= Yii::t('frontend', 'Headquarters') ?></h5>
                            <address class="contact-info">
                                <p class="text-uppercase">9863 - 9867 MILL ROAD, CAMBRIDGE, MG09 99HT.</p>
                                <dl class="list-terms-inline">
                                    <dt>Telephone</dt>
                                    <dd><a class="link-secondary" href="tel:#">+1 800 603 6035</a></dd>
                                </dl>
                                <dl class="list-terms-inline">
                                    <dt>E-mail</dt>
                                    <dd><a class="link-primary" href="mailto:#">mail@demolink.org</a></dd>
                                </dl>
                            </address>
                        </div>
                        <div class="col-md-6 col-lg-12">
                            <h5>Support Centre</h5>
                            <address class="contact-info">
                                <p class="text-uppercase">9870 ST VINCENT PLACE, GLASGOW, DC 45 FR 45</p>
                                <dl class="list-terms-inline">
                                    <dt>Telephone</dt>
                                    <dd><a class="link-secondary" href="tel:#">+1 800 603 6035</a></dd>
                                </dl>
                                <dl class="list-terms-inline">
                                    <dt>E-mail</dt>
                                    <dd><a class="link-primary" href="mailto:#">mail@demolink.org</a></dd>
                                </dl>
                            </address>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-7 col-xl-6">
                <h3>Get in Touch</h3>
                <form class="rd-mailform form-modern" data-form-output="form-output-global" data-form-type="contact" method="post" action="bat/rd-mailform.php">
                    <div class="row row-30">
                        <div class="col-md-6">
                            <div class="form-wrap">
                                <input class="form-input" id="contact-name" type="text" name="name" data-constraints="@Required">
                                <label class="form-label" for="contact-name"><?= Yii::t('frontend', 'Name') ?></label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-wrap">
                                <input class="form-input" id="contact-email" type="email" name="email" data-constraints="@Email @Required">
                                <label class="form-label" for="contact-email"><?= Yii::t('frontend', 'Email') ?></label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-wrap">
                                <div class="textarea-lined-wrap">
                                    <textarea class="form-input" id="contact-message" name="message" data-constraints="@Required"></textarea>
                                    <label class="form-label" for="contact-message"><?= Yii::t('frontend', 'Message') ?></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row row-30 row-offset-5">
                        <div class="col-sm-8">
                            <button class="button button-primary button-block" type="submit"><?= Yii::t('frontend', 'Send') ?></button>
                        </div>
                        <div class="col-sm-4">
                            <button class="button button-silver-outline button-block" type="reset"><?= Yii::t('frontend', 'Reset') ?></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<section class="section">
    <div class="google-map-container" data-zoom="15" data-center="9870 St Vincent Place, Glasgow, DC 45 Fr 45." data-styles="[{&quot;featureType&quot;:&quot;administrative.locality&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;hue&quot;:&quot;#2c2e33&quot;},{&quot;saturation&quot;:7},{&quot;lightness&quot;:19},{&quot;visibility&quot;:&quot;on&quot;}]},{&quot;featureType&quot;:&quot;landscape&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;hue&quot;:&quot;#ffffff&quot;},{&quot;saturation&quot;:-100},{&quot;lightness&quot;:100},{&quot;visibility&quot;:&quot;simplified&quot;}]},{&quot;featureType&quot;:&quot;poi&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;hue&quot;:&quot;#ffffff&quot;},{&quot;saturation&quot;:-100},{&quot;lightness&quot;:100},{&quot;visibility&quot;:&quot;off&quot;}]},{&quot;featureType&quot;:&quot;road&quot;,&quot;elementType&quot;:&quot;geometry&quot;,&quot;stylers&quot;:[{&quot;hue&quot;:&quot;#bbc0c4&quot;},{&quot;saturation&quot;:-93},{&quot;lightness&quot;:31},{&quot;visibility&quot;:&quot;simplified&quot;}]},{&quot;featureType&quot;:&quot;road&quot;,&quot;elementType&quot;:&quot;labels&quot;,&quot;stylers&quot;:[{&quot;hue&quot;:&quot;#bbc0c4&quot;},{&quot;saturation&quot;:-93},{&quot;lightness&quot;:31},{&quot;visibility&quot;:&quot;on&quot;}]},{&quot;featureType&quot;:&quot;road.arterial&quot;,&quot;elementType&quot;:&quot;labels&quot;,&quot;stylers&quot;:[{&quot;hue&quot;:&quot;#bbc0c4&quot;},{&quot;saturation&quot;:-93},{&quot;lightness&quot;:-2},{&quot;visibility&quot;:&quot;simplified&quot;}]},{&quot;featureType&quot;:&quot;road.local&quot;,&quot;elementType&quot;:&quot;geometry&quot;,&quot;stylers&quot;:[{&quot;hue&quot;:&quot;#e9ebed&quot;},{&quot;saturation&quot;:-90},{&quot;lightness&quot;:-8},{&quot;visibility&quot;:&quot;simplified&quot;}]},{&quot;featureType&quot;:&quot;transit&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;hue&quot;:&quot;#e9ebed&quot;},{&quot;saturation&quot;:10},{&quot;lightness&quot;:69},{&quot;visibility&quot;:&quot;on&quot;}]},{&quot;featureType&quot;:&quot;water&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;hue&quot;:&quot;#e9ebed&quot;},{&quot;saturation&quot;:-78},{&quot;lightness&quot;:67},{&quot;visibility&quot;:&quot;simplified&quot;}]}]">
        <div class="google-map"></div>
        <ul class="google-map-markers">
            <li data-location="9870 St Vincent Place, Glasgow, DC 45 Fr 45." data-description="9870 St Vincent Place, Glasgow" data-icon="/images/gmap_marker.png" data-icon-active="/images/gmap_marker_active.png"></li>
        </ul>
    </div>
</section>
