<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use kekaadrenalin\recaptcha3\ReCaptchaWidget;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>

    <section class="section-60 section-md-top-90 section-md-bottom-100">
      <div class="container">
        <div class="row row-50 justify-content-lg-between">
          <div class="col-lg-5 col-xl-4">
            <div class="inset-lg-right-15 inset-xl-right-0">
              <div class="row row-30 row-md-40">
                <div class="col-md-10 col-lg-12">
                  <h3><?= Yii::t('frontend', 'How to Find Us') ?></h3>
                  <p><?= Yii::t('frontend', 'If you have any questions, just fill in the contact form, and we will answer you shortly. If you are living nearby, come visit LawExpert at one of our comfortable offices.') ?></p>
                </div>
                <div class="col-md-6 col-lg-12">
                  <h5><?= Yii::t('frontend', 'Headquarters') ?></h5>
                  <address class="contact-info">
                    <p class="text-uppercase"><?= Yii::t('frontend', '9863 - 9867 MILL ROAD, CAMBRIDGE, MG09 99HT.') ?></p>
                    <dl class="list-terms-inline">
                      <dt><?= Yii::t('frontend', 'Telephone') ?></dt>
                      <dd><a class="link-secondary" href="tel:#">+1 800 603 6035</a></dd>
                    </dl>
                    <dl class="list-terms-inline">
                      <dt><?= Yii::t('frontend', 'E-mail') ?></dt>
                      <dd><a class="link-primary" href="mailto:#">mail@demolink.org</a></dd>
                    </dl>
                  </address>
                </div>
                <div class="col-md-6 col-lg-12">
                  <h5><?= Yii::t('frontend', 'Support Centre') ?></h5>
                  <address class="contact-info">
                    <p class="text-uppercase"><?= Yii::t('frontend', '9870 ST VINCENT PLACE, GLASGOW, DC 45 FR 45') ?></p>
                    <dl class="list-terms-inline">
                      <dt><?= Yii::t('frontend', 'Telephone') ?></dt>
                      <dd><a class="link-secondary" href="tel:#">+1 800 603 6035</a></dd>
                    </dl>
                    <dl class="list-terms-inline">
                      <dt><?= Yii::t('frontend', 'E-mail') ?></dt>
                      <dd><a class="link-primary" href="mailto:#">mail@demolink.org</a></dd>
                    </dl>
                  </address>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-7 col-xl-6">
            <h3><?= Yii::t('frontend', 'Get in Touch') ?></h3>
              <?php $form = ActiveForm::begin(['id' => 'contact-form', 'options' => [
                  'class' => 'form-modern'
              ]]); ?>

                  <div class="row row-30">
                      <?php if(Yii::$app->session->hasFlash('success')): ?>
                        <div class="col-sm-12">
                            <?= \common\widgets\Alert::widget() ?>
                        </div>
                      <?php endif; ?>

                      <?php if ($model->hasErrors()): ?>
                      <div class="col-md-12">
                          <?= $form->errorSummary($model, ['class' => 'alert alert-danger', 'role' => 'alert']) ?>
                      </div>
                      <?php endif; ?>
                      <div class="col-md-6">
                          <?= $form->field($model, 'name', [
                              'options' => [
                                  'class' => 'form-wrap'
                              ]
                          ])->textInput([
                              'autofocus' => true,
                              'class' => 'form-input form-control-has-validation'
                          ])->label(null, [
                              'class' => 'form-label rd-input-label'
                          ]) ?>
                      </div>

                      <div class="col-md-6">
                          <?= $form->field($model, 'email', [
                              'options' => [
                                  'class' => 'form-wrap'
                              ]
                          ])->textInput(['type' => 'email', 'class' => 'form-input form-control-has-validation'])->label(null, [
                              'class' => 'form-label rd-input-label'
                          ]) ?>
                      </div>

                      <div class="col-sm-12">
                          <div class="form-wrap">
                              <?= $form->field($model, 'message', [
                                  'options' => [
                                      'class' => 'textarea-lined-wrap'
                                  ]
                              ])->textarea(['class' => 'form-input form-control-has-validation'])->label(null, [
                                  'class' => 'form-label rd-input-label'
                              ]) ?>
                          </div>
                      </div>
                  </div>

                  <?= $form->field($model, 'captcha')->widget(ReCaptchaWidget::class) ?>
              
                    <div class="row row-30 row-offset-5">
                        <div class="col-sm-8">
                            <?= Html::submitButton('Submit', ['class' => 'button button-primary button-block', 'name' => 'contact-button']) ?>
                        </div>
                    </div>

              <?php ActiveForm::end(); ?>
          </div>
        </div>
      </div>
    </section>

