<?php
/**
 * @var Faq[] $faqs
 */

use common\src\entities\faq\entity\Faq;

?>
<section class="section-40 section-md-60 section-lg-90 section-xl-120 bg-gray-dark page-title-wrap overlay-5" style="background-image: url(/images/bg-search.jpg);">
    <div class="container">
        <div class="page-title text-center">
            <h2> <?= Yii::t('frontend', 'FAQ') ?></h2>
        </div>
    </div>
</section>

<section class="section-60 section-md-90 section-xl-bottom-120 bg-whisperapprox">
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-md-10 col-lg-12">
                <div class="row row-50">
                    <div class="col-lg-12">
                        <h3> <?= Yii::t('frontend', 'General Questions') ?></h3>
                        <p> <?= Yii::t('frontend', 'All you need to know about Investment Smart design studio and how to get Support.') ?></p>

                        <div class="card-group card-group-custom card-group-corporate" id="accordionOne" role="tablist" aria-multiselectable="true">
                            <div class="row">
                                <?php foreach ($faqs as $faq): ?>
                                    <div class="col-md-6">
                                        <div class="card card-custom card-corporate">
                                            <div class="card-heading" id="accordionOneHeading1" role="tab">
                                                <div class="card-title"><a role="button" data-bs-toggle="collapse" data-parent="#accordionOne" href="#accordionOneCollapse1" aria-controls="accordionOneCollapse1" aria-expanded="true"> <?= $faq->f_question ?>
                                                        <div class="card-arrow"></div></a></div>
                                            </div>
                                            <div class="card-collapse collapse show" id="accordionOneCollapse1" role="tabpanel" aria-labelledby="accordionOneHeading1">
                                                <div class="card-body">
                                                    <p class="text-gray-05"> <?= $faq->f_answer ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>