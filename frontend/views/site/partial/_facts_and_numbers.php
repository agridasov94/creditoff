<section class="section parallax-container bg-black" data-parallax-img="/images/bg-image-3.jpg">
    <div class="parallax-content">
        <div class="section-60 section-md-100 overlay-9">
            <div class="container">
                <h3 class="text-center"><?= Yii::t('frontend', 'Facts and Numbers') ?></h3>
                <div class="row row-40 row-offset-1 align-items-sm-end">
                    <div class="col-sm-6 col-md-3">
                        <div class="box-counter"><span class="icon icon-11"></span>
                            <div class="text-large counter">1450</div>
                            <h5 class="box-header"><?= Yii::t('frontend', 'Trusted Clients') ?></h5>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="box-counter"><span class="icon icon-12"></span>
                            <div class="text-large counter">18</div>
                            <h5 class="box-header"><?= Yii::t('frontend', 'Awards') ?></h5>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="box-counter"><span class="icon icon-13"></span>
                            <div class="text-large counter">21</div>
                            <h5 class="box-header"><?= Yii::t('frontend', 'Years of Experience') ?></h5>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="box-counter"><span class="icon icon-14"></span>
                            <div class="text-large counter counter-k">16</div>
                            <h5 class="box-header"><?= Yii::t('frontend', 'Experts') ?></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>