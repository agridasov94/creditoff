<?php
/**
 * @var $this \yii\web\View
 * @var $shortFormLoanRequest \frontend\src\useCase\loanRequest\shortForm\ShortFormLoanRequest
 * @var $banners \common\src\entities\mainBanner\entity\MainBanner[]
 */

use common\src\entities\mainBanner\useCase\displayFile\DisplayFile;

?>
<section style="position: relative;">
    <div class="swiper-container swiper-slider swiper-variant-1 bg-black" data-loop="true" data-autoplay="1000000" data-simulate-touch="true">
        <div class="swiper-wrapper text-center">
            <?php foreach ($banners as $banner): ?>
                <?= (new DisplayFile($banner, DisplayFile::FRONTEND))->render($this) ?>
            <?php endforeach; ?>
<!--            <div class="swiper-slide swiper-slide-bottom overlay-5" data-slide-bg="/images/home-slider-1-slide-1.jpg">-->
<!--                <div class="swiper-slide-caption">-->
<!--                    <div class="container">-->
<!--                        <div class="row justify-content-start">-->
<!--                            <div class="col-md-5 col-lg-6 ">-->
<!--                                <div class="shilder-header-with-divider" data-caption-animate="fadeInUp" data-caption-delay="0s">--><?php //= Yii::t('frontend', '20 years in Stock Market') ?><!--</div>-->
<!--                                <h2 class="slider-header" data-caption-animate="fadeInUp" data-caption-delay="100s">--><?php //= Yii::t('frontend', 'Manage Your Invesments') ?><!--<br>--><?php //= Yii::t('frontend', 'Easily and Effectively ') ?><!--</h2><a class="button button-icon button-icon-right button-primary big" data-caption-animate="fadeInUp" data-caption-delay="250" href="#"><span class="icon icon-xs fa-angle-right"></span>--><?php //= Yii::t('frontend', 'Get a Quote') ?><!--</a>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="swiper-slide overlay-5" data-slide-bg="/images/home-slider-1-slide-2.jpg">-->
<!--                <div class="swiper-slide-caption">-->
<!--                    <div class="container">-->
<!--                        <div class="row justify-content-start">-->
<!--                            <div class="col-xl-1 d-none d-xl-inline-block d-xxl-none"></div>-->
<!--                            <div class="col-md-5 col-lg-6 ">-->
<!--                                <h2 class="slider-header" data-caption-animate="fadeInUp" data-caption-delay="0s">--><?php //= Yii::t('frontend', 'Does Your Money') ?><!--<br>--><?php //= Yii::t('frontend', 'Need Speed?') ?><!--</h2>-->
<!--                                <p class="text-bigger text-regular slider-text" data-caption-animate="fadeInUp" data-caption-delay="100">--><?php //= Yii::t('frontend', 'Our company can assist you with any aspect of investment management') ?><!--</p><a class="button button-icon button-icon-right button-primary big" data-caption-animate="fadeInUp" data-caption-delay="250" href="#"><span class="icon icon-xs fa-angle-right"></span>--><?php //= Yii::t('frontend', 'Get a Quote ') ?><!--</a>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="swiper-slide swiper-slide-bottom overlay-5" data-slide-bg="/images/home-slider-1-slide-1.jpg">-->
<!--                <div class="swiper-slide-caption">-->
<!--                    <div class="container">-->
<!--                        <div class="row justify-content-start">-->
<!--                            <div class="col-md-5 col-lg-6">-->
<!--                                <div class="shilder-header-with-divider" data-caption-animate="fadeInUp" data-caption-delay="0s">--><?php //= Yii::t('frontend', '20 years in Stock Market') ?><!--</div>-->
<!--                                <h2 class="slider-header" data-caption-animate="fadeInUp" data-caption-delay="100s">--><?php //= Yii::t('frontend', 'Manage Your Invesments') ?><!--<br>--><?php //= Yii::t('frontend', 'Easily and Effectively') ?><!--</h2><a class="button button-icon button-icon-right button-primary big" data-caption-animate="fadeInUp" data-caption-delay="250" href="#"><span class="icon icon-xs fa-angle-right"></span>--><?php //= Yii::t('frontend', 'Get a Quote') ?><!--</a>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="swiper-slide swiper-slide-bottom overlay-5" data-slide-bg="/images/home-slider-1-slide-1.jpg">-->
<!--                <div class="swiper-slide-caption">-->
<!--                    <div class="container">-->
<!--                        <div class="row justify-content-start">-->
<!--                            <div class="col-md-5 col-lg-6">-->
<!--                                <div class="shilder-header-with-divider" data-caption-animate="fadeInUp" data-caption-delay="0s">--><?php //= Yii::t('frontend', '20 years in Stock Market') ?><!--</div>-->
<!--                                <h2 class="slider-header" data-caption-animate="fadeInUp" data-caption-delay="100s">--><?php //= Yii::t('frontend', 'Manage Your Invesments') ?><!--<br>--><?php //= Yii::t('frontend', 'Easily and Effectively ') ?><!--</h2><a class="button button-icon button-icon-right button-primary big" data-caption-animate="fadeInUp" data-caption-delay="250" href="#"><span class="icon icon-xs fa-angle-right"></span>--><?php //= Yii::t('frontend', 'Get a Quote') ?><!--</a>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="swiper-slide swiper-slide-bottom overlay-5" data-slide-bg="/images/home-slider-1-slide-1.jpg">-->
<!--                <div class="swiper-slide-caption">-->
<!--                    <div class="container">-->
<!--                        <div class="row justify-content-start">-->
<!--                            <div class="col-md-5 col-lg-6">-->
<!--                                <div class="shilder-header-with-divider" data-caption-animate="fadeInUp" data-caption-delay="0s">--><?php //= Yii::t('frontend', '20 years in Stock Market') ?><!--</div>-->
<!--                                <h2 class="slider-header" data-caption-animate="fadeInUp" data-caption-delay="100s">--><?php //= Yii::t('frontend', 'Manage Your Invesments') ?><!--<br>--><?php //= Yii::t('frontend', 'Easily and Effectively') ?><!--</h2><a class="button button-icon button-icon-right button-primary big" data-caption-animate="fadeInUp" data-caption-delay="250" href="#"><span class="icon icon-xs fa-angle-right"></span>--><?php //= Yii::t('frontend', 'Get a Quote') ?><!--</a>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="swiper-slide swiper-slide-bottom overlay-5" data-slide-bg="/images/home-slider-1-slide-1.jpg">-->
<!--                <div class="swiper-slide-caption">-->
<!--                    <div class="container">-->
<!--                        <div class="row justify-content-start">-->
<!--                            <div class="col-md-5 col-lg-6">-->
<!--                                <div class="shilder-header-with-divider" data-caption-animate="fadeInUp" data-caption-delay="0s">--><?php //= Yii::t('frontend', '20 years in Stock Market') ?><!--</div>-->
<!--                                <h2 class="slider-header" data-caption-animate="fadeInUp" data-caption-delay="100s">--><?php //= Yii::t('frontend', 'Manage Your Invesments') ?><!--<br>--><?php //= Yii::t('frontend', 'Easily and Effectively') ?><!--</h2><a class="button button-icon button-icon-right button-primary big" data-caption-animate="fadeInUp" data-caption-delay="250" href="#"><span class="icon icon-xs fa-angle-right"></span>--><?php //= Yii::t('frontend', 'Get a Quote ') ?><!--</a>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
        </div>
        <div class="swiper-scrollbar d-xl-none"></div>
       
        <div class="swiper-pagination swiper-pagination-bullets swiper-pagination-horizontal">
            <div class="container">
                <div class="col-md-3">
                </div>
            </div>
        </div>
    </div>
    <?= $this->render('_loan_request_form', ['shortFormLoanRequest' => $shortFormLoanRequest]) ?>
</section>