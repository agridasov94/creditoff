<section class="section-60 section-md-90 section-xl-bottom-120 bg-whisperapprox">
    <div class="container">
        <h3><?= Yii::t('frontend', 'Our Clients') ?></h3>
        <div class="row row-40">
            <div class="col-md-7 col-lg-5 col-xl-4">
                <div class="owl-carousel owl-nav-bottom-left" data-items="1" data-stage-padding="0" data-loop="true" data-margin="30" data-mouse-drag="false" data-nav="true" data-animation-in="fadeIn" data-animation-out="fadeOut">
                    <div class="item">
                        <p><?= Yii::t('frontend', 'We work closely with our diverse client base to provide the exceptional service. Among our clients there are many world-renowned industry leaders.') ?></p>
                    </div>
                    <div class="item">
                        <p><?= Yii::t('frontend', 'Our firm is like family. We truly believe that each case is someone’s life and we need to treat it as such.') ?></p>
                    </div>
                    <div class="item">
                        <p><?= Yii::t('frontend', 'Results are only successful when we  make a difference in your life. We’ll help you get the money and benefits.') ?></p>
                    </div>
                </div>
            </div>
            <div class="col-lg-7 col-xl-8">
                <div class="row row-30 shift-lg-top-1">
                    <div class="col-sm-6 col-md-3">
                        <div class="link-image-wrap"><a class="link-image" href="#"><img src="/images/clients-3-132x83.png" alt="" width="132" height="83"/></a></div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="link-image-wrap"><a class="link-image" href="#"><img src="/images/clients-4-134x77.png" alt="" width="134" height="77"/></a></div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="link-image-wrap"><a class="link-image" href="#"><img src="/images/clients-5-141x88.png" alt="" width="141" height="88"/></a></div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="link-image-wrap"><a class="link-image" href="#"><img src="/images/clients-6-152x64.png" alt="" width="152" height="64"/></a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>