<section class="section-50 section-md-90">
    <div class="container text-center">
        <h3><?= Yii::t('frontend', 'Our Services') ?></h3>
        <div class="row row-40 row-offset-3">
            <div class="col-md-6 col-lg-4 height-fill">
                <article class="icon-box">
                    <div class="box-top">
                        <div class="box-icon"><span class="icon icon-primary icon-lg-bigger material-icons-remove_red_eye material-icons-device_hub"></span></div>
                        <div class="box-header">
                            <h5><a href="#"><?= Yii::t('frontend', 'Market Research') ?></a></h5>
                        </div>
                    </div>
                    <div class="divider"></div>
                    <div class="box-body">
                        <p class="text-gray-05"><?= Yii::t('frontend', 'Assessment of viability, stability, and profitability of a business, sub-business or project.') ?></p>
                    </div>
                </article>
            </div>
            <div class="col-md-6 col-lg-4 height-fill">
                <article class="icon-box">
                    <div class="box-top">
                        <div class="box-icon"><span class="icon icon-primary icon-lg-bigger mercury-ico mercury-icon-partners"></span></div>
                        <div class="box-header">
                            <h5><a href="#"><?= Yii::t('frontend', 'Investment Management') ?></a></h5>
                        </div>
                    </div>
                    <div class="divider"></div>
                    <div class="box-body">
                        <p class="text-gray-05"><?= Yii::t('frontend', 'Our Investment Management Services help you pursue your financial needs as they grow and change.') ?></p>
                    </div>
                </article>
            </div>
            <div class="col-md-6 col-lg-4 height-fill">
                <article class="icon-box">
                    <div class="box-top">
                        <div class="box-icon"><span class="icon icon-primary icon-lg-bigger material-icons-supervisor_account"></span></div>
                        <div class="box-header">
                            <h5><a href="#"><?= Yii::t('frontend', 'Sales') ?> &amp;<?= Yii::t('frontend', 'Trading') ?> </a></h5>
                        </div>
                    </div>
                    <div class="divider"></div>
                    <div class="box-body">
                        <p class="text-gray-05"><?= Yii::t('frontend', 'This service offers a variety of benefits to boost your sales and suit your company’s fixed income.') ?></p>
                    </div>
                </article>
            </div>
        </div>
    </div>
</section>