<section class="bg-whisperapprox">
    <div class="container">
        <div class="row justify-content-sm-center justify-content-md-start">
            <div class="col-sm-10 col-md-6 text-start section-60 section-md-90">
                <div class="inset-right-30">
                    <h3><?= Yii::t('frontend', 'Our Mission') ?></h3>
                    <div class="box-photo-frame"><img src="/images/about-1-510x268.jpg" alt="" width="510" height="268"/>
                    </div>
                    <h6><?= Yii::t('frontend', 'Our Investment Principles') ?></h6>
                    <p><?= Yii::t('frontend', 'Our goal is to maximize investor return and minimize any potential loss by applying these principles in a disciplined and pragmatic process. We rely on facts, not speculation to purchase high return, predictable companies at significant discounts to intrinsic values.') ?></p>
                </div>
            </div>
            <div class="col-md-6 context-dark section-image-aside section-image-aside-right pos-relative-before-sm section-60 section-md-0 d-md-flex">
                <div class="section-image-aside-img" style="background-image: url(/images/bg-image-12.jpg)">
                    <div class="section-bordered-inside"></div>
                </div>
                <div class="row align-items-md-center offset-top-0">
                    <div class="col-md-11 col-lg-10 to-front offset-md-1 offset-lg-2">
                        <h3><?= Yii::t('frontend', 'Benefits & Risks') ?></h3>
                        <h5><?= Yii::t('frontend', 'Advantages of Managed Funds') ?></h5>
                        <p class="text-white-05"><?= Yii::t('frontend', 'The benefits of using managed funds include the ability to gain exposure to different asset classes and market sectors.') ?></p>
                        <ul class="list-marked">
                            <li class="text-white"><?= Yii::t('frontend', 'Diversification') ?></li>
                            <li class="text-white"><?= Yii::t('frontend', 'Potential wealth generation') ?></li>
                            <li class="text-white"><?= Yii::t('frontend', 'Cost-effective investment') ?></li>
                            <li class="text-white"><?= Yii::t('frontend', 'Access to a range of assets') ?></li>
                            <li class="text-white"><?= Yii::t('frontend', 'Geared exposure') ?></li>
                            <li class="text-white"><?= Yii::t('frontend', 'Underperforming investments') ?></li>
                        </ul><a class="button button-icon button-icon-right button-primary big" href="appointment.html"><span class="icon icon-xs fa-angle-right"></span><?= Yii::t('frontend', 'Make an Appointment') ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="bg-whisperapprox">
    <div class="container">
        <div class="row justify-content-sm-center justify-content-md-start">
            <div class="col-md-6 context-dark section-image-aside section-image-aside-left pos-relative-before-sm section-60 section-md-0 d-md-flex">
                <div class="section-image-aside-img" style="background-image: url(/images/bg-image-9.jpg)">
                    <div class="section-bordered-inside"></div>
                </div>
                <div class="row align-items-md-center offset-top-0">
                    <div class="col-lg-10 col-xl-9 to-front">
                        <div class="inset-lg-left-15">
                            <h3><?= Yii::t('frontend', 'Why Clients Choose Us') ?></h3>
                            <div class="unit unit-spacing-md flex-sm-row">
                                <div class="unit-left"><span class="icon icon-7"></span></div>
                                <div class="unit-body">
                                    <h6><?= Yii::t('frontend', 'Competent Professionals') ?></h6>
                                    <p class="text-white-05"><?= Yii::t('frontend', 'We work in an atmosphere of trust and camaraderie, where partners help each other.') ?></p>
                                </div>
                            </div>
                            <div class="unit unit-spacing-md flex-sm-row">
                                <div class="unit-left"><span class="icon icon-8"></span></div>
                                <div class="unit-body">
                                    <h6><?= Yii::t('frontend', 'Superior Service') ?></h6>
                                    <p class="text-white-05"><?= Yii::t('frontend', 'We are committed to providing clients with the best value and service in the industry.') ?></p>
                                </div>
                            </div>
                            <div class="unit unit-spacing-md flex-sm-row">
                                <div class="unit-left"><span class="icon icon-9"></span></div>
                                <div class="unit-body">
                                    <h6><?= Yii::t('frontend', 'Competitive Pricing') ?></h6>
                                    <p class="text-white-05"><?= Yii::t('frontend', 'Providing value through straightforward commissions and added services.') ?></p>
                                </div>
                            </div><a class="button button-icon button-icon-right button-primary big buttons-inset-horizontal-15" href="appointment.html"><span class="icon icon-xs fa-angle-right"></span><?= Yii::t('frontend', 'Make an Appointment') ?></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-10 col-md-6 text-start section-60 section-md-75">
                <div class="inset-left-30">
                    <div class="box-photo-frame d-block">
                        <div class="ratio">
                            <iframe class="ratio-item" src="//www.youtube.com/embed/z9sPGHw_uw0" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 d-none d-lg-inline-block"></div>
                        <div class="col-lg-10">
                            <blockquote class="quote-simple inset-lg-left-15">
                                <div class="quote-body">
                                    <q><?= Yii::t('frontend', 'Together, we can enable your business to achieve even more.') ?></q>
                                </div>
                            </blockquote>
                            <p class="text-dusty-gray"><?= Yii::t('frontend', 'Investment Smart leads the industry in wealth management. Our independent RIA and broker services are powered by over 20 years of industry experience.') ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>