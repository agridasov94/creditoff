<?php
/**
 * @var $this \yii\web\View
 * @var $shortFormLoanRequest \frontend\src\useCase\loanRequest\shortForm\ShortFormLoanRequest
 */

use common\src\entities\product\entity\ProductQuery;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

\frontend\assets\JqueryUI::register($this);
?>
<div class="short-form-wrapper">
    <div class="container container-custom" style="position: relative;">
        <div class="form-wrapper">
            <?php $form = ActiveForm::begin(['action' => '/loan-request/create', 'method' => 'GET', 'options' => ['class' => 'short-form', 'id' => 'short-form'], 'enableClientScript' => false]) ?>
            <div>
            <?= $form->field($shortFormLoanRequest, 'productId', [
                    'template' => '{input}',
                    'options' => [
                        'class' => 'moth-form'
                    ]
                ])->dropDownList(ProductQuery::getList(), [
                    'class' => 'form-select',
                    'prompt' => $shortFormLoanRequest->getAttributeLabel('productId'),
                    'id' => 'loan_type',
                    'required' => true
                ]) ?>

                <?= $form->field($shortFormLoanRequest, 'loanAmount', [
                    'template' => '{label} {input}',
                    'options' => [
                        'class' => 'inp-wrap'
                    ]
                ])->input('text', ['class' => 'inp-set', 'id' => 'loan_amount', 'required' => true])->label(null, ['class' => 'lab-set']) ?>

                <div id="slider-range-min"></div>

                <?= $form->field($shortFormLoanRequest, 'loanTerm', [
                    'template' => '{label} 
                        <div class="input-group" style="width: 120px;">
                            {input}
                            <span class="imput-group-text" id="basic-addon2">
                                <span class="icon icon-md icon-primary fa-calendar icon-opt"></span>
                            </span>
                        </div>',
                    'options' => [
                        'class' => 'inp-wrap'
                    ]
                ])->input('number', ['class' => 'form-control', 'id' => 'loan_term', 'required' => true])->label(null, ['class' => 'lab-set']) ?>

                <div id="slider-range-term"></div>

                <?= $form->field($shortFormLoanRequest, 'firstName', [
                    'template' => '{input}'
                ])->input('text', [
                    'maxlength' => true,
                    'minlength' => true,
                    'class' => 'form-input form-control-has-validation wrap',
                    'placeholder' => $shortFormLoanRequest->getAttributeLabel('firstName')
                ])->label(false) ?>

                <?= $form->field($shortFormLoanRequest, 'lastName', [
                    'template' => '{input}'
                ])->input('text', [
                    'maxlength' => true,
                    'minlength' => true,
                    'class' => 'form-input form-control-has-validation wrap',
                    'placeholder' => $shortFormLoanRequest->getAttributeLabel('lastName')
                ])->label(false) ?>

                <?= $form->field($shortFormLoanRequest, 'phoneNumber', [
                    'template' => '{input}'
                ])->input('text', [
                    'maxlength' => true,
                    'minlength' => true,
                    'class' => 'form-input form-control-has-validation wrap',
                    'placeholder' => $shortFormLoanRequest->getAttributeLabel('phoneNumber')
                ])->label(false) ?>

                <?= $form->field($shortFormLoanRequest, 'acceptPolicy', [
                    'template' => '{input}<a href="'.\common\src\helpers\app\SettingHelper::getPrivacyPolicyFilePath().'" target="_blank">{label}</a>'
                ])->checkbox([
                    'class' => 'checkbox',
                    'required' => true
                ], false)->label(null, ['class' => 'lab-set-sec', 'for' => '']) ?>
            </div>

            <div class="short-form-wrapper-btn">
                <?= Html::submitButton(Yii::t('frontend', 'Confirm'), [
                    'class' => 'button button-primary btn-wrap',
                    'id' => 'confirm_loan_request'
                ]) ?>
            </div>
            <?php $form::end(); ?>
        </div>
    </div>
</div>
<?php
$getProductDataUrl = \yii\helpers\Url::to(['/loan-request/ajax-term-range']);
$js = <<<JS
$(document).ready( function () {
    let sliderRangeMinAmount = $("#slider-range-min");
    let sliderRangerTerm = $("#slider-range-term");
    let inputLoanAmount = $('#loan_amount');
    let inputLoanTerm = $('#loan_term');
    let minAmount = 0;
    let maxAmount = 0;
    let minTerm = 0;
    let maxTerm = 0;
    
    function initSlider(elem, min, max, value, inputElem) {
        elem.slider({
            range: "min",
            value: value,
            min: min,
            max: max,
            slide: function( event, ui ) {
                inputElem.val(ui.value);
            }
        });
    }
    $('#slider-range-min').on('change', function(){
        $('#loan_amount').val($('#slider-range-min').val());
    });

    inputLoanAmount.on('keyup change', function(){
        sliderRangeMinAmount.slider('destroy');
        initSlider(sliderRangeMinAmount, minAmount, maxAmount, inputLoanAmount.val(), inputLoanAmount);
    });
    
    inputLoanTerm.on('keyup change', function(){
        sliderRangerTerm.slider('destroy');
        initSlider(sliderRangerTerm, minTerm, maxTerm, inputLoanTerm.val(), inputLoanTerm);
    });
    
    inputLoanAmount.attr('min', 0).attr('max', 0).val(0);
    inputLoanTerm.attr('min', 0).attr('max', 0).val(0);
    initSlider(sliderRangeMinAmount, 0, 0, 0, inputLoanAmount)
    initSlider(sliderRangerTerm, 0, 0, 0, inputLoanTerm)
    
    $('#loan_type').on('change', function () {
        let submitBtn = $('#confirm_loan_request');
        submitBtn.addClass('disabled').prop('disabled', true);
        let val = $(this).val();
        
        $.ajax({
            url: '$getProductDataUrl',
            data: {productId: val},
            type: 'post',
            dataType: 'json',
            success: function (data) {
                minAmount = data.minAmount;
                maxAmount = data.maxAmount;
                minTerm = data.minTerm;
                maxTerm = data.maxTerm;
                inputLoanAmount.attr('min', data.minAmount).attr('max', data.maxAmount).val(data.minAmount);
                inputLoanTerm.attr('min', data.minTerm).attr('max', data.maxTerm).val(data.minTerm);
                initSlider(sliderRangeMinAmount, data.minAmount, data.maxAmount, data.minAmount, inputLoanAmount);
                initSlider(sliderRangerTerm, data.minTerm, data.maxTerm, data.minTerm, inputLoanTerm);
                submitBtn.removeClass('disabled').prop('disabled', false);
            }
        })
    });
    
    document.getElementById('short-form').reset();
});
JS;
$this->registerJs($js, \yii\web\View::POS_END);
?>