<section class="section-50 section-md-90 section-md-bottom-100">
    <div class="container text-center">
        <h3>Latest News</h3>
        <div class="row row-40 row-offset-6 justify-content-sm-center">
            <div class="col-sm-8 col-md-5 col-xl-3">
                <div class="post-boxed d-xl-inline-block">
                    <div class="post-boxed-img-wrap"><a href="blog-post.html"><img src="/images/home-1-268x182.jpg" alt="" width="268" height="182"/></a></div>
                    <div class="post-boxed-caption">
                        <div class="post-boxed-title fw-bold"><a href="blog-post.html"><?= Yii::t('frontend', 'What’s the Right Asset Allocation For Investors?') ?></a></div>
                        <div class="offset-top-5">
                            <ul class="list-inline list-inline-dashed text-uppercase font-accent">
                                <li><?= Yii::t('frontend', 'JUNE 14, 2021') ?></li>
                                <li><span><?= Yii::t('frontend', 'by') ?><a class="text-primary" href="#"><?= Yii::t('frontend', 'ADMIN') ?></a></span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-8 col-md-5 col-xl-3">
                <div class="post-boxed d-xl-inline-block">
                    <div class="post-boxed-img-wrap"><a href="blog-post.html"><img src="/images/home-2-268x182.jpg" alt="" width="268" height="182"/></a></div>
                    <div class="post-boxed-caption">
                        <div class="post-boxed-title fw-bold"><a href="blog-post.html"><?= Yii::t('frontend', 'How Return Assumptions Affect Investor Behavior') ?></a></div>
                        <ul class="list-inline list-inline-dashed text-uppercase font-accent">
                            <li><?= Yii::t('frontend', 'JUNE 20, 2021') ?></li>
                            <li><span><?= Yii::t('frontend', 'by') ?> <a class="text-primary" href="#"><?= Yii::t('frontend', 'ADMIN') ?></a></span></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-8 col-md-5 col-xl-3">
                <div class="post-boxed d-xl-inline-block">
                    <div class="post-boxed-img-wrap"><a href="blog-post.html"><img src="/images/home-3-268x182.jpg" alt="" width="268" height="182"/></a></div>
                    <div class="post-boxed-caption">
                        <div class="post-boxed-title fw-bold"><a href="blog-post.html"><?= Yii::t('frontend', 'Deconstructing 30 Year Stock Market Returns') ?></a></div>
                        <ul class="list-inline list-inline-dashed text-uppercase font-accent">
                            <li><?= Yii::t('frontend', 'JUNE 23, 2021') ?></li>
                            <li><span><?= Yii::t('frontend', 'by') ?> <a class="text-primary" href="#"><?= Yii::t('frontend', 'ADMIN') ?></a></span></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-8 col-md-5 col-xl-3">
                <div class="post-boxed d-xl-inline-block">
                    <div class="post-boxed-img-wrap"><a href="blog-post.html"><img src="/images/home-4-268x182.jpg" alt="" width="268" height="182"/></a></div>
                    <div class="post-boxed-caption">
                        <div class="post-boxed-title fw-bold"><a href="blog-post.html"><?= Yii::t('frontend', 'Why Money Manager Due Diligence is so Difficult') ?></a></div>
                        <ul class="list-inline list-inline-dashed text-uppercase font-accent">
                            <li><?= Yii::t('frontend', 'JUNE 12, 2021') ?></li>
                            <li><span><?= Yii::t('frontend', 'by') ?> <a class="text-primary" href="#"><?= Yii::t('frontend', 'ADMIN') ?></a></span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>