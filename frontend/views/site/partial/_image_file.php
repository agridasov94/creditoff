<?php
/* @var $banner MainBanner */

use common\src\entities\mainBanner\entity\MainBanner;
use yii\helpers\Html;

?>


<div class="swiper-slide swiper-slide-bottom overlay-5" data-slide-bg="<?= $banner->getPublicImagePath() ?>">
    <div class="swiper-slide-caption">
        <div class="container">
            <div class="row justify-content-start">
                <div class="col-md-5 col-lg-6 ">
                    <?php if ($banner->mb_subtitle): ?>
                        <div class="shilder-header-with-divider" data-caption-animate="fadeInUp" data-caption-delay="0s"><?= Html::encode($banner->mb_subtitle) ?></div>
                    <?php endif; ?>
                    <h2 class="slider-header" data-caption-animate="fadeInUp" data-caption-delay="100s"><?= Html::encode($banner->mb_title) ?></h2>
                    <?php if ($banner->mb_link && $banner->mb_link_text): ?>
                        <a class="button button-icon button-icon-right button-primary big" data-caption-animate="fadeInUp" data-caption-delay="250" href="<?= $banner->mb_link ?>"><span class="icon icon-xs fa-angle-right"></span><?= Html::encode($banner->mb_link_text) ?></a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>