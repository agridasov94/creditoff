<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use frontend\widgets\navbar\Breadcrumbs;
use yii\helpers\Html;

$this->title = $name;
?>

<div class="site-error">
    <div class="section-single-main">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-md-9 col-lg-7 wrapper">
                    <span><?= Yii::t('frontend', 'Page not found') ?></span>
                    <div class="text-extra-large-bordered">
                        <p><?= Yii::t('frontend', '404') ?></p>
                    </div>
                    <div class="error_bnts">
                        <?= Html::a(Yii::t('frontend', 'Back to home'), ['/'], [
                            'class' => 'button button-primary'
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
