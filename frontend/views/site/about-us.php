<section class="section-66 section-md-90 section-xl-bottom-120 bg-whisperapprox">
    <div class="container">
        <h3 style="text-align:center;"><?= Yii::t('frontend', 'About Us') ?></h3>
        <div class="row row-40 justify-content-xl-between align-items-center">
            <div class="col-md-12 col-xl-12 text-secondary">
                <div class="inset-md-right-15 inset-xl-right-0">
                    <p style="text-align:center;">
                    <?= Yii::t('frontend', 'Our Investment Company, located in Rockville, Maryland, 
                        is a full service real estate company that provides acquisition, 
                        development and finance expertise for both commercial and multi-family projects.') ?>
                        
                    </p>
                    <p style="text-align:center;">
                    <?= Yii::t('frontend', 'Our experience in structuring complicated financial transactions
                        while accommodating diverse partnership interests, 
                        has allowed it to achieve over $1 Billion in acquisitions and financing.') ?>
                        
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="bg-whisperapprox" style="background-color: #e1e4f4;">
    <div class="container">
        <div class="row justify-content-sm-center justify-content-md-start">
            <div class="col-sm-10 col-md-6 text-start section-60 section-md-90">
                <div>
                    <h3><?= Yii::t('frontend', 'Our Mission') ?></h3>
                    <div class="box-photo-frame"><img src="/images/about-1-510x268.jpg" alt="" width="510" height="268"/>
                    </div>
                    <h6><?= Yii::t('frontend', 'Our Investment Principles') ?></h6>
                    <p><?= Yii::t('frontend', 'Our goal is to maximize investor return and minimize any potential loss by applying these principles in a disciplined and pragmatic process. We rely on facts, not speculation to purchase high return, predictable companies at significant discounts to intrinsic values.') ?></p>
                </div>
            </div>
            <div class="col-md-6 context-dark section-image-aside section-image-aside-right pos-relative-before-sm section-60 section-md-0 d-md-flex">
                <div class="section-image-aside-img" style="background-image: url(/images/bg-image-12.jpg)">
                    <div class="section-bordered-inside"></div>
                </div>
                <div class="row align-items-md-center offset-top-0">
                    <div class="col-md-11 col-lg-10 to-front offset-md-1 offset-lg-2">
                        <h3><?= Yii::t('frontend', 'Benefits & Risks') ?></h3>
                        <h5><?= Yii::t('frontend', 'Advantages of Managed Funds') ?></h5>
                        <p class="text-white-05"><?= Yii::t('frontend', 'The benefits of using managed funds include the ability to gain exposure to different asset classes and market sectors.') ?></p>
                        <ul class="list-marked">
                            <li class="text-white"><?= Yii::t('frontend', 'Diversification') ?></li>
                            <li class="text-white"><?= Yii::t('frontend', 'Potential wealth generation') ?></li>
                            <li class="text-white"><?= Yii::t('frontend', 'Cost-effective investment') ?></li>
                            <li class="text-white"><?= Yii::t('frontend', 'Access to a range of assets') ?></li>
                            <li class="text-white"><?= Yii::t('frontend', 'Geared exposure') ?></li>
                            <li class="text-white"><?= Yii::t('frontend', 'Underperforming investments') ?></li>
                        </ul><a class="button button-icon button-icon-right button-primary big" href="appointment.html"><span class="icon icon-xs fa-angle-right"></span><?= Yii::t('frontend', 'Make an Appointment') ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
