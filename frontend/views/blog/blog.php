<?php
/**
 * @var $this \yii\web\View
 * @var $models BlogPosts[]
 * @var $pages \yii\data\Pagination
 * @var $categories \common\src\entities\blog\categories\entity\BlogCategories[]
 * @var $otherPosts BlogPosts[]
 */

use common\src\entities\blog\posts\entity\BlogPosts;
use frontend\assets\MainSliderAsset;
use yii\helpers\Html;
use yii\helpers\Url;

MainSliderAsset::register($this);
?>

<section class="section-60 section-md-75 section-xl-90">
    <div class="container" style="position: relative;">
        <div class="row ">
            <div class="col-md-9">
                <?php if(!empty($models)): ?>
                <div class="row row-30 justify-content-md-center justify-content-lg-start">
                    <?php foreach($models as $model): ?>
                    <div class="col-md-9 col-lg-6">
                        <article class="post-modern">
                            <div class="post-header">
                                <h5><?= Html::a($model->bp_title, $model->getUrl()) ?></h5>
                            </div>
                            <div class="post-body">
                                <figure><img src="<?= $model->getPublicPreviewPath() ?>" alt="<?= Html::encode($model->bp_title) ?>"/>
                                </figure>
                                <div class="post-inset">
                                    <p class="text-gray-05"><?= Html::encode($model->bp_short_description) ?></p>
                                </div>
                            </div>
                            <div class="post-footer">
                                <div class="post-meta">
                                    <ul class="post-list">
                                        <li class="object-inline"><span class="icon icon-xxs icon-black material-icons-query_builder"></span>
                                            <time class="text-gray-05" datetime="2021-01-01">2 days ago</time>
                                        </li>
                                        <li class="object-inline"><span class="icon icon-xxs icon-black material-icons-loyalty"></span>
                                            <ul class="list-tags-inline">
                                                <li><a href="<?=Url::to($model->category->getUrl()) ?>"><?= Html::encode($model->category->bc_name) ?></a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </article>
                    </div>
                    <?php endforeach; ?>
                </div>
                <div class="pagination-custom-wrap text-center">
                    <?= \yii\widgets\LinkPager::widget([
                        'pagination' => $pages,
                        'options' => [
                            'class' => 'pagination-custom'
                        ],
                        'disabledPageCssClass' => null,
                        'nextPageLabel' => Yii::t('frontend', 'Next'),
                        'prevPageLabel' => Yii::t('frontend', 'Prev'),
                        'disabledListItemSubTagOptions' => [
                            'tag' => 'a'
                        ]
                    ]); ?>
                </div>
                <?php else: ?>
                    <div class="alert alert-info" role="alert">
                        <?= Yii::t('frontend', 'Not found posts') ?>
                    </div>
                <?php endif; ?>
            </div>
            <div class="col-lg-4 col-xl-3" style="position: relative;">
                <?= $this->render('partial/_right_block', [
                    'categories' => $categories,
                    'otherPosts' => $otherPosts
                ]) ?>
            </div>
        </div>
    </div>
</section>