<?php
/***
 * @var $this \yii\web\View
 * @var $otherPosts \common\src\entities\blog\posts\entity\BlogPosts[]
 * @var $categories \common\src\entities\blog\categories\entity\BlogCategories[]
 */

use yii\helpers\Html;
use yii\helpers\Url;

?>
<!-- //Categories -->
<div class="blog-aside-item">
    <h6 class="text-uppercase"><?= Yii::t('frontend', 'Categories') ?></h6>
    <ul class="list-marked-bordered">
        <?php foreach($categories as $category): ?>
            <li><a href="<?= Url::to([$category->getUrl()]) ?>"><span><?= $category->bc_name ?></span><span class="text-dusty-gray">(<?= $category->countRelatedPostsByLang(Yii::$app->language) ?>)</span></a></li>
        <?php endforeach; ?>
    </ul>
</div>
<!--//End Catigories -->
<!-- //Comments -->
<div class="blog-aside-item">
    <h6 class="text-uppercase"><?= Yii::t('frontend', 'Other Posts') ?></h6>
    <?php foreach($otherPosts as $post): ?>
        <article class="post post-preview">
            <a href="<?= Url::to([$post->getUrl()]) ?>">
                <div class="unit flex-row unit-spacing-sm">
                    <div class="unit-left">
                        <figure class="post-image"><img src="<?= $post->getPublicPreviewPath() ?>" alt="<?= Html::encode($post->bp_title) ?>"></figure>
                    </div>
                    <div class="unit-body">
                        <div class="post-header">
                            <p><?= Yii::t('frontend', 'Your Credit Card Security Code is the Key to Fraud') ?></p>
                        </div>
                        <div class="post-meta">
                            <ul class="list-meta">
                                <li>
                                    <time datetime="2018-02-04">Feb 4, 2018</time>
                                </li>
                                <li> <?= Yii::t('frontend', '3 Comments') ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </a>
        </article>
    <?php endforeach; ?>
</div>