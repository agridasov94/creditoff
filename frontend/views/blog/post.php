<?php
/**
 * @var $this \yii\web\View
 * @var $blogPost \common\src\entities\blog\posts\entity\BlogPosts
 * @var $otherPosts \common\src\entities\blog\posts\entity\BlogPosts[]
 * @var $categories \common\src\entities\blog\categories\entity\BlogCategories[]
 */

use yii\helpers\Html;
use yii\helpers\Url;

?>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/<?= Yii::$app->language ?>_<?= strtoupper(Yii::$app->language) ?>/sdk.js#xfbml=1&version=v13.0&appId=670287224036028&autoLogAppEvents=1" nonce="UN0TfGWc"></script>
<section class="section-60 section-md-75 section-xl-90">
    <div class="container">
        <div class="row row-50">
            <div class="col-lg-8 col-xl-9">
                <article class="post post-single">
                    <div class="post-image">
                        <figure><img src="<?= $blogPost->getPublicPreviewPath() ?>" alt="<?= Html::encode($blogPost->bp_title) ?>" style="width: 100%; height: auto;"/>
                        </figure>
                    </div>
                    <div class="post-header">
                        <h4><?= Html::encode($blogPost->bp_title) ?></h4>
                    </div>
                    <div class="post-meta">
                        <ul class="list-bordered-horizontal">
                            <li>
                                <dl class="list-terms-inline">
                                    <dt>Date</dt>
                                    <dd>
                                        <time datetime="2021-01-22">January 22, 2021</time>
                                    </dd>
                                </dl>
                            </li>
                            <li>
                                <dl class="list-terms-inline">
                                    <dt><?= Yii::t('frontend', 'Posted by') ?></dt>
                                    <dd><?= Html::encode(strtoupper($blogPost->author->username)) ?></dd>
                                </dl>
                            </li>
                            <li>
                                <dl class="list-terms-inline">
                                    <dt>Comments</dt>
                                    <dd>3</dd>
                                </dl>
                            </li>
                            <li>
                                <dl class="list-terms-inline">
                                    <dt><?= Yii::t('frontend', 'Category') ?></dt>
                                    <dd><?= $blogPost->category->bc_name ?></dd>
                                </dl>
                            </li>
                        </ul>
                    </div>
                    <div class="divider-fullwidth bg-gray-light"></div>
                    <div class="post-body">
                        <?= $blogPost->bp_content ?>
                    </div>
                    <div class="post-footer">
                        <h5><?= Yii::t('frontend', 'Share this post') ?></h5>
                        <ul class="list-inline list-inline-xs">
                            <li><a class="icon icon-xxs-small link-tundora fa-facebook" href="#"></a></li>
                            <li><a class="icon icon-xxs-small link-tundora fa-twitter" href="#"></a></li>
                            <li><a class="icon icon-xxs-small link-tundora fa-google-plus" href="#"></a></li>
                            <li><a class="icon icon-xxs-small link-tundora fa-pinterest-p" href="#"></a></li>
                        </ul>
                    </div>
                </article>
                <div class="divider-fullwidth bg-gray-lighter"></div>
                <div class="comment-list-wrap">
                    <h4><?= Yii::t('frontend', 'Comments') ?></h4>
                    <div class="comment-list">
                        <div class="fb-comments" data-href="<?= Url::base('https'); ?>" data-width="100%" data-numposts="5"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-xl-3">
                <div class="blog-aside">
                    <?= $this->render('partial/_right_block', [
                        'categories' => $categories,
                        'otherPosts' => $otherPosts
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</section>