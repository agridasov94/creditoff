<?php

namespace frontend\models;

use kekaadrenalin\recaptcha3\ReCaptchaValidator;
use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $message;
    public $captcha;

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'message'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],

            [['captcha'], ReCaptchaValidator::class, 'acceptance_score' => 0, 'message' => \Yii::t('frontend', 'Invalid captcha')],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'name' => Yii::t('frontend', 'Name'),
            'email' => Yii::t('frontend', 'Email'),
            'message' => Yii::t('frontend', 'Message')
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return bool whether the email was sent
     */
    public function sendEmail($email)
    {
        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom($this->email)
            ->setSubject(Yii::t('frontend', 'Contact form from: ' . $this->name))
            ->setHtmlBody($this->message)
            ->send();
    }
}
