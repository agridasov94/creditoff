<?php

return [

    [
        'pattern' => 'about-us',
        'route' => 'site/about-us'
    ],

    [
        'pattern' => 'faq',
        'route' => 'site/questions-and-answers'
    ],

    [
        'pattern' => 'contact',
        'route' => 'site/contact'
    ],

    [
        'pattern' => 'terms-of-use',
        'route' => 'site/terms-of-use'
    ],

    [
        'pattern' => 'privacy-policy',
        'route' => 'site/privacy-policy'
    ],

    [
        'pattern' => 'blog/category/<category>',
        'route' => 'blog/category'
    ],

    [
        'pattern' => 'blog/post/<post>',
        'route' => 'blog/post'
    ],

    [
        'pattern' => 'cabinet/loan-application/<applicationNumber:[a-zA-Z0-9]+>/update',
        'route' => 'cabinet/update-loan-application'
    ],

    [
        'pattern' => 'cabinet/loan-application/cancel',
        'route' => 'cabinet/cancel-loan-application'
    ],

    [
        'pattern' => 'cabinet/loan-application/view',
        'route' => 'cabinet/view-loan-application'
    ]
];
