<?php

use kekaadrenalin\recaptcha3\ReCaptcha;
use common\components\i18n\Formatter;
use common\src\entities\client\entity\Client;
use common\src\entities\users\entity\Users;
use sharnhorst\yii2\multiLanguage\MultiLangUrlManager;
use yii\log\DbTarget;
use yii\web\UserEvent;

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log',
        'multiLanguage',
    ],
    'controllerNamespace' => 'frontend\controllers',
    'language' => 'ro',
    'components' => [
        'request' => [
            'class' => \sharnhorst\yii2\multiLanguage\MultiLangRequest::class,
            'baseUrl' => '',
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => Client::class,
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
            'loginUrl' => ['/client/login']
        ],
        'backendUser' => [
            'class' => \yii\web\User::class,
            'identityClass' => Users::class,
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true, 'domain' => $params['domain'] ?? ''],
            'on afterLogin' => static function (UserEvent $event) {
                Yii::$app->backendUser->identity->loginHandler();
            },
            'on afterLogout' => static function (UserEvent $event) {
                $event->identity->logoutHandler();
            },
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
            'cookieParams' => [
                'domain' => $params['domain'] ?? '',
                'httpOnly' => true
            ]
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                'db-error' => [
                    'class' => DbTarget::class,
                    'levels' => ['error', 'warning'],
                    'except' => [
                        'yii\web\HttpException:404',
                        'yii\web\HttpException:403',
                        'yii\web\HttpException:400',
                    ],
                    'logVars' => [],
                ],
                'db-info' => [
                    'class' => DbTarget::class,
                    'levels' => ['info'],
                    'except' => [
                        'yii\web\HttpException:404',
                    ],
                    'logVars' => [],
                    'categories' => ['info\*', 'log\*']
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
            "class" => MultiLangUrlManager::class,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'baseUrl' => '/',
            'scriptUrl' => '/',
            'rules' => require __DIR__ . '/rules.php',
        ],

        'assetManager' => [
            'forceCopy' => false,
            'hashCallback' => static function ($path) {
                return hash('md4', $path);
            },
            'basePath' => '@webroot/assets',
            'baseUrl' => '/assets',
            'bundles' => [
                \yii\web\JqueryAsset::class => [
                    'js' => []
                ],
                \yii\web\YiiAsset::class => [
                    'depends' => [\frontend\assets\AppAsset::class]
                ],
                \yii\bootstrap\BootstrapAsset::class => [
                    'class' => \frontend\assets\AppAsset::class,
                ],
                \unclead\multipleinput\assets\MultipleInputAsset::class => [
                    'depends' => [\frontend\assets\AppAsset::class]
                ],
                \yii\bootstrap4\BootstrapPluginAsset::class => [
                    'depends' => [\frontend\assets\AppAsset::class]
                ]
            ]
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => \yii\i18n\DbMessageSource::class,
                    'db' => 'db',
                    'sourceLanguage' => 'xx-XX', // Developer language
                    'sourceMessageTable' => '{{%language_source}}',
                    'messageTable' => '{{%language_translate}}',
                    'cachingDuration' => 86400,
                    'enableCaching' => false,
                ],
            ],
        ],
        "multiLanguage" => [
            "class" => \sharnhorst\yii2\multiLanguage\MultiLangComponent::class,
            'langs' => ['ru', 'en', 'ro'],
            'default_lang' => 'ro',         //Language to which no language settings are added.
            'lang_param_name' => 'lang',
        ],
        'formatter' => [
            'class' => Formatter::class,
            'dateFormat' => 'php:d-M-Y', //'dd.MM.yyyy',
            'datetimeFormat' => 'php:d-M-Y [H:i]',
            'timeFormat' => 'php:H:i',
            //'decimalSeparator' => ',',
            //'thousandSeparator' => ' ',
            //'currencyCode' => 'USD',
        ],
        'reCaptcha3' => [
            'class'      => ReCaptcha::class,
            'site_key'   => '',
            'secret_key' => '',
        ],
    ],
    'params' => $params,
];
