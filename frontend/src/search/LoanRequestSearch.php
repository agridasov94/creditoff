<?php

namespace frontend\src\search;

use common\src\entities\loanApplication\entity\LoanApplication;
use yii\data\ActiveDataProvider;

class LoanRequestSearch extends LoanApplication
{
    public function search(int $clientId, array $params)
    {
        $query = LoanApplication::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'la_client_id' => $clientId
        ]);

        return $dataProvider;
    }
}