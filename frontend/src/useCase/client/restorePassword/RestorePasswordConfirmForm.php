<?php

namespace frontend\src\useCase\client\restorePassword;

use common\src\entities\client\entity\Client;
use common\src\entities\client\entity\ClientQuery;
use yii\base\Model;
use yii\redis\Connection;

class RestorePasswordConfirmForm extends Model
{
    public $clientUid;
    public $password;
    public $rePassword;
    public $reCaptcha;
    public $code;

    private ?Client $_client = null;

    public function rules(): array
    {
        return [
            [['password', 'rePassword', 'reCaptcha', 'code', 'clientUid'], 'required'],
            [['password'], 'string', 'min' => 8],
            [['code'], 'string'],
            ['rePassword', 'compare', 'compareAttribute' => 'password'],
            [['code'], 'validateCode']
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'password' => \Yii::t('frontend', 'Password'),
            'rePassword' => \Yii::t('frontend', 'Repeat password'),
            'code' => \Yii::t('frontend', 'Verification code'),
        ];
    }

    private function getClient(): ?Client
    {
        if (!$this->_client) {
            return $this->_client = ClientQuery::findByUid($this->clientUid);
        }
        return $this->_client;
    }

    public function validateCode()
    {
        /** @var Connection $redis */
        $redis = \Yii::$app->redis;
        $client = $this->getClient();
        if (!$redis->exists(Client::getActivationCodeKey($client->c_uid))) {
            $this->addError('general', \Yii::t('frontend', 'Verification code expired'));
            return false;
        }
        $code = $redis->get(Client::getActivationCodeKey($client->c_uid));
        if ($code !== $this->code) {
            $this->addError('activationCode', \Yii::t('frontend', 'Code is not valid'));
            return false;
        }
        return true;
    }
}