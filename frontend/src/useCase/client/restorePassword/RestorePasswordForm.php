<?php

namespace frontend\src\useCase\client\restorePassword;

use kekaadrenalin\recaptcha3\ReCaptchaValidator;
use yii\base\Model;

class RestorePasswordForm extends Model
{
    public $idnp;
    public $partialPhoneNumber;
    public $reCaptcha;

    public function rules(): array
    {
        return [
            [['idnp', 'partialPhoneNumber', 'reCaptcha'], 'required'],
            [['idnp', 'partialPhoneNumber', 'reCaptcha'], 'trim'],
            [['idnp'], 'string', 'min' => 13, 'max' => 13],
            [['idnp'], 'match', 'pattern' => '/^[0-9]+$/'],
            [['partialPhoneNumber'], 'string', 'min' => 4, 'max' => 4],
            [['reCaptcha'], ReCaptchaValidator::class, 'acceptance_score' => 0, 'message' => \Yii::t('frontend', 'Invalid captcha')]
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'idnp' => \Yii::t('frontend', 'Enter Your IDNP'),
            'partialPhoneNumber' => \Yii::t('frontend', 'Enter last 4 numbers of your phone number')
        ];
    }
}