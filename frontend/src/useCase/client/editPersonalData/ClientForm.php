<?php

namespace frontend\src\useCase\client\editPersonalData;

use common\src\entities\client\entity\Client;
use common\src\entities\users\entity\Users;
use yii\base\Model;

class ClientForm extends Model
{
    public $name;
    public $lastName;
    public $birthDate;
    public $idnp;
    public $gender;
    public $civilStatus;
    public $socialStatus;
    public $ownProperty;

    public function rules()
    {
        return [
            [['civilStatus', 'socialStatus', 'ownProperty'], 'integer'],
        ];
    }

    public static function createByClientEntity(Client $client)
    {
        $self = new self();
        $self->name = $client->c_name;
        $self->lastName = $client->c_last_name;
        $self->birthDate = $client->c_birth_date;
        $self->idnp = $client->c_idnp;
    }

    public function formName(): string
    {
        return 'client';
    }
}