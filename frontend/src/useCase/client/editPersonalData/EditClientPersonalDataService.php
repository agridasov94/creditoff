<?php

namespace frontend\src\useCase\client\editPersonalData;

use common\src\entities\client\email\entity\ClientEmailQuery;
use common\src\entities\client\email\entity\ClientEmailRepository;
use common\src\entities\client\employmentData\entity\ClientEmploymentDataQuery;
use common\src\entities\client\employmentData\entity\ClientEmploymentDataRepository;
use common\src\entities\client\entity\ClientRepository;
use common\src\entities\client\passportData\entity\ClientPassportDataRepository;
use common\src\entities\client\phone\entity\ClientPhoneQuery;
use common\src\entities\client\phone\entity\ClientPhoneRepository;
use common\src\helpers\app\AppHelper;

class EditClientPersonalDataService
{
    private ClientRepository $clientRepository;
    private ClientEmploymentDataRepository $clientEmploymentDataRepository;
    private ClientPassportDataRepository $clientPassportDataRepository;
    private ClientEmailRepository $clientEmailRepository;
    private ClientPhoneRepository $clientPhoneRepository;

    public function __construct(
        ClientRepository $clientRepository,
        ClientEmploymentDataRepository $clientEmploymentDataRepository,
        ClientPassportDataRepository $clientPassportDataRepository,
        ClientEmailRepository $clientEmailRepository,
        ClientPhoneRepository $clientPhoneRepository
    ) {
        $this->clientRepository = $clientRepository;
        $this->clientEmploymentDataRepository = $clientEmploymentDataRepository;
        $this->clientPassportDataRepository = $clientPassportDataRepository;
        $this->clientEmailRepository = $clientEmailRepository;
        $this->clientPhoneRepository = $clientPhoneRepository;
    }

    public function update(EditClientDataForm $form, array $postData)
    {
        $childBirthDate = [];
        if (!empty($postData[ChildrenForm::getFormName()])) {
            foreach ($postData[ChildrenForm::getFormName()] as $children) {
                $childBirthDate[] = ['dateOfBirth' => $children['dateOfBirth'] ?? ''];
            }
        }

        $transaction = \Yii::$app->db->beginTransaction();

        try {
            $client = $form->client;
            $client->c_children = $childBirthDate;
            $client->detachBehavior('user');
            $this->clientRepository->save($client);

            ClientEmploymentDataQuery::removeByClientId($client->c_id);
            foreach ($form->clientEmploymentData as $employmentDataModel) {
                $employmentDataModel->setIsNewRecord(true);
                $employmentDataModel->ced_updated_dt = date('Y-m-d H:i:s');
                $employmentDataModel->detachBehavior('user');
                $this->clientEmploymentDataRepository->save($employmentDataModel);
            }

            ClientEmailQuery::removeByClientId($client->c_id);
            foreach ($form->clientEmail as $clientEmail) {
                $clientEmail->setIsNewRecord(true);
                $clientEmail->ce_updated_dt = date('Y-m-d H:i:s');
                $clientEmail->detachBehavior('user');
                $this->clientEmailRepository->save($clientEmail);
            }

            ClientPhoneQuery::removeByClientId($client->c_id);
            foreach ($form->clientPhone as $clientPhone) {
                $clientPhone->setIsNewRecord(true);
                $clientPhone->cp_updated_dt = date('Y-m-d H:i:s');
                $clientPhone->detachBehavior('user');
                $this->clientPhoneRepository->save($clientPhone);
            }

            $form->clientPassportData->detachBehavior('user');
            $this->clientPassportDataRepository->save($form->clientPassportData);

            $transaction->commit();
        } catch (\RuntimeException $e) {
            $transaction->rollBack();
            \Yii::error(AppHelper::throwableLog($e, true), 'EditClientPersonalDataService::update::RuntimeException');
            $form->addError('general', $e->getMessage());
        } catch (\Throwable $e) {
            $transaction->rollBack();
            \Yii::error(AppHelper::throwableLog($e, true), 'EditClientPersonalDataService::update::Throwable');
            $form->addError('general', 'Internal Server Error');
        }
    }
}