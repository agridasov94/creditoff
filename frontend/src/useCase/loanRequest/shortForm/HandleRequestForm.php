<?php

namespace frontend\src\useCase\loanRequest\shortForm;

use common\src\entities\client\entity\Client;
use common\src\entities\client\entity\ClientQuery;
use yii\base\Model;
use yii\redis\Connection;

class HandleRequestForm extends Model
{
    public $activationCode;
    public $password;
    public $rePassword;
    public $clientUid;

    private ?Client $_client = null;

    public function rules(): array
    {
        return [
            [['activationCode', 'password', 'rePassword', 'clientUid'], 'required'],
//            [['clientUid'], 'exist', 'skipOnEmpty' => false, 'skipOnError' => true, 'targetClass' => Client::class, 'targetAttribute' => 'c_uid'],
            [['password', 'rePassword'], 'string', 'min' => 8],
            ['rePassword', 'compare', 'compareAttribute' => 'password'],
            [['clientUid'], 'validateClient'],
            [['activationCode'], 'validateActivationCode']
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'activationCode' => \Yii::t('frontend', 'Activation Code'),
            'password' => \Yii::t('frontend', 'Password'),
            'rePassword' => \Yii::t('frontend', 'Repeat Password'),
            'clientUid' => \Yii::t('frontend', 'Client Unique Id'),
        ];
    }

    public function validateClient()
    {
        $client = $this->getClient();
        if (!$client) {
            $this->addError('general', \Yii::t('frontend', 'Client not found'));
            return false;
        }
        if ($client->c_profile_status !== Client::PROFILE_STATUS_REGISTERED) {
            $this->addError('general', \Yii::t('frontend', 'Profile already registered or removed'));
            return false;
        }
        return true;
    }

    private function getClient(): ?Client
    {
        if (!$this->_client) {
            return $this->_client = ClientQuery::findByUid($this->clientUid);
        }
        return $this->_client;
    }

    public function validateActivationCode()
    {
        /** @var Connection $redis */
        $redis = \Yii::$app->redis;
        $client = $this->getClient();
        if (!$redis->exists(Client::getActivationCodeKey($client->c_uid))) {
            $this->addError('general', \Yii::t('frontend', 'Activation code expired'));
            return false;
        }
        $code = $redis->get(Client::getActivationCodeKey($client->c_uid));
        if ($code !== $this->activationCode) {
            $this->addError('activationCode', \Yii::t('frontend', 'Code is not equal'));
            return false;
        }
        return true;
    }
}