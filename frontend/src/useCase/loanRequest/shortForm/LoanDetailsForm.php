<?php

namespace frontend\src\useCase\loanRequest\shortForm;

use common\src\entities\product\entity\Product;
use common\src\entities\product\entity\ProductQuery;
use yii\base\Model;

class LoanDetailsForm extends Model
{
    public $amount;
    public $term;
    public $productId;
    public $companies;

    private ?Product $_product = null;

    public function rules(): array
    {
        return [
            [['amount', 'term', 'productId'], 'required'],
            [['amount', 'term', 'productId'], 'integer'],
            [['productId'], 'validateProduct', 'skipOnError' => false, 'skipOnEmpty' => 'false']
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'amount' => \Yii::t('frontend', 'Amount'),
            'term' => \Yii::t('frontend', 'Term'),
            'productId' => \Yii::t('frontend', 'Type of loan'),
            'companies' => \Yii::t('frontend', 'Compaines will receive loan request')
        ];
    }

    public function validateProduct(): bool
    {
        $product = $this->getProduct();
        if (!$product) {
            $this->addError('productId', \Yii::t('frontend', 'Product not found'));
            return false;
        }
        if ($this->amount < $product->p_min_credit_amount || $this->amount > $product->p_max_credit_amount) {
            $this->addError('amount', \Yii::t('frontend', 'Amount is out of range'));
            return false;
        }
        if ($this->term < $product->p_min_credit_term || $this->term > $product->p_max_credit_term) {
            $this->addError('term', \Yii::t('frontend', 'Term is out of range'));
            return false;
        }
        return true;
    }

    /**
     * @return Product|null
     */
    public function getProduct(): ?Product
    {
        if (!$this->_product && $this->productId) {
            $this->_product = ProductQuery::getActiveById($this->productId);
        }
        return $this->_product;
    }
}
