<?php

namespace frontend\src\useCase\loanRequest\shortForm;

use common\src\entities\client\entity\Client;
use common\src\entities\client\entity\ClientQuery;
use yii\base\Model;
use yii\redis\Connection;

class ResendVerificationCodeForm extends Model
{
    public string $cuid = '';

    private Client $_client;

    public function rules(): array
    {
        return [
            ['cuid', 'required'],
            ['cuid', 'string', 'max' => 15],
            ['cuid', 'validateClient']
        ];
    }

    public function formName(): string
    {
        return '';
    }

    public function getClient(): Client
    {
        return $this->_client;
    }

    public function validateClient(): bool
    {
        if (!$this->_client = ClientQuery::findByUid($this->cuid)) {
            $this->addError('cuid', \Yii::t('frontend', 'Not Found Client'));
            return false;
        }
        if ($this->_client->isBlocked()) {
            $this->addError('cuid', \Yii::t('frontend', 'Client profile blocked'));
            return false;
        }
        if ($this->_client->isActivated()) {
            $this->addError('cuid', \Yii::t('frontend', 'Client profile already activated'));
            return false;
        }
        /** @var $redis Connection */
        $redis = \Yii::$app->redis;
        if ($redis->exists(Client::getActivationCodeKey($this->cuid))) {
            $this->addError('cuid', \Yii::t('frontend', 'Verification Code already sent'));
            return false;
        }
        return true;
    }
}