<?php

namespace frontend\src\useCase\loanRequest\shortForm;

use backend\src\entities\partnerCompany\entity\PartnerCompany;
use borales\extensions\phoneInput\PhoneInputValidator;
use common\src\entities\client\email\entity\ClientEmail;
use common\src\entities\client\entity\Client;
use common\src\entities\client\phone\entity\ClientPhone;
use yii\base\Model;

class PersonalDataForm extends Model
{
    public $firstName;
    public $lastName;
    public $dateOfBirth;
    public $idnp;
    public $phoneNumber;
    public $email;
    public $comment;
    public $policy;
    public $gender;

    public function rules(): array
    {
        return [
            [['firstName', 'lastName', 'dateOfBirth', 'idnp', 'phoneNumber', 'policy', 'gender'], 'required'],
            [['email'], 'email', 'skipOnEmpty' => true],
            [['firstName', 'lastName'], 'trim'],
            [['firstName', 'lastName'], 'string', 'max' => 70, 'min' => 2],
            [['firstName', 'lastName'], 'match', 'pattern' => '/^[a-zA-Z\s-]+$/'],
            [['idnp'], 'string', 'max' => 13, 'min' => 13],
            [['idnp'], 'match', 'pattern' => '/^[0-9]+$/', 'message' => \Yii::t('frontend', 'IDNP must contain only numbers')],
            [['idnp'], 'unique', 'targetClass' => Client::class, 'targetAttribute' => 'c_idnp', 'message' => \Yii::t('frontend', 'IDNP alrready registered')],
            [['dateOfBirth'], 'string'],
            [['dateOfBirth'], 'date', 'format' => 'php:d-m-Y'],
            [['phoneNumber'], 'string', 'max' => 9, 'min' => 9, 'message' => \Yii::t('frontend', 'The phone number must be in the format 0XXXXXXXX and start with 07 or 06')],
            [['phoneNumber'], 'match', 'pattern' => '/^(07|06)([0-9]){7}$/', 'message' => \Yii::t('frontend', 'The phone number must be in the format 0XXXXXXXX and start with 07 or 06')],
            ['phoneNumber', 'unique', 'targetClass' => ClientPhone::class, 'targetAttribute' => ['phoneNumber' => 'cp_phone'], 'message' => \Yii::t('frontend', 'Phone number already registered')],
            [['email'], 'unique', 'skipOnError' => true, 'targetClass' => ClientEmail::class, 'targetAttribute' => 'ce_email', 'message' => \Yii::t('frontend', 'Email already registered')],
            [['policy'], 'boolean'],
            [['comment'], 'string', 'max' => 255],
            [['gender'], 'integer'],
            [['gender'], 'in', 'range' => array_keys(Client::getGenderListName())],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'firstName' => \Yii::t('frontend.short_form', 'First Name'),
            'lastName' => \Yii::t('frontend.short_form', 'Last Name'),
            'dateOfBirth' => \Yii::t('frontend.short_form', 'Date of birth'),
            'idnp' => \Yii::t('frontend.short_form', 'IDNP'),
            'phoneNumber' => \Yii::t('frontend.short_form', 'Phone number'),
            'policy' => \Yii::t('frontend', 'Accept” confirm că eu: (i) am citit și acceptat Termeni și condiții de folosire a portalului și Politica de Confidențialitate'),
            'email' => \Yii::t('frontend.short_form', 'Email'),
            'comment' => \Yii::t('frontend.short_form', 'Comment'),
            'gender' => \Yii::t('frontend.short_form', 'Gender'),
        ];
    }
}