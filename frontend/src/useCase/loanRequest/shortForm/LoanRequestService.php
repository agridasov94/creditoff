<?php

namespace frontend\src\useCase\loanRequest\shortForm;

use backend\src\entities\partnerCompany\entity\PartnerCompanyQuery;
use common\src\entities\client\email\entity\ClientEmail;
use common\src\entities\client\email\entity\ClientEmailRepository;
use common\src\entities\client\employmentData\entity\ClientEmploymentData;
use common\src\entities\client\employmentData\entity\ClientEmploymentDataRepository;
use common\src\entities\client\entity\Client;
use common\src\entities\client\entity\ClientRepository;
use common\src\entities\client\passportData\entity\ClientPassportData;
use common\src\entities\client\passportData\entity\ClientPassportDataRepository;
use common\src\entities\client\phone\entity\ClientPhone;
use common\src\entities\client\phone\entity\ClientPhoneRepository;
use common\src\entities\loanApplication\clientEmail\entity\LoanApplicationClientEmail;
use common\src\entities\loanApplication\clientEmail\entity\LoanApplicationClientEmailRepository;
use common\src\entities\loanApplication\clientPhone\entity\LoanApplicationClientPhone;
use common\src\entities\loanApplication\clientPhone\entity\LoanApplicationClientPhoneRepository;
use common\src\entities\loanApplication\companies\entity\LoanApplicationCompanies;
use common\src\entities\loanApplication\companies\entity\LoanApplicationCompaniesRepository;
use common\src\entities\loanApplication\entity\LoanApplication;
use common\src\entities\loanApplication\entity\LoanApplicationRepository;
use common\src\entities\loanApplication\entity\LoanApplicationStatus;
use frontend\components\jobs\LoanApplicationDistributionJob;
use Yii;

class LoanRequestService
{
    private ClientRepository $clientRepository;
    private ClientPhoneRepository $clientPhoneRepository;
    private ClientEmailRepository $clientEmailRepository;
    private LoanApplicationRepository $loanApplicationRepository;
    private ClientEmploymentDataRepository $clientEmploymentDataRepository;
    private ClientPassportDataRepository $clientPassportDataRepository;
    private LoanApplicationCompaniesRepository $loanApplicationCompaniesRepository;
    private LoanApplicationClientPhoneRepository $loanApplicationClientPhoneRepository;
    private LoanApplicationClientEmailRepository $loanApplicationClientEmailRepository;

    public function __construct(
        ClientRepository $clientRepository,
        ClientPhoneRepository $clientPhoneRepository,
        ClientEmailRepository $clientEmailRepository,
        LoanApplicationRepository $loanApplicationRepository,
        ClientEmploymentDataRepository $clientEmploymentDataRepository,
        ClientPassportDataRepository $clientPassportDataRepository,
        LoanApplicationCompaniesRepository $loanApplicationCompaniesRepository,
        LoanApplicationClientPhoneRepository $loanApplicationClientPhoneRepository,
        LoanApplicationClientEmailRepository $loanApplicationClientEmailRepository
    ) {
        $this->clientRepository = $clientRepository;
        $this->clientPhoneRepository = $clientPhoneRepository;
        $this->clientEmailRepository = $clientEmailRepository;
        $this->loanApplicationRepository = $loanApplicationRepository;
        $this->clientEmploymentDataRepository = $clientEmploymentDataRepository;
        $this->clientPassportDataRepository = $clientPassportDataRepository;
        $this->loanApplicationCompaniesRepository = $loanApplicationCompaniesRepository;
        $this->loanApplicationClientPhoneRepository = $loanApplicationClientPhoneRepository;
        $this->loanApplicationClientEmailRepository = $loanApplicationClientEmailRepository;
    }

    public function create(LoanRequestCreateForm $form): Client
    {
        $client = new Client();
        $client->c_name = $form->personalData->firstName;
        $client->c_last_name = $form->personalData->lastName;
        $client->c_birth_date = $form->personalData->dateOfBirth;
        $client->c_idnp = $form->personalData->idnp;
        $client->c_profile_status = Client::PROFILE_STATUS_REGISTERED;
        $client->c_gender = $form->personalData->gender;
        $client->generateUid();
        $client->setNotActivated();
        $client->generateAuthKey();

        $clientPhone = new ClientPhone();
        $clientPhone->cp_phone = $form->personalData->phoneNumber;

        $request = new LoanApplication();
        $request->la_product_id = $form->loanDetails->productId;
        $request->la_loan_amount = $form->loanDetails->amount;
        $request->la_loan_term = $form->loanDetails->term;
        $request->la_client_name = $client->getFullName();
        $request->la_client_birth_date = $form->personalData->dateOfBirth;
        $request->la_client_idnp = $client->c_idnp;
        $request->setStatus(LoanApplicationStatus::STATUS_SENT);
        $request->generateUid();

        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $clientId = $this->clientRepository->save($client);

            $clientPhone->cp_client_id = $clientId;
            $this->clientPhoneRepository->save($clientPhone);

            $clientEmploymentData = new ClientEmploymentData();
            $clientEmploymentData->ced_client_id = $clientId;
            $this->clientEmploymentDataRepository->save($clientEmploymentData);

            $clientPassportData = new ClientPassportData();
            $clientPassportData->cpd_client_id = $clientId;
            $this->clientPassportDataRepository->save($clientPassportData);

            $request->la_client_id = $clientId;
            $this->loanApplicationRepository->save($request);

            $companies = PartnerCompanyQuery::getListByProduct($form->loanDetails->productId);
            if (!$companies) {
                throw new \RuntimeException(Yii::t('frontend', 'None of the companies is considering applications for this product'));
            }
            foreach ($companies as $companyId => $companyName) {
                $loanCompanyRelation = new LoanApplicationCompanies();
                $loanCompanyRelation->lac_la_id = $request->la_id;
                $loanCompanyRelation->lac_partner_company_id = (int)$companyId;
                $this->loanApplicationCompaniesRepository->save($loanCompanyRelation);
            }

            $loanApplicationClientPhone = new LoanApplicationClientPhone();
            $loanApplicationClientPhone->lacp_client_id = $client->c_id;
            $loanApplicationClientPhone->lacp_phone = $clientPhone->cp_phone;
            $loanApplicationClientPhone->lacp_la_id = $request->la_id;
            $this->loanApplicationClientPhoneRepository->save($loanApplicationClientPhone);

            if (!empty($form->personalData->email)) {
                $clientEmail = new ClientEmail();
                $clientEmail->ce_email = $form->personalData->email;
                $clientEmail->ce_client_id = $clientId;
                $this->clientEmailRepository->save($clientEmail);

                $loanApplicationClientEmail = new LoanApplicationClientEmail();
                $loanApplicationClientEmail->lace_client_id = $client->c_id;
                $loanApplicationClientEmail->lace_email = $form->personalData->email;
                $loanApplicationClientEmail->lace_la_id = $request->la_id;
                $this->loanApplicationClientEmailRepository->save($loanApplicationClientEmail);

            }

            $transaction->commit();

            $job = new LoanApplicationDistributionJob($request->la_product_id);
            /** @var $queue \yii\queue\beanstalk\Queue */
            $queue = \Yii::$app->queue_loan_application_distribution;
            $queue->push($job);

            return $client;
        } catch (\RuntimeException $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
}