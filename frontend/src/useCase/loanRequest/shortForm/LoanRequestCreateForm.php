<?php

namespace frontend\src\useCase\loanRequest\shortForm;

use elisdn\compositeForm\CompositeForm;
use kekaadrenalin\recaptcha3\ReCaptchaValidator;

/**
 * @var $step string|null
 * @var $loanDetails LoanDetailsForm
 * @var $personalData PersonalDataForm
 */
class LoanRequestCreateForm extends CompositeForm
{
    const STEP_DETAILS = 'details';
    const STEP_PERSONAL_DATA = 'personal_data';
    const STEP_HANDLE_REQUEST = 'handle_request';
    const STEP_PAYMENT_MONEY = 'payment_money';

    public $step;
    public $activeStep;
    public $reCaptcha;

    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->loanDetails = new LoanDetailsForm();
        $this->personalData = new PersonalDataForm();
    }

    public function rules(): array
    {
        return [
            [['reCaptcha'], 'required'],
            [['step'], 'string'],
            [['reCaptcha'], ReCaptchaValidator::class, 'acceptance_score' => 0, 'message' => \Yii::t('frontend', 'Invalid captcha')],
        ];
    }

    protected function internalForms(): array
    {
        return ['loanDetails', 'personalData'];
    }

    public static function getStepList(): array
    {
        return [
            self::STEP_PERSONAL_DATA => \Yii::t('frontend', 'Personal Data'),
//            self::STEP_DETAILS => \Yii::t('frontend', 'Details'),
            self::STEP_HANDLE_REQUEST => \Yii::t('frontend', 'Handle Request'),
            self::STEP_PAYMENT_MONEY => \Yii::t('frontend', 'Payment of Money'),
        ];
    }

    public function fillInByShortForm(ShortFormLoanRequest $form)
    {
        $this->step = $form->step;
        $this->loanDetails->amount = $form->loanAmount;
        $this->loanDetails->term = $form->loanTerm;
        $this->loanDetails->productId = $form->productId;
        $this->personalData->firstName = $form->firstName;
        $this->personalData->lastName = $form->lastName;
        $this->personalData->phoneNumber = $form->phoneNumber;
        $this->personalData->policy = $form->acceptPolicy;
    }

    public function getProgressbarClassByStep(string $step): string
    {
        $class = [];
        switch ($step) {
            case self::STEP_DETAILS:
                if ($this->loanDetails->hasErrors()) {
                    $class[] = 'error step active';
                    $this->activeStep = $step;
                    break;
                }
                $class[] = 'done';
                if ($this->step === $step && $this->step !== $this->activeStep) {
                    $class[] = 'active';
                    $this->activeStep = $step;
                }
                $class[] = 'step';
                break;
            case self::STEP_PERSONAL_DATA:
                if ($this->personalData->hasErrors()) {
                    $class[] = 'error step active';
                    $this->activeStep = $step;
                    break;
                }

                $class[] = 'done';
                if ($this->step === $step && $this->step !== $this->activeStep) {
                    $class[] = 'active';
                    $this->activeStep = $step;
                }
                $class[] = 'step';
                break;
        }
        return trim(implode(' ', $class));
    }
}