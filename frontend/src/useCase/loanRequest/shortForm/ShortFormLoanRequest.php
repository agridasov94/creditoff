<?php

namespace frontend\src\useCase\loanRequest\shortForm;

use borales\extensions\phoneInput\PhoneInputValidator;
use yii\base\Model;

class ShortFormLoanRequest extends Model
{
    public $loanAmount;
    public $loanTerm;
    public $productId;
    public $firstName;
    public $lastName;
    public $phoneNumber;
    public $acceptPolicy;
    public $step;

    public function rules()
    {
        return [
            [['loanAmount', 'loanTerm', 'productId', 'firstName', 'lastName', 'phoneNumber', 'acceptPolicy'], 'required'],
            [['loanAmount', 'loanTerm'], 'number'],
            [['firstName', 'lastName'], 'trim'],
            [['firstName', 'lastName'], 'string', 'max' => 70, 'min' => 2],
            [['firstName', 'lastName'], 'match', 'pattern' => '/^[a-zA-Z\s-]+$/'],
            [['phoneNumber'], 'string', 'max' => 9, 'min' => 9, 'message' => \Yii::t('frontend', 'The phone number must be in the format 0XXXXXXXX and start with 07 or 06')],
            [['phoneNumber'], 'match', 'pattern' => '/^(07|06)([0-9]){7}$/', 'message' => \Yii::t('frontend', 'The phone number must be in the format 0XXXXXXXX and start with 07 or 06')],
        ];
    }

    public function attributeLabels()
    {
        return [
            'loanAmount' => \Yii::t('frontend', 'Loan Amount'),
            'loanTerm' => \Yii::t('frontend', 'Loan Term'),
            'productId' => \Yii::t('frontend', 'Type Of Loan'),
            'firstName' => \Yii::t('frontend', 'First Name'),
            'lastName' => \Yii::t('frontend', 'Last Name'),
            'phoneNumber' => \Yii::t('frontend', 'Phone Number 078123456'),
            'acceptPolicy' => \Yii::t('frontend', 'Accept” confirm că eu: (i) am citit și acceptat Termeni și condiții de folosire a portalului și Politica de Confidențialitate'),
        ];
    }

    public function formName(): string
    {
        return '';
    }
}