<?php

namespace frontend\src\useCase\loanRequest\update;

use backend\src\entities\partnerCompany\entity\PartnerCompany;
use borales\extensions\phoneInput\PhoneInputValidator;
use common\src\entities\client\email\entity\ClientEmail;
use common\src\entities\client\entity\Client;
use common\src\entities\client\phone\entity\ClientPhone;
use common\src\entities\loanApplication\entity\LoanApplication;
use yii\base\Model;

class PersonalDataForm extends Model
{
    public $firstName;
    public $lastName;
    public $dateOfBirth;
    public $idnp;
    public $comment;
    public $policy;
    public $gender;
    public $civilStatus;
    public $socialStatus;
    public $ownProperty;

    public function rules(): array
    {
        return [
            [['civilStatus', 'socialStatus', 'ownProperty'], 'integer'],
            [['civilStatus'], 'in', 'range' => array_keys(Client::getCivilStatusListName())],
            [['socialStatus'], 'in', 'range' => array_keys(Client::getSocialStatusListName())],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'firstName' => \Yii::t('frontend.short_form', 'First Name'),
            'lastName' => \Yii::t('frontend.short_form', 'Last Name'),
            'dateOfBirth' => \Yii::t('frontend.short_form', 'Date of birth'),
            'idnp' => \Yii::t('frontend.short_form', 'IDNP'),
            'phoneNumber' => \Yii::t('frontend.short_form', 'Phone number'),
            'policy' => \Yii::t('frontend', 'Accept” confirm că eu: (i) am citit și acceptat Termeni și condiții de folosire a portalului și Politica de Confidențialitate'),
            'email' => \Yii::t('frontend.short_form', 'Email'),
            'comment' => \Yii::t('frontend.short_form', 'Comment'),
            'gender' => \Yii::t('frontend.short_form', 'Gender'),
        ];
    }

    public function fillInByModels(Client $client, LoanApplication $application): self
    {
        $this->firstName = $client->c_name;
        $this->lastName = $client->c_last_name;
        $this->dateOfBirth = $client->c_birth_date;
        $this->idnp = $client->c_idnp;
        $this->gender = $client->c_gender;
        $this->civilStatus = $application->la_client_civil_status;
        $this->socialStatus = $application->la_client_social_status;
        $this->ownProperty = $application->la_client_own_property;
        return $this;
    }
}