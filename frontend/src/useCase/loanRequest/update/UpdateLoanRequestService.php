<?php

namespace frontend\src\useCase\loanRequest\update;

use common\src\entities\client\email\entity\ClientEmail;
use common\src\entities\client\email\entity\ClientEmailQuery;
use common\src\entities\client\email\entity\ClientEmailRepository;
use common\src\entities\client\employmentData\entity\ClientEmploymentData;
use common\src\entities\client\employmentData\entity\ClientEmploymentDataQuery;
use common\src\entities\client\employmentData\entity\ClientEmploymentDataRepository;
use common\src\entities\client\entity\Client;
use common\src\entities\client\entity\ClientRepository;
use common\src\entities\client\passportData\entity\ClientPassportDataRepository;
use common\src\entities\client\phone\entity\ClientPhone;
use common\src\entities\client\phone\entity\ClientPhoneQuery;
use common\src\entities\client\phone\entity\ClientPhoneRepository;
use common\src\entities\loanApplication\clientEmail\entity\LoanApplicationClientEmail;
use common\src\entities\loanApplication\clientEmail\entity\LoanApplicationClientEmailQuery;
use common\src\entities\loanApplication\clientEmail\entity\LoanApplicationClientEmailRepository;
use common\src\entities\loanApplication\clientEmployedData\entity\LoanApplicationClientEmployedData;
use common\src\entities\loanApplication\clientEmployedData\entity\LoanApplicationClientEmployedDataQuery;
use common\src\entities\loanApplication\clientEmployedData\entity\LoanApplicationClientEmployedDataRepository;
use common\src\entities\loanApplication\clientPassportData\entity\LoanApplicationClientPassportData;
use common\src\entities\loanApplication\clientPassportData\entity\LoanApplicationClientPassportDataRepository;
use common\src\entities\loanApplication\clientPhone\entity\LoanApplicationClientPhone;
use common\src\entities\loanApplication\clientPhone\entity\LoanApplicationClientPhoneQuery;
use common\src\entities\loanApplication\clientPhone\entity\LoanApplicationClientPhoneRepository;
use common\src\entities\loanApplication\companies\entity\LoanApplicationCompanies;
use common\src\entities\loanApplication\companies\entity\LoanApplicationCompaniesRepository;
use common\src\entities\loanApplication\entity\LoanApplication;
use common\src\entities\loanApplication\entity\LoanApplicationRepository;
use common\src\entities\loanApplication\entity\LoanApplicationStatus;
use common\src\helpers\app\AppHelper;
use frontend\components\jobs\LoanApplicationDistributionJob;

/**
 * @property-read LoanApplicationRepository $loanApplicationRepository
 * @property-read LoanApplicationClientEmailRepository $loanApplicationClientEmailRepository
 * @property-read LoanApplicationClientPhoneRepository $loanApplicationClientPhoneRepository
 * @property-read LoanApplicationClientEmployedDataRepository $loanApplicationClientEmployedDataRepository
 * @property-read LoanApplicationClientPassportDataRepository $loanApplicationClientPassportDataRepository
 * @property-read ClientEmailRepository $clientEmailRepository
 * @property-read ClientPhoneRepository $clientPhoneRepository
 * @property-read ClientRepository $clientRepository
 * @property-read ClientEmploymentDataRepository $clientEmploymentDataRepository
 * @property-read ClientPassportDataRepository $clientPassportDataRepository
 * @property-read LoanApplicationCompaniesRepository $loanApplicationCompaniesRepository
 */
class UpdateLoanRequestService
{
    private LoanApplicationRepository $loanApplicationRepository;
    private LoanApplicationClientEmailRepository $loanApplicationClientEmailRepository;
    private LoanApplicationClientPhoneRepository $loanApplicationClientPhoneRepository;
    private LoanApplicationClientEmployedDataRepository $loanApplicationClientEmployedDataRepository;
    private LoanApplicationClientPassportDataRepository $loanApplicationClientPassportDataRepository;
    private ClientEmailRepository $clientEmailRepository;
    private ClientPhoneRepository $clientPhoneRepository;
    private ClientRepository $clientRepository;
    private ClientEmploymentDataRepository $clientEmploymentDataRepository;
    private ClientPassportDataRepository $clientPassportDataRepository;
    private LoanApplicationCompaniesRepository $loanApplicationCompaniesRepository;

    public function __construct(
        LoanApplicationRepository $loanApplicationRepository,
        LoanApplicationClientEmailRepository $loanApplicationClientEmailRepository,
        LoanApplicationClientPhoneRepository $loanApplicationClientPhoneRepository,
        LoanApplicationClientEmployedDataRepository $loanApplicationClientEmployedDataRepository,
        LoanApplicationClientPassportDataRepository $loanApplicationClientPassportDataRepository,
        ClientEmailRepository $clientEmailRepository,
        ClientPhoneRepository $clientPhoneRepository,
        ClientRepository $clientRepository,
        ClientEmploymentDataRepository $clientEmploymentDataRepository,
        ClientPassportDataRepository $clientPassportDataRepository,
        LoanApplicationCompaniesRepository $loanApplicationCompaniesRepository
    ) {
        $this->loanApplicationRepository = $loanApplicationRepository;
        $this->loanApplicationClientEmailRepository = $loanApplicationClientEmailRepository;
        $this->loanApplicationClientPhoneRepository = $loanApplicationClientPhoneRepository;
        $this->loanApplicationClientEmployedDataRepository = $loanApplicationClientEmployedDataRepository;
        $this->loanApplicationClientPassportDataRepository = $loanApplicationClientPassportDataRepository;
        $this->clientEmailRepository = $clientEmailRepository;
        $this->clientPhoneRepository = $clientPhoneRepository;
        $this->clientRepository = $clientRepository;
        $this->clientEmploymentDataRepository = $clientEmploymentDataRepository;
        $this->clientPassportDataRepository = $clientPassportDataRepository;
        $this->loanApplicationCompaniesRepository = $loanApplicationCompaniesRepository;
    }

    public function handle(LoanRequestUpdateForm $form, Client $client, LoanApplication $loanApplication)
    {
        $loanApplication->la_loan_term = $form->loanDetails->term;
        $loanApplication->la_loan_amount = $form->loanDetails->amount;

        $childBirthDate = [];
        foreach ($form->children as $children) {
            $childBirthDate[] = ['dateOfBirth' => $children['dateOfBirth'] ?? ''];
        }
        $loanApplication->la_client_children = $childBirthDate;
        $loanApplication->la_client_civil_status = $form->personalData->civilStatus;
        $loanApplication->la_client_social_status = $form->personalData->socialStatus;
        $loanApplication->la_client_own_property = $form->personalData->ownProperty;

        $transaction = \Yii::$app->db->beginTransaction();
        try {

            $loanApplication->detachBehavior('user');
            $this->loanApplicationRepository->save($loanApplication);

            if ($form->clientEmail) {
                LoanApplicationClientEmailQuery::removeByLoanApplicationId($loanApplication->la_id);
                /** @var LoanApplicationClientEmail $clientEmailForm */
                foreach ($form->clientEmail as $clientEmailForm) {
                    $loanApplicationClientEmail = new LoanApplicationClientEmail();
                    $loanApplicationClientEmail->lace_client_id = $client->c_id;
                    $loanApplicationClientEmail->lace_la_id = $loanApplication->la_id;
                    $loanApplicationClientEmail->lace_email = $clientEmailForm->lace_email;
                    $this->loanApplicationClientEmailRepository->save($loanApplicationClientEmail);
                }
            }

            if ($form->clientPhone) {
                LoanApplicationClientPhoneQuery::removeByLoanApplicationId($loanApplication->la_id);
                /** @var LoanApplicationClientPhone $clientPhoneForm */
                foreach ($form->clientPhone as $clientPhoneForm) {
                    $loanApplicationClientPhone = new LoanApplicationClientPhone();
                    $loanApplicationClientPhone->lacp_client_id = $client->c_id;
                    $loanApplicationClientPhone->lacp_la_id = $loanApplication->la_id;
                    $loanApplicationClientPhone->lacp_phone = $clientPhoneForm->lacp_phone;
                    $this->loanApplicationClientPhoneRepository->save($loanApplicationClientPhone);
                }
            }

            LoanApplicationClientEmployedDataQuery::removeByLoanApplicationId($loanApplication->la_id);
            /** @var $clientEmploymentData LoanApplicationClientEmployedData */
            foreach ($form->clientEmploymentData as $clientEmploymentData) {
                $loanClientEmploymentData = new LoanApplicationClientEmployedData();
                $loanClientEmploymentData->laced_client_id = $client->c_id;
                $loanClientEmploymentData->laced_la_id = $loanApplication->la_id;
                $loanClientEmploymentData->laced_type = $clientEmploymentData->laced_type;
                $loanClientEmploymentData->laced_work_phone = $clientEmploymentData->laced_work_phone;
                $loanClientEmploymentData->laced_organization_name = $clientEmploymentData->laced_organization_name;
                $loanClientEmploymentData->laced_position = $clientEmploymentData->laced_position;
                $loanClientEmploymentData->laced_seniority = $clientEmploymentData->laced_seniority;
                $loanClientEmploymentData->laced_official_wages = $clientEmploymentData->laced_official_wages;
                $loanClientEmploymentData->laced_unofficial_wages = $clientEmploymentData->laced_unofficial_wages;
                $loanClientEmploymentData->laced_revenues_documented = $clientEmploymentData->laced_revenues_documented;
                $loanClientEmploymentData->laced_revenues_non_documented = $clientEmploymentData->laced_revenues_non_documented;
                $this->loanApplicationClientEmployedDataRepository->save($loanClientEmploymentData);
            }

            $clientPassportData = $loanApplication->clientPassportData ?? new LoanApplicationClientPassportData();
            $clientPassportData->lacpd_client_id = $client->c_id;
            $clientPassportData->lacpd_la_id = $loanApplication->la_id;
            $clientPassportData->lacpd_type = $form->clientPassportData->cpd_type;
            $clientPassportData->lacpd_series = $form->clientPassportData->cpd_series;
            $clientPassportData->lacpd_number = $form->clientPassportData->cpd_number;
            $clientPassportData->lacpd_issued_by = $form->clientPassportData->cpd_issued_by;
            $clientPassportData->lacpd_issue_date = $form->clientPassportData->cpd_issue_date;
            $clientPassportData->lacpd_issue_department_code = $form->clientPassportData->cpd_issue_department_code;
            $clientPassportData->lacpd_expiration_date = $form->clientPassportData->cpd_expiration_date;
            $clientPassportData->lacpd_registered_address = $form->clientPassportData->cpd_registered_address;
            $clientPassportData->lacpd_residence_address = $form->clientPassportData->cpd_residence_address;
            $this->loanApplicationClientPassportDataRepository->save($clientPassportData);

            if ($form->updatePersonalInfoData) {
                $client->c_civil_status = $form->personalData->civilStatus;
                $client->c_social_status = $form->personalData->socialStatus;
                $client->c_children = $childBirthDate;
                $client->c_own_property = $form->personalData->ownProperty;
                $client->detachBehavior('user');
                $this->clientRepository->save($client);

                ClientEmailQuery::removeByClientId($client->c_id);
                /** @var LoanApplicationClientEmail $email */
                foreach ($form->clientEmail as $email) {
                    $clientEmail = new ClientEmail();
                    $clientEmail->detachBehavior('user');
                    $clientEmail->ce_email = $email->lace_email;
                    $clientEmail->ce_client_id = $client->c_id;
                    $this->clientEmailRepository->save($clientEmail);
                }

                ClientPhoneQuery::removeByClientId($client->c_id);
                /** @var LoanApplicationClientPhone $phone */
                foreach ($form->clientPhone as $phone) {
                    $clientPhone = new ClientPhone();
                    $clientPhone->detachBehavior('user');
                    $clientPhone->cp_phone = $phone->lacp_phone;
                    $clientPhone->cp_client_id = $client->c_id;
                    $this->clientPhoneRepository->save($clientPhone);
                }
            }

            if ($form->updateEmployedData) {
                ClientEmploymentDataQuery::removeByClientId($client->c_id);
                /** @var LoanApplicationClientEmployedData $model */
                foreach ($form->clientEmploymentData as $model) {
                    $employmentDataModel = new ClientEmploymentData();
                    $employmentDataModel->ced_client_id = $client->c_id;
                    $employmentDataModel->ced_type = $model->laced_type;
                    $employmentDataModel->ced_work_phone = $model->laced_work_phone;
                    $employmentDataModel->ced_organization_name = $model->laced_organization_name;
                    $employmentDataModel->ced_position = $model->laced_position;
                    $employmentDataModel->ced_seniority = $model->laced_seniority;
                    $employmentDataModel->ced_official_wages = $model->laced_official_wages;
                    $employmentDataModel->ced_unofficial_wages = $model->laced_unofficial_wages;
                    $employmentDataModel->ced_revenues_documented = $model->laced_revenues_documented;
                    $employmentDataModel->ced_revenues_non_documented = $model->laced_revenues_non_documented;
                    $employmentDataModel->detachBehavior('user');
                    $this->clientEmploymentDataRepository->save($employmentDataModel);
                }
            }

            if ($form->updatePassportData) {
                $form->clientPassportData->detachBehavior('user');
                $this->clientPassportDataRepository->save($form->clientPassportData);
            }

            $transaction->commit();
        } catch (\RuntimeException $e) {
            $transaction->rollBack();
            $form->addError('general', $e->getMessage());
            \Yii::error(AppHelper::throwableLog($e, true), 'UpdateLoanRequestService::handle::RuntimeException');
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $form->addError('general', \Yii::t('frontend', 'Internal Server Error'));
            \Yii::error(AppHelper::throwableLog($e, true), 'UpdateLoanRequestService::handle::Throwable');
        }
    }
}