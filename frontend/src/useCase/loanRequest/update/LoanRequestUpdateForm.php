<?php

namespace frontend\src\useCase\loanRequest\update;

use common\src\entities\client\email\entity\ClientEmail;
use common\src\entities\client\employmentData\entity\ClientEmploymentData;
use common\src\entities\client\entity\Client;
use common\src\entities\client\passportData\entity\ClientPassportData;
use common\src\entities\client\phone\entity\ClientPhone;
use common\src\entities\loanApplication\clientEmail\entity\LoanApplicationClientEmail;
use common\src\entities\loanApplication\clientEmployedData\entity\LoanApplicationClientEmployedData;
use common\src\entities\loanApplication\clientPhone\entity\LoanApplicationClientPhone;
use common\src\entities\loanApplication\entity\LoanApplication;
use elisdn\compositeForm\CompositeForm;
use yii\helpers\VarDumper;

/**
 * @var $loanDetails LoanDetailsForm
 * @var $personalData PersonalDataForm
 * @var $children ChildrenForm[]
 * @var $clientEmail LoanApplicationClientEmail[]
 * @var $clientPhone LoanApplicationClientPhone[]
 * @var $clientEmploymentData LoanApplicationClientEmployedData[]
 * @var $clientPassportData ClientPassportData
 */
class LoanRequestUpdateForm extends CompositeForm
{
    private int $clientId;
    private int $loanApplicationId;
    private Client $client;

    const STEP_DETAILS = 'details';
    const STEP_PERSONAL_DATA = 'personal_data';
    const STEP_EMPLOYED_DATA = 'employed_data';
    const STEP_PASSPORT_DATA = 'passport_data';

    public $step;
    public $activeStep;
    public $updatePersonalInfoData;
    public $updateEmployedData;
    public $updatePassportData;

    public function __construct(Client $client, LoanApplication $loanApplication, $config = [])
    {
        parent::__construct($config);
        $this->clientId = $client->c_id;
        $this->loanApplicationId = $loanApplication->la_id;
        $this->client = $client;
        $this->loanDetails = new LoanDetailsForm($loanApplication);
        $this->personalData = (new PersonalDataForm())->fillInByModels($client, $loanApplication);
        $children = [];
        if ($loanApplication->la_client_children) {
            foreach ($loanApplication->la_client_children as $c_child) {
                $child = new ChildrenForm();
                $child->dateOfBirth = $c_child['dateOfBirth'] ?? '';
                $children[] = $child;
            }
        }
        $this->children = $children;

        $clientEmail = [];
        if ($loanApplication->clientEmails) {
            foreach ($loanApplication->clientEmails as $clientEmailModel) {
                $clientEmail[] = $clientEmailModel;
            }
        }
        $this->clientEmail = $clientEmail;

        $clientPhone = [];
        if ($loanApplication->clientPhones) {
            foreach ($loanApplication->clientPhones as $clientPhoneModel) {
                $clientPhone[] = $clientPhoneModel;
            }
        }
        $this->clientPhone = $clientPhone;

        $employmentData = [];
        if ($loanApplication->clientEmployedData) {
            foreach ($loanApplication->clientEmployedData as $employmentDataModel) {
                $employmentData[] = $employmentDataModel;
            }
        } else {
            $employmentData[] = new LoanApplicationClientEmployedData();
        }
        $this->clientEmploymentData = $employmentData;

        $this->clientPassportData = $client->clientPassportData ?? new ClientPassportData();
    }

    public function rules(): array
    {
        return [
            [['step'], 'string'],
            [['updateEmployedData', 'updatePassportData', 'updatePersonalInfoData'], 'boolean']
        ];
    }

    public function load($data, $formName = null)
    {
        if ($data) {
            $clientEmploymentData = [];
            foreach ($data['LoanApplicationClientEmployedData'] as $clientEmploymentDataPost) {
                if (!empty($clientEmploymentDataPost['laced_id'])) {
                    $clientEmploymentData[] = LoanApplicationClientEmployedData::findOne($clientEmploymentDataPost['laced_id']);
                } else {
                    $model = new LoanApplicationClientEmployedData();
                    $model->laced_client_id = $this->clientId;
                    $model->laced_la_id = $this->loanApplicationId;
                    $clientEmploymentData[] = $model;
                }
            }
            $this->clientEmploymentData = $clientEmploymentData;

            $childrenData = [];
            foreach ($data['ChildrenForm'] as $childrenDataPost) {
                $model = new ChildrenForm();
                $model->dateOfBirth = $childrenDataPost['dateOfBirth'];
                $childrenData[] = $model;
            }
            $this->children = $childrenData;

            $clientEmail = [];
            foreach ($data['LoanApplicationClientEmail'] as $clientEmailPost) {
                if (!empty($clientEmailPost['lace_id'])) {
                    $clientEmail[] = LoanApplicationClientEmail::findOne($clientEmailPost['lace_id']);
                } else if (!empty($clientEmailPost['lace_email'])) {
                    $model = new LoanApplicationClientEmail();
                    $model->lace_client_id = $this->clientId;
                    $model->lace_la_id = $this->loanApplicationId;
                    $clientEmail[] = $model;
                }
            }
            $this->clientEmail = $clientEmail;

            $clientPhone = [];
            foreach ($data['LoanApplicationClientPhone'] as $clientPhonePost) {
                if (!empty($clientPhonePost['lacp_id'])) {
                    $clientPhone[] = LoanApplicationClientPhone::findOne($clientPhonePost['lacp_id']);
                } else if (!empty($clientPhonePost['lacp_phone'])) {
                    $model = new LoanApplicationClientPhone();
                    $model->lacp_client_id = $this->clientId;
                    $model->lacp_la_id = $this->loanApplicationId;
                    $clientPhone[] = $model;
                }
            }
            $this->clientPhone = $clientPhone;
        }
        return parent::load($data, $formName); // TODO: Change the autogenerated stub
    }

    public static function getStepList(): array
    {
        return [
            self::STEP_DETAILS => \Yii::t('frontend', 'Details'),
            self::STEP_PERSONAL_DATA => \Yii::t('frontend', 'Personal info'),
            self::STEP_EMPLOYED_DATA => \Yii::t('frontend', 'Employed data'),
            self::STEP_PASSPORT_DATA => \Yii::t('frontend', 'Passport Data'),
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'updatePersonalInfoData' => \Yii::t('frontend', 'Update Personal Info of your profile'),
            'updatePassportData' => \Yii::t('frontend', 'Update Passport Info of your profile'),
            'updateEmployedData' => \Yii::t('frontend', 'Update Employed Info of your profile'),
        ];
    }

    public function getProgressbarClassByStep(string $step): string
    {
        $class = [];
        switch ($step) {
            case self::STEP_DETAILS:
                if ($this->loanDetails->hasErrors()) {
                    $class[] = 'error step active';
                    $this->activeStep = $step;
                    break;
                }
                $class[] = 'done';
                if ($this->step === $step && $this->step !== $this->activeStep) {
                    $class[] = 'active';
                    $this->activeStep = $step;
                }
                $class[] = 'step';
                break;
            case self::STEP_PERSONAL_DATA:
                $emailError = false;
                foreach ($this->clientEmail as $clientEmail) {
                    if ($clientEmail->hasErrors()) {
                        $emailError = true;
                        break;
                    }
                }
                $phoneErrors = false;
                foreach ($this->clientPhone as $clientPhone) {
                    if ($clientPhone->hasErrors()) {
                        $phoneErrors = true;
                        break;
                    }
                }
                $childError = false;
                foreach($this->children as $child) {
                    if ($child->hasErrors()) {
                        $childError = true;
                        break;
                    }
                }
                if ($this->personalData->hasErrors() || $emailError || $phoneErrors || $childError) {
                    $class[] = 'error step active';
                    $this->activeStep = $step;
                    break;
                }

                $class[] = 'done';
                if ($this->step === $step && $this->step !== $this->activeStep) {
                    $class[] = 'active';
                    $this->activeStep = $step;
                }
                $class[] = 'step';
                break;
            case self::STEP_EMPLOYED_DATA:
                foreach ($this->clientEmploymentData as $clientEmploymentData) {
                    if ($clientEmploymentData->hasErrors()) {
                        $class[] = 'error step active';
                        $this->activeStep = $step;
                        break;
                    }
                }

                $class[] = 'done';
                if ($this->step === $step && $this->step !== $this->activeStep) {
                    $class[] = 'active';
                    $this->activeStep = $step;
                }
                $class[] = 'step';
                break;
            case self::STEP_PASSPORT_DATA:
                if ($this->clientPassportData->hasErrors()) {
                    $class[] = 'error step active';
                    $this->activeStep = $step;
                    break;
                }

                $class[] = 'done';
                if ($this->step === $step && $this->step !== $this->activeStep) {
                    $class[] = 'active';
                    $this->activeStep = $step;
                }
                $class[] = 'step';
                break;

        }
        return trim(implode(' ', $class));
    }

    protected function internalForms(): array
    {
        return ['loanDetails', 'personalData', 'children', 'clientEmail', 'clientPhone', 'clientEmploymentData', 'clientPassportData'];
    }

    public function personalDataHasErrors(): bool
    {
        $childrenFormValidation = false;
        foreach ($this->children as $child) {
            if ($child->hasErrors()) {
                $childrenFormValidation = true;
                $this->addError('general', $child->getErrorSummary(true)[0]);
                break;
            }
        }
        $phoneValidation = false;
        foreach ($this->clientPhone as $phone) {
            if ($phone->hasErrors()) {
                $phoneValidation = true;
                $this->addError('general', $phone->getErrorSummary(true)[0]);
                break;
            }
        }
        return $this->personalData->hasErrors() || $childrenFormValidation || $phoneValidation;
    }
}