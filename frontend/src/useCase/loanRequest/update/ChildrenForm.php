<?php

namespace frontend\src\useCase\loanRequest\update;

use yii\base\Model;

class ChildrenForm extends Model
{
    public $dateOfBirth;

    public $confirm;

    public function rules(): array
    {
        return [
            ['dateOfBirth', 'date', 'format' => 'php:d-m-Y'],
        ];
    }

    public static function getFormName(): string
    {
        return 'ChildrenForm';
    }

    public function attributeLabels()
    {
        return [
            'dateOfBirth' => \Yii::t('client', 'Child Date of Birth')
        ];
    }
}