<?php

namespace frontend\src\useCase\loanRequest\cancel;
use backend\src\helpers\WebSocketHelper;
use common\src\entities\loanApplication\entity\LoanApplicationRepository;
use common\src\entities\loanApplicationAccess\entity\LoanApplicationAccessQuery;
use frontend\components\jobs\LoanApplicationDistributionJob;

class CancelLoanApplicationService
{
    private LoanApplicationRepository $loanApplicationRepository;
    public function __construct(LoanApplicationRepository $loanApplicationRepository)
    {
        $this->loanApplicationRepository = $loanApplicationRepository;
    }

    public function handle(string $number): void
    {
        $loanApplication = $this->loanApplicationRepository->findByNumber($number);
        $loanApplication->detachBehavior('user');
        $prevStatus = $loanApplication->la_status;
        $loanApplication->cancel();
        $this->loanApplicationRepository->save($loanApplication);

        $job = new LoanApplicationDistributionJob($loanApplication->la_product_id);
        /** @var $queue \yii\queue\beanstalk\Queue */
        $queue = \Yii::$app->queue_loan_application_distribution;
        $queue->push($job);

        $loanApplicationAccesses = LoanApplicationAccessQuery::findPendingByRequest($loanApplication->la_id);
        foreach ($loanApplicationAccesses as $applicationAccess) {
            WebSocketHelper::pub(
                [WebSocketHelper::getLoanApplicationPubSubKey($prevStatus, (int)$applicationAccess->laa_user_id)],
                'remove_loan_application',
                [
                    'loanApplicationId' => $loanApplication->la_id,
                ]
            );
        }
    }
}
