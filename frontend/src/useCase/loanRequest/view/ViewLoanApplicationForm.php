<?php

namespace frontend\src\useCase\loanRequest\view;

use common\src\entities\client\entity\Client;
use common\src\entities\loanApplication\entity\LoanApplicationQuery;
use yii\base\Model;

class ViewLoanApplicationForm extends Model
{
    public string $number = '';

    public function rules()
    {
        return [
            ['number' , 'required'] ,
            ['number', 'string', 'max' => 10, 'min' => 1],
            ['number', 'validateLoanApplication', 'skipOnError' => true, 'skipOnEmpty' => false]
        ];
    }

    public function formName()
    {
        return '';
    }

    public function validateLoanApplication(): bool
    {
        $loanApplication = LoanApplicationQuery::findByNumber($this->number);
        if (!$loanApplication) {
            $this->addError('number', \Yii::t('frontend', 'Loan Application not found.'));
            return false;
        }
        /** @var $client Client */
        $client = \Yii::$app->user->identity;
        if ($loanApplication->la_client_id !== $client->c_id) {
            $this->addError('number', \Yii::t('frontend', 'You are not the owner of this loan application.'));
            return false;
        }
        return true;
    }
}