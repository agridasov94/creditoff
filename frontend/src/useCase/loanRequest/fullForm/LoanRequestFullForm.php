<?php

namespace frontend\src\useCase\loanRequest\fullForm;

use common\src\entities\client\email\entity\ClientEmail;
use common\src\entities\client\employmentData\entity\ClientEmploymentData;
use common\src\entities\client\entity\Client;
use common\src\entities\client\passportData\entity\ClientPassportData;
use common\src\entities\client\phone\entity\ClientPhone;
use elisdn\compositeForm\CompositeForm;
use yii\helpers\VarDumper;

/**
 * @var $loanDetails LoanDetailsForm
 * @var $personalData PersonalDataForm
 * @var $children ChildrenForm[]
 * @var $clientEmail ClientEmail[]
 * @var $clientPhone ClientPhone[]
 * @var $clientEmploymentData ClientEmploymentData[]
 * @var $clientPassportData ClientPassportData
 */
class LoanRequestFullForm extends CompositeForm
{
    private int $clientId;
    private Client $client;

    const STEP_DETAILS = 'details';
    const STEP_PERSONAL_DATA = 'personal_data';
    const STEP_EMPLOYED_DATA = 'employed_data';
    const STEP_PASSPORT_DATA = 'passport_data';
    const STEP_HANDLE_REQUEST = 'handle_request';
    const STEP_PAYMENT_MONEY = 'payment_money';

    public $step;
    public $activeStep;
    public $updatePersonalInfoData = true;
    public $updateEmployedData = true;
    public $updatePassportData = true;

    public function __construct(Client $client, $config = [])
    {
        parent::__construct($config);
        $this->clientId = $client->c_id;
        $this->client = $client;
        $this->loanDetails = new LoanDetailsForm();
        $this->personalData = (new PersonalDataForm())->fillInByClient($client);
        $children = [];
        if ($client->c_children) {
            foreach ($client->c_children as $c_child) {
                $child = new ChildrenForm();
                $child->dateOfBirth = $c_child['dateOfBirth'] ?? '';
                $children[] = $child;
            }
        }
        $this->children = $children;

        $clientEmail = [];
        if ($client->clientEmails) {
            foreach ($client->clientEmails as $clientEmailModel) {
                $clientEmail[] = $clientEmailModel;
            }
        }
        $this->clientEmail = $clientEmail;

        $clientPhone = [];
        if ($client->clientPhones) {
            foreach ($client->clientPhones as $clientPhoneModel) {
                $clientPhone[] = $clientPhoneModel;
            }
        }
        $this->clientPhone = $clientPhone;

        $employmentData = [];
        if ($client->clientEmploymentData) {
            foreach ($client->clientEmploymentData as $employmentDataModel) {
                $employmentData[] = $employmentDataModel;
            }
        } else {
            $employmentData[] = new ClientEmploymentData();
        }
        $this->clientEmploymentData = $employmentData;

        $this->clientPassportData = $client->clientPassportData ?? new ClientPassportData();
    }

    public function rules(): array
    {
        return [
            [['step'], 'string'],
            [['updateEmployedData', 'updatePassportData', 'updatePersonalInfoData'], 'boolean']
        ];
    }

    public function load($data, $formName = null)
    {
        if ($data) {
            $clientEmploymentData = [];
            foreach ($data['ClientEmploymentData'] as $clientEmploymentDataPost) {
                if (!empty($clientEmploymentDataPost['ced_id'])) {
                    $clientEmploymentData[] = ClientEmploymentData::findOne($clientEmploymentDataPost['ced_id']);
                } else {
                    $model = new ClientEmploymentData();
                    $model->ced_client_id = $this->clientId;
                    $clientEmploymentData[] = $model;
                }
            }
            $this->clientEmploymentData = $clientEmploymentData;

            $childrenData = [];
            foreach ($data['ChildrenForm'] as $childrenDataPost) {
                $model = new ChildrenForm();
                $model->dateOfBirth = $childrenDataPost['dateOfBirth'];
                $childrenData[] = $model;
            }
            $this->children = $childrenData;

            $clientEmail = [];
            foreach ($data['ClientEmail'] as $clientEmailPost) {
                if (!empty($clientEmailPost['ce_id'])) {
                    $clientEmail[] = ClientEmail::findOne($clientEmailPost['ce_id']);
                } else if (!empty($clientEmailPost['ce_email'])) {
                    $model = new ClientEmail();
                    $model->ce_client_id = $this->clientId;
                    $clientEmail[] = $model;
                }
            }
            $this->clientEmail = $clientEmail;

            $clientPhone = [];
            foreach ($data['ClientPhone'] as $clientPhonePost) {
                if (!empty($clientPhonePost['cp_id'])) {
                    $clientPhone[] = ClientPhone::findOne($clientPhonePost['cp_id']);
                } else if (!empty($clientPhonePost['cp_phone'])) {
                    $model = new ClientPhone();
                    $model->cp_client_id = $this->clientId;
                    $clientPhone[] = $model;
                }
            }
            $this->clientPhone = $clientPhone;
        }
        return parent::load($data, $formName); // TODO: Change the autogenerated stub
    }

    public static function getStepList(): array
    {
        return [
            self::STEP_DETAILS => \Yii::t('frontend', 'Details'),
            self::STEP_PERSONAL_DATA => \Yii::t('frontend', 'Personal info'),
            self::STEP_EMPLOYED_DATA => \Yii::t('frontend', 'Employed data'),
            self::STEP_PASSPORT_DATA => \Yii::t('frontend', 'Passport Data'),
            self::STEP_HANDLE_REQUEST => \Yii::t('frontend', 'Handle Request'),
            self::STEP_PAYMENT_MONEY => \Yii::t('frontend', 'Payment of Money'),
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'updatePersonalInfoData' => \Yii::t('frontend', 'Update Personal Info of your profile'),
            'updatePassportData' => \Yii::t('frontend', 'Update Passport Info of your profile'),
            'updateEmployedData' => \Yii::t('frontend', 'Update Employed Info of your profile'),
        ];
    }

    public function getProgressbarClassByStep(string $step): string
    {
        $class = [];
        switch ($step) {
            case self::STEP_DETAILS:
                if ($this->loanDetails->hasErrors()) {
                    $class[] = 'error step active';
                    $this->activeStep = $step;
                    break;
                }
                $class[] = 'done';
                if ($this->step === $step && $this->step !== $this->activeStep) {
                    $class[] = 'active';
                    $this->activeStep = $step;
                }
                $class[] = 'step';
                break;
            case self::STEP_PERSONAL_DATA:
                $emailError = false;
                foreach ($this->clientEmail as $clientEmail) {
                    if ($clientEmail->hasErrors()) {
                        $emailError = true;
                        break;
                    }
                }
                $phoneErrors = false;
                foreach ($this->clientPhone as $clientPhone) {
                    if ($clientPhone->hasErrors()) {
                        $phoneErrors = true;
                        break;
                    }
                }
                $childError = false;
                foreach($this->children as $child) {
                    if ($child->hasErrors()) {
                        $childError = true;
                        break;
                    }
                }
                if ($this->personalData->hasErrors() || $emailError || $phoneErrors || $childError) {
                    $class[] = 'error step active';
                    $this->activeStep = $step;
                    break;
                }

                $class[] = 'done';
                if ($this->step === $step && $this->step !== $this->activeStep) {
                    $class[] = 'active';
                    $this->activeStep = $step;
                }
                $class[] = 'step';
                break;
            case self::STEP_EMPLOYED_DATA:
                foreach ($this->clientEmploymentData as $clientEmploymentData) {
                    if ($clientEmploymentData->hasErrors()) {
                        $class[] = 'error step active';
                        $this->activeStep = $step;
                        break;
                    }
                }

                $class[] = 'done';
                if ($this->step === $step && $this->step !== $this->activeStep) {
                    $class[] = 'active';
                    $this->activeStep = $step;
                }
                $class[] = 'step';
                break;
            case self::STEP_PASSPORT_DATA:
                if ($this->clientPassportData->hasErrors()) {
                    $class[] = 'error step active';
                    $this->activeStep = $step;
                    break;
                }

                $class[] = 'done';
                if ($this->step === $step && $this->step !== $this->activeStep) {
                    $class[] = 'active';
                    $this->activeStep = $step;
                }
                $class[] = 'step';
                break;

        }
        return trim(implode(' ', $class));
    }

    protected function internalForms(): array
    {
        return ['loanDetails', 'personalData', 'children', 'clientEmail', 'clientPhone', 'clientEmploymentData', 'clientPassportData'];
    }

    public function personalDataHasErrors(): bool
    {
        $childrenFormValidation = false;
        foreach ($this->children as $child) {
            if ($child->hasErrors()) {
                $childrenFormValidation = true;
                $this->addError('general', $child->getErrorSummary(true)[0]);
                break;
            }
        }
        $phoneValidation = false;
        foreach ($this->clientPhone as $phone) {
            if ($phone->hasErrors()) {
                $phoneValidation = true;
                $this->addError('general', $phone->getErrorSummary(true)[0]);
                break;
            }
        }
        return $this->personalData->hasErrors() || $childrenFormValidation || $phoneValidation;
    }
}