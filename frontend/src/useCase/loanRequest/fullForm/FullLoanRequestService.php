<?php

namespace frontend\src\useCase\loanRequest\fullForm;

use backend\src\entities\partnerCompany\entity\PartnerCompanyQuery;
use common\src\entities\client\email\entity\ClientEmail;
use common\src\entities\client\email\entity\ClientEmailQuery;
use common\src\entities\client\email\entity\ClientEmailRepository;
use common\src\entities\client\employmentData\entity\ClientEmploymentData;
use common\src\entities\client\employmentData\entity\ClientEmploymentDataQuery;
use common\src\entities\client\employmentData\entity\ClientEmploymentDataRepository;
use common\src\entities\client\entity\Client;
use common\src\entities\client\entity\ClientRepository;
use common\src\entities\client\passportData\entity\ClientPassportDataRepository;
use common\src\entities\client\phone\entity\ClientPhone;
use common\src\entities\client\phone\entity\ClientPhoneQuery;
use common\src\entities\client\phone\entity\ClientPhoneRepository;
use common\src\entities\loanApplication\clientEmail\entity\LoanApplicationClientEmail;
use common\src\entities\loanApplication\clientEmail\entity\LoanApplicationClientEmailRepository;
use common\src\entities\loanApplication\clientEmployedData\entity\LoanApplicationClientEmployedData;
use common\src\entities\loanApplication\clientEmployedData\entity\LoanApplicationClientEmployedDataRepository;
use common\src\entities\loanApplication\clientPassportData\entity\LoanApplicationClientPassportData;
use common\src\entities\loanApplication\clientPassportData\entity\LoanApplicationClientPassportDataRepository;
use common\src\entities\loanApplication\clientPhone\entity\LoanApplicationClientPhone;
use common\src\entities\loanApplication\clientPhone\entity\LoanApplicationClientPhoneRepository;
use common\src\entities\loanApplication\companies\entity\LoanApplicationCompanies;
use common\src\entities\loanApplication\companies\entity\LoanApplicationCompaniesRepository;
use common\src\entities\loanApplication\entity\LoanApplication;
use common\src\entities\loanApplication\entity\LoanApplicationRepository;
use common\src\entities\loanApplication\entity\LoanApplicationStatus;
use common\src\helpers\app\AppHelper;
use frontend\components\jobs\LoanApplicationDistributionJob;

/**
 * @property-read LoanApplicationRepository $loanApplicationRepository
 * @property-read LoanApplicationClientEmailRepository $loanApplicationClientEmailRepository
 * @property-read LoanApplicationClientPhoneRepository $loanApplicationClientPhoneRepository
 * @property-read LoanApplicationClientEmployedDataRepository $loanApplicationClientEmployedDataRepository
 * @property-read LoanApplicationClientPassportDataRepository $loanApplicationClientPassportDataRepository
 * @property-read ClientEmailRepository $clientEmailRepository
 * @property-read ClientPhoneRepository $clientPhoneRepository
 * @property-read ClientRepository $clientRepository
 * @property-read ClientEmploymentDataRepository $clientEmploymentDataRepository
 * @property-read ClientPassportDataRepository $clientPassportDataRepository
 * @property-read LoanApplicationCompaniesRepository $loanApplicationCompaniesRepository
 */
class FullLoanRequestService
{
    private LoanApplicationRepository $loanApplicationRepository;
    private LoanApplicationClientEmailRepository $loanApplicationClientEmailRepository;
    private LoanApplicationClientPhoneRepository $loanApplicationClientPhoneRepository;
    private LoanApplicationClientEmployedDataRepository $loanApplicationClientEmployedDataRepository;
    private LoanApplicationClientPassportDataRepository $loanApplicationClientPassportDataRepository;
    private ClientEmailRepository $clientEmailRepository;
    private ClientPhoneRepository $clientPhoneRepository;
    private ClientRepository $clientRepository;
    private ClientEmploymentDataRepository $clientEmploymentDataRepository;
    private ClientPassportDataRepository $clientPassportDataRepository;
    private LoanApplicationCompaniesRepository $loanApplicationCompaniesRepository;

    public function __construct(
        LoanApplicationRepository $loanApplicationRepository,
        LoanApplicationClientEmailRepository $loanApplicationClientEmailRepository,
        LoanApplicationClientPhoneRepository $loanApplicationClientPhoneRepository,
        LoanApplicationClientEmployedDataRepository $loanApplicationClientEmployedDataRepository,
        LoanApplicationClientPassportDataRepository $loanApplicationClientPassportDataRepository,
        ClientEmailRepository $clientEmailRepository,
        ClientPhoneRepository $clientPhoneRepository,
        ClientRepository $clientRepository,
        ClientEmploymentDataRepository $clientEmploymentDataRepository,
        ClientPassportDataRepository $clientPassportDataRepository,
        LoanApplicationCompaniesRepository $loanApplicationCompaniesRepository
    ) {
        $this->loanApplicationRepository = $loanApplicationRepository;
        $this->loanApplicationClientEmailRepository = $loanApplicationClientEmailRepository;
        $this->loanApplicationClientPhoneRepository = $loanApplicationClientPhoneRepository;
        $this->loanApplicationClientEmployedDataRepository = $loanApplicationClientEmployedDataRepository;
        $this->loanApplicationClientPassportDataRepository = $loanApplicationClientPassportDataRepository;
        $this->clientEmailRepository = $clientEmailRepository;
        $this->clientPhoneRepository = $clientPhoneRepository;
        $this->clientRepository = $clientRepository;
        $this->clientEmploymentDataRepository = $clientEmploymentDataRepository;
        $this->clientPassportDataRepository = $clientPassportDataRepository;
        $this->loanApplicationCompaniesRepository = $loanApplicationCompaniesRepository;
    }

    public function handle(LoanRequestFullForm $form, Client $client)
    {
        $request = new LoanApplication();
        $request->la_product_id = $form->loanDetails->productId;
        $request->la_loan_amount = $form->loanDetails->amount;
        $request->la_loan_term = $form->loanDetails->term;
        $request->la_client_id = $client->c_id;
        $request->la_client_name = $client->getFullName();
        $request->la_client_birth_date = $client->c_birth_date;
        $request->la_client_idnp = $client->c_idnp;
        $request->la_client_gender = $form->personalData->gender;
        $request->la_client_civil_status = $form->personalData->civilStatus;
        $request->la_client_social_status = $form->personalData->socialStatus;
        $request->la_client_own_property = $form->personalData->ownProperty;
        $request->setStatus(LoanApplicationStatus::STATUS_SENT);
        $request->generateUid();

        $childBirthDate = [];
        foreach ($form->children as $children) {
            $childBirthDate[] = ['dateOfBirth' => $children['dateOfBirth'] ?? ''];
        }
        $request->la_client_children = $childBirthDate;

        $transaction = \Yii::$app->db->beginTransaction();
        try {

            $request->detachBehavior('user');
            $this->loanApplicationRepository->save($request);

            if ($form->clientEmail) {
                /** @var ClientEmail $clientEmailForm */
                foreach ($form->clientEmail as $clientEmailForm) {
                    $loanApplicationClientEmail = new LoanApplicationClientEmail();
                    $loanApplicationClientEmail->lace_client_id = $client->c_id;
                    $loanApplicationClientEmail->lace_la_id = $request->la_id;
                    $loanApplicationClientEmail->lace_email = $clientEmailForm->ce_email;
                    $this->loanApplicationClientEmailRepository->save($loanApplicationClientEmail);
                }
            }

            if ($form->clientPhone) {
                /** @var ClientPhone $clientPhoneForm */
                foreach ($form->clientPhone as $clientPhoneForm) {
                    $loanApplicationClientPhone = new LoanApplicationClientPhone();
                    $loanApplicationClientPhone->lacp_client_id = $client->c_id;
                    $loanApplicationClientPhone->lacp_la_id = $request->la_id;
                    $loanApplicationClientPhone->lacp_phone = $clientPhoneForm->cp_phone;
                    $this->loanApplicationClientPhoneRepository->save($loanApplicationClientPhone);
                }
            }

            /** @var $clientEmploymentData ClientEmploymentData */
            foreach ($form->clientEmploymentData as $clientEmploymentData) {
                $loanClientEmploymentData = new LoanApplicationClientEmployedData();
                $loanClientEmploymentData->laced_client_id = $client->c_id;
                $loanClientEmploymentData->laced_la_id = $request->la_id;
                $loanClientEmploymentData->laced_type = $clientEmploymentData->ced_type;
                $loanClientEmploymentData->laced_work_phone = $clientEmploymentData->ced_work_phone;
                $loanClientEmploymentData->laced_organization_name = $clientEmploymentData->ced_organization_name;
                $loanClientEmploymentData->laced_position = $clientEmploymentData->ced_position;
                $loanClientEmploymentData->laced_seniority = $clientEmploymentData->ced_seniority;
                $loanClientEmploymentData->laced_official_wages = $clientEmploymentData->ced_official_wages;
                $loanClientEmploymentData->laced_unofficial_wages = $clientEmploymentData->ced_unofficial_wages;
                $loanClientEmploymentData->laced_revenues_documented = $clientEmploymentData->ced_revenues_documented;
                $loanClientEmploymentData->laced_revenues_non_documented = $clientEmploymentData->ced_revenues_non_documented;
                $this->loanApplicationClientEmployedDataRepository->save($loanClientEmploymentData);
            }

            $clientPassportData = new LoanApplicationClientPassportData();
            $clientPassportData->lacpd_client_id = $client->c_id;
            $clientPassportData->lacpd_la_id = $request->la_id;
            $clientPassportData->lacpd_type = $form->clientPassportData->cpd_type;
            $clientPassportData->lacpd_series = $form->clientPassportData->cpd_series;
            $clientPassportData->lacpd_number = $form->clientPassportData->cpd_number;
            $clientPassportData->lacpd_issued_by = $form->clientPassportData->cpd_issued_by;
            $clientPassportData->lacpd_issue_date = $form->clientPassportData->cpd_issue_date;
            $clientPassportData->lacpd_issue_department_code = $form->clientPassportData->cpd_issue_department_code;
            $clientPassportData->lacpd_expiration_date = $form->clientPassportData->cpd_expiration_date;
            $clientPassportData->lacpd_registered_address = $form->clientPassportData->cpd_registered_address;
            $clientPassportData->lacpd_residence_address = $form->clientPassportData->cpd_residence_address;
            $this->loanApplicationClientPassportDataRepository->save($clientPassportData);

            $companies = PartnerCompanyQuery::getListByProduct($form->loanDetails->productId);
            if (!$companies) {
                throw new \RuntimeException(\Yii::t('frontend', 'None of the companies is considering applications for this product'));
            }
            foreach ($companies as $companyId => $companyName) {
                $loanCompanyRelation = new LoanApplicationCompanies();
                $loanCompanyRelation->lac_la_id = $request->la_id;
                $loanCompanyRelation->lac_partner_company_id = $companyId;
                $this->loanApplicationCompaniesRepository->save($loanCompanyRelation);
            }

            if ($form->updatePersonalInfoData) {
                $client->c_civil_status = $form->personalData->civilStatus;
                $client->c_social_status = $form->personalData->socialStatus;
                $client->c_children = $childBirthDate;
                $client->c_own_property = $form->personalData->ownProperty;
                $client->detachBehavior('user');
                $this->clientRepository->save($client);

                ClientEmailQuery::removeByClientId($client->c_id);
                /** @var ClientEmail $clientEmail */
                foreach ($form->clientEmail as $clientEmail) {
                    $clientEmail->setIsNewRecord(true);
                    $clientEmail->ce_updated_dt = date('Y-m-d H:i:s');
                    $clientEmail->detachBehavior('user');
                    $this->clientEmailRepository->save($clientEmail);
                }

                ClientPhoneQuery::removeByClientId($client->c_id);
                /** @var ClientPhone $clientPhone */
                foreach ($form->clientPhone as $clientPhone) {
                    $clientPhone->setIsNewRecord(true);
                    $clientPhone->cp_updated_dt = date('Y-m-d H:i:s');
                    $clientPhone->detachBehavior('user');
                    $this->clientPhoneRepository->save($clientPhone);
                }
            }

            if ($form->updateEmployedData) {
                ClientEmploymentDataQuery::removeByClientId($client->c_id);
                /** @var ClientEmploymentData $employmentDataModel */
                foreach ($form->clientEmploymentData as $employmentDataModel) {
                    $employmentDataModel->setIsNewRecord(true);
                    $employmentDataModel->ced_updated_dt = date('Y-m-d H:i:s');
                    $employmentDataModel->detachBehavior('user');
                    $this->clientEmploymentDataRepository->save($employmentDataModel);
                }
            }

            if ($form->updatePassportData) {
                $form->clientPassportData->detachBehavior('user');
                $this->clientPassportDataRepository->save($form->clientPassportData);
            }

            $transaction->commit();

            $job = new LoanApplicationDistributionJob($request->la_product_id);
            /** @var $queue \yii\queue\beanstalk\Queue */
            $queue = \Yii::$app->queue_loan_application_distribution;
            $queue->push($job);

        } catch (\RuntimeException $e) {
            $transaction->rollBack();
            $form->addError('general', $e->getMessage());
            \Yii::error(AppHelper::throwableLog($e, true), 'FullLoanRequestService::handle::RuntimeException');
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $form->addError('general', \Yii::t('frontend', 'Internal Server Error'));
            \Yii::error(AppHelper::throwableLog($e, true), 'FullLoanRequestService::handle::Throwable');
        }
    }
}