<?php

namespace frontend\src\useCase\partner\restorePassword;

use kekaadrenalin\recaptcha3\ReCaptchaValidator;

class RestorePasswordForm extends \yii\base\Model
{
    public $username;
    public $partialPhoneNumber;
    public $reCaptcha;

    public function rules(): array
    {
        return [
            [['username', 'partialPhoneNumber', 'reCaptcha'], 'required'],
            [['username', 'partialPhoneNumber', 'reCaptcha'], 'trim'],
            [['username'], 'string', 'min' => 0, 'max' => 255],
//            [['username'], 'match', 'pattern' => '/^[-9]+$/'],
            [['partialPhoneNumber'], 'string', 'min' => 4, 'max' => 4],
            [['reCaptcha'], ReCaptchaValidator::class, 'acceptance_score' => 0, 'message' => \Yii::t('frontend', 'Invalid captcha')]
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'username' => \Yii::t('frontend', 'Enter Your Login'),
            'partialPhoneNumber' => \Yii::t('frontend', 'Enter last 4 numbers of your phone number')
        ];
    }
}