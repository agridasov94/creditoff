<?php

namespace frontend\src\useCase\partner\restorePassword;

use common\src\entities\users\entity\Users;
use common\src\entities\users\entity\UsersQuery;
use Yii;
use yii\helpers\ArrayHelper;
use yii\redis\Connection;
use yii\web\IdentityInterface;

class RestorePasswordConfirmForm extends \yii\base\Model
{
    public $partnerUid;
    public $password;
    public $rePassword;
    public $reCaptcha;
    public $code;

    private ?Users $_partner = null;

    public function rules(): array
    {
        return [
            [['password', 'rePassword', 'reCaptcha', 'code', 'partnerUid'], 'required'],
            [['password'], 'string', 'min' => 8],
            [['code'], 'string'],
            ['rePassword', 'compare', 'compareAttribute' => 'password'],
            [['code'], 'validateCode']
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'password' => Yii::t('frontend', 'Password'),
            'rePassword' => Yii::t('frontend', 'Repeat password'),
            'code' => Yii::t('frontend', 'Verification code'),
        ];
    }

    private function getPartner(): ?Users
    {
        if (!$this->_partner) {
            return $this->_partner = UsersQuery::findByUid($this->partnerUid);
        }
        return $this->_partner;
    }

    public function validateCode()
    {
        /** @var Connection $redis */
        $redis = Yii::$app->redis;
        $partner = $this->getPartner();
        if (!$partner) {
            $this->addError('general', Yii::t('frontend', 'Not found partner'));
            return false;
        }
        if (!$redis->exists(Users::getVerificationCodeKey($partner->uid))) {
            $this->addError('general', Yii::t('frontend', 'Verification code expired'));
            return false;
        }
        $code = $redis->get(Users::getVerificationCodeKey($partner->uid));
        if ($code !== $this->code) {
            $this->addError('activationCode', Yii::t('frontend', 'Code is not valid'));
            return false;
        }
        return true;
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login(Users $user): bool
    {
        Yii::$app->session->setName('advanced-backend');
        return Yii::$app->backendUser->login($user, 0);
    }

    /**
     * @param IdentityInterface $identity
     * @param int $duration
     * @throws \yii\base\InvalidConfigException
     */
    public function sendWsIdentityCookie(IdentityInterface $identity, int $duration = 0): void
    {
        $params = Yii::$app->params;
        $identityCookie = $params['backWsIdentityCookie'] ?? [];

        $cookie = Yii::createObject(array_merge($identityCookie, [
            'class' => 'yii\web\Cookie',
            'value' => json_encode([
                $identity->getId(),
                $identity->getAuthKey(),
                $duration,
            ], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE),
            'expire' => $duration ? time() + $duration : 0,
        ]));
        $backendConfig = ArrayHelper::merge(
            require \Yii::getAlias('@backend/config/main.php'),
            require \Yii::getAlias('@backend/config/main-local.php')
        );
        Yii::$app->request->cookieValidationKey = $backendConfig['components']['request']['cookieValidationKey'] ?? '';
        Yii::$app->getResponse()->getCookies()->add($cookie);
    }
}