<?php
namespace frontend\src\helper;

use common\src\entities\loanApplication\clientEmail\entity\LoanApplicationClientEmailQuery;
use common\src\entities\loanApplication\clientPhone\entity\LoanApplicationClientPhoneQuery;
use common\src\entities\loanApplication\entity\LoanApplication;

class LoanApplicationHelper
{
    public static function getLoanApplicationLender(LoanApplication $loanApplication, string $separator = ','): string
    {
        $loanApplications = [];
        if ($owner = $loanApplication->owner) {
            $loanApplications[] = $owner->partnerCompany->pc_name;
        } else {
            foreach ($loanApplication->loanApplicationCompanies as $loanApplicationCompany)
            {
                $loanApplications[] = $loanApplicationCompany->partnerCompany->pc_name;
            }
        }
        return implode($separator, $loanApplications);
    }

    public static function getClientEmails(LoanApplication $loanApplication, string $separator = ','): string
    {
        $loanApplicationClientEmail = LoanApplicationClientEmailQuery::findAllByClientIdLoanApplicationId($loanApplication->la_client_id, $loanApplication->la_id);
        $emails = [];
        foreach ($loanApplicationClientEmail as $clientEmail) {
            $emails[] = $clientEmail->lace_email;
        }
        return implode($separator, $emails);
    }

    public static function getClientPhones(LoanApplication $loanApplication, string $separator = ','): string
    {
        $loanApplicationClientPhones = LoanApplicationClientPhoneQuery::findAllByClientIdLoanApplicationId($loanApplication->la_client_id, $loanApplication->la_id);
        $phones = [];
        foreach ($loanApplicationClientPhones as $clientPhone) {
            $phones[] = $clientPhone->lacp_phone;
        }
        return implode($separator, $phones);
    }
}
