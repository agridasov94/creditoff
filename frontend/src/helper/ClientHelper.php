<?php

namespace frontend\src\helper;

use common\src\entities\client\entity\Client;

class ClientHelper
{
    public static function getClientPhones(Client $client, string $separator = ','): string
    {
        $clientPhones = [];
        foreach ($client->clientPhones as $clientPhone) {
            $clientPhones[] = $clientPhone->cp_phone;
        }
        return implode($separator, $clientPhones);
    }

    public static function getClientEmails(Client $client, string $separator = ','): string
    {
        $clientEmails = [];
        foreach ($client->clientEmails as $clientEmail) {
            $clientEmails[] = $clientEmail->ce_email;
        }
        return implode($separator, $clientEmails);
    }
}