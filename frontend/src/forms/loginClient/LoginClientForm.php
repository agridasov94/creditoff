<?php

namespace frontend\src\forms\loginClient;

use common\src\entities\client\entity\Client;
use common\src\entities\client\entity\ClientQuery;
use kekaadrenalin\recaptcha3\ReCaptchaValidator;

class LoginClientForm extends \yii\base\Model
{
    public $idnp;
    public $password;
    public $reCaptcha;

    private $_client;

    public function rules()
    {
        return [
            [['idnp', 'password'], 'required'],
            [['idnp', 'password'], 'string'],
            [['idnp'], 'exist', 'skipOnError' => true, 'targetClass' => Client::class, 'targetAttribute' => ['idnp' => 'c_idnp'], 'message' => \Yii::t('frontend', 'The profile does not exist with the given idnp')],
            [['password'], 'validatePassword'],
            [['reCaptcha'], ReCaptchaValidator::class, 'acceptance_score' => 0, 'message' => \Yii::t('frontend', 'Invalid captcha')]
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $client = $this->getClient();
            if (!$client || empty($client->c_password_hash) || !$client->validatePassword($this->password)) {
                $this->addError($attribute, \Yii::t('frontend', 'Incorrect idnp or password.'));
            }
        }
    }

    /**
     * @return Client|null
     */
    public function getClient()
    {
        if ($this->_client === null) {
            $this->_client = ClientQuery::findByIdnp($this->idnp);
        }

        return $this->_client;
    }

    /**
     * @return bool whether the client is logged in successfully
     */
    public function login(): bool
    {
        return \Yii::$app->user->login($this->getClient(), 0);
    }
}