<?php

namespace frontend\src\forms\login;

use common\src\entities\users\entity\Users;
use Yii;
use yii\base\Model;
use yii\web\IdentityInterface;

class SetPasswordForm extends Model
{
    public $password;
    public $rePassword;

    public function rules(): array
    {
        return [
            [['password', 'rePassword'], 'required'],
            [['password'], 'string', 'min' => 8],
            ['rePassword', 'compare', 'compareAttribute' => 'password'],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'password' => Yii::t('frontend', 'Password'),
            'rePassword' => Yii::t('frontend', 'Repeat Password')
        ];
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login(Users $user): bool
    {
        Yii::$app->session->setName('advanced-backend');
        return Yii::$app->backendUser->login($user, 0);
    }

    /**
     * @param IdentityInterface $identity
     * @param int $duration
     * @throws \yii\base\InvalidConfigException
     */
    public function sendWsIdentityCookie(IdentityInterface $identity, int $duration = 0): void
    {
        $params = Yii::$app->params;
        $identityCookie = $params['backWsIdentityCookie'] ?? [];

        $cookie = Yii::createObject(array_merge($identityCookie, [
            'class' => 'yii\web\Cookie',
            'value' => json_encode([
                $identity->getId(),
                $identity->getAuthKey(),
                $duration,
            ], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE),
            'expire' => $duration ? time() + $duration : 0,
        ]));
        Yii::$app->getResponse()->getCookies()->add($cookie);
    }
}