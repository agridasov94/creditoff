<?php

namespace frontend\src\forms\login;

use common\src\entities\users\entity\Users;
use kekaadrenalin\recaptcha3\ReCaptchaValidator;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

class LoginPartnerForm extends Model
{
    public $login;
    public $password;
    public $reCaptcha;

    private $_user;

    public function rules()
    {
        return [
            [['login', 'password'], 'required'],
            [['login', 'password'], 'string'],
            [['reCaptcha'], ReCaptchaValidator::class, 'acceptance_score' => 0, 'message' => \Yii::t('frontend', 'Invalid captcha')],
            [['login'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['login' => 'username'], 'message' => Yii::t('frontend', 'The user does not exist with the given login')],
            [['password'], 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect login or password.');
            }
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return Users|null
     */
    public function getUser()
    {
        if ($this->_user === null) {
            $this->_user = Users::findByUsername($this->login);
        }

        return $this->_user;
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login(): bool
    {
        Yii::$app->session->setName('advanced-backend');
        return Yii::$app->backendUser->login($this->getUser(), 0);
    }

    /**
     * @param IdentityInterface $identity
     * @param int $duration
     * @throws \yii\base\InvalidConfigException
     */
    public function sendWsIdentityCookie(IdentityInterface $identity, int $duration = 0): void
    {
        $params = Yii::$app->params;
        $identityCookie = $params['backWsIdentityCookie'] ?? [];

        $cookie = Yii::createObject(array_merge($identityCookie, [
            'class' => 'yii\web\Cookie',
            'value' => json_encode([
                $identity->getId(),
                $identity->getAuthKey(),
                $duration,
            ], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE),
            'expire' => $duration ? time() + $duration : 0,
        ]));
        $backendConfig = ArrayHelper::merge(
            require \Yii::getAlias('@backend/config/main.php'),
            require \Yii::getAlias('@backend/config/main-local.php')
        );
        Yii::$app->request->cookieValidationKey = $backendConfig['components']['request']['cookieValidationKey'] ?? '';
        Yii::$app->getResponse()->getCookies()->add($cookie);
    }
}