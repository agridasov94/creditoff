<?php

namespace frontend\components\jobs;

use common\src\entities\product\entity\ProductQuery;
use common\src\exceptions\NotFoundException;
use common\src\helpers\app\AppHelper;
use common\src\services\LoanApplicationDistributionService;
use yii\base\BaseObject;
use yii\db\Transaction;
use yii\queue\JobInterface;
use yii\queue\Queue;

class LoanApplicationDistributionJob extends BaseObject implements JobInterface
{
    public int $productId;

    public function __construct(int $productId, $config = [])
    {
        parent::__construct($config);
        $this->productId = $productId;
    }

    public function execute($queue)
    {
        $transaction = new Transaction(['db' => \Yii::$app->db]);
        try {
            $product = ProductQuery::getActiveById($this->productId);
            if (!$product) {
                throw new NotFoundException('Product not found by id: ' . $this->productId . ' in distribution logic process');
            }

            $service = \Yii::createObject(LoanApplicationDistributionService::class);
            $transaction->begin();
            $service->removeCurrentDistributionsAndAccess($this->productId);
            $service->handle($product);
            $transaction->commit();
        } catch (\RuntimeException $e) {
            $transaction->rollBack();
            \Yii::warning(AppHelper::throwableLog($e, true), 'LoanApplicationDistributionJob::RuntimeException');
        } catch (\Throwable $e) {
            $transaction->rollBack();
            \Yii::error(AppHelper::throwableLog($e,true), 'LoanApplicationDistributionJob::Throwable');
        }
    }
}