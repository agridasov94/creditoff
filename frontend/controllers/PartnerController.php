<?php

namespace frontend\controllers;

use backend\src\repositories\users\UserRepository;
use common\components\jobs\sms\SendSmsJob;
use common\components\sms\SmsComponent;
use common\src\entities\users\entity\Users;
use common\src\entities\users\entity\UsersQuery;
use common\src\helpers\app\SettingHelper;
use frontend\src\forms\login\LoginPartnerForm;
use frontend\src\forms\login\SetPasswordForm;
use frontend\src\useCase\partner\restorePassword\RestorePasswordConfirmForm;
use frontend\src\useCase\partner\restorePassword\RestorePasswordForm;
use Yii;
use yii\db\Transaction;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\redis\Connection;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class PartnerController extends Controller
{
    private const RESTORE_PASSWORD_SESSION_KEY = 'restore-password-partner-confirm';

    private UserRepository $userRepository;

    public function __construct($id, $module, UserRepository $userRepository, $config = [])
    {
        $this->userRepository = $userRepository;
        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'set-password', 'restore-password', 'restore-password-confirm'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actionLogin()
    {
//        Yii::$app->user->logout(true);

        if (!Yii::$app->backendUser->isGuest) {
            return $this->redirect(Yii::$app->params['backendHost']);
        }

        $this->layout = 'login';

        $model = new LoginPartnerForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $user = $model->getUser();

            if (empty($user->last_login_dt)) {
                Yii::$app->session->set('user-set-password', $user->id);
                return $this->redirect('/partner/set-password');
            }
            $model->login();
            $model->sendWsIdentityCookie($user);
            return $this->redirect(Yii::$app->params['backendHost']);
        }

        $model->password = '';

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionSetPassword()
    {
        $userId = Yii::$app->session->get('user-set-password');
        if (!Yii::$app->backendUser->isGuest) {
            return $this->redirect(Yii::$app->params['backendHost']);
        }
        if (empty($userId)) {
            throw new NotFoundHttpException('Page not found', 404);
        }
        $user = Users::findOne((int)$userId);
        if ($user === null) {
            throw new NotFoundHttpException('Page not found', 404);
        }
        if (!empty($user->last_login_dt)) {
            throw new NotFoundHttpException('Page not found', 404);
        }

        $model = new SetPasswordForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $transaction = new Transaction(['db' => Yii::$app->db]);
            $user->setPassword($model->password);

            $transaction->begin();
            try {
                $this->userRepository->save($user);
                if (!$model->login($user)) {
                    throw new \RuntimeException(Yii::t('frontend', 'Unable to authorize user.'));
                }
                $model->sendWsIdentityCookie($user);
                Yii::$app->session->remove('user-set-password');
                $transaction->commit();
                return $this->redirect(Yii::$app->params['backendHost']);
            } catch (\RuntimeException $e) {
                $transaction->rollBack();
                $model->addError('password', $e->getMessage());
            } catch (\Throwable $e) {
                $transaction->rollBack();
                $model->addError('password', Yii::t('frontend', 'Internal Server Error'));
                var_dump($e->getMessage());die;
            }
        }

        $this->layout = 'login';

        return $this->render('set-password', [
            'model' => $model,
            'username' => $user->getFullName()
        ]);
    }

    public function actionRestorePassword()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->redirect(['/cabinet/loan-application-list']);
        }

        $this->layout = 'login';

        $model = new RestorePasswordForm();
        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {

            $partner = UsersQuery::findByUsernameAndPartialPhoneNumber($model->username, $model->partialPhoneNumber);
            if ($partner) {
                /** @var $sms SmsComponent */
                $sms = \Yii::$app->sms;
                $code = $sms->generateActivationCode();

                $job = new SendSmsJob($partner->phone, \Yii::t('frontend', 'Verifiction code: {code}', [
                    'code' => $code
                ]));
                /** @var $queue \yii\queue\beanstalk\Queue */
                $queue = \Yii::$app->queue_sms_job;
                $queue->push($job);
                \Yii::$app->session->set(self::RESTORE_PASSWORD_SESSION_KEY, [
                    'uid' => $partner->uid
                ]);
                /** @var Connection $redis */
                $redis = \Yii::$app->redis;
                $redis->setex(Users::getVerificationCodeKey($partner->uid), SettingHelper::getSecondsLifetimeProfileActivationCode(), $code);
                return $this->redirect(['/partner/restore-password-confirm']);
            }

            $model->addError('general', \Yii::t('frontend', 'Not found partner by login and phone number'));
        }
        if (\Yii::$app->session->hasFlash('error')) {
            $model->addError('general', \Yii::$app->session->getFlash('error'));
        }

        return $this->render('restore_password', [
            'model' => $model
        ]);
    }

    public function actionRestorePasswordConfirm()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->redirect(['/cabinet/loan-application-list']);
        }

        $model = new RestorePasswordConfirmForm();

        $partnerData = \Yii::$app->session->get(self::RESTORE_PASSWORD_SESSION_KEY);
        if (empty($partnerData)) {
            return $this->redirect(['/partner/login']);
        }

        $partner = UsersQuery::findByUid((string)$partnerData['uid']);
        if (empty($partner)) {
            \Yii::$app->session->setFlash('error', \Yii::t('frontend', 'Not found partner by login and phone number'));
            \Yii::$app->session->remove(self::RESTORE_PASSWORD_SESSION_KEY);
            return $this->redirect(['/partner/restore-password']);
        }

        /** @var $redis Connection */
        $redis = \Yii::$app->redis;

        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            $partner->setPassword($model->password);
            $partner->save($partner);
            $redis->del(Users::getVerificationCodeKey($partner->uid));
            \Yii::$app->session->remove(self::RESTORE_PASSWORD_SESSION_KEY);
            if (!$model->login($partner)) {
                throw new \RuntimeException(Yii::t('frontend', 'Unable to authorize user.'));
            }
            $model->sendWsIdentityCookie($partner);
            \Yii::$app->session->setFlash('success', \Yii::t('frontend', 'Your password has been successfully recovered'));
            return $this->redirect(Yii::$app->params['backendHost']);
        }

        if (!$redis->exists(Users::getVerificationCodeKey($partner->uid))) {
            \Yii::$app->session->setFlash('error', \Yii::t('frontend', 'Looks like the verification code is out of date. Try again.'));
            \Yii::$app->session->remove(self::RESTORE_PASSWORD_SESSION_KEY);
            return $this->redirect(['/partner/restore-password']);
        }

        $this->layout = 'login';
        $model->partnerUid = $partner->uid;

        return $this->render('restore_password_confirm', [
            'model' => $model
        ]);
    }
}