<?php

namespace frontend\controllers;

use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii2mod\rbac\filters\AccessControl;

class AuthController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = [
            'access' => [
                'class' => AccessControl::class,
            ],
        ];

        return ArrayHelper::merge(parent::behaviors(), $behaviors);
    }

//    public function beforeAction($action)
//    {
//        if (!\Yii::$app->user->isGuest && !\Yii::$app->request->isAjax) {
////            /** @var Users $user */
////            $user = \Yii::$app->user->identity;
//
//
//
////            $limitUserConnection = SettingHelper::getLimitUserConnection() ?: \Yii::$app->params['limitUserConnections'];
////
////            if ($limitUserConnection > 0) {
////                $countConnections = UserConnection::find()->where(['uc_user_id' => \Yii::$app->user->id])->count();
////                if ($countConnections >= $limitUserConnection && 'site/error' != \Yii::$app->controller->action->uniqueId) {
////                    throw new ForbiddenHttpException('Denied Access: You have too many connections (' . $countConnections . '). Close the old browser tabs and try again!');
////                }
////            }
//        }
//
//        return parent::beforeAction($action);
//    }
}