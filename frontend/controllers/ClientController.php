<?php

namespace frontend\controllers;

use common\components\jobs\sms\SendSmsJob;
use common\components\sms\SmsComponent;
use common\src\entities\client\entity\Client;
use common\src\entities\client\entity\ClientQuery;
use common\src\entities\client\entity\ClientRepository;
use common\src\helpers\app\SettingHelper;
use frontend\src\forms\loginClient\LoginClientForm;
use frontend\src\useCase\client\restorePassword\RestorePasswordConfirmForm;
use frontend\src\useCase\client\restorePassword\RestorePasswordForm;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\redis\Connection;

/**
 * @property-read ClientRepository $clientRepository
 */
class ClientController extends AuthController
{
    private const RESTORE_PASSWORD_SESSION_KEY = 'restore-password-confirm';

    private ClientRepository $clientRepository;

    public function __construct($id, $module, ClientRepository $clientRepository, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->clientRepository = $clientRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'restore-password', 'restore-password-confirm'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout'],
                        'roles' => ['@'],
                    ],
                ],
            ],
//            'verbs' => [
//                'class' => VerbFilter::class,
//                'actions' => [
//                    'logout' => ['post'],
//                ],
//            ],
        ];
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->redirect(['cabinet/loan-application-list']);
        }

        $this->layout = 'login';

        $model = new LoginClientForm();
        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            $model->login();
            return $this->redirect(Url::to(['/cabinet/loan-application-list']));
        }

        if ($model->getErrors('reCaptcha')) {
            header('Location: /client/login');
            exit;
        }

        $model->password = '';

        return $this->render('login', [
            'loginForm' => $model
        ]);
    }

    public function actionRestorePassword()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->redirect(['/cabinet/loan-application-list']);
        }

        $this->layout = 'login';

        $model = new RestorePasswordForm();
        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {

            $client = ClientQuery::findByIdnpAndPartialPhoneNumber($model->idnp, $model->partialPhoneNumber);
            if ($client) {
                /** @var $sms SmsComponent */
                $sms = \Yii::$app->sms;
                $code = $sms->generateActivationCode();

                $job = new SendSmsJob($client['cp_phone'], \Yii::t('frontend', 'Verifiction code: {code}', [
                    'code' => $code
                ]));
                /** @var $queue \yii\queue\beanstalk\Queue */
                $queue = \Yii::$app->queue_sms_job;
                $queue->push($job);

                \Yii::$app->session->set(self::RESTORE_PASSWORD_SESSION_KEY, [
                    'uid' => $client['c_uid']
                ]);
                /** @var Connection $redis */
                $redis = \Yii::$app->redis;
                $redis->setex(Client::getActivationCodeKey($client['c_uid']), SettingHelper::getSecondsLifetimeProfileActivationCode(), $code);
                return $this->redirect(['/client/restore-password-confirm']);
            }

            $model->addError('general', \Yii::t('frontend', 'Not found client by idnp and phone number'));
        }

        if ($model->getErrors('reCaptcha')) {
            header('Location: /client/restore-password');
            exit;
        }

        return $this->render('restore_password', [
            'model' => $model
        ]);
    }

    public function actionRestorePasswordConfirm()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->redirect(['/cabinet/loan-application-list']);
        }

        $model = new RestorePasswordConfirmForm();

        $clientData = \Yii::$app->session->get(self::RESTORE_PASSWORD_SESSION_KEY);
        if (empty($clientData)) {
            return $this->redirect(['/client/login']);
        }

        $client = ClientQuery::findByUid((string)$clientData['uid']);
        if (empty($client)) {
            \Yii::$app->session->setFlash('error', \Yii::t('frontend', 'Not found client by idnp and phone number'));
            \Yii::$app->session->remove(self::RESTORE_PASSWORD_SESSION_KEY);
            return $this->redirect(['/client/restore-password']);
        }

        /** @var $redis Connection */
        $redis = \Yii::$app->redis;

        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            $client->setPassword($model->password);
            $client->setActivated();
            $this->clientRepository->save($client);
            $redis->del(Client::getActivationCodeKey($client->c_uid));
            \Yii::$app->session->remove(self::RESTORE_PASSWORD_SESSION_KEY);
            \Yii::$app->user->login($client);
            \Yii::$app->session->setFlash('success', \Yii::t('frontend', 'Your password has been successfully recovered'));
            return $this->redirect(Url::to(['/cabinet/loan-application-list']));
        }

        if (!$redis->exists(Client::getActivationCodeKey($client->c_uid))) {
            \Yii::$app->session->setFlash('error', \Yii::t('frontend', 'Looks like the verification code is out of date. Try again.'));
            \Yii::$app->session->remove(self::RESTORE_PASSWORD_SESSION_KEY);
            return $this->redirect(['/client/restore-password']);
        }

        if ($model->getErrors('reCaptcha')) {
            header('Location: /client/restore-password-confirm');
            exit;
        }

        $this->layout = 'login';
        $model->clientUid = $client->c_uid;

        return $this->render('restore_password_confirm', [
            'model' => $model
        ]);
    }

    public function actionLogout()
    {
        \Yii::$app->user->logout();
        return $this->goHome();
    }
}