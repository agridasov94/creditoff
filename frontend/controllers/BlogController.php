<?php

namespace frontend\controllers;

use common\src\entities\blog\categories\entity\BlogCategories;
use common\src\entities\blog\categories\entity\BlogCategoriesQuery;
use common\src\entities\blog\posts\entity\BlogPosts;
use common\src\entities\blog\posts\entity\BlogPostsQuery;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class BlogController extends Controller
{
    public function actionIndex()
    {
        $this->view->params['breadcrumbs'][] = \Yii::t('frontend', 'Blog');

        $query = BlogPosts::find();
        $query->andWhere(['bp_lang_id' => \Yii::$app->language, 'bp_active' => true]);
        $query->orderBy(['bp_id' => SORT_DESC]);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'defaultPageSize' => 1]);
        $models = $query->offset($pages->offset)->limit($pages->limit)->all();

        $categories = BlogCategoriesQuery::findByLangId(\Yii::$app->language);

        $otherPosts = BlogPostsQuery::getRandomOtherPostsExcluding(null, \Yii::$app->language);

        return $this->render('blog', [
            'models' => $models,
            'pages' => $pages,
            'categories' => $categories,
            'otherPosts' => $otherPosts
        ]);
    }

    public function actionCategory(string $category)
    {
        $this->view->params['breadcrumbs'][] = [
            'url' => Url::to(['/blog']),
            'label' => \Yii::t('frontend', 'Blog')
        ];

        $categoryModel = BlogCategoriesQuery::findByLangAndUrl(\Yii::$app->language, $category);
        if (!$categoryModel) {
            throw new NotFoundHttpException('Page not found');
        }

        $query = BlogPosts::find();
        $query->andWhere(['bp_lang_id' => \Yii::$app->language, 'bp_active' => true]);
        $query->orderBy(['bp_id' => SORT_DESC]);
        $query->innerJoin(BlogCategories::tableName(), 'bp_category_id = bc_id and bc_url = :url and bc_lang_id = :lang', [
            'url' => $category,
            'lang' => \Yii::$app->language
        ]);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'defaultPageSize' => 1]);
        $models = $query->offset($pages->offset)->limit($pages->limit)->all();

        $this->view->params['breadcrumbs'][] = $categoryModel->bc_name;

        $categories = BlogCategoriesQuery::findByLangId(\Yii::$app->language);

        $otherPosts = BlogPostsQuery::getRandomOtherPostsExcluding(null, \Yii::$app->language);

        return $this->render('blog', [
            'models' => $models,
            'pages' => $pages,
            'categories' => $categories,
            'otherPosts' => $otherPosts
        ]);
    }

    public function actionPost(string $post)
    {
        $this->view->params['breadcrumbs'][] = [
            'url' => Url::to(['/blog']),
            'label' => \Yii::t('frontend', 'Blog')
        ];

        $blogPost = BlogPostsQuery::findByUrlAndLang($post, \Yii::$app->language);
        if (!$blogPost) {
            throw new NotFoundHttpException(\Yii::t('frontend', 'Page not found'));
        }

        $this->view->params['breadcrumbs'][] = [
            'url' => Url::to(['/blog/category/' . $blogPost->category->bc_url]),
            'label' => $blogPost->category->bc_name
        ];
        $this->view->params['breadcrumbs'][] = $blogPost->bp_title;

        $categories = BlogCategoriesQuery::findByLangId(\Yii::$app->language);

        $otherPosts = BlogPostsQuery::getRandomOtherPostsExcluding($blogPost->bp_id, \Yii::$app->language);

        return $this->render('post', [
            'blogPost' => $blogPost,
            'otherPosts' => $otherPosts,
            'categories' => $categories
        ]);
    }
}