<?php

use yii\console\controllers\MigrateController;

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'console\controllers',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'controllerMap' => [
        'fixture' => [
            'class' => 'yii\console\controllers\FixtureController',
            'namespace' => 'common\fixtures',
        ],
        'migrate' => [
            'class' => MigrateController::class,
            'migrationNamespaces' => [
                'lajax\translatemanager\migrations\namespaced',
            ],
            'migrationPath' => ['@console/migrations', '@vendor/yiisoft/yii2/log/migrations']
        ],
    ],
    'components' => [
        'log' => [
            'targets' => [
                'db-error' => [
                    'class' => \yii\log\DbTarget::class,
                    'levels' => ['error', 'warning'],
                    'logVars' => [],
                ],
                'db-info' => [
                    'class' => \yii\log\DbTarget::class,
                    'levels' => ['info'],
                    'logVars' => [],
                    'categories' => ['info\*', 'log\*']
                ],
            ],
        ],
        'urlManager' => [
            'scriptUrl' => '/',
            'hostInfo' => ''
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'cache' => 'cache',
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => \yii\i18n\DbMessageSource::class,
                    'db' => 'db',
                    'sourceLanguage' => 'xx-XX', // Developer language
                    'sourceMessageTable' => '{{%language_source}}',
                    'messageTable' => '{{%language_translate}}',
                    'cachingDuration' => 86400,
                    'enableCaching' => false,
                ],
            ],
        ],
    ],
    'modules' => [
        'rbac' => [
            'class' => \yii2mod\rbac\ConsoleModule::class
        ]
    ],
    'params' => $params,
];
