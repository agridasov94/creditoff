<?php

use yii\db\Migration;

/**
 * Class m220630_215814_reset_user_online_status
 */
class m220630_215814_reset_user_online_status extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        \common\src\entities\users\entity\Users::updateAll(['is_online' => 0], ['is_online' => 1]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m220630_215814_reset_user_online_status cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220630_215814_reset_user_online_status cannot be reverted.\n";

        return false;
    }
    */
}
