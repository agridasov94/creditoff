<?php

use yii\db\Migration;

/**
 * Class m220507_151120_add_column_status_client_table
 */
class m220507_151120_add_column_status_client_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%client}}', 'c_profile_status', $this->tinyInteger()->after('c_is_blocked'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%client}}', 'c_profile_status');
    }
}
