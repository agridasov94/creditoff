<?php

use common\src\entities\users\entity\Users;
use yii\db\Migration;

/**
 * Class m220420_095201_alter_table_users
 */
class m220420_095201_alter_table_users extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%users}}', 'created_at', $this->integer());
        $this->alterColumn('{{%users}}', 'updated_at', $this->integer());

        Users::updateAll(['created_at' => null, 'updated_at' => null], ['IS NOT', 'id', null]);

        $this->alterColumn('{{%users}}', 'created_at', $this->dateTime());
        $this->alterColumn('{{%users}}', 'updated_at', $this->dateTime());
        $this->renameColumn('{{%users}}', 'created_at', 'created_dt');
        $this->renameColumn('{{%users}}', 'updated_at', 'updated_dt');

        $this->addColumn('{{%users}}', 'last_login_dt', $this->dateTime());
        $this->addColumn('{{%users}}', 'first_name', $this->string(50)->after('username'));
        $this->addColumn('{{%users}}', 'last_name', $this->string(50)->after('first_name'));

        $this->addColumn('{{%users}}', 'partner_company_id', $this->integer()->after('status'));
        $this->addColumn('{{%users}}', 'partner_company_position_id', $this->integer()->after('partner_company_id'));
        $this->addColumn('{{%users}}', 'phone', $this->string(50)->notNull()->unique()->after('email'));
        $this->alterColumn('{{%users}}', 'email', $this->string()->notNull()->unique());

        $this->addColumn('{{%users}}', 'created_user_id', $this->integer());
        $this->addColumn('{{%users}}', 'updated_user_id', $this->integer());

        $this->addColumn('{{%users}}', 'is_online', $this->tinyInteger(1)->after('status')->defaultValue(0));

        $this->addForeignKey('FK-users-partner_company_id', '{{%users}}', 'partner_company_id', '{{%partner_company}}', 'pc_id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('FK-users-partner_company_position_id', '{{%users}}', 'partner_company_position_id', '{{%partner_company_position}}', 'pcp_id', 'SET NULL', 'CASCADE');

        $this->alterColumn('{{%users}}', 'auth_key', $this->string(32));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
