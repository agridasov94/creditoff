<?php

use yii\db\Migration;

/**
 * Class m220726_214410_alter_column_in_tbl_loan_application_distribution
 */
class m220726_214410_alter_column_in_tbl_loan_application_distribution extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%loan_application_distribution}}', 'lad_queue_depth', $this->decimal(18, 9)->unsigned());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('{{%loan_application_distribution}}', 'lad_queue_depth', $this->decimal(4, 2)->unsigned());
    }
}
