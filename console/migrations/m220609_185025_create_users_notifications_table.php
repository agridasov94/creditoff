<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%users_notifications}}`.
 */
class m220609_185025_create_users_notifications_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%users_notifications}}', [
            'un_id'         => $this->primaryKey(),
            'un_unique_id'  => $this->string(40),
            'un_user_id'    => $this->integer()->notNull(),
            'un_type_id'    => $this->smallInteger()->notNull(),
            'un_title'      => $this->string(100),
            'un_message'    => $this->text(),
            'un_new'        => $this->boolean()->defaultValue(true),
            'un_deleted'    => $this->boolean()->defaultValue(false),
            'un_popup'      => $this->boolean()->defaultValue(false),
            'un_popup_show' => $this->boolean()->defaultValue(false),
            'un_read_dt'    => $this->dateTime(),
            'un_created_dt' => $this->dateTime(),
        ], $tableOptions);
        $this->addForeignKey('FK-users_notifications-un_user_id', '{{%users_notifications}}', ['un_user_id'], '{{%users}}', ['id'], 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%users_notifications}}');
    }
}
