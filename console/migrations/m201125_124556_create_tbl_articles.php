<?php

use yii\db\Migration;

/**
 * Class m201125_124556_create_tbl_articles
 */
class m201125_124556_create_tbl_articles extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('articles', [
            'id' => $this->primaryKey(),
            'title'=> $this->string(200),
            'text'=> $this->string(2000),
            'author_id'=> $this->integer(),
            'alias'=> $this->string(200),
            'data'=> $this->dateTime(),
            'likes'=> $this->integer(),
            'hits'=> $this->integer(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('articles');
    }

}
