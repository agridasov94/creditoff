<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%settings}}`.
 */
class m220522_142421_create_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%settings}}', [
            's_id' => $this->primaryKey(),
            's_key' => $this->string(255)->unique()->notNull(),
            's_name' => $this->string(255),
            's_description' => $this->string(255),
            's_type' => $this->string(10)->notNull(),
            's_value' => $this->string(255)->notNull(),
            's_updated_dt' => $this->dateTime(),
            's_updated_user_id' => $this->integer(),
        ]);
        $this->addForeignKey('FK-setting-s_updated_user_id', '{{%settings}}', ['s_updated_user_id'], '{{%users}}', ['id'], 'SET NULL', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%settings}}');
    }
}
