<?php

use yii\db\Migration;

/**
 * Class m220630_191537_alter_column_queue_depth
 */
class m220630_191537_alter_column_queue_depth extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%loan_application_distribution}}', 'lad_queue_depth', $this->decimal(4, 2)->unsigned());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('{{%loan_application_distribution}}', 'lad_queue_depth', $this->integer()->unsigned());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220630_191537_alter_column_queue_depth cannot be reverted.\n";

        return false;
    }
    */
}
