<?php

use common\src\entities\product\entity\Product;
use common\src\helpers\app\SettingHelper;
use yii\db\Migration;

/**
 * Class m220616_172228_add_aolumn_to_table_product
 */
class m220616_172228_add_aolumn_to_table_product extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%product}}', 'p_settings', $this->json());
        $products = Product::find()->all();

        foreach ($products as $product) {
            $product->p_settings = json_encode(['step_in_queue_variables' => SettingHelper::getStepInQueueVariables()]);
            $product->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%product}}', 'p_settings');
    }
}
