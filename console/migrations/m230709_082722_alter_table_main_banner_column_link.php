<?php

use yii\db\Migration;

/**
 * Class m230709_082722_alter_table_main_banner_column_link
 */
class m230709_082722_alter_table_main_banner_column_link extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%main_banner}}', 'mb_link', $this->string());
        $this->alterColumn('{{%main_banner}}', 'mb_link_text', $this->string());
        $this->dropColumn('{{%main_banner}}', 'mb_file_path');
        $this->addColumn('{{%main_banner}}', 'mb_file_name', $this->string()->after('mb_description'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('{{%main_banner}}', 'mb_link', $this->string()->notNull());
        $this->alterColumn('{{%main_banner}}', 'mb_link_text', $this->string()->notNull());
        $this->dropColumn('{{%main_banner}}', 'mb_file_name');
        $this->addColumn('{{%main_banner}}', 'mb_file_path', $this->string()->after('mb_description'));
    }
}
