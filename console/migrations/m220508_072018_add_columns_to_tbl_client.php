<?php

use yii\db\Migration;

/**
 * Class m220508_072018_add_columns_to_tbl_client
 */
class m220508_072018_add_columns_to_tbl_client extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%client}}', 'c_password_hash', $this->string()->after('c_profile_status'));
        $this->addColumn('{{%client}}', 'c_uid', $this->string(15)->unique()->after('c_id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%client}}', 'c_password_hash');
        $this->dropColumn('{{%client}}', 'c_uid');
    }
}
