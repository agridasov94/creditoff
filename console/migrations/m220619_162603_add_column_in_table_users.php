<?php

use yii\db\Migration;

/**
 * Class m220619_162603_add_column_in_table_users
 */
class m220619_162603_add_column_in_table_users extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%users}}', 'uid', $this->string(15));
        foreach (\common\src\entities\users\entity\Users::find()->all() as $user) {
            $user->generateUid();
            $user->save();
        }
        $this->alterColumn('{{%users}}', 'uid', $this->string(15)->unique()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%users}}', 'uid');
    }
}
