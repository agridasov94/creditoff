<?php

use yii\db\Migration;

/**
 * Class m220524_204147_add_column_to_table_loan_application_access
 */
class m220524_204147_add_column_to_table_loan_application_access extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%loan_application_access}}', 'laa_reason', $this->string(500)->after('laa_status'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%loan_application_access}}', 'laa_reason');
    }
}
