<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%users_product_access}}`.
 */
class m220421_132316_create_users_product_access_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%users_product_access}}', [
            'upa_user_id' => $this->integer()->notNull(),
            'upa_product_id' => $this->integer()->notNull(),
            'upa_created_dt' => $this->dateTime(),
            'upa_updated_dt' => $this->dateTime(),
            'upa_created_user_id' => $this->integer(),
            'upa_updated_user_id' => $this->integer()
        ], $tableOptions);

        $this->addPrimaryKey('PK-users_product_access', '{{%users_product_access}}', ['upa_user_id', 'upa_product_id']);
        $this->addForeignKey('FK-users_product_access-upa_user_id', '{{%users_product_access}}', 'upa_user_id', '{{%users}}', 'id', 'NO ACTION', 'CASCADE');
        $this->addForeignKey('FK-users_product_access-upa_product_id', '{{%users_product_access}}', 'upa_product_id', '{{%product}}', 'p_id', 'NO ACTION', 'CASCADE');
        $this->addForeignKey('FK-users_product_access-upa_created_user_id', '{{%users_product_access}}', 'upa_created_user_id', '{{%users}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('FK-users_product_access-upa_updated_user_id', '{{%users_product_access}}', 'upa_updated_user_id', '{{%users}}', 'id', 'SET NULL', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%users_product_access}}');
    }
}
