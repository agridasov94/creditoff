<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%product}}`.
 */
class m220420_081525_create_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%product}}', [
            'p_id' => $this->primaryKey(),
            'p_name' => $this->string(100),
            'p_is_active' => $this->tinyInteger(1),
            'p_min_credit_amount' => $this->integer(),
            'p_max_credit_amount' => $this->integer(),
            'p_min_credit_term' => $this->smallInteger(),
            'p_max_credit_term' => $this->smallInteger(),
            'p_min_request_price' => $this->integer(),
            'p_created_user_id' => $this->integer(),
            'p_updated_user_id' => $this->integer(),
            'p_created_dt' => $this->dateTime(),
            'p_updated_dt' => $this->dateTime()
        ]);

        $this->addForeignKey('FK-product-p_created_user_id', '{{%product}}', 'p_created_user_id', '{{%users}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('FK-product-p_updated_user_id', '{{%product}}', 'p_updated_user_id', '{{%users}}', 'id', 'SET NULL', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%product}}');
    }
}
