<?php

use yii\db\Migration;

/**
 * Class m220422_101922_alter_table_users_product_access
 */
class m220422_101922_alter_table_users_product_access extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%users_product_access}}', 'upa_request_price', $this->integer()->notNull()->unsigned()->after('upa_product_id'));
        $this->addColumn('{{%users_product_access}}', 'upa_status', $this->tinyInteger()->notNull()->after('upa_request_price'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%users_product_access}}', 'upa_request_price');
        $this->dropColumn('{{%users_product_access}}', 'upa_status');
    }
}
