<?php

use yii\db\Migration;

/**
 * Class m220710_151347_alter_client_tables_columns
 */
class m220710_151347_alter_client_tables_columns extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%client}}', 'c_idnp', $this->string(30)->notNull());
        $this->alterColumn('{{%client_employment_data}}', 'ced_seniority', $this->string(30));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('{{%client}}', 'c_idnp', $this->string(13)->notNull());
        $this->alterColumn('{{%client_employment_data}}', 'ced_seniority', $this->string(10));
    }
}
