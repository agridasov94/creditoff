<?php

use common\src\entities\users\entity\Users;
use console\migrations\RbacMigrationService;
use yii\db\Migration;

/**
 * Class m220620_060944_add_rbac_permission
 */
class m220620_060944_add_rbac_permission extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        (new RbacMigrationService())->up([
            '/client/switch'
        ], [Users::ROLE_ROOT, Users::ROLE_ADMIN]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        (new RbacMigrationService())->down([
            '/client/switch'
        ], [Users::ROLE_ROOT, Users::ROLE_ADMIN]);
    }
}
