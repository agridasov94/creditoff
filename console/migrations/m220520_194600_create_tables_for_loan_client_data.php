<?php

use yii\db\Migration;

/**
 * Class m220520_194600_create_tables_for_loan_client_data
 */
class m220520_194600_create_tables_for_loan_client_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%loan_application}}', 'la_client_email');
        $this->dropColumn('{{%loan_application}}', 'la_client_phone');

        $this->addColumn('{{%loan_application}}', 'la_client_gender', $this->tinyInteger(1)->after('la_client_idnp'));
        $this->addColumn('{{%loan_application}}', 'la_client_civil_status', $this->tinyInteger(1)->after('la_client_gender'));
        $this->addColumn('{{%loan_application}}', 'la_client_social_status', $this->tinyInteger(1)->after('la_client_civil_status'));
        $this->addColumn('{{%loan_application}}', 'la_client_own_property', $this->tinyInteger(1)->after('la_client_social_status'));
        $this->addColumn('{{%loan_application}}', 'la_client_children', $this->json()->after('la_client_own_property'));

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%loan_application_client_email}}', [
            'lace_id' => $this->primaryKey(),
            'lace_client_id' => $this->integer(),
            'lace_la_id' => $this->integer()->notNull(),
            'lace_email' => $this->string(100)->notNull()->unique(),
            'lace_created_dt' => $this->dateTime()
        ], $tableOptions);

        $this->addForeignKey('FK-loan_application_client_email-lace_client_id', '{{%loan_application_client_email}}', 'lace_client_id', '{{%client}}', 'c_id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('FK-loan_application_client_email-lace_la_id', '{{%loan_application_client_email}}', 'lace_la_id', '{{%loan_application}}', 'la_id', 'CASCADE', 'CASCADE');


        $this->createTable('{{%loan_application_client_phone}}', [
            'lacp_id' => $this->primaryKey(),
            'lacp_client_id' => $this->integer(),
            'lacp_la_id' => $this->integer()->notNull(),
            'lacp_phone' => $this->string(30)->notNull()->unique(),
            'lacp_created_dt' => $this->dateTime()
        ], $tableOptions);
        $this->addForeignKey('FK-loan_application_client_phone-lacp_client_id', '{{%loan_application_client_phone}}', 'lacp_client_id', '{{%client}}', 'c_id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('FK-loan_application_client_phone-lacp_la_id', '{{%loan_application_client_phone}}', 'lacp_la_id', '{{%loan_application}}', 'la_id', 'CASCADE', 'CASCADE');

        $this->createTable('{{%loan_application_client_employed_data}}', [
            'laced_id' => $this->primaryKey(),
            'laced_client_id' => $this->integer(),
            'laced_la_id' => $this->integer(),
            'laced_type' => $this->tinyInteger(1),
            'laced_work_phone' => $this->string(30),
            'laced_organization_name' => $this->string(100),
            'laced_position' => $this->string(100),
            'laced_seniority' => $this->string(10),
            'laced_official_wages' => $this->string(50),
            'laced_unofficial_wages' => $this->string(50),
            'laced_revenues_documented' => $this->string(),
            'laced_revenues_non_documented' => $this->string(),
            'laced_created_dt' => $this->dateTime(),
        ], $tableOptions);
        $this->addForeignKey('FK-loan_application_client_employed_data-laced_client_id', '{{%loan_application_client_employed_data}}', 'laced_client_id', '{{%client}}', 'c_id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('FK-loan_application_client_employed_data-laced_la_id', '{{%loan_application_client_employed_data}}', 'laced_la_id', '{{%loan_application}}', 'la_id', 'CASCADE', 'CASCADE');


        $this->createTable('{{%loan_application_client_passport_data}}', [
            'lacpd_id' => $this->primaryKey(),
            'lacpd_client_id' => $this->integer(),
            'lacpd_la_id' => $this->integer(),
            'lacpd_type' => $this->tinyInteger(1),
            'lacpd_series' => $this->string(100),
            'lacpd_number' => $this->string(100),
            'lacpd_issued_by' => $this->string(50),
            'lacpd_issue_date' => $this->date(),
            'lacpd_issue_department_code' => $this->string(50),
            'lacpd_expiration_date' => $this->date(),
            'lacpd_registered_address' => $this->string(100),
            'lacpd_residence_address' => $this->string(100),
            'lacpd_created_dt' => $this->dateTime(),
        ], $tableOptions);
        $this->createIndex('UNQ-loan_application_client_passport_data', '{{%loan_application_client_passport_data}}', ['lacpd_client_id', 'lacpd_la_id'], true);
        $this->addForeignKey('FK-loan_application_client_passport_data-lacpd_client_id', '{{%loan_application_client_passport_data}}', 'lacpd_client_id', '{{%client}}', 'c_id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('FK-loan_application_client_passport_data-lacpd_la_id', '{{%loan_application_client_passport_data}}', 'lacpd_la_id', '{{%loan_application}}', 'la_id', 'CASCADE', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%loan_application_client_passport_data}}');
        $this->dropTable('{{%loan_application_client_employed_data}}');
        $this->dropTable('{{%loan_application_client_phone}}');
        $this->dropTable('{{%loan_application_client_email}}');

        $this->dropColumn('{{%loan_application}}', 'la_client_gender');
        $this->dropColumn('{{%loan_application}}', 'la_client_civil_status');
        $this->dropColumn('{{%loan_application}}', 'la_client_social_status');
        $this->dropColumn('{{%loan_application}}', 'la_client_own_property');
        $this->dropColumn('{{%loan_application}}', 'la_client_children');

        $this->addColumn('{{%loan_application}}', 'la_client_email', $this->string(100));
        $this->addColumn('{{%loan_application}}', 'la_client_phone', $this->string(50));
    }
}
