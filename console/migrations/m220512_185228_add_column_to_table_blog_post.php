<?php

use yii\db\Migration;

/**
 * Class m220512_185228_add_column_to_table_blog_post
 */
class m220512_185228_add_column_to_table_blog_post extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->truncateTable('{{%blog_posts}}');
        $this->addColumn('{{%blog_posts}}', 'bp_url', $this->string(255)->notNull()->after('bp_title'));
        $this->createIndex('UNQ-blog_posts-bp_url-bp_lang_id', '{{%blog_posts}}', ['bp_lang_id', 'bp_url'], true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%blog_posts}}', 'bp_url');
    }
}
