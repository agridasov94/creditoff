<?php

use yii\db\Migration;

/**
 * Class m220612_132901_add_new_column_for_product
 */
class m220612_132901_add_new_column_for_product extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%product}}', 'p_sort_order', $this->smallInteger());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%product}}', 'p_sort_order');
    }
}
