<?php

use yii\db\Migration;

/**
 * Class m220313_082601_alter_tbl_blog_categories_add_lang_id
 */
class m220313_082601_alter_tbl_blog_categories_add_lang_id extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->db->createCommand('alter table blog_posts modify bp_lang_id varchar(5) CHARACTER SET utf8 collate utf8_unicode_ci not null')->execute();
        $this->addForeignKey('FK-blog_posts-bp_lang_id', '{{%blog_posts}}', 'bp_lang_id', 'language', 'language_id', 'CASCADE', 'CASCADE');

        $this->addColumn('{{%blog_categories}}', 'bc_lang_id', $this->string(5)->notNull()->after('bc_id'));
        $this->db->createCommand('alter table blog_categories modify bc_lang_id varchar(5) CHARACTER SET utf8 collate utf8_unicode_ci not null')->execute();
        $this->addForeignKey('FK-blog_categories-bc_lang_id', '{{%blog_categories}}', 'bc_lang_id', 'language', 'language_id', 'CASCADE', 'CASCADE');

        $this->dropForeignKey('FK-blog_posts-bp_category_id', '{{%blog_posts}}');
        $this->alterColumn('{{%blog_categories}}', 'bc_id', $this->integer());
        $this->dropPrimaryKey('PRIMARY', '{{%blog_categories}}');
        $this->addPrimaryKey('PRIMARY', '{{%blog_categories}}', ['bc_id', 'bc_lang_id']);

        $this->alterColumn('{{%blog_posts}}', 'bp_id', $this->integer());
        $this->dropPrimaryKey('PRIMARY', '{{%blog_posts}}');
        $this->addPrimaryKey('PRIMARY', '{{%blog_posts}}', ['bp_id', 'bp_lang_id']);
        $this->alterColumn('{{%blog_posts}}', 'bp_id', $this->integer() . ' auto_increment');

        $this->alterColumn('{{%blog_categories}}', 'bc_id', $this->integer() . ' auto_increment');
        $this->addForeignKey('FK-blog_posts-bp_category_id', '{{%blog_posts}}', ['bp_category_id', 'bp_lang_id'], '{{%blog_categories}}', ['bc_id', 'bc_lang_id'], 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK-blog_posts-bp_lang_id', '{{%blog_posts}}');
        $this->dropForeignKey('FK-blog_posts-bp_category_id', '{{%blog_posts}}');
        $this->dropForeignKey('FK-blog_categories-bc_lang_id', '{{%blog_categories}}');
        $this->alterColumn('{{%blog_categories}}', 'bc_id', $this->integer());

        $this->dropPrimaryKey('PRIMARY', '{{%blog_categories}}');
        $this->alterColumn('{{%blog_categories}}', 'bc_id', $this->primaryKey());
        $this->dropColumn('{{%blog_categories}}', 'bc_lang_id');
        $this->addForeignKey('FK-blog_posts-bp_category_id', '{{%blog_posts}}', 'bp_category_id', '{{%blog_categories}}', 'bc_id', 'CASCADE', 'CASCADE');

        $this->alterColumn('{{%blog_posts}}', 'bp_id', $this->integer());
        $this->dropPrimaryKey('PRIMARY', '{{%blog_posts}}');
        $this->alterColumn('{{%blog_posts}}', 'bp_id', $this->primaryKey());
    }
}
