<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%partner_company_amount_history}}`.
 */
class m220419_215237_create_partner_company_amount_history_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%partner_company_amount_history}}', [
            'pcah_id' => $this->primaryKey(),
            'pcah_partner_company_id' => $this->integer()->notNull(),
            'pcah_action' => $this->tinyInteger(),
            'pcah_action_dt' => $this->dateTime(),
            'pcah_amount_value' => $this->integer(),
            'pcah_bonus_value' => $this->integer(),
            'pcah_amount_before' => $this->integer(),
            'pcah_amount_after' => $this->integer(),
            'pcah_bonus_before' => $this->integer(),
            'pcah_bonus_after' => $this->integer(),
            'pcah_created_user_id' => $this->integer()
        ]);

        $this->addForeignKey('Fk-partner_company_amount_history-pcah_partner_company_id', '{{%partner_company_amount_history}}', 'pcah_partner_company_id', '{{%partner_company}}', 'pc_id', 'NO ACTION', 'CASCADE');
        $this->addForeignKey('Fk-partner_company_amount_history-pcah_created_user_id', '{{%partner_company_amount_history}}', 'pcah_created_user_id', '{{%users}}', 'id', 'NO ACTION', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%partner_company_amount_history}}');
    }
}
