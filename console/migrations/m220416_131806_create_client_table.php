<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%client}}`.
 */
class m220416_131806_create_client_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%client}}', [
            'c_id' => $this->primaryKey(),
            'c_name' => $this->string(70)->notNull(),
            'c_last_name' => $this->string(70)->notNull(),
            'c_birth_date' => $this->date()->notNull(),
            'c_idnp' => $this->string(13)->notNull(),
            'c_gender' => $this->tinyInteger(1),
            'c_civil_status' => $this->tinyInteger(1),
            'c_social_status' => $this->tinyInteger(1),
            'c_own_property' => $this->tinyInteger(1),
            'c_children' => $this->json(),
            'c_created_user_id' => $this->integer(),
            'c_updated_user_id' => $this->integer(),
            'c_created_dt' => $this->dateTime(),
            'c_updated_dt' => $this->dateTime(),
        ], $tableOptions);

        $this->addForeignKey('FK-client-c_created_user_id', '{{%client}}', 'c_created_user_id', '{{%users}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('FK-client-c_updated_user_id', '{{%client}}', 'c_updated_user_id', '{{%users}}', 'id', 'SET NULL', 'CASCADE');

        $this->createTable('{{%client_email}}', [
            'ce_id' => $this->primaryKey(),
            'ce_client_id' => $this->integer()->notNull(),
            'ce_email' => $this->string(100)->notNull(),
            'ce_created_user_id' => $this->integer(),
            'ce_updated_user_id' => $this->integer(),
            'ce_created_dt' => $this->dateTime(),
            'ce_updated_dt' => $this->dateTime()
        ], $tableOptions);

        $this->addForeignKey('FK-client_email-ce_created_user_id', '{{%client_email}}', 'ce_created_user_id', '{{%users}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('FK-client_email-ce_updated_user_id', '{{%client_email}}', 'ce_updated_user_id', '{{%users}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('FK-client_email-ce_client_id', '{{%client_email}}', 'ce_client_id', '{{%client}}', 'c_id', 'CASCADE', 'CASCADE');

        $this->createTable('{{%client_phone}}', [
            'cp_id' => $this->primaryKey(),
            'cp_client_id' => $this->integer()->notNull(),
            'cp_phone' => $this->string(30)->notNull(),
            'cp_created_user_id' => $this->integer(),
            'cp_updated_user_id' => $this->integer(),
            'cp_created_dt' => $this->dateTime(),
            'cp_updated_dt' => $this->dateTime()
        ], $tableOptions);

        $this->addForeignKey('FK-client_phone-cp_created_user_id', '{{%client_phone}}', 'cp_created_user_id', '{{%users}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('FK-client_phone-cp_updated_user_id', '{{%client_phone}}', 'cp_updated_user_id', '{{%users}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('FK-client_phone-cp_client_id', '{{%client_phone}}', 'cp_client_id', '{{%client}}', 'c_id', 'CASCADE', 'CASCADE');

        $this->createTable('{{%client_employment_data}}', [
            'ced_id' => $this->primaryKey(),
            'ced_client_id' => $this->integer()->notNull(),
            'ced_type' => $this->tinyInteger(1),
            'ced_work_phone' => $this->string(30),
            'ced_organization_name' => $this->string(100),
            'ced_position' => $this->string(100),
            'ced_seniority' => $this->string(10),
            'ced_official_wages' => $this->string(50),
            'ced_unofficial_wages' => $this->string(50),
            'ced_revenues_documented' => $this->string(),
            'ced_revenues_non_documented' => $this->string(),
            'ced_created_user_id' => $this->integer(),
            'ced_updated_user_id' => $this->integer(),
            'ced_created_dt' => $this->dateTime(),
            'ced_updated_dt' => $this->dateTime()
        ], $tableOptions);

        $this->addForeignKey('FK-client_employment_data-cp_client_id', '{{%client_employment_data}}', 'ced_client_id', '{{%client}}', 'c_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK-client_employment_data-ced_created_user_id', '{{%client_employment_data}}', 'ced_created_user_id', '{{%users}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('FK-client_employment_data-ced_updated_user_id', '{{%client_employment_data}}', 'ced_updated_user_id', '{{%users}}', 'id', 'SET NULL', 'CASCADE');

        $this->createTable('{{%client_passport_data}}', [
            'cpd_client_id' => $this->primaryKey(),
            'cpd_type' => $this->tinyInteger(1),
            'cpd_series' => $this->string(100),
            'cpd_number' => $this->string(100),
            'cpd_issued_by' => $this->string(50),
            'cpd_issue_date' => $this->date(),
            'cpd_issue_department_code' => $this->string(50),
            'cpd_expiration_date' => $this->date(),
            'cpd_registered_address' => $this->string(100),
            'cpd_residence_address' => $this->string(100),
            'cpd_created_user_id' => $this->integer(),
            'cpd_updated_user_id' => $this->integer(),
            'cpd_created_dt' => $this->dateTime(),
            'cpd_updated_dt' => $this->dateTime()
        ], $tableOptions);

        $this->addForeignKey('FK-client_passport_data-cpd_client_id', '{{%client_passport_data}}', 'cpd_client_id', '{{%client}}', 'c_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK-client_passport_data-cpd_created_user_id', '{{%client_passport_data}}', 'cpd_created_user_id', '{{%users}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('FK-client_passport_data-cpd_updated_user_id', '{{%client_passport_data}}', 'cpd_updated_user_id', '{{%users}}', 'id', 'SET NULL', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%client_email}}');
        $this->dropTable('{{%client_phone}}');
        $this->dropTable('{{%client_employment_data}}');
        $this->dropTable('{{%client_passport_data}}');
        $this->dropTable('{{%client}}');
    }
}
