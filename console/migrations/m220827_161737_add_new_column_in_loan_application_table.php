<?php

use yii\db\Migration;

/**
 * Class m220827_161737_add_new_column_in_loan_application_table
 */
class m220827_161737_add_new_column_in_loan_application_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%loan_application}}', 'la_distribution_iteration', $this->smallInteger());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%loan_application}}', 'la_distribution_iteration');
    }
}
