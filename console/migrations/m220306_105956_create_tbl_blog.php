<?php

use yii\db\Migration;

/**
 * Class m220306_105956_create_tbl_blog
 */
class m220306_105956_create_tbl_blog extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%blog_categories}}', [
            'bc_id' => $this->primaryKey(),
            'bc_name' => $this->string(50),
            'bc_created_dt' => $this->dateTime(),
            'bc_updated_dt' => $this->dateTime(),
            'bc_created_user_id' => $this->integer(),
            'bc_updated_user_id' => $this->integer()
        ], $tableOptions);

        $this->addForeignKey('FK-blog_categories-bc_created_user_id', '{{%blog_categories}}', 'bc_created_user_id', '{{%users}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('FK-blog_categories-bc_updated_user_id', '{{%blog_categories}}', 'bc_updated_user_id', '{{%users}}', 'id', 'SET NULL', 'CASCADE');

        $this->createTable('{{%blog_posts}}', [
            'bp_id' => $this->primaryKey(),
            'bp_lang_id' => $this->string(5)->notNull(),
            'bp_category_id' => $this->integer()->notNull(),
            'bp_title' => $this->string(150)->notNull(),
            'bp_short_description' => $this->string(),
            'bp_content' => $this->text(),
            'bp_author_id' => $this->integer()->notNull(),
            'bp_alias' => $this->string(),
            'bp_active' => $this->boolean(),
            'bp_created_user_id' => $this->integer(),
            'bp_updated_user_id' => $this->integer(),
            'bp_created_dt' => $this->dateTime(),
            'bp_updated_dt' => $this->dateTime()
        ], $tableOptions);

        $this->addForeignKey('FK-blog_posts-bp_author_id', '{{%blog_posts}}', 'bp_author_id', '{{%users}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK-blog_posts-bp_created_user_id', '{{%blog_posts}}', 'bp_created_user_id', '{{%users}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('FK-blog_posts-bp_updated_user_id', '{{%blog_posts}}', 'bp_updated_user_id', '{{%users}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('FK-blog_posts-bp_category_id', '{{%blog_posts}}', 'bp_category_id', '{{%blog_categories}}', 'bc_id', 'CASCADE', 'CASCADE');

        $this->createIndex('UNQ-blog_posts', '{{%blog_posts}}', ['bp_lang_id', 'bp_title']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%blog_posts}}');
        $this->dropTable('{{%blog_categories}}');
    }
}
