<?php

use yii\db\Migration;

/**
 * Class m220417_075843_add_full_permission_for_superadmin_role
 */
class m220417_075843_add_full_permission_for_superadmin_role extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = \Yii::$app->authManager;

        $superAdmin = $auth->getRole('superadmin');
        if (!$superAdmin) {
            $superAdmin = $auth->createRole('superadmin');
            $superAdmin->description = 'SuperAdmin';
            $auth->add($superAdmin);
        }

        $fullPermission = $auth->createPermission('/*');
        $auth->add($fullPermission);
        $auth->addChild($superAdmin, $fullPermission);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;

        $permission = $auth->getPermission('/*');
        $auth->remove($permission);

        $roleSuperadmin = $auth->getRole('superadmin');
        $auth->remove($roleSuperadmin);
    }
}
