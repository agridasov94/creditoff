<?php

use console\migrations\RbacMigrationService;
use yii\db\Migration;

/**
 * Class m220413_160047_add_permission_to_log_pages
 */
class m220413_160047_add_permission_to_log_pages extends Migration
{
    private array $routes = [
        '/log/index',
        '/log/ajax-category-list',
        '/log/clean-table',
        '/log/view',
        '/log/delete',
        '/log/clear',
    ];

    private array $roles = [
        'root'
    ];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        (new RbacMigrationService())->up($this->routes, $this->roles);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        (new RbacMigrationService())->down($this->routes, $this->roles);
    }
}
