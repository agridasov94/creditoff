<?php

use yii\db\Migration;

/**
 * Class m220521_085726_alter_tbl_loan_application_email_phone
 */
class m220521_085726_alter_tbl_loan_application_email_phone extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropIndex('lacp_phone', '{{%loan_application_client_phone}}');
        $this->dropIndex('lace_email', '{{%loan_application_client_email}}');
        $this->alterColumn('{{%loan_application_client_email}}', 'lace_email', $this->string(100)->notNull());
        $this->alterColumn('{{%loan_application_client_phone}}', 'lacp_phone', $this->string(30)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('{{%loan_application_client_email}}', 'lace_email', $this->string(100)->notNull()->unique());
        $this->alterColumn('{{%loan_application_client_phone}}', 'lacp_phone', $this->string(30)->notNull()->unique());
    }
}
