<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_product_access_log}}`.
 */
class m220814_123834_create_user_product_access_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user_product_access_log}}', [
            'upal_id' => $this->primaryKey(),
            'upal_user_id' => $this->integer()->notNull(),
            'upal_product_id' => $this->integer()->notNull(),
            'upal_request_price_from' => $this->integer(),
            'upal_request_price_to' => $this->integer(),
            'upal_status_from' => $this->integer(),
            'upal_status_to' => $this->integer(),
            'upal_action_by_user_id' => $this->integer(),
            'upal_start_dt' => $this->dateTime(),
            'upal_end_dt' => $this->dateTime()
        ], $tableOptions);

        $this->addForeignKey('FK-user_product_access_log-upal_user_id-upal_product_id', '{{%user_product_access_log}}', ['upal_user_id', 'upal_product_id'], '{{%users_product_access}}', ['upa_user_id', 'upa_product_id'], 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK-user_product_access_log-upal_action_by_user_id', '{{%user_product_access_log}}', ['upal_action_by_user_id'], '{{%users}}', ['id'], 'SET NULL', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_product_access_log}}');
    }
}
