<?php

use console\migrations\RbacMigrationService;
use yii\db\Migration;

/**
 * Class m220520_213437_create_rbac_permissions_for_crud_pages_loan_application_client
 */
class m220520_213437_create_rbac_permissions_for_crud_pages_loan_application_client extends Migration
{
    private array $routes = [
        '/loan-application-client-email/index',
        '/loan-application-client-email/create',
        '/loan-application-client-email/update',
        '/loan-application-client-email/delete',
        '/loan-application-client-email/view',

        '/loan-application-client-phone/index',
        '/loan-application-client-phone/create',
        '/loan-application-client-phone/update',
        '/loan-application-client-phone/delete',
        '/loan-application-client-phone/view',

        '/loan-application-client-employed-data/index',
        '/loan-application-client-employed-data/create',
        '/loan-application-client-employed-data/update',
        '/loan-application-client-employed-data/delete',
        '/loan-application-client-employed-data/view',

        '/loan-application-client-passport-data/index',
        '/loan-application-client-passport-data/create',
        '/loan-application-client-passport-data/update',
        '/loan-application-client-passport-data/delete',
        '/loan-application-client-passport-data/view',
    ];

    public array $roles = [
        'root'
    ];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
//        (new RbacMigrationService())->up($this->routes, $this->roles);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
//        (new RbacMigrationService())->down($this->routes, $this->roles);
    }
}
