<?php

use yii\db\Migration;

/**
 * Class m210328_195918_create_tbl_items_category
 */
class m210328_195918_create_tbl_items_category extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('items_category', [
        'id' => $this->primaryKey(),
        'name' => $this->string(50)->notNull()->unique(),
        'added_by' => $this->integer(),
        'created_dt' => $this->dateTime(),
        'updated_by' => $this->integer(),
        'updated_at' => $this->dateTime(),
        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB');
        
        $this->addForeignKey(
            'added_by-id',
            'items_category',
            'added_by',
            'users',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'updated_by-id',
            'items_category',
            'updated_by',
            'users',
            'id',
            'CASCADE'
        );
    }

        /**
        * {@inheritdoc}
        */
        public function down()
        {
        $this->dropTable('items_category');
        }
}
