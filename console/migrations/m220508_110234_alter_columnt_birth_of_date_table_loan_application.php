<?php

use yii\db\Migration;

/**
 * Class m220508_110234_alter_columnt_birth_of_date_table_loan_application
 */
class m220508_110234_alter_columnt_birth_of_date_table_loan_application extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%loan_application}}', 'la_client_birth_date', $this->date());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('{{%loan_application}}', 'la_client_birth_date', $this->dateTime());
    }
}
