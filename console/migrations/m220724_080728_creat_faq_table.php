<?php

use yii\db\Migration;

/**
 * Class m220724_080728_creat_faq_table
 */
class m220724_080728_creat_faq_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%faq}}', [
            'f_id' => $this->integer()->notNull(),
            'f_lang_id' => $this->string(5)->notNull(),
            'f_question' => $this->string()->notNull(),
            'f_answer' => $this->text(),
            'f_created_dt' => $this->dateTime(),
            'f_updated_dt' => $this->dateTime(),
            'f_created_user_id' => $this->integer(),
            'f_updated_user_id' => $this->integer()
        ], $tableOptions);

        $this->db->createCommand('alter table faq modify f_lang_id varchar(5) CHARACTER SET utf8 collate utf8_unicode_ci not null')->execute();
        $this->addPrimaryKey('PK-faq', '{{%faq}}', ['f_id', 'f_lang_id']);
        $this->addForeignKey('FK-faq-f_lang_id', '{{%faq}}', 'f_lang_id', '{{%language}}', 'language_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK-faq-f_created_user_id', '{{%faq}}', 'f_created_user_id', '{{%users}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('FK-faq-f_updated_user_id', '{{%faq}}', 'f_updated_user_id', '{{%users}}', 'id', 'SET NULL', 'CASCADE');
        $this->alterColumn('{{%faq}}', 'f_id', $this->integer()->notNull() . ' AUTO_INCREMENT');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%faq}}');
    }
}
