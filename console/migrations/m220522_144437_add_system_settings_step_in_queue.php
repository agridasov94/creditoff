<?php

use common\src\entities\settings\entity\Settings;
use yii\db\Migration;

/**
 * Class m220522_144437_add_system_settings_step_in_queue
 */
class m220522_144437_add_system_settings_step_in_queue extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('{{%settings}}', [
            's_key' => 'step_in_queue_variables',
            's_name' => 'Step In Queue Variables',
            's_description' => null,
            's_type' => Settings::TYPE_ARRAY,
            's_value' => json_encode([
                'c' => 20,
                'd' => 4,
                'i' => 10
            ]),
            's_updated_dt' => date('Y-m-d H:i:s')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('{{%settings}}', ['IN', 's_key', [
            'step_in_queue_variables'
        ]]);
    }
}
