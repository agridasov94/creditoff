<?php

use common\src\entities\settings\entity\Settings;
use yii\db\Migration;

/**
 * Class m220722_204830_add_new_site_setting_for_storing_contact_us_email
 */
class m220722_204830_add_new_site_setting_for_storing_contact_us_email extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('{{%settings}}', [
            's_key' => 'contact_us_email_address',
            's_name' => 'Email Address for sending an email to',
            's_description' => null,
            's_type' => Settings::TYPE_STRING,
            's_value' => '',
            's_updated_dt' => date('Y-m-d H:i:s')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('{{%settings}}', ['IN', 's_key', [
            'contact_us_email_address'
        ]]);
    }
}
