<?php

use yii\db\Migration;

/**
 * Class m220419_185402_add_column_to_client_tbl
 */
class m220419_185402_add_column_to_client_tbl extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%client}}', 'c_is_blocked', $this->tinyInteger(1)->after('c_children'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%client}}', 'c_is_blocked');
    }
}
