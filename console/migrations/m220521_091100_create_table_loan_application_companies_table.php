<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%table_loan_application_companies}}`.
 */
class m220521_091100_create_table_loan_application_companies_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%loan_application_companies}}', [
            'lac_la_id' => $this->integer()->notNull(),
            'lac_partner_company_id' => $this->integer()->notNull(),
            'lac_created_dt' => $this->dateTime()
        ]);
        $this->addPrimaryKey('PK-loan_application_companies', '{{%loan_application_companies}}', ['lac_la_id', 'lac_partner_company_id']);
        $this->addForeignKey('Fk-loan_application_companies-lac_la_id', '{{%loan_application_companies}}', 'lac_la_id', '{{%loan_application}}', 'la_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('Fk-loan_application_companies-lac_partner_company_id', '{{%loan_application_companies}}', 'lac_partner_company_id', '{{%partner_company}}', 'pc_id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%table_loan_application_companies}}');
    }
}
