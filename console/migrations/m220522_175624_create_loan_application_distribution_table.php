<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%loan_application_distribution}}`.
 */
class m220522_175624_create_loan_application_distribution_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%loan_application_distribution}}', [
            'lad_id' => $this->primaryKey(),
            'lad_product_id' => $this->integer(),
            'lad_user_id' => $this->integer(),
            'lad_queue_depth' => $this->smallInteger()->unsigned(),
            'lad_total_display_loan_application' => $this->smallInteger(),
            'lad_created_dt' => $this->dateTime(),
            'lad_created_user_id' => $this->integer()
        ]);
        $this->addForeignKey('FK-loan_application_distribution-lad_product_id', '{{%loan_application_distribution}}', 'lad_product_id', '{{%product}}', 'p_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK-loan_application_distribution-lad_user_id', '{{%loan_application_distribution}}', 'lad_user_id', '{{%users}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK-loan_application_distribution-lad_created_user_id', '{{%loan_application_distribution}}', 'lad_created_user_id', '{{%users}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%loan_application_distribution}}');
    }
}
