<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%partner_company_position}}`.
 */
class m220420_075410_create_partner_company_position_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%partner_company_position}}', [
            'pcp_id' => $this->primaryKey(),
            'pcp_name' => $this->string(50),
            'pcp_created_user_id' => $this->integer(),
            'pcp_updated_user_id' => $this->integer(),
            'pcp_created_dt' => $this->dateTime(),
            'pcp_updated_dt' => $this->dateTime()
        ]);

        $this->addForeignKey('FK-partner_company_position-pcp_created_user_id', '{{%partner_company_position}}', 'pcp_created_user_id', '{{%users}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('FK-partner_company_position-pcp_updated_user_id', '{{%partner_company_position}}', 'pcp_updated_user_id', '{{%users}}', 'id', 'SET NULL', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%partner_company_position}}');
    }
}
