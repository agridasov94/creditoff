<?php

use yii\db\Migration;

/**
 * Class m230403_170014_add_site_setting_privacy_policy_file_path
 */
class m230403_170014_add_site_setting_privacy_policy_file_path extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('{{%settings}}', [
            's_key' => 'privacy_policy_file_path',
            's_name' => 'Privacy Policy file path',
            's_description' => null,
            's_type' => \common\src\entities\settings\entity\Settings::TYPE_STRING,
            's_value' => '/files/Acord_prelucrare_date_cu_caracter_personal.pdf',
            's_updated_dt' => date('Y-m-d H:i:s')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('{{%settings}}', ['IN', 's_key', [
            'privacy_policy_file_path'
        ]]);
    }
}
