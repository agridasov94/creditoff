<?php

use yii\db\Migration;

/**
 * Class m220419_191334_add_tbl_partener_company
 */
class m220419_191334_add_tbl_partener_company extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%partner_company}}', [
            'pc_id' => $this->primaryKey(),
            'pc_name' => $this->string(100),
            'pc_fiscal_code' => $this->string(50),
            'pc_total_amount' => $this->smallInteger(),
            'pc_total_bonus_amount' => $this->smallInteger(),
            'pc_is_blocked' => $this->tinyInteger(1),
            'pc_created_user_id' => $this->integer(),
            'pc_updated_user_id' => $this->integer(),
            'pc_created_dt' => $this->dateTime(),
            'pc_updated_dt' => $this->dateTime()
        ], $tableOptions);

        $this->addForeignKey('FK-partner_company-pc_created_user_id', '{{%partner_company}}', 'pc_created_user_id', '{{%users}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('FK-partner_company-pc_updated_user_id', '{{%partner_company}}', 'pc_updated_user_id', '{{%users}}', 'id', 'SET NULL', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%partner_company}}');
    }
}
