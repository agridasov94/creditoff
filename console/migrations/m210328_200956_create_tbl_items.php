<?php

use yii\db\Migration;

/**
 * Class m210328_200956_create_tbl_items
 */
class m210328_200956_create_tbl_items extends Migration
{
    /**
     * {@inheritdoc}
     */
        public function up()
    {
        $this->createTable('items', [
        'id' => $this->primaryKey(),
        'name' => $this->string(100)->notNull()->unique(),
        'description' => $this->string(1000)->notNull(),
        'category_id' => $this->integer(),
        'count' => $this->smallInteger(),
        'added_by' => $this->integer(),
        'created_dt' => $this->dateTime(),
        'updated_by' => $this->integer(),
        'updated_at' => $this->dateTime(),
        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB');
        
        $this->addForeignKey(
            'category_id-id/i',
            'items',
            'category_id',
            'items_category',
            'id',
            'CASCADE'
        );
        
        $this->addForeignKey(
            'added_by-id/i',
            'items',
            'added_by',
            'users',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'updated_by-id/i',
            'items',
            'updated_by',
            'users',
            'id',
            'CASCADE'
        );
    
    
    }


    /**
    * {@inheritdoc}
    */
    public function down()
    {
        $this->dropTable('items');
    }
}
