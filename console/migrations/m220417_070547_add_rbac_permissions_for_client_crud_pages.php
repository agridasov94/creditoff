<?php

use yii\db\Migration;

/**
 * Class m220417_070547_add_rbac_permissions_for_client_crud_pages
 */
class m220417_070547_add_rbac_permissions_for_client_crud_pages extends Migration
{
    private array $routes = [
        '/client/create',
        '/client/update',
        '/client/delete',
        '/client/view',
        '/client/index',
    ];

    private array $roles = [
        'root'
    ];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        (new \console\migrations\RbacMigrationService())->up($this->routes, $this->roles);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        (new \console\migrations\RbacMigrationService())->down($this->routes, $this->roles);
    }
}
