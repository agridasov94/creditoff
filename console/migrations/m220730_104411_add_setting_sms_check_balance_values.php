<?php

use yii\db\Migration;

/**
 * Class m220730_104411_add_setting_sms_check_balance_values
 */
class m220730_104411_add_setting_sms_check_balance_values extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('{{%settings}}', [
            's_key' => 'sms_check_balance_amount',
            's_name' => '',
            's_description' => null,
            's_type' => \common\src\entities\settings\entity\Settings::TYPE_ARRAY,
            's_value' => json_encode([
                'critical_balance_amount' => 200.00,
                'user_roles_to_be_notified' => [
                    'Admin'
                ]
            ]),
            's_updated_dt' => date('Y-m-d H:i:s')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('{{%settings}}', ['IN', 's_key', [
            'sms_check_balance_amount'
        ]]);
    }
}
