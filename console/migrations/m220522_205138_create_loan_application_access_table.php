<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%loan_application_access}}`.
 */
class m220522_205138_create_loan_application_access_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%loan_application_access}}', [
            'laa_id' => $this->primaryKey(),
            'laa_la_id' => $this->integer(),
            'laa_user_id' => $this->integer(),
            'laa_status' => $this->tinyInteger(),
            'laa_action_by' => $this->tinyInteger(),
            'laa_created_dt' => $this->dateTime(),
            'laa_updated_dt' => $this->dateTime(),
            'laa_created_user_id' => $this->integer(),
            'laa_updated_user_id' => $this->integer()
        ]);

        $this->addForeignKey('FK-loan_application_access-laa_la_id', '{{%loan_application_access}}', 'laa_la_id', '{{%loan_application}}', 'la_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK-loan_application_access-laa_user_id', '{{%loan_application_access}}', 'laa_user_id', '{{%users}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK-loan_application_access-laa_created_user_id', '{{%loan_application_access}}', 'laa_created_user_id', '{{%users}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('FK-loan_application_access-laa_updated_user_id', '{{%loan_application_access}}', 'laa_updated_user_id', '{{%users}}', 'id', 'SET NULL', 'CASCADE');

        $this->addColumn('{{%loan_application}}', 'la_manual_distribution', $this->tinyInteger(1)->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%loan_application_access}}');
        $this->dropColumn('{{%loan_application}}', 'la_manual_distribution');
    }
}
