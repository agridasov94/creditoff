<?php

use yii\db\Migration;

/**
 * Class m220320_145449_alter_tbl_blog_posts_add_column_preview_image
 */
class m220320_145449_alter_tbl_blog_posts_add_column_preview_image extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%blog_posts}}', 'bp_preview_image', $this->string()->after('bp_alias'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%blog_posts}}', 'bp_preview_image');
    }
}
