<?php

use yii\db\Migration;

/**
 * Class m220806_101920_add_new_site_setting_seconds_lifetime_actiovation_code
 */
class m220806_101920_add_new_site_setting_seconds_lifetime_actiovation_code extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('{{%settings}}', [
            's_key' => 'seconds_lifetime_profile_activation_code',
            's_name' => 'Lifetime in seconds of profile activation code',
            's_description' => null,
            's_type' => \common\src\entities\settings\entity\Settings::TYPE_INT,
            's_value' => 120,
            's_updated_dt' => date('Y-m-d H:i:s')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('{{%settings}}', ['IN', 's_key', [
            'seconds_lifetime_profile_activation_code'
        ]]);
    }
}
