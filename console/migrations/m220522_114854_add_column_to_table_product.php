<?php

use yii\db\Migration;

/**
 * Class m220522_114854_add_column_to_table_product
 */
class m220522_114854_add_column_to_table_product extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%product}}', 'p_step_in_queue', $this->smallInteger()->unsigned()->after('p_min_request_price'));
        $this->addColumn('{{%product}}', 'p_step_in_queue_auto_calculate', $this->tinyInteger(1)->defaultValue(1)->after('p_step_in_queue'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%product}}', 'p_step_in_queue');
        $this->dropColumn('{{%product}}', 'p_step_in_queue_auto_calculate');
    }
}
