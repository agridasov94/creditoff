<?php

use yii\db\Migration;

/**
 * Class m220621_220915_alter_table_users_alter_fk
 */
class m220621_220915_alter_table_users_alter_fk extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('FK-users_product_access-upa_user_id', '{{%users_product_access}}');
        $this->dropForeignKey('FK-users_product_access-upa_product_id', '{{%users_product_access}}');
        $this->addForeignKey('FK-users_product_access-upa_user_id', '{{%users_product_access}}', 'upa_user_id', '{{%users}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK-users_product_access-upa_product_id', '{{%users_product_access}}', 'upa_product_id', '{{%product}}', 'p_id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK-users_product_access-upa_user_id', '{{%users_product_access}}');
        $this->dropForeignKey('FK-users_product_access-upa_product_id', '{{%users_product_access}}');
        $this->addForeignKey('FK-users_product_access-upa_user_id', '{{%users_product_access}}', 'upa_user_id', '{{%users}}', 'id', 'NO ACTION', 'CASCADE');
        $this->addForeignKey('FK-users_product_access-upa_product_id', '{{%users_product_access}}', 'upa_product_id', '{{%product}}', 'p_id', 'NO ACTION', 'CASCADE');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220621_220915_alter_table_users_alter_fk cannot be reverted.\n";

        return false;
    }
    */
}
