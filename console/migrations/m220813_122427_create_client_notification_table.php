<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%client_notification}}`.
 */
class m220813_122427_create_client_notification_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%client_notification}}', [
            'cn_id'         => $this->primaryKey(),
            'cn_unique_id'  => $this->string(40),
            'cn_client_id'    => $this->integer()->notNull(),
            'cn_type_id'    => $this->smallInteger()->notNull(),
            'cn_title'      => $this->string(100),
            'cn_message'    => $this->text(),
            'cn_new'        => $this->boolean()->defaultValue(true),
            'cn_deleted'    => $this->boolean()->defaultValue(false),
            'cn_popup'      => $this->boolean()->defaultValue(false),
            'cn_popup_show' => $this->boolean()->defaultValue(false),
            'cn_read_dt'    => $this->dateTime(),
            'cn_created_dt' => $this->dateTime(),
            'cn_created_by' => $this->integer()
        ], $tableOptions);
        $this->addForeignKey('FK-client_notification-cn_client_id', '{{%client_notification}}', ['cn_client_id'], '{{%client}}', ['c_id'], 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK-client_notification-cn_created_by', '{{%client_notification}}', ['cn_created_by'], '{{%users}}', ['id'], 'SET NULL', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%client_notification}}');
    }
}
