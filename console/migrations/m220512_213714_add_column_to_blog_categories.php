<?php

use yii\db\Migration;

/**
 * Class m220512_213714_add_column_to_blog_categories
 */
class m220512_213714_add_column_to_blog_categories extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->truncateTable('{{%blog_posts}}');
        $this->db->createCommand('delete from blog_categories where bc_id >= 1')->execute();
        $this->addColumn('{{%blog_categories}}', 'bc_url', $this->string(255)->notNull()->after('bc_name'));
        $this->createIndex('UNQ-blog_categories-bc_url-bc_lang_name', '{{%blog_categories}}', ['bc_lang_id', 'bc_url'], true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK-blog_categories-bc_lang_id', '{{%blog_categories}}');
        $this->dropIndex('UNQ-blog_categories-bc_url-bc_lang_name', '{{%blog_categories}}');
        $this->dropColumn('{{%blog_categories}}', 'bc_url');
        $this->addForeignKey('FK-blog_categories-bc_lang_id', '{{%blog_categories}}', 'bc_lang_id', 'language', 'language_id', 'CASCADE', 'CASCADE');
    }
}
