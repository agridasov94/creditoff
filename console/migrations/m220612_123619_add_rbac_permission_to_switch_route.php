<?php

use common\src\entities\users\entity\Users;
use console\migrations\RbacMigrationService;
use yii\db\Migration;

/**
 * Class m220612_123619_add_rbac_permission_to_switch_route
 */
class m220612_123619_add_rbac_permission_to_switch_route extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        (new RbacMigrationService())->up([
            '/users/switch'
        ], [Users::ROLE_ROOT, Users::ROLE_ADMIN]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        (new RbacMigrationService())->down([
            '/users/switch'
        ], [Users::ROLE_ROOT, Users::ROLE_ADMIN]);
    }
}
