<?php

use yii\db\Migration;

/**
 * Class m230416_125930_create_table_main_banner
 */
class m230416_125930_create_table_main_banner extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableExists = $this->db->getTableSchema('{{%main_banner}}', true);
        if ($tableExists === null) {
            $tableOptions = null;
            if ($this->db->driverName === 'mysql') {
                $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
            }

            $this->createTable('{{%main_banner}}', [
                'mb_id' => $this->integer(),
                'mb_title' => $this->string()->notNull(),
                'mb_subtitle' => $this->string(),
                'mb_link' => $this->string()->notNull(),
                'mb_link_text' => $this->string()->notNull(),
                'mb_is_active' => $this->smallInteger()->notNull()->defaultValue(0),
                'mb_is_deleted' => $this->smallInteger()->notNull()->defaultValue(0),
                'mb_order' => $this->integer()->notNull()->defaultValue(0),
                'mb_description' => $this->text(),
                'mb_file_path' => $this->string()->notNull(),
                'mb_file_type' => $this->smallInteger()->notNull(),
                'mb_lang_id' => $this->string(5)->notNull(),
                'mb_created_by' => $this->integer(),
                'mb_updated_by' => $this->integer(),
                'mb_created_dt' => $this->dateTime(),
                'mb_updated_dt' => $this->dateTime(),
            ], $tableOptions);

            $this->db->createCommand('alter table main_banner modify mb_lang_id varchar(5) CHARACTER SET utf8 collate utf8_unicode_ci not null')->execute();
            $this->addPrimaryKey('pk-main_banner', '{{%main_banner}}', ['mb_id', 'mb_lang_id']);
            $this->db->createCommand('alter table main_banner modify mb_id int not null auto_increment')->execute();

            $this->addForeignKey(
                '{{%fk-main_banner-created_by}}',
                '{{%main_banner}}',
                'mb_created_by',
                '{{%users}}',
                'id',
                'CASCADE',
                'CASCADE'
            );

            $this->addForeignKey(
                '{{%fk-main_banner-updated_by}}',
                '{{%main_banner}}',
                'mb_updated_by',
                '{{%users}}',
                'id',
                'CASCADE',
                'CASCADE'
            );

            $this->addForeignKey(
                '{{%fk-main_banner-lang_id}}',
                '{{%main_banner}}',
                'mb_lang_id',
                '{{%language}}',
                'language_id',
                'CASCADE',
                'CASCADE'
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // check if foreign key fk-main_banner-created_by exists before dropping it
        $fkExists = $this->db->getTableSchema('{{%main_banner}}', true)->foreignKeys;
        if (isset($fkExists['fk-main_banner-created_by'])) {
            $this->dropForeignKey('{{%fk-main_banner-created_by}}', '{{%main_banner}}');
        }
        // check if foreign key fk-main_banner-updated_by exists before dropping it
        if (isset($fkExists['fk-main_banner-updated_by'])) {
            $this->dropForeignKey('{{%fk-main_banner-updated_by}}', '{{%main_banner}}');
        }
        // check if foreign key fk-main_banner-lang_id exists before dropping it
        if (isset($fkExists['fk-main_banner-lang_id'])) {
            $this->dropForeignKey('{{%fk-main_banner-lang_id}}', '{{%main_banner}}');
        }
        // check if table exists before dropping it
        $tableExists = $this->db->getTableSchema('{{%main_banner}}', true);
        if ($tableExists !== null) {
            $this->dropTable('{{%main_banner}}');
        }
    }
}
