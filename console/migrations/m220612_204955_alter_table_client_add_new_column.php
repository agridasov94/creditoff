<?php

use yii\db\Migration;

/**
 * Class m220612_204955_alter_table_client_add_new_column
 */
class m220612_204955_alter_table_client_add_new_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%client}}', 'c_auth_key', $this->string(32));
        $this->addColumn('{{%client}}', 'c_is_activated', $this->boolean()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%client}}', 'c_auth_key');
        $this->dropColumn('{{%client}}', 'c_is_activated');
    }
}
