<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%loan_application}}`.
 */
class m220425_064618_create_loan_application_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%loan_application}}', [
            'la_id' => $this->primaryKey(),
            'la_number' => $this->string(10)->unique()->notNull(),
            'la_product_id' => $this->integer()->notNull(),
            'la_owner_id' => $this->integer(),
            'la_status' => $this->tinyInteger()->notNull(),
            'la_loan_amount' => $this->integer()->notNull(),
            'la_loan_term' => $this->smallInteger()->notNull(),
            'la_client_id' => $this->integer(),
            'la_client_name' => $this->string(100)->notNull(),
            'la_client_birth_date' => $this->dateTime()->notNull(),
            'la_client_idnp' => $this->string(13)->notNull(),
            'la_client_phone' => $this->string(50),
            'la_client_email' => $this->string(50),
            'la_updated_status_dt' => $this->dateTime(),
            'la_created_dt' => $this->dateTime(),
            'la_updated_dt' => $this->dateTime(),
            'la_updated_user_id' => $this->integer()
        ], $tableOptions);

        $this->addForeignKey('FK-loan_application-la_product_id', '{{%loan_application}}', 'la_product_id', '{{%product}}', 'p_id', 'NO ACTION', 'CASCADE');
        $this->addForeignKey('FK-loan_application-la_owner_id', '{{%loan_application}}', 'la_owner_id', '{{%users}}', 'id', 'NO ACTION', 'CASCADE');
        $this->addForeignKey('FK-loan_application-la_client_id', '{{%loan_application}}', 'la_client_id', '{{%client}}', 'c_id', 'NO ACTION', 'CASCADE');
        $this->addForeignKey('FK-loan_application-la_updated_user_id', '{{%loan_application}}', 'la_updated_user_id', '{{%users}}', 'id', 'SET NULL', 'CASCADE');

        $this->createTable('{{%loan_application_log}}', [
            'lal_id' => $this->primaryKey(),
            'lal_loan_application_id' => $this->integer()->notNull(),
            'lal_owner_id' => $this->integer(),
            'lal_status_before' => $this->tinyInteger(),
            'lal_status_after' => $this->tinyInteger(),
            'lal_start_dt' => $this->dateTime(),
            'lal_end_dt' => $this->dateTime()
        ], $tableOptions);

        $this->addForeignKey('FK-loan_application_log-lal_loan_application_id', '{{%loan_application_log}}', 'lal_loan_application_id', '{{%loan_application}}', 'la_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK-loan_application_log-lal_owner_id', '{{%loan_application_log}}', 'lal_owner_id', '{{%users}}', 'id', 'SET NULL', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%loan_application}}');
        $this->dropTable('{{%loan_application_log}}');
    }
}
