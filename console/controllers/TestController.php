<?php
namespace console\controllers;

use backend\src\entities\usersNotifications\entity\UsersNotifications;
use backend\src\entities\usersNotifications\UsersNotificationsService;
use backend\src\entities\usersProductAccess\entity\UsersProductAccessQuery;
use backend\src\helpers\WebSocketHelper;
use common\components\jobs\sms\SendSmsJob;
use common\components\jobs\test\TestJob;
use common\components\sms\SmsComponent;
use common\src\entities\loanApplication\entity\LoanApplication;
use common\src\entities\loanApplication\entity\LoanApplicationQuery;
use common\src\entities\product\entity\Product;
use common\src\entities\product\entity\ProductQuery;
use common\src\entities\users\entity\UsersQuery;
use common\src\helpers\loanApplication\LoanApplicationHelper;
use common\src\services\product\ProductService;
use frontend\components\jobs\LoanApplicationDistributionJob;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\redis\Connection;

class TestController extends \yii\console\Controller
{
    public function actionSendEmailByUserId($userId)
    {
        $user = \common\src\entities\users\entity\Users::findOne($userId);
        if (!$user) {
            throw new \RuntimeException('User Not found');
        }

        $sendEmail = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($user->email)
            ->setSubject('Account registration at ' . Yii::$app->name)
            ->send();

        echo $sendEmail . PHP_EOL;
    }

    public function actionRedisConnection()
    {
        /** @var $redis Connection */
        $redis = Yii::$app->redis;
        /** @var $smsComponent SmsComponent */
        $smsComponent = Yii::$app->sms;

        $code = $smsComponent->generateActivationCode();

        $redis->setex('client_1_code', 60, $code);
    }

    public function actionGetRedisKey(string $key)
    {
        /** @var $redis Connection */
        $redis = Yii::$app->redis;
        var_dump($redis->get('client_1_code'));
    }

    public function actionTestSms()
    {
        /** @var $smsComponent SmsComponent */
        $smsComponent = Yii::$app->sms;

        $result = $smsComponent->send('+37378077519', 'Activation Code: 415SF2');

        if ($result->hasError()) {
            var_dump([
                'errorCode' => $result->getErrorCode(),
                'errorMessage' => $result->getErrorMessage()
            ]) . PHP_EOL;die;
        }

        var_dump($result->getResult());
    }

    public function actionTestJob()
    {
        /** @var $queue \yii\queue\beanstalk\Queue */
        $queue = Yii::$app->queue_loan_application_distribution;
        $job = new TestJob();
        $queue->push($job);
    }

    public function actionTestWsSendMessage()
    {
        /** @var $redis Connection */
        $redis = Yii::$app->redis;
        $data = Json::encode([
            'cmd' => 'command',
            'message' => 'hello'
        ], JSON_INVALID_UTF8_SUBSTITUTE);
        WebSocketHelper::pub(['loan-application-1-1'], 'add_loan_application_row', [
            'yo' => 'hello world'
        ]);
    }

    public function actionSendWsToUser()
    {
        $data = [
            'cmd' => 'command',
            'message' => 'hello'
        ];
        WebSocketHelper::pub(['user-1'], 'hello', $data);
    }

    public function actionTestQuery()
    {
        $service = Yii::createObject(ProductService::class);
        $product = Product::findOne(1);
        $service->calculateStepInQueue($product);
    }

    public function actionAvailableUsers()
    {
        $data = UsersProductAccessQuery::findAllAvailableUsersByProduct(1);
        var_dump(ArrayHelper::toArray($data));
    }

    public function actionDistributionLogic()
    {
        $job = new LoanApplicationDistributionJob(1);
        /** @var $queue \yii\queue\beanstalk\Queue */
        $queue = Yii::$app->queue_loan_application_distribution;
        $queue->push($job);
    }

    public function actionCreateAndSendNotification()
    {
        $service = Yii::createObject(UsersNotificationsService::class);
        $message = <<<HTML
you have a new entry in the application list. <a href="/loan-application/sent">Check it out</a>.
HTML;
        $service->createAndPublish(1, 'New Loan Application', $message, UsersNotifications::TYPE_INFO, false);
    }

    public function actionQuery()
    {
        echo UsersQuery::findAvailableUsersForLoanApplication(29);
    }

    public function actionQuery2()
    {
        echo LoanApplicationQuery::findAllSentByProductExceptDeclinedId(1);
    }

    public function actionQuery3()
    {
        var_dump(ProductQuery::getListIdsByUsersAssignedToCompany(3));die;
    }

    public function actionLog()
    {
        /** @var $sms SmsComponent */
        $sms = \Yii::$app->sms;
        $code = $sms->generateActivationCode();

        $job = new SendSmsJob('078077519', \Yii::t('frontend', 'Activation code: {code}', [
            'code' => $code
        ]));
        /** @var $queue \yii\queue\beanstalk\Queue */
        $queue = \Yii::$app->queue_sms_job;
        $queue->delay(0)->push($job);
    }

    public function actionWs()
    {
        $application = LoanApplication::findOne(92);
        WebSocketHelper::pub(
            [WebSocketHelper::getLoanApplicationPubSubKey((int)$application->la_status, (int)1)],
            'add_loan_application',
            [
                'data' => LoanApplicationHelper::generateLoanApplicationSentData($application),
                'loanApplicationId' => $application->la_id,
                'btnHtml' => LoanApplicationHelper::getLoanApplicationSentBtn($application->la_number)
            ]
        );
    }
}