<?php

namespace console\controllers;

use backend\src\entities\usersNotifications\UsersNotificationsService;
use common\components\sms\SmsComponent;
use common\src\services\sms\SmsService;

class SmsController extends \yii\console\Controller
{
    private SmsService $smsService;

    public function __construct($id, $module, SmsService $smsService, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->smsService = $smsService;
    }

    public function actionCheckBalance()
    {
        $this->smsService->checkBalance(\Yii::createObject(UsersNotificationsService::class));
    }
}