<?php

use yii2mod\rbac\migrations\Migration;

class m220216_210811_create_role_root extends Migration
{
    public function safeUp()
    {
        $this->createRole('root', 'root role for access to the entire system');
    }

    public function safeDown()
    {
        $this->removeRole('root');
    }
}